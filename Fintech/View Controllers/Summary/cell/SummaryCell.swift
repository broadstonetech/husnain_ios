//
//  SummaryCell.swift
//  Fintech
//
//  Created by apple on 23/03/2020.
//  Copyright © 2020 Broadstone Technologies. All rights reserved.
//

import UIKit

class SummaryCell: UITableViewCell {
    @IBOutlet weak var CategaryName_lbl: UILabel!
    @IBOutlet weak var Question_lbl: UILabel!
    @IBOutlet weak var Answer_lbl: UILabel!
    @IBOutlet weak var backView :UIView!
    @IBOutlet weak var bottomView :UIView!
    @IBOutlet weak var innerView :UIView!
    @IBOutlet weak  var bottomconst : NSLayoutConstraint!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setColor (color: UIColor){
        innerView.backgroundColor = color
        backView.backgroundColor = color
    }
    func setlastCell()  {
        bottomView.isHidden = true
        bottomconst.constant = 1
    }
}
