//
//  CategoryPieChartCell.swift
//  Fintech
//
//  Created by broadstone on 13/02/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import Foundation
import UIKit
import Charts
class CategoryPieChartCell: UITableViewCell {
    @IBOutlet weak var pieChartView: PieChartView!
    @IBOutlet weak var filterBtn: UIButton!
    
    override class func awakeFromNib() {
        super.awakeFromNib()
    }
}
