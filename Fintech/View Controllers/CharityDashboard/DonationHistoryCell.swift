//
//  DonationHistoryCell.swift
//  Fintech
//
//  Created by broadstone on 30/12/2020.
//  Copyright © 2020 Broadstone Technologies. All rights reserved.
//

import Foundation
import UIKit
class DonationHistoryCell:UITableViewCell{
    @IBOutlet weak var charityName:UILabel!
    @IBOutlet weak var donationAmount:UILabel!
    @IBOutlet weak var donationMethod:UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
