//
//  SubmitQuestionDataModel.swift
//  Fintech
//
//  Created by Apple on 29/10/2020.
//  Copyright © 2020 Broadstone Technologies. All rights reserved.
//

import Foundation


class SubmitQuestionDataModel : NSObject{
    
    fileprivate var questionid : String
    fileprivate var categoryid : String
    
    var answerid = [String]()
    
    override init() {
        questionid = ""
        categoryid = ""
        
    }
    func setquestionid(_ text: String){
        self.questionid = text
    }
    func  getquestionidle() ->String {
        return self.questionid
    }
    //
    func setcategoryid(_ text: String){
        self.categoryid = text
    }
    func  getcategoryid() ->String {
        return self.categoryid
    }
    //
//    func setanswerid(_ text: String){
//        self.answerid = [text]
//    }
//    func  getanswerid() ->String {
//        return self.answerid
//    }
    
    
}



class SubmitQDataModel: Codable{
    var question_id = String()
    var answer_id = [String]()
    var category_id = String()
    
    init(qid : String,aid : [String],cid : String) {
        question_id = qid
        answer_id = aid
        category_id = cid
        
        
    }

}
