//
//  DeepDivesCell.swift
//  Fintech
//
//  Created by MacBook-Mubashar on 31/07/2019.
//  Copyright © 2019 Broadstone Technologies. All rights reserved.
//

import UIKit
import Lottie
class DeepDivesCell: UITableViewCell {
    @IBOutlet weak var awardImage_img: UIImageView!
    
    @IBOutlet weak var award_education_img: AnimationView!
    @IBOutlet weak var background_view: UIView!
    @IBOutlet weak var heading_lbl: UILabel!
    @IBOutlet weak var description_lbl: UILabel!
    @IBOutlet weak var Image_education_img: UIImageView!
    @IBOutlet weak var Image_img: UIImageView!
    @IBOutlet weak var Image_deepDives_img: UIImageView!
    @IBOutlet weak var Image_keyDocs_img: UIImageView!
    @IBOutlet weak var Image_Media_img: UIImageView!
    @IBOutlet weak var percentage: UILabel!

    @IBOutlet weak var heading_backtest_lbl: UILabel!
    
    @IBOutlet weak var return_backtest_lbl: UILabel!
    @IBOutlet weak var return_amount_backtest_lbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

class DeepDivesCell3: UITableViewCell {

    @IBOutlet weak var heading_lbl: UILabel!
   
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
