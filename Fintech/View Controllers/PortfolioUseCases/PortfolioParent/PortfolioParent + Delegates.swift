//
//  PortfolioParent + Delegates.swift
//  Fintech
//
//  Created by broadstone on 14/03/2022.
//  Copyright © 2022 Broadstone Technologies. All rights reserved.
//

import Foundation
struct ValueMappedPortfolioData {
    var portfolioId: Int
    var risk: Int
    var template: String
    var portfolioData: PortfolioImpact?
}

protocol AssetsUpdateDelegate {
    func onCartUpdate(tickersInCart: [PortfolioTickersData], proceedToAlloc: Bool)
    func cartDeleted()
}

protocol ValueMappedApiDelegate {
    func apiCalled(valueName: String, bucketType: String, model: ValueMappedTickersModel)
}

protocol SavePortfolioDelegate {
    func portfolioSaved(success: Bool)
}

protocol PortfolioListApiDalegate {
    func portfolioListApiCalled(model: [PortfolioListData])
}

protocol ScoreImprovementTemplateDelegate {
    func scoreImrpovementTemplateSelected(_ selectedTemplate: PortfolioValuesTemplate)
}

struct PortfolioEsgType {
    static let ENV = "env"
    static let SOC = "soc"
    static let GOV = "gov"
}
