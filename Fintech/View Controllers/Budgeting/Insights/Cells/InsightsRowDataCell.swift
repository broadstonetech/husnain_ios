//
//  InsightValueMappingCell.swift
//  Task1
//
//  Created by Billal on 29/01/2021.
//

import UIKit

class InsightsRowDataCell: UITableViewCell {

    @IBOutlet weak var categoryLbl: UILabel!
    @IBOutlet weak var budgetLbl: UILabel!
    @IBOutlet weak var spentLbl: UILabel!
    @IBOutlet weak var diffImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
