//
//  ExpenseTrackingHeadingCell.swift
//  Fintech
//
//  Created by broadstone on 08/07/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import UIKit

class ExpenseTrackingHeadingCell: UITableViewCell {
    
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var parentView: UIView!
    @IBOutlet weak var bucketIcon: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
