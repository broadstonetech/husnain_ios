//
//  PortfolioImpactViewController.swift
//  Fintech
//
//  Created by broadstone on 05/08/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import UIKit

enum UseCaseType {
    case UseCase1
    case UseCase2
    case UseCase3
}

class PortfolioImpactViewController: PortfolioParent {
    
    struct TableViewData {
        var opened: Bool
        var title: String
        var desc: String
        var data: Any
        var cardColor: UIColor?
    }
    
    var tableViewDataArray = [TableViewData]()
    
    var plaidToken = ""
    
    var useCaseType: UseCaseType = .UseCase1
    var isViewingPreMadePortfolios = false
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var dialogView: UIView!
    @IBOutlet weak var blurView: UIVisualEffectView!
    @IBOutlet weak var copyPortfolioView: UIView!
    @IBOutlet weak var copyPortfolioHeightConstraint: NSLayoutConstraint!

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        
        self.portfolioSavedDalegate = self
        
        if isViewingPreMadePortfolios {
            copyPortfolioView.isHidden = false
            copyPortfolioHeightConstraint.constant = 55
        }else {
            copyPortfolioView.isHidden = true
            copyPortfolioHeightConstraint.constant = 0
        }
        
        let closeDialogGesture = UITapGestureRecognizer.init(target: self, action: #selector(closeBtnTapped))
        blurView.isUserInteractionEnabled = true
        blurView.addGestureRecognizer(closeDialogGesture)
        
        if useCaseType == .UseCase1 {
            if self.portfolioImpactModel == nil {
                callCalculateExternalPortfolioAPI(params: getParams())                
            }else {
                if self.portfolioImpactModel.data.portfolioInfo.portfolioName == "" {
                    createRightButton(title: getLocalizedString(key: "_save"), action: #selector(savePortfolio))
                }else {
                    populateTableViewData()
                }
            }
            
        }else if useCaseType == .UseCase3 && self.portfolioImpactModel.data.portfolioInfo.portfolioName == "" {
            createRightButton(title: getLocalizedString(key: "_save"), action: #selector(savePortfolio))
            populateTableViewData()
        } else {
            populateTableViewData()
        }
        
        
        self.navigationController?.viewControllers.removeAll(where: {(vc) -> Bool in
            let name = NSStringFromClass(vc.classForCoder)
            print(name)
            if name.contains("PlaidLinkViewController") ||
                vc.isKind(of: ExternalPortfolioVC.self) ||
                vc.isKind(of: AddAndSearchSymbolVC.self) ||
                vc.isKind(of: ViewAssetVC.self) ||
                vc.isKind(of: AssetAllocationVC.self) ||
                vc.isKind(of: PortfolioValuesTemplateVC.self) ||
                vc.isKind(of: PortfolioRiskVC.self) ||
                vc.isKind(of: ValueMappedTickersVC.self) {
                    return true
            }else {
                return false
            }
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        if self.portfolioImpactModel == nil || self.portfolioImpactModel.data.portfolioInfo.portfolioName == "" {
            setNavBarTitile(title: "Back", topItem: getLocalizedString(key: "portfolio_impact"))
        }else {
            setNavBarTitile(title: "Back", topItem: self.portfolioImpactModel.data.portfolioInfo.portfolioName)
        }
        
    }
    
    @IBAction func copyPortfolioBtnPressed(_ sender: UIButton) {
        callCopyPortfolioAPI()
    }
    
    @objc func savePortfolio(){
        let alert = UIAlertController(title: getLocalizedString(key: "portfolio_name"), message: getLocalizedString(key: "enter_portfolio_name"), preferredStyle: .alert)

        //2. Add the text field. You can configure it however you need.
        alert.addTextField { (textField) in
            textField.placeholder = getLocalizedString(key: "portfolio_name")
        }

        // 3. Grab the value from the text field, and print it when the user clicks OK.
        alert.addAction(UIAlertAction(title: getLocalizedString(key: "ok"), style: .default, handler: { [weak alert] (_) in
            let textField = alert?.textFields![0] // Force unwrapping because we know it exists.
            let text = textField?.text ?? ""
            
            if !text.isEmpty {
                self.dismiss(animated: true, completion: nil)
                
            }
            
            self.generatePortfolioAPI(url: ApiUrl.INTERNAL_PORTFOLIO_IMPACT, params: self.savePortfolioApiParams(portfolioName: text), useCaseType: self.useCaseType, for: false)
        }))
        
        alert.addAction(UIAlertAction(title: getLocalizedString(key: "cancel_btn"), style: .cancel, handler: { (action) in
            self.dismiss(animated: true, completion: nil)
        }))

        // 4. Present the alert.
        self.present(alert, animated: true, completion: nil)
    }
    
    private func populateTableViewData() {
        tableViewDataArray.removeAll()
        
        if portfolioImpactModel == nil {
            print("No data in model")
            return
        }
        
        tableViewDataArray.append(TableViewData.init(opened: false, title: getLocalizedString(key: "_assets"), desc: "", data: 0))
        for i in 0..<portfolioImpactModel.data.assets.count {
            tableViewDataArray.append(TableViewData.init(opened: false, title: "", desc: "", data: portfolioImpactModel.data.assets[i]))
        }
        tableViewDataArray.append(TableViewData(opened: false, title: getLocalizedString(key: "past_performance", comment: ""), desc: "", data: 1))
        
        let controversy = portfolioImpactModel.data.controversyData.count
        
        if controversy == 0 {
            tableViewDataArray.append(TableViewData(opened: false, title: getLocalizedString(key: "_controversy"), desc: getLocalizedString(key: "no_significant_controversy"), data: "", cardColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)))
        }else {
            tableViewDataArray.append(TableViewData(opened: false, title: getLocalizedString(key: "_controversy", comment: ""), desc: String(controversy), data: portfolioImpactModel.data.controversyData, cardColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)))
        }
        
        tableViewDataArray.append(TableViewData(opened: false, title: getLocalizedString(key: "portfolio_impact"), desc: "", data: portfolioImpactModel.data.overallESG))
        tableViewDataArray.append(TableViewData(opened: false, title: getLocalizedString(key: "env_score", comment: ""), desc: getLocalizedString(key: "env_desc"), data: portfolioImpactModel.data.overallESG.environment, cardColor: #colorLiteral(red: 0.2745098039, green: 0.4862745098, blue: 0.1411764706, alpha: 1)))
        tableViewDataArray.append(TableViewData(opened: false, title: getLocalizedString(key: "social_score"), desc: getLocalizedString(key: "social_desc"), data: portfolioImpactModel.data.overallESG.social, cardColor: #colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1)))
        tableViewDataArray.append(TableViewData(opened: false, title: getLocalizedString(key: "governance_score"), desc: getLocalizedString(key: "governance_desc"), data: portfolioImpactModel.data.overallESG.governance, cardColor: #colorLiteral(red: 0.5058823824, green: 0.3372549117, blue: 0.06666667014, alpha: 1)))
        tableViewDataArray.append(TableViewData(opened: false, title: getLocalizedString(key: "esg_subfactors", comment: ""), desc: "", data: 1, cardColor: #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1)))
        tableViewDataArray.append(TableViewData(opened: false, title: getLocalizedString(key: "how_i_improve"), desc: "", data: 1))
        tableViewDataArray.append(TableViewData(opened: false, title: getLocalizedString(key: "invest_in_charities"), desc: "", data: 1))
        tableViewDataArray.append(TableViewData(opened: false, title: getLocalizedString(key: "education_link_portfolio"), desc: "", data: 1))
        self.tableView.reloadData()
    }
    
    private func callCalculateExternalPortfolioAPI(params: [String: AnyObject]){
        self.view.activityStartAnimating()
        let url = ApiUrl.EXTERNAL_PORTFOLIO_IMPACT
        print(params)
        
        APIManager.sharedInstance.postRequest(serviceName: url, sendData: params, success: { (response) in
            print(response)
            
            if APIManager.sharedInstance.isSuccess {
                DispatchQueue.main.async {
                    self.createRightButton(title: getLocalizedString(key: "_save"), action: #selector(self.savePortfolio))
                    self.fetchExternalPortfolioResponse(res: response)
                    self.populateTableViewData()
                }
            }else{
                
                DispatchQueue.main.async { [self] in
                    if response["message"] != nil {
                        self.showAlert(response["message"] as! String)
                    } else {
                        self.showAlert(ConstantStrings.NO_RESULT_FOUND_ALERT_TEXT)
                    }
                }
            }
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
        }, failure: { (data) in
            print(data)
            DispatchQueue.main.async { [self] in
                view.activityStopAnimating()
            }
            if APIManager.sharedInstance.error400 {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
            }
            if(APIManager.sharedInstance.error401)
            {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
            }else{
                self.showAlert(ConstantStrings.Error_msg_generic)
            }
        })
    }
    
    private func getParams() -> [String: AnyObject] {
        return
            ["namespace": SPManager.sharedInstance.getCurrentUserNamespace()] as [String: AnyObject]
    }
    
    private func showControversyDialog(model: ControversyModel){
        let dialog = ControversyDialogView.init(frame: dialogView.bounds)
        dialog.controversyModel = model
        dialog.setValues()
        
        dialogView.addSubview(dialog)
        dialogView.isHidden = false
        blurView.isHidden = false
    }
    
    @objc private func closeBtnTapped(){
        dialogView.isHidden = true
        blurView.isHidden = true
        for view in self.dialogView.subviews {
            view.removeFromSuperview()
        }
    }
    
    
    
    
}
extension PortfolioImpactViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return tableViewDataArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableViewDataArray[section].opened == false{
            return 1
        }else {
            return 1
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AssetsHeadingCell") as! AssetsHeadingCell
            
            let tableViewData = tableViewDataArray[indexPath.section]
            cell.headingLabel.text = tableViewData.title
            
            return cell
        }
        else if indexPath.section > 0 && indexPath.section <= portfolioImpactModel.data.assets.count {
            let tableViewData = tableViewDataArray[indexPath.section]
            let data = tableViewData.data as! PortfolioCreationAssets
            
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "AssetSymbolCell") as! AssetSymbolCell
                cell.symbolLabel.text = data.ticker
                cell.nameLabel.text = data.symbolName
                cell.allocationLabel.text = FintechUtils.sharedInstance.doubleToPercentageOutput(doubleVal: data.allocation) + "%"
                return cell
            }else if indexPath.row == 1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "AssetMainHeadingCell") as! InvestmentValuesNewMainHeadingCell
                
                cell.headingLabel.text = "Symbol impact"
                cell.descLabel.text = "Symbol impact on ESG"
//                cell.scoreLabel.text = String(data.portfolioComposition.overallPortfolioScore) + "/" + String(data.portfolioComposition.maxPortfolioScore)
                
                return cell
            }else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "AssetSubHeadingCell") as! InvestmentValuesNewSubHeadingCell
                cell.parentView.backgroundColor = UIColor(named: "ExpandedCellColor")
                var esgData: PortfolioCreationComposition
                if indexPath.row == 2 {
                    esgData = data.portfolioComposition.environment
                    cell.headingLabel.text = getLocalizedString(key: "env")
                    //cell.descLabel.text = getLocalizedString(key: "asset_env_desc")
                    cell.headingLabel.textColor = #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1)
                }else if indexPath.row == 3 {
                    esgData = data.portfolioComposition.social
                    cell.headingLabel.text = getLocalizedString(key: "soc")
                    //cell.descLabel.text = getLocalizedString(key: "asset_social_desc")
                    cell.headingLabel.textColor = #colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1)
                }else {
                    esgData = data.portfolioComposition.governance
                    cell.headingLabel.text = getLocalizedString(key: "gov")
                    //cell.descLabel.text = getLocalizedString(key: "asset_governance_desc")
                    cell.headingLabel.textColor = #colorLiteral(red: 0.5058823824, green: 0.3372549117, blue: 0.06666667014, alpha: 1)
                }
                
//                cell.scoreLabel.text =  String(esgData.score) + "/" + String(esgData.maxScore) + "\n"
//                                        String(esgData.percentile) + " percentile"
                    
                
                return cell
                
            }
        } else if indexPath.section <= portfolioImpactModel.data.assets.count + 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ScoreImprovementCell") as! ScoreImprovementCell
            
            let rowData = tableViewDataArray[indexPath.section]
            
            cell.headingCell.text = rowData.title
            
            return cell
        }
        else if indexPath.section <= portfolioImpactModel.data.assets.count + 1 + 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ControversyCell") as! ControversyCell
            
            cell.controversyLabel.text = tableViewDataArray[indexPath.section].title
            cell.controversyScore.text = tableViewDataArray[indexPath.section].desc
            
            let data = tableViewDataArray[indexPath.section].data as? ControversyData
            if data == nil {
                cell.ellipses.isHidden = true
                cell.ellipses.heightConstaint?.constant = 0
            }else {
                cell.ellipses.isHidden = false
                cell.ellipses.heightConstaint?.constant = 20
            }
            
            return cell
            
        } else if indexPath.section <= portfolioImpactModel.data.assets.count + 1 + 1 + 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "UserInvestmentMainHeadingCell") as! InvestmentValuesNewMainHeadingCell
            
            let rowData = tableViewDataArray[indexPath.section]
            
            let data = rowData.data as! PortfolioCreationESG
            
            cell.headingLabel.text = rowData.title
            //cell.descLabel.text = rowData.desc
            //cell.scoreLabel.text = data.impact
            cell.scoreLabel.text = FintechUtils.sharedInstance.getImpactForPortfolio(data.zScore)
            
            return cell
        }
        else if indexPath.section <= portfolioImpactModel.data.assets.count + 1 + 1 + 1 + 1
        || indexPath.section <= portfolioImpactModel.data.assets.count + 1 + 1 + 1 + 1 + 1
        || indexPath.section <= portfolioImpactModel.data.assets.count + 1 + 1 + 1 + 1 + 1 + 1 {
            let rowData = tableViewDataArray[indexPath.section]
            let data = rowData.data as! PortfolioCreationComposition
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "UserInvestmentSubHeadingCell") as! InvestmentValuesNewSubHeadingCell
                
                
                
                cell.headingLabel.text = rowData.title
                //cell.descLabel.text = rowData.desc
//                cell.scoreLabel.text = data.impact + "\n" + getLocalizedString(key: "better_than") + " " + data.badPeers
                //cell.scoreLabel.text = data.impact
                cell.scoreLabel.text = FintechUtils.sharedInstance.getImpactForPortfolio(data.zScore)
                cell.parentView.backgroundColor = .white
                cell.headingLabel.textColor = tableViewDataArray[indexPath.section].cardColor
                
                return cell
            }else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "UserValuesCell2") as! UserValuesCell2
                let derivedValue = data.derivedValues[indexPath.row - 1]
                
                cell.headingLbl.text = derivedValue.value
                cell.iconImage.image = FintechUtils.sharedInstance.getInvestmentValuesIcons(investmentValue: derivedValue.value)
                
                if derivedValue.score > 0.0 {
                    cell.impactLbl.text = "Some impact"
                }else {
                    cell.impactLbl.text = "No impact"
                }
                return cell
            }
        }else if indexPath.section == tableView.numberOfSections - 4 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "EsgSubFactorCell") as! ControversyCell
            
            cell.controversyLabel.text = tableViewDataArray[indexPath.section].title
            
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ScoreImprovementCell") as! ScoreImprovementCell
            
            let rowData = tableViewDataArray[indexPath.section]
            
            cell.headingCell.text = rowData.title
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section > 0 && indexPath.section <= portfolioImpactModel.data.assets.count {
            let tableViewData = tableViewDataArray[indexPath.section]
            let data = tableViewData.data as! PortfolioCreationAssets
            getTickerSearchData(tickerName: data.ticker)
        }
        else if indexPath.section == 1 + portfolioImpactModel.data.assets.count {
            getPortfolioBacktestAPI(portfolioId: "")
        }else if indexPath.section == 2 + portfolioImpactModel.data.assets.count {
            let data = tableViewDataArray[indexPath.section].data as? ControversyData
            if data != nil {
                let model = ControversyModel.init(success: true, message: "Data found", data: data!)
                showControversyDialog(model: model)
            }
            
        }
        else if indexPath.section == tableView.numberOfSections - 4 {
            let storyboard = UIStoryboard(name: "Portfolio", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "EsgSubFactorVC") as! EsgSubFactorVC
            vc.envTagsData = parseSubFactorsData(esgType: PortfolioEsgType.ENV)
            vc.socTagsData = parseSubFactorsData(esgType: PortfolioEsgType.SOC)
            vc.govTagsData = parseSubFactorsData(esgType: PortfolioEsgType.GOV)
            vc.tagsLabelColor = .black
            vc.tagsBackgroundColor = .lightGray
            vc.companyName = getLocalizedString(key: "esg_subfactors")
            self.navigationController?.pushViewController(vc, animated: true)
            //self.navigationController?.present(vc, animated: true, completion: nil)
        }
        if indexPath.section == tableView.numberOfSections - 3 {
            let storyboard = UIStoryboard(name: "InvestmentDetails", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ScoreImprovementVC") as! ScoreImprovementVC
            self.present(vc, animated: true, completion: nil)
        }
        else if indexPath.section == tableView.numberOfSections - 2 {
            let storyboard = UIStoryboard(name: "Charity", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "listOfCharitiesVC") as! listOfCharitiesVC
            self.navigationController?.pushViewController(vc, animated: true)
        }else if indexPath.section == tableView.numberOfSections - 1 {
            let resoucesStoryboard = UIStoryboard(name: "Resources", bundle: nil)
            let vc = resoucesStoryboard.instantiateViewController(withIdentifier: "ResourcesVC") as! ResourcesVC
            self.navigationController?.pushViewController(vc, animated: true)
        }else {
            if indexPath.row == 0 {
                if tableViewDataArray[indexPath.section].opened{
                    tableViewDataArray[indexPath.section].opened = false
                }else {
                    tableViewDataArray[indexPath.section].opened = true
                }
                let sections = IndexSet.init(integer: indexPath.section)
                tableView.reloadSections(sections, with: .none)
            }
        }
        
        
    }
    
    private func parseSubFactorsData(esgType: String) -> [SwiftTagData]{
        var tagsData = [SwiftTagData]()
        
        if esgType == PortfolioEsgType.ENV {
            for value in portfolioImpactModel.data.overallESG.environment.derivedValues {
                tagsData.append(SwiftTagData.init(label: value.value, tagImage: FintechUtils.sharedInstance.getInvestmentValuesIcons(investmentValue: value.value), value: value.score))
            }
        }else if esgType == PortfolioEsgType.SOC {
            for value in portfolioImpactModel.data.overallESG.social.derivedValues {
                tagsData.append(SwiftTagData.init(label: value.value, tagImage: FintechUtils.sharedInstance.getInvestmentValuesIcons(investmentValue: value.value), value: value.score))
            }
        }else {
            for value in portfolioImpactModel.data.overallESG.governance.derivedValues {
                tagsData.append(SwiftTagData.init(label: value.value, tagImage: FintechUtils.sharedInstance.getInvestmentValuesIcons(investmentValue: value.value), value: value.score))
            }
        }
        
        
        
        return tagsData
    }
}
extension PortfolioImpactViewController {
    //Mark:- Serach API
    func  getTickerSearchData(tickerName: String){
        self.view.activityStartAnimating()
        let url = ApiUrl.GET_TICKER_SEARCH_DATA_NEW
        let namespace = SPManager.sharedInstance.getCurrentUserNamespace()
        //let namespace = "kh01-sdRt"
        
        let params:Dictionary<String,AnyObject> =
            ["namespace" : namespace  as AnyObject,
             "symbol_name": tickerName as AnyObject,
             "is_exact_match": true as AnyObject]
        print(params)
        
        APIManager.sharedInstance.postRequest(serviceName: url, sendData: params, success: { (response) in
            print(response)
            
            if APIManager.sharedInstance.isSuccess {
                let res = response["message"] as! [AnyObject]
                DispatchQueue.main.async {
                    self.fetchSearchData(items: res)
                }
            }else{
                
                DispatchQueue.main.async { [self] in
                    if response["message"] != nil {
                        self.showAlert(response["message"] as! String)
                    } else {
                        self.showAlert(ConstantStrings.NO_RESULT_FOUND_ALERT_TEXT)
                    }
                }
            }
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
        }, failure: { (data) in
            print(data)
            DispatchQueue.main.async { [self] in
                view.activityStopAnimating()
            }
            if APIManager.sharedInstance.error400 {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
            }
            if(APIManager.sharedInstance.error401)
            {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
            }else{
                self.showAlert(ConstantStrings.Error_msg_generic)
            }
        })
        
    }
    private func fetchSearchData(items: [AnyObject]){
    
        
        if items.count > 0 {
            let response = items[0] as! [String: AnyObject]
            
            let symbolName = response["Symbol_name"] as! String
            let industry = response["industry"] as! String
            let ticker = (response["ticker"] as? String) ?? ""
            
            let portfolioComposition = response["portfolio_composition"] as! [String: AnyObject]
            
            
            let controversy = portfolioComposition["Controversy"] as! Int
            let overallImpact = portfolioComposition["overall_impact"] as! String
            let overallBetterPeers = portfolioComposition["overall_better_peers"] as! String
            let overallBadPeers = portfolioComposition["overall_bad_peers"] as! String
            let zScore = portfolioComposition["z_score"] as! Double
            
            let envioromentData = fetchEsgValuesForSearch(portfolioComposition: portfolioComposition, key: "environment")
            let socialData = fetchEsgValuesForSearch(portfolioComposition: portfolioComposition, key: "social")
            let governanceData = fetchEsgValuesForSearch(portfolioComposition: portfolioComposition, key: "governance")
            
            let overallData = OverallNewEsgModel.init(controversy: controversy, impact: overallImpact, betterPeers: overallBetterPeers, badPeers: overallBadPeers, zScore: zScore)
            
            let searchData = SearchData(symbolName: symbolName, industry: industry, portfolioComposition: overallData, envData: envioromentData, socialData: socialData, governanceData: governanceData, symbol: ticker)
            
            let vc = UIStoryboard.init(name: "InvestmentDetails", bundle: Bundle.main).instantiateViewController(withIdentifier: "SearchResultsNewVC") as? SearchResultNewVC
            vc?.searchData = searchData
            //self.navigationController?.present(vc!, animated: true, completion: nil)
            self.navigationController?.pushViewController(vc!, animated: true)
        }else {
            self.view.makeToast(getLocalizedString(key: "no_result_found"))
        }
        
    
    }
    
    private func fetchEsgValuesForSearch(portfolioComposition: [String: AnyObject], key: String) -> NewEsgScoreModel{
        let esg = portfolioComposition[key] as! [String: AnyObject]
        let impact = esg["impact"] as! String
        let betterPeers = esg["better_peers"] as! String
        let badPeers = esg["bad_peers"] as! String
        let zScore = esg["z_score"] as! Double
        let derivedValues = esg["derived_values"] as! [AnyObject]
        
        var derivedValuesModel = [EsgDerivedValues]()
        for i in 0..<derivedValues.count {
            let derivedValue = derivedValues[i] as! [String: AnyObject]
            let name = derivedValue["name"] as! String
            let derivedScore = derivedValue["score"] as! Double
            
            derivedValuesModel.append(EsgDerivedValues.init(valueName: name, score: derivedScore))
        }
        
        let esgData = NewEsgScoreModel.init(impact: impact, betterPeers: betterPeers, badPeers: badPeers, zScore: zScore, derivedValues: derivedValuesModel)
        return esgData
        
    }
}
//MARK:- Backtest API
private extension PortfolioImpactViewController {
    func  getPortfolioBacktestAPI(portfolioId: String){
        self.view.activityStartAnimating()
        let url = ApiUrl.PORTFOLIO_BACKTEST_API
        
        let params = getBacktestApiParams()
        print(params)
        
        APIManager.sharedInstance.postRequest(serviceName: url, sendData: params, success: { (response) in
            print(response)
            
            if APIManager.sharedInstance.isSuccess {
                DispatchQueue.main.async {
                    let model = self.fetchBacktestData(res: response)
                    self.pushPortfolioBacktestVC(backtestModel: model)
                }
            }else{
                
                DispatchQueue.main.async { [self] in
                    if response["message"] != nil {
                        self.showAlert(response["message"] as! String)
                    } else {
                        self.showAlert(ConstantStrings.NO_RESULT_FOUND_ALERT_TEXT)
                    }
                }
            }
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
        }, failure: { (data) in
            print(data)
            DispatchQueue.main.async { [self] in
                view.activityStopAnimating()
            }
            if APIManager.sharedInstance.error400 {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
            }
            if(APIManager.sharedInstance.error401)
            {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
            }else{
                self.showAlert(ConstantStrings.Error_msg_generic)
            }
        })
        
    }
    
    func getBacktestApiParams() -> [String: AnyObject] {
        var assets = [AnyObject]()
        for i in portfolioImpactModel.data.assets {
            let asset = ["ticker": i.ticker,
                         "allocation": i.allocation] as [String: AnyObject]
            assets.append(asset as AnyObject)
        }
        
        return ["namespace": SPManager.sharedInstance.getCurrentUserNamespace(),
                "data": assets] as [String: AnyObject]
    }
    
    func fetchBacktestData(res: [String: AnyObject]) -> PortfolioBacktestModel{
        let success = res["success"] as! Bool
        let message = res["message"] as! String
        let data = res["data"] as! [String: AnyObject]
        
        let backtest = data["backtest"] as! [String: AnyObject]
        let amount = backtest["amount"] as! Double
        let cash = backtest["cash"] as! Double
        let startDate = backtest["start_date"] as! Int64
        let endDate = backtest["end_date"] as! Int64
        
        let performance = data["performance"] as! [AnyObject]
        
        var backtestPerformance = [BacktestPerformance]()
        
        for i in 0..<performance.count{
            let item = performance[i] as! [String: AnyObject]
            
            let name = item["portfolio"] as! String
            let portfolioReturn = item["return"] as! Double
            
            backtestPerformance.append(BacktestPerformance.init(portfolioName: name, portfolioReturn: portfolioReturn))
        }
        let risk = data["risk"] as! [String: AnyObject]
        let beta = risk["portfolio_beta"] as! Double
        let volatility = risk["portfolio_volatility"] as! Double
        
        
        let portfolioBacktest = PortfolioBacktest(amount: amount, cash: cash, endDate: endDate, startDate: startDate)
        
        let portfolioRisk = PortfolioBacktestRisk(beta: beta, volatility: volatility)
        
        let portfolioBacktestData = PortfolioBacktestData(backtest: portfolioBacktest, performance: backtestPerformance, risk: portfolioRisk)
        
        
        return PortfolioBacktestModel(success: success, message: message, data: portfolioBacktestData)
    }
}
//MARK:- API Calling for save portfolio
extension PortfolioImpactViewController: SavePortfolioDelegate {
    private func savePortfolioApiParams(portfolioName: String) -> [String: AnyObject]{
        var assets = [AnyObject]()
        for i in portfolioImpactModel.data.assets {
            let asset = ["ticker": i.ticker,
                         "allocation": i.allocation] as [String: AnyObject]
            assets.append(asset as AnyObject)
        }
        
        var portfolioType = ""
        if useCaseType == .UseCase1 {
            portfolioType = "broker"
        }else if useCaseType == .UseCase3 {
            portfolioType = "value_based"
        }
        
        let data = ["portfolio_name": portfolioName as AnyObject,
                    "assets": assets as AnyObject,
                    "portfolio_type": portfolioType as AnyObject]
        
        return ["namespace": SPManager.sharedInstance.getCurrentUserNamespace(),
                "data": data] as [String: AnyObject]
    }
    
    
    
    func portfolioSaved(success: Bool) {
        if success {
            if let topItem = self.navigationController?.navigationBar.topItem {
                topItem.rightBarButtonItem = nil
            }
        }
    }
}
