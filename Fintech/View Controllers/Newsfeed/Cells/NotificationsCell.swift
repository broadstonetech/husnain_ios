//
//  NotificationsCell.swift
//  Fintech
//
//  Created by MacBook-Mubashar on 24/07/2019.
//  Copyright © 2019 Broadstone Technologies. All rights reserved.
//

import UIKit

class NotificationsCell: UITableViewCell {

    @IBOutlet var notificationHeading_lbl: UILabel!
    @IBOutlet var notificationDesc_lbl: UILabel!
    @IBOutlet weak var Image_img: UIImageView!
   // @IBOutlet var imgImage: UIImageView!
    @IBOutlet var subHeading_lbl: UILabel!
    @IBOutlet var timestamp_lbl: UILabel!
    @IBOutlet weak var ticker_lbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
