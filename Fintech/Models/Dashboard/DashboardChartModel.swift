// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let dashboardChartModel = try? newJSONDecoder().decode(DashboardChartModel.self, from: jsonData)

import Foundation

// MARK: - DashboardChartModel
struct DashboardChartModel: Codable {
    let success: Bool
    let message: String
    let data: DashboardChartDataClass?
}

// MARK: - DataClass
struct DashboardChartDataClass: Codable {
    let dataReturn: String
    let amount: Double
    let userKeyFace: String
    let chartData: DashboardChartData

    enum CodingKeys: String, CodingKey {
        case dataReturn = "return"
        case amount
        case userKeyFace = "user_key_face"
        case chartData = "chart_data"
    }
}

// MARK: - ChartData
struct DashboardChartData: Codable {
    let lastRefresh: Int64
    let data: [DataPoints]

    enum CodingKeys: String, CodingKey {
        case lastRefresh = "last_refresh"
        case data
    }
}

// MARK: - Datum
struct DataPoints: Codable {
    let xAxis: String
    let yAxis: Double

    enum CodingKeys: String, CodingKey {
        case xAxis = "x_axis"
        case yAxis = "y_axis"
    }
}
