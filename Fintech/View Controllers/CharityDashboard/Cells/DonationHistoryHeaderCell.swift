//
//  DonationHistoryHeaderCell.swift
//  Fintech
//
//  Created by broadstone on 12/01/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import Foundation
import UIKit

class DonationHistoryHeaderCell: UITableViewCell {
    
    @IBOutlet weak var donationDate: UILabel!
    @IBOutlet weak var donationMonth: UILabel!
    @IBOutlet weak var donationTotalAmountLabel: UILabel!
    @IBOutlet weak var dropdownImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
}
