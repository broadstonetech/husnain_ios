//
//  ExpendDetailsTableviewCell.swift
//  vesgo
//
//  Created by Apple on 05/01/2021.
//  Copyright © 2021 Apple. All rights reserved.
//

import UIKit

class ExpendDetailsTableviewCell: UITableViewCell {

    @IBOutlet weak var img_main: UIImageView!
    
    @IBOutlet weak var amount_lbl: UILabel!
    @IBOutlet weak var heading_lbl: UILabel!
    @IBOutlet weak var descripation_lbl: UILabel!
    
    @IBOutlet weak var merchantNameLbl: UILabel!
    @IBOutlet weak var imgBgView: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
