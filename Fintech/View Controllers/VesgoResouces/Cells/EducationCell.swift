//
//  EducationCell.swift
//  Fintech
//
//  Created by broadstone on 05/01/2022.
//  Copyright © 2022 Broadstone Technologies. All rights reserved.
//

import UIKit

class EducationCell: UICollectionViewCell {
    
    @IBOutlet weak var parentView: UIView!
    @IBOutlet weak var lessonTitle: UILabel!
    @IBOutlet weak var lessonCompletedLabel: UILabel!
    @IBOutlet weak var progressView: UIView!
    @IBOutlet weak var educationImage: UIImageView!
    
    override class func awakeFromNib() {
        super.awakeFromNib()
    }
    
}
