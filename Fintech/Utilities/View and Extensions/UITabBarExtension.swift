//
//  UITabBarExtension.swift
//  Fintech
//
//  Created by broadstone on 26/08/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import Foundation
import UIKit
extension UITabBarItem {
    @IBInspectable
    var localizedKey: String {
        set (key) {
            title = getLocalizedString(key: key)
        }
        get {
            return title!
        }
    }
}
