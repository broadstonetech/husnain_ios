//
//  InvestmentValuesTemplateVC.swift
//  Fintech
//
//  Created by broadstone on 31/05/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import UIKit

protocol TemplateDelgate {
    func getTemplateType(type: ValuesTemplate)
}

class InvestmentValuesTemplateVC: Parent {
    
    var availableTemplates = [
        getLocalizedString(key: "template_catholic"),
        getLocalizedString(key: "template_env_imapct"),
        getLocalizedString(key: "template_islamic"),
        getLocalizedString(key: "template_methodist"),
        getLocalizedString(key: "template_peace"),
        getLocalizedString(key: "template_vegan")
    ]
    
    var selectedTemplateIndex = -1
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var doneBtn: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var swipeLabel: UILabel!
    
    var templateDelegate: TemplateDelgate?

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.isNavigationBarHidden = false
        setNavBarTitile(title: getLocalizedString(key: "investment_values"), topItem: getLocalizedString(key: "investment_values"))
        
        titleLabel.text = getLocalizedString(key: "suggested_by_vesgo")
        swipeLabel.text = getLocalizedString(key: "swipe_left_to_view_details")
        
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    @IBAction func doneBtnPressed(_ sender: UIButton) {
        let selectedTemplate = mapValuesTemplate()
        if selectedTemplate == .NOT_SET {
            self.view.makeToast("Please select the template")
            return
        }else {
            templateDelegate?.getTemplateType(type: selectedTemplate)
        }
        
        self.PopVC()
    }
    

}
extension InvestmentValuesTemplateVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return availableTemplates.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 20
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int)
    {
        view.tintColor = #colorLiteral(red: 0.9411764706, green: 0.9411764706, blue: 0.9411764706, alpha: 1)
        view.backgroundColor = #colorLiteral(red: 0.9411764706, green: 0.9411764706, blue: 0.9411764706, alpha: 1)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ValuesTemplateCell") as! ValuesTemplateCell
        
        cell.templateLabel.text = availableTemplates[indexPath.section]
        cell.cellIndex = indexPath.section
//        cell.clickDelegate = self
        
        if selectedTemplateIndex == indexPath.section {
            cell.parentView.backgroundColor = .systemGreen
            cell.templateLabel.textColor = .white
//            cell.detailBtn.tintColor = .white
        }else {
            cell.parentView.backgroundColor = .white
            cell.templateLabel.textColor = .black
//            cell.detailBtn.tintColor = .black
        }
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedTemplateIndex = indexPath.section
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let detailsAction = UIContextualAction(style: .normal, title:  getLocalizedString(key: "_details"), handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in

            self.detailButtonPressed(cellIndex: indexPath.section)
            success(true)
            
            
        })
        //detailsAction.image = UIImage(sys)
        detailsAction.backgroundColor = #colorLiteral(red: 0.2935393751, green: 0.6106157899, blue: 0.8765475154, alpha: 1)

        return UISwipeActionsConfiguration(actions: [detailsAction])
    }
    
    func detailButtonPressed(cellIndex: Int) {
        //Present template detail screen
        let vc = UIStoryboard(name: "InvestmentStrategy", bundle: nil).instantiateViewController(withIdentifier: "ValuesTemplateDetailsVC") as! ValuesTemplateDetailsVC
        vc.index = cellIndex
        vc.templateName = availableTemplates[cellIndex]
        self.present(vc, animated: true, completion: nil)
    }
    
    func mapValuesTemplate() -> ValuesTemplate {
        if selectedTemplateIndex == 0 {
            return .Catholic
        }
        else if selectedTemplateIndex == 1 {
            return .Environment
        }
        else if selectedTemplateIndex == 2 {
            return .Islamic
        }
        else if selectedTemplateIndex == 3 {
            return .Methodist
        }
        else if selectedTemplateIndex == 4 {
            return .Peace
        }
        else if selectedTemplateIndex == 5 {
            return .Vegan
        }else {
            return .NOT_SET
        }
        
    }
}
