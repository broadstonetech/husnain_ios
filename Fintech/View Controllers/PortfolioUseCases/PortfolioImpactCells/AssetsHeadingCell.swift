//
//  AssetsHeadingCell.swift
//  Fintech
//
//  Created by broadstone on 05/08/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import UIKit

class AssetsHeadingCell: UITableViewCell {
    
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
