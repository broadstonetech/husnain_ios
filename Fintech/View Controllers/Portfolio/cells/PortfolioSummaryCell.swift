//
//  PortfolioSummaryCell.swift
//  Fintech
//
//  Created by MacBook-Mubashar on 23/07/2019.
//  Copyright © 2019 Broadstone Technologies. All rights reserved.
//

import UIKit

class PortfolioSummaryCell: UITableViewCell {

    @IBOutlet weak var amount_lbl: UILabel!
    @IBOutlet weak var ticker_share_lbl: UILabel!
    @IBOutlet weak var ticker_title_lbl: UILabel!
    @IBOutlet weak var ticker_name_lbl: UILabel!
    @IBOutlet weak var ticker_lbl: UILabel!
    @IBOutlet weak var tickerAmount_lbl: UILabel!
    @IBOutlet weak var ticker_value_lbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
