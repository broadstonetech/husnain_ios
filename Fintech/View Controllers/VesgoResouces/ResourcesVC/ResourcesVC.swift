//
//  ResourcesVC.swift
//  Fintech
//
//  Created by broadstone on 05/01/2022.
//  Copyright © 2022 Broadstone Technologies. All rights reserved.
//

import UIKit
import Kingfisher
class ResourcesVC: ResourcesParent {
    
    @IBOutlet weak var advisorsCollectionView: UICollectionView!
    @IBOutlet weak var vesgoSuggestedEduCollectionView: UICollectionView!
    @IBOutlet weak var userExpertiseView: UIScrollView!
    @IBOutlet weak var userEducationView: UIView!
    @IBOutlet weak var advisorView: UIView!
    @IBOutlet weak var notLoginLabel: UILabel!
    
    var vesgoSuggestedLesson = [EducationModel]()
    var othersLesson = [EducationModel]()
    var isViewingVesgoSuggestedLesson = true
    
    private var advisorsModel = [AdvisorsModel]()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.advisorApiDelegate = self
        
        
        advisorsCollectionView.delegate = self
        advisorsCollectionView.dataSource = self
        
        vesgoSuggestedEduCollectionView.delegate = self
        vesgoSuggestedEduCollectionView.dataSource = self
        
//        othersEduCollectionView.delegate = self
//        othersEduCollectionView.dataSource = self

        setupAdvisorsCollectionViewSize()
        setupSelectedEduCollectionViewSize()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
        self.navigationController?.isNavigationBarHidden = false
        setNavBarTitile(title: "Back", topItem: getLocalizedString(key: "_resources"))
        
        if SPManager.sharedInstance.saveUserLoginStatus() {
            //createMessagesButton()
            getAdvisors(url: ApiUrl.Top_Advisor)
            notLoginLabel.isHidden = true
        }else {
            notLoginLabel.isHidden = false
        }
        
        calculateUserPlan()
        
        
        
    }
    
    func createMessagesButton(){
        let rightButton = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 25))

        rightButton.setBackgroundImage(UIImage(named: "ic_message")!, for: .normal)
        rightButton.addTarget(self, action: #selector(messagesBtnPressed), for: .touchUpInside)

        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: rightButton)
    }
    
    @IBAction func messagesBtnPressed(_ sender: UIButton){
        if !SPManager.sharedInstance.saveUserLoginStatus() {
            self.view.makeToast("Login to view recent chats")
            return
        }
        self.tabBarController?.tabBar.isHidden = true
        let vc = storyboard?.instantiateViewController(withIdentifier: "RecentChatsVC") as! RecentChatsVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func viewAllAdvisorsBtnPressed(_ sender: UIButton) {
        if !SPManager.sharedInstance.saveUserLoginStatus() {
            self.view.makeToast(getLocalizedString(key: "join_now_to_view_advisors"))
            return
        }
        self.tabBarController?.tabBar.isHidden = true
        let vc = storyboard?.instantiateViewController(withIdentifier: "FinancialAdvisorsVC") as! FinancialAdvisorsVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func myProfile_pressed(_ sender: Any) {
        if SPManager.sharedInstance.saveUserLoginStatus() {
            self.tabBarController?.tabBar.isHidden = true
            let storyboard = UIStoryboard(name: "EducationProfile", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "myProfile_view") as! myProfile_view
            
            navigationController?.pushViewController(vc, animated: true)
        }else{
            let toast = getLocalizedString(key: "Join now for a custom profile", comment: "")
            self.view.makeToast(toast)

        }
    }
    
    @IBAction func newsfeedBtnPressed(_ sender: UIButton){
        self.tabBarController?.tabBar.isHidden = true
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "NotificationsVC") as! NotificationsVC
        self.navigationController?.pushViewController(vc, animated: true)
        setAnalyticsAction(ScreenName: "EducationalScreenVc", method: "NewsFeed")
    }
    
    private func setupAdvisorsCollectionViewSize(){

        let width = (advisorsCollectionView.frame.width)/2.1
        let height = advisorsCollectionView.frame.height
        let layout = advisorsCollectionView.collectionViewLayout as! UICollectionViewFlowLayout
        
        layout.itemSize = CGSize(width: width, height: height)
        layout.prepare()
        layout.invalidateLayout()
    }
    
    private func setupSelectedEduCollectionViewSize(){
        let width = (vesgoSuggestedEduCollectionView.frame.width)/2.1
        let height = vesgoSuggestedEduCollectionView.frame.height
        let layout = vesgoSuggestedEduCollectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.itemSize = CGSize(width: width, height: height)
        layout.prepare()
        layout.invalidateLayout()
    }
    
    

}
extension ResourcesVC: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == advisorsCollectionView {
            return advisorsModel.count
        }else if collectionView == vesgoSuggestedEduCollectionView {
            print("vesgoSuggestedEduCollectionView")
            if isViewingVesgoSuggestedLesson {
                return vesgoSuggestedLesson.count
            }else {
                return othersLesson.count
            }
            //return vesgoSuggestedLesson.count
        }
        else {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == advisorsCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AdvisorsCell", for: indexPath) as! AdvisorsCell
            cell.nameLabel.text = advisorsModel[indexPath.row].advisorName
            cell.descriptionLabel.text = advisorsModel[indexPath.row].advisorDescription
            cell.profilePic.kf.setImage(with: URL(string: advisorsModel[indexPath.row].advisorProfilePic), placeholder: UIImage(named: "placeholder-images-image_large"))
            return cell
        }else if collectionView == vesgoSuggestedEduCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "VesgoSuggestedEduCell", for: indexPath) as! EducationCell
            var data: EducationModel
            if isViewingVesgoSuggestedLesson {
                data = vesgoSuggestedLesson[indexPath.row]
            }else {
                data = othersLesson[indexPath.row]
            }
            cell.progressView.isHidden = true
            cell.parentView.backgroundColor = UIColor.init(named: data.bgColor)
            cell.lessonTitle.text = data.mainHeadingText
            cell.educationImage.image = UIImage.init(named: data.image)
            return cell
        }else {
            return UICollectionViewCell()
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == advisorsCollectionView {
            goToAdvisorProfile(advisorModel: advisorsModel[indexPath.row])
            
        }else if collectionView == vesgoSuggestedEduCollectionView {
            
            if isViewingVesgoSuggestedLesson {
                if indexPath.row == vesgoSuggestedLesson.count - 1 {
                    isViewingVesgoSuggestedLesson = false
                    DispatchQueue.main.async {
                        self.vesgoSuggestedEduCollectionView.reloadData()
                        self.vesgoSuggestedEduCollectionView.scrollToItem(at: IndexPath.init(row: 1, section: 0), at: .right, animated: true)
                    }
                }else {
                    goToEducationLesson(index: indexPath.row)
                }
            }else {
                if indexPath.row == 0 {
                    isViewingVesgoSuggestedLesson = true
                    DispatchQueue.main.async {
                        self.vesgoSuggestedEduCollectionView.reloadData()
                    }
                }else {
                    goToEducationLesson(index: indexPath.row)
                }
            }
            
        }
    }
    
    private func goToEducationLesson(index: Int) {
        self.tabBarController?.tabBar.isHidden = true
        var data: EducationModel
        if isViewingVesgoSuggestedLesson {
            data = vesgoSuggestedLesson[index]
        }else {
            data = othersLesson[index]
        }
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "EduSectionDetailsVC") as! EduSectionDetailsVC
        vc.additionalInfo = data.id
        vc.titleString = data.mainHeadingText
        navigationController?.pushViewController(vc, animated: true)
    }
}
extension ResourcesVC: AdvisorApiDelegate {
    func advisorApiCalled(isSuccessful: Bool, advisor: [AdvisorsModel]) {
        if isSuccessful {
            advisorsModel = advisor
            DispatchQueue.main.async {
                self.advisorsCollectionView.reloadData()
            }
        }
    }
}
