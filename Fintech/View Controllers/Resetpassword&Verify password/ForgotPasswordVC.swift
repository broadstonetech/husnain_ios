//
//  ForgotPasswordVC.swift
//  Fintech
//
//  Created by MacBook-Mubashar on 26/08/2019.
//  Copyright © 2019 Broadstone Technologies. All rights reserved.
//

import UIKit
import Lottie
class ForgotPasswordVC: Parent {
    
    @IBOutlet weak var animation_img_view: AnimationView!
    @IBOutlet var description_lbl: UILabel!
    @IBOutlet var username_tf: UITextField!
    @IBOutlet var submit_btn: UIButton!
    
    @IBOutlet weak var heading_des_lbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        animation_img_view.contentMode = .scaleAspectFit
        animation_img_view.loopMode = .loop
        
        animation_img_view.animationSpeed = 0.5
        animation_img_view.play()
        
        username_tf.delegate = self
        submit_btn.addTarget(self, action: #selector(callForgotPassword), for: UIControl.Event.touchUpInside)
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        localization()
    }
    func localization(){
        submit_btn.setTitle(getLocalizedString(key: "Continue", comment: ""), for: .normal)
        let heading = getLocalizedString(key: "FORGOT PASSWORD", comment: "")
        let descripation = getLocalizedString(key: "forgotpasswordviewDes", comment: "")
        
        description_lbl.text = heading
        heading_des_lbl.text = descripation
        username_tf.placeholder = getLocalizedString(key: "enter_username_or_email", comment: "")
        
    }
    
    //MARK: - Controlling the Keyboards
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == username_tf {
            username_tf.resignFirstResponder()
        } else{
            textField.resignFirstResponder()
            
        }
        return true
    }
    
    
    // MARK: - IB Actions
    
    @IBAction func back_btn_pressed(_ sender: Any) {
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "LogInVC") as!LogInVC
        vc.modalPresentationStyle = .fullScreen
        vc.modalTransitionStyle = .partialCurl
        UIApplication.shared.keyWindow?.rootViewController = vc
    }
    
    
    
    @objc func callForgotPassword() {
        if checkFormInputs() {
            DispatchQueue.main.async {
                
                if NetworkReachability.isConnectedToNetwork() {
                    DispatchQueue.main.async {
                        self.callForgotPasswordApi()
                    }
                } else {
                    self.showAlert(ConstantStrings.NO_INTERNET_AVAILABLE)
                }
            }
        }
    }
    // MARK: - Form validations
    func checkFormInputs() -> Bool {
        guard let username = username_tf.text, !username.isEmpty else {
            showAlert(ConstantStrings.username_empty)
            return false
        }
        
        return true
    }
    
    // MARK: - API call--SignIn
    func callForgotPasswordApi() {
        DispatchQueue.main.async {
            self.view.activityStartAnimating()
        }
        let url = ApiUrl.FORGOT_PASSWORD_URL
        let data:Dictionary<String,AnyObject> = ["username": username_tf.text as AnyObject]
        
        
        let params:Dictionary<String,AnyObject> = ["data" : data as AnyObject]
        print(data)
        APIManager.sharedInstance.postRequest(serviceName: url, sendData: params, success: { (response) in
            print(response)
            
            let success = response["success"] as? Bool ?? false
            if success {
                DispatchQueue.main.async {
                    let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                    SPManager.sharedInstance.saveCurrentUserUsername(self.username_tf.text!)
                    let vc = storyBoard.instantiateViewController(withIdentifier: "VerifyPasswordTokenVC") as! VerifyPasswordTokenVC
                    vc.descriptionText = ("\(response["message"] as! String) \(response["email"] as! String)")
                    SPManager.sharedInstance.saveCurrentUserNamespace(response["namespace"] as? String ?? "")
                    UIApplication.shared.keyWindow?.rootViewController = vc
                    //self.navigationController?.pushViewController(vc, animated: true)
                    
                    //self.present(vc, animated:true, completion:nil)
                }
                
            }else{
                DispatchQueue.main.async {
                    if (response["message"] != nil) {
                        
                        self.showAlert(response["message"] as! String)
                    }else {
                        self.showAlert(ConstantStrings.Error_msg_generic)
                    }
                }
            }
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
            
        }, failure: { (data) in
            print(data)
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
            if APIManager.sharedInstance.error400 {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
            }
            if(APIManager.sharedInstance.error401)
            {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
            }else{
                
            }
        })
    }
    
    
    
    // MARK: - Touch events
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        username_tf.resignFirstResponder()
        
    }
}
