//
//  ScoreImprovementLinkCell.swift
//  Fintech
//
//  Created by broadstone on 09/04/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import UIKit

class ScoreImprovementLinkCell: UITableViewCell {
    
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var linkLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
