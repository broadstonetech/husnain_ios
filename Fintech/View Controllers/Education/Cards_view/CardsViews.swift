//
//  CardsViews.swift
//  Fintech
//
//  Created by Apple on 10/07/2020.
//  Copyright © 2020 Broadstone Technologies. All rights reserved.
//

import UIKit
import Koloda

class CardsViews: UIViewController {
    //MARK: - Properties
    //MARK: - Properties
    var index = 0
    var doneFlag = Bool()
    var delegate: greencolorDelegate?
    var viewModelData = [CardsDataModel]()
    var stackContainer : StackContainerView!
    var mainQuestion = Int()
    var innnerQuestion = String()
    var lastQuestion = Int()
    //MARK: - Init
    @IBOutlet weak var Question_heading_lbl: UILabel!
    
    @IBOutlet weak var CountQuestion_lbl: UILabel!
    @IBOutlet weak var tempNextbtn:UIButton!
    @IBOutlet weak var tempPrevicebtn:UIButton!
    @IBAction func Next_btn(_ sender: Any) {
    }
    @IBAction func back_btn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)

    }
    var questionLabel = UILabel()
    var counterLabel = UILabel()
    var nextBtn = UIButton()
    var prevBtn = UIButton()
    let db = Databasehandler()

    var questionText = ""
    var qId = ""
    var data = [LessonAnswerModel]()
    override func loadView() {
        view = UIView()
        view.backgroundColor = #colorLiteral(red: 0.3098039216, green: 0.6156862745, blue: 0.8666666667, alpha: 1)
        
        stackContainer = StackContainerView()
        view.addSubview(stackContainer)
        configureStackContainer()
        stackContainer.translatesAutoresizingMaskIntoConstraints = false
        setViews()
        //loadCardData()
    }
    func loadCardData(){
        viewModelData = []
        data = db.readLessonAnswer(id: qId)
        DispatchQueue.main.async {
            for item in self.data{
                self.viewModelData.append(CardsDataModel(text: item.lesson))
            }
            self.stackContainer.reloadData()
        }
    }
    //MARK: - Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        stackContainer.delegate = self
        title = "Expense Tracker"

        stackContainer.dataSource = self
        loadCardData()
        
        let button1 = UIButton()
        button1.setTitle("", for: .normal)
        let img = UIImage(named: "cross")
        
        button1.setImage(img, for: .normal)
        button1.backgroundColor = .clear
        button1.translatesAutoresizingMaskIntoConstraints = false
        button1.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        
        self.view.addSubview(button1)
        button1.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 20).isActive = true
        button1.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor, constant: -20).isActive = true
        button1.heightAnchor.constraint(equalToConstant: 40).isActive = true
        button1.widthAnchor.constraint(equalToConstant: 40).isActive = true
    }
    
    @objc func buttonAction(sender: UIButton!) {
        print("Button tapped")
        doneFlag = false
        selectedIndex -= 1


    navigationController?.popViewController(animated: true)
        
        
//        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
//        let vc = storyBoard.instantiateViewController(withIdentifier: "EducationalSectionVC") as! EducationalSectionVC
        //self.dismiss(animated: true)

        //UIApplication.shared.keyWindow?.rootViewController = vc
       // navigationController?.pushViewController(vc, animated: true)
    }
    
    
    //MARK: - Configurations
    func configureStackContainer() {
        stackContainer.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        stackContainer.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: -15).isActive = true
        stackContainer.widthAnchor.constraint(equalToConstant: 300).isActive = true
        stackContainer.heightAnchor.constraint(equalToConstant: 390).isActive = true
    }
    func strike(){
    
    }
}

//extension ViewController : SwipeCardsDataSource {
////
////    func numberOfCardsToShow() -> Int {
////        return viewModelData.count
////    }
////
////    func card(at index: Int) -> SwipeCardView {
////        let card = SwipeCardView()
////        card.dataSource = viewModelData[index]
////        return card
////    }
//
//    func emptyView() -> UIView? {
//        return nil
//    }
/*
 // MARK: - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
 // Get the new view controller using segue.destination.
 // Pass the selected object to the new view controller.
 }
 */

//}

//extension CardsViews: KolodaViewDelegate {
//    func kolodaDidRunOutOfCards(_ koloda: KolodaView) {
//        koloda.reloadData()
//    }
//
//    func koloda(_ koloda: KolodaView, didSelectCardAt index: Int) {
//        let alert = UIAlertController(title: "Congratulation!", message: "Now you're \(images[index])", preferredStyle: .alert)
//        alert.addAction(UIAlertAction(title: "OK", style: .default))
//        self.present(alert, animated: true)
//    }
//}
//extension CardsViews: KolodaViewDataSource {
//
//    func kolodaNumberOfCards(_ koloda:KolodaView) -> Int {
//        return images.count
//    }
//
//    func koloda(_ koloda: KolodaView, viewForCardAt index: Int) -> UIView {
//        let view = UIImageView(image: UIImage(named: images[index]))
//        view.layer.cornerRadius = 20
//        view.clipsToBounds = true
//        return view
//    }
//}

extension CardsViews : SwipeCardsDataSource,CheckLastCardDelegate {
    func checkLast() {
        self.delegate?.checkLast(at: index)
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "AwardsViewController") as! AwardsViewController
        vc.mainQuestion = self.mainQuestion
        vc.innnerQuestion = self.innnerQuestion
        vc.lastQuestion = self.lastQuestion
        vc.doneFlag = self.doneFlag
        vc.lessonName = self.questionText
        navigationController?.pushViewController(vc, animated: true)
      


        
       // self.dismiss(animated: true, completion: nil)
    }
    func numberOfCardsToShow() -> Int {
        return viewModelData.count
    }
    
    func card(at index: Int) -> SwipeCardView {
        let card = SwipeCardView()
        card.dataSource = viewModelData[index]
        return card
    }
    
    func emptyView() -> UIView? {
        return nil
    }
    
    
}
extension CardsViews{
    
    func setViews (){
        self.view.addSubview(questionLabel)
        questionLabel.text =  questionText
        questionLabel.font = UIFont(name: questionLabel.font.fontName, size: 25)

        questionLabel.numberOfLines = 0
        // questionLabel.backgroundColor = .black
        questionLabel.textColor = .white
        questionLabel.translatesAutoresizingMaskIntoConstraints = false
        questionLabel.textAlignment = .center
        questionLabel.leadingAnchor.constraint(equalTo: self.view.leadingAnchor,constant: 10).isActive = true
        questionLabel.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -10).isActive = true
        questionLabel.bottomAnchor.constraint(equalTo: self.stackContainer.topAnchor,constant: -20).isActive = true
       // setCountViews()
    }
    func setNextBtn (){
        self.view.addSubview(nextBtn)
        nextBtn.setTitle("Next", for: .normal)
        
        nextBtn.backgroundColor = .lightGray
        nextBtn.tintColor = .white
        //nextBtn.setImage(UIImage(named: ""), for: .normal)
        nextBtn.translatesAutoresizingMaskIntoConstraints = false
        //nextBtn.leadingAnchor.constraint(equalTo: self.view.leadingAnchor,constant: 10).isActive = true
        nextBtn.leadingAnchor.constraint(equalTo: self.counterLabel.trailingAnchor, constant: 0).isActive = true
        nextBtn.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor,constant: -10).isActive = true
        nextBtn.widthAnchor.constraint(equalToConstant: 44).isActive = true
        nextBtn.heightAnchor.constraint(equalToConstant: 44).isActive = true
        nextBtn.layer.cornerRadius = 22
    }
    
    func setPrevBtn (){
        self.view.addSubview(prevBtn)
        prevBtn.setTitle("Prev", for: .normal)
        prevBtn.backgroundColor = .lightGray
        prevBtn.tintColor = .white
        //nextBtn.setImage(UIImage(named: ""), for: .normal)
        prevBtn.translatesAutoresizingMaskIntoConstraints = false
        prevBtn.trailingAnchor.constraint(equalTo: self.counterLabel.leadingAnchor,constant: 0).isActive = true
        prevBtn.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor,constant: -10).isActive = true
        prevBtn.widthAnchor.constraint(equalToConstant: 44).isActive = true
        prevBtn.heightAnchor.constraint(equalToConstant: 44).isActive = true
        prevBtn.layer.cornerRadius = 22
        counterLabel.centerYAnchor.constraint(equalTo: prevBtn.centerYAnchor).isActive = true
    }
    
    func setCountViews (){
        self.view.addSubview(counterLabel)
        counterLabel.text =  "1/20"
        counterLabel.numberOfLines = 3
        // questionLabel.backgroundColor = .black
        counterLabel.textColor = .white
        counterLabel.translatesAutoresizingMaskIntoConstraints = false
        counterLabel.textAlignment = .center
        counterLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        counterLabel.widthAnchor.constraint(equalToConstant: 100).isActive = true
       // setNextBtn()
       // setPrevBtn()
    }
}
protocol greencolorDelegate {
    func checkLast(at index:Int)
    
}
