//
//  AdvisorProfiileVC.swift
//  EducationModuleSP
//
//  Created by SaifUllah Butt on 28/12/2021.
//

import UIKit
import TagListView
import Kingfisher
class AdvisorProfiileVC: ResourcesParent {

    let deviceId = UIDevice.current.identifierForVendor?.uuidString
    
    @IBOutlet weak var advisorProileImage: UIImageView!
    @IBOutlet weak var advisorProfileName: UILabel!
    @IBOutlet weak var advisorProfileDescription: UITextView!
    @IBOutlet weak var chatNowOutlet: UIButton!
    
    @IBOutlet weak var taglistView: TagListView!
    
    @IBOutlet weak var secondCard: UIView!
    @IBOutlet weak var certificationView: UIView!
    @IBOutlet weak var certifNameView: UIView!
    @IBOutlet weak var contactView: UIView!
    @IBOutlet weak var socialLinkView: UIView!
    @IBOutlet weak var advisorCertfications: UILabel!
    @IBOutlet weak var mailIV: UIImageView!
    @IBOutlet weak var twitterIV: UIImageView!
    @IBOutlet weak var linkedInIV: UIImageView!
    
    var advisorData: AdvisorsModel!
    var advisorChatDelegate: ChatWithAdvisorDelegate?
    
//    var advName = ""
//    var AdvExpertise = [String]()
//    var advDescription = ""
//    var advNamespace = ""
//
//    var facebookLink = ""
//    var twitterLink = ""
//    var linkedinLink = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        secondCard.layer.cornerRadius = 5

      //  taglistView.addTags(["Stock", "ETF", "Crypto"])
        taglistView.textFont = UIFont.systemFont(ofSize: 17)
        taglistView.alignment = .center
        taglistView.tagBackgroundColor = UIColor(named: "AppMainColor")!
        
        advisorProileImage.layer.borderWidth = 1.0
        advisorProileImage.layer.masksToBounds = false
        advisorProileImage.layer.borderColor = UIColor.white.cgColor
        advisorProileImage.layer.cornerRadius = advisorProileImage.frame.size.width / 2
        advisorProileImage.clipsToBounds = true
        
        chatNowOutlet.layer.cornerRadius = 10
        
//        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneTapped))
//        navigationItem.rightBarButtonItem?.tintColor = .white
        
        advisorProfileName.text = advisorData.advisorName
        advisorProfileDescription.text = advisorData.advisorDescription
        advisorProileImage.kf.setImage(with: URL(string: advisorData.advisorProfilePic), placeholder: UIImage(named: "caleb-dp"))
        advisorCertfications.text = FintechUtils.sharedInstance.ArrayToString(array: advisorData.advisorCertificates)
        
        mailIV.isUserInteractionEnabled = true
        twitterIV.isUserInteractionEnabled = true
        linkedInIV.isUserInteractionEnabled = true
        mailIV.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(mailBtnPressed)))
        twitterIV.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(twitterBtnPressed)))
        linkedInIV.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(linkedinBtnPressed)))
        
        taglistView.removeAllTags()
        taglistView.addTags(advisorData.advisorExpertise)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        let name = advisorData.advisorName + "'s " + getLocalizedString(key: "Profile")
        setNavBarTitile(title: "Back", topItem: name)
    }
    
    
    @objc func doneTapped() {
       print("navbar")
    }
        
    
    
    @IBAction func chatNowBtn(_ sender: Any) {
        self.advisorChatDelegate?.startChatBtnPressed(advisorModel: advisorData)
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func doneBtn(_ sender: Any) {
    
        self.dismiss(animated: true, completion: nil)
    
    }
    
    @objc func mailBtnPressed() {
        if let url = URL(string: "mailto:\(advisorData.advisorEmail)") {
           UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }

    @objc func twitterBtnPressed() {
        FintechUtils.sharedInstance.openUrl(url: advisorData.advisorSocialLinks.twitter)
    }
    
    @objc func linkedinBtnPressed() {
        FintechUtils.sharedInstance.openUrl(url: advisorData.advisorSocialLinks.linkedIn)
    }
}
