//
//  GetOffersModels.swift
//  Fintech
//
//  Created by broadstone on 11/10/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import Foundation
struct GetOffersModels {
    let success: Bool
    let message: String
    let data: [GetOffersData]
}

struct GetOffersData {
    let desc: String
    let price: Double
    let offerCode: String
    let startDate: Int64
    let endDate: Int64
    let currency: String
    let offerLink: String
}
