//
//  Summary2Cell.swift
//  Fintech
//
//  Created by Apple on 13/05/2020.
//  Copyright © 2020 Broadstone Technologies. All rights reserved.
//

import UIKit

class Summary2Cell: UITableViewCell {
    @IBOutlet weak var Question_lbl: UILabel!
    
    @IBOutlet weak var Answer_lbl: UILabel!
    @IBOutlet weak var Category_lbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
