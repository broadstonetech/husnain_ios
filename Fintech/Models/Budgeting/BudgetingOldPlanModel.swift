// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let budgetingOldPlanModel = try? newJSONDecoder().decode(BudgetingOldPlanModel.self, from: jsonData)

import Foundation

// MARK: - BudgetingOldPlanModel
struct BudgetingOldPlanModel: Codable {
    let success: Bool
    let data: [BudgetinOldPlanDatum]
}

// MARK: - Datum
struct BudgetinOldPlanDatum: Codable {
    let xAxis: Int64
    let yAxisPlanned: Double
    let yAxisSpent: Double

    enum CodingKeys: String, CodingKey {
        case xAxis = "x_axis"
        case yAxisPlanned = "y_axis_planned"
        case yAxisSpent = "y_axis_spent"
    }
}
