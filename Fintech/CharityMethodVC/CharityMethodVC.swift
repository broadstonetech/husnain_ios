//
//  CharityMethodVC.swift
//  Fintech
//
//  Created by Apple on 21/12/2020.
//  Copyright © 2020 Broadstone Technologies. All rights reserved.
//

import UIKit
var donate_text = ""

class CharityMethodVC: PaymentsBaseClass {
    var eraserstatus = false
    var checked = false
    var cheak_Box_Donate = false
    @IBOutlet weak var textfiled: UITextField!
    
    @IBOutlet weak var customized_btn: UIButton!
    
    @IBOutlet weak var main_heading_donation_method: UILabel!
    
    
    @IBOutlet weak var heading_donation_method: UILabel!
    @IBOutlet weak var heading_2_donation_method: UILabel!
    
    @IBOutlet weak var transaction_cost_lbl: UILabel!
    @IBOutlet weak var descripation_2_donation_method_lbl: UILabel!
    @IBOutlet weak var descripation_donation_method: UILabel!
    @IBOutlet weak var continue_btn: UIButton!
    
    @IBOutlet weak var include_transaction_fee_btn: UIButton!
    
    @IBOutlet weak var Customize_distribution_text_lbl: UILabel!
    @IBOutlet weak var img_fixed_amount: UIImageView!
    @IBOutlet weak var img_donate_without_invesment: UIImageView!
    
    @IBOutlet weak var paymentView: paymentOptionView!
    @IBOutlet weak var blurView: UIVisualEffectView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(List_values)
        print(count_LIST_VALUES)
        include_transaction_fee_btn.setImage(UIImage(named:"cheak_box"), for: .normal)
        checked = true
        
        textfiled.delegate = self
        
        createRightButton(title: getLocalizedString(key: "continue_btn"))
        
        blurView.isUserInteractionEnabled = true
        blurView.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(showHidePaymentOptionsView)))
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        self.setNavBarTitile(title: getLocalizedString(key: "Donate_btn"), topItem: getLocalizedString(key: "Donate_btn"))
        localization()
        if count_LIST_VALUES == 1 {
            Customize_distribution_text_lbl.isHidden = true
            customized_btn.isHidden = true
        }else{
            Customize_distribution_text_lbl.isHidden = false
            customized_btn.isHidden = false
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.onPaymentTokenGenerated(_:)), name: NSNotification.Name(rawValue:PaymentObservers.paymentCompleteNotificationKey), object: nil)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func showHidePaymentOptionsView(){
        if paymentView.isHidden {
            paymentView.isHidden = false
            blurView.isHidden = false
        }else{
            paymentView.isHidden = true
            blurView.isHidden = true
        }
    }
    
    func createRightButton(title: String){
        
        let rightButton = UIButton(frame: CGRect(x: 0, y: 0, width: 45, height: 16))
        rightButton.setTitle(title, for: .normal)
        rightButton.addTarget(self, action: #selector(continue_pressed(_:)), for: .touchUpInside)
        //rightButton.titleLabel?.font = UIFont(name: "HelveticaNeue", size: 15)
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: rightButton)
    }
    
    func localization(){
        //setNavBarTitile(title:getLocalizedString(key: "Portfolio", comment: ""), topItem: getLocalizedString(key: "Portfolio", comment: ""))
        ///navigationController?.navigationBar.barStyle = .blackOpaque
        
        
        let main_heading_donation = getLocalizedString(key: "Choose_donation_lbl", comment: "")
        let heading_donation = getLocalizedString(key: "Direct_cash_lbl", comment: "")
        let descripation_donation = getLocalizedString(key: "Donate_through_portfolio_lbl", comment: "")
        let heading_2_donation = getLocalizedString(key: "Invest_charitable_amount_lbl", comment: "")
        let descripation_2_donation = getLocalizedString(key: "Your_charity_contribution_lbl", comment: "")
        let transaction_cost = getLocalizedString(key: "Cover_transaction_cost_lbl", comment: "")
        let Customize_distribution = getLocalizedString(key: "Customize_distribution_text_lbl", comment: "")
        donate_text = getLocalizedString(key: "will_be_donated", comment: "")
        main_heading_donation_method.text = main_heading_donation
        heading_donation_method.text = heading_donation
        descripation_donation_method.text = descripation_donation
        heading_2_donation_method.text = heading_2_donation
        descripation_2_donation_method_lbl.text = descripation_2_donation
        transaction_cost_lbl.text = transaction_cost
        Customize_distribution_text_lbl.text = Customize_distribution
        
        
        
        
        customized_btn.setTitle(getLocalizedString(key: "customiz_btn", comment: ""), for: .normal)
        //continue_btn.setTitle(getLocalizedString(key: "continue_btn", comment: ""), for: .normal)
        
        
        
    }
    
    @IBAction func include_transaction_fee_pressed(_ sender: Any) {
        if (textfiled.text?.isEmpty)!{
            self.view.makeToast("Please enter amount")
            
        }else {
            if checked{
                include_transaction_fee_btn.setImage( UIImage(named:"Uncheak_box"), for: .normal)
                checked = false
            }else {
                include_transaction_fee_btn.setImage(UIImage(named:"cheak_box"), for: .normal)
                checked = true
            }
        }
        
    }
    
    @IBAction func donate_without_invesment_pressed(_ sender: Any) {
        img_donate_without_invesment.image = UIImage(named: "filled")
        img_fixed_amount.image = UIImage(named: "Circle-1")
        cheak_Box_Donate = true
        PaymentAPIUtils.charityPlan = PaymentConstants.CHARITY_METHOD_SOLID
    }
    
    @IBAction func fixed_amount_pressed(_ sender: Any) {
        img_donate_without_invesment.image = UIImage(named: "Circle-1")
        img_fixed_amount.image = UIImage(named: "filled")
        cheak_Box_Donate = true
        PaymentAPIUtils.charityPlan = PaymentConstants.CHARITY_METHOD_INVEST
        
    }
    
    @IBAction func customize_pressed(_ sender: Any) {
        if cheak_Box_Donate{
            print("true")
            
            
            let amountDonated = Double(textfiled.text!)
            if (textfiled.text!.isEmpty  || amountDonated == 0){
                self.view.makeToast("Please enter amount")
            }else{
                PaymentAPIUtils.isEqualDistribution = false
                let storyboard = UIStoryboard(name: "Charity", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "customizedDistributionVC") as! customizedDistributionVC
                vc.amountToBeDonated = amountDonated!
                vc.isFeesIncluded = checked
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }else {
            print("false")
            self.view.makeToast("Please make a selection")
        }
    }
    
    @IBAction func continue_pressed(_ sender: Any) {
        if cheak_Box_Donate{
            print("false")
            let amountDonated = Double(textfiled.text!)
            if (textfiled.text!.isEmpty  || amountDonated == 0){
                self.view.makeToast("Please enter amount")
            }else{
                PaymentAPIUtils.isEqualDistribution = true
                showHidePaymentOptionsView()
                PaymentAPIUtils.amount = amountDonated!
                paymentView.setData(isTransactionFeeIncluded: checked, amount: amountDonated!)
            }
        }else {
            self.view.makeToast("Please select charity method")
            
        }
    }
    
    @objc private func onPaymentTokenGenerated(_ notification: NSNotification){
        print("Charity method class #174")
        showHidePaymentOptionsView()
        if let token = notification.userInfo?["token"] as? String {
            callPostTokenAPI(paymentToken: token, amount: PaymentAPIUtils.amount)
        }
    }
    
    
        func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    
            if textfiled.keyboardType == .decimalPad {
                let regex = try! NSRegularExpression(pattern: "^(^[0-9]{0,7})*([.,][0-9]{0,2})?$", options: .caseInsensitive)
    
                if let newText = (textfiled.text as NSString?)?.replacingCharacters(in: range, with: string) {
                    return regex.firstMatch(in: newText, options: [], range: NSRange(location: 0, length: newText.count)) != nil
    
                } else {
                    return false
                }
            }else {
                return false
            }
    
    
        }
    
}
