//
//  UIViewController.swift
//  Jedi
//
//  Created by MacBook-Mubashar on 14/01/2019.
//  Copyright © 2019 Apple. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    @IBAction func floatyaction(){
        
        /// jska
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "Main_login_page") as! mainpageloninsignup
        vc.modalTransitionStyle = .partialCurl
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    func postManagerUser(method: String, isThisURLEmbdedData: Bool, urlString: String,timeoutInterval:Int = NetworkConstants.REQUEST_TIMEOUT_INTERVAL ,parameter : [String: Any] , success:@escaping ( _ response:Data)->(), failure:@escaping ( _ error:Error)->())
        
    {
        let parameters = parameter
        print("param: ",parameters)
        let value = urlString.replacingOccurrences(of: " ", with: "")
        guard let url = URL(string: value)
            else{
                return
        }
        let config = URLSessionConfiguration.default
        config.httpAdditionalHeaders = [
            "Content-Type" : "application/json"
        ]
        let session = URLSession(configuration: config)
        var request = URLRequest(url: url)
        request.httpMethod = method
        request.timeoutInterval = TimeInterval(timeoutInterval)
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
        } catch let error {
            print(error.localizedDescription)
        }
        session.dataTask(with: request, completionHandler:{(data, response, error)
            in
            DispatchQueue.main.async {
            }
            if let response = response
            {
                print("Response: ",response)
                
            }
            if let error = error
            {
                DispatchQueue.main.async {
                    print(error)
                    failure(error)
                }
            }
            if let data = data
            {
                success(data)
            }
        }).resume()
    }
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle = UIBarStyle.default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(doneButtonAction))
        done.tintColor = .black
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        
    }
    @objc func doneButtonAction()
    {
        
        
    }

}
