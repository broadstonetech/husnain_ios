//
//  DataCell2.swift
//  Vesgo-PF
//
//  Created by Billal on 02/02/2021.
//

import UIKit

class DataCell2: UITableViewCell {

    @IBOutlet weak var detailLbl1: UILabel!
    @IBOutlet weak var tickerPercentage: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
