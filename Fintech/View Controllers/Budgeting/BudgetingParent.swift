//
//  BudgetingParent.swift
//  Fintech
//
//  Created by broadstone on 25/01/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import UIKit
import LinkKit

//protocol LinkOAuthHandling {
//    var linkHandler: Handler? { get }
//    var oauthRedirectUri: URL? { get }
//}

class BudgetingParent: Parent {
    
    var oauthRedirectUri: URL? = { URL(string: "https://vesgo.io/plaid_redirect") }()
    var linkHandler: Handler?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    
    func getPlaidLinkToken(){
        self.view.activityStartAnimating()
        
        let url = ApiUrl.GET_PLAID_LINK_TOKEN
        
        let namespace = SPManager.sharedInstance.getCurrentUserNamespace()
        
        let params: Dictionary<String, AnyObject> =
                        ["namespace": namespace as AnyObject]
        
        print(params)
        APIManager.sharedInstance.postRequest(serviceName: url, sendData: params, success: { (response) in
            print(response)
            
            if APIManager.sharedInstance.isSuccess {
                    
                    DispatchQueue.main.async {
                        let message = response["message"] as? [String: AnyObject]
                        let linkToken = message!["link_token"] as? String ?? ""
                        let configuration = LinkTokenConfiguration(
                            token: linkToken,
                            onSuccess: { [self] linkSuccess in
                                
                                print(linkSuccess.publicToken)
                                print(linkSuccess.metadata.accounts)
                                
                                
                                sendPlaidToken(token: linkSuccess.publicToken)
                            }
                        )
                        
                        let result = Plaid.create(configuration)
                        switch result {
                            case .failure(let error):
                                print("Unable to create Plaid handler due to: \(error)")
                                
                            case .success(let handler):
                                handler.open(presentUsing: .viewController(self))
                                self.linkHandler = handler
                                
                        }
                    }
                    
            } else {
                
            }
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
        }, failure: { (data) in
            DispatchQueue.main.async {
                print("IN FALIURE")
                self.view.activityStopAnimating()
            }
            if APIManager.sharedInstance.error400 {
                DispatchQueue.main.async {
                    self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
                }
            }
            if(APIManager.sharedInstance.error401)
            {
                DispatchQueue.main.async {
                    self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
                }
            }else{
                DispatchQueue.main.async {
                    self.showAlert(ConstantStrings.Error_msg_generic)
                }
            }
        })
    }
    
    
    //Mark:- Send Plaid Token
    func sendPlaidToken(token: String){
        self.view.activityStartAnimating()
        
        let url = ApiUrl.SEND_PLAID_TOKEN
        
        let namespace = SPManager.sharedInstance.getCurrentUserNamespace()
        
        let params: Dictionary<String, AnyObject> =
                        ["namespace": namespace as AnyObject,
                         "public_token": token as AnyObject,
                         "card_type": "credit" as AnyObject,
                         "lastdigits": 4242 as AnyObject]
        
        print(params)
        APIManager.sharedInstance.postRequest(serviceName: url, sendData: params, success: { (response) in
            print(response)
            
            if APIManager.sharedInstance.isSuccess {
                    
                    DispatchQueue.main.async { [self] in
                        view.makeToast(getLocalizedString(key: "account_added"))
                    }
                
                DispatchQueue.main.async {
                    self.navigationController?.viewControllers.removeAll(where: {(vc) -> Bool in
                        let name = NSStringFromClass(vc.classForCoder)
                        print(name)
                        return name.contains("PlaidLinkViewController")
                    })
                }
                    
            } else {
                
            }
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
        }, failure: { (data) in
            DispatchQueue.main.async {
                print("IN FALIURE")
                self.view.activityStopAnimating()
            }
            if APIManager.sharedInstance.error400 {
                DispatchQueue.main.async {
                    self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
                }
            }
            if(APIManager.sharedInstance.error401)
            {
                DispatchQueue.main.async {
                    self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
                }
            }else{
                DispatchQueue.main.async {
                    self.showAlert(ConstantStrings.Error_msg_generic)
                }
            }
        })
    }
}
