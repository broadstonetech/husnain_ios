//
//  language.swift
//  Fintech
//
//  Created by Apple on 03/04/2020.
//  Copyright © 2020 Broadstone Technologies. All rights reserved.
//

import Foundation

struct LanguageCodeType
{
    static let English = "en"
    static let Spanish = "es"
    static let French = "fr"
    static let Chinese = "zh-Hans"
    static let Arabic = "ar"
}


class AppPrefsManager: NSObject
{
    static let sharedInstance = AppPrefsManager()
    
    
    let LANGUAGE_CODE                           =   "LANGUAGE_CODE"
    let LANGUAGE_SELECT                           =   "LANGUAGE_SELECT"
    
    
    
    func setDataToPreference(data: AnyObject, forKey key: String)
    {
        let archivedData = NSKeyedArchiver.archivedData(withRootObject: data)
        UserDefaults.standard.set(archivedData, forKey: key)
        UserDefaults.standard.synchronize()
    }
    
    func getDataFromPreference(key: String) -> AnyObject?
    {
        let archivedData = UserDefaults.standard.object(forKey: key)
        if(archivedData != nil)
        {
            return NSKeyedUnarchiver.unarchiveObject(with: archivedData! as! Data) as AnyObject?
        }
        return nil
    }
    
    func removeDataFromPreference(key: String)
    {
        UserDefaults.standard.removeObject(forKey: key)
        UserDefaults.standard.synchronize()
    }
    
    
    
    //MARK: - User login boolean
    func setIsLanguageSet(isLanguage: Bool)
    {
        setDataToPreference(data: isLanguage as AnyObject, forKey: LANGUAGE_SELECT)
    }
    
    func isLanguageSelcet() -> Bool
    {
        let isUserLogin = getDataFromPreference(key: LANGUAGE_SELECT)
        return isUserLogin == nil ? false:(isUserLogin as! Bool)
    }
    
    
    //MARK: - Language Code
    func saveLanguageCode(languageCode: String)
    {
        setDataToPreference(data: languageCode as AnyObject, forKey: LANGUAGE_CODE)
    }
    
    func getLanguageCode() -> String
    {
        return getDataFromPreference(key: LANGUAGE_CODE) as? String ?? LanguageCodeType.English
    }
    
    
}

func getLocalizedString(key: String, comment: String) -> String
{
    let languageCode = AppPrefsManager.sharedInstance.getLanguageCode()
    
    if let path = Bundle.main.path(forResource: languageCode, ofType: "lproj")
    {
        let bundle = Bundle(path: path)
        return NSLocalizedString(key, tableName: nil, bundle: bundle!, value: "", comment: comment)
    }
    return NSLocalizedString(key, comment: comment)
}

func getLocalizedString(key: String) -> String {
    return getLocalizedString(key: key, comment: "")
}
