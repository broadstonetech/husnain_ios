//
//  ScoreImprovementVcHeadingCell.swift
//  Fintech
//
//  Created by broadstone on 09/04/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import UIKit

class ScoreImprovementVcHeadingCell: UITableViewCell {
    
    @IBOutlet weak var headingLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
