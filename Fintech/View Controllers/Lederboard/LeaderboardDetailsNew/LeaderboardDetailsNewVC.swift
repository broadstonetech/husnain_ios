// LeaderboardDetailsNewVC.swift
//  Fintech
//
//  Created by broadstone on 18/03/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import UIKit
import UserNotifications
class LeaderboardDetailsNewVC: Parent {
    //Stub data
    struct StockAllocationData {
        var symbol: String
        var name: String
        var share: String
        var shareAmount: String
    }
    
    var stocksData = [
        StockAllocationData(symbol: "FB", name: "Facebook Inc", share: "65.0%", shareAmount: "$6500.00"),
        StockAllocationData(symbol: "BGNE", name: "BeiGene, Ltd", share: "35.0%", shareAmount: "$3500.00")
    ]
    
    //End Stub data
    
    
    var userLeaderboardData: LeaderboardUsersList!
    
    struct TableViewData {
        var opened: Bool
        var title: String
        var desc: String
        var data: Any
        var cardColor: UIColor
    }
    
    var tableViewDataArray = [TableViewData]()
    

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var dialogView: UIView!
    @IBOutlet weak var blurView: UIVisualEffectView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        // Do any additional setup after loading the view.
        
        let closeDialogGesture = UITapGestureRecognizer.init(target: self, action: #selector(closeBtnTapped))
        blurView.isUserInteractionEnabled = true
        blurView.addGestureRecognizer(closeDialogGesture)
        
        populateData()
        
    }
    

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        setNavBarTitile(title: "Back", topItem: userLeaderboardData.name)
    }
    
    private func populateData(){
        tableViewDataArray.append(TableViewData(opened: false, title: getLocalizedString(key: "_assets"), desc: getLocalizedString(key: "assets_desc"), data: userLeaderboardData.assets, cardColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)))
        tableViewDataArray.append(TableViewData(opened: false, title: getLocalizedString(key: "risk_tolerance"), desc: String(userLeaderboardData.riskScore), data: 0, cardColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)))
        tableViewDataArray.append(TableViewData(opened: false, title: getLocalizedString(key: "investment_values_lbl", comment: ""), desc: getLocalizedString(key: "value_supported_by_portfolio", comment: ""), data: userLeaderboardData.positiveValues, cardColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)))
        let controversy = userLeaderboardData.portfolioComposition.controversy
        if controversy!.count == 0 {
            tableViewDataArray.append(TableViewData(opened: false, title: getLocalizedString(key: "_controversy"), desc: getLocalizedString(key: "no_significant_controversy"), data: "", cardColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)))
        }else {
            tableViewDataArray.append(TableViewData(opened: false, title: getLocalizedString(key: "_controversy", comment: ""), desc: String(controversy!.count), data: controversy, cardColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)))
        }
        tableViewDataArray.append(TableViewData(opened: false, title: getLocalizedString(key: "portfolio_impact", comment: ""), desc: getLocalizedString(key: "my_impact_desc", comment: ""), data: userLeaderboardData.portfolioComposition, cardColor: #colorLiteral(red: 0.9976013303, green: 0.9241877198, blue: 0.9816413522, alpha: 1)))
        tableViewDataArray.append(TableViewData(opened: false, title: getLocalizedString(key: "env_score", comment: ""), desc: getLocalizedString(key: "env_desc"), data: userLeaderboardData.portfolioComposition.environment, cardColor: #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1)))
        tableViewDataArray.append(TableViewData(opened: false, title: getLocalizedString(key: "social_score"), desc: getLocalizedString(key: "social_desc"), data: userLeaderboardData.portfolioComposition.socialScore, cardColor: #colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1)))
        tableViewDataArray.append(TableViewData(opened: false, title: getLocalizedString(key: "governance_score"), desc: getLocalizedString(key: "governance_desc"), data: userLeaderboardData.portfolioComposition.governanceScore, cardColor: #colorLiteral(red: 0.5058823824, green: 0.3372549117, blue: 0.06666667014, alpha: 1)))
        tableViewDataArray.append(TableViewData(opened: false, title: getLocalizedString(key: "esg_subfactors"), desc: "", data: 1, cardColor: #colorLiteral(red: 1, green: 0.5843137255, blue: 0, alpha: 1)))
        
        tableView.reloadData()
    }
    
    


    @IBAction func copyPortfolio(_ sender: Any) {
        
        if !SPManager.sharedInstance.saveUserLoginStatus(){
            let toast = getLocalizedString(key: "Join now for a custom profile", comment: "")
            self.view.makeToast(toast)
        }else{
            copyPortfolioAPI()
        }
    }

    private func copyPortfolioAPI(){
        DispatchQueue.main.async {
            self.view.activityStartAnimating()
        }
        let namespace = SPManager.sharedInstance.getCurrentUserNamespace()
        let params:Dictionary<String,AnyObject> = [
            "namespace" : namespace as AnyObject,
            "portfolio_id": userLeaderboardData.portfolioId as AnyObject]

        APIManager.sharedInstance.postRequest(serviceName: ApiUrl.COPY_PORTFOLIO_URL, sendData: params,
                                              success: { (response) in
                                                DispatchQueue.main.async {
                                                    self.view.activityStopAnimating()
                                                    let toast = "Request received, we will update the portfolio shortly"
                                                    self.view.makeToast(toast, duration: 7.0)
//                                                    NotificationCenter.default.post(Notification(name: .portfolioCopied))
                                                }
        },
                                              failure: { (data) in
                                                DispatchQueue.main.async {
                                                    self.view.activityStopAnimating()
                                                    let toast = "Unable to copy portfolio, try again later"
                                                    self.view.makeToast(toast)


                                                }
        })
    }
    
    private func showControversyDialog(model: ControversyModel){
        let dialog = ControversyDialogView.init(frame: dialogView.bounds)
        dialog.controversyModel = model
        dialog.setValues()
        
        dialogView.addSubview(dialog)
        dialogView.isHidden = false
        blurView.isHidden = false
    }
    
    @objc private func closeBtnTapped(){
        dialogView.isHidden = true
        blurView.isHidden = true
        for view in self.dialogView.subviews {
            view.removeFromSuperview()
        }
    }


}
extension LeaderboardDetailsNewVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return tableViewDataArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableViewDataArray[section].opened {
            if section == 0 {
                let data = tableViewDataArray[section].data as! [LeaderboardAssets]
                return 1 + data.count
            }
            else if section == 1 {
                return 1
            }
            else if section == 2 {
                let data = tableViewDataArray[section].data as! [String]
                return 1 + data.count
                //return 1
            }else if section == 3 {
                return 1
            }else if section == 4 {
                return 1
            }else if section == tableView.numberOfSections - 1 {
                return 1
            }
            else {
//                let data = tableViewDataArray[section].data as! EsgScore
//                return 1 + data.derivedValues.count
                return 1
            }
        }else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "UserInvestmentHeadingCell") as! InvestmentValuesNewHeadingCell
                
                cell.headingLabel.text = tableViewDataArray[indexPath.section].title
                cell.descLabel.text = tableViewDataArray[indexPath.section].desc
                
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "AssetsCell") as! AssetsCell
                
                let data = tableViewDataArray[indexPath.section].data as! [LeaderboardAssets]
                
                cell.assetName.text = data[indexPath.row - 1].symbolName
                cell.assetSymbol.text = data[indexPath.row - 1].symbol
                cell.assetShare.text = "$" + FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: data[indexPath.row - 1].shareAmount) +
                    "\n" + String(data[indexPath.row - 1].sharePercent) + "%"
                
                return cell
            }
        }else if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ControversyCell") as! ControversyCell
            
            cell.controversyLabel.text = tableViewDataArray[indexPath.section].title
            cell.controversyScore.text = tableViewDataArray[indexPath.section].desc
            cell.ellipses.isHidden = true
            cell.ellipses.heightConstaint?.constant = 0
            
            return cell
        }
        if indexPath.section == 2 {
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "UserInvestmentHeadingCell") as! InvestmentValuesNewHeadingCell
                
                cell.headingLabel.text = tableViewDataArray[indexPath.section].title
                cell.descLabel.text = tableViewDataArray[indexPath.section].desc
                
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "UserValuesCell") as! UserValuesCell
                
                let positiveVals = tableViewDataArray[indexPath.section].data as! [String]
                
                cell.image1.image = FintechUtils.sharedInstance.getInvestmentValuesIcons(investmentValue: positiveVals[indexPath.row - 1])
                cell.image2.image = UIImage(named: "care")
                cell.valuesLabel.text = positiveVals[indexPath.row - 1]
                
                return cell
            }
        }else if indexPath.section == 3 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ControversyCell") as! ControversyCell
            
            cell.controversyLabel.text = tableViewDataArray[indexPath.section].title
            cell.controversyScore.text = tableViewDataArray[indexPath.section].desc
            
            let data = tableViewDataArray[indexPath.section].data as? ControversyData
            if data == nil {
                cell.ellipses.isHidden = true
                cell.ellipses.heightConstaint?.constant = 0
            }else {
                cell.ellipses.isHidden = false
                cell.ellipses.heightConstaint?.constant = 20
            }
            
            return cell
        }
        else if indexPath.section == 4 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "UserInvestmentMainHeadingCell") as! InvestmentValuesNewMainHeadingCell
            
            let data = tableViewDataArray[indexPath.section].data as! UserPortfolioComposition
            cell.headingLabel.text = tableViewDataArray[indexPath.section].title
            //cell.descLabel.text = tableViewDataArray[indexPath.section].desc
            //cell.scoreLabel.text = data.impact
            cell.scoreLabel.text = FintechUtils.sharedInstance.getImpactForPortfolio(data.zScore)
            //cell.parentView.backgroundColor = tableViewDataArray[indexPath.section].cardColor
            
            return cell
        }else if indexPath.section == tableView.numberOfSections - 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "EsgSubFactorCell") as! ControversyCell
            
            cell.controversyLabel.text = tableViewDataArray[indexPath.section].title
            
            return cell
        }
        else {
            let data = tableViewDataArray[indexPath.section].data as! NewEsgScoreModel
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "UserInvestmentSubHeadingCell") as! InvestmentValuesNewSubHeadingCell
                
                
                cell.headingLabel.text = tableViewDataArray[indexPath.section].title
                //cell.descLabel.text = tableViewDataArray[indexPath.section].desc
                //cell.scoreLabel.text = data.impact
                cell.scoreLabel.text = FintechUtils.sharedInstance.getImpactForPortfolio(data.zScore)
                cell.headingLabel.textColor = tableViewDataArray[indexPath.section].cardColor
                return cell
                
            }else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "UserValuesCell2") as! UserValuesCell2
                
                let derivedValue = data.derivedValues[indexPath.row - 1]
                
                cell.headingLbl.text = derivedValue.valueName
                cell.iconImage.image = FintechUtils.sharedInstance.getInvestmentValuesIcons(investmentValue: derivedValue.valueName)
                
                if derivedValue.score > 0.0 {
                    cell.impactLbl.text = "Some impact"
                }else {
                    cell.impactLbl.text = "No impact"
                }
                
                return cell
            }
            
            
            
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 3 {
            let data = tableViewDataArray[3].data as? ControversyData
            if data != nil {
                let model = ControversyModel.init(success: true, message: "Data found", data: data!)
                showControversyDialog(model: model)
            }
        }
        
        if indexPath.section == tableView.numberOfSections - 1 {
            let storyboard = UIStoryboard(name: "Portfolio", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "EsgSubFactorVC") as! EsgSubFactorVC
            vc.envTagsData = parseSubFactorsData(esgType: PortfolioEsgType.ENV)
            vc.socTagsData = parseSubFactorsData(esgType: PortfolioEsgType.SOC)
            vc.govTagsData = parseSubFactorsData(esgType: PortfolioEsgType.GOV)
            vc.tagsLabelColor = .black
            vc.tagsBackgroundColor = .lightGray
            vc.companyName = userLeaderboardData.name
            self.navigationController?.pushViewController(vc, animated: true)
            //self.navigationController?.present(vc, animated: true, completion: nil)
        }
        
        if indexPath.row == 0 {
            if tableViewDataArray[indexPath.section].opened {
                tableViewDataArray[indexPath.section].opened = false
            }else {
                tableViewDataArray[indexPath.section].opened = true
            }
            
        }
        
        let sections = IndexSet.init(integer: indexPath.section)
        tableView.reloadSections(sections, with: .none)
    }
    
    private func parseSubFactorsData(esgType: String) -> [SwiftTagData]{
        var tagsData = [SwiftTagData]()
        
        if esgType == PortfolioEsgType.ENV {
            for value in userLeaderboardData.portfolioComposition.environment.derivedValues {
                tagsData.append(SwiftTagData.init(label: value.valueName, tagImage: FintechUtils.sharedInstance.getInvestmentValuesIcons(investmentValue: value.valueName), value: value.score))
            }
        }else if esgType == PortfolioEsgType.SOC {
            for value in userLeaderboardData.portfolioComposition.socialScore.derivedValues {
                tagsData.append(SwiftTagData.init(label: value.valueName, tagImage: FintechUtils.sharedInstance.getInvestmentValuesIcons(investmentValue: value.valueName), value: value.score))
            }
        }else {
            for value in userLeaderboardData.portfolioComposition.governanceScore.derivedValues {
                tagsData.append(SwiftTagData.init(label: value.valueName, tagImage: FintechUtils.sharedInstance.getInvestmentValuesIcons(investmentValue: value.valueName), value: value.score))
            }
        }
        
        
        
        return tagsData
    }
    
}

