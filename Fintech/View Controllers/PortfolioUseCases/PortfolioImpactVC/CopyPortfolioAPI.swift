//
//  CopyPortfolioAPI.swift
//  Fintech
//
//  Created by broadstone on 09/03/2022.
//  Copyright © 2022 Broadstone Technologies. All rights reserved.
//

import Foundation
import UIKit
extension PortfolioImpactViewController {
    func callCopyPortfolioAPI(){
        DispatchQueue.main.async {
            self.view.activityStartAnimating()
        }
        let namespace = SPManager.sharedInstance.getCurrentUserNamespace()
        let params:Dictionary<String,AnyObject> = [
            "namespace" : namespace as AnyObject,
            "portfolio_id": portfolioImpactModel.data.portfolioInfo.portfolioId as AnyObject]
        
        APIManager.sharedInstance.postRequest(serviceName: ApiUrl.COPY_PORTFOLIO_URL, sendData: params, success: { (response) in
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
                let toast = "Request received, we will update the portfolio shortly"
                self.view.makeToast(toast, duration: 7.0)
            }
        },failure: { (data) in
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
                let toast = "Unable to copy portfolio, try again later"
                self.view.makeToast(toast)
                
                
            }
        })
    }
}
