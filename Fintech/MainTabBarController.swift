//
//  MainTabBarController.swift
//  Fintech
//
//  Created by broadstone on 26/08/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import UIKit

extension Notification.Name {
    static let refreshAllTabs = Notification.Name("RefreshAllTabs")
    static let portfolioCopied = Notification.Name("PortfolioCopied")
}

class MainTabBarController: UITabBarController, UITabBarControllerDelegate {
    
    var notificationType = FCM.NOTIFICATION_TYPE_DEFAULT

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 15, *) {
            let tabBarAppearance = UITabBarAppearance()
            tabBarAppearance.backgroundColor = UIColor.init(named: "AppBgColor")!
            tabBarAppearance.stackedLayoutAppearance.selected.titleTextAttributes = [.foregroundColor: UIColor.init(named: "AppMainColor")!]
            tabBarAppearance.stackedLayoutAppearance.normal.titleTextAttributes = [.foregroundColor: UIColor.darkGray]
            tabBar.standardAppearance = tabBarAppearance
            tabBar.scrollEdgeAppearance = tabBarAppearance
        }
        
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let invesmentPersonaVC = storyboard.instantiateViewController(withIdentifier: "invesmentPersonaVC") as! invesmentPersonaVC
        let portfolioVC = storyboard.instantiateViewController(withIdentifier: "PortfolioVC") as! PortfolioVC
        let portfolioMainVC = storyboard.instantiateViewController(withIdentifier: "PortfolioMainVC") as! PortfolioMainVC
        //let educationVC = storyboard.instantiateViewController(withIdentifier: "EducationalSectionVC") as! EducationalSectionVC
        let resoucesStoryboard = UIStoryboard(name: "Resources", bundle: nil)
        let educationVC = resoucesStoryboard.instantiateViewController(withIdentifier: "ResourcesNavigationController") as! UINavigationController
        let accountVC = storyboard.instantiateViewController(withIdentifier: "AccountVC") as! AccountVC
        
        
        invesmentPersonaVC.tabBarItem.title = getLocalizedString(key: "_dashboard")
        portfolioVC.tabBarItem.title = getLocalizedString(key: "holdings_key")
        portfolioMainVC.tabBarItem.title = getLocalizedString(key: "_build")
        educationVC.tabBarItem.title = getLocalizedString(key: "_resources")
        accountVC.tabBarItem.title = getLocalizedString(key: "Account")
        
        educationVC.tabBarItem.image = UIImage(named: "resources_gray")
        educationVC.tabBarItem.selectedImage = UIImage(named: "resources_blue")
        //localization()
        
        self.viewControllers = [invesmentPersonaVC, portfolioVC, portfolioMainVC, educationVC, accountVC]
        

        NotificationCenter.default.addObserver(forName: .refreshAllTabs, object: nil, queue: nil) {
            (notification) in

            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let vc1 = storyboard.instantiateViewController(withIdentifier: "invesmentPersonaVC") as! invesmentPersonaVC
            let vc2 = storyboard.instantiateViewController(withIdentifier: "PortfolioVC") as! PortfolioVC
            let vc3 = storyboard.instantiateViewController(withIdentifier: "PortfolioMainVC") as! PortfolioMainVC
            //let vc4 = storyboard.instantiateViewController(withIdentifier: "EducationalSectionVC") as! EducationalSectionVC
            let resoucesStoryboard = UIStoryboard(name: "Resources", bundle: nil)
            let vc4 = resoucesStoryboard.instantiateViewController(withIdentifier: "ResourcesNavigationController") as! UINavigationController
            let vc5 = self.viewControllers?[4] as! AccountVC
            
            vc1.tabBarItem.title = getLocalizedString(key: "_dashboard")
            vc2.tabBarItem.title = getLocalizedString(key: "holdings_key")
            vc3.tabBarItem.title = getLocalizedString(key: "_build")
            vc4.tabBarItem.title = getLocalizedString(key: "_resources")
            vc5.tabBarItem.title = getLocalizedString(key: "Account")
            
            vc4.tabBarItem.image = UIImage(named: "resources_gray")
            vc4.tabBarItem.selectedImage = UIImage(named: "resources_blue")

            self.viewControllers = [vc1,vc2,vc3,vc4,vc5]

            self.tabBarController?.tabBar.items![4].isEnabled = true
            self.tabBarController?.selectedIndex = 4
            
            self.disableHoldingsTabs()
        }
        
        NotificationCenter.default.addObserver(forName: .portfolioCopied, object: nil, queue: nil) {
            (notification) in
            DispatchQueue.main.async {
                self.portfolioCopied()
            }
        }
        disableHoldingsTabs()
        
        goToSomeVCFromNotification()
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func disableHoldingsTabs(){
        if !SPManager.sharedInstance.saveUserLoginStatus() || SPManager.sharedInstance.getCurrentUserQuestionaireStatus() != "done" {
            
            //self.tabBarController?.tabBar.items![1].isEnabled = false
            if let arrayOfTabBarItems = self.tabBar.items as AnyObject as? NSArray,let
               tabBarItem = arrayOfTabBarItems[1] as? UITabBarItem {
               tabBarItem.isEnabled = false
            }
        }
    }
    
    private func portfolioCopied() {
        NotificationCenter.default.addObserver(forName: .refreshAllTabs, object: nil, queue: nil) {
            (notification) in

            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let vc1 = storyboard.instantiateViewController(withIdentifier: "invesmentPersonaVC") as! invesmentPersonaVC
            let vc2 = storyboard.instantiateViewController(withIdentifier: "PortfolioVC") as! PortfolioVC
            let vc3 = storyboard.instantiateViewController(withIdentifier: "PortfolioMainVC") as! PortfolioMainVC
            //let vc4 = storyboard.instantiateViewController(withIdentifier: "EducationalSectionVC") as! EducationalSectionVC
            let resoucesStoryboard = UIStoryboard(name: "Resources", bundle: nil)
            let vc4 = resoucesStoryboard.instantiateViewController(withIdentifier: "ResourcesNavigationController") as! UINavigationController
            let vc5 = storyboard.instantiateViewController(withIdentifier: "AccountVC") as! AccountVC
            
            

            self.viewControllers = [vc1,vc2,vc3,vc4,vc5]
            self.tabBarController?.selectedIndex = 0
            
        }
    }

    private func localization(){
        self.tabBar.items![0].title = getLocalizedString(key: "_dashboard")
        self.tabBar.items![1].title = getLocalizedString(key: "holdings_key")
        self.tabBar.items![2].title = getLocalizedString(key: "_build")
        self.tabBar.items![3].title = getLocalizedString(key: "_resources")
        self.tabBar.items![4].title = getLocalizedString(key: "Account")
        
        self.tabBar.items![3].image = UIImage(named: "resources_gray")
        self.tabBar.items![3].selectedImage = UIImage(named: "resources_blue")
        
    }
    
    private func goToSomeVCFromNotification(){
        if !SPManager.sharedInstance.saveUserLoginStatus() {
            return
        }
        if notificationType == FCM.NOTIFICATION_TYPE_CHARITY {
            let storyboard = UIStoryboard(name: "Charity", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "CharityDashboardVC") as! CharityDashboardVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if notificationType == FCM.NOTIFICATION_TYPE_SPENDING {
            let storyboard = UIStoryboard(name: "Budgeting", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "BudgetingDashboard") as! BudgetingDashboard
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if notificationType == FCM.NOTIFICATION_TYPE_PORTFOLIO {
            self.tabBarController?.tabBar.items![1].isEnabled = true
            self.tabBarController?.selectedIndex = 1
        }
        else if notificationType == FCM.NOTIFICATION_TYPE_NEWSFEED {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "NotificationsVC") as! NotificationsVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if notificationType == FCM.NOTIFICATION_TYPE_EDUCATION {
            self.tabBarController?.tabBar.items![3].isEnabled = true
            self.tabBarController?.selectedIndex = 3
        }
        else if notificationType == FCM.NOTIFICATION_TYPE_SPENDING_INSIGHTS {
            let storyboard = UIStoryboard(name: "Budgeting", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "BudgetingDashboard") as! BudgetingDashboard
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if notificationType == FCM.NOTIFICATION_TYPE_LEADERBOARD {
            self.tabBarController?.tabBar.items![1].isEnabled = true
            self.tabBarController?.selectedIndex = 1
        }else if notificationType == FCM.NOTIFICATION_TYPE_GENERAL {
            
        }
    }
    
}
