//
//  OverspentDialogView.swift
//  Fintech
//
//  Created by broadstone on 10/03/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import UIKit

class OverspentDialogView: UIView {

    
    @IBOutlet weak var parentView: UIView!
    @IBOutlet weak var closeBtn: UIButton!
    @IBOutlet weak var categoryName: UILabel!
    @IBOutlet weak var amountPlannedLabel: UILabel!

    @IBOutlet weak var amountPlanned: UILabel!
    @IBOutlet weak var amountSpentLabel: UILabel!
    @IBOutlet weak var amountSpent: UILabel!
    @IBOutlet weak var amountSavedLabel: UILabel!
    @IBOutlet weak var amountSaved: UILabel!
    @IBOutlet weak var educationLabel: UILabel!
    @IBOutlet weak var educationBtn: UIButton!
    @IBOutlet weak var categoryIcon: UIImageView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    private func commonInit(){
        Bundle.main.loadNibNamed("OverspentDialogView", owner: self, options: nil)
        addSubview(parentView)
        parentView.frame = self.bounds
        parentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        
    }
    
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    
    
    
    func show(){
        UIApplication.shared.keyWindow?.addSubview(parentView)
    }
    
    @IBAction func educationBtnPressed(_ sender: Any) {
        
    }
    
}
