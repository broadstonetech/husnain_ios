
import Foundation
import UIKit

struct PaymentAPIUtils {
    static var isEqualDistribution: Bool = true
    static var charityPlan: String = "solid"
    static var amount: Double = 0.0
    static var stripeAmount: Double = 0.0
    static var applePayAmount: Double = 0.0
}
class PaymentsBaseClass: Parent {
    
    
//    override func viewDidLoad() {
//        NotificationCenter.default.addObserver(self, selector: #selector(self.onPaymentTokenGenerated(_:)), name: NSNotification.Name(rawValue:PaymentObservers.paymentCompleteNotificationKey), object: nil)
//    }
//
//    @objc private func onPaymentTokenGenerated(_ notification: NSNotification){
//        print("Payment base class #17")
//        if let token = notification.userInfo?["token"] as? String {
//            callPostTokenAPI(token: token, amount: PaymentAPIUtils.amount)
//        }
//    }
    
    func callPostTokenAPI(paymentToken: String, amount: Double){
        PaymentAPIUtils.amount = amount
        self.view.activityStartAnimating()
        let plan = PaymentAPIUtils.charityPlan
        var url = ApiUrl.DONATION_METHOD_SOLID
//        if plan == PaymentConstants.CHARITY_METHOD_SOLID {
//            url = ApiUrl.DONATION_METHOD_SOLID
//        } else {
//            url = ApiUrl.DONATION_METHOD_INVEST
//        }
        let namespace = SPManager.sharedInstance.getCurrentUserNamespace()
        
        let equalDistribution = PaymentAPIUtils.isEqualDistribution
        let totalCharitesCount = ArrayLists.sharedInstance.SelectedListOfCharies.count
        
        var charityData: Array<AnyObject> = Array<AnyObject>()
        for i in 0..<totalCharitesCount {
            let charity = ArrayLists.sharedInstance.SelectedListOfCharies[i]
            let charityName = charity.charityName
            let charityId = charity.charityID
            
            var singleCharityData: Dictionary<String, AnyObject>
            if equalDistribution {
                 singleCharityData = ["charity_name": charityName as AnyObject,
                     "charity_id": charityId as AnyObject,
                     "share_amount": amount/Double(totalCharitesCount) as AnyObject,
                     "share_percentage": 100/Double(totalCharitesCount) as AnyObject]
            }else{
                singleCharityData = ["charity_name": charityName as AnyObject,
                    "charity_id": charityId as AnyObject,
                    "share_amount": (amount)*(Double(persentageAmountArray[i])/100.0) as AnyObject,
                    "share_percentage": Double(persentageAmountArray[i]) as AnyObject]
            }
            
            charityData.append(singleCharityData as AnyObject)
            
            
            
        }
        let params: Dictionary<String, AnyObject> =
                        ["namespace": namespace as AnyObject,
                        "plan": plan as AnyObject,
                        "equal_distribution": equalDistribution as AnyObject,
                        "donated_amount": amount as AnyObject,
                        "payment_token": paymentToken as AnyObject,
                        "charities": charityData as AnyObject,
                        "payment_method": "card" as AnyObject]
        
        print(params)
        APIManager.sharedInstance.postRequest(serviceName: url, sendData: params, success: { (response) in
            print(response)
            
            if APIManager.sharedInstance.isSuccess {
                    
                    DispatchQueue.main.async {
                        
                    }
                    
            } else {
                
            }
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
            DispatchQueue.main.async {
                print("IN SUCCESS")
                let storyboard = UIStoryboard(name: "Charity", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "PaymentCompletionVC") as! DonationCompleteVC
                //self.present(vc, animated: true, completion: nil)
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }, failure: { (data) in
            DispatchQueue.main.async {
                print("IN FALIURE")
                self.view.activityStopAnimating()
//                let storyboard = UIStoryboard(name: "Charity", bundle: nil)
//                let vc = storyboard.instantiateViewController(withIdentifier: "PaymentCompletionVC") as! DonationCompleteVC
//                self.navigationController?.pushViewController(vc, animated: true)
            }
            if APIManager.sharedInstance.error400 {
                DispatchQueue.main.async {
                    self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
                }
            }
            if(APIManager.sharedInstance.error401)
            {
                DispatchQueue.main.async {
                    self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
                }
            }else{
                
            }
        })
    }
}
