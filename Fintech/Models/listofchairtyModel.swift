// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let listofcharitesModel = try? newJSONDecoder().decode(ListofcharitesModel.self, from: jsonData)

import Foundation

// MARK: - ListofcharitesModel
struct ListofcharitesModel: Codable {
    let success: Int?
    let message: [MessagelistofCharites]?
}

// MARK: - Message
struct MessagelistofCharites: Codable {
    let userValue: String?
    let charities: [Charity]?
    
    enum CodingKeys: String, CodingKey {
        case userValue = "user_value"
        case charities
    }
}

// MARK: - Charity
struct Charity: Codable {
    let charityID, charityName: String?
    let url: String?
    let donationUrl: String?
    let charityDescription: String?
    let otherValues: [String]?
    
    enum CodingKeys: String, CodingKey {
        case charityID = "charity_id"
        case charityName = "charity_name"
        case url
        case donationUrl = "donation_url"
        case charityDescription = "description"
        case otherValues = "other_values"
    }
}

