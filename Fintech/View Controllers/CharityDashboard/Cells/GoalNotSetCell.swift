//
//  GoalNotSetCell.swift
//  Fintech
//
//  Created by broadstone on 30/12/2020.
//  Copyright © 2020 Broadstone Technologies. All rights reserved.
//

import Foundation
import UIKit

class GoalNotSetCell: UITableViewCell {
    @IBOutlet weak var goalProgressLabel:UILabel!
    @IBOutlet weak var goalNotSetDescLabel:UILabel!
    @IBOutlet weak var setGoalBtn:UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}
