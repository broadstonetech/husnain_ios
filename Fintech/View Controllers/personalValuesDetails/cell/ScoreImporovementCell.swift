//
//  ScoreImporovementCell.swift
//  Fintech
//
//  Created by broadstone on 12/01/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import Foundation
import UIKit

class ScoreImporovementCell: UITableViewCell {
    let cellContent = ["What can I do to improve this score?",
                       "Awareness is the first step towards action. You can also support your favorite charity through Vesgo.",
                       "Veso team has curated a number of resources for you:",
                       "\u{2022} Calculate your footprint on the environment:",
                        "\u{2022} Find and buy carbon offsets:",
                        "\u{2022} Start or support a petition:",
                        "\u{2022} Union of concerned Scientists:",
                        "\u{2022} Freedom to thrive:",
                        "\u{2022} Use this interactive simulator to evaluate different scenarios that may help you decide how you can affect climate change."]
    
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var descLabel1: UILabel!
    @IBOutlet weak var Link1Label: UILabel!
    @IBOutlet weak var Link2Label: UILabel!
    @IBOutlet weak var Link3Label: UILabel!
    @IBOutlet weak var Link4Label: UILabel!
    @IBOutlet weak var Link5Label: UILabel!
    @IBOutlet weak var Link6Label: UILabel!
    
    @IBOutlet weak var Link1Btn: UIButton!
    @IBOutlet weak var Link2Btn: UIButton!
    @IBOutlet weak var Link3Btn: UIButton!
    @IBOutlet weak var Link4Btn: UIButton!
    @IBOutlet weak var Link5Btn: UIButton!
    @IBOutlet weak var Link6Btn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func initLabels(){
        headingLabel.text = cellContent[0]
        descLabel.text = cellContent[1]
        descLabel1.text = cellContent[2]
        Link1Label.text = cellContent[3]
        Link2Label.text = cellContent[4]
        Link3Label.text = cellContent[5]
        Link4Label.text = cellContent[6]
        Link5Label.text = cellContent[7]
        Link6Label.text = cellContent[8]
        
    }
    
    @IBAction func link1Clicked(_ sender: UIButton){
        openUrl(url: Link1Btn.title(for: .normal)!)
    }
    
    @IBAction func link2Clicked(_ sender: UIButton){
        openUrl(url: Link2Btn.title(for: .normal)!)
    }
    
    @IBAction func link3Clicked(_ sender: UIButton){
        openUrl(url: Link3Btn.title(for: .normal)!)
    }
    
    @IBAction func link4Clicked(_ sender: UIButton){
        openUrl(url: Link4Btn.title(for: .normal)!)
    }
    
    @IBAction func link5Clicked(_ sender: UIButton){
        openUrl(url: Link5Btn.title(for: .normal)!)
    }
    
    @IBAction func link6Clicked(_ sender: UIButton){
        openUrl(url: Link6Btn.title(for: .normal)!)
    }
    
    private func openUrl(url: String){
        if let Url = URL(string: url) {
            UIApplication.shared.open(Url)
        }
    }
}
