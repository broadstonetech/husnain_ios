//
//  DonationLabelCell.swift
//  Fintech
//
//  Created by broadstone on 30/12/2020.
//  Copyright © 2020 Broadstone Technologies. All rights reserved.
//

import Foundation
import UIKit
class DonationLabelCell: UITableViewCell {
    @IBOutlet weak var donationLabel:UILabel!
    
    override class func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
