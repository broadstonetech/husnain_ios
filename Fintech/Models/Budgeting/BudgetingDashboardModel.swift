// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let budgetingDashboardModel = try? newJSONDecoder().decode(BudgetingDashboardModel.self, from: jsonData)

import Foundation

// MARK: - BudgetingDashboardModel
struct BudgetingDashboardModel: Codable {
    let success: Bool?
    let message: String?
    let data: BudgetingDashboardModelData?
}

// MARK: - BudgetingDashboardModelData
struct BudgetingDashboardModelData: Codable {
    let data: DataData?
    let spendingHistory: SpendingHistory?
    let planInfo: PlanInfo?

    enum CodingKeys: String, CodingKey {
        case data
        case spendingHistory = "spending_history"
        case planInfo = "plan_info"
    }
}

// MARK: - DataData
struct DataData: Codable {
    let needsBucket, havesBucket, wantsBucket, miscBucket: [Bucket]?

    enum CodingKeys: String, CodingKey {
        case needsBucket = "needs_bucket"
        case havesBucket = "haves_bucket"
        case wantsBucket = "wants_bucket"
        case miscBucket = "misc_bucket"
    }
}

// MARK: - Bucket
struct Bucket: Codable {
    let categoryName: String?
    let amount: Double?

    enum CodingKeys: String, CodingKey {
        case categoryName = "category_name"
        case amount
    }
}

// MARK: - PlanInfo
struct PlanInfo: Codable {
    let needsBucketSpent: Double?
    let havesBucketSpent: Double?
    let wantsBucketSpent: Double?
    let miscBucketSpent, needsBucketAmount, havesBucketAmount, wantsBucketAmount: Double?
    let plannedStartDate, plannedEndDate: Int64?
    let plannedAmountSpent, plannedAmount: Double?

    enum CodingKeys: String, CodingKey {
        case needsBucketSpent = "needs_bucket_spent"
        case havesBucketSpent = "haves_bucket_spent"
        case wantsBucketSpent = "wants_bucket_spent"
        case miscBucketSpent = "misc_bucket_spent"
        case needsBucketAmount = "needs_bucket_amount"
        case havesBucketAmount = "haves_bucket_amount"
        case wantsBucketAmount = "wants_bucket_amount"
        case plannedStartDate = "planned_start_date"
        case plannedEndDate = "planned_end_date"
        case plannedAmount = "planned_amount"
        case plannedAmountSpent = "planned_amount_spent"
    }
}

// MARK: - SpendingHistory
struct SpendingHistory: Codable {
    let needsBucketSpent, havesBucketSpent, wantsBucketSpent, miscBucketSpent: Double?
    let totalAmountSpent: Double?

    enum CodingKeys: String, CodingKey {
        case needsBucketSpent = "needs_bucket_spent"
        case havesBucketSpent = "haves_bucket_spent"
        case wantsBucketSpent = "wants_bucket_spent"
        case miscBucketSpent = "misc_bucket_spent"
        case totalAmountSpent = "total_amount_spent"
    }
}
