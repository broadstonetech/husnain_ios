//
//  VesgoTutorialCell.swift
//  Fintech
//
//  Created by broadstone on 18/03/2022.
//  Copyright © 2022 Broadstone Technologies. All rights reserved.
//

import UIKit
import youtube_ios_player_helper

class VesgoTutorialCell: UITableViewCell {
    
    @IBOutlet weak var videoLabel: UILabel!
    @IBOutlet weak var playerView: YTPlayerView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
