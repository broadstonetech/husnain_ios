//
//  SymbolCell.swift
//  Fintech
//
//  Created by broadstone on 06/08/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import UIKit

class SymbolCell: UITableViewCell {

    @IBOutlet weak var parentView: UIView!
    @IBOutlet weak var symbolLabel: UILabel!
    @IBOutlet weak var companyLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
