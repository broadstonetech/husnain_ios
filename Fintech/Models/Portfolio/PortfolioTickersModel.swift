//
//  PortfolioTickersModel.swift
//  Fintech
//
//  Created by broadstone on 06/08/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import Foundation

struct PortfolioTickersModel {
    let success: Bool
    let message: String
    let data: [PortfolioTickersData]
}

struct PortfolioTickersData {
    let symbol: String
    let CompanyName: String
}
