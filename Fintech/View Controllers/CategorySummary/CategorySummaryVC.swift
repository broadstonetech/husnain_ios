//
//  CategorySummaryVC.swift
//  Fintech
//
//  Created by Apple on 07/05/2020.
//  Copyright © 2020 Broadstone Technologies. All rights reserved.
//###############################################################
//                 In This View show summary answer category wise
//                 investment category change image in answer wise
//     Rebuild Button    Move to summary page   and back Questionnaire View
//     LooksGood Button  Move to Newsfeed View
//###############################################################

import UIKit
// variables
var personalList = [summary]()
var investemenList = [summary]()
var valuesList = [summary]()
var riskList = [summary]()
var tableData = [summary]()
var data = [summary]()
var personamodel: Messasge3?

class CategorySummaryVC: UIViewController {
    
    //MARK : - outlets
    @IBOutlet weak var investement_lbl: UILabel!
    @IBOutlet weak var values_lbl: UILabel!
    @IBOutlet weak var risk_lbl: UILabel!
    @IBOutlet weak var personal_lbl: UILabel!
    @IBOutlet weak var Risk_img: UIImageView!
    @IBOutlet weak var values_Img: UIImageView!
    @IBOutlet weak var investment_Img: UIImageView!
    @IBOutlet weak var personaldetails_img: UIImageView!
    
    
    //MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        // Do any additional setup after loading the view.
        data = self.getUserData(key: SPManager.sharedInstance.getCurrentUserEmail()) ?? [summary]()
        for sum in data{
            if sum.catogries == 1 {
                print("qd\(sum.questionNo!)")
                print(sum.answertext!)
                print(sum.catogries!)
                personalList.append(sum)
                tableData = personalList

                var arry = [String]()
                for personal  in personalList {
                    arry.append(getLocalizedString(key: personal.answertext ?? "",comment: ""))
                    
                }

              //  print(arry)
                //   investement_lbl.text = arry2.joined(separator: ",")
                

            } else
                if sum.catogries == 2 {
                    investemenList.append(sum)
                }else
                    if sum.catogries == 3 {
                        riskList.append(sum)
                    }else
                        if sum.catogries == 4 {
                            valuesList.append(sum)
            }
        }
        //MARK: - Funcation call
     //   pernonallist()
        investment()
        values()
        risk()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.personaapi()
    

    }
    //MARK: - Change Image investment category Answer wise
    func pernonallist(){
        tableData = personalList
        var arry = [String]()
        for personal  in personalList {
            arry.append(getLocalizedString(key: personal.answertext ?? "",comment: ""))
            
        }
        
        if personalList[0].answertext ==  "Q1Option1" && personalList[1].answertext == "Q2Option1"{
            personaldetails_img.image = UIImage(named: "m_gen_z")
            
        }else if personalList[0].answertext ==  "Q1Option1" && personalList[1].answertext == "Q2Option2"{
            personaldetails_img.image = UIImage(named: "f_gen_z")
        }else if personalList[0].answertext ==  "Q1Option1" && personalList[1].answertext == "Q2Option3"{
            personaldetails_img.image = UIImage(named: "n_gen_z")
        }else if personalList[0].answertext ==  "Q1Option2" && personalList[1].answertext == "Q2Option1"{
            personaldetails_img.image = UIImage(named: "m_mill")
        }else if personalList[0].answertext ==  "Q1Option2" && personalList[1].answertext == "Q2Option2"{
            personaldetails_img.image = UIImage(named: "f_milli")
        }else if personalList[0].answertext ==  "Q1Option2" && personalList[1].answertext == "Q2Option3"{
            personaldetails_img.image = UIImage(named: "n_milli")
        }else if personalList[0].answertext ==  "Q1Option3" && personalList[1].answertext == "Q2Option1"{
            personaldetails_img.image = UIImage(named: "m_gen_x")
        }else if personalList[0].answertext ==  "Q1Option3" && personalList[1].answertext == "Q2Option2"{
            personaldetails_img.image = UIImage(named: "f_gen_x")
        }else if personalList[0].answertext ==  "Q1Option3" && personalList[1].answertext == "Q2Option3"{
            personaldetails_img.image = UIImage(named: "n_gen_x")
        }else if personalList[0].answertext ==  "Q1Option4" && personalList[1].answertext == "Q2Option1"{
            personaldetails_img.image = UIImage(named: "m_baby_boomers")
        }else if personalList[0].answertext ==  "Q1Option4" && personalList[1].answertext == "Q2Option2"{
            personaldetails_img.image = UIImage(named: "f_baby_boomers")
        }else if personalList[0].answertext ==  "Q1Option4" && personalList[1].answertext == "Q2Option3"{
            personaldetails_img.image = UIImage(named: "n_baby_boomers")
        }
        
        
       // personal_lbl.text = arry.joined(separator: ",")
        
    }
    
    func investment(){
        var arry2 = [String]()
        for personal  in investemenList {
            arry2.append(getLocalizedString(key: personal.answertext ?? "",comment: ""))
            
        }
     //   investement_lbl.text = arry2.joined(separator: ",")
        
    }
    func values(){
        var arry3 = [String]()
        for personal  in valuesList {
            arry3.append(getLocalizedString(key: personal.answertext ?? "",comment: ""))
            
        }
        //values_lbl.text = arry3.joined(separator: ",")
        
    }
    
    func risk(){
        var arry4 = [String]()
        for personal  in riskList {
            arry4.append(getLocalizedString(key: personal.answertext ?? "",comment: ""))
            
        }
      //  risk_lbl.text = arry4.joined(separator: ",")
        
    }
    
    // MARK :- IB acction
    @IBAction func Looksgood_pressed(_ sender: Any) {
        
        if let tabbedbar = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "nav_bar_start") as? UINavigationController {
            self.view.window?.rootViewController = tabbedbar
        }
    }
    
    @IBAction func tryagain_pressed(_ sender: Any) {
        
        let alert = UIAlertController(title: "Investment strategy", message: "Do you want to build a new strategy or edit an existing option?" ,  preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "Edit", style: UIAlertAction.Style.default, handler: { _ in
            
            summaryFlag = true
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SummaryVC") as! SummaryVC
            SelectedQuestionNO = -1
            self.navigationController?.pushViewController(vc, animated: true)
            
        }))
        alert.addAction(UIAlertAction(title: "Stat Again", style: UIAlertAction.Style.default, handler: { _ in
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "homesurfay") as UIViewController
            UIApplication.shared.keyWindow?.rootViewController = vc
            
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style:
            UIAlertAction.Style.default, handler: { _ in
                
                //Cancel Action
        }))
        
        
        self.present(alert, animated: true, completion: nil)
        
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension UIImageView {
    
    func makeRounded() {
        let radius = self.frame.width/30.0
        self.layer.cornerRadius = radius
        self.layer.masksToBounds = true
    }
}

extension CategorySummaryVC{
    
    
    func pnaapi(){
        let url = "http://165.22.3.113/user/persona"
        let namespace = SPManager.sharedInstance.getCurrentUserNamespace()
        //let namespace = "uk-DdZ0"
        print(namespace)
        postManagerUser(method: "POST", isThisURLEmbdedData: false, urlString: url, parameter: ["namespace":namespace,"data": data], success: {(value) in
            
            do
            {
                print(value)
                let jsonObject = try JSONDecoder().decode(PersonaModel?.self, from: value)
                DispatchQueue.main.async {
                    if let value = jsonObject?.messasge
                    {
                        
                        personamodel = value
                        self.personal_lbl.text = personamodel?.demographic
                        self.investement_lbl.text = personamodel?.investment
                        self.risk_lbl.text = personamodel?.risk
                        self.values_lbl.text = personamodel?.values
                        print(personamodel)
                        //                        print(self.tickertabledta)
                        //                        self.tableview_summary.reloadData()
                        
                        // self.TableView_leaderboard.reloadData()
                        
                    }
                }
                
            }
            catch
            {
                DispatchQueue.main.async {
                    // self.btnEnable = true
                    
                    // self.POPUp(message: "No data found", time: 1.8)
                }
                
            }
            
        }, failure: {(err) in
            
            DispatchQueue.main.async {
                //self.btnEnable = true
                
                // self.POPUp(message: "Something went wrong", time: 1.8)
            }
            if APIManager.sharedInstance.error400 {
                //  self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
            }
            if(APIManager.sharedInstance.error401)
            {
                //  self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
            }else{
                
            }
            
        })
    }
    
}
extension CategorySummaryVC{
    func personaapi(){
        let url = "http://165.22.3.113/user/persona"
        let namespace = SPManager.sharedInstance.getCurrentUserNamespace()
        //let namespace = "uk-DdZ0"
        print(namespace)
        postManagerUser(method: "POST", isThisURLEmbdedData: false, urlString: url, parameter: ["namespace":namespace], success: {(value) in
            
            do
            {
                print(value)
                let jsonObject = try JSONDecoder().decode(PersonaModel?.self, from: value)
                DispatchQueue.main.async {
                    if let value = jsonObject?.messasge
                    {
                        
                           personamodel = value
                        self.personal_lbl.text = personamodel?.demographic
                        self.investement_lbl.text = personamodel?.investment
                        self.risk_lbl.text = personamodel?.risk
                        self.values_lbl.text = personamodel?.values
                        print(personamodel)
//                        print(self.tickertabledta)
//                        self.tableview_summary.reloadData()
                        
                        // self.TableView_leaderboard.reloadData()
                        
                    }
                }
                
            }
            catch
            {
                DispatchQueue.main.async {
                    // self.btnEnable = true
                    
                    // self.POPUp(message: "No data found", time: 1.8)
                }
                
            }
            
        }, failure: {(err) in
            
            DispatchQueue.main.async {
                //self.btnEnable = true
                
                // self.POPUp(message: "Something went wrong", time: 1.8)
            }
            if APIManager.sharedInstance.error400 {
              //  self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
            }
            if(APIManager.sharedInstance.error401)
            {
              //  self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
            }else{
                
            }
            
        })
    }
}
