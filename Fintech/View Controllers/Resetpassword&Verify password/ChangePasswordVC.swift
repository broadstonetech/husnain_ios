//
//  ResetPasswordVC.swift
//  Fintech
//
//  Created by MacBook-Mubashar on 26/08/2019.
//  Copyright © 2019 Broadstone Technologies. All rights reserved.
//

import UIKit
import Toast_Swift
import Lottie
class ChangePasswordVC: Parent {
    
    //MARK: -outlets
    @IBOutlet var submit_btn: UIButton!
    @IBOutlet var oldPassword_tf: UITextField!
    @IBOutlet var newPassword_tf: UITextField!
    @IBOutlet var confirmPassword_tf: UITextField!
    @IBOutlet var oldPasswordToggle_btn: UIButton!
    @IBOutlet var newPasswordToggle_btn: UIButton!
    @IBOutlet var confirmPasswordToggle_btn: UIButton!
    @IBOutlet var back_btn: UIButton!
    
    @IBOutlet weak var heading_changepassword_lbl: UILabel!
    @IBOutlet weak var changePasswordAnimation: AnimationView!
    @IBOutlet weak var oldPassword_view: UIView!
    //MARK: -class variables
    var oldPassFlag = true
    var newPassFlag = true
    var confirmPassFlag = true
    
    let imageIv = UIImage(named: "invisible")
    let imageV = UIImage(named: "visibility")
    
    
    //MARK: - Life cycle
    var isFromForgotpassword : Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isFromForgotpassword {
            oldPassword_view.isHidden = true
            
        }else {
            oldPassword_view.isHidden = false
        }
         localization()
        
        changePasswordAnimation.contentMode = .scaleAspectFit
        changePasswordAnimation.animationSpeed = 1
        changePasswordAnimation.play()
        //textfield delegates
        oldPassword_tf.delegate = self
        newPassword_tf.delegate = self
        confirmPassword_tf.delegate = self
        
        //IB Actions
        submit_btn.addTarget(self, action: #selector(resetPasswordTapped), for: UIControl.Event.touchUpInside)
        oldPasswordToggle_btn.addTarget(self, action: #selector(viewOldPasswordTapped), for: UIControl.Event.touchUpInside)
        newPasswordToggle_btn.addTarget(self, action: #selector(viewNewPasswordTapped), for: UIControl.Event.touchUpInside)
        confirmPasswordToggle_btn.addTarget(self, action: #selector(viewConfirmPasswordTapped), for: UIControl.Event.touchUpInside)
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        self.setNavBarTitile(title: getLocalizedString(key: "CHANGE PASSWORD", comment: ""), topItem: getLocalizedString(key: "CHANGE PASSWORD", comment: ""))
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    func localization(){
        submit_btn.setTitle(getLocalizedString(key: "Change password", comment: ""), for: .normal)
        let heading = getLocalizedString(key: "CHANGE PASSWORD", comment: "")
        
        heading_changepassword_lbl.text = heading
        
        oldPassword_tf.placeholder = getLocalizedString(key: "old_password", comment: "")
        newPassword_tf.placeholder = getLocalizedString(key: "new_password", comment: "")
        confirmPassword_tf.placeholder = getLocalizedString(key: "confirm_password", comment: "")
    }
    
    //MARK: - Controlling the Keyboard
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == oldPassword_tf {
            oldPassword_tf.resignFirstResponder()
            newPassword_tf.becomeFirstResponder()
        } else if textField == newPassword_tf {
            newPassword_tf.resignFirstResponder()
            confirmPassword_tf.becomeFirstResponder()
        }else if textField == confirmPassword_tf {
            confirmPassword_tf.resignFirstResponder()
        } else{
            textField.resignFirstResponder()
            
        }
        return true
    }
    
    // MARK: - IB Actions
    @objc func resetPasswordTapped() {
        if checkFormInputs() {
            DispatchQueue.main.async {
                
                if NetworkReachability.isConnectedToNetwork() {
                    DispatchQueue.main.async {
                        self.callResetPasswordApi()
                        
                        self.setAnalyticsAction(ScreenName:"ChangePasswordVc", method: "PasswordReset")
                        
                    }
                    
                } else {
                    self.showAlert(ConstantStrings.NO_INTERNET_AVAILABLE)
                }
            }
        }
    }
    
    @objc func viewOldPasswordTapped() {
        showHidePassword(textfield: oldPassword_tf, btnPwd: oldPasswordToggle_btn)
    
                if oldPassFlag == false {
                oldPasswordToggle_btn.setImage(imageIv, for: .normal)
                    oldPassFlag = true
                } else if oldPassFlag == true{
                oldPasswordToggle_btn.setImage(imageV, for: .normal)
                    oldPassFlag = false
                }
        
    }
    
    @objc func viewNewPasswordTapped() {
        showHidePassword(textfield: newPassword_tf, btnPwd: newPasswordToggle_btn)
        
        if newPassFlag == false {
        newPasswordToggle_btn.setImage(imageIv, for: .normal)
            newPassFlag = true
        } else if newPassFlag == true{
        newPasswordToggle_btn.setImage(imageV, for: .normal)
            newPassFlag = false
        }
    }
    
    @objc func viewConfirmPasswordTapped() {
        showHidePassword(textfield: confirmPassword_tf, btnPwd: confirmPasswordToggle_btn)
        
        if confirmPassFlag == false {
            confirmPasswordToggle_btn.setImage(imageIv, for: .normal)
            confirmPassFlag = true
        } else if confirmPassFlag == true{
            confirmPasswordToggle_btn.setImage(imageV, for: .normal)
            confirmPassFlag = false
        }
        
    }
//    @objc func backTapped() {
//
//        if isFromForgotpassword {
//            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
//
//            let vc = storyBoard.instantiateViewController(withIdentifier: "VerifyPasswordTokenVC") as! VerifyPasswordTokenVC
//            self.present(vc, animated:true, completion:nil)
//        }else {
//            self.view.makeToast("go back to previous vc")
//        }
//    }
    
    func checkFormInputs() -> Bool {
        if !isFromForgotpassword {
            if oldPassword_tf.text?.isEmpty ?? false {
                showAlert(ConstantStrings.OLD_PASSWORD_emty)
                return false
            }
        }
        if newPassword_tf.text?.isEmpty ?? false {
            showAlert(ConstantStrings.NEW_PASSWORD_Emty)
            return false
        } else if confirmPassword_tf.text?.isEmpty ?? false {
            showAlert(ConstantStrings.CONFIRM_PASSWORD_Emty)
            return false
        } else if newPassword_tf.text! != confirmPassword_tf.text! {
            showAlert(ConstantStrings.PASSWORD_MISMATCH)
            return false
        } else if !self.isPasswordValid(newPassword_tf.text!) {
            self.showAlert(ConstantStrings.PASSWORD_SHOULD_BE_8_CHARACTERS_ONE_ALPHABET)
            return false
        }
        
        return true
    }
    
    // MARK: - API call--SignIn
    func callResetPasswordApi() {
        self.view.activityStartAnimating()
        let url = ApiUrl.CHANGE_PASSWORD_URL
        
        let passwordSHA = newPassword_tf.text!.data(using: String.Encoding.utf8)?.sha1()
        let nameSpace = SPManager.sharedInstance.getCurrentUserNamespace()
        var data:Dictionary<String,AnyObject>
        if isFromForgotpassword {
            data = ["type" : "reset" as AnyObject,
                    "old_password" : "" as AnyObject,
                    "username": SPManager.sharedInstance.getCurrentUserUsername() as AnyObject,
                    "new_password" : passwordSHA as AnyObject]
        }else {
            let oldPasswordSha = oldPassword_tf.text!.data(using: String.Encoding.utf8)?.sha1()
            data = ["type" : "change" as AnyObject,
                    "old_password" : oldPasswordSha as AnyObject,
                    "username": SPManager.sharedInstance.getCurrentUserUsername() as AnyObject,
                    "new_password" : passwordSHA as AnyObject]
        }
        
        
        let params:Dictionary<String,AnyObject> = ["data" : data as AnyObject, "namespace" : nameSpace as AnyObject]
        print(data)
        APIManager.sharedInstance.postRequest(serviceName: url, sendData: params, success: { (response) in
            print(response)
            
            if APIManager.sharedInstance.isSuccess {
                DispatchQueue.main.async {
                    self.view.makeToast(ConstantStrings.PASSWORD_RESET_SUCCESS)
                    if self.isFromForgotpassword {
                        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2) ) {
                            if self.isFromForgotpassword {
                                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)

                                let vc = storyBoard.instantiateViewController(withIdentifier: "LogInVC") as! LogInVC
                                UIApplication.shared.keyWindow?.rootViewController = vc

                            }
                        }
                    }
                }
                
            }else{
                DispatchQueue.main.async {
                    if response["message"] as AnyObject != nil {
                        self.showAlert(response["message"] as! String)
                    }else {
                        self.showAlert(ConstantStrings.Error_msg_generic)
                    }
                }
            }
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
        }, failure: { (data) in
            print(data)
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
            if APIManager.sharedInstance.error400 {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
            }
            if(APIManager.sharedInstance.error401)
            {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
            }else{
                
            }
        })
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        oldPassword_tf.resignFirstResponder()
        newPassword_tf.resignFirstResponder()
        confirmPassword_tf.resignFirstResponder()
        
    }
    
    
}
