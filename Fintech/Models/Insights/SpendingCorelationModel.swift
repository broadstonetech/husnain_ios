// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let spendingCorelationModel = try? newJSONDecoder().decode(SpendingCorelationModel.self, from: jsonData)

import Foundation

// MARK: - SpendingCorelationModel
struct SpendingCorelationModel: Codable {
    let success: Bool
    var data: [SpendingCoreationData]
}

// MARK: - Datum
struct SpendingCoreationData: Codable {
    let categoryName: String
    let value: Double

    enum CodingKeys: String, CodingKey {
        case categoryName = "category_name"
        case value
    }
}
