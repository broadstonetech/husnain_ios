//
//  PortfolioVC.swift
//  Fintech
//
//  Created by MacBook-Mubashar on 23/07/2019.
//  Copyright © 2019 Broadstone Technologies. All rights reserved.
//
////#################################################################
//  In This view show all chart data and library data
//  portfolio data
// Deep Device data
// key Doc data
// Media data
//################################################################
import UIKit
import SwiftyToolTip
import Charts
import Floaty
import Lottie
let dollars1 = [10.0, 11.0, 12.0, 13.0, 14.0, 15.0, 16.0]
let dollars2 = [10.0, 11.5, 12.5, 13.5, 14.5, 15.5, 16.5]
let dollars3 = [10.0, 12.0, 13.0, 14.0, 15.0, 16.0, 17.0]
let months = ["Jan" , "Feb", "Mar", "Apr", "May", "June", "July"]
var  arr11 = [String]()
var infoType : String = ""
var infoValue : String = ""
class PortfolioVC: Parent {
    
    var selectedSegmentForLeaderboard: SelectedSegmentForLeaderboard = .PAST_MONTH
    var selectedLeaderboardType: SelectedLeaderboardType = .ALL
    var leaderboardModel: LeaderboardModel!
    var leaderboardModelAgeGroup: LeaderboardModel!
    var leaderboardData: [LeaderboardUsersList]!
    
    
    
    var pastMonth, pastThreeYear, pastYear: [Past]?
    //var pastMonth: [PastMonth]?
    var tickertabledta:[Message1]?
    var backtestdata: Messagebacktest?
    var backtestModel: PortfolioBacktestModel!
    var futurepredication:[FuturePredictionData]?
    var tableData:[Past]?
    //var pasttableDate: [PastMonth]?
    var selectedIndx = 0
    
    var chartDataModel: PortfolioChartModel!
    
    
    var tickerTitle = ["SPDR S&P 500 ETF Trust","Invesco Global Real Estate Fund Class A"]
    var tickershare = ["50","7"]
    var lederbordImgesarray = ["1st gold","2nd silver","3rd bronze"]
    var headingBacktest = ["Vesgo","S&P 500"]
    var returnBacktest = ["15.76%","12.98%"]

    @IBOutlet weak var futureprediction_linechartview: LineChartView!
    @IBOutlet weak var topbar_collectionview: UICollectionView!
    @IBOutlet weak var topbarSegmentControl: UISegmentedControl!
    //views
    
    var xAxis:[String] = []
    var yAxis:[Double] = []
    private var dis = String()
    var SELECTED_DATA_TYPE : String = ""
    let MAIN_DATA : String = "main_data"
    var bechYAxis:[Double] = []
    let unitsSold = [ 0.999999999999806,
                      2.0000000000004761,
                      2.9999999999998579,
                      1.0125076992217941e-14]
    var months = ["15/03/19", "16/03/19", "17/03/19", "18/03/19", "19/03/19"]
    // MArk: - linechart view outlet
    @IBOutlet weak var usd_lbl: UILabel!
    @IBOutlet weak var documents_view: UIView!
    @IBOutlet weak var InvestmentStrategy_showview_view: UIView!
    
    @IBOutlet weak var Heading_leaderboard_view: UIView!
    
    @IBOutlet weak var animation_Img_view: AnimationView!
    @IBOutlet weak var lineChartView: CustomLineChartView!
    @IBOutlet weak var future_predictions_view: UIView!
    @IBOutlet weak var summary_view: UIView!
    @IBOutlet weak var sample_portfolio_lbl: UILabel!
    @IBOutlet weak var history_view: UIView!
    @IBOutlet weak var details_view: UIView!
    @IBOutlet weak var Day_lbl: UIButton!
    @IBOutlet weak var Week_lbl: UIButton!
    // MArk: - outlets view1- summary
    @IBOutlet weak var year_btn: UIButton!
    @IBOutlet weak var month_lbl: UIButton!
    @IBOutlet weak var All_btn: UIButton!
    @IBOutlet weak var compalinDetails_lbl: UILabel!
    @IBOutlet weak var info_btn: UIButton!
    @IBOutlet weak var future_prediction_View: UIView!
    @IBOutlet weak var Btns_Day_Week_all_view: UIView!
    @IBOutlet weak var highlighterSV: UIStackView!
    @IBOutlet weak var vesgoHighlighterLabel: UILabel!
    @IBOutlet weak var dateHighlighterLabel: UILabel!
    // MArk: -  IB linechart!
    //libary tableview
    
    @IBOutlet weak var chartDataNotAvailable: UILabel!
    @IBOutlet weak var amount_date_enddate_backtest_lbl: UILabel!
    @IBOutlet weak var backtest_btn: UIButton!
    @IBOutlet weak var bactest_Tableview: UITableView!
    @IBOutlet weak var backtest_View: UIView!
    @IBOutlet weak var deepDivesTableview: UITableView!
    @IBOutlet weak var meditableView: UITableView!
    @IBOutlet weak var keyDocsTableview: UITableView!
    @IBOutlet weak var segment1_view: UIView!
    @IBOutlet weak var segment2_view: UIView!
    @IBOutlet weak var segment3_view: UIView!
    @IBOutlet weak var Main_viewLibrary: UIView!
    @IBOutlet weak var deepDives_btn: UIButton!
    @IBOutlet weak var keyDocs_btn: UIButton!
    @IBOutlet weak var media_btn: UIButton!
    @IBOutlet weak var portfolio_btn: UIButton!
    @IBOutlet weak var documents_btn: UIButton!
    @IBOutlet weak var leaderboard_btn: UIButton!
    @IBOutlet weak var futureprediction_btn: UIButton!
    @IBOutlet weak var floaty_btn: Floaty!
    @IBOutlet weak var companyInfo_btn : UIButton!
    @IBOutlet weak var tableview_summary: UITableView!
    // MArk: - outlets view3-lederboard
    @IBOutlet weak var lederboard_view: UIView!
    @IBOutlet weak var segment_lederboard: UISegmentedControl!
    
    @IBOutlet weak var segment_myage_leaderbaord: UISegmentedControl!
    // MArk: - outlets view2-history
    @IBOutlet var historyAppName_lbl: UILabel!
    @IBOutlet var historyCompetitorname_lbl: UILabel!
    @IBOutlet weak var historyStackview: UIStackView!
    @IBOutlet weak var historyTableview: UITableView!
    @IBOutlet weak var historyDetails_view: UIView!
    @IBOutlet weak var historyDisclosureView: UIView!
    @IBOutlet weak var historyDisclosure_btn: UIButton!
    @IBOutlet weak var historyDisclosure_img: UIImageView!
    @IBOutlet weak var lederboard_tableview: UITableView!
    @IBOutlet weak var sample_portfolio_height: NSLayoutConstraint!
    // MArk: - outlets view3-details
    // future predictions
    @IBOutlet var tableview_details: UITableView!
    @IBOutlet weak var futurePredictionsTableView: UITableView!
    @IBOutlet weak var futurePredictionsLineChart: LineChart!
    // MArk: - class varibales
    var refreshControl: UIRefreshControl!
    var selectedChartInterval : String = "day"
    var dataEntriesChart: [PointEntry]?
    var dataEntriesBenchmarkChart : [PointEntry]?
    var benchMarkTitle = "S&P 500"
    var chartdata = Int()
    
    var searchesResult = [SearchData]()
    
    
    //Starting new variables for portfolio screen
    
    @IBOutlet weak var riskToleranceLabel: UILabel!
    
    @IBOutlet weak var portfolioAnalysisLbl: UILabel!
    @IBOutlet weak var portfolioAnalysisDescLbl: UILabel!
    @IBOutlet weak var investmentValuesLbl: UILabel!
    @IBOutlet weak var investmentValuesDescLbl: UILabel!
    @IBOutlet weak var searchLbl: UILabel!
    @IBOutlet weak var searchDescLbl: UILabel!
    @IBOutlet weak var documentsLbl: UILabel!
    @IBOutlet weak var documentsDescLbl: UILabel!
    @IBOutlet weak var futurePredictionDisclaimerLable: UILabel!
    
    

    
    @IBAction func investmentValuesBtnPressed(_ sender: Any) {
        let storyboard = UIStoryboard(name: "InvestmentDetails", bundle: nil)
        //let vc = storyboard.instantiateViewController(withIdentifier: "UserInvestmentValuesVC")
        let vc = storyboard.instantiateViewController(withIdentifier: "InvestmentValuesNew")
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func portfolioCompositionBtnPressed(_ sender: Any) {
        let storyboard = UIStoryboard(name: "InvestmentDetails", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "PortfolioCompositionVC")
        self.navigationController?.pushViewController(vc, animated: true)
        
        setAnalyticsAction(ScreenName: "PortfolioVC", method: "PortfolioComposition")
        
    }
    
    @IBAction func incrementalSearchBtnPressed(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "InvestmentDetails", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "IncrementalSearchVC")
        self.navigationController?.pushViewController(vc, animated: true)
        setAnalyticsAction(ScreenName: "PortfolioVC", method: "IncrementalSearch")
    }
    
    @IBAction func deepDivesBtnPressed(_ sender: Any) {
        let storyboard = UIStoryboard(name: "SEC", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "AssetsListVC") as! AssetsListVC
        self.navigationController?.pushViewController(vc, animated: true)
        setAnalyticsAction(ScreenName: "PortfolioVC", method: "DeepDives")
        
    }
    
    @IBAction func riskToleranceBtnPressed(_ sender: Any) {
        let alert = UIAlertController(title: "Investment strategy", message: "Do you want to recreate your investment strategy?",  preferredStyle: UIAlertController.Style.alert)
        setAnalyticsAction(ScreenName: "PortfolioVC", method: "riskTolerance")

        alert.addAction(UIAlertAction(title: "No", style: UIAlertAction.Style.cancel, handler: { _ in
            alert.dismiss(animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertAction.Style.default, handler: { _ in
            self.getQuestionnaireStatusAPI()
        }))

        self.present(alert, animated: true, completion: nil)
        
    }
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //self.future_prediction_View.transform = CGAffineTransform(rotationAngle: .pi/2)
        highlighterSV.isHidden = true
        InvestmentStrategy_showview_view.isHidden  = true
        future_prediction_View.isHidden = true
        usd_lbl.isHidden = true
        compalinDetails_lbl.isHidden = true
        self.navigationController?.isNavigationBarHidden = true
        Main_viewLibrary.isHidden = true
        
        //        if items[1].title! == "Portfolio" {
        self.tabBarController?.tabBar.layer.zPosition = 4
      
        floaty_btn.isHidden = true
        if SPManager.sharedInstance.saveUserLoginStatus() {
            sample_portfolio_height.constant = 0
            compalinDetails_lbl.text = "\(SPManager.sharedInstance.getCurrentUserNameForProfile())'s Portfolio"
            if NetworkReachability.isConnectedToNetwork() {
                DispatchQueue.main.async {
                    self.lineChartView.chartDescription?.text = "Portfolio data is being processed."
                    self.callNewChartAPI()
                    //self.getLibData()
                    self.stocksallocationcallapi()
                    

                   // self.fucturepredticonapicall()
                }
                animation_Img_view.isHidden = true
            } else {
                animation_Img_view.contentMode = .scaleAspectFit
                animation_Img_view.animationSpeed = 0.5
                //award = AnimationView(name: "trophy")
                animation_Img_view.play()
                self.view.makeToast(ConstantStrings.NO_INTERNET_CONNECTION_FOUND)
            }
        }else{
            if NetworkReachability.isConnectedToNetwork() {

                getLeaderboardData(isAgeGroupData: false)
                self.lederboard_tableview.reloadData()
            }else {
          self.view.makeToast(ConstantStrings.NO_INTERNET_CONNECTION_FOUND)
            }
            
            compalinDetails_lbl.text = "Portfolio"

            sample_portfolio_height.constant = 0
        }
        sample_portfolio_lbl.isHidden = true
        initTableViews()
//        info_btn.addToolTip(description: "information about companies you own listed here. Tap to view more details.")
        usd_lbl.isHidden = true
        compalinDetails_lbl.isHidden = true
        
        Btns_Day_Week_all_view.isHidden = true
       
        // Do any additional setup after loading the view.
    }
    //override var supportedInterfaceOrientations: UIInterfaceOrientationMask { return .portrait }
    
    override func viewWillAppear(_ animated: Bool) {
        // UIView.setAnimationsEnabled(false)
        print(selectedIndx)
        InvestmentStrategy_showview_view.isHidden  = true
        if SPManager.sharedInstance.saveUserLoginStatus() {
            self.topbar_collectionview.isHidden = false
            self.topbar_collectionview!.heightConstaint?.constant = 50
            self.Heading_leaderboard_view!.heightConstaint?.constant = 0

            
        }else {
            DispatchQueue.main.async {
                self.summary_view.isHidden = true
                self.Main_viewLibrary.isHidden = true
                self.future_predictions_view.isHidden = true
                self.lederboard_view.isHidden = false
                self.topbar_collectionview.isHidden = true
                self.Heading_leaderboard_view!.heightConstaint?.constant = 88
                self.topbar_collectionview!.heightConstaint?.constant = 0
            }
        }
       var sample_username = UserDefaults.standard.string(forKey: "sampleportfolio_username") ?? "a"
        documents_view.isHidden = true
        lederboard_view.isHidden = true
        documents_view!.heightConstaint?.constant = 0
        if selectedIndx == 1 {
            documents_view.isHidden = true
            lederboard_view.isHidden = false
            documents_view!.heightConstaint?.constant = 0
            future_prediction_View.isHidden = true
            portfolio_btn.titleLabel?.fontSize = 13
            futureprediction_btn.titleLabel?.fontSize = 13
            documents_btn.titleLabel?.fontSize = 13
            leaderboard_btn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 15)
        }else {
        }
        
        
        
        
        var date = ""
        if SPManager.sharedInstance.saveUserLoginStatus() {
            
            if UserDefaults.standard.object(forKey: "\(SPManager.sharedInstance.getCurrentUserEmail())date") != nil {
                
                date = UserDefaults.standard.string(forKey: "\(SPManager.sharedInstance.getCurrentUserEmail())date")!
                if date != nil {
                    //InvestmentStrategy_showview_view.isHidden  = true
                } else {
                    date =  ""
                    // InvestmentStrategy_mesg_lbl.isHidden = true
                }
            } else {
                // key doesn't exist in defaults, use default value
                date = ""
                //InvestmentStrategy_mesg_lbl.isHidden = true
                //InvestmentStrategy_showview_view.isHidden = false
     
            }
        }else {
           // InvestmentStrategy_showview_view.isHidden  = true
            
        }
        UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")
        UIView.setAnimationsEnabled(true)
        
        
        
        portfolio_btn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 15)

        deepDives_btn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        self.navigationController?.isNavigationBarHidden = true
//        self.Day_lbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
//        self.Week_lbl.backgroundColor = #colorLiteral(red: 0.8980392157, green: 0.8941176471, blue: 0.9019607843, alpha: 1)
//        self.month_lbl.backgroundColor = #colorLiteral(red: 0.8980392157, green: 0.8941176471, blue: 0.9019607843, alpha: 1)
//        self.year_btn.backgroundColor = #colorLiteral(red: 0.8980392157, green: 0.8941176471, blue: 0.9019607843, alpha: 1)
//        self.All_btn.backgroundColor = #colorLiteral(red: 0.8980392157, green: 0.8941176471, blue: 0.9019607843, alpha: 1)
        
//        self.Week_lbl.backgroundColor = UIColor.clear
//
//        self.Week_lbl.setTitleColor(#colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1), for: .normal)
//        self.month_lbl.setTitleColor(#colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1), for: .normal)
//
//        self.year_btn.setTitleColor(#colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1), for: .normal)
//
//        self.All_btn.setTitleColor(#colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1), for: .normal)
//        self.All_btn.backgroundColor = UIColor.clear
//        self.month_lbl.backgroundColor = UIColor.clear
//        self.year_btn.backgroundColor = UIColor.clear
//
//        self.Week_lbl.backgroundColor = UIColor.clear
        
        selectedChartInterval = "day"
        SELECTED_DATA_TYPE = MAIN_DATA
        chartdata = 20
        
        if selectedIndx == 4 {
            setLandscapeOrientation(true)
        }else {
            setLandscapeOrientation(false)
        }
        
    }

    
    override func viewDidAppear(_ animated: Bool) {
        localization()
       
        if SPManager.sharedInstance.saveUserLoginStatus() {
            if NetworkReachability.isConnectedToNetwork() {
                self.getUserRiskFactor()
            } else {
                self.view.makeToast(ConstantStrings.NO_INTERNET_CONNECTION_FOUND)
            }
        }
        future_prediction_View.isHidden = true
       
        self.navigationController?.isNavigationBarHidden = true
        //Main_viewLibrary.isHidden = true
        //  self.gestureRecognizerShouldBegin(HorizontalBarChartRenderer)
        topbar_collectionview.reloadData()
        historyTableview.reloadData()
        
        
    }
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIPanGestureRecognizer) -> Bool {
        let velocity = gestureRecognizer.velocity(in: future_predictions_view)
        return abs(velocity.x) > abs(velocity.y)
    }
    
    
    
    // MArk: - floatybutton
    func floatybutton() {
        
        if SPManager.sharedInstance.saveUserLoginStatus() {
            floaty_btn.isHidden = true
        }else{
            floaty_btn.buttonColor = #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1)
            floaty_btn.itemButtonColor = #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1)
            floaty_btn.buttonImage = UIImage(named: "floatyicon")
            floaty_btn.addItem("Sample Portfolios",icon: UIImage(named: "services-portfolio1")!, handler: { item in
                if let tabbedbar = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "nav_bar_sampleportfolio") as? UINavigationController {
                    tabbedbar.modalPresentationStyle = .fullScreen
                    
                    UIApplication.shared.keyWindow?.rootViewController = tabbedbar
                }
            })
            
            floaty_btn.addItem("Account Setup",icon: UIImage(named: "baseline_person_add_black")!, handler: { item in
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "Main_login_page") as UIViewController
                vc.modalPresentationStyle = .fullScreen
                
                UIApplication.shared.keyWindow?.rootViewController = vc
            })
            DispatchQueue.main.async {
                self.view.addSubview(self.floaty_btn)
            }
        }
    }
    // MArk: - localization change Language data
    func localization(){
        setNavBarTitile(title:getLocalizedString(key: "Portfolio", comment: ""), topItem: getLocalizedString(key: "Portfolio", comment: ""))
        navigationController?.navigationBar.barStyle = .blackOpaque
        Day_lbl.setTitle(getLocalizedString(key: "5_day", comment: ""), for: .normal)
        Week_lbl.setTitle(getLocalizedString(key: "week_", comment: ""), for: .normal)
        month_lbl.setTitle(getLocalizedString(key: "_month", comment: ""), for: .normal)
        year_btn.setTitle(getLocalizedString(key: "_year", comment: ""), for: .normal)
        All_btn.setTitle(getLocalizedString(key: "_max", comment: ""), for: .normal)
        
        portfolioAnalysisLbl.text = getLocalizedString(key: "portfolio_analysis_lbl", comment: "")
        portfolioAnalysisDescLbl.text = getLocalizedString(key: "portfolio_analysis_desc", comment: "")
        investmentValuesLbl.text = getLocalizedString(key: "investment_values_lbl", comment: "")
        investmentValuesDescLbl.text = getLocalizedString(key: "investment_values_desc", comment: "")
        searchLbl.text = getLocalizedString(key: "search_lbl", comment: "")
        searchDescLbl.text = getLocalizedString(key: "search_desc", comment: "")
        documentsLbl.text = getLocalizedString(key: "portfolio_documents_lbl", comment: "")
        documentsDescLbl.text = getLocalizedString(key: "portfolio_documents_desc", comment: "")
        futurePredictionDisclaimerLable.text = getLocalizedString(key: "we_run_thousands_of_simulations", comment: "")
        
        arr11 = [
            getLocalizedString(key: "holdings_key", comment: ""),
            getLocalizedString(key: "leaderboard_key", comment: ""),
            getLocalizedString(key: "deep_dive", comment: ""),
            getLocalizedString(key: "past_performance", comment: ""),
            getLocalizedString(key: "future_prediction", comment: "")
        ]
    }
    //     MARK: - Class Functions
    func initTableViews() {
        self.tableview_summary.delegate = self
        self.tableview_summary.dataSource = self
        self.tableview_summary.estimatedRowHeight = 300
        self.tableview_summary.rowHeight = UITableView.automaticDimension
        self.historyTableview.delegate = self
        self.historyTableview.dataSource = self
        self.historyTableview.estimatedRowHeight = 120
        self.historyTableview.rowHeight = UITableView.automaticDimension
        
        self.tableview_details.delegate = self
        self.tableview_details.dataSource = self
        self.tableview_details.estimatedRowHeight = 120
        self.tableview_details.rowHeight = UITableView.automaticDimension
        
        self.futurePredictionsTableView.delegate = self
        self.futurePredictionsTableView.dataSource = self
        self.futurePredictionsTableView.estimatedRowHeight = 100
        self.futurePredictionsTableView.rowHeight = UITableView.automaticDimension
        
        self.keyDocsTableview.delegate = self
        self.keyDocsTableview.dataSource = self
        self.keyDocsTableview.rowHeight = UITableView.automaticDimension
        self.keyDocsTableview.estimatedRowHeight = 200
        
        self.deepDivesTableview.delegate = self
        self.deepDivesTableview.dataSource = self
        self.deepDivesTableview.rowHeight = UITableView.automaticDimension
        self.deepDivesTableview.estimatedRowHeight = 200
        
        self.meditableView.delegate = self
        self.meditableView.dataSource = self
        self.meditableView.rowHeight = UITableView.automaticDimension
        self.meditableView.estimatedRowHeight = 200
    }
    // MARK: - Line chart funcs
    //generate chart random enteries for test data
    func generateRandomEntries(arrayList : [PortfolioDataModel]) -> [PointEntry] {
        var result: [PointEntry] = []
        for i in 0..<arrayList.count {
            
            let yValue = arrayList[i].getDataYaxisValue()
            let xValue = arrayList[i].getDataXaxisValue() //Int(arc4random() % 500)
            
            result.append(PointEntry(systolic: Int(yValue), diastolic: Int(ArrayLists.sharedInstance.portfolioBenchmarkDayChartDataList[i].getDataYaxisValue()), label: xValue))
            
        }
        return result
    }

    
    func loadBenchmarkChart(interval : String) {
        let  dataEntriesChart = generateRandomEntries(arrayList: ArrayLists.sharedInstance.portfolioBenchmarkMonthChartDataList)
    }
    @IBOutlet weak var collectionLayout: UICollectionViewFlowLayout!
    //        didSet {
    //            collectionLayout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
    //        }
    
    @IBAction func Segment_Myage_leaderboard(_ sender: Any) {
        let index = segment_myage_leaderbaord.selectedSegmentIndex
        if index == 0{
            selectedLeaderboardType = .ALL
            if leaderboardModel != nil {
                self.lederboard_tableview.reloadData()
            }else {
                self.getLeaderboardData(isAgeGroupData: false)
            }
        }else if index == 1{
            if SPManager.sharedInstance.saveUserLoginStatus(){
                selectedLeaderboardType = .MY_AGE_GROUP
                if leaderboardModelAgeGroup != nil {
                    self.lederboard_tableview.reloadData()
                }else {
                    self.getLeaderboardData(isAgeGroupData: true)
                }
            }else{
                var toast = getLocalizedString(key: "Join now for a customized view", comment: " ")
                self.view.makeToast(toast)
                segment_myage_leaderbaord.selectedSegmentIndex = 0
            }
            
        }
    }
    
    @IBAction func Segment_lederboard(_ sender: Any){
        let index = segment_lederboard.selectedSegmentIndex
        if index == 0 {
//            self.tableData = pastMonth
//            self.tableData = self.tableData!.sorted { $0.rank < $1.rank }
            self.selectedSegmentForLeaderboard = .PAST_MONTH
            

        }else if index == 1 {
            self.selectedSegmentForLeaderboard = .PAST_YEAR
            


        }else{
            self.selectedSegmentForLeaderboard = .PAST_3YEAR

        }
        self.lederboard_tableview.reloadData()
    }
    //     MARK: - IB Actions
    @IBAction func sortButtonPressed(_ sender: Any) {
        openBottomActionSheetForSorting()
    }
    
    
    @IBAction func graphBtn_pressed(_ sender: Any) {
        guard let graphVC = self.storyboard?.instantiateViewController(withIdentifier: "graph_vc") as? GraphVC else {
            return
        }
        self.navigationController?.pushViewController(graphVC, animated: true)
    }
    
    @IBAction func portfolio_pressed(_ sender: Any) {
        Main_viewLibrary.isHidden = true
        //media_btn.titleLabel?.fontSize = 15
        //keyDocs_btn.titleLabel?.fontSize = 15
        //deepDives_btn.titleLabel?.fontSize = 15
        documents_view.isHidden = true
        lederboard_view.isHidden = true
        documents_view!.heightConstaint?.constant = 0
        summary_view.isHidden = false
        future_prediction_View.isHidden = true
        
        documents_btn.titleLabel?.fontSize = 13
        leaderboard_btn.titleLabel?.fontSize = 13
        futureprediction_btn.titleLabel?.fontSize = 13
        portfolio_btn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 15)
        
        //portfolio_btn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
    }
    
    @IBAction func documents_pressed(_ sender: Any) {
        documents_view.isHidden = false
        lederboard_view.isHidden = true
        documents_view!.heightConstaint?.constant = 40
        future_prediction_View.isHidden = true
        
        summary_view.isHidden = true
        Main_viewLibrary.isHidden = false
        portfolio_btn.titleLabel?.fontSize = 13
        leaderboard_btn.titleLabel?.fontSize = 13
        futureprediction_btn.titleLabel?.fontSize = 13
        documents_btn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 15)
        
    }
    
    @IBAction func futurepredection_pressed(_ sender: Any) {
        documents_view!.heightConstaint?.constant = 0
        
        portfolio_btn.titleLabel?.fontSize = 13
        leaderboard_btn.titleLabel?.fontSize = 13
        documents_btn.titleLabel?.fontSize = 13
        futureprediction_btn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 15)
        documents_view.isHidden = true
        Main_viewLibrary.isHidden = true
        lederboard_view.isHidden = true
        summary_view.isHidden = true
        
        future_prediction_View.isHidden = false
        
        setAnalyticsAction(ScreenName: "PortfolioVC", method: "FuturePrediction")
    }
    
    @IBAction func investmentstrategy_Pressed(_ sender: Any) {
        InvestmentStrategyVars.isFromAccountVC = true
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "HomeSurveyVC") as! HomeSurveyVC
        self.navigationController?.pushViewController(vc, animated: true)
        setAnalyticsAction(ScreenName: "PortfolioVC", method: "investmentStrategy")
        
    }
    @IBAction func leaderboard_pressed(_ sender: Any) {
        documents_view.isHidden = true
        lederboard_view.isHidden = false
        documents_view!.heightConstaint?.constant = 0
        future_prediction_View.isHidden = true
        
        portfolio_btn.titleLabel?.fontSize = 13
        futureprediction_btn.titleLabel?.fontSize = 13
        documents_btn.titleLabel?.fontSize = 13
        leaderboard_btn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 15)
        setAnalyticsAction(ScreenName: "PortfolioVC", method: "leaderboard")
    }
    
    @IBAction func backtest_pressed(_ sender: Any) {
        
        segment1_view.isHidden = true
        segment2_view.isHidden = true
        segment3_view.isHidden = true
        Main_viewLibrary.isHidden = false
        backtest_View.isHidden = false
        // portfolio_btn.titleLabel?.fontSize = 15
        
        deepDives_btn.titleLabel?.fontSize = 15
        keyDocs_btn.titleLabel?.fontSize = 15
        media_btn.titleLabel?.fontSize = 15
        backtest_btn.setTitleColor(#colorLiteral(red: 0.3098039216, green: 0.6156862745, blue: 0.8666666667, alpha: 1), for: .normal)
        keyDocs_btn.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)

        backtest_btn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        setAnalyticsAction(ScreenName: "PortfolioVC", method: "backtest")
    }
    @IBAction func media_pressed(_ sender: Any) {
        segment1_view.isHidden = true
        segment2_view.isHidden = true
        segment3_view.isHidden = false
        Main_viewLibrary.isHidden = false
        backtest_View.isHidden = true

        // portfolio_btn.titleLabel?.fontSize = 15
        
        deepDives_btn.titleLabel?.fontSize = 15
        keyDocs_btn.titleLabel?.fontSize = 15
        backtest_btn.titleLabel?.fontSize = 15

        media_btn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        setAnalyticsAction(ScreenName: "PortfolioVC", method: "media")
    }
    @IBAction func KeyDocs(_ sender: Any) {
        segment1_view.isHidden = true
        segment2_view.isHidden = false
        segment3_view.isHidden = true
        backtest_View.isHidden = true

        Main_viewLibrary.isHidden = false
        // portfolio_btn.titleLabel?.fontSize = 15
        
        deepDives_btn.titleLabel?.fontSize = 15
        media_btn.titleLabel?.fontSize = 15
        backtest_btn.titleLabel?.fontSize = 15
       keyDocs_btn.setTitleColor(#colorLiteral(red: 0.3098039216, green: 0.6156862745, blue: 0.8666666667, alpha: 1), for: .normal)
        backtest_btn.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)

        keyDocs_btn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        keyDocs_btn.backgroundColor = UIColor.clear
        setAnalyticsAction(ScreenName: "PortfolioVC", method: "keyDocs")
    }
    @IBAction func portfolio_btn(_ sender: Any) {
        //summary_view.isHidden = true
        Main_viewLibrary.isHidden = true
        media_btn.titleLabel?.fontSize = 15
        keyDocs_btn.titleLabel?.fontSize = 15
        deepDives_btn.titleLabel?.fontSize = 15
        portfolio_btn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        setAnalyticsAction(ScreenName: "PortfolioVC", method: "portfolioBtn")
        
    }
    @IBAction func Deepdive_btn(_ sender: Any) {
        segment1_view.isHidden = true
        segment2_view.isHidden = false
        segment3_view.isHidden = true
        Main_viewLibrary.isHidden = false
        backtest_View.isHidden = true

        // portfolio_btn.titleLabel?.fontSize = 15
        
        media_btn.titleLabel?.fontSize = 15
        keyDocs_btn.titleLabel?.fontSize = 15
        backtest_btn.titleLabel?.fontSize = 15

        deepDives_btn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        setAnalyticsAction(ScreenName: "PortfolioVC", method: "DeepDives")
    }
    //     MARK: - Segment Actions
    @IBAction func segmentIndexChanged(_ sender: Any) {
        switch topbarSegmentControl.selectedSegmentIndex
        {
        case 0:
            tableview_summary.reloadData()
            summary_view.isHidden = false
            history_view.isHidden = true
            details_view.isHidden = true
            future_predictions_view.isHidden = true
        case 1:
            historyTableview.reloadData()
            summary_view.isHidden = true
            history_view.isHidden = false
            details_view.isHidden = true
            future_predictions_view.isHidden = true
        case 2:
            tableview_details.reloadData()
            summary_view.isHidden = true
            history_view.isHidden = true
            details_view.isHidden = true
            getFuturePredctions()
            self.futurePredictionsTableView.reloadData()
            future_predictions_view.isHidden = false
        default:
            print("default case")
        }
    }
    //     MARK: - Chart data actions
    
    @IBAction func loadDayData(_ sender: Any) {
        
//        if ArrayLists.sharedInstance.portfolioWeekChartDataList.count == 0 {
//            self.view.makeToast(getLocalizedString(key: "no_data"))
//            return
//
//        }
        
        // Data.reload()
        lineChartView.data?.notifyDataChanged()
        lineChartView.notifyDataSetChanged()
        
        self.Day_lbl.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)

        self.Day_lbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.Week_lbl.setTitleColor(#colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1), for: .normal)
        self.month_lbl.setTitleColor(#colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1), for: .normal)
        self.year_btn.setTitleColor(#colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1), for: .normal)
        self.All_btn.setTitleColor(#colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1), for: .normal)
        
        
        self.Week_lbl.backgroundColor = UIColor.clear
        self.month_lbl.backgroundColor = UIColor.clear
        self.All_btn.backgroundColor = UIColor.clear
        self.year_btn.backgroundColor = UIColor.clear

        let data = get5DayChartData()
        setChart(values: data.vesgoData, values2: data.benchMarkData)
        lineChartView.highlightValue(nil)
        highlighterSV.isHidden = true
        setAnalyticsAction(ScreenName: "PortfolioVC", method: "loadDayData")
        
    }
    @IBAction func loadWeekData(_ sender: Any) {
//        if ArrayLists.sharedInstance.portfolioWeekChartDataList.count == 0 {
//            self.view.makeToast(getLocalizedString(key: "no_data"))
//            return
//        }
        lineChartView.data?.notifyDataChanged()
        lineChartView.notifyDataSetChanged()
        selectedChartInterval = "week"

        chartdata = 20
        
        let data = get5DayChartData()
        setChart(values: data.vesgoData, values2: data.benchMarkData)
        lineChartView.highlightValue(nil)
        highlighterSV.isHidden = true
        setAnalyticsAction(ScreenName: "PortfolioVC", method: "loadWeekData")
        
        self.Week_lbl.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        
        self.Week_lbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.Day_lbl.setTitleColor(#colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1), for: .normal)
        self.month_lbl.setTitleColor(#colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1), for: .normal)
        self.year_btn.setTitleColor(#colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1), for: .normal)
        self.All_btn.setTitleColor(#colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1), for: .normal)
        
        
        self.Day_lbl.backgroundColor = UIColor.clear
        self.month_lbl.backgroundColor = UIColor.clear
        self.All_btn.backgroundColor = UIColor.clear
        self.year_btn.backgroundColor = UIColor.clear
    }
    @IBAction func loadMonthData(_ sender: Any) {
//        if ArrayLists.sharedInstance.portfolioMonthChartDataList.count == 0 {
//            self.view.makeToast(getLocalizedString(key: "no_data"))
//            return
//        }
        lineChartView.data?.notifyDataChanged()
        lineChartView.notifyDataSetChanged()
        
        
        self.month_lbl.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        
        self.month_lbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.Day_lbl.setTitleColor(#colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1), for: .normal)
        self.Week_lbl.setTitleColor(#colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1), for: .normal)
        self.year_btn.setTitleColor(#colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1), for: .normal)
        self.All_btn.setTitleColor(#colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1), for: .normal)
        
        
        self.Day_lbl.backgroundColor = UIColor.clear
        self.Week_lbl.backgroundColor = UIColor.clear
        self.All_btn.backgroundColor = UIColor.clear
        self.year_btn.backgroundColor = UIColor.clear
        
        
        selectedChartInterval = "month"
        chartdata = 5
        
        let data = getMonthChartData()
        setChart(values: data.vesgoData, values2: data.benchMarkData)
        lineChartView.highlightValue(nil)
        highlighterSV.isHidden = true
        setAnalyticsAction(ScreenName: "PortfolioVC", method: "loadMonthData")
    }
    @IBAction func loadYearData(_ sender: Any) {
//        if ArrayLists.sharedInstance.portfolioYearChartDataList.count == 0 {
//            self.view.makeToast(getLocalizedString(key: "no_data"))
//            return
//        }
        lineChartView.data?.notifyDataChanged()
        lineChartView.notifyDataSetChanged()
        
        self.year_btn.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        
        self.year_btn.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.Day_lbl.setTitleColor(#colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1), for: .normal)
        self.Week_lbl.setTitleColor(#colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1), for: .normal)
        self.month_lbl.setTitleColor(#colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1), for: .normal)
        self.All_btn.setTitleColor(#colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1), for: .normal)
        
        
        self.Day_lbl.backgroundColor = UIColor.clear
        self.Week_lbl.backgroundColor = UIColor.clear
        self.All_btn.backgroundColor = UIColor.clear
        self.month_lbl.backgroundColor = UIColor.clear

        chartdata = 20
        let data = getYearChartData()
        setChart(values: data.vesgoData, values2: data.benchMarkData)
        lineChartView.highlightValue(nil)
        highlighterSV.isHidden = true
        setAnalyticsAction(ScreenName: "PortfolioVC", method: "YearData")
    }
    @IBAction func loadAllData(_ sender: Any) {
//        if ArrayLists.sharedInstance.portfolioallChartDataList.count == 0 {
//            self.view.makeToast(getLocalizedString(key: "no_data"))
//            return
//        }
        lineChartView.data?.notifyDataChanged()
        lineChartView.notifyDataSetChanged()
        
        
        
        self.All_btn.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        
        self.All_btn.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
        self.Day_lbl.setTitleColor(#colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1), for: .normal)
        self.Week_lbl.setTitleColor(#colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1), for: .normal)
        self.month_lbl.setTitleColor(#colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1), for: .normal)
        self.year_btn.setTitleColor(#colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1), for: .normal)
        
        
        self.Day_lbl.backgroundColor = UIColor.clear
        self.Week_lbl.backgroundColor = UIColor.clear
        self.year_btn.backgroundColor = UIColor.clear
        self.month_lbl.backgroundColor = UIColor.clear
        
        
        chartdata = 20
        let data = getMaxChartData()
        setChart(values: data.vesgoData, values2: data.benchMarkData)
        lineChartView.highlightValue(nil)
        highlighterSV.isHidden = true
        setAnalyticsAction(ScreenName: "PortfolioVC", method: "AllData")
    }
    
    
    //MARK: -Future Predictions Api
    func getFuturePredctions() {
        self.view.activityStartAnimating()
        let url = ApiUrl.GET_PORTFOLIO_FUTURE_PREDICTIONS_URL
        let namespace = SPManager.sharedInstance.getCurrentUserNamespace()

        let params:Dictionary<String,AnyObject> = ["namespace" : namespace as AnyObject]
        APIManager.sharedInstance.postRequest(serviceName: url, sendData: params, success: { (response) in
            print(response)
            
            if APIManager.sharedInstance.isSuccess {
                let res = response["message"] as AnyObject
                DispatchQueue.main.async {
                    
                    self.fetchFuturePredictionData(jsonData: res as! [[String : AnyObject]])
                    self.futurePredictionsTableView.reloadData()
                }
                
            }else{
                if response["message"] != nil {
                    DispatchQueue.main.async {
                        self.showAlert(response["message"] as! String)
                    }
                } else {
                    DispatchQueue.main.async {
                        self.showAlert(ConstantStrings.NO_RESULT_FOUND_ALERT_TEXT)
                    }
                }
            }
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
        }, failure: { (data) in
            
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
            if APIManager.sharedInstance.error400 {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
            }
            if(APIManager.sharedInstance.error401)
            {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
            }else{
                
            }
        })
    }
    func fetchFuturePredictionData(jsonData : [[String : AnyObject]]) {
        ArrayLists.sharedInstance.futurePredictionsList.removeAll()
        for data in jsonData {
            let plan = data["plan"] as! String
            let details = data["details"] as! [[String: AnyObject]]
            for planDetail in details {
                
                let item = FuturePredictionsModel()
                let confidence = planDetail["confidence_level"] as! Double
                let profit = planDetail["profit"] as! Double
                item.setConfidenceLevel(Int(confidence))
                item.setProfit(Int(profit))
                item.setPlan(plan)
                if plan == "0 years" {
                    
                } else {
                    if plan == "5 years" {
                        ArrayLists.sharedInstance.futurePredictionsTextualList.append(item)
                    }
                    ArrayLists.sharedInstance.futurePredictionsList.append(item)
                }
            }
            
        }
    }
    
    // MARK: - Navigation getsummarydata
    func getSummaryData() {
        if !SPManager.sharedInstance.saveUserLoginStatus() {
            return
        }
        self.view.activityStartAnimating()
        let url = ApiUrl.GET_USER_PORTFOLIO_SUMMARY_DATA_URL
        let namespace = SPManager.sharedInstance.getCurrentUserNamespace()
        
        
        let data:Dictionary<String,AnyObject> = [:]
        //test6-MecY
        
        let params:Dictionary<String,AnyObject> = ["namespace" : namespace as AnyObject, "data" : data as AnyObject]
       
        APIManager.sharedInstance.postRequest(serviceName: url, sendData: params, success: { (response) in
            print(response)
            
            if APIManager.sharedInstance.isSuccess {
                if response["Success"] as! Bool {
                    let res = response["message"] as AnyObject
                    
                    DispatchQueue.main.async {
                          //self.summary_view.isHidden = false
                        //                    QuestionnaireParser.sharedInstance.fetchMetaData(json: res as! [String : AnyObject], table_view: self.tableView)
//                        self.usd_lbl.isHidden = false
                        self.Btns_Day_Week_all_view.isHidden = false
                        self.chartDataNotAvailable.isHidden = true

                        self.fetchMetaData(jsonData: res as! [String : AnyObject], table_view: self.tableview_summary)
                    }
                }else{
                    DispatchQueue.main.async {
                        self.usd_lbl.isHidden = true
                        self.Btns_Day_Week_all_view.isHidden = true
                        self.chartDataNotAvailable.isHidden = true
                    }
                }
                
                DispatchQueue.main.async {
                    self.compalinDetails_lbl.isHidden = false
                }
                
                
            }else{
                if response["message"] != nil {
                    DispatchQueue.main.async {
                        self.showAlert(response["message"] as! String)
                    }
                } else {
                    DispatchQueue.main.async {
                        self.showAlert(ConstantStrings.NO_RESULT_FOUND_ALERT_TEXT)
                    }
                }
            }
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
        }, failure: { (data) in
           
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
            if APIManager.sharedInstance.error400 {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
            }
            if(APIManager.sharedInstance.error401)
            {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
            }else{
                
            }
        })
    }
    
    // MARK: - fetchData
    func fetchMetaData(jsonData : [String : AnyObject], table_view : UITableView) {
        ArrayLists.sharedInstance.portfolioSummaryDataList.removeAll()
        ArrayLists.sharedInstance.portfolioHistoryDataList.removeAll()
        ArrayLists.sharedInstance.portfolioDetailsDataList.removeAll()
        
        //let companiesData = jsonData["categories_data"] as! [[String : AnyObject]]
        let chartData = jsonData["chart_data"] as! [String : AnyObject]
        let benchmarkChartData = jsonData["benchmark_chart_data"] as! [String : AnyObject]
        //let historyData = jsonData["history"] as! [[String : AnyObject]]
        //let detailsData = jsonData["details"] as! [[String : AnyObject]]
        
//        for jsonData in companiesData {
//            let model = PortfolioDataModel()
//            let companyName = jsonData["name"] as! String
//            let companyValue = jsonData["value"] as! Double
//            let childCompanies = jsonData["companies"] as! [String]
//            //print("companyValue== \(companyValue)")
//            model.setCategoryValue(value: companyValue)
//            model.setCategoryName(name: companyName)
//            model.childCompaniesList = childCompanies
//
//            ArrayLists.sharedInstance.portfolioSummaryDataList.append(model)
//        }
        //Mark: - parse app chart data
       
        //let dayData = chartData["day_data"] as! [String : AnyObject]
        let weekData = chartData["week_data"] as! [String : AnyObject]
        let monthData = chartData["month_data"] as! [String : AnyObject]
        let yearData = chartData["year_data"] as! [String : AnyObject]
        let allData = chartData["all_data"] as! [String : AnyObject]
        
        //Mark: - parse benchmark chart data
        
        //let benchmarkTitle = benchmarkChartData["title"] as! String
        let benchmarkTitle = "S&P 500"
            self.benchMarkTitle = benchmarkTitle
        //let benchmarkDayData = benchmarkChartData["day_data"] as! [String : AnyObject]
        let benchmarkWeekData = benchmarkChartData["week_data"] as! [String : AnyObject]
        let benchmarkMonthData = benchmarkChartData["month_data"] as! [String : AnyObject]
        let benchmarkYearData = benchmarkChartData["year_data"] as! [String : AnyObject]
        let benchmarkallData = benchmarkChartData["all_data"] as! [String : AnyObject]
        
        //Mark: - add app chart data into lists
//        print(dayData)
//        print(weekData)
//        print(monthData)
//        print(yearData)
        //ArrayLists.sharedInstance.portfolioDayChartDataList = parseChartJsonData(data: dayData)
        ArrayLists.sharedInstance.portfolioWeekChartDataList = parseChartJsonData(data: weekData)
        ArrayLists.sharedInstance.portfolioMonthChartDataList = parseChartJsonData(data: monthData)
        ArrayLists.sharedInstance.portfolioYearChartDataList = parseChartJsonData(data: yearData)
        ArrayLists.sharedInstance.portfolioallChartDataList = parseChartJsonData(data: allData)
        
        //Mark: - get benchmark data
        //ArrayLists.sharedInstance.portfolioBenchmarkDayChartDataList = parseChartJsonData(data: benchmarkDayData)
        ArrayLists.sharedInstance.portfolioBenchmarkWeekChartDataList = parseChartJsonData(data: benchmarkWeekData)
        ArrayLists.sharedInstance.portfolioBenchmarkMonthChartDataList = parseChartJsonData(data: benchmarkMonthData)
        ArrayLists.sharedInstance.portfolioBenchmarkYearChartDataList = parseChartJsonData(data: benchmarkYearData)
        ArrayLists.sharedInstance.portfolioBenchmarkallChartDataList = parseChartJsonData(data: benchmarkallData)
//        ArrayLists.sharedInstance.portfolioHistoryDataList = parseHistoryJsonData(data: historyData)
//        ArrayLists.sharedInstance.portfolioDetailsDataList = parseDetailsJsonData(data: detailsData)
        DispatchQueue.main.async {
            table_view.reloadData()
//            self.getvalues(list: ArrayLists.sharedInstance.portfolioWeekChartDataList, bechmarklist: ArrayLists.sharedInstance.portfolioBenchmarkWeekChartDataList)
            
            //            self.getvalues(list: ArrayLists.sharedInstance.portfolioWeekChartDataList, bechmarklist: ArrayLists.sharedInstance.portfolioBenchmarkDayChartDataList )
        }
        
    }
    
    func parseChartJsonData(data : [String : AnyObject]) -> [PortfolioDataModel] {
        var list : [PortfolioDataModel] = [PortfolioDataModel]()
        let lastRefreshedInterval = data["last_refresh"] as! Int
        let chartData = data["data"] as! [[String : AnyObject]]
        for jsonData in chartData {
            let model = PortfolioDataModel()
            let xAxis = jsonData["x_axis"] as! String
            let yAxis = jsonData["y_axis"] as! Double
            model.setDataInterval(interval:"year_data")
            model.setDataXaxisValue(value: xAxis)
            model.setDataYaxisValue(value: yAxis)
            model.setLastRefreshedTime(value: lastRefreshedInterval)
            list.append(model)
        }
        return list
    }
    /// libaray data
    
    
    @objc func viewDiscalimerTapped() {
        viewPdfFile(urlString: dis,urlType: "")
    }
    
    //Mark: - parse portfolio history data
    func parseHistoryJsonData(data : [[String : AnyObject]]) -> [PortfolioDataModel] {
        var list : [PortfolioDataModel] = [PortfolioDataModel]()
        for jsonData in data {
            let model = PortfolioDataModel()
            let keyName = jsonData["name"] as! String //current_year
            let keyValue = jsonData["value"] as! String //2019
            let appName = jsonData["app_name"] as! String //fintech
            let appValue = jsonData["app_value"] as! Double //-7.5
            let competitorName = jsonData["competitor_name"] as! String //s&p
            let competitorValue = jsonData["competitor_value"] as! Double //5.0
            self.historyAppName_lbl.text = appName
            self.historyCompetitorname_lbl.text = competitorName
            model.setKeyName(name: keyName)
            model.setKeyValue(value: keyValue)
            model.setAppName(name: appName)
            model.setAppValue(value: appValue)
            model.setCompetitorName(name: competitorName)
            model.setCompetitorValue(value: competitorValue)
            list.append(model)
        }
        return list
    }
    // Mark: - parse portfolio details data
    func parseDetailsJsonData(data : [[String : AnyObject]]) -> [NotificationDataModel] {
        
        var list : [NotificationDataModel] = [NotificationDataModel]()
        for jsonData in data {
            let model = NotificationDataModel()
            let mainHeading = jsonData["main_heading"] as! String
            let subHeading = jsonData["subheading"] as! String
            let description = jsonData["description"] as! String
            let iconUrl = jsonData["icon_url"] as! String
            //            let progressValue = jsonData["progress_value"] as! Double //umair is changing string-> float
            let dataType = jsonData["type"] as! String
            let timestamp = jsonData["eve_sec"] as! Double
            
            model.setMainHeadingText(text: mainHeading)
            model.setSubHeadingText(text: subHeading)
            model.setDescriptionText(text: description)
            model.setIconImage(text: iconUrl)
            model.setProgressValue(text: 2.3)
            model.setDataType(text: dataType)
            model.setUnixTimestamp(timestamp: timestamp)
            
            if jsonData["additional_info"] as? [String : AnyObject] != nil {
                let additionalInfo = jsonData["additional_info"] as! [String : AnyObject]
                //Mark: - parse additional info
                let infoType = additionalInfo["type"] as! String
                model.setInfoType(type: infoType)
                if infoType == "text" {
                    let infoValue = additionalInfo["value"] as! [String : AnyObject]
                    let infoHeading = infoValue["info_heading"] as! String
                    let infoSubHeading = infoValue["info_sub_heading"] as! String
                    let infoDescription = infoValue["info_description"] as! String
                    let infoDisclaimerText = infoValue["info_disclaimer_text"] as! String
                    let infoDisclaimerLink = infoValue["info_disclaimer_link"] as! String
                    
                    
                    model.setInfoHeading(text: infoHeading)
                    model.setInfoSubHeading(text: infoSubHeading)
                    model.setInfoDescription(text: infoDescription)
                    model.setInfoDisclaimerText(text: infoDisclaimerText)
                    model.setInfoDisclaimerLink(text: infoDisclaimerLink)
                    //                model.setInfoDisclaimerLinkType(text: infoDisclaimerLinkType)
                } else {
                    let infoValue = additionalInfo["value"] as! String
                    model.setInfoValue(value: infoValue)
                }
            } else {
                model.setInfoType(type: "none")
            }
            
            
            list.append(model)
        }
        return list
    }
}

extension PortfolioVC : UITableViewDelegate, UITableViewDataSource {
    // MARK: - TableView Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == self.tableview_summary {
            // return ArrayLists.sharedInstance.portfolioSummaryDataList.count
            return (tickertabledta?[section].companies?.count ?? 0)+1
            
        } else if tableView == self.historyTableview {
            return ArrayLists.sharedInstance.portfolioHistoryDataList.count
        } else if tableView == tableview_details {
            return ArrayLists.sharedInstance.portfolioDetailsDataList.count
        } else if tableView == futurePredictionsTableView {
            return ArrayLists.sharedInstance.futurePredictionsTextualList.count
        }else if tableView == deepDivesTableview {
            return ArrayLists.sharedInstance.LibraryMainDataList.count
        } else if tableView == keyDocsTableview {
            return ArrayLists.sharedInstance.LibraryDocsDataList.count
        } else if tableView == meditableView {
            return ArrayLists.sharedInstance.LibraryMediaDataList.count
        }else if tableView == lederboard_tableview{
            //return self.pasttableDate?.count ?? 0
            var leaderboardModel: LeaderboardModel?
            if selectedLeaderboardType == .ALL {
                leaderboardModel = self.leaderboardModel
            }else {
                leaderboardModel = self.leaderboardModelAgeGroup
            }
            if leaderboardModel != nil {
                if selectedSegmentForLeaderboard == .PAST_YEAR {
                    leaderboardData = leaderboardModel?.data.pastYear.sorted {
                        $0.rank < $1.rank
                    }
                    return leaderboardModel?.data?.pastYear?.count ?? 0
                }else if selectedSegmentForLeaderboard == .PAST_3YEAR {
                    leaderboardData = leaderboardModel?.data.past3Year.sorted {
                        $0.rank < $1.rank
                    }
                    return leaderboardModel?.data?.past3Year?.count ?? 0
                }else{
                    leaderboardData = leaderboardModel?.data.pastMonth.sorted {
                        $0.rank < $1.rank
                    }
                    return leaderboardModel?.data?.pastMonth?.count ?? 0
                }
            }else {
                return 0
            }
            //return self.tableData?.count ?? 0
            
        }else if tableView == bactest_Tableview{
            if self.backtestModel == nil {
                return 0
            }
            return self.backtestModel.data.performance.count
        }
        return 0
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == self.tableview_summary {
            return tickertabledta?.count ?? 0
            
        }
        return 1
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       // print("selected index == \(indexPath.row)")
      
        if tableView == tableview_summary{
            let item = tickertabledta?[indexPath.section]
            if indexPath.row > 0  {
                let item2 = item?.companies?[indexPath.row-1]
                self.getTickerSearchData(tickerName: item2!.ticker!)
            }
            //let Cell2 =  self.tableview_summary.dequeueReusableCell(withIdentifier: "cell2") as! PortfolioSummaryCell
            
            
            
        }else if tableView == deepDivesTableview {
            let item = ArrayLists.sharedInstance.LibraryMainDataList[indexPath.row]
            infoType = item.getInfoType()
            
            infoValue = item.getInfoValue()
            dis = item.getInfoDisclaimerLink()
            self.viewDiscalimerTapped()
        } else if tableView == keyDocsTableview {
            let item = ArrayLists.sharedInstance.LibraryDocsDataList[indexPath.row]
            infoType = item.getInfoType()
            infoValue = item.getInfoValue()
           
             self.datadiscripation()
        } else if tableView == meditableView{
            let item = ArrayLists.sharedInstance.LibraryMediaDataList[indexPath.row]
            infoType = item.getInfoType()
            infoValue = item.getInfoValue()
        }else if tableView == lederboard_tableview{
            
            let storyboard = UIStoryboard(name: "InvestmentDetails", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "LeaderboardDetailsNewVC") as! LeaderboardDetailsNewVC
            vc.userLeaderboardData = leaderboardData[indexPath.row]
            navigationController?.pushViewController(vc, animated: true)
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == tableview_summary {
            let item = tickertabledta?[indexPath.section]
            
            let summaryCell =  self.tableview_summary.dequeueReusableCell(withIdentifier: "PortfolioSummaryCell") as! PortfolioSummaryCell
            
            if indexPath.row == 0{
                summaryCell.ticker_name_lbl.text = "Assets"
                summaryCell.ticker_value_lbl.text = "(\(String(item?.value ?? 0))%)"
                summaryCell.amount_lbl.text = "$\(FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: (item?.amount)!))"
                return summaryCell
            }
            let item2 = item?.companies?[indexPath.row-1]
            let Cell2 =  self.tableview_summary.dequeueReusableCell(withIdentifier: "cell2") as! PortfolioSummaryCell
            
            Cell2.ticker_lbl.text = item2?.ticker ?? ""
            Cell2.ticker_title_lbl.text = item2?.tickerTitle ?? ""
            
            Cell2.ticker_share_lbl.text = "\(String(item2?.tickerShare ?? 0))%"
            Cell2.tickerAmount_lbl.text = "$\(FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: item2!.tickerAmmount))"
            return Cell2
            
            // let item = ArrayLists.sharedInstance.portfolioSummaryDataList[indexPath.row]
            
            
            
            
            
            //summaryCell.label1.text = item.getCategoryName()
            //            summaryCell.label1.text = item?.name ?? ""
            ////            summaryCell.details_lbl.text = tickerTitle[indexPath.row]
            ////            summaryCell.ticker_share_lbl.text = tickershare[indexPath.row]
            //            summaryCell.label2.text = item?.companies?[0].ticker ?? ""
            //            summaryCell.label3.text = String(item?.value ?? 0.0)
            //            summaryCell.details_lbl.text = item?.companies?[0].tickerTitle ?? ""
            //            summaryCell.ticker_share_lbl.text = String(item?.companies?[0].tickerShare ?? 0)
            var companiesList : String = ""
            //            for a in item.childCompaniesList {
            //                companiesList += a + " "
            //            }
            
            
            //// add cheak
            if SPManager.sharedInstance.getUserPortfolioStatus() {
                print("user portfolio is complete")
                
                
                let tickerText = String(companiesList.dropLast())
                if tickerText == portfolioTicker.GENERAL_TICKER_Text {
                    
                }
                else if tickerText == portfolioTicker.AAPl_Ticker_Text{
                }
                
                
                //summaryCell.label2.text = String(companiesList.dropLast())
            } else {
                print("user portfolio is not complete")
                sample_portfolio_lbl.isHidden = false
                // let t = tableview
                sample_portfolio_lbl.heightConstaint?.constant = 25
                let tickerText = String(companiesList.dropLast())
                
                if tickerText == portfolioTicker.GENERAL_TICKER_Text {
                }
                else if tickerText == portfolioTicker.AAPl_Ticker_Text{
                    //  summaryCell.label2.backgroundColor = portfolioTickerColor.MSFT_Ticker_color
                }
                //summaryCell.label2.text = String(companiesList.dropLast())
                
                //                summaryCell.label2.text = "SHY, QQQ, BND"
            }
            
            //  print("value== \(String(describing: item.getCategoryValue()) + "%")")
            //   summaryCell.label3.text = String(describing: item.getCategoryValue()) + "%"
            
            //return summaryCell
        } else if tableView == historyTableview {
            let historyCell =  self.historyTableview.dequeueReusableCell(withIdentifier: "PortfolioHistoryCell") as! PortfolioHistoryCell
            
            let item = ArrayLists.sharedInstance.portfolioHistoryDataList[indexPath.row]
            historyCell.label_a.text = item.getKeyValue()
            historyCell.label_b.text = "$" + String(describing: item.getAppValue())
            historyCell.label_c.text = "$" + String(describing: item.getCompetitorValue())
            
            return historyCell
        }else if tableView == self.futurePredictionsTableView {
            let futurPredictionsCell = self.futurePredictionsTableView.dequeueReusableCell(withIdentifier: "future_preditions_cell") as! FuturePredictionsCell
            
            let item = ArrayLists.sharedInstance.futurePredictionsTextualList[indexPath.row]
            let plan = item.getPlan()
            let confidence = item.getConfidenceLevel()
            let profit = item.getProfit()
            
            if plan == "5 years" {
                futurPredictionsCell.confidence_lbl.text = "$" + String(describing: profit)
                let a : Float = Float(((profit - 10000)))
                let b : Float = Float(a / 10000)
                let c : Float = Float(b * 100)
                futurPredictionsCell.profit_lbl.text = String(describing: c) + "%"
                futurPredictionsCell.plan_lbl.text =  String(describing: confidence) + "%"
                return futurPredictionsCell
            } else {
                return UITableViewCell()
            }
            
        } else if tableView == self.tableview_details{
            let detailsCell =  self.tableview_details.dequeueReusableCell(withIdentifier: "PortfolioDetailsCell") as! PortfolioDetailsCell
            let item = ArrayLists.sharedInstance.portfolioDetailsDataList[indexPath.row]
            detailsCell.subHeading_lbl.text = item.getSubHeadingText()
            detailsCell.mainHeading_lbl.text = item.getMainHeadingText()
            detailsCell.description_lbl.text = item.getDescriptionText()
            
            return detailsCell
        }else if tableView == deepDivesTableview {
            let deepDiveCell =  self.deepDivesTableview.dequeueReusableCell(withIdentifier: "DeepDivesCell") as! DeepDivesCell
            let item = ArrayLists.sharedInstance.LibraryMainDataList[indexPath.row]
            deepDiveCell.heading_lbl.text = item.getMainHeadingText()
            deepDiveCell.description_lbl.text = item.getDescriptionText()
            deepDiveCell.Image_deepDives_img.layer.cornerRadius = 10
            deepDiveCell.Image_deepDives_img.clipsToBounds = true
            return deepDiveCell
        } else if tableView == keyDocsTableview {
            let keyDocsCell =  self.keyDocsTableview.dequeueReusableCell(withIdentifier: "KeyDocsCell") as! DeepDivesCell
            let item = ArrayLists.sharedInstance.LibraryDocsDataList[indexPath.row]
            keyDocsCell.heading_lbl.text = item.getMainHeadingText()
            keyDocsCell.description_lbl.text = item.getDescriptionText()
            return keyDocsCell
        } else if tableView == meditableView {
            let mediaCell =  self.meditableView.dequeueReusableCell(withIdentifier: "MediaCell") as! DeepDivesCell
            let item = ArrayLists.sharedInstance.LibraryMediaDataList[indexPath.row]
            mediaCell.heading_lbl.text = item.getMainHeadingText()
            mediaCell.description_lbl.text = item.getDescriptionText()
            
            if item.getInfoType() == "video" {
                let url = URL(string: item.getInfoValue())
                if let thumbnailImage = getThumbnailImage(forUrl: url!) {
                    mediaCell.Image_Media_img.image = thumbnailImage
                } else {
                    //set a place holder image in imageview
                }
            } else {
                // if only for video -- hide image vieww
            }
            
            return mediaCell
        }else if tableView == lederboard_tableview{
            
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! leaderBoardCell

//        let items = self.tableData?[indexPath.row]
//
//
//
//        cell.Heading_lbl.text = items?.portfolioTitle ?? ""
//
//        var pastreturn = Int((items?.pastReturn)!)
//
//
//
//        cell.Number_lbl.text = String(items?.rank ?? 0)
//        if indexPath.row  == 0 {
//            cell.icon_img.isHidden = false
//            cell.icon_img.image = UIImage(named: lederbordImgesarray[0])
//        }
//        else if indexPath.row  == 1 {
//            cell.icon_img.isHidden = false
//            cell.icon_img.image = UIImage(named: lederbordImgesarray[1])
//        }
//        else if indexPath.row  == 2 {
//            cell.icon_img.isHidden = false
//            cell.icon_img.image = UIImage(named: lederbordImgesarray[2])
//        }
//        else {
//            cell.icon_img.isHidden = true
//        }
//
//        if (items?.pastReturn?.contains("-"))!{
//            cell.presentage_lbl.text = items?.pastReturn ?? ""
//            cell.presentage_lbl.textColor = UIColor.red
//
//        } else if (items?.pastReturn?.contains("+"))!{
//            cell.presentage_lbl.text = items?.pastReturn ?? ""
//            cell.presentage_lbl.textColor = #colorLiteral(red: 0, green: 0.5607843137, blue: 0, alpha: 1)
//        }
        
            let items = self.leaderboardData[indexPath.row]
            cell.Heading_lbl.text = items.name
            cell.Number_lbl.text = String(items.rank)
            if indexPath.row  == 0 {
                cell.icon_img.isHidden = false
                cell.icon_img.image = UIImage(named: lederbordImgesarray[0])
            }
            else if indexPath.row  == 1 {
                cell.icon_img.isHidden = false
                cell.icon_img.image = UIImage(named: lederbordImgesarray[1])
            }
            else if indexPath.row  == 2 {
                cell.icon_img.isHidden = false
                cell.icon_img.image = UIImage(named: lederbordImgesarray[2])
            }
            else {
                cell.icon_img.isHidden = true
            }
            
            if (items.portfolioReturn.contains("-")){
                cell.presentage_lbl.text = items.portfolioReturn
                cell.presentage_lbl.textColor = UIColor.red
                
            } else if (items.portfolioReturn.contains("+")){
                cell.presentage_lbl.text = items.portfolioReturn
                cell.presentage_lbl.textColor = #colorLiteral(red: 0, green: 0.5607843137, blue: 0, alpha: 1)
            }
            
            
        return cell
    }
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! DeepDivesCell
        let items = self.backtestModel.data.performance[indexPath.row]
        
        cell.heading_backtest_lbl.text = items.portfolioName
        cell.return_backtest_lbl.text = FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: items.portfolioReturn)
        let returnAmount = self.backtestModel.data.backtest.amount
        let returnPercentage = items.portfolioReturn
        cell.return_amount_backtest_lbl.text = "$\(FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: ((returnAmount * returnPercentage) / 100.0)))" 
          return cell
    }
    
    func datadiscripation(){
        if infoType == "text" {
        }else if infoType == "video" {
            viewPdfFile(urlString: infoValue, urlType: infoType)
        } else if infoType == "none" {
            viewPdfFile(urlString: infoValue, urlType: infoType)
        }else if infoType == "pdf"{
            viewPdfFile(urlString: infoValue, urlType: infoType)
            
        }
    }
}
// MArk: - get library data

extension PortfolioVC {
    func getLibData() {
        DispatchQueue.main.async {
            self.view.activityStartAnimating()
        }
        let url = ApiUrl.GET_LIBRARY_DATA_URL
        let namespace = SPManager.sharedInstance.getCurrentUserNamespace()
        let data:Dictionary<String,AnyObject> = [:]
        
        let params:Dictionary<String,AnyObject> = ["namespace" : namespace as AnyObject, "data" : data as AnyObject]
      
        APIManager.sharedInstance.postRequest(serviceName: url, sendData: params, success: { (response) in
          
            
            if APIManager.sharedInstance.isSuccess {
                let res = response["message"] as AnyObject
                DispatchQueue.main.async {
                    
                    
                    
                    self.fetchMetaData(res: res as! [String : AnyObject], table_view: self.deepDivesTableview)
                }
                
            }else{
                if response["message"] != nil {
                    self.showAlert(response["message"] as! String)
                } else {
                    self.showAlert(ConstantStrings.NO_RESULT_FOUND_ALERT_TEXT)
                }
            }
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
        }, failure: { (data) in
           
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
            if APIManager.sharedInstance.error400 {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
            }
            if(APIManager.sharedInstance.error401)
            {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
            }else{
                
            }
        })
        
    }
    
    
    // MARK: - fetchData
    func fetchMetaData(res : [String : AnyObject], table_view : UITableView) {
        ArrayLists.sharedInstance.LibraryMainDataList.removeAll()
        ArrayLists.sharedInstance.LibraryDocsDataList.removeAll()
        ArrayLists.sharedInstance.LibraryMediaDataList.removeAll()
        
        //let mainData = res["main_data"] as! [[String : AnyObject]]
        let docsData = res["doc_data"] as! [[String : AnyObject]]
        //let mediaData = res["media_data"] as! [[String : AnyObject]]
        
        DispatchQueue.main.async {
            
           // ArrayLists.sharedInstance.LibraryMainDataList = self.parseJsonList(data: mainData)
            ArrayLists.sharedInstance.LibraryDocsDataList = self.parseJsonList(data: docsData)
            //ArrayLists.sharedInstance.LibraryMediaDataList = self.parseJsonList(data: mediaData)
        }
        
        DispatchQueue.main.async {
            self.keyDocsTableview.reloadData()
        }
        DispatchQueue.main.async {
            self.deepDivesTableview.reloadData()
        }
        
        DispatchQueue.main.async {
            self.meditableView.reloadData()
        }
    }
    func parseJsonList(data : [[String : AnyObject]]) -> [LibraryDataModel] {
        var list : [LibraryDataModel] = [LibraryDataModel]()
        for jsonData in data {
            let model = LibraryDataModel()
            var SELECTED_DATA_TYPE : String = ""
            let mainHeading = jsonData["main_heading"] as! String
            let subHeading = jsonData["subheading"] as! String
            //parse model scaling json
            let description = jsonData["description"] as! String
            let iconUrl = jsonData["icon_url"] as! String
            let progressValue = jsonData["progress_value"] as! Double
            let dataType = jsonData["type"] as! String
            let timestamp = jsonData["eve_sec"] as! Int
            
            if jsonData["additional_info"] as? [String : AnyObject] != nil {
                let additionalInfo = jsonData["additional_info"] as! [String : AnyObject]
                //parse additional info
                //            if additionalInfo != nil {
                let infoType = additionalInfo["type"] as! String
                
                model.setInfoType(type: infoType)
                if infoType == "text" {
                    let infoValue = additionalInfo["value"] as! [String : AnyObject]
                    let infoHeading = infoValue["info_heading"] as! String
                    let infoSubHeading = infoValue["info_sub_heading"] as! String
                    let infoDescription = infoValue["info_description"] as! String
                    let infoDisclaimerText = infoValue["info_disclaimer_text"] as! String
                    let infoDisclaimerLink = infoValue["info_disclaimer_link"] as! String
                    let infoDisclaimerLinkType = infoValue["info_disclaimer_link_type"] as! String
                    
                    model.setInfoHeading(text: infoHeading)
                    model.setInfoSubHeading(text: infoSubHeading)
                    model.setInfoDescription(text: infoDescription)
                    model.setInfoDisclaimerText(text: infoDisclaimerText)
                    model.setInfoDisclaimerLink(text: infoDisclaimerLink)
                    model.setInfoDisclaimerLinkType(text: infoDisclaimerLinkType)
                } else {
                    let infoValue = additionalInfo["value"] as! String
                    model.setInfoValue(value: infoValue)
                }
            } else {
                model.setInfoType(type: "none")
            }
            
            model.setMainHeadingText(text: mainHeading)
            model.setSubHeadingText(text: subHeading)
            model.setDescriptionText(text: description)
            model.setIconImage(text: iconUrl)
            model.setProgressValue(text: progressValue)
            model.setDataType(text: dataType)
            
            list.append(model)
        }
        
        return list
    }
    
    
    

    
}

extension PortfolioVC {
    //MARK : - lederboard api
    func leaderboardcallapi(){
        DispatchQueue.main.async {
            self.view.activityStartAnimating()
        }
        let url = ApiUrl.GET_Leaderboard_URL
        let namespace = SPManager.sharedInstance.getCurrentUserNamespace()
        postManagerUser(method: "POST", isThisURLEmbdedData: false, urlString: url, parameter: ["namespace": namespace], success: {(value) in
            do
            {
                let jsonObject = try JSONDecoder().decode(LeaderBoardModel?.self, from: value)
                DispatchQueue.main.async {
                    if let value = jsonObject?.message
                    {
                        if let value1 = value.pastMonth{
                            self.pastMonth = value1
                        }
                        if let value2 = value.pastYear{
                            self.pastYear = value2
                        }
                        
                        if let value3 = value.pastThreeYear{
                            self.pastThreeYear = value3
                        }
                        self.tableData = self.pastMonth
                       
                        DispatchQueue.main.async {
                      self.tableData = self.tableData!.sorted { $0.rank < $1.rank }
                        self.lederboard_tableview.reloadData()
                       }
                        DispatchQueue.main.async {
                            self.view.activityStopAnimating()
                        }
                        
                    }
                }
                
            }
            catch
            {
                DispatchQueue.main.async {
                    self.view.activityStopAnimating()
                }
            }
            
        }, failure: {(err) in
            
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
        })
    }
    
//    func sorterForFileIDASC(this:PastMonth, that:PastMonth) -> Bool {
//
//        return this.rank > that.rank
//
//    }
    
    
    
    
}
extension UIButton {
    func underline() {
        guard let text = self.titleLabel?.text else { return }
        let attributedString = NSMutableAttributedString(string: text)
        //NSAttributedStringKey.foregroundColor : UIColor.blue
        attributedString.addAttribute(NSAttributedString.Key.underlineColor, value: self.titleColor(for: .normal)!, range: NSRange(location: 0, length: text.count))
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: self.titleColor(for: .normal)!, range: NSRange(location: 0, length: text.count))
        attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: NSRange(location: 0, length: text.count))
        self.setAttributedTitle(attributedString, for: .normal)
    }
}
extension PortfolioVC {
    func stocksallocationcallapi(){
        let url = ApiUrl.GET_Stocks_Allocation_URL
      
        var namespace = ""
        var charset = CharacterSet(charactersIn: "gst-one1")
        
        if SPManager.sharedInstance.saveUserLoginStatus() {
            //if namespace.rangeOfCharacter(from: charset) != nil {
       
            namespace = SPManager.sharedInstance.getCurrentUserNamespace()
        } else {
         namespace = "gst-one1"
        }
        postManagerUser(method: "POST", isThisURLEmbdedData: false, urlString: url, parameter: ["namespace": namespace], success: {(value) in
            
            do
            {
                
                let jsonObject = try JSONDecoder().decode(TickerModel?.self, from: value)
                DispatchQueue.main.async {
                    if let value = jsonObject?.message
                    {
                        self.tickertabledta = value
                        for i in 0...(self.tickertabledta!.count-1) {
                            
                            self.tickertabledta![i].companies =  self.tickertabledta![i].companies!.sorted {
                                $0.tickerShare > $1.tickerShare
                                
                            }
                        }
                        
                        //                        self.tickertabledta = self.tickertabledta!.sorted{ { $0.companies?[0].tickerAmmount < $1.companies?[0].tickerAmmount }
                        
//                        self.tickertabledta?.sorted(by: { (this, that) -> Bool in
//                            return this.companies?[0].tickerAmmount > that.companies?[0].tickerAmmount
//                        })
//
//                        self.tickertabledta?.sort{ Self(rawValue: Self.RawValue(($0.companies?[0].tickerAmmount)!)) <  $1.companies?[0].tickerAmmount }


                        
                            self.tableview_summary.reloadData()
                    }
                }
                
            }
            catch
            {
                DispatchQueue.main.async {
                    // self.btnEnable = true
                    
                    // self.POPUp(message: "No data found", time: 1.8)
                }
                
            }
            
        }, failure: {(err) in
            
            DispatchQueue.main.async {
                //self.btnEnable = true
                
                // self.POPUp(message: "Something went wrong", time: 1.8)
            }
            if APIManager.sharedInstance.error400 {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
            }
            if(APIManager.sharedInstance.error401)
            {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
            }else{
                
            }
            
        })
    }
    
    
}
extension PortfolioVC {
    
    func  getPortfolioBacktestAPI(){
        self.view.activityStartAnimating()
        let url = ApiUrl.GET_BACK_TEST_URL
        
        let params = ["namespace": SPManager.sharedInstance.getCurrentUserNamespace()] as [String: AnyObject]
        print(params)
        
        APIManager.sharedInstance.postRequest(serviceName: url, sendData: params, success: { (response) in
            print(response)
            
            if APIManager.sharedInstance.isSuccess {
                DispatchQueue.main.async {
                    self.backtestModel = self.fetchBacktestData(res: response)
                    
                    let amount = FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: self.backtestModel.data.backtest.amount)
                    let startDate = FintechUtils.sharedInstance.getDateInUSLocale(timestamp: self.backtestModel.data.backtest.startDate)
                    let endDate = FintechUtils.sharedInstance.getDateInUSLocale(timestamp: self.backtestModel.data.backtest.endDate)
                    self.amount_date_enddate_backtest_lbl.text = "Vesgo simulated a portfolio of \(amount) starting from \(startDate) to \(endDate)"
                    
                    self.bactest_Tableview.reloadData()
                }
            }else{
                
                DispatchQueue.main.async { [self] in
                    if response["message"] != nil {
                        self.showAlert(response["message"] as! String)
                    } else {
                        self.showAlert(ConstantStrings.NO_RESULT_FOUND_ALERT_TEXT)
                    }
                }
            }
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
        }, failure: { (data) in
            print(data)
            DispatchQueue.main.async { [self] in
                view.activityStopAnimating()
            }
            if APIManager.sharedInstance.error400 {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
            }
            if(APIManager.sharedInstance.error401)
            {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
            }else{
                self.showAlert(ConstantStrings.Error_msg_generic)
            }
        })
        
    }
    
    func fetchBacktestData(res: [String: AnyObject]) -> PortfolioBacktestModel{
        let success = res["success"] as! Bool
        let message = res["message"] as! String
        let data = res["data"] as! [String: AnyObject]
        
        let backtest = data["backtest"] as! [String: AnyObject]
        let amount = backtest["amount"] as! Double
        let cash = backtest["cash"] as! Double
        let startDate = backtest["start_date"] as! Int64
        let endDate = backtest["end_date"] as! Int64
        
        let performance = data["performance"] as! [AnyObject]
        
        var backtestPerformance = [BacktestPerformance]()
        
        for i in 0..<performance.count{
            let item = performance[i] as! [String: AnyObject]
            
            let name = item["portfolio"] as! String
            let portfolioReturn = item["return"] as! Double
            
            backtestPerformance.append(BacktestPerformance.init(portfolioName: name, portfolioReturn: portfolioReturn))
        }
        let risk = data["risk"] as! [String: AnyObject]
        let beta = risk["portfolio_beta"] as! Double
        let volatility = risk["portfolio_volatility"] as! Double
        
        
        let portfolioBacktest = PortfolioBacktest(amount: amount, cash: cash, endDate: endDate, startDate: startDate)
        
        let portfolioRisk = PortfolioBacktestRisk(beta: beta, volatility: volatility)
        
        let portfolioBacktestData = PortfolioBacktestData(backtest: portfolioBacktest, performance: backtestPerformance, risk: portfolioRisk)
        
        
        return PortfolioBacktestModel(success: success, message: message, data: portfolioBacktestData)
    }
    
    
}


extension Double {
    func toString() -> String {
        return String(format: "%.2f",self)
    }
}
extension PortfolioVC{
    func fucturepredticonapicall(){
        DispatchQueue.main.async {
            self.view.activityStartAnimating()
        }
        let url = ApiUrl.GET_future_prediction_URL
        let namespace = SPManager.sharedInstance.getCurrentUserNamespace()
        
        
        postManagerUser(method: "POST", isThisURLEmbdedData: false, urlString: url, parameter: ["namespace": namespace], success: {(value) in
            do
            {

                let jsonObject = try JSONDecoder().decode(FuturepredictionModel?.self, from: value)
                DispatchQueue.main.async {
                    if let value = jsonObject?.data
                    {
                        self.futurepredication = value
                        
                        self.futurepredication?.sorted {
                            $0.xAxis < $1.xAxis
                        }
                        
                        
                            if let line1data = self.futurepredication?.map({$0.percent25}),
                            let line2data = self.futurepredication?.map({$0.percent50}),
                            let line3data = self.futurepredication?.map({$0.percent75}),
                            let line4data = self.futurepredication?.map({$0.percent90}),
                            let xaixs = self.futurepredication?.map({$0.xAxis}){
                            
                            
                            var years = [String]()
                                for i in 0..<xaixs.count {
                                    let item = xaixs[i]
                                    
                                    if i % 6 == 0 {
                                        let parsedTime = FintechUtils.sharedInstance.getShortDate(fromSeconds: item/1000)
                                        years.append("\(parsedTime)")
                                    }else {
                                        years.append("")
                                    }
                                    
                            }
                            
                            self.setChartData(months: years, line1data: line1data as! [Double] , line2data: line2data  as! [Double], line3data: line3data as! [Double], line4data: line4data as! [Double])
                            DispatchQueue.main.async {
                                self.view.activityStopAnimating()
                            }
                        }
                    }
                }
                DispatchQueue.main.async {
                    self.view.activityStopAnimating()
                }
            }
            catch
            {
                DispatchQueue.main.async {
                    self.view.activityStopAnimating()
                }
                
            }
            
        }, failure: {(err) in
            
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
            if APIManager.sharedInstance.error400 {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
            }
            if(APIManager.sharedInstance.error401)
            {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
            }else{
                
            }
            
        })
    }
    func setChartData(months : [String],line1data : [Double],line2data : [Double],line3data : [Double],line4data : [Double] ) {
        let data = LineChartData()
        
        var line1 = LineChartDataSet()
        var line4 = LineChartDataSet()
        
        if (line1data.count > 0) {
            var lineChartEntry1 = [ChartDataEntry]()
            for i in 0..<line1data.count {
                lineChartEntry1.append(ChartDataEntry(x: Double(i), y: Double(line1data[i]) ?? 0.0))
            }
            line1 = LineChartDataSet(entries: lineChartEntry1, label: "25%")
            line1.drawValuesEnabled = false
            line1.drawCirclesEnabled = false
            line1.lineWidth=2.0
            line1.circleColors = [#colorLiteral(red: 0.5058823529, green: 0.7803921569, blue: 0.5176470588, alpha: 1)]
            line1.colors = [#colorLiteral(red: 0.5058823529, green: 0.7803921569, blue: 0.5176470588, alpha: 1)]
            // line1.colors=[EZLoadingActivity.Settings.CompetitorChartLineColor]
            
            data.addDataSet(line1)
        }
        if (line2data.count > 0) {
            var lineChartEntry2 = [ChartDataEntry]()
            for i in 0..<line2data.count {
                lineChartEntry2.append(ChartDataEntry(x: Double(i), y: Double(line2data[i]) ?? 0.0))
            }
            let line2 = LineChartDataSet(entries: lineChartEntry2, label: "50%")
            //  line2.mode = .cubicBezier
            line2.drawValuesEnabled = false
            line2.drawCirclesEnabled = false
            line2.lineWidth=2.0
            line2.circleColors = [#colorLiteral(red: 0.2980392157, green: 0.6862745098, blue: 0.3137254902, alpha: 1)]
            line2.colors = [#colorLiteral(red: 0.2980392157, green: 0.6862745098, blue: 0.3137254902, alpha: 1)]
            //line2.colors=[EZLoadingActivity.Settings.CompetitorChartLineColor]
            //  line2.drawFilledEnabled = true
            
            data.addDataSet(line2)
            
            
        }
        if (line3data.count > 0) {
            var lineChartEntry3 = [ChartDataEntry]()
            for i in 0..<line3data.count {
                lineChartEntry3.append(ChartDataEntry(x: Double(i), y: Double(line3data[i]) ?? 0.0))
            }
            let line3 = LineChartDataSet(entries: lineChartEntry3, label: "75%")
            line3.drawValuesEnabled = false
            line3.drawCirclesEnabled = false
            line3.circleColors = [#colorLiteral(red: 0.03921568627, green: 0.5607843137, blue: 0.03137254902, alpha: 1)]
            line3.colors = [#colorLiteral(red: 0.03921568627, green: 0.5607843137, blue: 0.03137254902, alpha: 1)]
            line3.lineWidth=2.0
            data.addDataSet(line3)
        }
        if (line4data.count > 0) {
            var lineChartEntry4 = [ChartDataEntry]()
            for i in 0..<line4data.count {
                lineChartEntry4.append(ChartDataEntry(x: Double(i), y: Double(line4data[i]) ?? 0.0))
            }
            
            line4 = LineChartDataSet(entries: lineChartEntry4, label: "90%")
            line4.drawValuesEnabled = false
            line4.drawCirclesEnabled = false
            line4.circleColors = [#colorLiteral(red: 0.1058823529, green: 0.368627451, blue: 0.1254901961, alpha: 1)]
            line4.colors = [#colorLiteral(red: 0.1058823529, green: 0.368627451, blue: 0.1254901961, alpha: 1)]
            line4.lineWidth=2.0
            data.addDataSet(line4)
        }
        let yAxis = futureprediction_linechartview.rightAxis
        let xAxis = futureprediction_linechartview.topAnchor
        
        
        yAxis.enabled = false
        yAxis.drawLabelsEnabled = false
        yAxis.drawAxisLineEnabled = false
        yAxis.drawGridLinesEnabled = false
        
        
        
        futureprediction_linechartview.xAxis.drawGridLinesEnabled = false
        futureprediction_linechartview.leftAxis.enabled = true
        futureprediction_linechartview.pinchZoomEnabled = false
        futureprediction_linechartview.scaleYEnabled = false
        futureprediction_linechartview.scaleXEnabled = true
        futureprediction_linechartview.highlighter = nil
        futureprediction_linechartview.backgroundColor = UIColor.clear
        futureprediction_linechartview.chartDescription?.text = ""
        futureprediction_linechartview.leftAxis.drawGridLinesEnabled = false
        futureprediction_linechartview.rightAxis.drawGridLinesEnabled = false
        futureprediction_linechartview.legend.horizontalAlignment = Legend.HorizontalAlignment.right
        futureprediction_linechartview.xAxis.granularityEnabled = true
        futureprediction_linechartview.xAxis.granularity = 1.0
        futureprediction_linechartview.xAxis.axisMinimum = 0.0
        futureprediction_linechartview.xAxis.axisMaximum = 36.0
        futureprediction_linechartview.xAxis.labelCount = 36
        futureprediction_linechartview.xAxis.forceLabelsEnabled = true
        futureprediction_linechartview.xAxis.granularityEnabled = true
        futureprediction_linechartview.xAxis.labelPosition = XAxis.LabelPosition.bottom
        futureprediction_linechartview.xAxis.valueFormatter = IndexAxisValueFormatter(values: months)
        
        let animator = self.futureprediction_linechartview.chartAnimator
        let viewPortHandler = self.futureprediction_linechartview.viewPortHandler
        
        
        //futureprediction_linechartview.setVisibleXRangeMinimum(36)
        //futureprediction_linechartview.setVisibleXRangeMaximum(36)
        self.futureprediction_linechartview.data = data
        
        futureprediction_linechartview.renderer = CustomLineChartRenderer.init(dataProvider: self.futureprediction_linechartview, animator: animator!, viewPortHandler: viewPortHandler!)
        
        line4.drawFilledEnabled = true
        line4.fillFormatter = AreaFillFormatter(fillLineDataSet: line1)
    }
}

extension PortfolioVC: UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//            let label = UILabel(frame: CGRect.zero)
//            label.text = arr11[indexPath.item]
//            label.sizeToFit()
            if indexPath.row == 0 {
                
                return CGSize(width: 90, height: 32)
            } else if indexPath.row == 1 {
                
                return CGSize(width: 130, height: 32)
            } else if indexPath.row == 2 {
                
                return CGSize(width: 120, height: 32)
            } else {
            return CGSize(width: 170, height: 32)
        }
        }
    
    
//    var theCollectionView : UICollectionView {
//        let layout = UICollectionViewFlowLayout()
//        layout.scrollDirection = .horizontal
//        layout.minimumLineSpacing = 1.0
//        layout.minimumInteritemSpacing = 1.0
//        // note: since the labels are "auto-width-stretching", the height here defines the actual height of the cells
//        layout.estimatedItemSize = CGSize(width: 60, height: 28)
//        let cv  = UICollectionView(frame: .zero, collectionViewLayout: layout)
//        // using lightGray for the background "fills in" the spacing, giving us "cell borders"
//        cv.backgroundColor = UIColor.lightGray
//        cv.dataSource = self
//        cv.delegate = self
//        return cv
//
//    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arr11.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! topbarportfolioUICollectionCell
        cell.heading_lbl.text = arr11[indexPath.item]
        if selectedIndx == indexPath.row {
            
            cell.heading_lbl!.textColor  = #colorLiteral(red: 0.3098039216, green: 0.6156862745, blue: 0.8666666667, alpha: 1)
            //cell.heading_lbl!.attributedText = arr11[indexPath.item].underLined
            
            
        }  else {
            cell.heading_lbl!.textColor = UIColor.black
            
        }
        
        
        //        let selectedCell:UICollectionViewCell = UICollectionView.cellForItemAtIndexPath(indexPath)!
        //        selectedCell.contentView.backgroundColor = UIColor(red: 102/256, green: 255/256, blue: 255/256, alpha: 0.66)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! topbarportfolioUICollectionCell
        selectedIndx = indexPath.row
        self.topbar_collectionview.reloadData()
        
        //  self.yourCollctionView.reloadData()
        
        setLandscapeOrientation(false)
        
        if indexPath.row == 0 {
            cell.isSelected = true
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! topbarportfolioUICollectionCell
            cell.heading_lbl!.textColor = UIColor.red
            cell.heading_lbl.backgroundColor = UIColor.red
            self.tabBarController?.tabBar.isHidden = false
//            if UIDevice.current.orientation.isLandscape {
//
//                var supportedInterfaceOrientations: UIInterfaceOrientationMask { return .portrait }
//                let value = UIInterfaceOrientation.portrait.rawValue
//                UIDevice.current.setValue(value, forKey: "orientation")
//                // imageView.image = UIImage(named: const2)
//            } else {
//
//            }
            cell.heading_lbl.font = UIFont.boldSystemFont(ofSize: 20.0)
            
            //cell.heading_lbl.font = UIFont.boldSystemFont(ofSize: 25)
            
            Main_viewLibrary.isHidden = true
            //media_btn.titleLabel?.fontSize = 15
            //keyDocs_btn.titleLabel?.fontSize = 15
            //deepDives_btn.titleLabel?.fontSize = 15
            documents_view.isHidden = true
            lederboard_view.isHidden = true
            documents_view!.heightConstaint?.constant = 0
            summary_view.isHidden = false
            future_prediction_View.isHidden = true
            documents_btn.titleLabel?.fontSize = 13
            leaderboard_btn.titleLabel?.fontSize = 13
            futureprediction_btn.titleLabel?.fontSize = 13
            portfolio_btn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 15)
        }else if indexPath.item == 1 {
            self.tabBarController?.tabBar.isHidden = false

            
//            if UIDevice.current.orientation.isLandscape {
//
//                var supportedInterfaceOrientations: UIInterfaceOrientationMask { return .portrait }
//                let value = UIInterfaceOrientation.portrait.rawValue
//                UIDevice.current.setValue(value, forKey: "orientation")
//                // imageView.image = UIImage(named: const2)
//            }
            documents_view.isHidden = true
            lederboard_view.isHidden = false
            documents_view!.heightConstaint?.constant = 0
            future_prediction_View.isHidden = true
            summary_view.isHidden = true
            portfolio_btn.titleLabel?.fontSize = 13
            futureprediction_btn.titleLabel?.fontSize = 13
            documents_btn.titleLabel?.fontSize = 13
            leaderboard_btn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 15)
        
            
            if segment_myage_leaderbaord.selectedSegmentIndex == 0{
                if leaderboardModel != nil {
                    selectedLeaderboardType = .ALL
                    self.lederboard_tableview.reloadData()
                }else {
                    self.getLeaderboardData(isAgeGroupData: false)
                }
            }else if segment_myage_leaderbaord.selectedSegmentIndex == 1{
                if SPManager.sharedInstance.saveUserLoginStatus(){
                    if leaderboardModelAgeGroup != nil {
                        selectedLeaderboardType = .MY_AGE_GROUP
                        self.lederboard_tableview.reloadData()
                    }else {
                        self.getLeaderboardData(isAgeGroupData: true)
                    }
                }else{
                    var toast = getLocalizedString(key: "Join now for a customized view", comment: " ")
                    self.view.makeToast(toast)
                    segment_myage_leaderbaord.selectedSegmentIndex = 0
                }
                
            }
            
            self.lederboard_tableview.reloadData()
            
            
            
        }else if indexPath.item == 2 {
            self.tabBarController?.tabBar.isHidden = false
            self.backtest_View.isHidden = true


//            if UIDevice.current.orientation.isLandscape {
//                print("Landscape")
//                var supportedInterfaceOrientations: UIInterfaceOrientationMask { return .portrait }
//                let value = UIInterfaceOrientation.portrait.rawValue
//                UIDevice.current.setValue(value, forKey: "orientation")
//                // imageView.image = UIImage(named: const2)
//            } else {
//                print("Portrait")
//
//            }
            
            
            
            documents_view.isHidden = true
            lederboard_view.isHidden = true
            documents_view!.heightConstaint?.constant = 0
            future_prediction_View.isHidden = true
            
            summary_view.isHidden = true
            Main_viewLibrary.isHidden = false
            portfolio_btn.titleLabel?.fontSize = 13
            leaderboard_btn.titleLabel?.fontSize = 13
            futureprediction_btn.titleLabel?.fontSize = 13
            documents_btn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 15)
            // documet view
            segment1_view.isHidden = true
            segment2_view.isHidden = false
            segment3_view.isHidden = true
            backtest_View.isHidden = true
            
            Main_viewLibrary.isHidden = false
            // portfolio_btn.titleLabel?.fontSize = 15
            
            deepDives_btn.titleLabel?.fontSize = 15
            media_btn.titleLabel?.fontSize = 15
            backtest_btn.titleLabel?.fontSize = 15
            keyDocs_btn.setTitleColor(#colorLiteral(red: 0.3098039216, green: 0.6156862745, blue: 0.8666666667, alpha: 1), for: .normal)
            backtest_btn.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
            
            keyDocs_btn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
            keyDocs_btn.backgroundColor = UIColor.clear

            
            
            //self.getLibData()

        }else if indexPath.item == 3{
            self.getPortfolioBacktestAPI()
            self.tabBarController?.tabBar.isHidden = false
            self.backtest_View.isHidden = true
            
            
//            if UIDevice.current.orientation.isLandscape {
//                print("Landscape")
//                var supportedInterfaceOrientations: UIInterfaceOrientationMask { return .portrait }
//                let value = UIInterfaceOrientation.portrait.rawValue
//                UIDevice.current.setValue(value, forKey: "orientation")
//                // imageView.image = UIImage(named: const2)
//            } else {
//                print("Portrait")
//
//            }
            
            segment1_view.isHidden = true
            segment2_view.isHidden = true
            segment3_view.isHidden = true
            Main_viewLibrary.isHidden = false
            future_prediction_View.isHidden = true
            backtest_View.isHidden = false
            // portfolio_btn.titleLabel?.fontSize = 15
            documents_view.isHidden = true
            documents_view!.heightConstaint?.constant = 0
            summary_view.isHidden = true
            lederboard_view.isHidden = true
            
            deepDives_btn.titleLabel?.fontSize = 15
            keyDocs_btn.titleLabel?.fontSize = 15
            media_btn.titleLabel?.fontSize = 15
            backtest_btn.setTitleColor(#colorLiteral(red: 0.3098039216, green: 0.6156862745, blue: 0.8666666667, alpha: 1), for: .normal)
            keyDocs_btn.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
            
            backtest_btn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
            
            
            
            
            
            self.getLibData()
    }else if indexPath.item == 4 {
            self.tabBarController?.tabBar.isHidden = true

//            if UIDevice.current.orientation.isLandscape {
//                print("Landscape")
//                var supportedInterfaceOrientations: UIInterfaceOrientationMask { return .portrait }
//                let value = UIInterfaceOrientation.portrait.rawValue
//                UIDevice.current.setValue(value, forKey: "orientation")
//                // imageView.image = UIImage(named: const2)
//            } else {
//                print("Portrait")
//                let value = UIInterfaceOrientation.landscapeRight.rawValue
//                UIDevice.current.setValue(value, forKey: "orientation")
//                var supportedInterfaceOrientations: UIInterfaceOrientationMask { return .portrait }
//
//                //imageView.image = UIImage(named: const)
//            }
//
//
//            func changeOrientationLandScape(_ orientation: UIInterfaceOrientationMask)
//
//            {
//
//                // AppUtility.lockOrientation(orientation)
//
//                let value = UIInterfaceOrientation.landscapeRight.rawValue
//
//                UIDevice.current.setValue(value, forKey: "orientation")
//
//                UIViewController.attemptRotationToDeviceOrientation()
//
//            }
        
        setLandscapeOrientation(true)
        
            documents_view!.heightConstaint?.constant = 0
            
            portfolio_btn.titleLabel?.fontSize = 13
            leaderboard_btn.titleLabel?.fontSize = 13
            documents_btn.titleLabel?.fontSize = 13
            futureprediction_btn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 15)
            documents_view.isHidden = true
            Main_viewLibrary.isHidden = true
            lederboard_view.isHidden = true
            summary_view.isHidden = true
            
            future_prediction_View.isHidden = false
            self.fucturepredticonapicall()

            
        }
    }
    
    //    override var isSelected: Bool {
    //        didSet {
    //            c.textColor = .red
    //        }
    //    }
    
}

//extension PortfolioVC: UICollectionViewDelegateFlowLayout {
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        return "String".size(withAttributes: nil)
//    }
//}

//extension PortfolioVC: UICollectionViewDelegateFlowLayout {
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        let size = collectionView.frame.size
//
//        return CGSize(width: 130, height: size.height)
//    }
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
//        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
//    }
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
//        return 0
//    }
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
//        return 0
//    }
//
//
//}


struct AppUtility {
    
    
    static func lockOrientation(_ orientation: UIInterfaceOrientationMask) {
        
        
        
        if let delegate = UIApplication.shared.delegate as? AppDelegate {
            
            delegate.orientationLock = orientation
            
        }
        
    }
    static func lockOrientation(_ orientation: UIInterfaceOrientationMask, andRotateTo rotateOrientation:UIInterfaceOrientation) {
        
        
        
        self.lockOrientation(orientation)
        
        
        
        UIDevice.current.setValue(rotateOrientation.rawValue, forKey: "orientation")
        
        UINavigationController.attemptRotationToDeviceOrientation()
        
    }
    
}
extension PortfolioVC {

    func changeOrientationLandScape(_ orientation: UIInterfaceOrientationMask)
    {
        AppUtility.lockOrientation(orientation)
        
        let value = UIInterfaceOrientation.landscapeRight.rawValue
        
        UIDevice.current.setValue(value, forKey: "orientation")
        
        UIViewController.attemptRotationToDeviceOrientation()
        
    }
    func changeOrientationPotrait(_ orientation: UIInterfaceOrientationMask)
    {
        AppUtility.lockOrientation(orientation)
        let value = UIInterfaceOrientation.portrait.rawValue
        
        UIDevice.current.setValue(value, forKey: "orientation")
        
        UIViewController.attemptRotationToDeviceOrientation()

    }
    /// OPTIONAL Added method to adjust lock and rotate to the desired orientation
}
extension PortfolioVC {
    private func openBottomActionSheetForSorting() {
        // if NetworkHelper.sharedInstance.isDmoatOnline {
        let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        
        let sortSymbol = UIAlertAction(title: getLocalizedString(key: "sort_by_symbol_a_z"), style: .default, handler: { [self]
            (alert: UIAlertAction!) -> Void in
            for i in 0..<(tickertabledta!.count) {
                
                tickertabledta![i].companies =  tickertabledta![i].companies!.sorted {
                    $0.tickerTitle! < $1.tickerTitle!
                    
                }
            }
            tableview_summary.reloadData()
        })
        
        let sortSymbolHighToLow = UIAlertAction(title: getLocalizedString(key: "sort_by_symbol_z_a"), style: .default, handler: { [self]
            (alert: UIAlertAction!) -> Void in
            for i in 0..<(tickertabledta!.count) {
                
                tickertabledta![i].companies =  tickertabledta![i].companies!.sorted {
                    $0.tickerTitle! > $1.tickerTitle!
                    
                }
            }
            tableview_summary.reloadData()
        })
        let sortHighToLow = UIAlertAction(title: getLocalizedString(key: "sort_by_amount_h_l"), style: .default, handler: { [self]
            (alert: UIAlertAction!) -> Void in
            
            for i in 0..<(tickertabledta!.count) {
                
                tickertabledta![i].companies =  tickertabledta![i].companies!.sorted {
                    $0.tickerShare > $1.tickerShare
                    
                }
            }
            tableview_summary.reloadData()
            
        })
        
        let sortLowToHigh = UIAlertAction(title: getLocalizedString(key: "sort_by_amount_l_h"), style: .default, handler: { [self]
            (alert: UIAlertAction!) -> Void in
            for i in 0..<(tickertabledta!.count) {
                
                tickertabledta![i].companies =  tickertabledta![i].companies!.sorted {
                    $0.tickerShare < $1.tickerShare
                    
                }
            }
            tableview_summary.reloadData()
        })
        
        
        let cancel = UIAlertAction(title: getLocalizedString(key: "cancel_btn"), style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            
            self.dismiss(animated: true, completion: nil)
            
        })
        
        
        optionMenu.addAction(sortHighToLow)
        optionMenu.addAction(sortLowToHigh)
        optionMenu.addAction(sortSymbol)
        optionMenu.addAction(sortSymbolHighToLow)
        optionMenu.addAction(cancel)
        self.present(optionMenu, animated: true, completion: nil)
    }
}
extension PortfolioVC {
    //Mark:- Risk tolerance API
    func  getUserRiskFactor(){
        //view.activityStartAnimating()
        let url = ApiUrl.GET_RISK_TOLERANCE
        let namespace = SPManager.sharedInstance.getCurrentUserNamespace()
        //let namespace = "AhmadAli-RDD5"

        postManagerUser(method: "POST", isThisURLEmbdedData: false, urlString: url, parameter: ["namespace": namespace], success: {(value) in

            do {
                let riskToleranceModel = try JSONDecoder().decode(RiskToleranceModel?.self, from: value)
                DispatchQueue.main.async { [self] in
                    riskToleranceLabel.text = getLocalizedString(key: "risk_score", comment: "") +  "\(String((riskToleranceModel?.message.risk)!))"
                }
                

            } catch {
                
            }

            
            DispatchQueue.main.async { [self] in
                //view.activityStopAnimating()
            }

        }, failure: {(err) in

            DispatchQueue.main.async { [self] in
                //view.activityStopAnimating()
            }

            if APIManager.sharedInstance.error400 {

                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)

            }

            if(APIManager.sharedInstance.error401)

            {

                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)

            }else{
                self.showAlert(ConstantStrings.Error_msg_generic)
                

            }
        })

    }
}
extension PortfolioVC {
    //Mark:- Serach API
    func  getTickerSearchData(tickerName: String){
        self.view.activityStartAnimating()
        let url = ApiUrl.GET_TICKER_SEARCH_DATA_NEW
        let namespace = SPManager.sharedInstance.getCurrentUserNamespace()
        //let namespace = "kh01-sdRt"
        
        let params:Dictionary<String,AnyObject> =
            ["namespace" : namespace  as AnyObject,
             "symbol_name": tickerName as AnyObject,
             "is_exact_match": true as AnyObject]
        print(params)
        
        APIManager.sharedInstance.postRequest(serviceName: url, sendData: params, success: { (response) in
            print(response)
            
            if APIManager.sharedInstance.isSuccess {
                let res = response["message"] as! [AnyObject]
                DispatchQueue.main.async {
                    self.fetchSearchData(items: res)
                }
            }else{
                
                DispatchQueue.main.async { [self] in
                    if response["message"] != nil {
                        self.showAlert(response["message"] as! String)
                    } else {
                        self.showAlert(ConstantStrings.NO_RESULT_FOUND_ALERT_TEXT)
                    }
                }
            }
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
        }, failure: { (data) in
            print(data)
            DispatchQueue.main.async { [self] in
                view.activityStopAnimating()
            }
            if APIManager.sharedInstance.error400 {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
            }
            if(APIManager.sharedInstance.error401)
            {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
            }else{
                self.showAlert(ConstantStrings.Error_msg_generic)
            }
        })
        
    }
    private func fetchSearchData(items: [AnyObject]){
    
        
        if items.count > 0 {
            let response = items[0] as! [String: AnyObject]
            
            let symbolName = response["Symbol_name"] as! String
            let industry = response["industry"] as! String
            let ticker = (response["ticker"] as? String) ?? ""
            
            let portfolioComposition = response["portfolio_composition"] as! [String: AnyObject]
            
            
            let controversy = portfolioComposition["Controversy"] as! Int
            let overallImpact = portfolioComposition["overall_impact"] as! String
            let overallBetterPeers = portfolioComposition["overall_better_peers"] as! String
            let overallBadPeers = portfolioComposition["overall_bad_peers"] as! String
            let zScore = portfolioComposition["z_score"] as! Double
            
            let envioromentData = fetchEsgValuesForSearch(portfolioComposition: portfolioComposition, key: "environment")
            let socialData = fetchEsgValuesForSearch(portfolioComposition: portfolioComposition, key: "social")
            let governanceData = fetchEsgValuesForSearch(portfolioComposition: portfolioComposition, key: "governance")
            
            let overallData = OverallNewEsgModel.init(controversy: controversy, impact: overallImpact, betterPeers: overallBetterPeers, badPeers: overallBadPeers, zScore: zScore)
            
            let searchData = SearchData(symbolName: symbolName, industry: industry, portfolioComposition: overallData, envData: envioromentData, socialData: socialData, governanceData: governanceData, symbol: ticker)
            
            let vc = UIStoryboard.init(name: "InvestmentDetails", bundle: Bundle.main).instantiateViewController(withIdentifier: "SearchResultsNewVC") as? SearchResultNewVC
            vc?.searchData = searchData
            self.navigationController?.pushViewController(vc!, animated: true)
        }else {
            self.view.makeToast("No result found, please try again later")
        }
        
    
    }
    
    private func fetchEsgValuesForSearch(portfolioComposition: [String: AnyObject], key: String) -> NewEsgScoreModel{
        let esg = portfolioComposition[key] as! [String: AnyObject]
        let impact = esg["impact"] as! String
        let betterPeers = esg["better_peers"] as! String
        let badPeers = esg["bad_peers"] as! String
        let zScore = esg["z_score"] as! Double
        let derivedValues = esg["derived_values"] as! [AnyObject]
        
        var derivedValuesModel = [EsgDerivedValues]()
        for i in 0..<derivedValues.count {
            let derivedValue = derivedValues[i] as! [String: AnyObject]
            let name = derivedValue["name"] as! String
            let derivedScore = derivedValue["score"] as! Double
            
            derivedValuesModel.append(EsgDerivedValues.init(valueName: name, score: derivedScore))
        }
        
        let esgData = NewEsgScoreModel.init(impact: impact, betterPeers: betterPeers, badPeers: badPeers, zScore: zScore, derivedValues: derivedValuesModel)
        return esgData
        
    }
}

//MARK:- Leaderoard API
extension PortfolioVC {
    func getLeaderboardData(isAgeGroupData: Bool) {
        self.view.activityStartAnimating()
        var url: String!
        if isAgeGroupData {
            url = ApiUrl.GET_AGE_GROUP_LEADERBOARD_DATA
        }else {
            url = ApiUrl.GET_Leaderboard_URL
        }
        let namespace = SPManager.sharedInstance.getCurrentUserNamespace()
        
        let params:Dictionary<String,AnyObject> =
            ["namespace" : namespace  as AnyObject]
        print(params)
        
        APIManager.sharedInstance.postRequest(serviceName: url, sendData: params, success: { [self] (response) in
            //print(response)
            
            if isAgeGroupData {
                self.leaderboardModelAgeGroup = fetchLeaderboardDataResponse(response: response)
            }else {
                self.leaderboardModel = fetchLeaderboardDataResponse(response: response)
            }
            
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
            
            DispatchQueue.main.async {
                self.lederboard_tableview.reloadData()
            }
                
            
        }, failure: { (data) in
            print(data)
            DispatchQueue.main.async { [self] in
                view.activityStopAnimating()
            }
            if APIManager.sharedInstance.error400 {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
            }
            if(APIManager.sharedInstance.error401)
            {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
            }else{
                self.showAlert(ConstantStrings.Error_msg_generic)
            }
        })
    }
    
    private func fetchLeaderboardDataResponse(response: [String: AnyObject]) -> LeaderboardModel {
        let success = response["success"] as? Bool
        let message = response["message"] as? String
        let data = fetchLeaderboardData(leaderboardData: response["data"] as? [String: AnyObject] ?? ["": "" as AnyObject])
        
        return LeaderboardModel(success: success, message: message, data: data)
        
    }
    
    private func fetchLeaderboardData(leaderboardData: [String: AnyObject]) -> LeaderboardData {
        let pastMonthData = fetchLeaderboardUserDataList(dataList: leaderboardData["past_month"] as? [AnyObject] ?? [AnyObject]())

        let pastYearData = fetchLeaderboardUserDataList(dataList: leaderboardData["past_year"] as? [AnyObject] ?? [AnyObject]())
        
        let past3YearData = fetchLeaderboardUserDataList(dataList: leaderboardData["past_3year"] as? [AnyObject] ?? [AnyObject]())
        
        return LeaderboardData(pastMonth: pastMonthData, pastYear: pastYearData, past3Year: past3YearData)
    }
    
    private func fetchLeaderboardUserDataList(dataList: [AnyObject]) -> [LeaderboardUsersList] {
        var usersDataList = [LeaderboardUsersList]()
        for i in 0..<dataList.count {
            let item = fetchLeaderboardUserData(leaderboardData: dataList[i] as! [String: AnyObject])
            
            usersDataList.append(item)
        }
        
        return usersDataList
    }
    
    private func fetchLeaderboardUserData(leaderboardData: [String: AnyObject]) -> LeaderboardUsersList {
        let portfolioId = leaderboardData["portfolio_id"] as? Int
        let rank = leaderboardData["rank"] as? Int
        let name = leaderboardData["name"] as? String
        let portfolioReturn = leaderboardData["portfolio_return"] as? String
        let riskScore = leaderboardData["risk_score"] as? Double
        let assets = fetchLeaderboardAssets(assetsData: leaderboardData["assets"] as? [AnyObject] ?? [AnyObject]())
        let positiveValues = leaderboardData["positive_values"] as? [String]
        let negativeValues = leaderboardData["negative_values"] as? [String]
        let portfolioComposition = fetchPortfolioCompositionData(portfolioComposition: leaderboardData["portfolio_composition"] as! [String: AnyObject])
        
        
        return LeaderboardUsersList(portfolioId: portfolioId, rank: rank, name: name, portfolioReturn: portfolioReturn, riskScore: riskScore, assets: assets, positiveValues: positiveValues, negativeValues: negativeValues, portfolioComposition: portfolioComposition)
    }
    
    private func fetchLeaderboardAssets(assetsData: [AnyObject]) -> [LeaderboardAssets] {
        var leaderboardAssets = [LeaderboardAssets]()
        for i in 0..<assetsData.count {
            let item = assetsData[i] as? [String: AnyObject]
            
            let symbol = item?["ticker"] as? String
            let name = item?["ticker_title"] as? String
            let shareAmount = item?["ticker_ammount"] as? Double
            let sharePercent = item?["ticker_share"] as? Double
            
            leaderboardAssets.append(LeaderboardAssets(symbol: symbol, symbolName: name, shareAmount: shareAmount, sharePercent: sharePercent))
        }
        
        return leaderboardAssets
    }
    
    private func fetchPortfolioCompositionData(portfolioComposition: [String: AnyObject]) -> UserPortfolioComposition {
        let impact = portfolioComposition["impact"] as! String
        let controversy = portfolioComposition["controversy"] as! [String: AnyObject]
        let zScore = portfolioComposition["z_score"] as! Double
        let controversyData = fetchControversyData(res: controversy)
        
        let env = fetchLeaderboardEsgValues(esg: portfolioComposition["Environment"] as! [String: AnyObject])
        
        let social = fetchLeaderboardEsgValues(esg: portfolioComposition["Social"] as! [String: AnyObject])
        
        let governance = fetchLeaderboardEsgValues(esg: portfolioComposition["Governance"] as! [String: AnyObject])
        
        
        return UserPortfolioComposition.init(impact: impact, zScore: zScore, controversy: controversyData, environment: env, socialScore: social, governanceScore: governance)
    }
    
    
    private func fetchLeaderboardEsgValues(esg: [String: AnyObject]) -> NewEsgScoreModel{
        let impact = esg["impact"] as! String
        let betterPeers = (esg["better_peers"] as? String) ?? ""
        let bad_peers = (esg["bad_peers"] as? String) ?? ""
        let zScore = (esg["z_score"] as? Double) ?? 0.0
        let riskScore = (esg["risk_score"] as? Double) ?? 0.0
        
        let derivedValues = esg["derived_values"] as! [AnyObject]
        
        var derivedValuesModel = [EsgDerivedValues]()
        for i in 0..<derivedValues.count {
            let derivedValue = derivedValues[i] as! [String: AnyObject]
            let name = derivedValue["name"] as! String
            let derivedScore = derivedValue["score"] as! Double
            
            derivedValuesModel.append(EsgDerivedValues.init(valueName: name, score: derivedScore))
        }
        
        let esgData = NewEsgScoreModel.init(impact: impact, betterPeers: betterPeers, badPeers: bad_peers, zScore: zScore, derivedValues: derivedValuesModel)
        
        return esgData
        
    }
    
    private func fetchControversyData(res: [String: AnyObject]) -> ControversyData {
        let count = res["Controversy_count"] as! Int
        let peerControversy = (res["avgPeerControversy"] as? Double) ?? 0.0
        let controversies = res["relatedControversy"] as! [AnyObject]
        
        var data = [String]()
        
        for i in 0..<controversies.count {
            data.append(controversies[i] as! String)
        }
        
        return ControversyData.init(count: count, controversies: data, peerControversy: peerControversy)
    }
}

