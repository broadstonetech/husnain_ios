import Firebase

import MessageKit

import FirebaseFirestore


 

struct senderr: SenderType{

    var senderId: String

    var displayName: String

}

struct chatMessage: MessageType {

//    var sender: Sender

    

    

    var sender: SenderType

    var kind: MessageKind

    let sentDate: Date

    let id: String?

    let content: String

    

//    let messageId : String?

//  let sender: Sender

  

  var data: MessageKind {

    return .text(content)

  }

  

  var messageId: String {

    return id ?? UUID().uuidString

  }

  

  var downloadURL: URL? = nil

  

  init(content: String) {

    let namespace = SPManager.sharedInstance.getCurrentUserNamespace()

//    sender = Sender(id:namespace, displayName: NetworkHelper.sharedInstance.name!)

    

    sender = senderr(senderId: namespace, displayName: SPManager.sharedInstance.getCurrentUserNamespace())

    

    self.content = content

    sentDate = Date()

    id = nil

    downloadURL = nil

    kind = .text(content)

  }

  

    

    init(content:String,sentDate:Date,senderId:String,senderName:String){

        sender = senderr(senderId: senderId, displayName:senderName)

        self.content = content

        self.sentDate = sentDate

        id = nil

        downloadURL = nil

        kind = .text(content)

    }

  

}


 

extension chatMessage: DatabaseRepresentation {


 

  var representation: [String : Any] {


 


 

    var rep: [String : Any] = [

      "created": Int64((sentDate.timeIntervalSince1970).rounded()),

        "sender_id": sender.senderId,

      "sender_name": sender.displayName

    ]


 

    if let url = downloadURL {

      rep["url"] = url.absoluteString

    } else {

      rep["content"] = content

    }


 

//    rep["content"] = content

    return rep

  }


 

}


 


 


 

extension chatMessage: Comparable {

  

  static func == (lhs: chatMessage, rhs: chatMessage) -> Bool {

    return lhs.id == rhs.id

  }

  

  static func < (lhs: chatMessage, rhs: chatMessage) -> Bool {

    return lhs.sentDate < rhs.sentDate

  }

  

}
