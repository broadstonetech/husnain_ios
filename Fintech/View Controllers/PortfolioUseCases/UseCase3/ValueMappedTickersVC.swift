//
//  ValueMappedTickersVC.swift
//  Fintech
//
//  Created by broadstone on 11/08/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import UIKit

enum BucketType {
    case leader
    case average
    case laggard
}

enum TickerValueType {
    case Env
    case Soc
    case Gov
}

struct ValueMappedTickersCacheData {
    let valueName: TickerValueType
    let bucketType: BucketType
    let model: ValueMappedTickersModel
}

class ValueMappedTickersVC: PortfolioParent {
    
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var doneBtn: UIButton!
    //@IBOutlet weak var countLabel: UILabel!
    
    @IBOutlet weak var leaderLabel: UILabel!
    @IBOutlet weak var averageLabel: UILabel!
    @IBOutlet weak var laggardLabel: UILabel!
    
    @IBOutlet weak var envLabel: UILabel!
    @IBOutlet weak var socLabel: UILabel!
    @IBOutlet weak var govLabel: UILabel!
    
    
    var countLabel: UILabel!
    
    
    
    var tickersModel: ValueMappedTickersModel!

    var cacheData = [ValueMappedTickersCacheData]()

    var tickersInCart = [PortfolioTickersData]()
    
    var risk: Int!
    
    var selectedValue: TickerValueType = .Env
    var selectedBucket: BucketType = .leader
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        
        self.valueMappedDelegate = self

        
        createNextButton(title: getLocalizedString(key: "_next"))
        countLabel.text = String(tickersInCart.count)
        
//        cacheData.append(ValueMappedTickersCacheData.init(valueName: valuesPickerData[0].valueName, bucketType: bucketPickerData[0].valueName, model: tickersModel))
        cacheData.append(ValueMappedTickersCacheData.init(valueName: selectedValue, bucketType: selectedBucket, model: tickersModel))
        setLabelTexts()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        setNavBarTitile(title: "Back", topItem: getLocalizedString(key: "asset_allocation"))
    }
    
    func createNextButton(title: String){
        //let label = UILabel(frame: CGRect(x: 55, y: 0, width: 20, height: 16))
        let label = UILabel(frame: CGRect(x: 60, y: 7, width: 30, height: 20))
        label.layer.borderColor = UIColor.clear.cgColor
        label.layer.borderWidth = 2
        label.layer.cornerRadius = label.bounds.size.height / 2
        //label.layer.cornerRadius = 5
        label.textAlignment = .center
        label.layer.masksToBounds = true
        label.font = UIFont(name: "HelveticaNeue-Light", size: 13)
        //label.textColor = UIColor(named: "AppMainColor")
        label.textColor = .black
        label.backgroundColor = .white
        label.text = String(tickersInCart.count)
        
        self.countLabel = label

          // button
        let rightButton = UIButton(frame: CGRect(x: 0, y: 0, width: 80, height: 16))
        rightButton.setTitle(title, for: .normal)
        rightButton.addTarget(self, action: #selector(doneBtnPressed(_:)), for: .touchUpInside)
        rightButton.addSubview(countLabel)
        
        let rightBarButtomItem = UIBarButtonItem(customView: rightButton)
        self.navigationItem.rightBarButtonItem = rightBarButtomItem
    }
    
    @IBAction func doneBtnPressed(_ sender: UIButton){
        if tickersInCart.count == 0 {
            self.view.makeToast("Add asset in bucket")
        }else {
            pushAssetAllocationVC(tickersInCart: tickersInCart, useCaseType: .UseCase3, assetUpdateDalegate: self)
        }
    }
    
    
    @IBAction func envBtnPressed(_ sender: UIButton){
        selectedValue = .Env
        selectedBucket = .leader
        
        let cacheModel = getCacheDataIfAvailable(valueName: selectedValue, bucketName: selectedBucket)
        
        if cacheModel == nil {
            callValueMappedTickerAPI(bucketType: "leader", isNavigatingToNextVC: false, valueName: getMappedSelectedValue(), risk: risk)
        }else {
            self.tickersModel = cacheModel!.model
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
        setLabelTexts()
    }
    
    @IBAction func socBtnPressed(_ sender: UIButton){
        selectedValue = .Soc
        selectedBucket = .leader
        
        let cacheModel = getCacheDataIfAvailable(valueName: selectedValue, bucketName: selectedBucket)
        
        if cacheModel == nil {
            callValueMappedTickerAPI(bucketType: "leader", isNavigatingToNextVC: false, valueName: getMappedSelectedValue(), risk: risk)
        }else {
            self.tickersModel = cacheModel!.model
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
        setLabelTexts()
    }
    
    @IBAction func govBtnPressed(_ sender: UIButton){
        selectedValue = .Gov
        selectedBucket = .leader
        
        let cacheModel = getCacheDataIfAvailable(valueName: selectedValue, bucketName: selectedBucket)
        
        if cacheModel == nil {
            callValueMappedTickerAPI(bucketType: "leader", isNavigatingToNextVC: false, valueName: getMappedSelectedValue(), risk: risk)
        }else {
            self.tickersModel = cacheModel!.model
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
        setLabelTexts()
    }
    
    @IBAction func leadersBtnPressed(_ sender: UIButton){
        selectedBucket = .leader
        
        let cacheModel = getCacheDataIfAvailable(valueName: selectedValue, bucketName: selectedBucket)
        
        if cacheModel == nil {
            callValueMappedTickerAPI(bucketType: "leader", isNavigatingToNextVC: false, valueName: getMappedSelectedValue(), risk: risk)
        }else {
            self.tickersModel = cacheModel!.model
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
        setLabelTexts()
    }
    
    @IBAction func averageBtnPressed(_ sender: UIButton){
        selectedBucket = .average
        
        let cacheModel = getCacheDataIfAvailable(valueName: selectedValue, bucketName: selectedBucket)
        
        if cacheModel == nil {
            callValueMappedTickerAPI(bucketType: "average", isNavigatingToNextVC: false, valueName: getMappedSelectedValue(), risk: risk)
        }else {
            self.tickersModel = cacheModel!.model
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
        setLabelTexts()
    }
    
    @IBAction func laggardBtnPressed(_ sender: UIButton){
        selectedBucket = .laggard
        
        let cacheModel = getCacheDataIfAvailable(valueName: selectedValue, bucketName: selectedBucket)
        
        if cacheModel == nil {
            callValueMappedTickerAPI(bucketType: "laggard", isNavigatingToNextVC: false, valueName: getMappedSelectedValue(), risk: risk)
        }else {
            self.tickersModel = cacheModel!.model
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
        setLabelTexts()
    }
    
    private func getMappedSelectedValue() -> String {
        if selectedValue == .Env {
            return "Environment"
        }else if selectedValue == .Soc {
            return "Social"
        }else {
            return "Governance"
        }
    }
    
    private func setLabelTexts(){
        envLabel.textColor =        .black
        socLabel.textColor =        .black
        govLabel.textColor =        .black
        leaderLabel.textColor =     .black
        averageLabel.textColor =    .black
        laggardLabel.textColor =    .black
        
        if selectedValue == .Env {
            envLabel.textColor = UIColor(named: "AppMainColor")
        } else if selectedValue == .Soc {
            socLabel.textColor = UIColor(named: "AppMainColor")
        }else {
            govLabel.textColor = UIColor(named: "AppMainColor")
        }
        
        if selectedBucket == .leader {
            leaderLabel.textColor = UIColor(named: "AppMainColor")
        }else if selectedBucket == .average {
            averageLabel.textColor = UIColor(named: "AppMainColor")
        }else {
            laggardLabel.textColor = UIColor(named: "AppMainColor")
        }
    }
    
    private func getCacheDataIfAvailable(valueName: TickerValueType, bucketName: BucketType) -> ValueMappedTickersCacheData? {
        let isAvailable = cacheData.contains(where: {
            $0.valueName == valueName && $0.bucketType == bucketName
        })
        
        var data: ValueMappedTickersCacheData!
        if isAvailable {
            data = cacheData.first(where: {
                $0.bucketType == bucketName && $0.valueName == valueName
            })
            
        }
        
        return data
    }
    
}
extension ValueMappedTickersVC: UITableViewDelegate, UITableViewDataSource {
        
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tickersModel == nil {
            return 0
        }else {
            return tickersModel.data.count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 0
        }
        return 20
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int)
    {
        view.tintColor = #colorLiteral(red: 0.9411764706, green: 0.9411764706, blue: 0.9411764706, alpha: 1)
        view.backgroundColor = #colorLiteral(red: 0.9411764706, green: 0.9411764706, blue: 0.9411764706, alpha: 1)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SymbolCell") as! SymbolCell
        let data = tickersModel.data[indexPath.row]
        cell.companyLabel.text = data.CompanyName
        cell.symbolLabel.text = data.symbol
        
        if isTickerAlreadyInBucket(symbol: data.symbol) {
            cell.parentView.backgroundColor = .systemGreen
            cell.companyLabel.textColor = .white
            //cell.symbolLabel.textColor = .white
        }else {
            cell.parentView.backgroundColor = .white
            cell.companyLabel.textColor = .black
            //cell.symbolLabel.textColor = .black
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        if indexPath.row != 0 {
//
//        }
        let data = tickersModel.data[indexPath.row]
        
        if isTickerAlreadyInBucket(symbol: data.symbol) {
            tickersInCart.removeAll(where: {
                $0.symbol == data.symbol
            })
        }else {
            tickersInCart.append(data)
        }
        countLabel.text = String(tickersInCart.count)
        tableView.reloadData()
    }
    
    func isTickerAlreadyInBucket(symbol: String) -> Bool {
        for ticker in tickersInCart {
            if ticker.symbol == symbol {
                return true
            }
        }
        
        return false
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let detailsAction = UIContextualAction(style: .normal, title:  getLocalizedString(key: "_details"), handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in

            let data = self.tickersModel.data[indexPath.row]
            self.getEsgSubFactorsAPI(ticker: data.symbol)
            success(true)
            
            
        })
        detailsAction.backgroundColor = #colorLiteral(red: 0.2935393751, green: 0.6106157899, blue: 0.8765475154, alpha: 1)

        return UISwipeActionsConfiguration(actions: [detailsAction])
    }
    
}
extension ValueMappedTickersVC: AssetsUpdateDelegate, ValueMappedApiDelegate {
    func onCartUpdate(tickersInCart: [PortfolioTickersData], proceedToAlloc: Bool) {
        self.tickersInCart = tickersInCart
        countLabel.text = String(tickersInCart.count)
        self.tableView.reloadData()
    }
    
    func cartDeleted() {
        tickersInCart.removeAll()
        countLabel.text = String(tickersInCart.count)
        self.tableView.reloadData()
    }
    
    func apiCalled(valueName: String, bucketType: String, model: ValueMappedTickersModel) {
        cacheData.append(ValueMappedTickersCacheData.init(valueName: selectedValue, bucketType: selectedBucket, model: model))
        self.tickersModel = model
        self.setLabelTexts()
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
}
//MARK:- Esg Subfactor API
extension ValueMappedTickersVC {
    
    private func getEsgSubFactorsAPI(ticker: String){
        self.view.activityStartAnimating()
        //print(params)
        
        let url = ApiUrl.GET_ESG_SUBFACTORS
        let data = ["ticker": ticker,
                    "value_name": getMappedSelectedValue()] as [String: AnyObject]
        
        let params = ["namespace": SPManager.sharedInstance.getCurrentUserNamespace(),
                    "data": data] as [String: AnyObject]
        
        APIManager.sharedInstance.postRequest(serviceName: url, sendData: params, success: { (response) in
            print(response)
            
            if APIManager.sharedInstance.isSuccess {
                DispatchQueue.main.async { [self] in
                    let storyboard = UIStoryboard(name: "Portfolio", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "EsgSubFactorVC") as! EsgSubFactorVC
                    vc.isFromValueMappedVC = true
                    vc.valuesTagsData = parseSubFactorsData(res: response)
                    vc.tagsLabelColor = .black
                    vc.tagsBackgroundColor = .lightGray
                    vc.companyName = ticker
                    vc.valueType = selectedValue
                    self.navigationController?.pushViewController(vc, animated: true)
                    //self.navigationController?.present(vc, animated: true, completion: nil)
                }
            }else{
                
                DispatchQueue.main.async { [self] in
                    if response["message"] != nil {
                        self.showAlert(response["message"] as! String)
                    } else {
                        self.showAlert(ConstantStrings.NO_RESULT_FOUND_ALERT_TEXT)
                    }
                }
            }
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
        }, failure: { (data) in
            print(data)
            DispatchQueue.main.async { [self] in
                view.activityStopAnimating()
            }
            if APIManager.sharedInstance.error400 {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
            }
            if(APIManager.sharedInstance.error401)
            {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
            }else{
                self.showAlert(ConstantStrings.Error_msg_generic)
            }
        })
    }
    
    
    private func parseSubFactorsData(res: [String: AnyObject]) -> [SwiftTagData]{
        
        let data = res["data"] as! [AnyObject]
        
        var tagsData = [SwiftTagData]()
        
        for i in 0..<data.count {
            let item = data[i] as! [String: AnyObject]
            
            let valueName = item["name"] as! String
            let score = item["score"] as! Double
            
            tagsData.append(SwiftTagData.init(label: valueName, tagImage: FintechUtils.sharedInstance.getInvestmentValuesIcons(investmentValue: valueName), value: score))
        }
        return tagsData
    }
}
