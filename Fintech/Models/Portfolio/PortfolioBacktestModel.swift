//
//  PortfolioBacktestModel.swift
//  Fintech
//
//  Created by broadstone on 25/08/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import Foundation

struct PortfolioBacktestModel {
    let success: Bool
    let message: String
    let data: PortfolioBacktestData
}
struct PortfolioBacktestData {
    let backtest: PortfolioBacktest
    let performance: [BacktestPerformance]
    let risk: PortfolioBacktestRisk
}

struct PortfolioBacktest {
    let amount: Double
    let cash: Double
    let endDate: Int64
    let startDate: Int64
}

struct BacktestPerformance {
    let portfolioName: String
    let portfolioReturn: Double
}

struct PortfolioBacktestRisk {
    let beta: Double
    let volatility: Double
}
