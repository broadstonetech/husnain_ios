//
//  SPManager.swift
//  Fintech
//
//  Created by MacBook-Mubashar on 22/08/2019.
//  Copyright © 2019 Broadstone Technologies. All rights reserved.
//

import Foundation

class SPManager : NSObject {
    struct SPManagerKeys {
        static let USERNAME_FOR_BIOMETRIC_KEY = "USERNAME_FOR_BIOMETRIC_KEY"
        static let USER_PASS_FOR_BIOMETRIC_KEY = "USER_PASS_FOR_BIOMETRIC_KEY"
        static let USER_VIEW_CHARITY_FIRST_TIME_KEY = "USER_VIEW_CHARITY_FIRST_TIME_KEY"
        static let PUSH_NOTIFICATION_STATUS_KEY = "PUSH_NOTIFICATION_STATUS_KEY"
        static let SHOW_CONSENT_FOR_BIOMETRIC_KEY = "SHOW_CONSENT_FOR_BIOMETRIC_KEY"
        static let USER_EDUCATION_EXPERTISE_LEVEL_KEY = "USER_EDUCATION_EXPERTISE_LEVEL_KEY"
    }
    
    struct SPManagerDefaultVals {
        static let USERNAME_FOR_BIOMETRIC_DEF_VAL = ""
        static let USER_PASS_FOR_BIOMETRIC_DEF_VAL = ""
        static let USER_VIEW_CHARITY_FIRST_TIME_DEF_VAL = true
        static let PUSH_NOTIFICATION_STATUS_DEF_VAL = true
        static let SHOW_CONSENT_FOR_BIOMETRIC_DEF_VAL = true
    }
    
    static let sharedInstance = SPManager()
    
    //save current user app settings in nsuserdefault
    
    func saveCurrentUserNameForProfile(_ userName:String)  {
        
        UserDefaults.standard.set(userName, forKey: "userNameForProfile")
        UserDefaults.standard.synchronize()
        
    }

    
    func saveUserFullName(_ fullname:String)  {
        
        UserDefaults.standard.set(fullname, forKey: "fullname")
        UserDefaults.standard.synchronize()
        
    }
    func saveUserLoginStatus(isLogin:Bool)  {
        
        UserDefaults.standard.set(isLogin, forKey: "isLogin")
        UserDefaults.standard.synchronize()
        
    }

    func saveCurrentUserPassword(_ password:String)  {
        
        UserDefaults.standard.set(password, forKey: "password")
        UserDefaults.standard.synchronize()
        
    }
    func saveCurrentUserEmail(_ email:String)  {
        
        UserDefaults.standard.set(email, forKey: "email")
        UserDefaults.standard.synchronize()
        
        
    }
    
    func saveCurrentUserNamespace(_ namespace:String)  {
        
        UserDefaults.standard.set(namespace, forKey: "namespace")
        UserDefaults.standard.synchronize()
        
    }
    
    func saveCurrentUserType(_ namespace:String)  {
        
        UserDefaults.standard.set(namespace, forKey: "userType")
        UserDefaults.standard.synchronize()
        
    }
    
    //get current user app settings
    func saveCurrentUserUsername(_ userName:String)  {
        
        UserDefaults.standard.set(userName, forKey: "userName")
        UserDefaults.standard.synchronize()
        
    }
    func getCurrentUserUsername() -> String {
        var userName = UserDefaults.standard.value(forKey: "userName") as! String?
        if(userName == nil)
        {
            userName = ""
        }
        return userName!
    }
    
    
    func saveUserLoginStatus() -> Bool  {
        
        let status = UserDefaults.standard.value(forKey: "isLogin") as! Bool?
        return status ?? false
        
    }
    
    
    func saveUserPortfolioStatus(isPortfolioComplete:Bool)  {
    
    UserDefaults.standard.set(isPortfolioComplete, forKey: "isPortfolioComplete")
    UserDefaults.standard.synchronize()
        
    }
    
    func getUserPortfolioStatus() -> Bool  { //to check either portfolio is complete or not
        
        let status = UserDefaults.standard.value(forKey: "isPortfolioComplete") as! Bool?
        return status ?? false
        
    }
    
    func getCurrentUserNameForProfile() -> String {
        var userName = UserDefaults.standard.value(forKey: "userNameForProfile") as! String?
        if(userName == nil)
        {
            userName = ""
        }
        return userName!
    }
    func getCurrentUserFullname() -> String {
        var fullname = UserDefaults.standard.value(forKey: "fullname") as! String?
        if(fullname == nil)
        {
            fullname = ""
        }
        return fullname!
    }
    
    func getCurrentToken() -> String {
        var token = UserDefaults.standard.value(forKey: "token") as! String?
        if(token == nil)
        {
            token = ""
        }
        return token!
    }
    func getCurrentUserEmail() -> String {
        if UserDefaults.standard.object(forKey: "email") != nil {
        let email = UserDefaults.standard.value(forKey: "email") as! String ?? ""
        return email
        }
        return ""
    }
    func getCurrentUserPostKey() -> String {
        var postKey = UserDefaults.standard.value(forKey: "postkey") as! String?
        if(postKey == nil)
        {
            postKey = ""
        }
        return postKey!
    }
    func getCurrentUserPassword() -> String {
        let password = UserDefaults.standard.value(forKey: "password") as! String
        return password
    }
    
    func getCurrentUserNamespace() -> String {
        let namespace = UserDefaults.standard.value(forKey: "namespace") as? String
        return namespace ?? ""
    }
    
    func getCurrentUserType() -> String {
        let type = UserDefaults.standard.value(forKey: "userType") as! String
        return type
    }
    
    //save shared url opened in app -- URL SCHEMA
    func saveCurrentTappedSocialURL(_ url:String)  {
        
        UserDefaults.standard.set(url, forKey: "social_shared_url")
        UserDefaults.standard.synchronize()
        
    }
    
    func getCurrentTappedSocialURL() -> String {
        var sharedURL = UserDefaults.standard.value(forKey: "social_shared_url") as! String?
        if(sharedURL == nil)
        {
            sharedURL = ""
        }
        return sharedURL!
    }
    
    func saveCurrentUserQuestionaireExpiry(expiry:String)  {
        
        UserDefaults.standard.set(expiry, forKey: "questionaire_expiry")
        UserDefaults.standard.synchronize()
        
    }
    
    func getCurrentUserQuestionaireExpiry() -> String {
        var expiryStatus = UserDefaults.standard.value(forKey: "questionaire_expiry") as! String?
        if(expiryStatus == nil)
        {
            expiryStatus = "0"
        }
        return expiryStatus!
    }
    
    
    func saveCurrentUserQuestionaireStatus(status:String)  {
        
        UserDefaults.standard.set(status, forKey: "questionaire_status")
        UserDefaults.standard.synchronize()
        
    }
    
    func getCurrentUserQuestionaireStatus() -> String {
        var status = UserDefaults.standard.value(forKey: "questionaire_status") as! String?
        if(status == nil)
        {
            status = "pending"
        }
        return status!
    }
    
    func clearUserData()  {
        UserDefaults.standard.removeObject(forKey: "userName")
        UserDefaults.standard.removeObject(forKey: "email")
        UserDefaults.standard.removeObject(forKey: "password")
        UserDefaults.standard.removeObject(forKey: "namespace")
        UserDefaults.standard.removeObject(forKey: "token")
        UserDefaults.standard.synchronize()
    }
    
    func clearUserAppSettings()  {
        UserDefaults.standard.removeObject(forKey: "userNameForProfile")
        UserDefaults.standard.synchronize()
        UserDefaults.standard.removeObject(forKey: "fullname")
        UserDefaults.standard.synchronize()
    }
    
    func setUserNameForBiometric(userName:String)  {
        
        UserDefaults.standard.set(userName, forKey: SPManagerKeys.USERNAME_FOR_BIOMETRIC_KEY)
        UserDefaults.standard.synchronize()
    }
    
    func setUserPassForBiometric(pass:String)  {
        
        UserDefaults.standard.set(pass, forKey: SPManagerKeys.USER_PASS_FOR_BIOMETRIC_KEY)
        UserDefaults.standard.synchronize()
    }
    
    func getUserNameForBiometric() -> String {
        
        return UserDefaults.standard.string(forKey: SPManagerKeys.USERNAME_FOR_BIOMETRIC_KEY) ?? SPManagerDefaultVals.USERNAME_FOR_BIOMETRIC_DEF_VAL
    }
    
    func getUserPassForBiometric() -> String {
        
        return UserDefaults.standard.string(forKey: SPManagerKeys.USER_PASS_FOR_BIOMETRIC_KEY) ?? SPManagerDefaultVals.USER_PASS_FOR_BIOMETRIC_DEF_VAL 
        
    }
    
    func isUserDetailSavedForBiometric() -> Bool {
        let user = KeyChain.retrieveUserFromKeyChain()
        if user == nil {
            return false
        }else {
            return true
        }
    }
    
    func shouldShowConsetDialogForBiometric() -> Bool {
        let user = KeyChain.retrieveUserFromKeyChain()
        if user == nil {
            return true
        }else {
            return false
        }
    }
    
    func setShowConsetDialogForBiometric(_ flaog: Bool) {
        UserDefaults.standard.set(flag, forKey: SPManagerKeys.SHOW_CONSENT_FOR_BIOMETRIC_KEY)
        UserDefaults.standard.synchronize()
    }
    
    
    
    func setIsUserViewingCharityForFirstTime(isViewingFirstTime : Bool)  {
        
        UserDefaults.standard.set(isViewingFirstTime, forKey: SPManagerKeys.USER_VIEW_CHARITY_FIRST_TIME_KEY)
        UserDefaults.standard.synchronize()
    }
    
    func isUserViewingCharityForFirstTime() -> Bool {
        
        return UserDefaults.standard.bool(forKey: SPManagerKeys.USER_VIEW_CHARITY_FIRST_TIME_KEY) ?? SPManagerDefaultVals.USER_VIEW_CHARITY_FIRST_TIME_DEF_VAL
    }
    
    func getCurrentFirebaseId() -> String {

            if UserDefaults.standard.object(forKey: "firebaseId") != nil {

            let firebaseId = UserDefaults.standard.value(forKey: "firebaseId") as! String ?? ""

            return firebaseId

            }

            return ""

        }
    
    func saveFirebaseid(_ firebaseId: String){

            UserDefaults.standard.set(firebaseId, forKey: "firebaseId")

            UserDefaults.standard.synchronize()

    }
    
    func setPushNotificationStatus(_ value: Bool){
        UserDefaults.standard.set(value, forKey: SPManagerKeys.PUSH_NOTIFICATION_STATUS_KEY)
        UserDefaults.standard.synchronize()
    }

    func getPushNotificationStatus() -> Bool {
        
        return UserDefaults.standard.bool(forKey: SPManagerKeys.PUSH_NOTIFICATION_STATUS_KEY)
    }
    
    func setUserEducationExpertiseLevel(_ value: String) {
        UserDefaults.standard.set(value, forKey: SPManagerKeys.USER_EDUCATION_EXPERTISE_LEVEL_KEY)
        UserDefaults.standard.synchronize()
    }
    
    func getUserEducationExpertiseLevel() -> String {
        return UserDefaults.standard.string(forKey: SPManagerKeys.USER_EDUCATION_EXPERTISE_LEVEL_KEY) ?? EducationExpertiseValues.levelNotSet
    }
}

