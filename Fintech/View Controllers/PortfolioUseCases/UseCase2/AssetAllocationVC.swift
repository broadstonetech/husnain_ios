//
//  AssetAllocationVC.swift
//  Fintech
//
//  Created by broadstone on 09/08/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import UIKit

struct AssetAllocationStruct {
    var symbol: String
    var allocation: Double
}

class AssetAllocationVC: PortfolioParent {
    
    var delegate: AssetsUpdateDelegate?
    
    var tickersInCart = [PortfolioTickersData]()
    
    var allocations = [AssetAllocationStruct]()
    
    var useCaseType: UseCaseType!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var generatePortfolioBtn: UIButton!
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var portfolioNameLabel: UILabel!
    @IBOutlet weak var portfolioNameTF: UITextField!
    
    var update = false
    var portfolioId: Int!
    var portfolioName: String!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.delegate = self
        
        tableView.delegate = self
        tableView.dataSource = self
        
        if !update {
            for i in 0..<tickersInCart.count {
                allocations.append(AssetAllocationStruct.init(symbol: tickersInCart[i].symbol, allocation: 0.0))
            }
            createRightButton(title: getLocalizedString(key: "_next"), action: #selector(generatePortfolioPressed(_:)))
            
        }else {
            createRightButton(title: getLocalizedString(key: "_update"), action: #selector(generatePortfolioPressed(_:)))
            if portfolioName != nil {
                self.portfolioNameTF.text = portfolioName
            }
        }
        
        
        tableView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        setNavBarTitile(title: "Back", topItem: getLocalizedString(key: "asset_allocation"))
    }
    
    override func willMove(toParent parent: UIViewController?) {
        self.delegate?.onCartUpdate(tickersInCart: tickersInCart, proceedToAlloc: false)
    }
    
    @IBAction func generatePortfolioPressed(_ sender: UIButton){
        if isAlloc100Percent() {
            let name = portfolioNameTF.text ?? ""
            if  name.isEmpty {
                self.view.makeToast(getLocalizedString(key: "enter_portfolio_name"))
            }else {
                if update {
                    generatePortfolioAPI(url: ApiUrl.UPDATE_PORTFOLIO_ASSET, params: getUpdateApiParams(), useCaseType: useCaseType, for: false)
                }else {
                    if useCaseType == UseCaseType.UseCase2 {
                        generatePortfolioAPI(url: ApiUrl.INTERNAL_PORTFOLIO_IMPACT, params: getApiParams(), useCaseType: .UseCase2, for: update)
                    }else if useCaseType == UseCaseType.UseCase3 {
                        generatePortfolioAPI(url: ApiUrl.INTERNAL_PORTFOLIO_IMPACT, params: getApiParams(), useCaseType: .UseCase3, for: update)
                    }
                }
            }
        }else {
            self.view.makeToast(getLocalizedString(key: "total_100"))
        }
    }
    
    private func isAlloc100Percent() -> Bool {
        var percent = 0.0
        for alloc in allocations {
            percent += alloc.allocation
        }
        
        if percent >= 99 && percent <= 100.0 {
            return true
        }
        else {
            return false
        }
    }
    
    private func getApiParams() -> [String: AnyObject]{
        var assets = [AnyObject]()
        for i in allocations {
            if i.allocation > 0.0 {
                let asset = ["ticker": i.symbol,
                             "allocation": i.allocation] as [String: AnyObject]
                assets.append(asset as AnyObject)
            }
        }
        
        var portfolioType: String
        if useCaseType == UseCaseType.UseCase2 {
            portfolioType = "ticker_based"
        }else {
            portfolioType = "value_based"
        }
        
        let data = ["portfolio_name": (portfolioNameTF.text ?? "") as AnyObject,
                    "assets": assets as AnyObject,
                    "portfolio_type": portfolioType as AnyObject]
        
        return ["namespace": SPManager.sharedInstance.getCurrentUserNamespace(),
                "data": data] as [String: AnyObject]
    }
    
    private func getUpdateApiParams() -> [String: AnyObject]{
        var assets = [AnyObject]()
        for i in allocations {
            if i.allocation > 0.0 {
                let asset = ["ticker": i.symbol,
                             "allocation": i.allocation] as [String: AnyObject]
                assets.append(asset as AnyObject)
            }
        }
        
        let data = ["portfolio_name": (portfolioNameTF.text ?? "") as AnyObject,
                    "assets": assets as AnyObject,
                    "portfolio_id": portfolioId as AnyObject]
        
        return ["namespace": SPManager.sharedInstance.getCurrentUserNamespace(),
                "data": data] as [String: AnyObject]
    }
    
    
}
extension AssetAllocationVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return tickersInCart.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //let cell = tableView.dequeueReusableCell(withIdentifier: "SymbolAllocationCell") as! SymbolAllocationCell
        let cell = tableView.dequeueReusableCell(withIdentifier: "SymbolAllocationCell", for: indexPath) as! SymbolAllocationCell
        cell.symbolLabel.text = tickersInCart[indexPath.section].symbol
        cell.companyLabel.text = tickersInCart[indexPath.section].CompanyName
        print(indexPath.section)
        cell.allocationTF.tag = indexPath.section
        print(indexPath.section)
        if allocations[indexPath.section].allocation > 0.0 {
            cell.allocationTF.text = FintechUtils.sharedInstance.doubleToPercentageOutput(doubleVal: allocations[indexPath.section].allocation)
        }else {
            cell.allocationTF.text = ""
            cell.allocationTF.placeholder = getLocalizedString(key: "_weight")
        }
        cell.allocationTF.delegate = self
        
        return cell
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        let tag = textField.tag
        if textField.text!.isEmpty {
            print("Empty")
            allocations[tag].allocation = 0.0
        }
        else {
            let text = FintechUtils.sharedInstance.parseDouble(strValue: textField.text ?? "")
            let alloc = FintechUtils.sharedInstance.doubleToPercentageOutput(doubleVal: text)
            allocations[tag].allocation = FintechUtils.sharedInstance.parseDouble(strValue: alloc)
        }
        
        tableView.reloadData()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        if textField.keyboardType == .decimalPad {
            let regex = try! NSRegularExpression(pattern: "^(^[0-9]{1,3})*([.,][0-9]{0,2})?$", options: .caseInsensitive)

            if let newText = (textField.text as NSString?)?.replacingCharacters(in: range, with: string) {
                return regex.firstMatch(in: newText, options: [], range: NSRange(location: 0, length: newText.count)) != nil

            } else {
                return false
            }
        }else {
            return false
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 0
        }
        return 20
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int)
    {
        view.tintColor = #colorLiteral(red: 0.9411764706, green: 0.9411764706, blue: 0.9411764706, alpha: 1)
        view.backgroundColor = #colorLiteral(red: 0.9411764706, green: 0.9411764706, blue: 0.9411764706, alpha: 1)
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let delete = UIContextualAction(style: .destructive, title:  getLocalizedString(key: "_delete"), handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in

            self.removeFromCart(rowIndex: indexPath.section)
            success(true)
            
            
        })
        delete.backgroundColor = #colorLiteral(red: 1, green: 0.231372549, blue: 0.1882352941, alpha: 1)
        if #available(iOS 13.0, *) {
            delete.image = UIImage(systemName: "xmark.bin")
        } else {
            // Fallback on earlier versions
        }

        return UISwipeActionsConfiguration(actions: [delete])
    }
    
    private func removeFromCart(rowIndex: Int) {
        self.allocations.removeAll(where: {
            $0.symbol == tickersInCart[rowIndex].symbol
        })
        self.tickersInCart.remove(at: rowIndex)
        
        self.tableView.reloadData()
        if tickersInCart.count > 0 {
            
        }
        else {
            self.PopVC()
            self.delegate?.cartDeleted()
        }
        
    }
}
