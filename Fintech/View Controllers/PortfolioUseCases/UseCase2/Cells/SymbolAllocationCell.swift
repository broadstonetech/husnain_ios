//
//  SymbolAllocationCell.swift
//  Fintech
//
//  Created by broadstone on 09/08/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import UIKit

class SymbolAllocationCell: UITableViewCell, UITextFieldDelegate {
    
    @IBOutlet weak var parentView: UIView!
    @IBOutlet weak var symbolLabel: UILabel!
    @IBOutlet weak var companyLabel: UILabel!
    @IBOutlet weak var allocationLabel: UILabel!
    @IBOutlet weak var allocationTF: UITextField!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
