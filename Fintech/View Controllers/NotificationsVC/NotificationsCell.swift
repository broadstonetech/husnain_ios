//
//  NotificationsCell.swift
//  Fintech
//
//  Created by MacBook-Mubashar on 24/07/2019.
//  Copyright © 2019 Broadstone Technologies. All rights reserved.
//

import UIKit

class NotificationsCell: UITableViewCell {

    @IBOutlet var notificationHeading_lbl: UILabel!
    @IBOutlet var notificationDesc_lbl: UILabel!
    @IBOutlet var imgImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
