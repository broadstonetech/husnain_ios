//
//  PieChartCell.swift
//  Vesgo-PF
//
//  Created by Billal on 01/02/2021.
//

import UIKit
import Charts

class PieChartCell: UITableViewCell,ChartViewDelegate {
    
    @IBOutlet weak var pieChartView: PieChartView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
