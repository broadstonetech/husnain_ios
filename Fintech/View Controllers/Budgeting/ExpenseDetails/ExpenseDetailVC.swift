//
//  ExpenseDetailVC.swift
//  vesgo
//
//  Created by Apple on 05/01/2021.
//  Copyright © 2021 Apple. All rights reserved.
//

import UIKit
var cardbackgroundimg = ["visa","master-card"]
var cardTitle = ["Visa","Master"]
var username = ["Caleb", "Caleb"]
var cardnumber = ["**** **** **** 4242","**** **** **** 4444"]
var expairedate = ["1/2025","1/2025",""]
var amount = ["$1200","$400"]
var headingtableview = ["Super Market","H&M","A&B","C&D"]
var amountableview = ["$100","$200","$300","$400"]
var categorySelected = ""
var budgetingHistoryFrequencyType = BudgetingConstants.FREQUENCY_TYPE_DAY
var selectData = ""
var currentindex = -1
var CURENT_VALUE_USER_SELECT = [String]()


struct TransactionClassificationData {
    var transactionId = ""
    var category = ""
}

class ExpenseDetailVC: Parent,UIPickerViewDelegate, UIPickerViewDataSource {
    var headingValues = ["Mortgage/Rent", "Online services", "Education", "Healthcare", "Dining out", "Groceries", "Insurance", "Utilities", "Misc."]
    
    //var pickerDataValues: [String] = [String]()
    
    @IBOutlet weak var doneBtnView: UIView!
    var transactionHistoryDetails: CategoryTransactionHistoryDetailsModel!
    var budgetingCategories: BudgetingCategoriesClass!
    var categoriesDict = OrderedDictionary<String, String>()
    var selectedCategoriesDict = OrderedDictionary<Int, String>()
    var tableViewSelectedRowIndex = 0
    
    var categories = [String]()
    
    var classificationData = [TransactionClassificationData]()
    
    @IBOutlet weak var pickerview: UIPickerView!
    @IBOutlet weak var tableview_list: UITableView!
    
    @IBOutlet weak var picker_view: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ArrayLists.sharedInstance.TransactionHistoryDetailList.removeAll()
        if categorySelected.lowercased() == "misc" {
            getBudgetingCategoriesAPI()
            doneBtnView.isHidden = false
        }else{
            
            doneBtnView.isHidden = true
        }
        categoryTransactionHistoryDetailscallapi()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        setNavBarTitile(title: "Spending details", topItem: "Spending details")
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    
    @IBAction func cancel_pressed(_ sender: Any) {
        picker_view.isHidden = true
        
    }
    @IBAction func done_pressed(_ sender: Any) {
        //CURENT_VALUE_USER_SELECT[currentindex] = selectData
        
        tableview_list.reloadData()
        picker_view.isHidden = true
        
    }
    
    
    @IBAction func doneButtonPressed(_ sender: Any) {
        submitClassifiedTransactions()
        setAnalyticsAction(ScreenName: "ExpenseDetails", method: "submitClassifiedTransactions")
    }
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return categories.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.categories[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        //selectData = self.categoriesDict.elementAt(row)!.value // selected item
        //selectedCategoriesDict.updateValue(self.categories[row], forKey: tableViewSelectedRowIndex)
        classificationData[tableViewSelectedRowIndex].category = self.categories[row]
        
    }
    
    
    private func getTransactionDetailsAPI(){
        
    }
    
    func  categoryTransactionHistoryDetailscallapi(){
        self.view.activityStartAnimating()
        let url = ApiUrl.GET_BUDGETING_TRANSACTION_HISTORY_DETAILS
        let namespace = SPManager.sharedInstance.getCurrentUserNamespace()
        let categoryName = categorySelected
        
        let params:Dictionary<String,AnyObject> =
            ["namespace" : namespace  as AnyObject,
             "category_name": categoryName as AnyObject,
             "frequency": budgetingHistoryFrequencyType as AnyObject]
        print(params)
        
        APIManager.sharedInstance.postRequest(serviceName: url, sendData: params, success: { (response) in
            print(response)
            
            if APIManager.sharedInstance.isSuccess {
                let res = response["data"] as AnyObject
                DispatchQueue.main.async {
                    
                    self.fetchTransactionDetails(response: res as! [String : AnyObject])
                }
            }else{
                
                DispatchQueue.main.async { [self] in
                    if response["message"] != nil {
                        self.showAlert(response["message"] as! String)
                    } else {
                        self.showAlert(ConstantStrings.NO_RESULT_FOUND_ALERT_TEXT)
                    }
                }
            }
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
        }, failure: { (data) in
            print(data)
            DispatchQueue.main.async { [self] in
                view.activityStopAnimating()
            }
            if APIManager.sharedInstance.error400 {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
            }
            if(APIManager.sharedInstance.error401)
            {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
            }else{
                self.showAlert(ConstantStrings.Error_msg_generic)
            }
        })
        
    }
    
    //Mark:- Submit Classified Transactions
    private func submitClassifiedTransactions(){
        self.view.activityStartAnimating()
        let url = ApiUrl.CLASSIFY_TRANSACTION_HISTORY
        let namespace = SPManager.sharedInstance.getCurrentUserNamespace()
        
        var transactions = Array<AnyObject>()
        
        for i in 0..<classificationData.count {
            
            if classificationData[i].category != "" {
                let transactionParam: Dictionary<String, AnyObject> =
                    ["category_name": classificationData[i].category as AnyObject,
                     "transaction_id": classificationData[i].transactionId as AnyObject]
                transactions.append(transactionParam as AnyObject)
            }
        }
        
        let params:Dictionary<String,AnyObject> =
            ["namespace" : namespace  as AnyObject,
             "transaction_list": transactions as AnyObject]
        print(params)
        
        APIManager.sharedInstance.postRequest(serviceName: url, sendData: params, success: { (response) in
            print(response)
            
            if APIManager.sharedInstance.isSuccess {
                DispatchQueue.main.async { [self] in
                    PopVC()
                }
            }else{
                
                DispatchQueue.main.async { [self] in
                    if response["message"] != nil {
                        self.showAlert(response["message"] as! String)
                    } else {
                        self.showAlert(ConstantStrings.NO_RESULT_FOUND_ALERT_TEXT)
                    }
                }
            }
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
        }, failure: { (data) in
            print(data)
            DispatchQueue.main.async { [self] in
                view.activityStopAnimating()
            }
            if APIManager.sharedInstance.error400 {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
            }
            if(APIManager.sharedInstance.error401)
            {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
            }else{
                self.showAlert(ConstantStrings.Error_msg_generic)
            }
        })
    }
    
    //Mark:- Fetch Details
    private func fetchTransactionDetails(response: [String: AnyObject]){
        
        ArrayLists.sharedInstance.TransactionHistoryDetailList.removeAll()
        
        let transactionHistory = response["tansactionHistory"] as? [AnyObject]
        classificationData.removeAll()
        for i in 0..<transactionHistory!.count {
            let history = transactionHistory?[i] as? [String: AnyObject]
            
            let amount = history?["amount"] as? Double ?? 0.0
            
            if amount != 0.0 {
                let transactionId = (history?["transaction_id"] as? String) ?? ""
                let categoryName = history?["category_name"] as? String
                let subCategory = ""
                let merchantName = history?["merchant_name"] as? String
                let date = history?["date"] as? Int64
                var transactionDetail: TransactionDetails
                if categorySelected.lowercased() == "misc" {
                    transactionDetail = TransactionDetails.init(categoryName: "", subCategoryName: "", amountSpent: amount, data: date!, merchantName: merchantName!, transactionId: transactionId)
                }else{
                    transactionDetail = TransactionDetails.init(categoryName: categoryName!, subCategoryName: subCategory, amountSpent: amount, data: date!, merchantName: merchantName!, transactionId: transactionId)
                }
                
                
                classificationData.append(TransactionClassificationData.init(transactionId: transactionId, category: ""))
                ArrayLists.sharedInstance.TransactionHistoryDetailList.append(transactionDetail)
                
            }
        }
        
        pickerview.reloadAllComponents()
        tableview_list.reloadData()
        
        
    }
}
extension ExpenseDetailVC:UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return cardbackgroundimg.count
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ExpenseDetailCell
        
        cell.background_card_img.image = UIImage(named: cardbackgroundimg[indexPath.row])
        cell.username_lbl.text = username[indexPath.row]
        cell.amount_lbl.text = amount[indexPath.row]
        cell.card_number_lbl.text = cardnumber[indexPath.row]
        
        return cell
        
    }
    
    
}

//MARK: - tableview

extension ExpenseDetailVC: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ArrayLists.sharedInstance.TransactionHistoryDetailList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! ExpendDetailsTableviewCell
        
        
        let transactionHistory = ArrayLists.sharedInstance.TransactionHistoryDetailList[indexPath.row]
        
        
        if categorySelected.lowercased() == "misc" {
            if classificationData[indexPath.row].category == "" {
                cell.heading_lbl.text = "Select category"
            }else{
                cell.heading_lbl.text = classificationData[indexPath.row].category
            }
            
        }else {
            if transactionHistory.subCategoryName.isEmpty{
                cell.heading_lbl.text = transactionHistory.categoryName
            }else{
                cell.heading_lbl.text = transactionHistory.subCategoryName
            }
        }
        
        if transactionHistory.merchantName.isEmpty || transactionHistory.merchantName == "" {
            cell.merchantNameLbl.text = ""
        }else{
            cell.merchantNameLbl.text = transactionHistory.merchantName
        }
        
        cell.img_main.image = FintechUtils.sharedInstance.getCaetgoryImageForBudgeting(value: transactionHistory.categoryName.lowercased())
        cell.imgBgView.backgroundColor = FintechUtils.sharedInstance.getColoredBackgroundForBudgeting(rowPosition: indexPath.row)
        
        cell.amount_lbl.text = "$" + FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: transactionHistory.amountSpent)
        cell.descripation_lbl.text = FintechUtils.sharedInstance.getDateInUSLocale(timestamp: transactionHistory.data/1000)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if categorySelected.lowercased() == "misc" {
            tableViewSelectedRowIndex = indexPath.row
            picker_view.isHidden = false
        }
        
        
    }
}
//MARK: Get budgeting Categories
extension ExpenseDetailVC {
    func  getBudgetingCategoriesAPI(){
        self.view.activityStartAnimating()
        let url = ApiUrl.GET_BUDGETING_CATEGORIES
        let namespace = SPManager.sharedInstance.getCurrentUserNamespace()
        let params = ["namespace": namespace] as [String: AnyObject]
        APIManager.sharedInstance.postRequest(serviceName: url, sendData: params, success: {
            (response) in
            
            self.budgetingCategories = self.fetchCategoriesApiResponse(response: response)
            
            DispatchQueue.main.async { [self] in
                view.activityStopAnimating()
            }
            
            
        }, failure: {
            (err) in
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
            if APIManager.sharedInstance.error400 {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
            }
            if(APIManager.sharedInstance.error401)
            {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
            }else{
                self.showAlert(ConstantStrings.Error_msg_generic)
            }
        })
    }
    
    private func fetchCategoriesApiResponse(response: [String: AnyObject]) -> BudgetingCategoriesClass{
        
        let success = response["success"] as! Bool
        let message = response["message"] as! String
        
        let data = response["data"] as! [String: AnyObject]
        let needsBucket = data["needs_bucket"] as! [String]
        let wantsBucket = data["wants_bucket"] as! [String]
        let havesBucket = data["haves_bucket"] as! [String]
        
        let categoriesData = BudgetingCategoriesData(needsBucketCategories: needsBucket, wantsBucketCategories: wantsBucket, havesBucketCategories: havesBucket)
        
        categories.append(contentsOf: needsBucket)
        categories.append(contentsOf: wantsBucket)
        categories.append(contentsOf: havesBucket)
        
        DispatchQueue.main.async {
            self.pickerview.reloadAllComponents()
        }
        
        return BudgetingCategoriesClass(success: success, message: message, data: categoriesData)
        
    }
}
