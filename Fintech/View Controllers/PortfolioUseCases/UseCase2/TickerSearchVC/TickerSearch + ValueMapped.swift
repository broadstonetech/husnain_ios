//
//  TickerSearch + ValueMapped.swift
//  Fintech
//
//  Created by broadstone on 14/03/2022.
//  Copyright © 2022 Broadstone Technologies. All rights reserved.
//

import Foundation
import UIKit
extension AddAndSearchSymbolVC: ValueMappedApiDelegate {
    @IBAction func envBtnPressed(_ sender: UIButton){
        envBtnFunctionality()
    }
    
    func envBtnFunctionality(){
        selectedValue = .Env
        selectedBucket = .leader
        
        let cacheModel = getCacheDataIfAvailable(valueName: selectedValue, bucketName: selectedBucket)
        
        if cacheModel == nil {
            callValueMappedTickerAPI(bucketType: "leader", isNavigatingToNextVC: false, valueName: getMappedSelectedValue(), risk: 23)
        }else {
            self.valueMappedTickersModel = cacheModel!.model
            DispatchQueue.main.async {
                self.valueMappedTableView.reloadData()
            }
        }
        setLabelTexts()
    }
    
    @IBAction func socBtnPressed(_ sender: UIButton){
        selectedValue = .Soc
        selectedBucket = .leader
        
        let cacheModel = getCacheDataIfAvailable(valueName: selectedValue, bucketName: selectedBucket)
        
        if cacheModel == nil {
            callValueMappedTickerAPI(bucketType: "leader", isNavigatingToNextVC: false, valueName: getMappedSelectedValue(), risk: 23)
        }else {
            self.valueMappedTickersModel = cacheModel!.model
            DispatchQueue.main.async {
                self.valueMappedTableView.reloadData()
            }
        }
        setLabelTexts()
    }
    
    @IBAction func govBtnPressed(_ sender: UIButton){
        selectedValue = .Gov
        selectedBucket = .leader
        
        let cacheModel = getCacheDataIfAvailable(valueName: selectedValue, bucketName: selectedBucket)
        
        if cacheModel == nil {
            callValueMappedTickerAPI(bucketType: "leader", isNavigatingToNextVC: false, valueName: getMappedSelectedValue(), risk: 23)
        }else {
            self.valueMappedTickersModel = cacheModel!.model
            DispatchQueue.main.async {
                self.valueMappedTableView.reloadData()
            }
        }
        setLabelTexts()
    }
    
    @IBAction func leadersBtnPressed(_ sender: UIButton){
        selectedBucket = .leader
        
        let cacheModel = getCacheDataIfAvailable(valueName: selectedValue, bucketName: selectedBucket)
        
        if cacheModel == nil {
            callValueMappedTickerAPI(bucketType: "leader", isNavigatingToNextVC: false, valueName: getMappedSelectedValue(), risk: 23)
        }else {
            self.valueMappedTickersModel = cacheModel!.model
            DispatchQueue.main.async {
                self.valueMappedTableView.reloadData()
            }
        }
        setLabelTexts()
    }
    
    @IBAction func averageBtnPressed(_ sender: UIButton){
        selectedBucket = .average
        
        let cacheModel = getCacheDataIfAvailable(valueName: selectedValue, bucketName: selectedBucket)
        
        if cacheModel == nil {
            callValueMappedTickerAPI(bucketType: "average", isNavigatingToNextVC: false, valueName: getMappedSelectedValue(), risk: 23)
        }else {
            self.valueMappedTickersModel = cacheModel!.model
            DispatchQueue.main.async {
                self.valueMappedTableView.reloadData()
            }
        }
        setLabelTexts()
    }
    
    @IBAction func laggardBtnPressed(_ sender: UIButton){
        selectedBucket = .laggard
        
        let cacheModel = getCacheDataIfAvailable(valueName: selectedValue, bucketName: selectedBucket)
        
        if cacheModel == nil {
            callValueMappedTickerAPI(bucketType: "laggard", isNavigatingToNextVC: false, valueName: getMappedSelectedValue(), risk: 23)
        }else {
            self.valueMappedTickersModel = cacheModel!.model
            DispatchQueue.main.async {
                self.valueMappedTableView.reloadData()
            }
        }
        setLabelTexts()
    }
    
    private func getMappedSelectedValue() -> String {
        if selectedValue == .Env {
            return "Environment"
        }else if selectedValue == .Soc {
            return "Social"
        }else {
            return "Governance"
        }
    }
    
    private func setLabelTexts(){
        envLabel.textColor =        .black
        socLabel.textColor =        .black
        govLabel.textColor =        .black
        leaderLabel.textColor =     .black
        averageLabel.textColor =    .black
        laggardLabel.textColor =    .black
        
        if selectedValue == .Env {
            envLabel.textColor = UIColor(named: "AppMainColor")
        } else if selectedValue == .Soc {
            socLabel.textColor = UIColor(named: "AppMainColor")
        }else {
            govLabel.textColor = UIColor(named: "AppMainColor")
        }
        
        if selectedBucket == .leader {
            leaderLabel.textColor = UIColor(named: "AppMainColor")
        }else if selectedBucket == .average {
            averageLabel.textColor = UIColor(named: "AppMainColor")
        }else {
            laggardLabel.textColor = UIColor(named: "AppMainColor")
        }
    }
    
    func getCacheDataIfAvailable(valueName: TickerValueType, bucketName: BucketType) -> ValueMappedTickersCacheData? {
        let isAvailable = cacheData.contains(where: {
            $0.valueName == valueName && $0.bucketType == bucketName
        })
        
        var data: ValueMappedTickersCacheData!
        if isAvailable {
            data = cacheData.first(where: {
                $0.bucketType == bucketName && $0.valueName == valueName
            })
            
        }
        
        return data
    }
    
    func apiCalled(valueName: String, bucketType: String, model: ValueMappedTickersModel) {
        cacheData.append(ValueMappedTickersCacheData.init(valueName: selectedValue, bucketType: selectedBucket, model: model))
        self.valueMappedTickersModel = model
        self.setLabelTexts()
        DispatchQueue.main.async {
            self.valueMappedTableView.reloadData()
        }
    }
}
