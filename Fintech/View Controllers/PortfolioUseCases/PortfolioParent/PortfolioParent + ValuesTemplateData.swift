//
//  PortfolioParent + ValuesTemplateData.swift
//  Fintech
//
//  Created by broadstone on 14/03/2022.
//  Copyright © 2022 Broadstone Technologies. All rights reserved.
//

import Foundation
import UIKit
struct PortfolioValuesTemplate {
    let template: String
    let localizedName: String
    let templateImage: UIImage
}

struct ScoreImprovementLinksStruct {
    let desc: String
    let link: String
}

extension PortfolioParent {
    func getPortfolioTemplates() -> [PortfolioValuesTemplate] {
        
        var data = [PortfolioValuesTemplate]()
        data.append(PortfolioValuesTemplate.init(template: "catholic", localizedName: getLocalizedString(key: "template_catholic"), templateImage: UIImage.init(named: "catholic_template")!))
        data.append(PortfolioValuesTemplate.init(template: "environment", localizedName: getLocalizedString(key: "template_env_imapct"), templateImage: UIImage.init(named: "env_template")!))
        data.append(PortfolioValuesTemplate.init(template: "govenance", localizedName: getLocalizedString(key: "gov"), templateImage: UIImage.init(named: "gov_template")!))
        data.append(PortfolioValuesTemplate.init(template: "islamic", localizedName: getLocalizedString(key: "template_islamic"), templateImage: UIImage.init(named: "islamic_template")!))
        data.append(PortfolioValuesTemplate.init(template: "methodist", localizedName: getLocalizedString(key: "template_methodist"), templateImage: UIImage.init(named: "methodist_template")!))
        data.append(PortfolioValuesTemplate.init(template: "peace", localizedName: getLocalizedString(key: "template_peace"), templateImage: UIImage.init(named: "peace_template")!))
        data.append(PortfolioValuesTemplate.init(template: "social", localizedName: getLocalizedString(key: "soc"), templateImage: UIImage.init(named: "soc_template")!))
        data.append(PortfolioValuesTemplate.init(template: "vegan", localizedName: getLocalizedString(key: "template_vegan"), templateImage: UIImage.init(named: "vegan_template")!))
        
        return data
    }
    
    func getScoreImprovementLinksData() -> [ScoreImprovementLinksStruct] {
        var data = [ScoreImprovementLinksStruct]()
        
        data.append(ScoreImprovementLinksStruct(desc: getLocalizedString(key: "awareness_is_the_first_step", comment: ""), link: ""))
        data.append(ScoreImprovementLinksStruct(desc: getLocalizedString(key: "score_link1_desc", comment: ""), link: "https://www.footprintcalculator.org"))
        data.append(ScoreImprovementLinksStruct(desc: getLocalizedString(key: "score_link2_desc", comment: ""), link: "https://www.green-e.org/certified-resources/carbon-offsets"))
        data.append(ScoreImprovementLinksStruct(desc: getLocalizedString(key: "score_link3_desc", comment: ""), link: "https://www.citizen.org"))
        data.append(ScoreImprovementLinksStruct(desc: getLocalizedString(key: "score_link4_desc", comment: ""), link: "https://www.ucsusa.org/"))
        data.append(ScoreImprovementLinksStruct(desc: getLocalizedString(key: "score_link5_desc", comment: ""), link: "https://freedomtothrive.org"))
        data.append(ScoreImprovementLinksStruct(desc: getLocalizedString(key: "score_link7_desc", comment: ""), link: "https://wren.co/"))
        data.append(ScoreImprovementLinksStruct(desc: getLocalizedString(key: "score_link8_desc", comment: ""), link: "https://goldstandard.org/"))
        data.append(ScoreImprovementLinksStruct(desc: getLocalizedString(key: "score_link6_desc", comment: ""), link: "https://en-roads.climateinteractive.org/scenario.html?v=2.7.29"))
        
        return data
    }
    
}
