//
//  QuestionnaireSummaryVC.swift
//  Fintech
//
//  Created by broadstone on 20/10/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import UIKit

class QuestionnaireSummaryVC: Parent {
    
    var tableViewData: [QuestionnaireCategoriesModel]!
    var selectedTemplate = ""
    
    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        self.setNavBarTitile(title: getLocalizedString(key: "_summary"), topItem: getLocalizedString(key: "_summary"))
    }
    
    @IBAction func changeBtnPressed(_ sender: UIButton) {
        PopVC()
    }
    
    @IBAction func submitBtnPressed(_ sender: UIButton) {
        showPortfolioTypeBottomSheet()
    }
    
    private func showPortfolioTypeBottomSheet(){
        let optionMenu = UIAlertController(title: nil, message: getLocalizedString(key: "select_portfolio_type"), preferredStyle: .actionSheet)
        
        let stocks = UIAlertAction(title: getLocalizedString(key: "stocks_only"), style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.submitInvestmentQuestionnaire(portfolioType: "stocks")
            
        })
        let etf = UIAlertAction(title: getLocalizedString(key: "etf_only"), style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.submitInvestmentQuestionnaire(portfolioType: "etf")
            
        })
        
        let vesgo = UIAlertAction(title: getLocalizedString(key: "let_vesgo_decide"), style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.submitInvestmentQuestionnaire(portfolioType: "both")
        })
        
        
        
        
        optionMenu.addAction(stocks)
        optionMenu.addAction(etf)
        //optionMenu.addAction(vesgo)
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    private func submitInvestmentQuestionnaire(portfolioType: String){
        view.activityStartAnimating()
        let url = ApiUrl.SET_PORTFOLIO_TYPE_API

        let namespace = SPManager.sharedInstance.getCurrentUserNamespace()
        print(namespace)

        let params: Dictionary<String, AnyObject> =
            ["namespace": namespace as AnyObject,
             "type": portfolioType as AnyObject]
        
        print(params)
        
        postManagerUser(method: "POST", isThisURLEmbdedData: false, urlString: url, timeoutInterval: NetworkConstants.INCREASED_TIMEOUT_INTERVAL, parameter: params,  success: {(value) in

            do {
                print(value)
                DispatchQueue.main.async {
                    SPManager.sharedInstance.saveUserPortfolioStatus(isPortfolioComplete : true)
                    if let tabbedbar = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "nav_bar_start") as? UINavigationController {
                            self.view.window?.rootViewController = tabbedbar
                        }
                }
            }
            catch {
                print("Error parsing response")
            }
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
        }, failure: {(err) in
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
            if APIManager.sharedInstance.error400{
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
            }
            if(APIManager.sharedInstance.error401) {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
            }else{
                self.showAlert(ConstantStrings.Error_msg_generic)
            }
        })
    }
}

extension QuestionnaireSummaryVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return tableViewData.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1 + tableViewData[section].questionsData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "QuestionnaireCategoryHeadingCell") as! QuestionnaireCategoryHeadingCell
            
            cell.headingLabel.text = tableViewData[indexPath.section].category
            
            return cell
        }else {
            let index = indexPath.row - 1
            let data = tableViewData[indexPath.section].questionsData[index]
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "QuestionnaireSummaryCell") as! QuestionnaireSummaryCell
            
            cell.questionLabel.text = data.question
            cell.answerLabel.text = FintechUtils.sharedInstance.ArrayToString(array: data.selectedAnswers)
            
            return cell
            
        }
    }
}
