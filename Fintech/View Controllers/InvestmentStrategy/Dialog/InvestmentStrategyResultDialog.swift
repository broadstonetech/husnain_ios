//
//  InvestmentStrategyResultDialog.swift
//  Fintech
//
//  Created by broadstone on 07/04/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import UIKit
import TagListView
class InvestmentStrategyResultDialog: UIView {

    @IBOutlet weak var parentView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var tagListView: TagListView!
    @IBOutlet weak var closeBtn: UIImageView!
    @IBOutlet weak var recalculateScoreBtn: UIButton!
    @IBOutlet weak var categoryImage: UIImageView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    private func commonInit(){
        Bundle.main.loadNibNamed("InvestmentStrategyResultsDialog", owner: self, options: nil)
        addSubview(parentView)
        parentView.frame = self.bounds
        parentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        
        recalculateScoreBtn.setTitle(getLocalizedString(key: "recalculate_score"), for: .normal)
        
        initTagListView()
    }
    
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    private func initTagListView(){
        tagListView.textFont = UIFont.systemFont(ofSize: 15)
        tagListView.tagBackgroundColor = .systemBlue
        tagListView.textColor = .white
    }

}
