//
//  PreMadePortfolioCell.swift
//  Fintech
//
//  Created by broadstone on 04/03/2022.
//  Copyright © 2022 Broadstone Technologies. All rights reserved.
//

import UIKit

class PreMadePortfolioCell: UITableViewCell {
    
    @IBOutlet weak var portfolioLabel: UILabel!
    @IBOutlet weak var impactLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
