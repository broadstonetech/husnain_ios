//
//  futurepredictionModel.swift
//  Fintech
//
//  Created by Apple on 30/09/2020.
//  Copyright © 2020 Broadstone Technologies. All rights reserved.
//

import Foundation

// MARK: - FuturepredictionModel
struct FuturepredictionModel: Codable {
    let success: Bool?
    let message: String?
    let data: [FuturePredictionData]?
}

// MARK: - Message
struct FuturePredictionData: Codable {
    let xAxis: Int64
    let percent25, percent50, percent75, percent90: Double?
    
    enum CodingKeys: String, CodingKey {
        case xAxis = "x_axis"
        case percent25 = "percent_25"
        case percent50 = "percent_50"
        case percent75 = "percent_75"
        case percent90 = "percent_90"
    }
}












//struct Message2: Codable {
//    let xAxis, percent25, percent50, percent75: Int?
//    let percent90: Int?
//
//    enum CodingKeys: String, CodingKey {
//        case xAxis = "x_axis"
//        case percent25 = "percent_25"
//        case percent50 = "percent_50"
//        case percent75 = "percent_75"
//        case percent90 = "percent_90"
//    }
//}
