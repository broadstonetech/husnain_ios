//
//  EducationalSectionVC.swift
//  Fintech
//
//  Created by MacBook-Mubashar on 24/09/2019.
//  Copyright © 2019 Broadstone Technologies. All rights reserved.
//
////#################################################################
//  In This get education data and add user level 
//    User level
//    Beginner
//    Intermediat
//    Expertise level
//    Take Quiz
//    getEducationalData  Api call
//################################################################

import UIKit
import Foundation
import Floaty
var award = Int()
var totalprenstage = Int()
var arrColor = [#colorLiteral(red: 0.5137254902, green: 0.5568627451, blue: 0.7764705882, alpha: 1),#colorLiteral(red: 0.4666666667, green: 0.7411764706, blue: 0.4549019608, alpha: 1),#colorLiteral(red: 0.8196078431, green: 0.9098039216, blue: 0.9607843137, alpha: 1),#colorLiteral(red: 0.4431372549, green: 0.768627451, blue: 0.8039215686, alpha: 1),#colorLiteral(red: 0, green: 0.631372549, blue: 0.6078431373, alpha: 1),#colorLiteral(red: 0.9294117647, green: 0.8588235294, blue: 0.7647058824, alpha: 1)]
var user = ""

let array1 = ["bonds","college_saving","retirement_planning","disability_insurance","annuities","etfs"]
var userlevel  = ""
var quizuserlevel = ""

class EducationalSectionVC : Parent {
    var tempdata = [EducationModel]()
    var data = [EducationModel]()
    @IBOutlet weak var profile_btn: UIButton!
    @IBOutlet weak var deepDivesTableview: UITableView!
    
    @IBOutlet weak var Level_VIew: UIView!
    @IBOutlet weak var floaty_btn: Floaty!
    
    @IBOutlet weak var takequiz_btn: UIButton!
    @IBOutlet weak var expert_btn: UIButton!
    @IBOutlet weak var intermediate_btn: UIButton!
    @IBOutlet weak var beginner_btn: UIButton!
    @IBOutlet weak var heading_expertieslevel_lbl: UILabel!
    
    @IBOutlet weak var unsureLabel: UILabel!
    //     MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
//        Dbname = "fintecheng"
//        UserDefaults.standard.set(Dbname, forKey: "Dbname")

        //self.floatybutton()
        floaty_btn.isHidden = true
        initTableViews()
        if NetworkReachability.isConnectedToNetwork() {
        } else {
            self.view.makeToast(ConstantStrings.NO_INTERNET_CONNECTION_FOUND)
        }
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        //Dbname = UserDefaults.standard.string(forKey: "Dbname") ?? ""
       //print(Dbname)
        Level_VIew.isHidden = false
        let db = Databasehandler()

        data = db.readEducation()

        if UserDefaults.standard.value(forKey: "userLevel" ) != nil{
        
            //Level_VIew.isHidden = false
            let value = UserDefaults.standard.value(forKey: "userLevel" )
                        userlevel = value as! String
                if userlevel == "Beginner" {
                    Level_VIew.isHidden = true
                }else  if userlevel == "Intermediate" {
                    Level_VIew.isHidden = true
                }else  if userlevel == "Expert" {
                    Level_VIew.isHidden = true
                }else{
                Level_VIew.isHidden = false
                }
                }else {
                Level_VIew.isHidden = false
                }
//        if let value = UserDefaults.standard.value(forKey: "userLevel" ){
//            userlevel = value as! String
//            print(userlevel)
//            Level_VIew.isHidden = true
//            print("aaaaaaaa")
//
//        }else{
//            Level_VIew.isHidden = false
//            print(userlevel)
//        }
        
             if userlevel != "takequiz"{
              self.sortdatacell1()
              }
              user = SPManager.sharedInstance.getCurrentUserEmail()
              if user == "" {
                //test
            user = "test"
        }
        sortdatacell1()

    }
    
    override func viewDidAppear(_ animated: Bool) {
        navigationController?.isNavigationBarHidden = false
        localization()
    }
    func sortdatacell1(){
        tempdata = []
        let edudta = data
        for i in edudta{
            if i.type == userlevel{
                let index = data.firstIndex(where: {$0.type == userlevel})
                
                
                let item = data.remove(at:index ?? index!)
                
                tempdata.append(item)
            }
        }
        data.insert(contentsOf: tempdata, at: 0)
        self.deepDivesTableview.reloadData()
    }
    func floatybutton() {
        
        if SPManager.sharedInstance.saveUserLoginStatus() {
            floaty_btn.isHidden = true
        }else{
            floaty_btn.buttonColor = #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1)
            floaty_btn.itemButtonColor = #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1)
            floaty_btn.buttonImage = UIImage(named: "floatyicon")
            floaty_btn.addItem("Sample Portfolios",icon: UIImage(named: "services-portfolio1")!, handler: { item in
                if let tabbedbar = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "nav_bar_sampleportfolio") as? UINavigationController {
                    UIApplication.shared.keyWindow?.rootViewController = tabbedbar
                }
            })
            floaty_btn.addItem("Account Setup",icon: UIImage(named: "baseline_person_add_black")!, handler: { item in
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "Main_login_page") as UIViewController
                UIApplication.shared.keyWindow?.rootViewController = vc
            })
            DispatchQueue.main.async {
                self.view.addSubview(self.floaty_btn)
                
            }
            
        }
    }
    
    func localization(){
        setNavBarTitile(title:getLocalizedString(key: "Education", comment: ""), topItem: getLocalizedString(key: "Education", comment: ""))
        profile_btn.setTitle(getLocalizedString(key: "My profile", comment: ""), for: .normal)
        let heading = getLocalizedString(key: "What is your expertise level", comment: "")
        heading_expertieslevel_lbl.text = heading
         intermediate_btn.setTitle(getLocalizedString(key: "intermediate", comment: ""), for: .normal)
         beginner_btn.setTitle(getLocalizedString(key: "beginner", comment: ""), for: .normal)
         expert_btn.setTitle(getLocalizedString(key: "expert", comment: ""), for: .normal)
          takequiz_btn.setTitle(getLocalizedString(key: "takequiz", comment: ""), for: .normal)
        unsureLabel.text = getLocalizedString(key: "if_you_are_unsure")
       
        navigationController?.navigationBar.barStyle = .blackOpaque
    }
    // MARK: - Class Functions
    func initTableViews() {
        self.deepDivesTableview.delegate = self
        self.deepDivesTableview.dataSource = self
        self.deepDivesTableview.estimatedRowHeight = 300
        self.deepDivesTableview.rowHeight = UITableView.automaticDimension
    }
    
    // MARK: - API calls
    func getEducationalData() {
        
        self.view.activityStartAnimating()
        let url = ApiUrl.GET_EDUCATIONAL_DATA_URL
        let namespace = SPManager.sharedInstance.getCurrentUserNamespace()
        let data:Dictionary<String,AnyObject> = [:]
        let params:Dictionary<String,AnyObject> = ["namespace" : namespace as AnyObject, "data" : data as AnyObject]
        print(data)
        APIManager.sharedInstance.postRequest(serviceName: url, sendData: params, success: { (response) in
            print(response)
            
            if APIManager.sharedInstance.isSuccess {
                let res = response["message"] as AnyObject
                DispatchQueue.main.async {
                    
                    self.fetchMetaData(res: res as! [[String : AnyObject]], table_view: self.deepDivesTableview)
                }
                
            }else{
                if response["message"] != nil {
                    self.showAlert(response["message"] as! String)
                } else {
                    self.showAlert(ConstantStrings.NO_RESULT_FOUND_ALERT_TEXT)
                }
            }
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
        }, failure: { (data) in
            print(data)
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
            if APIManager.sharedInstance.error400 {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
            }
            if(APIManager.sharedInstance.error401)
            {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
            }else{
                
            }
        })
        
    }
    
    // MARK: - fetchData
    func fetchMetaData(res : [[String : AnyObject]], table_view : UITableView) {
        ArrayLists.sharedInstance.EducationalDataList.removeAll()
        
        DispatchQueue.main.async {
            
            ArrayLists.sharedInstance.EducationalDataList = self.parseJsonList(data: res)
        }
        
        DispatchQueue.main.async {
            table_view.reloadData()
        }
    }
    
    func parseJsonList(data : [[String : AnyObject]]) -> [LibraryDataModel] {
        
        var list : [LibraryDataModel] = [LibraryDataModel]()
        var i = 1
        for jsonData in data {
            let model = LibraryDataModel()
            var mainHeading = jsonData["main_heading"] as! String
            mainHeading = mainHeading.replacingOccurrences(of: "'", with: "''")
            var subHeading = jsonData["subheading"] as! String
            subHeading = subHeading.replacingOccurrences(of: "'", with: "''")
            //parse model scaling json
            let description = jsonData["description"] as! String
            var iconUrl = jsonData["icon_url"] as! String
            iconUrl = iconUrl.replacingOccurrences(of: "'", with: "''")
            let progressValue = jsonData["progress_value"] as! Double
            let dataType = jsonData["type"] as! String
            let timestamp = jsonData["eve_sec"] as! Int
             if jsonData["additional_info"] as? [String : AnyObject] != nil {
                let additionalInfo = jsonData["additional_info"] as! [String : AnyObject]
                //parse additional info
                //            if additionalInfo != nil {
                let infoType = additionalInfo["type"] as! String
                
                model.setInfoType(type: infoType)
                if infoType == "text" {
                    let infoValue = additionalInfo["value"] as! [[String : AnyObject]]
                    model.additionalInfoList = infoValue
                } else {
                    let infoValue = additionalInfo["value"] as! String
                    model.setInfoValue(value: infoValue)
                }
            } else {
                model.setInfoType(type: "none")
            }
            model.setMainHeadingText(text: mainHeading)
            model.setSubHeadingText(text: subHeading)
            model.setDescriptionText(text: description)
            model.setIconImage(text: iconUrl)
            model.setProgressValue(text: progressValue)
            model.setDataType(text: dataType)
            list.append(model)
            let query = "INSERT INTO education (heading,subheading,image) VALUES ('\(mainHeading)','\(subHeading)','\(iconUrl)')"
            let db = Databasehandler()

            let result = db.executeQuery(query: query)
    
            for jsonData in model.additionalInfoList {
                let model = LibraryDataModel()
                
                var infoHeading = jsonData["info_heading"] as! String
                infoHeading =  infoHeading.replacingOccurrences(of: "'", with: "''")
                var infoSubHeading = jsonData["info_sub_heading"] as! String
                infoSubHeading = infoSubHeading.replacingOccurrences(of: "'", with: "''")
                var infoDescription = jsonData["info_description"] as! String
                infoDescription = infoDescription.replacingOccurrences(of: "'", with: "''")
                var infoDisclaimerText = jsonData["info_disclaimer_text"] as! String
                infoDisclaimerText = infoDisclaimerText.replacingOccurrences(of: "'", with: "''")
                var infoDisclaimerLink = jsonData["info_disclaimer_link"] as! String
                infoDisclaimerLink = infoDisclaimerLink.replacingOccurrences(of: "'", with: "''")
                var infoDisclaimerLinkType = jsonData["info_disclaimer_link_type"] as! String
                infoDisclaimerLinkType = infoDisclaimerLinkType.replacingOccurrences(of: "'", with: "''")
                var infoImage = jsonData["info_description_image"] as! String
                infoImage = infoImage.replacingOccurrences(of: "'", with: "''")
                model.setInfoHeading(text: infoHeading)
                model.setInfoSubHeading(text: infoSubHeading)
                model.setInfoDescription(text: infoDescription)
                model.setInfoDisclaimerText(text: infoDisclaimerText)
                model.setInfoDisclaimerLink(text: infoDisclaimerLink)
                model.setInfoDisclaimerLinkType(text: infoDisclaimerLinkType)
                model.setInfoDisclaimerImage(text: infoImage)
                let q = "INSERT INTO educationDetail (eid,infoHeading,infoSubHeading,infoDescription,infoDisclaimerImage,infoDisclaimerText) VALUES ('\(i)','\(infoHeading)','\(infoSubHeading)','\(infoDescription)','\(infoImage)','\(infoDisclaimerText)')"
                let res = db.executeQuery(query: q)
                print(res)
            }
            
            i += 1
        }
        let db = Databasehandler()

        self.data = db.readEducation()
        DispatchQueue.main.async {
            self.deepDivesTableview.reloadData()
        }
        return list
        
    }
    //     MARK: - Button Acction
    @IBAction func myProfile_pressed(_ sender: Any) {
        if SPManager.sharedInstance.saveUserLoginStatus() {

        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "myProfile_view") as! myProfile_view
        
        navigationController?.pushViewController(vc, animated: true)
        }else{
            let toast = getLocalizedString(key: "Join now for a custom profile", comment: "")
            self.view.makeToast(toast)

        }
    }
    @IBAction func Beginner_btn(_ sender: Any) {
        userlevel = "Beginner"
        Level_VIew.isHidden = true
        UserDefaults.standard.set(userlevel, forKey: "userLevel")
        self.sortdatacell1()
        
        setAnalyticsAction(ScreenName: "EducationalScreenVc", method: "Beginner")
        
    }
    @IBAction func intermediate(_ sender: Any) {
        userlevel = "Intermediate"
        Level_VIew.isHidden = true
        UserDefaults.standard.set(userlevel, forKey: "userLevel")
        self.sortdatacell1()
        
        setAnalyticsAction(ScreenName: "EducationalScreenVc", method: "Intermediate")
    }
    @IBAction func Expert_btn(_ sender: Any) {
        
        userlevel = "Expert"
        Level_VIew.isHidden = true
        UserDefaults.standard.set(userlevel, forKey: "userLevel")
        setAnalyticsAction(ScreenName: "EducationalScreenVc", method: "Expert")
        self.sortdatacell1()
    }
    @IBAction func takeQuiz_btn(_ sender: Any) {
        //userlevel = "Intermediate"
        //quizuserlevel = "qz"
        //Level_VIew.isHidden = true
        //UserDefaults.standard.set(userlevel, forKey: "userLevel")
        //UserDefaults.standard.set(quizuserlevel, forKey: "qz")
        //self.sortdatacell1()
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "QuizExpertiselevel_VC") as! QuizExpertiselevel_VC
        navigationController?.pushViewController(vc, animated: true)
        
        setAnalyticsAction(ScreenName: "EducationalScreenVc", method: "Quiz")
        
    }
    
    @IBAction func newsfeedBtnPressed(_ sender: UIButton){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "NotificationsVC") as! NotificationsVC
        self.navigationController?.pushViewController(vc, animated: true)
        setAnalyticsAction(ScreenName: "EducationalScreenVc", method: "NewsFeed")
    }
    
   
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    
}
extension Array where Element: Equatable {
    
    // Remove first collection element that is equal to the given `object`:
    mutating func remove(object: Element) {
        guard let index = firstIndex(of: object) else {return}
        remove(at: index)
    }
    
}

// MARK: - Ext. TableView Delegates
extension EducationalSectionVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if userlevel != "takequiz"{
            
            if indexPath.row == tempdata.count+1{
                let deepDiveCell =  self.deepDivesTableview.dequeueReusableCell(withIdentifier: "DeepDivesCell3") as! DeepDivesCell3
                 var title = getLocalizedString(key: "You may also learn", comment: "")
                deepDiveCell.heading_lbl.text = title
                print(title)
             //   "You may also learn"
                //"You may also learn"
                return deepDiveCell
                
            }}
        if indexPath.row == 0{
            let deepDiveCell =  self.deepDivesTableview.dequeueReusableCell(withIdentifier: "DeepDivesCell3") as! DeepDivesCell3
             var titleheading = getLocalizedString(key: "Lesson plans selected for you:", comment: "")
            deepDiveCell.heading_lbl.text = titleheading
            return deepDiveCell
        }
        
        let deepDiveCell =  self.deepDivesTableview.dequeueReusableCell(withIdentifier: "DeepDivesCell") as! DeepDivesCell
        var index = indexPath.row-1
        if userlevel != "takequiz"{
            
            if indexPath.row < tempdata.count+1{
                index = indexPath.row-1
            }else{
                index = indexPath.row-2
            }
        }
        let item = data[index]
        //        showHideCellAccessory(infoType: item.getInfoType(), cell: deepDiveCell)
        deepDiveCell.heading_lbl.text = item.mainHeadingText
        
        if item.id == 1 {
            deepDiveCell.Image_education_img.image =  UIImage(named: "bonds")
            deepDiveCell.background_view.backgroundColor = #colorLiteral(red: 0.5137254902, green: 0.5568627451, blue: 0.7764705882, alpha: 1)
        }else if item.id == 2 {
            deepDiveCell.Image_education_img.image =  UIImage(named: "img_retirement_planning_choose_topic")
            deepDiveCell.background_view.backgroundColor = #colorLiteral(red: 0.8196078431, green: 0.9098039216, blue: 0.9607843137, alpha: 1)
            
        }else if item.id == 3 {
            
            deepDiveCell.Image_education_img.image =  UIImage(named: "img_annuities_choose_topic")
            deepDiveCell.background_view.backgroundColor = #colorLiteral(red: 0, green: 0.631372549, blue: 0.6078431373, alpha: 1)
        }else if item.id == 4 {
            deepDiveCell.Image_education_img.image =  UIImage(named: "img_lesson_d")
            deepDiveCell.background_view.backgroundColor = #colorLiteral(red: 0.6470588235, green: 0.8392156863, blue: 0.6549019608, alpha: 1)
            
        }else if item.id == 5 {
            deepDiveCell.Image_education_img.image =  UIImage(named: "img_lesson_e")
            deepDiveCell.background_view.backgroundColor = #colorLiteral(red: 0.937254902, green: 0.9725490196, blue: 0.9607843137, alpha: 1)
            
        }else if item.id == 6 {
            deepDiveCell.Image_education_img.image =  UIImage(named: "img_lesson_f")
            deepDiveCell.background_view.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            
            
        }else if item.id == 7 {
            deepDiveCell.Image_education_img.image =  UIImage(named: "bigginer_2")
            deepDiveCell.background_view.backgroundColor = #colorLiteral(red: 0.5843137503, green: 0.8235294223, blue: 0.4196078479, alpha: 1)
            
            
        }
        else if item.id == 8 {
            deepDiveCell.Image_education_img.image =  UIImage(named: "inter_4")
            deepDiveCell.background_view.backgroundColor = #colorLiteral(red: 0.9568627477, green: 0.6588235497, blue: 0.5450980663, alpha: 1)
            
            
        }
        else if item.id == 9 {
            deepDiveCell.Image_education_img.image =  UIImage(named: "inter_5")
            deepDiveCell.background_view.backgroundColor = #colorLiteral(red: 0.4745098054, green: 0.8392156959, blue: 0.9764705896, alpha: 1)
            
            
        }
        else if item.id == 10 {
            deepDiveCell.Image_education_img.image =  UIImage(named: "skilled_3")
            deepDiveCell.background_view.backgroundColor = #colorLiteral(red: 0.9098039269, green: 0.4784313738, blue: 0.6431372762, alpha: 1)
        }
        else if item.id == 11 {
            deepDiveCell.Image_education_img.image =  UIImage(named: "sec_edu_image")
            deepDiveCell.background_view.backgroundColor = #colorLiteral(red: 0.003921568627, green: 0.8745098039, blue: 0.7490196078, alpha: 1)
        }
                
        print(item.mainHeadingText)
        deepDiveCell.Image_education_img.layer.cornerRadius = 10
        deepDiveCell.Image_education_img.clipsToBounds = true
        deepDiveCell.award_education_img.isHidden = true
       // deepDiveCell.Image_education_img.image =  UIImage(named: array1[index])
        //deepDiveCell.background_view.backgroundColor = arrColor[index]
        let db = Databasehandler()

        let total = Float(db.readEducationDETAIL(id: item.id).count)
        let done =  Float(db.getQuestion(user: user, mainQ: item.id).count)
        let percent = (done*100)/total
        print(total)
        totalprenstage = Int(percent)
        if percent == 100{
            award = count
        }
        
        if percent > 0{
            deepDiveCell.award_education_img.isHidden = false
            deepDiveCell.award_education_img.contentMode = .scaleAspectFit
            // MARK: - Adjust animation speed
            deepDiveCell.award_education_img.animationSpeed = 0.5
            deepDiveCell.award_education_img.play()
            deepDiveCell.percentage.text = "\(Int(percent))% " + getLocalizedString(key: "completed_", comment: "")
        }else{
            deepDiveCell.percentage.text = ""
            
        }
        return deepDiveCell
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if userlevel == "takequiz"{
            return data.count + 1
        }
        return (data.count + 2)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let isClickableCell = tableView.cellForRow(at: indexPath)?.isKind(of: DeepDivesCell3.self) ?? false
        
        if isClickableCell {
            print("Not clickable data")
            return
        }
        print("selected index == \(indexPath.row)")
        var index = indexPath.row-1
        if userlevel != "takequiz"{
            
            if indexPath.row < tempdata.count+1{
                index = indexPath.row-1
            }else{
                index = indexPath.row-2
            }
        }
        let item = data[index]
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "EduSectionDetailsVC") as! EduSectionDetailsVC
        vc.additionalInfo = item.id
        vc.titleString = item.mainHeadingText
        navigationController?.pushViewController(vc, animated: true)
    }
    //Mark :- check to show/hide cell accessory
    func showHideCellAccessory(infoType : String, cell : UITableViewCell) {
        if infoType != "none" {
            
            cell.accessoryType = .disclosureIndicator
        } else {
            cell.accessoryType = .none
        }
    }
}







