//
//  MyViewController.swift
//  Fintech
//
//  Created by broadstone on 12/08/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import UIKit
import PassKit
import Stripe
//import Alamofire


 

class MyViewController: PKAddPaymentPassViewController, STPIssuingCardEphemeralKeyProvider {
    func createIssuingCardKey(withAPIVersion apiVersion: String, completion: @escaping STPJSONResponseCompletionBlock) {
        
    }
    
   var pushProvisioningContext: STPPushProvisioningContext? = nil
    override func viewDidLoad() {

        super.viewDidLoad()

        // Do any additional setup after loading the view.

    }

    func beginPushProvisioning() {

        let config = STPPushProvisioningContext.requestConfiguration(

          withName: "Jenny Rosen", // the cardholder's name

          description: "RocketRides Card", // optional; a description of your card

          last4: "4242", // optional; the last 4 digits of the card

          brand: .visa // optional; the brand of the card

        )

        let controller = PKAddPaymentPassViewController(requestConfiguration: config, delegate: self)
        self.present(controller!, animated: true, completion: nil)

      }

//    func createIssuingCardKey(withAPIVersion apiVersion: String, completion: @escaping STPJSONResponseCompletionBlock) {
//
//
//
//        let innerParams : [String : Any] = ["parsl" : "016DDE"]
//
//          let params : [String : Any] = ["data" : innerParams]
//
//
//
//
//
//            AF.request("https://parsl.io/get/ephemeral/key",
//
//              method: .post,
//
//              parameters: params)
//
//            .responseJSON { response in
//
//                switch response.result {
//
//                case .success:
//
//              if let data = response.data {
//
//                  do {
//
//                    let obj = try JSONSerialization.jsonObject(with: data, options: []) as! [AnyHashable: Any]
//
//        completion(obj, nil)
//
//                    let decodedData = try JSONDecoder().decode(DataExtractor.self, from: data)
//
//               let id = decodedData.data.id as! [AnyHashable: Any]
//
//                    completion(id, nil)
//
//
//
//                  } catch {
//
//                    completion(nil, error)
//
//            }
//
//
//
//                }case .failure(let error):
//
//                   completion(nil, error)
//
//        }
//
//
//
//      }
//
//    }

}
extension MyViewController: PKAddPaymentPassViewControllerDelegate {

  func addPaymentPassViewController(_ controller: PKAddPaymentPassViewController, generateRequestWithCertificateChain certificates: [Data], nonce: Data, nonceSignature: Data, completionHandler handler: @escaping (PKAddPaymentPassRequest) -> Void) {

    self.pushProvisioningContext = STPPushProvisioningContext(keyProvider: self)

    // STPPushProvisioningContext implements this delegate method for you, by retrieving encrypted card details from the Stripe API.

    self.pushProvisioningContext?.addPaymentPassViewController(controller, generateRequestWithCertificateChain: certificates, nonce: nonce, nonceSignature: nonceSignature, completionHandler: handler);

  }
  func addPaymentPassViewController(_ controller: PKAddPaymentPassViewController, didFinishAdding pass: PKPaymentPass?, error: Error?) {

    // Depending on if `error` is present, show a success or failure screen.

    self.dismiss(animated: true, completion: nil)

  }

}
