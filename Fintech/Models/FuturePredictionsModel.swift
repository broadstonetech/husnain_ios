//
//  FuturePredictionsModel.swift
//  Fintech
//
//  Created by MacBook-Mubashar on 23/01/2020.
//  Copyright © 2020 Broadstone Technologies. All rights reserved.
//

import Foundation


class FuturePredictionsModel: NSObject {
    
    fileprivate var plan : String
    fileprivate var confidenceLevel : Int
    fileprivate var profit : Int
    var planDetails : [[String : AnyObject]] = [[String : AnyObject]]()
    
    
    override init() {
        
        self.plan = ""
        self.confidenceLevel = -1
        self.profit = -1
        
    }
    
    func setProfit(_ profit: Int) {
        self.profit = profit
    }
    func getProfit()->Int {
        return self.profit
    }
    func setConfidenceLevel(_ confidenceLevel: Int) {
        self.confidenceLevel = confidenceLevel
    }
    func getConfidenceLevel()->Int {
        return self.confidenceLevel
    }
    func setPlan(_ plan: String){
        self.plan = plan
    }
    
    func getPlan() ->String {
        return self.plan
    }
}
