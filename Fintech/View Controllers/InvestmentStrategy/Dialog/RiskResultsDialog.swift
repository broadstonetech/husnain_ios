//
//  RiskResultsDialog.swift
//  Fintech
//
//  Created by broadstone on 08/04/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import UIKit
import TagListView
class RiskResultsDialog: UIView {

    @IBOutlet weak var parentView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var tagListView: TagListView!
    @IBOutlet weak var closeBtn: UIImageView!
    @IBOutlet weak var changeRiskHeadingLbl: UILabel!
    @IBOutlet weak var riskSliderMinLbl: UILabel!
    @IBOutlet weak var riskSliderMaxLbl: UILabel!
    @IBOutlet weak var riskSlider: UISlider!
    @IBOutlet weak var riskSliderResultLbl: UILabel!
    @IBOutlet weak var recalculateRiskBtn: UIButton!
    
    var updatedRiskScore: Int = 0
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    private func commonInit(){
        Bundle.main.loadNibNamed("RiskResultsDialog", owner: self, options: nil)
        addSubview(parentView)
        parentView.frame = self.bounds
        parentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        //setInitialViewData()
        
        riskSliderResultLbl.text = getLocalizedString(key: "risk_neutral")
        recalculateRiskBtn.setTitle(getLocalizedString(key: "recalculate_score"), for: .normal)
        
        initTagListView()
    }
    
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }

    
    func setInitialViewData(riskScore: Float){
        riskSlider.minimumValue = 1
        riskSlider.maximumValue = 47
        riskSlider.value = riskScore
        riskSlider.minimumTrackTintColor = .clear
        riskSlider.maximumTrackTintColor = .clear
        
        if riskSlider.value < 23 {
            riskSlider.thumbTintColor = .systemGreen
            riskSliderResultLbl.text = getLocalizedString(key: "risk_averse")
        } else if riskSlider.value >= 23 && riskSlider.value < 33 {
            riskSlider.thumbTintColor = .systemYellow
            riskSliderResultLbl.text = getLocalizedString(key: "risk_neutral")
        } else {
            riskSlider.thumbTintColor = .systemRed
            riskSliderResultLbl.text = getLocalizedString(key: "risk_loving")
        }
        
        tagListView.removeAllTags()
        tagListView.addTag(getLocalizedString(key: "risk_score_tag") + "\(Int(riskScore)) " + getLocalizedString(key: "out_of") + " 47")
        
        riskSlider.addTarget(self, action: #selector(sliderValueDidChange(sender:)),for: .valueChanged)
    }
    

    @objc func sliderValueDidChange(sender: UISlider) {
            if sender.value < 23 {
                riskSlider.thumbTintColor = .systemGreen
                riskSliderResultLbl.text = getLocalizedString(key: "risk_averse")
            } else if sender.value >= 23 && sender.value < 33 {
                riskSlider.thumbTintColor = .systemYellow
                riskSliderResultLbl.text = getLocalizedString(key: "risk_neutral")
            } else {
                riskSlider.thumbTintColor = .systemRed
                riskSliderResultLbl.text = getLocalizedString(key: "risk_loving")
            }
        self.updatedRiskScore = Int(sender.value)
        tagListView.removeAllTags()
        tagListView.addTag(getLocalizedString(key: "risk_score_tag") + "\(updatedRiskScore) " + getLocalizedString(key: "out_of") + " 47")
        }
    
    private func initTagListView(){
        tagListView.textFont = UIFont.systemFont(ofSize: 15)
        tagListView.tagBackgroundColor = .systemBlue
        tagListView.textColor = .white
        
        
    }
}
