//
//  PortfolioListVC.swift
//  Fintech
//
//  Created by broadstone on 09/08/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import UIKit

class PortfolioListVC: PortfolioParent, PortfolioListApiDalegate {
    var portfolioListData = [PortfolioListData]()
    var useCaseType: UseCaseType!
    
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var swipeLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var isNavigatingBack = false

    override func viewDidLoad() {
        super.viewDidLoad()

        self.portfolioListApiDalegate = self
        
        tableView.delegate = self
        tableView.dataSource = self
        
        createPlusButton(action: #selector(newPortfolioBtnPressed(_:)))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        setNavBarTitile(title: "Back", topItem: getLocalizedString(key: "_portfolios"))
        
        if isNavigatingBack {
            getPortfolioList(useCaseType: useCaseType, isNavigatingBack: true)
        }
        isNavigatingBack = true
    }
    
    @objc func newPortfolioBtnPressed(_ sender: UIButton) {
        if useCaseType == UseCaseType.UseCase1 {
            let vc = ExternalPortfolioVC()
            self.navigationController?.pushViewController(vc, animated: true)
        } else if useCaseType == UseCaseType.UseCase2 {
            pushAddSymbolVC()
        }else {
            pushPortfolioTemplateVC()
        }
        
        
    }
    
    func portfolioListApiCalled(model: [PortfolioListData]) {
        self.portfolioListData = model
        tableView.reloadData()
    }

}
extension PortfolioListVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return portfolioListData.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PortfolioListCell") as! PortfolioListCell
        cell.nameLabel.text = portfolioListData[indexPath.section].portfolioName
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 0
        }
        return 20
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 20))
        headerView.backgroundColor = UIColor.clear
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let delete = UIContextualAction(style: .destructive, title:  "Delete", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            self.deletePortfolio(rowIndex: indexPath.section)
            success(true)
        })

        
        let update = UIContextualAction(style: .normal, title:  getLocalizedString(key: "_update"), handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            self.updatePortfolio(rowIndex: indexPath.section)
            success(true)
        })
        update.title = getLocalizedString(key: "_update")
        delete.title = getLocalizedString(key: "_delete")
        update.backgroundColor = #colorLiteral(red: 1, green: 0.8, blue: 0, alpha: 1)
        delete.backgroundColor = #colorLiteral(red: 1, green: 0.231372549, blue: 0.1882352941, alpha: 1)
        
        if #available(iOS 13.0, *) {
            delete.image = UIImage(systemName: "xmark.bin")
            update.image = UIImage(systemName: "pencil")
        } else {
            // Fallback on earlier versions
        }

        return UISwipeActionsConfiguration(actions: [update, delete])
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        generatePortfolioAPI(url: ApiUrl.GET_PORTFOLIO_DETAILS, params: getPortfolioDetailsParams(index: indexPath.section), useCaseType: useCaseType, for: false)
    }
    
    private func getPortfolioDetailsParams(index: Int) -> [String: AnyObject] {
        let data = ["portfolio_id": portfolioListData[index].portfolioId] as [String: AnyObject]
        
        return ["namespace": SPManager.sharedInstance.getCurrentUserNamespace(),
                "data": data] as [String: AnyObject]
    }
    
}
//MARK:- Delete Portfolio
private extension PortfolioListVC {
    
    func deletePortfolio(rowIndex: Int) {
        let alert = UIAlertController(title: getLocalizedString(key: "_delete"), message: getLocalizedString(key: "delete_ur_portfolio"), preferredStyle: .alert)
        let ok = UIAlertAction(title: getLocalizedString(key: "yes_"), style: .destructive, handler: { (action) in
            self.dismiss(animated: true, completion: nil)
            self.deletePortfolioAPI(rowIndex: rowIndex)
        })
        
        let cancel = UIAlertAction(title: getLocalizedString(key: "no_"), style: .cancel, handler: { (action) in
            self.dismiss(animated: true, completion: nil)
        })
        
        alert.addAction(ok)
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
    }
    
    func deletePortfolioAPI(rowIndex: Int){
        self.view.activityStartAnimating()
        let url = ApiUrl.DELETE_PORTFOLIO
        let params = getPortfolioDetailsParams(index: rowIndex)
        print(params)
        
        APIManager.sharedInstance.postRequest(serviceName: url, sendData: params, success: { (response) in
            //print(response)
            
            if APIManager.sharedInstance.isSuccess {
                DispatchQueue.main.async { [self] in
                    portfolioListData.remove(at: rowIndex)
                    tableView.reloadData()
                }
            }else{
                
                DispatchQueue.main.async { [self] in
                    if response["message"] != nil {
                        self.showAlert(response["message"] as! String)
                    } else {
                        self.showAlert(ConstantStrings.NO_RESULT_FOUND_ALERT_TEXT)
                    }
                }
            }
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
        }, failure: { (data) in
            print(data)
            DispatchQueue.main.async { [self] in
                view.activityStopAnimating()
            }
            if APIManager.sharedInstance.error400 {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
            }
            if(APIManager.sharedInstance.error401)
            {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
            }else{
                self.showAlert(ConstantStrings.Error_msg_generic)
            }
        })
    }
}
//MARK:- Update Portfolio
private extension PortfolioListVC {
    
    func updatePortfolio(rowIndex: Int) {
        let alert = UIAlertController(title: getLocalizedString(key: "_update"), message: getLocalizedString(key: "update_ur_portfolio"), preferredStyle: .actionSheet)
        let name = UIAlertAction(title: getLocalizedString(key: "edit_portfolio_name"), style: .default, handler: { (action) in
            self.dismiss(animated: true, completion: nil)
            self.showUpdateNameDialog(rowIndex: rowIndex)
        })
        
        let asset = UIAlertAction(title: getLocalizedString(key: "update_assets"), style: .default, handler: { (action) in
            self.dismiss(animated: true, completion: nil)
            self.generatePortfolioAPI(url: ApiUrl.GET_PORTFOLIO_DETAILS, params: self.getPortfolioDetailsParams(index: rowIndex), useCaseType: self.useCaseType, for: true)
        })
        
        let cancel = UIAlertAction(title: getLocalizedString(key: "cancel_btn"), style: .cancel, handler: { (action) in
            self.dismiss(animated: true, completion: nil)
        })
        
        alert.addAction(name)
        alert.addAction(asset)
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func showUpdateNameDialog(rowIndex: Int) {
        let alert = UIAlertController(title: getLocalizedString(key: "portfolio_name"), message: getLocalizedString(key: "edit_portfolio_name"), preferredStyle: .alert)

        //2. Add the text field. You can configure it however you need.
        alert.addTextField { [self] (textField) in
            textField.text = portfolioListData[rowIndex].portfolioName
            textField.placeholder = getLocalizedString(key: "enter_portfolio_name")
        }

        // 3. Grab the value from the text field, and print it when the user clicks OK.
        alert.addAction(UIAlertAction(title: getLocalizedString(key: "ok"), style: .default, handler: { [weak alert] (_) in
            let textField = alert?.textFields![0] // Force unwrapping because we know it exists.
            let text = textField?.text ?? ""
            
            if !text.isEmpty {
                self.dismiss(animated: true, completion: nil)
                self.callUpdateNameDialogAPI(rowIndex: rowIndex, updatedName: text)
            }
        }))
        
        alert.addAction(UIAlertAction(title: getLocalizedString(key: "cancel_btn"), style: .cancel, handler: { (action) in
            self.dismiss(animated: true, completion: nil)
        }))

        // 4. Present the alert.
        self.present(alert, animated: true, completion: nil)
    }
    
    func callUpdateNameDialogAPI(rowIndex: Int, updatedName: String){
        self.view.activityStartAnimating()
        let url = ApiUrl.UPDATE_PORTFOLIO_NAME
        let data = ["portfolio_id": portfolioListData[rowIndex].portfolioId,
                    "portfolio_name": updatedName] as [String: AnyObject]
        
        let params = ["namespace": SPManager.sharedInstance.getCurrentUserNamespace(),
                "data": data] as [String: AnyObject]
        
        print(params)
        
        APIManager.sharedInstance.postRequest(serviceName: url, sendData: params, success: { (response) in
            //print(response)
            
            if APIManager.sharedInstance.isSuccess {
                DispatchQueue.main.async { [self] in
                    self.portfolioListData[rowIndex] = PortfolioListData.init(portfolioId: self.portfolioListData[rowIndex].portfolioId, portfolioName: updatedName)
                    tableView.reloadData()
                }
            }else{
                
                DispatchQueue.main.async { [self] in
                    if response["message"] != nil {
                        self.showAlert(response["message"] as! String)
                    } else {
                        self.showAlert(ConstantStrings.NO_RESULT_FOUND_ALERT_TEXT)
                    }
                }
            }
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
        }, failure: { (data) in
            print(data)
            DispatchQueue.main.async { [self] in
                view.activityStopAnimating()
            }
            if APIManager.sharedInstance.error400 {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
            }
            if(APIManager.sharedInstance.error401)
            {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
            }else{
                self.showAlert(ConstantStrings.Error_msg_generic)
            }
        })
    }
}
