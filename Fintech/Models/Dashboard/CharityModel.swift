//
//  charityModel.swift
//  vesgoAPI
//
//  Created by Billal on 28/01/2021.
//

import Foundation

struct charityResponse : Decodable {
    var success : Int
    var data : charityData
}
struct charityData : Decodable {
    var timestamp : Int
    var has_donated : Bool
    var donated_amount : Int
    var planned_amount : Int
}
