//
//  PreMadePortfolioVC + TableView.swift
//  Fintech
//
//  Created by broadstone on 04/03/2022.
//  Copyright © 2022 Broadstone Technologies. All rights reserved.
//

import Foundation
import UIKit

extension PreMadePortfolioListVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if portfoliosList == nil {
            return 0
        }
        
        return portfoliosList.data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PreMadePortfolioCell") as! PreMadePortfolioCell
        cell.portfolioLabel.text = portfoliosList.data[indexPath.row].portfolioName
        let impactText = getLocalizedString(key: "portfolio_impact") + ": " + FintechUtils.sharedInstance.getImpactForPortfolio(portfoliosList.data[indexPath.row].overAllZScore)
        cell.impactLabel.text = impactText
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        getPortfolioDetail(portfolioId: portfoliosList.data[indexPath.row].portfolioId)
    }
}
