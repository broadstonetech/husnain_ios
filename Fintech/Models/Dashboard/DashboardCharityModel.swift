// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let dashboardCharityModel = try? newJSONDecoder().decode(DashboardCharityModel.self, from: jsonData)

import Foundation

// MARK: - DashboardCharityModel
struct DashboardCharityModel: Codable {
    let success: Bool
    let message: String
    let data: DashboardCharityDataClass
}

// MARK: - DataClass
struct DashboardCharityDataClass: Codable {
    let timestamp: Int64
    let hasDonated: Bool
    let donatedAmount, plannedAmount: Double

    enum CodingKeys: String, CodingKey {
        case timestamp
        case hasDonated = "has_donated"
        case donatedAmount = "donated_amount"
        case plannedAmount = "planned_amount"
    }
}
