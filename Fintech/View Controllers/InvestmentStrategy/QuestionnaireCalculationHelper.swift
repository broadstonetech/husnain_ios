//
//  QuestionnaireCalculationHelper.swift
//  Fintech
//
//  Created by broadstone on 01/06/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import Foundation


enum ValuesTemplate {
    case Catholic
    case Environment
    case Islamic
    case Methodist
    case Peace
    case Vegan
    case Social
    case Governance
    case Manual
    case NOT_SET
}

class QuestionnaireCalculationHelper: NSObject {
    static let sharedInstance = QuestionnaireCalculationHelper()
    
    func getInvestmentValue(userAnswer: [String]) -> Int {
        if userAnswer[0] == "a" {
            return 1
        }else {
            return 0
        }
    }
    
    func getEsgPercentile(userAnswer: [String]) -> Int {
        if userAnswer[0] == "a" {
            return 75
        }else if userAnswer[0] == "b" {
            return 60
        }else if userAnswer[0] == "c" {
            return 50
        }else {
            return 0
        }
    }
    
    func getEnvPercentile(positiveValues: [String]) -> Int {
        if positiveValues.contains(getLocalizedString(key: "_vegan")) {
            return 50
        }else {
            return 0
        }
    }
    
    func getSocialPercentile(positiveValues: [String]) -> Int {
        if positiveValues.contains(getLocalizedString(key: "_vegan")) {
            return 50
        }else {
            return 0
        }
    }
    
    func getGovernancePercentile(valuesModel: [Questionnaire]) -> Int {
        let qv7Answer = FintechUtils.sharedInstance.getInvestmentStrategySelectedAnswerIDs(selectedAnswers: valuesModel[6].selectedAnswersIds)[0]
        if qv7Answer == "a" {
            return 50
        }
        
        let postiveValues = valuesModel[1].selectedAnswers
        
        if postiveValues.contains(getLocalizedString(key: "24b")) || postiveValues.contains(getLocalizedString(key: "24c")) || postiveValues.contains(getLocalizedString(key: "24d")){
            return 50
        }else {
            return 0
        }
    }
    
    func getMinorityOwnedPercentile(userAnswer: [String]) -> Int {
        if userAnswer[0] == "a" {
            return 25
        }else {
            return 0
        }
    }
    
    func getInterestBearingValue(negativeValues: [String]) -> Int {
        if negativeValues.contains(getLocalizedString(key: "23i")) {
            return 1
        }else {
            return 0
        }
    }
    
    func getCatholicValue(negativeValues: [String]) -> Int {
        if negativeValues.contains(getLocalizedString(key: "23a")) || negativeValues.contains(getLocalizedString(key: "_contraceptives")) {
            return 1
        }else {
            return 0
        }
    }
    
    func getResultantNegativeValues (valuesModel: [Questionnaire]) -> [String] {
        var resultantArray = valuesModel[2].selectedAnswers
        
        if resultantArray.contains(getLocalizedString(key: "_firarms")) {
            resultantArray.append(getLocalizedString(key: "Firearms"))
            resultantArray.append("Military contract")
            resultantArray.append("Controversial weapons")
        }
        
        let qv5Answer = FintechUtils.sharedInstance.getInvestmentStrategySelectedAnswerIDs(selectedAnswers: valuesModel[4].selectedAnswersIds)[0]
        
        if qv5Answer == "a" || qv5Answer == "b" || qv5Answer == "c" {
            if !resultantArray.contains(getLocalizedString(key: "_coal")) {
                resultantArray.append("Coal")
            }
            if !resultantArray.contains(getLocalizedString(key: "palm_oil")) {
                resultantArray.append("Palm oil")
            }
        }
        
        let qv6Answer = FintechUtils.sharedInstance.getInvestmentStrategySelectedAnswerIDs(selectedAnswers: valuesModel[5].selectedAnswersIds)[0]
        
        
        if qv6Answer == "a" || qv6Answer == "b" || qv6Answer == "c" || qv6Answer == "d" {
            if !resultantArray.contains("Firearms") {
                resultantArray.append("Coal")
            }
            if !resultantArray.contains("Firearms") {
                resultantArray.append("Military contract")
            }
            if !resultantArray.contains("Controversial weapons") {
                resultantArray.append("Controversial weapons")
            }
        }
        
        return resultantArray
    }
    
    //Mark:- Risk score calculation
    struct RiskScore {
        var questionId: Int!
        var scoreValues: [Int]!
    }
    
    func getRiskScores() -> [RiskScore] {
        let scoreArray:[RiskScore] = [
            RiskScore(questionId: 0, scoreValues: [4,3,2,1]),
            RiskScore(questionId: 1, scoreValues: [1,2,3,4]),
            RiskScore(questionId: 2, scoreValues: [1,2,3,4]),
            RiskScore(questionId: 3, scoreValues: [1,2,3]),
            RiskScore(questionId: 4, scoreValues: [1,2,3]),
            RiskScore(questionId: 5, scoreValues: [1,2,3,4]),
            RiskScore(questionId: 6, scoreValues: [1,2,3,4]),
            RiskScore(questionId: 7, scoreValues: [1,2,3,4]),
            RiskScore(questionId: 8, scoreValues: [1,3]),
            RiskScore(questionId: 9, scoreValues: [1,3]),
            RiskScore(questionId: 10, scoreValues: [1,2,3,4]),
            RiskScore(questionId: 11, scoreValues: [1,2,3]),
            RiskScore(questionId: 12, scoreValues: [1,2,3,4])
        ]
        
        return scoreArray
    }
    
    func calculateUserRawRiskScore(riskQuestionnaireData: [Questionnaire]) -> Int {
        var totalScore = 0
        
        for i in 0..<riskQuestionnaireData.count {
            let selectedAnswerId = riskQuestionnaireData[i].selectedAnswersIds[0]
            
            let riskScore = getRiskScores()[i].scoreValues[selectedAnswerId - 1]
            
            totalScore += riskScore
        }
        
        return totalScore
    }
    
    func getUserRiskType(rawScore: Int) -> String {
        if rawScore >= 0 && rawScore < 23 {
            return "conservative"
        }
        else if rawScore >= 23 && rawScore < 33 {
            return "neutral"
        }
        else if rawScore >= 33 && rawScore <= 47 {
            return "aggressive"
        }else {
            return ""
        }
    }
    
    func getUserRiskScaledScore(rawScore: Int) -> Double {
        let score = (Double(rawScore) / 47.0) * 10.0
        let scoreStr = FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: score)
        
        return FintechUtils.sharedInstance.parseDouble(strValue: scoreStr)
        
    }
    
    //Mark:- Values Template Parameters
    func getCatholicTemplateParams(isPortfolioUseCase: Bool = false, risk: Int = 0, portfolioType: String = "") -> [String: AnyObject] {
        if isPortfolioUseCase {
            return [
                    "positive_values": ["Animal rights", "Family values", "Gender equality", "Immigration"] as AnyObject,
                    "negative_values": ["Abortion", "Adult entertainment", "Contraceptives", "Firearms", "Controversial weapons", "Tobacco", "Palm oil"] as AnyObject,
                    "esg_percentile": 60,
                    "env_percentile": 0,
                    "soc_percentile": 0,
                    "gov_percentile": 50,
                    "minorities": 25,
                    "interest_bearing": 0,
                    "template": "catholic",
                    "risk": getRiskDist(risk),
                    "portfolio_type": portfolioType
                    ] as [String: AnyObject]
        }
        return ["investment_values": 1,
                "positive_values": ["Animal rights", "Family values", "Gender equality", "Immigration"]  as AnyObject,
                "negative_values": ["Abortion", "Adult entertainment", "Contraceptives", "Firearms", "Controversial weapons", "Tobacco", "Palm oil"]  as AnyObject,
                "esg_percentile": 60,
                "env_percentile": 0,
                "soc_percentile": 0,
                "gov_percentile": 50,
                "minorities": 25,
                "interest_bearing": 0,
                "template": "catholic"] as [String: AnyObject]
    }
    
    func getEnvTemplateParams(isPortfolioUseCase: Bool = false, risk: Int = 0, portfolioType: String = "") -> [String: AnyObject] {
        if isPortfolioUseCase {
            return [
                    "positive_values": ["Animal rights", "Family values", "Gender equality", "Immigration", "Vegan"]  as AnyObject,
                    "negative_values": ["Coal", "Firearms", "Fur leather", "GMO", "Palm oil", "Pesticides", "Tobacco", "Controversial weapons"] as AnyObject,
                    "esg_percentile": 75,
                    "env_percentile": 75,
                    "soc_percentile": 50,
                    "gov_percentile": 50,
                    "minorities": 25,
                    "template": "environment",
                    "interest_bearing": 0,
                    "risk": getRiskDist(risk),
                    "portfolio_type": portfolioType
                    ] as [String: AnyObject]
        }
        return ["investment_values": 1,
                "positive_values": ["Animal rights", "Family values", "Gender equality", "Immigration", "Vegan"] as AnyObject,
                "negative_values": ["Coal", "Firearms", "Fur leather", "GMO", "Palm oil", "Pesticides", "Tobacco", "Controversial weapons"] as AnyObject,
                "esg_percentile": 75,
                "env_percentile": 75,
                "soc_percentile": 50,
                "gov_percentile": 50,
                "minorities": 25,
                "template": "environment",
                "interest_bearing": 0] as [String: AnyObject]
    }
    
    func getIslamicTemplateParams(isPortfolioUseCase: Bool = false, risk: Int = 0, portfolioType: String = "") -> [String: AnyObject] {
        if isPortfolioUseCase {
            return [
                    "positive_values": ["Animal rights", "Family values", "Gender equality", "Immigration"] as AnyObject,
                    "negative_values": ["Abortion", "Adult entertainment", "Alcohol", "Contraceptives", "Firearms", "Controversial weapons", "Tobacco", "Gambling", "Interest Bearing/Traditional Banks and finance"] as AnyObject,
                    "esg_percentile": 0,
                    "env_percentile": 0,
                    "soc_percentile": 0,
                    "gov_percentile": 50,
                    "minorities": 25,
                    "template": "islamic",
                    "interest_bearing": 1,
                    "risk": getRiskDist(risk),
                    "portfolio_type": portfolioType
                    ] as [String: AnyObject]
        }
        return ["investment_values": 1,
                "positive_values": ["Animal rights", "Family values", "Gender equality", "Immigration"] as AnyObject,
                "negative_values": ["Abortion", "Adult entertainment", "Alcohol", "Contraceptives", "Firearms", "Controversial weapons", "Tobacco", "Gambling", "Interest Bearing/Traditional Banks and finance"] as AnyObject,
                "esg_percentile": 0,
                "env_percentile": 0,
                "soc_percentile": 0,
                "gov_percentile": 50,
                "minorities": 25,
                "template": "islamic",
                "interest_bearing": 1] as [String: AnyObject]
    }
    
    func getMethodistTemplateParams(isPortfolioUseCase: Bool = false, risk: Int = 0, portfolioType: String = "") -> [String: AnyObject] {
        if isPortfolioUseCase {
            return [
                    "positive_values": ["Animal rights", "Family values", "Gender equality", "Immigration"] as AnyObject,
                    "negative_values": ["Abortion", "Adult entertainment", "Contraceptives", "Firearms", "Controversial weapons", "Tobacco"] as AnyObject,
                    "esg_percentile": 0,
                    "env_percentile": 0,
                    "soc_percentile": 0,
                    "gov_percentile": 50,
                    "minorities": 25,
                    "template": "methodist",
                    "interest_bearing": 0,
                    "risk": getRiskDist(risk),
                    "portfolio_type": portfolioType
                    ] as [String: AnyObject]
        }
        return ["investment_values": 1,
                "positive_values": ["Animal rights", "Family values", "Gender equality", "Immigration"] as AnyObject,
                "negative_values": ["Abortion", "Adult entertainment", "Contraceptives", "Firearms", "Controversial weapons", "Tobacco"] as AnyObject,
                "esg_percentile": 0,
                "env_percentile": 0,
                "soc_percentile": 0,
                "gov_percentile": 50,
                "minorities": 25,
                "template": "methodist",
                "interest_bearing": 0] as [String: AnyObject]
    }
    
    func getPeaceTemplateParams(isPortfolioUseCase: Bool = false, risk: Int = 0, portfolioType: String = "") -> [String: AnyObject] {
        if isPortfolioUseCase {
            return [
                    "positive_values": ["Animal rights", "Family values", "Gender equality", "Immigration", "Vegan"] as AnyObject,
                    "negative_values": ["Coal", "Firearms", "Fur leather", "GMO", "Palm oil", "Pesticides", "Tobacco", "Controversial weapons", "Military contract"] as AnyObject,
                    "esg_percentile": 75,
                    "env_percentile": 50,
                    "soc_percentile": 50,
                    "gov_percentile": 50,
                    "minorities": 25,
                    "interest_bearing": 0,
                    "template": "peace",
                    "risk": getRiskDist(risk),
                    "portfolio_type": portfolioType
                    ] as [String: AnyObject]
        }
        return ["investment_values": 1,
                "positive_values": ["Animal rights", "Family values", "Gender equality", "Immigration", "Vegan"] as AnyObject,
                "negative_values": ["Coal", "Firearms", "Fur leather", "GMO", "Palm oil", "Pesticides", "Tobacco", "Controversial weapons", "Military contract"] as AnyObject,
                "esg_percentile": 75,
                "env_percentile": 50,
                "soc_percentile": 50,
                "gov_percentile": 50,
                "minorities": 25,
                "interest_bearing": 0,
                "template": "peace"
                ] as [String: AnyObject]
    }
    
    func getVeganTemplateParams(isPortfolioUseCase: Bool = false, risk: Int = 0, portfolioType: String = "") -> [String: AnyObject] {
        if isPortfolioUseCase {
            return [
                    "positive_values": ["Animal rights", "Family values", "Gender equality", "Immigration", "Vegan", "LGBTQ"] as AnyObject,
                    "negative_values": ["Fur leather", "GMO", "Palm oil", "Pesticides", "Tobacco"] as AnyObject,
                    "esg_percentile": 50,
                    "env_percentile": 50,
                    "soc_percentile": 50,
                    "gov_percentile": 50,
                    "minorities": 25,
                    "interest_bearing": 0,
                    "template": "vegan",
                    "risk": getRiskDist(risk),
                    "portfolio_type": portfolioType
                    ] as [String: AnyObject]
        }
        return ["investment_values": 1,
                "positive_values": ["Animal rights", "Family values", "Gender equality", "Immigration", "Vegan", "LGBTQ"] as AnyObject,
                "negative_values": ["Fur leather", "GMO", "Palm oil", "Pesticides", "Tobacco"] as AnyObject,
                "esg_percentile": 50,
                "env_percentile": 50,
                "soc_percentile": 50,
                "gov_percentile": 50,
                "minorities": 25,
                "interest_bearing": 0,
                "template": "vegan"
                ] as [String: AnyObject]
    }
    
    func getSocTemplateParams(isPortfolioUseCase: Bool = false, risk: Int = 0, portfolioType: String = "") -> [String: AnyObject] {
        if isPortfolioUseCase {
            return [
                    "positive_values": ["Animal rights", "Family values", "Gender equality", "Immigration", "Vegan"]  as AnyObject,
                    "negative_values": ["Coal", "Firearms", "Fur leather", "GMO", "Palm oil", "Pesticides", "Tobacco", "Controversial weapons"] as AnyObject,
                    "esg_percentile": 75,
                    "env_percentile": 50,
                    "soc_percentile": 75,
                    "gov_percentile": 50,
                    "minorities": 25,
                    "template": "social",
                    "interest_bearing": 0,
                    "risk": getRiskDist(risk),
                    "portfolio_type": portfolioType
                    ] as [String: AnyObject]
        }
        return ["investment_values": 1,
                "positive_values": ["Animal rights", "Family values", "Gender equality", "Immigration", "Vegan"] as AnyObject,
                "negative_values": ["Coal", "Firearms", "Fur leather", "GMO", "Palm oil", "Pesticides", "Tobacco", "Controversial weapons"] as AnyObject,
                "esg_percentile": 75,
                "env_percentile": 50,
                "soc_percentile": 75,
                "gov_percentile": 50,
                "minorities": 25,
                "template": "social",
                "interest_bearing": 0] as [String: AnyObject]
    }
    
    func getGovTemplateParams(isPortfolioUseCase: Bool = false, risk: Int = 0, portfolioType: String = "") -> [String: AnyObject] {
        if isPortfolioUseCase {
            return [
                    "positive_values": ["Animal rights", "Family values", "Gender equality", "Immigration", "Vegan"]  as AnyObject,
                    "negative_values": ["Coal", "Firearms", "Fur leather", "GMO", "Palm oil", "Pesticides", "Tobacco", "Controversial weapons"] as AnyObject,
                    "esg_percentile": 75,
                    "env_percentile": 50,
                    "soc_percentile": 50,
                    "gov_percentile": 75,
                    "minorities": 25,
                    "template": "governance",
                    "interest_bearing": 0,
                    "risk": getRiskDist(risk),
                    "portfolio_type": portfolioType
                    ] as [String: AnyObject]
        }
        return ["investment_values": 1,
                "positive_values": ["Animal rights", "Family values", "Gender equality", "Immigration", "Vegan"] as AnyObject,
                "negative_values": ["Coal", "Firearms", "Fur leather", "GMO", "Palm oil", "Pesticides", "Tobacco", "Controversial weapons"] as AnyObject,
                "esg_percentile": 75,
                "env_percentile": 50,
                "soc_percentile": 50,
                "gov_percentile": 75,
                "minorities": 25,
                "template": "governance",
                "interest_bearing": 0] as [String: AnyObject]
    }
    
    func getRiskDist(_ risk: Int) -> [String: AnyObject] {
        return  ["raw_score": risk,
                 "scaled_score": getUserRiskScaledScore(rawScore: risk),
                 "type": getUserRiskType(rawScore: risk)] as [String: AnyObject]
    }
    
    func getSelectedTemplate(_ selectedTemplate: ValuesTemplate) -> String {
        switch selectedTemplate {
            case .Catholic:
                return "catholic"
                
            case .Environment:
                return "environment"
                
            case .Islamic:
                return "islamic"
                
            case .Methodist:
                return "methodist"
                
            case .Peace:
                return "peace"
                
            case .Vegan:
                return "vegan"
            case .Social:
                return "social"
            case .Governance:
                return "governance"
            case .NOT_SET:
                return ""
            default:
                return ""
        }
    }
    
    func getSelectedTemplateFromString(_ value: String) -> ValuesTemplate {
        switch value {
            case "catholic":
                return .Catholic
                
            case "environment":
                return .Environment
                
            case "islamic":
                return .Islamic
                
            case "methodist":
                return .Methodist
                
            case "peace":
                return .Peace
                
            case "vegan":
                return .Vegan
            case "social":
                return .Social
            case "governance":
                return .Governance
            default:
                return .NOT_SET
        }
    }
    
    func getSelectedTemplateIndex(_ value: String) -> Int {
        switch value {
            case "catholic":
                return 0
                
            case "environment":
                return 1
                
            case "islamic":
                return 2
                
            case "methodist":
                return 3
                
            case "peace":
                return 4
                
            case "vegan":
                return 5
            default:
                return -1
        }
    }
    
}


