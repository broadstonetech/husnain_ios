//
//  IncrementalSearchVC.swift
//  Fintech
//
//  Created by broadstone on 03/02/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import Foundation
import UIKit

struct SearchData {
    var symbolName: String
    var industry: String
    var portfolioComposition: OverallNewEsgModel
    var envData, socialData, governanceData : NewEsgScoreModel
    var symbol: String
}

class IncrementalSearchVC: Parent,UITableViewDelegate,UITableViewDataSource {
    
    var searchesResult = [SearchData]()

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchResultLabel: UILabel!
    
    @IBOutlet weak var nameSearchImg: UIImageView!
    @IBOutlet weak var symbolSearchImg: UIImageView!
    
    @IBOutlet weak var clearBtn: UIButton!
    
    var trendingTickersModel: PortfolioTickersModel!
    
    var isSearchByNameSelected = true
    var isCallingTrendingAPI = true
    
    var incrementalSearchModel: IncrementalSearchModel!
    
    let countryNameArray = ["Apple Inc.", "Google Inc.", "Facebook Inc.", "Tesla Inc.",
                            "iRhythm Technologies Inc,", "CRISPR Therapeutics AG.", "BeiGene Ltd."]

    var searchCountry = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchBar.placeholder = getLocalizedString(key: "search_by_name")
        
        searchBar.delegate = self
        
        clearBtn.isHidden = true
        
        getTickersFromAPI(url: ApiUrl.PORTFOLIO_TRENDING_TICKERS, params: getParamsForTrending())
        
        searchByNamePressed(self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
        //setNavBarTitile(title: "Incremental search", topItem: "Incremental search")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    
    @IBAction func clearBtnPressed(_ sender: UIButton){
        isCallingTrendingAPI = true
        self.tableView.reloadData()
        clearBtn.isHidden = true
        searchBar.text = ""
    }
    
    @IBAction func searchByNamePressed(_ sender: Any) {
        isSearchByNameSelected = true
        nameSearchImg.image = UIImage(named: "radio_on_button")!
        symbolSearchImg.image = UIImage(named: "radio_off_button")!
        
        searchBar.placeholder = getLocalizedString(key: "search_by_name")
        
        setAnalyticsAction(ScreenName: "IncrementalSearchVC", method: "SearchByName")
        
    }
    
    @IBAction func searchBySymbolPressed(_ sender: UIButton) {
        isSearchByNameSelected = false
        
        symbolSearchImg.image = UIImage(named: "radio_on_button")!
        nameSearchImg.image = UIImage(named: "radio_off_button")!
        
        searchBar.placeholder = getLocalizedString(key: "search_by_symbol")
        
        setAnalyticsAction(ScreenName: "IncrementalSearchVC", method: "SearchBySymbol")
        
    }
    
    @IBAction func back(_ sender: Any) {
        self.PopVC()
    }
    
    
    
    //MARK: -Populating TableView
    
    // Getting the number of rows
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        if incrementalSearchModel == nil{
//            return 0
//        }else{
//            return incrementalSearchModel.message.count
//        }
        if isCallingTrendingAPI {
            if trendingTickersModel == nil {
                return 0
            }else {
                return trendingTickersModel.data.count
            }
        }else {
            return searchesResult.count
        }
    }
    //populating the tableview
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchBarCell") as! SearchBarCell
        
        //cell.nameLabel.text = incrementalSearchModel.message[indexPath.row].symbolName
        if isCallingTrendingAPI {
            cell.nameLabel.text = trendingTickersModel.data[indexPath.row].CompanyName
        }else {
            cell.nameLabel.text = searchesResult[indexPath.row].symbolName
        }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isCallingTrendingAPI {
            getSearchResultAPI(tickerName: trendingTickersModel.data[indexPath.row].symbol, isNavigatingToNextVC: true, isExactMatch: true)
        }else {
            let vc = UIStoryboard.init(name: "InvestmentDetails", bundle: Bundle.main).instantiateViewController(withIdentifier: "SearchResultsNewVC") as? SearchResultNewVC
            vc?.searchData = searchesResult[indexPath.row]
            self.navigationController?.pushViewController(vc!, animated: true)
        }
    }
}


    //MARK: - SearchBar Extensions
extension IncrementalSearchVC : UISearchBarDelegate {
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        let searchText = searchBar.text
        
        if searchText!.isEmpty {
            
            
        }else {
            searchBar.endEditing(true)
            getSearchResultAPI(tickerName: searchText!)
        }
        
        }
}
    //MARK: Alert Extension
extension UIAlertController {
    class func alert(title:String, msg:String, target: UIViewController) {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default) {
        (result: UIAlertAction) -> Void in
        })
        target.present(alert, animated: true, completion: nil)
    }
}
extension IncrementalSearchVC {
    func  getSearchResultAPI(tickerName: String, isNavigatingToNextVC: Bool = false, isExactMatch:Bool = false){
        self.view.activityStartAnimating()
        let url = ApiUrl.GET_TICKER_SEARCH_DATA_NEW
        let namespace = SPManager.sharedInstance.getCurrentUserNamespace()
        //let namespace = "kh01-sdRt"
        
        var params: Dictionary<String,AnyObject>
        
        if isExactMatch {
            params =
                ["namespace" : namespace  as AnyObject,
                 "symbol_name": tickerName as AnyObject,
                 "is_exact_match": true as AnyObject]
            print(params)
        }else {
            params =
                ["namespace" : namespace  as AnyObject,
                 "symbol_name": tickerName as AnyObject,
                 "is_exact_match": !isSearchByNameSelected as AnyObject]
            print(params)
        }
        
        APIManager.sharedInstance.postRequest(serviceName: url, sendData: params, success: { (response) in
            print(response)
            
            if APIManager.sharedInstance.isSuccess {
                let res = response["message"] as! [AnyObject]
                if isNavigatingToNextVC {
                    DispatchQueue.main.async {
                        self.fetchSearchDataForNextVC(items: res)
                    }
                }else {
                    DispatchQueue.main.async {
                        self.fetchSearchData(items: res)
                    }
                }
            }else{
                
                DispatchQueue.main.async { [self] in
                    if response["message"] != nil {
                        self.showAlert(response["message"] as! String)
                    } else {
                        self.showAlert(ConstantStrings.NO_RESULT_FOUND_ALERT_TEXT)
                    }
                }
            }
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
        }, failure: { (data) in
            print(data)
            DispatchQueue.main.async { [self] in
                view.activityStopAnimating()
            }
            if APIManager.sharedInstance.error400 {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
            }
            if(APIManager.sharedInstance.error401)
            {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
            }else{
                self.showAlert(ConstantStrings.Error_msg_generic)
            }
        })
        
    }
    private func fetchSearchData(items: [AnyObject]){
        searchesResult.removeAll()
        for i in 0..<items.count {
            let response = items[i] as! [String: AnyObject]
            
            let symbolName = response["Symbol_name"] as! String
            let industry = response["industry"] as! String
            let ticker = (response["ticker"] as? String) ?? ""
            
            let portfolioComposition = response["portfolio_composition"] as! [String: AnyObject]
            
            
            let controversy = portfolioComposition["Controversy"] as! Int
            let overallImpact = portfolioComposition["overall_impact"] as! String
            let overallBetterPeers = portfolioComposition["overall_better_peers"] as! String
            let overallBadPeers = portfolioComposition["overall_bad_peers"] as! String
            
            let envioromentData = fetchEsgValues(portfolioComposition: portfolioComposition, key: "environment")
            let socialData = fetchEsgValues(portfolioComposition: portfolioComposition, key: "social")
            let governanceData = fetchEsgValues(portfolioComposition: portfolioComposition, key: "governance")
            
            let overallData = OverallNewEsgModel.init(controversy: controversy, impact: overallImpact, betterPeers: overallBetterPeers, badPeers: overallBadPeers)
            
            let searchData = SearchData(symbolName: symbolName, industry: industry, portfolioComposition: overallData, envData: envioromentData, socialData: socialData, governanceData: governanceData, symbol: ticker)
            
            searchesResult.append(searchData)
            
        }
        isCallingTrendingAPI = false
        clearBtn.isHidden = false
        searchResultLabel.text = getLocalizedString(key: "search_results")
        tableView.reloadData()
    }
    
    private func fetchSearchDataForNextVC(items: [AnyObject]) {
        let i = 0
        let response = items[i] as! [String: AnyObject]
        print(response)
        
        let symbolName = response["Symbol_name"] as! String
        let industry = response["industry"] as! String
        let ticker = (response["ticker"] as? String) ?? ""
        
        let portfolioComposition = response["portfolio_composition"] as! [String: AnyObject]
        
        let controversy = portfolioComposition["Controversy"] as! Int
        let overallImpact = portfolioComposition["overall_impact"] as! String
        let overallBetterPeers = portfolioComposition["overall_better_peers"] as! String
        let overallBadPeers = portfolioComposition["overall_bad_peers"] as! String
        let zScore = portfolioComposition["z_score"] as! Double
        
        let envioromentData = fetchEsgValues(portfolioComposition: portfolioComposition, key: "environment")
        let socialData = fetchEsgValues(portfolioComposition: portfolioComposition, key: "social")
        let governanceData = fetchEsgValues(portfolioComposition: portfolioComposition, key: "governance")
        
        let overallData = OverallNewEsgModel.init(controversy: controversy, impact: overallImpact, betterPeers: overallBetterPeers, badPeers: overallBadPeers, zScore: zScore)
        
        let searchData = SearchData(symbolName: symbolName, industry: industry, portfolioComposition: overallData, envData: envioromentData, socialData: socialData, governanceData: governanceData, symbol: ticker)
        
        DispatchQueue.main.async {
            let vc = UIStoryboard.init(name: "InvestmentDetails", bundle: Bundle.main).instantiateViewController(withIdentifier: "SearchResultsNewVC") as? SearchResultNewVC
            vc?.searchData = searchData
            self.navigationController?.pushViewController(vc!, animated: true)
        }
    }
    
    private func fetchEsgValues(portfolioComposition: [String: AnyObject], key: String) -> NewEsgScoreModel{
        let esg = portfolioComposition[key] as! [String: AnyObject]
        let impact = esg["impact"] as! String
        let betterPeers = esg["better_peers"] as! String
        let badPeers = esg["bad_peers"] as! String
        let zScore = esg["z_score"] as! Double
        
        let derivedValues = esg["derived_values"] as! [AnyObject]
        
        var derivedValuesModel = [EsgDerivedValues]()
        for i in 0..<derivedValues.count {
            let derivedValue = derivedValues[i] as! [String: AnyObject]
            let name = derivedValue["name"] as! String
            let derivedScore = derivedValue["score"] as! Double
            
            derivedValuesModel.append(EsgDerivedValues.init(valueName: name, score: derivedScore))
        }
        
        let esgData = NewEsgScoreModel.init(impact: impact, betterPeers: betterPeers, badPeers: badPeers, zScore: zScore, derivedValues: derivedValuesModel)
        return esgData
        
    }
}
extension IncrementalSearchVC {
    private func getTickersFromAPI(url: String, params: [String: AnyObject]){
        self.view.activityStartAnimating()
        print(params)
        
        APIManager.sharedInstance.postRequest(serviceName: url, sendData: params, success: { (response) in
            print(response)
            
            if APIManager.sharedInstance.isSuccess {
                DispatchQueue.main.async {
                    self.trendingTickersModel = self.fetchTickersResponse(res: response)
                    self.clearBtn.isHidden = true
                    self.searchResultLabel.isHidden = false
                    self.searchResultLabel.text = getLocalizedString(key: "trending_tickers_for_u")
                    self.tableView.reloadData()
                }
            }else{
                
                DispatchQueue.main.async { [self] in
                    if response["message"] != nil {
                        self.showAlert(response["message"] as! String)
                    } else {
                        self.showAlert(ConstantStrings.NO_RESULT_FOUND_ALERT_TEXT)
                    }
                }
            }
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
        }, failure: { (data) in
            print(data)
            DispatchQueue.main.async { [self] in
                view.activityStopAnimating()
            }
            if APIManager.sharedInstance.error400 {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
            }
            if(APIManager.sharedInstance.error401)
            {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
            }else{
                self.showAlert(ConstantStrings.Error_msg_generic)
            }
        })
    }
    
    private func getParamsForTrending() -> [String: AnyObject] {
        return ["namespace": SPManager.sharedInstance.getCurrentUserNamespace()] as [String: AnyObject]
    }
    
    private func fetchTickersResponse(res: [String: AnyObject]) -> PortfolioTickersModel {
        let success = res["success"] as! Bool
        let message = res["message"] as! String
        let data = res["data"] as! [AnyObject]
        
        var symbols = [PortfolioTickersData]()
        for i in 0..<data.count {
            let asset = data[i] as! [String: String]
            let symbol = asset["symbol"]!
            let company = asset["company_name"]!
            
            symbols.append(PortfolioTickersData.init(symbol: symbol, CompanyName: company))
        }
        
        return PortfolioTickersModel.init(success: success, message: message, data: symbols)
    }
}
