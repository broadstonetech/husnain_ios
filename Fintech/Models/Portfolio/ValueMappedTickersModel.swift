//
//  ValueMappedTickersModel.swift
//  Fintech
//
//  Created by broadstone on 11/08/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//


import Foundation

struct ValueMappedTickersModel {
    let success: Bool
    let message: String
    var data: [PortfolioTickersData]
}
//struct ValueMappedTickersData {
//    let buckets: [TickersBucketsData]
//}
//struct TickersBucketsData {
//    let bucketName: String
//    let assests: [PortfolioTickersData]
//}
