//
//  ItemDetailsVC.swift
//  Fintech
//
//  Created by MacBook-Mubashar on 28/08/2019.
//  Copyright © 2019 Broadstone Technologies. All rights reserved.
//

import UIKit

class NotificationDetailsVC: Parent {
    
    //IB outlets
    @IBOutlet var mainHeading_lbl: UILabel!
    @IBOutlet var subHeading_lbl: UILabel!
    @IBOutlet var description_lbl: UILabel!
    @IBOutlet var disclaimer_btn: UIButton!
    
    //class variables
    var selectedNotificationIndex : Int = -1
    var disclaimerLinkString : String = ""
    
    @IBAction func Back_btn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let item = ArrayLists.sharedInstance.notificationsDataList[selectedNotificationIndex]
        disclaimerLinkString = item.getInfoDisclaimerLink()
        mainHeading_lbl.text = item.getInfoHeading()
        subHeading_lbl.text = item.getInfoSubHeading()
        description_lbl.text = item.getInfoDescription()
        disclaimer_btn.setTitle(item.getInfoDisclaimerText(),for: .normal)
        disclaimer_btn.addTarget(self, action: #selector(viewDiscalimerTapped), for: UIControl.Event.touchUpInside)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    override func viewDidAppear(_ animated: Bool) {
        setNavBarTitile(title: "", topItem: "Details")
    }
    
    // MARK: - Class Functions
    @objc func viewDiscalimerTapped() {
        viewPdfFile(urlString: disclaimerLinkString,urlType: "")
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
}
