//
//  PlusBudgetingTableViewCell.swift
//  vesgo
//
//  Created by Apple on 13/01/2021.
//  Copyright © 2021 Apple. All rights reserved.
//

import UIKit

class addDateBudgetingTableViewCell: UITableViewCell {

    
    @IBOutlet weak var date_lbl: UILabel!
    
    @IBOutlet weak var imageBgView: UIView!
 
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var amount_textfield: UITextField!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
