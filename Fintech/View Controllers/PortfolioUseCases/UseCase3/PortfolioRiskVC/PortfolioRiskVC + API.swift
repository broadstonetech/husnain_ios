//
//  PortfolioRiskVC + API.swift
//  Fintech
//
//  Created by broadstone on 02/03/2022.
//  Copyright © 2022 Broadstone Technologies. All rights reserved.
//

import Foundation
struct PortfolioRiskBoundryValue {
    static let lowRiskBoundryValue = 1
    static let mediumRiskBoundryValue = 23
    static let highRiskBoundryValue = 33
}


extension PortfolioRiskVC {
    
    func callValueMappedApi() {
        valueMappedSuggestedPortfolios.removeAll()
        if QuestionnaireCalculationHelper.sharedInstance.getUserRiskType(rawScore: riskScore) == "conservative" {
            valueMappedSuggestedPortfolios.append(ValueMappedPortfolioData.init(portfolioId: 0, risk: riskScore, template: template, portfolioData: nil))
            valueMappedSuggestedPortfolios.append(ValueMappedPortfolioData.init(portfolioId: 1, risk: PortfolioRiskBoundryValue.mediumRiskBoundryValue, template: template, portfolioData: nil))
            valueMappedSuggestedPortfolios.append(ValueMappedPortfolioData.init(portfolioId: 2, risk: PortfolioRiskBoundryValue.highRiskBoundryValue, template: template, portfolioData: nil))
            
        }else if QuestionnaireCalculationHelper.sharedInstance.getUserRiskType(rawScore: riskScore) == "neutral" {
            valueMappedSuggestedPortfolios.append(ValueMappedPortfolioData.init(portfolioId: 0, risk: riskScore, template: template, portfolioData: nil))
            valueMappedSuggestedPortfolios.append(ValueMappedPortfolioData.init(portfolioId: 1, risk: PortfolioRiskBoundryValue.lowRiskBoundryValue, template: template, portfolioData: nil))
            valueMappedSuggestedPortfolios.append(ValueMappedPortfolioData.init(portfolioId: 2, risk: PortfolioRiskBoundryValue.highRiskBoundryValue, template: template, portfolioData: nil))
            
        }else if QuestionnaireCalculationHelper.sharedInstance.getUserRiskType(rawScore: riskScore) == "aggressive" {
            valueMappedSuggestedPortfolios.append(ValueMappedPortfolioData.init(portfolioId: 0, risk: riskScore, template: template, portfolioData: nil))
            valueMappedSuggestedPortfolios.append(ValueMappedPortfolioData.init(portfolioId: 1, risk: PortfolioRiskBoundryValue.lowRiskBoundryValue, template: template, portfolioData: nil))
            valueMappedSuggestedPortfolios.append(ValueMappedPortfolioData.init(portfolioId: 2, risk: PortfolioRiskBoundryValue.mediumRiskBoundryValue, template: template, portfolioData: nil))
        }
        let risk1 = valueMappedSuggestedPortfolios[0].risk
        let risk2 = valueMappedSuggestedPortfolios[1].risk
        let risk3 = valueMappedSuggestedPortfolios[2].risk
        
        let param1 = getValuesCalculationParams(template: template, rawRisk: risk1, portfolioType: portfolioType, postiveVals: positiveVals, negativeVals: negativeVals)
        let param2 = getValuesCalculationParams(template: template, rawRisk: risk2, portfolioType: portfolioType, postiveVals: positiveVals, negativeVals: negativeVals)
        let param3 = getValuesCalculationParams(template: template, rawRisk: risk3, portfolioType: portfolioType, postiveVals: positiveVals, negativeVals: negativeVals)
        
        generatePortfolioAPI(url: ApiUrl.GET_VALUE_MAPPED_PORTFOLIO_IMPACT, params: param1, returnData: valueMappedSuggestedPortfolios[0])
        generatePortfolioAPI(url: ApiUrl.GET_VALUE_MAPPED_PORTFOLIO_IMPACT, params: param2, returnData: valueMappedSuggestedPortfolios[1])
        generatePortfolioAPI(url: ApiUrl.GET_VALUE_MAPPED_PORTFOLIO_IMPACT, params: param3, returnData: valueMappedSuggestedPortfolios[2])
    }
    
    private func generatePortfolioAPI(url: String, params: [String: AnyObject], returnData: ValueMappedPortfolioData){
        self.view.activityStartAnimating()
        
        let payload = ["namespace": SPManager.sharedInstance.getCurrentUserNamespace(),
                       "data": params] as [String: AnyObject]
        
        APIManager.sharedInstance.postRequest(serviceName: url, sendData: payload, success: { (response) in
            print(response)
            
            if APIManager.sharedInstance.isSuccess {
                DispatchQueue.main.async { [self] in
                    let data = self.fetchExternalPortfolioResponse(res: response)!
                    self.valueMappedSuggestedPortfolios[returnData.portfolioId].portfolioData = data
                    self.apiCounter += 1
                    print("Api Called: \(self.apiCounter)")
                    if self.apiCounter == 3 {
                        print("Api Called 3 times")
                        self.pushValueSuggestedPortfolioVC(data: self.valueMappedSuggestedPortfolios, portfolioType: portfolioType, posVals: positiveVals, negVals: negativeVals)
                    }
                }
            }else{
                
                DispatchQueue.main.async { [self] in
                    if response["message"] != nil {
                        self.showAlert(response["message"] as! String)
                    } else {
                        self.showAlert(ConstantStrings.NO_RESULT_FOUND_ALERT_TEXT)
                    }
                }
            }
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
        }, failure: { (data) in
            print("Error: \(data)")
            DispatchQueue.main.async { [self] in
                view.activityStopAnimating()
            }
            if APIManager.sharedInstance.error400 {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
            }
            if(APIManager.sharedInstance.error401)
            {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
            }else{
                self.showAlert(ConstantStrings.Error_msg_generic)
            }
        })
    }
}
