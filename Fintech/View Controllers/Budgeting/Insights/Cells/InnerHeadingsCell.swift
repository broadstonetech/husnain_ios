//
//  InsightDetailCell.swift
//  Task1
//
//  Created by Billal on 29/01/2021.
//

import UIKit

class InnerHeadingsCell: UITableViewCell {

    @IBOutlet weak var heading1Lbl: UILabel!
    @IBOutlet weak var headings2Lbl: UILabel!
    @IBOutlet weak var heading3Lbl: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
