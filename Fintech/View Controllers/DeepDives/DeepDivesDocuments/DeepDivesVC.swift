//
//  DeepDivesVC.swift
//  Fintech
//
//  Created by broadstone on 03/02/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import Foundation
import UIKit

class DeepDivesVC: Parent {
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        self.navigationController?.navigationBar.isHidden = false
        setNavBarTitile(title: "Deep dives", topItem: "Deep dives")
        tableView.delegate = self
        tableView.dataSource = self
        
        getLibData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    @IBAction func back(_ sender: Any) {
        self.PopVC()
    }
    
    
    func getLibData() {
        DispatchQueue.main.async {
            self.view.activityStartAnimating()
        }
        let url = ApiUrl.GET_LIBRARY_DATA_URL
        let namespace = SPManager.sharedInstance.getCurrentUserNamespace()
        let data:Dictionary<String,AnyObject> = [:]
        
        let params:Dictionary<String,AnyObject> = ["namespace" : namespace as AnyObject, "data" : data as AnyObject]
      
        APIManager.sharedInstance.postRequest(serviceName: url, sendData: params, success: { (response) in
          
            
            if APIManager.sharedInstance.isSuccess {
                let res = response["message"] as AnyObject
                DispatchQueue.main.async {
                    
                    
                    
                    self.fetchMetaData(res: res as! [String : AnyObject], table_view: self.tableView)
                }
                
            }else{
                if response["message"] != nil {
                    self.showAlert(response["message"] as! String)
                } else {
                    self.showAlert(ConstantStrings.NO_RESULT_FOUND_ALERT_TEXT)
                }
            }
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
        }, failure: { (data) in
           
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
            if APIManager.sharedInstance.error400 {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
            }
            if(APIManager.sharedInstance.error401)
            {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
            }else{
                
            }
        })
        
    }
    
    
    // MARK: - fetchData
    func fetchMetaData(res : [String : AnyObject], table_view : UITableView) {
        ArrayLists.sharedInstance.LibraryMainDataList.removeAll()
        ArrayLists.sharedInstance.LibraryDocsDataList.removeAll()
        ArrayLists.sharedInstance.LibraryMediaDataList.removeAll()
        
        //let mainData = res["main_data"] as! [[String : AnyObject]]
        let docsData = res["doc_data"] as! [[String : AnyObject]]
        //let mediaData = res["media_data"] as! [[String : AnyObject]]
        
        DispatchQueue.main.async {
            
           // ArrayLists.sharedInstance.LibraryMainDataList = self.parseJsonList(data: mainData)
            ArrayLists.sharedInstance.LibraryDocsDataList = self.parseJsonList(data: docsData)
            //ArrayLists.sharedInstance.LibraryMediaDataList = self.parseJsonList(data: mediaData)
        }
        
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    func parseJsonList(data : [[String : AnyObject]]) -> [LibraryDataModel] {
        var list : [LibraryDataModel] = [LibraryDataModel]()
        for jsonData in data {
            let model = LibraryDataModel()
            var SELECTED_DATA_TYPE : String = ""
            let mainHeading = jsonData["main_heading"] as! String
            let subHeading = jsonData["subheading"] as! String
            //parse model scaling json
            let description = jsonData["description"] as! String
            let iconUrl = jsonData["icon_url"] as! String
            let progressValue = jsonData["progress_value"] as! Double
            let dataType = jsonData["type"] as! String
            let timestamp = jsonData["eve_sec"] as! Int
            
            if jsonData["additional_info"] as? [String : AnyObject] != nil {
                let additionalInfo = jsonData["additional_info"] as! [String : AnyObject]
                //parse additional info
                //            if additionalInfo != nil {
                let infoType = additionalInfo["type"] as! String
                
                model.setInfoType(type: infoType)
                if infoType == "text" {
                    let infoValue = additionalInfo["value"] as! [String : AnyObject]
                    let infoHeading = infoValue["info_heading"] as! String
                    let infoSubHeading = infoValue["info_sub_heading"] as! String
                    let infoDescription = infoValue["info_description"] as! String
                    let infoDisclaimerText = infoValue["info_disclaimer_text"] as! String
                    let infoDisclaimerLink = infoValue["info_disclaimer_link"] as! String
                    let infoDisclaimerLinkType = infoValue["info_disclaimer_link_type"] as! String
                    
                    model.setInfoHeading(text: infoHeading)
                    model.setInfoSubHeading(text: infoSubHeading)
                    model.setInfoDescription(text: infoDescription)
                    model.setInfoDisclaimerText(text: infoDisclaimerText)
                    model.setInfoDisclaimerLink(text: infoDisclaimerLink)
                    model.setInfoDisclaimerLinkType(text: infoDisclaimerLinkType)
                } else {
                    let infoValue = additionalInfo["value"] as! String
                    model.setInfoValue(value: infoValue)
                }
            } else {
                model.setInfoType(type: "none")
            }
            
            model.setMainHeadingText(text: mainHeading)
            model.setSubHeadingText(text: subHeading)
            model.setDescriptionText(text: description)
            model.setIconImage(text: iconUrl)
            model.setProgressValue(text: progressValue)
            model.setDataType(text: dataType)
            
            list.append(model)
        }
        
        return list
    }
}
extension DeepDivesVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ArrayLists.sharedInstance.LibraryDocsDataList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let keyDocsCell =  tableView.dequeueReusableCell(withIdentifier: "KeyDocsCell") as! DeepDivesCell
        let item = ArrayLists.sharedInstance.LibraryDocsDataList[indexPath.row]
        keyDocsCell.heading_lbl.text = item.getMainHeadingText()
        keyDocsCell.description_lbl.text = item.getDescriptionText()
        
        return keyDocsCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = ArrayLists.sharedInstance.LibraryDocsDataList[indexPath.row]
        
        FintechUtils.sharedInstance.openUrl(url: item.getInfoValue())
    }
    
    
}
