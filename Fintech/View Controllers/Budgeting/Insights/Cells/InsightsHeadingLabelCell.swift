//
//  InsightsHeadingLabelCell.swift
//  Fintech
//
//  Created by broadstone on 15/02/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import Foundation
import UIKit

class InsightsHeadingLabelCell: UITableViewCell {
    @IBOutlet weak var headingLbl: UILabel!
    
    override class func awakeFromNib() {
        super.awakeFromNib()
    }
}
