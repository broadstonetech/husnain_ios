//
//  LogInVC.swift
//  Fintech
//////#################################################################
//  In This view user Login account
//    user name textfield
//    password  textfield
//    animation
//    forgetpassword Button
//    login button
//    create new account button move to signup screen
//    cancel button move to mainlogin page
//    hide and show password button (eye)
//    api call user Login cheak portfolio user status (true , false)
//    when portfolio status is true then move dashboard UIView.
//    when portfolio status is false then move HomeSurvey VC UIView.(Questionnaire)
//################################################################

import UIKit
import Toast_Swift
import Lottie
import LocalAuthentication

class LogInVC: Parent {
    // MARk: - IB outlets
    @IBOutlet weak var username_tf: UITextField!
    @IBOutlet weak var password_tf: UITextField!
    @IBOutlet weak var login_btn: UIButton!
    @IBOutlet var passwordToggle_btn: UIButton!
    @IBOutlet weak var biometricAuthBtn: UIButton!
    //Animation View outlet
    @IBOutlet weak var animation_img_view: AnimationView!
    
    @IBOutlet weak var createAccount_btn: UIButton!
    @IBOutlet weak var heading_login_lbl: UILabel!
    @IBOutlet weak var descripation_lbl: UILabel!
    
    
    @IBOutlet weak var forgetpassword_btn: UIButton!

    
    //Mark: - class variables
    var flag = true
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        animation_img_view.contentMode = .scaleAspectFit
        animation_img_view.animationSpeed = 1
        animation_img_view.play()
        username_tf.delegate = self
        password_tf.delegate = self
        print("Big difference in status or Class")
        login_btn.addTarget(self, action: #selector(loginTapped), for: UIControl.Event.touchUpInside)
        passwordToggle_btn.addTarget(self, action: #selector(viewPasswordTapped), for: UIControl.Event.touchUpInside)
        showBiometricOptions()
        // mark : -  Do any additional setup after loading the view.

    }
    override func viewWillAppear(_ animated: Bool) {
        localization()
    }
    func localization(){
        // setNavBarTitile(title:getLocalizedString(key: "Education", comment: ""), topItem: getLocalizedString(key: "Education", comment: ""))
        
        let headingUserlogin = getLocalizedString(key: "USER LOGIN", comment: "")
        let descripation = getLocalizedString(key: "Login to your account", comment: "")
         heading_login_lbl.text = headingUserlogin
        descripation_lbl.text = descripation
        login_btn.setTitle(getLocalizedString(key: "Login", comment: ""), for: .normal)
        forgetpassword_btn.setTitle(getLocalizedString(key: "Forgot password", comment: ""), for: .normal)
        createAccount_btn.setTitle(getLocalizedString(key: "Create account", comment: ""), for: .normal)
        username_tf.placeholder = getLocalizedString(key: "username_or_email", comment: "")
        password_tf.placeholder = getLocalizedString(key: "password_", comment: "")
        
        
        //navigationController?.navigationBar.barStyle = .blackOpaque
    }
    
    private func showBiometricOptions(){
        if LocalAuth.shared.hasTouchId(){
            if #available(iOS 13.0, *) {
                biometricAuthBtn.setImage(UIImage(systemName: "touchid"), for: .normal)
            }
        }else if LocalAuth.shared.hasFaceId(){
            if #available(iOS 13.0, *) {
                biometricAuthBtn.setImage(UIImage(systemName: "faceid"), for: .normal)
            }
        }else{
            biometricAuthBtn.isHidden = true
        }
    }
    
       //MARK: -IB Actions
    
    
    @IBAction func forgetpassword_pressed(_ sender: Any) {
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "Forgot") as!ForgotPasswordVC
        vc.modalPresentationStyle = .fullScreen
        //UIApplication.shared.keyWindow?.rootViewController = vc
        UIApplication.shared.windows[0].rootViewController = vc
    }
    
    @IBAction func localAuthAction(_ sender: Any) {
        print("hello there!.. You have clicked the touch ID")
        if SPManager.sharedInstance.isUserDetailSavedForBiometric() {
            let myContext = LAContext()
            var myLocalizedReasonString = ""

            if LocalAuth.shared.hasTouchId(){
                myLocalizedReasonString = "This app uses Touch ID to confirm your identity for login"
            }else if LocalAuth.shared.hasFaceId(){
                myLocalizedReasonString = "This app uses Face ID to confirm your identity for login"
            }
            
            var authError: NSError?
            if #available(iOS 8.0, macOS 10.12.1, *) {
                if myContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &authError) {
                    myContext.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: myLocalizedReasonString) { success, evaluateError in

                        DispatchQueue.main.async {
                            if success {
                                let userData = KeyChain.retrieveUserFromKeyChain()
                                self.callLoginApi(username: userData!.identifier, password: userData!.password)
                            } else {
                                let ac = UIAlertController(title: "Authentication failed", message: "You could not be verified; please try again.", preferredStyle: .alert)
                                ac.addAction(UIAlertAction(title: "OK", style: .default))
                                self.present(ac, animated: true)
                            }
                        }
                    }
                }else{
                    if let err = authError {
                        switch err.code{

                        case LAError.Code.biometryNotEnrolled.rawValue:
                            notifyUser("User is not enrolled",
                                       err: err.localizedDescription)

                        case LAError.Code.passcodeNotSet.rawValue:
                            notifyUser("Device passcode has not been set",
                                       err: err.localizedDescription)


                        case LAError.Code.biometryNotAvailable.rawValue:
                            notifyUser("Biometric authentication not available",
                                       err: err.localizedDescription)
                        default:
                            notifyUser("Unknown error",
                                       err: err.localizedDescription)
                        }
                    }
                }
            }
        }else {
            self.view.makeToast(ConstantStrings.INVALID_FINGER_PRINT_TEXT)
        }
    }
    
    func notifyUser(_ msg: String, err: String?) {
        let alert = UIAlertController(title: msg,
            message: err,
            preferredStyle: .alert)

        let cancelAction = UIAlertAction(title: "OK",
            style: .cancel, handler: nil)

        alert.addAction(cancelAction)

        self.present(alert, animated: true,
                            completion: nil)
    }
    
    
    @IBAction func creat_Accout_pressed(_ sender: Any) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "SignUp") as!SignUpVC
        vc.modalPresentationStyle = .fullScreen

        
    UIApplication.shared.keyWindow?.rootViewController = vc
        self.setAnalyticsAction(ScreenName:"LogInVc", method: "CreateAcoount")
    }
    
    @IBAction func Cancel_btn(_ sender: Any) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "Main_login_page") as!mainpageloninsignup
        vc.modalPresentationStyle = .fullScreen
        UIApplication.shared.keyWindow?.rootViewController = vc
    }
       //MARK: -Text field delegates
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == username_tf {
            return string.rangeOfCharacter(from: CharacterSet(charactersIn: "§±!#$%^&*+()-= []{};:/?>,<")) == nil
        }
        return string.rangeOfCharacter(from: CharacterSet(charactersIn: "")) == nil
        
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == username_tf {
            username_tf.resignFirstResponder()
            password_tf.becomeFirstResponder()
        } else if textField == password_tf {
            textField.resignFirstResponder()
        } else{
            textField.resignFirstResponder()
            
        }
        return true
    }
    
    //MARK: -Class Actions
    @objc func loginTapped() {
        
        if checkFormInputs() {
            let passwordSHA = password_tf.text?.data(using: String.Encoding.utf8)?.sha1()
            DispatchQueue.main.async {
                
                if NetworkReachability.isConnectedToNetwork() {
                    self.callLoginApi(username: self.username_tf.text!, password: passwordSHA!)
                    
                    self.setAnalyticsAction(ScreenName: "LogInVc", method: "loggedIn")
                    
                } else {
                    self.showAlert(ConstantStrings.NO_INTERNET_AVAILABLE)
                }
            }
        }
        
    }
    @objc func viewPasswordTapped() {
        showHidePwd(textfield: password_tf, btnPwd: passwordToggle_btn)
        let imageIv = UIImage(named: "invisible")
        let imageV = UIImage(named: "visibility")
        
        if flag == false {
        passwordToggle_btn.setImage(imageIv, for: .normal)
            flag = true
            print("true")
        } else if flag == true{
            passwordToggle_btn.setImage(imageV, for: .normal)
            flag = false
            print("false")
        }
    }
    
    
    // MARK: - API call--SignIn
    func callLoginApi(username : String, password : String) {
        self.view.activityStartAnimating()
        let url = ApiUrl.LOGIN_URL
        let data:Dictionary<String,AnyObject> = ["username": username as AnyObject , "password": password as AnyObject, "device_type": "ios" as AnyObject, "device_token":  FintechUtils.sharedInstance.DEVICE_TOKEN as AnyObject, "login_type" : "app" as AnyObject, "timestamp": getTimestamp() as AnyObject , "apple_token" : "" as AnyObject]
        
        print(username)
        let params:Dictionary<String,AnyObject> = ["login_type" : "app" as AnyObject, "data" : data as AnyObject]
        //  print(data)
        APIManager.sharedInstance.postRequest(serviceName: url, sendData: params, success: { (response) in
            print(response)
            
            if APIManager.sharedInstance.isSuccess {
                DispatchQueue.main.async {
                    self.deleteChat()
                }
                let res = response["message"] as AnyObject
                if res["namespace"] != nil {
                    SPManager.sharedInstance.saveUserLoginStatus(isLogin: true)
                    
                    DispatchQueue.main.async {
                        if SPManager.sharedInstance.shouldShowConsetDialogForBiometric() {
                            self.openAlertDialogForBiometricConsent(username: username, password: password, response: res)
                        }else {
                            self.fetchLoginDetails(res: res)
                        }
                        
                    }
                    
                } else {
                    DispatchQueue.main.async {
                        self.showAlert(getLocalizedString(key: "invalid_username_email", comment: ""))
                    }
                }
            }else{
                if response["message"] as AnyObject != nil {
                    DispatchQueue.main.async {
                        self.showAlert(response["message"] as! String)
                    }
                }else {
                    DispatchQueue.main.async {
                        self.showAlert(ConstantStrings.LOGIN_FAILED_ALERT_TEXT)
                    }
                }
            }
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
        }, failure: { (data) in
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
            if APIManager.sharedInstance.error400 {
                DispatchQueue.main.async {
                    self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
                }
            }
            if(APIManager.sharedInstance.error401)
            {
                DispatchQueue.main.async {
                    self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
                }
            }else{
                
            }
        })
    }
    func fetchLoginDetails(res : AnyObject) {
        let nameSpace = res["namespace"] as! String
        let firstname = res["first_name"] as! String
        let lastname = res["last_name"] as! String
        let email = res["email"] as! String
        let questionnaireStatus = res["questionnaire_status"] as! String
        let questionnaireExpiry = res["questionnaire_expiry"] as! Int
                // saveUserData(value: [summary](), key: email)
        SPManager.sharedInstance.saveCurrentUserEmail(email)
        SPManager.sharedInstance.saveCurrentUserNameForProfile(firstname)
        SPManager.sharedInstance.saveUserFullName(firstname+" "+lastname)
        SPManager.sharedInstance.saveCurrentUserNamespace(nameSpace)
        SPManager.sharedInstance.saveCurrentUserQuestionaireExpiry(expiry: String(describing: questionnaireExpiry))
        SPManager.sharedInstance.saveCurrentUserQuestionaireStatus(status: questionnaireStatus)
        print(email)
//        testusername.append(firstname+lastname)
//        usernameget.append(email)
//        print(testusername)
        DispatchQueue.main.async {
            if questionnaireStatus == "pending" { //go to questionnaire
                
                SPManager.sharedInstance.saveUserPortfolioStatus(isPortfolioComplete : false)
                
                let alert = UIAlertController(title: ConstantStrings.appName, message: ConstantStrings.user_status_pending_movequestionpage, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: getLocalizedString(key: "no_"), style: .cancel, handler: { action in
                    switch action.style{
                    case .cancel:
                        print("cancel")
                        guestuser = "aqib"
                        UserDefaults.standard.set(guestuser, forKey: "guestuser")
                        if let tabbedbar = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "nav_bar_start") as? UINavigationController {
                            tabbedbar.modalPresentationStyle = .fullScreen

                            UIApplication.shared.keyWindow?.rootViewController = tabbedbar
                        }
                    case .default:
                        print("default")
                        
                    case .destructive:
                        print("destructive")
                    }}))
                alert.addAction(UIAlertAction(title: getLocalizedString(key: "yes_"), style: .default, handler: { [self] action in
                    
//                    InvestmentStrategyVars.isFromAccountVC = false
                    guestuser = "aqib"
                    UserDefaults.standard.set(guestuser, forKey: "guestuser")
//                    userlevel = ""
//
//                    let vc = UIStoryboard(name: "InvestmentStrategy", bundle: nil).instantiateViewController(withIdentifier: "InvestmentQuestionnaire") as! InvestmentQuestionnaire
//                    vc.questionnaireStatusModel = nil
//
//                    vc.modalPresentationStyle = .fullScreen
//                    guestuser = "aqib"
//                    UserDefaults.standard.set(guestuser, forKey: "guestuser")
//
//                         UserDefaults.standard.set("", forKey: "userLevel")
//                    UserDefaults.standard.set("", forKey: "sampleportfolio_username")
//
//                    UIApplication.shared.keyWindow?.rootViewController = vc
                    self.getQuestionnaireStatusAPI()
                    
                    }))
                self.present(alert, animated: true, completion: nil)
                
                
                
            } else { // already built the strategy
                SPManager.sharedInstance.saveUserPortfolioStatus(isPortfolioComplete : true)
                

                if let tabbedbar = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "nav_bar_start") as? UINavigationController {
                    tabbedbar.modalPresentationStyle = .fullScreen
                    guestuser = "aqib"

                    UserDefaults.standard.set(guestuser, forKey: "guestuser")
                     UserDefaults.standard.set("", forKey: "userLevel")
                    UserDefaults.standard.set("", forKey: "sampleportfolio_username")

                    UIApplication.shared.keyWindow?.rootViewController = tabbedbar
                }
            }
        }
    }
    
    private func openAlertDialogForBiometricConsent(username: String, password: String, response: AnyObject){
        var alert:UIAlertController!
        if LocalAuth.shared.hasTouchId() {
            alert = UIAlertController(title: "Touch ID", message: "Would you like to enable Touch ID for future logins?", preferredStyle: UIAlertController.Style.alert)
        }else if LocalAuth.shared.hasFaceId(){
            alert = UIAlertController(title: "Face ID", message: "Would you like to enable Face ID for future logins?", preferredStyle: UIAlertController.Style.alert)
        }else{
            self.fetchLoginDetails(res: response)
            return
        }
        // add the actions (buttons)
        alert.addAction(UIAlertAction(title: "Enable", style: UIAlertAction.Style.default, handler: {action in
//            SPManager.sharedInstance.setUserNameForBiometric(userName: username)
//            SPManager.sharedInstance.setUserPassForBiometric(pass: password)
            let result = KeyChain.saveUserInKeyChain(user: KeyChain.User.init(identifier: username, password: password))
            print("KeyChain Save Result: \(result)")
            self.fetchLoginDetails(res: response)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: { action in
//            SPManager.sharedInstance.setUserNameForBiometric(userName: SPManager.SPManagerDefaultVals.USERNAME_FOR_BIOMETRIC_DEF_VAL)
//            SPManager.sharedInstance.setUserPassForBiometric(pass: SPManager.SPManagerDefaultVals.USER_PASS_FOR_BIOMETRIC_DEF_VAL)
            self.fetchLoginDetails(res: response)
        }))
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        username_tf.resignFirstResponder()
        password_tf.resignFirstResponder()
    }
}
//Mark :- Textfield Padding
extension UITextField {
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}
extension LogInVC{
    // MARK: - Input validations
    func checkFormInputs() -> Bool {
        guard let username = username_tf.text, !username.isEmpty else {
            showAlert(ConstantStrings.username_empty)
            return false
        }
        guard let password = password_tf.text, !password.isEmpty else {
            showAlert(ConstantStrings.password_empty)
            return false
        }
        
        return true
    }

    
    private func deleteChat() {
//        //first server will save the chat then we will delete from firebase
        let data :Dictionary<String,Any> = ["firebase_id": SPManager.sharedInstance.getCurrentFirebaseId() as String]
        print(data)
        APIManager.sharedInstance.postChatRequest(serviceName: "https://api.vesgo.io/save/chat", sendData: data as [String: AnyObject], success: {(response) in
            print(response)
            let defaults = UserDefaults.standard
            let statusCode = defaults.string(forKey: "statusCode")

            if ((statusCode?.range(of: "200")) != nil){
                DispatchQueue.main.async {
                    SPManager.sharedInstance.saveFirebaseid("")
                }
            }

        }, failure: { (data) in
            print(data)

        })
    }
}
