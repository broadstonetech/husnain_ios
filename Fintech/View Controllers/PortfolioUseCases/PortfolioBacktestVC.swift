//
//  PortfolioBacktestVC.swift
//  Fintech
//
//  Created by broadstone on 25/08/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import UIKit

class PortfolioBacktestVC: PortfolioParent {
    
    @IBOutlet weak var backtestDetailLabel: UILabel!
    @IBOutlet weak var vesgoLabel: UILabel!
    @IBOutlet weak var vesgoReturnPercent: UILabel!
    @IBOutlet weak var vesgoReturnDollar: UILabel!
    
    @IBOutlet weak var competitorLabel: UILabel!
    @IBOutlet weak var competitorReturnPercent: UILabel!
    @IBOutlet weak var competitorReturnDollar: UILabel!
    
    @IBOutlet weak var betaValueLabel: UILabel!
    @IBOutlet weak var volatilityValueLabel: UILabel!
    
    var backtestModel: PortfolioBacktestModel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let amount = "$" + FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: backtestModel.data.backtest.amount)
        let startDate = FintechUtils.sharedInstance.getDateInUSLocale(timestamp: backtestModel.data.backtest.startDate / 1000)
        let endDate = FintechUtils.sharedInstance.getDateInUSLocale(timestamp: backtestModel.data.backtest.endDate / 1000)
        
        backtestDetailLabel.text = String(format: getLocalizedString(key: "backtest_simulation_text"), amount, startDate, endDate)
        
        
        let vesgoData = backtestModel.data.performance[0]
        vesgoLabel.text = vesgoData.portfolioName
        vesgoReturnPercent.text = FintechUtils.sharedInstance.doubleToPercentageOutput(doubleVal: vesgoData.portfolioReturn) + "%"
        let vesgoAmount = (backtestModel.data.backtest.amount) * (vesgoData.portfolioReturn / 100.0)
        vesgoReturnDollar.text = "$" + FintechUtils.sharedInstance.doubleToPercentageOutput(doubleVal: vesgoAmount)
        
        let competitorData = backtestModel.data.performance[1]
        competitorLabel.text = competitorData.portfolioName
        competitorReturnPercent.text = FintechUtils.sharedInstance.doubleToPercentageOutput(doubleVal: competitorData.portfolioReturn) + "%"
        let competitorAmount = (backtestModel.data.backtest.amount) * (competitorData.portfolioReturn / 100.0)
        competitorReturnDollar.text = "$" + FintechUtils.sharedInstance.doubleToPercentageOutput(doubleVal: competitorAmount)
        
        betaValueLabel.text = String(backtestModel.data.risk.beta)
        volatilityValueLabel.text = String(backtestModel.data.risk.volatility)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        setNavBarTitile(title: "Back", topItem: getLocalizedString(key: "past_performance"))
    }
    
    
    

}
