//
//  InvestmentValuesNew.swift
//  Fintech
//
//  Created by broadstone on 08/03/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import UIKit
import Foundation
class InvestmentValuesNew: Parent {
    
    struct TableViewData {
        var opened: Bool
        var title: String
        var desc: String
        var data: Any
        var cardColor: UIColor
    }
    
    var tableViewDataArray = [TableViewData]()
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var dialogView: UIView!
    @IBOutlet weak var blurView: UIVisualEffectView!
    
    var env: NewEsgScoreModel!
    var soc: NewEsgScoreModel!
    var gov: NewEsgScoreModel!

    override func viewDidLoad() {
        super.viewDidLoad()
        

        tableView.delegate = self
        tableView.dataSource = self
        // Do any additional setup after loading the view.
        
        let closeDialogGesture = UITapGestureRecognizer.init(target: self, action: #selector(closeBtnTapped))
        blurView.isUserInteractionEnabled = true
        blurView.addGestureRecognizer(closeDialogGesture)
        
        getUserInvestmentValuesAPI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        setNavBarTitile(title: "Back", topItem: "Investment values")
    }
    
    func  getUserInvestmentValuesAPI(){
        self.view.activityStartAnimating()
        let url = ApiUrl.GET_USER_INVESTMENT_VALUES_NEW
        let namespace = SPManager.sharedInstance.getCurrentUserNamespace()
        //let namespace = "kh01-sdRt"
        
        let params:Dictionary<String,AnyObject> =
            ["namespace" : namespace  as AnyObject]
        print(params)
        
        APIManager.sharedInstance.postRequest(serviceName: url, sendData: params, success: { (response) in
            print(response)
            
            if APIManager.sharedInstance.isSuccess {
                let res = response["data"] as! [String : AnyObject]
                DispatchQueue.main.async {
                    self.fetchInvestmentValues(response: res)
                }
            }else{
                
                DispatchQueue.main.async { [self] in
                    if response["message"] != nil {
                        self.showAlert(response["message"] as! String)
                    } else {
                        self.showAlert(ConstantStrings.NO_RESULT_FOUND_ALERT_TEXT)
                    }
                }
            }
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
        }, failure: { (data) in
            print(data)
            DispatchQueue.main.async { [self] in
                view.activityStopAnimating()
            }
            if APIManager.sharedInstance.error400 {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
            }
            if(APIManager.sharedInstance.error401)
            {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
            }else{
                self.showAlert(ConstantStrings.Error_msg_generic)
            }
        })
        
    }
    
    private func fetchInvestmentValues(response : [String : AnyObject]){
        let positiveValues = response["positive_values"] as! [String]
        let portfolioComposition = response["portfolio_composition"] as! [String: AnyObject]
        
        let esg = portfolioComposition["Esg"] as! [String: AnyObject]
        let impact = esg["impact"] as! String
        let betterPeers = (esg["better_peers"] as? String) ?? ""
        let bad_peers = (esg["bad_peers"] as? String) ?? ""
        let zScore = (esg["z_score"] as? Double) ?? 0.0
        let riskScore = (esg["risk_score"] as? Double) ?? 0.0
        
        let envioromentData = fetchEsgValues(portfolioComposition: portfolioComposition, key: "Environment")
        let socialData = fetchEsgValues(portfolioComposition: portfolioComposition, key: "Social")
        let governanceData = fetchEsgValues(portfolioComposition: portfolioComposition, key: "Governance")
//        let othersData = fetchEsgValues(portfolioComposition: portfolioComposition, key: "others")
        
        let overallData = OverallNewEsgModel.init(controversy: 0, impact: impact, betterPeers: betterPeers, badPeers: bad_peers, zScore: zScore)
        
        env = envioromentData
        soc = socialData
        gov = governanceData
        
        let investmentValuesLabel = SPManager.sharedInstance.getCurrentUserNameForProfile() + "'s " + getLocalizedString(key: "investment_values_lbl", comment: "").lowercased()
        tableViewDataArray.append(TableViewData(opened: false, title: investmentValuesLabel, desc: getLocalizedString(key: "investment_values_desc", comment: ""), data: positiveValues, cardColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)))
        
        let controversy = fetchControversyData(res: portfolioComposition["controvercy_data"] as! [String: AnyObject])
        
        if controversy.count == 0 {
            tableViewDataArray.append(TableViewData(opened: false, title: getLocalizedString(key: "_controversy"), desc: getLocalizedString(key: "no_significant_controversy"), data: "", cardColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)))
        }else {
            tableViewDataArray.append(TableViewData(opened: false, title: getLocalizedString(key: "_controversy", comment: ""), desc: String(controversy.count), data: controversy, cardColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)))
        }
        
        tableViewDataArray.append(TableViewData(opened: false, title: getLocalizedString(key: "my_impact", comment: ""), desc: getLocalizedString(key: "my_impact_desc", comment: ""), data: overallData, cardColor: #colorLiteral(red: 0.9976013303, green: 0.9241877198, blue: 0.9816413522, alpha: 1)))
        tableViewDataArray.append(TableViewData(opened: false, title: getLocalizedString(key: "env_score", comment: ""), desc: getLocalizedString(key: "env_desc"), data: envioromentData, cardColor: #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1)))
        tableViewDataArray.append(TableViewData(opened: false, title: getLocalizedString(key: "social_score"), desc: getLocalizedString(key: "social_desc"), data: socialData, cardColor: #colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1)))
        tableViewDataArray.append(TableViewData(opened: false, title: getLocalizedString(key: "governance_score"), desc: getLocalizedString(key: "governance_desc"), data: governanceData, cardColor: #colorLiteral(red: 0.5058823824, green: 0.3372549117, blue: 0.06666667014, alpha: 1)))
        tableViewDataArray.append(TableViewData(opened: false, title: getLocalizedString(key: "esg_subfactors"), desc: "", data: 1, cardColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)))
        tableViewDataArray.append(TableViewData(opened: false, title: getLocalizedString(key: "how_i_improve"), desc: "", data: 1, cardColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)))
        tableViewDataArray.append(TableViewData(opened: false, title: getLocalizedString(key: "invest_in_charities"), desc: "", data: 1, cardColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)))
        
        tableView.reloadData()
    }
    
    private func fetchEsgValues(portfolioComposition: [String: AnyObject], key: String) -> NewEsgScoreModel{
        let esg = portfolioComposition[key] as! [String: AnyObject]
        
        let impact = esg["impact"] as! String
        let betterPeers = (esg["better_peers"] as? String) ?? ""
        let bad_peers = (esg["bad_peers"] as? String) ?? ""
        let zScore = (esg["z_score"] as? Double) ?? 0.0
        let riskScore = (esg["risk_score"] as? Double) ?? 0.0
        
        let derivedValues = esg["derived_values"] as! [AnyObject]
        
        var derivedValuesModel = [EsgDerivedValues]()
        for i in 0..<derivedValues.count {
            let derivedValue = derivedValues[i] as! [String: AnyObject]
            let name = derivedValue["name"] as! String
            let derivedScore = derivedValue["score"] as! Double
            
            derivedValuesModel.append(EsgDerivedValues.init(valueName: name, score: derivedScore))
        }
        
        let esgData = NewEsgScoreModel.init(impact: impact, betterPeers: betterPeers, badPeers: bad_peers, zScore: zScore, derivedValues: derivedValuesModel)
        
        return esgData
        
    }
    
    private func fetchControversyData(res: [String: AnyObject]) -> ControversyData {
        let count = res["Controversy_count"] as! Int
        let peerControversy = (res["avgPeerControversy"] as? Double) ?? 0.0
        let controversies = res["relatedControversy"] as! [AnyObject]
        
        var data = [String]()
        
        for i in 0..<controversies.count {
            data.append(controversies[i] as! String)
        }
        
        return ControversyData.init(count: count, controversies: data, peerControversy: peerControversy)
    }
    
    private func showControversyDialog(model: ControversyModel){
        let dialog = ControversyDialogView.init(frame: dialogView.bounds)
        dialog.controversyModel = model
        dialog.setValues()
        
        dialogView.addSubview(dialog)
        dialogView.isHidden = false
        blurView.isHidden = false
    }
    
    @objc private func closeBtnTapped(){
        dialogView.isHidden = true
        blurView.isHidden = true
        for view in self.dialogView.subviews {
            view.removeFromSuperview()
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension InvestmentValuesNew: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return tableViewDataArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableViewDataArray[section].opened {
//            if section == 0 {
//                let data = tableViewDataArray[section].data as! [String]
//                return 1 + data.count
//            }else if section == 1 {
//                return 1
//            }
//            else if section > 2 && section < tableView.numberOfSections - 2 {
//                let data = tableViewDataArray[section].data as! EsgScore
//                return 1 + data.derivedValues.count
//            } else if section == tableView.numberOfSections - 1 {
//                return 1
//            }
//            else {
//                return 1
//            }
            if section == 0 {
                let data = tableViewDataArray[section].data as! [String]
                return 1 + data.count
            }else {
                return 1
            }
        }else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "UserInvestmentHeadingCell") as! InvestmentValuesNewHeadingCell
                
                cell.headingLabel.text = tableViewDataArray[indexPath.section].title
                cell.descLabel.text = tableViewDataArray[indexPath.section].desc
                
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "UserValuesCell") as! UserValuesCell
                
                let positiveVals = tableViewDataArray[indexPath.section].data as! [String]
                
                cell.image1.image = FintechUtils.sharedInstance.getInvestmentValuesIcons(investmentValue: positiveVals[indexPath.row - 1])
                cell.image2.image = UIImage(named: "care")
                cell.valuesLabel.text = positiveVals[indexPath.row - 1]
                
                return cell
            }
        }else if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ControversyCell") as! ControversyCell
            
            cell.controversyLabel.text = tableViewDataArray[indexPath.section].title
            cell.controversyScore.text = tableViewDataArray[indexPath.section].desc
            
            let data = tableViewDataArray[indexPath.section].data as? ControversyData
            if data == nil {
                cell.ellipses.isHidden = true
                cell.ellipses.heightConstaint?.constant = 0
            }else {
                cell.ellipses.isHidden = false
                cell.ellipses.heightConstaint?.constant = 20
            }
            
            return cell
        }
        else if indexPath.section == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "UserInvestmentMainHeadingCell") as! InvestmentValuesNewMainHeadingCell
            
            let data = tableViewDataArray[indexPath.section].data as! OverallNewEsgModel
            cell.headingLabel.text = tableViewDataArray[indexPath.section].title
            //cell.descLabel.text = tableViewDataArray[indexPath.section].desc
            //cell.scoreLabel.text = data.impact
            cell.scoreLabel.text = FintechUtils.sharedInstance.getImpactForPortfolio(data.zScore)
            //cell.parentView.backgroundColor = tableViewDataArray[indexPath.section].cardColor
            
            return cell
        } else if indexPath.section > 2 && indexPath.section < tableView.numberOfSections - 3 {
            let data = tableViewDataArray[indexPath.section].data as! NewEsgScoreModel
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "UserInvestmentSubHeadingCell") as! InvestmentValuesNewSubHeadingCell
                
                
                cell.headingLabel.text = tableViewDataArray[indexPath.section].title
                //cell.descLabel.text = tableViewDataArray[indexPath.section].desc
//                cell.scoreLabel.text = data.impact + "\n" + getLocalizedString(key: "better_than") + " " + data.badPeers + " " + getLocalizedString(key: "_peers")
                //cell.scoreLabel.text = data.impact
                cell.scoreLabel.text = FintechUtils.sharedInstance.getImpactForPortfolio(data.zScore)
                cell.headingLabel.textColor = tableViewDataArray[indexPath.section].cardColor
                return cell
                
            }else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "UserValuesCell2") as! UserValuesCell2
                
                let derivedValue = data.derivedValues[indexPath.row - 1]
                
                cell.headingLbl.text = derivedValue.valueName
                cell.iconImage.image = FintechUtils.sharedInstance.getInvestmentValuesIcons(investmentValue: derivedValue.valueName)
                
                if derivedValue.score > 0.0 {
                    cell.impactLbl.text = "Some impact"
                }else {
                    cell.impactLbl.text = "No impact"
                }
                
                return cell
            }
            
            
            
        }else if indexPath.section == tableView.numberOfSections - 3 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "EsgSubFactorCell") as! ControversyCell
            
            cell.controversyLabel.text = tableViewDataArray[indexPath.section].title
            
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ScoreImprovementCell") as! ScoreImprovementCell
            
            
            cell.headingCell.text = tableViewDataArray[indexPath.section].title
            cell.charitiesButton.setTitle(getLocalizedString(key: "invest_in_charities", comment: ""), for: .normal)
            
            return cell
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            if tableViewDataArray[indexPath.section].opened {
                tableViewDataArray[indexPath.section].opened = false
            }else {
                tableViewDataArray[indexPath.section].opened = true
            }
            
        }
        
        if indexPath.section == 1 {
            let data = tableViewDataArray[1].data as? ControversyData
            if data != nil {
                let model = ControversyModel.init(success: true, message: "Data found", data: data!)
                showControversyDialog(model: model)
            }
            
        }else if indexPath.section == tableView.numberOfSections - 2 {
            let storyboard = UIStoryboard(name: "InvestmentDetails", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ScoreImprovementVC") as! ScoreImprovementVC
            self.present(vc, animated: true, completion: nil)
        }
        else if indexPath.section == tableView.numberOfSections - 1 {
            let storyboard = UIStoryboard(name: "Charity", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "listOfCharitiesVC") as! listOfCharitiesVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        if indexPath.section == tableView.numberOfSections - 3 {
            let storyboard = UIStoryboard(name: "Portfolio", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "EsgSubFactorVC") as! EsgSubFactorVC
            vc.envTagsData = parseSubFactorsData(esgType: PortfolioEsgType.ENV)
            vc.socTagsData = parseSubFactorsData(esgType: PortfolioEsgType.SOC)
            vc.govTagsData = parseSubFactorsData(esgType: PortfolioEsgType.GOV)
            vc.tagsLabelColor = .black
            vc.tagsBackgroundColor = .lightGray
            vc.companyName = getLocalizedString(key: "esg_subfactors")
            self.navigationController?.pushViewController(vc, animated: true)
            //self.navigationController?.present(vc, animated: true, completion: nil)
        }
        
        let sections = IndexSet.init(integer: indexPath.section)
        tableView.reloadSections(sections, with: .none)
    }
    
    private func parseSubFactorsData(esgType: String) -> [SwiftTagData]{
        var tagsData = [SwiftTagData]()
        
        if esgType == PortfolioEsgType.ENV {
            for value in env.derivedValues {
                tagsData.append(SwiftTagData.init(label: value.valueName, tagImage: FintechUtils.sharedInstance.getInvestmentValuesIcons(investmentValue: value.valueName), value: value.score))
            }
        }else if esgType == PortfolioEsgType.SOC {
            for value in soc.derivedValues {
                tagsData.append(SwiftTagData.init(label: value.valueName, tagImage: FintechUtils.sharedInstance.getInvestmentValuesIcons(investmentValue: value.valueName), value: value.score))
            }
        }else {
            for value in gov.derivedValues {
                tagsData.append(SwiftTagData.init(label: value.valueName, tagImage: FintechUtils.sharedInstance.getInvestmentValuesIcons(investmentValue: value.valueName), value: value.score))
            }
        }
        
        
        
        return tagsData
    }
}
