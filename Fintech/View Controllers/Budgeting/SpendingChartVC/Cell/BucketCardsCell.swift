//
//  BucketCardsCell.swift
//  Fintech
//
//  Created by broadstone on 13/07/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import UIKit

class BucketCardsCell: UITableViewCell {
    @IBOutlet weak var needsLabel: UILabel!
    @IBOutlet weak var wantsLabel: UILabel!
    @IBOutlet weak var havesLabel: UILabel!
    @IBOutlet weak var needsAmount: UILabel!
    @IBOutlet weak var wantsAmount: UILabel!
    @IBOutlet weak var havesAmount: UILabel!
    @IBOutlet weak var bucketCardsStack: UIStackView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
