//
//  SpendingTrackDetailVC.swift
//  vesgo
//
//  Created by Apple on 14/01/2021.
//  Copyright © 2021 Apple. All rights reserved.
//

import UIKit

class SpendingTrackDetailVC: BudgetingParent {
    @IBOutlet weak var tableView: UITableView!
    
    var headingValues = ["Mortgage/Rent", "Online services", "Education", "Healthcare", "Dining out", "Groceries", "Insurance", "Utilities", "Misc."]
    
    var categoriesDict =  OrderedDictionary<String,String>()
    var categoriesAmountDict = OrderedDictionary<String, Double>()
    
    var merchantNameDict = OrderedDictionary<String, String>()
    var amountSpentDict = OrderedDictionary<String, Double>()
    var spendingDateDict = OrderedDictionary<String, Int64>()
    
    //var amountPlannedByCategories: UsersuserBudgetingDataModel!
    
    var amountTextField: UITextField!
    var dateTextField: UITextField!
    var merchantNameTextField: UITextField!
    var datePicker = UIDatePicker()
    
    var epochTimeForSpendingDateField = Int64((Date().timeIntervalSince1970*1000).rounded())
    
    var manualTrackingData: ManualTrackingData!
    
    let needsMerchantNameTag = 1000
    let needsDateTag = 500
    let needsAmountTag = 100
    
    let wantsMerchantNameTag = 2000
    let wantsDateTag = 600
    let wantsAmountTag = 200
    
    let havesMerchantNameTag = 3000
    let havesDateTag = 700
    let havesAmountTag = 300
    
    
    
    var userBudgetingData: BudgetingPlanData!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getBudgetingCategoriesAPI()
        
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(title: "Add account", style: .done, target: self, action: #selector(addCard))
        setNavBarTitile(title: "Spending details", topItem: "Spending details")
    }
    
    @objc private func addCard(){
        getPlaidLinkToken()
    }
    
    
    @IBAction func donePressed(_ sender: Any) {
        submitManualTrackingData()
        
        setAnalyticsAction(ScreenName: "SpendingTrackDetails", method: "ManualTrackingDataSubmitted")
        
    }
    
    // MARK:- Api to get user spending plan
    func  getBudgetingCategoriesAPI(){
        self.view.activityStartAnimating()
        let url = ApiUrl.GET_BUDGETING_PLAN
        let namespace = SPManager.sharedInstance.getCurrentUserNamespace()
        let params = ["namespace": namespace] as [String: AnyObject]
        
        APIManager.sharedInstance.postRequest(serviceName: url, sendData: params, success: {
            (response) in
            
            let success = response["success"] as? Bool ?? false
            if success {
                self.userBudgetingData = self.fetchUserBudgetingPlan(data: response["data"] as! [String: AnyObject])
                if self.userBudgetingData != nil {
                    self.populateInitialDataForManualTracking()
                }
            }else {
                print("No data found in Manual tracking")
            }
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
        }, failure: {
            (err) in
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
            if APIManager.sharedInstance.error400 {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
            }
            if(APIManager.sharedInstance.error401)
            {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
            }else{
                self.showAlert(ConstantStrings.Error_msg_generic)
            }
        })
        
        
    }
    
    private func fetchUserBudgetingPlan(data: [String: AnyObject]) -> BudgetingPlanData{
        let startDate = data["start_date"] as! Int64
        let endDate = data["end_date"] as! Int64
        let amountDetails = data["amount_details"] as! [String: AnyObject]
        
        let totalAmount = amountDetails["total_amount"] as! Double
        let needsAmount = amountDetails["needs_bucket_amount"] as! Double
        let wantsAmount = amountDetails["wants_bucket_amount"] as! Double
        let havesAmount = amountDetails["haves_bucket_amount"] as! Double
        let needsPercent = amountDetails["needs_bucket_percent"] as! Double
        let wantsPercent = amountDetails["needs_bucket_percent"] as! Double
        let havesPercent = amountDetails["needs_bucket_percent"] as! Double
        
        let planDivision = data["plan_division"] as! [String: AnyObject]
        let needsBucket = planDivision["needs_bucket"] as! [AnyObject]
        let wantsBucket = planDivision["wants_bucket"] as! [AnyObject]
        let havesBucket = planDivision["haves_bucket"] as! [AnyObject]
        
        var needsBucketArr = [BucketCategoryData]()
        var wantsBcuketArr = [BucketCategoryData]()
        var havesBucketArr = [BucketCategoryData]()
        
        for i in 0..<needsBucket.count {
            let item = needsBucket[i] as! [String: AnyObject]
            let categoryName = item["category_name"] as! String
            let amount = item["amount"] as! Double
            
            needsBucketArr.append(BucketCategoryData(index: i, amount: amount, label: categoryName))
        }
        
        for i in 0..<wantsBucket.count {
            let item = wantsBucket[i] as! [String: AnyObject]
            let categoryName = item["category_name"] as! String
            let amount = item["amount"] as! Double
            
            wantsBcuketArr.append(BucketCategoryData(index: i, amount: amount, label: categoryName))
        }
        
        for i in 0..<havesBucket.count {
            let item = havesBucket[i] as! [String: AnyObject]
            let categoryName = item["category_name"] as! String
            let amount = item["amount"] as! Double
            
            havesBucketArr.append(BucketCategoryData(index: i, amount: amount, label: categoryName))
        }
        
        let needsBucketData = BudgetingPlanBucketData(bucketAmount: needsAmount, bucketPercentage: needsPercent, bucketCategories: needsBucketArr)
        
        let wantsBucketData = BudgetingPlanBucketData(bucketAmount: wantsAmount, bucketPercentage: wantsPercent, bucketCategories: wantsBcuketArr)
        
        let havesBucketData = BudgetingPlanBucketData(bucketAmount: havesAmount, bucketPercentage: havesPercent, bucketCategories: havesBucketArr)
        
        return BudgetingPlanData(totalAmount: totalAmount, startDate: startDate, endDate: endDate, needsBucket: needsBucketData, wantsBucket: wantsBucketData, havesBucket: havesBucketData)
    }
    
    private func populateInitialDataForManualTracking() {
        var needsBucket = [ManualTrackingBucketData]()
        var wantsBucket = [ManualTrackingBucketData]()
        var havesBucket = [ManualTrackingBucketData]()
        
        for i in 0..<userBudgetingData.needsBucket.bucketCategories.count {
            let item = userBudgetingData.needsBucket.bucketCategories[i]
            
            needsBucket.append(ManualTrackingBucketData(index: item.index, categoryName: item.label, amount: 0.0, spendingDate: epochTimeForSpendingDateField, marchantName: ""))
        }
        
        for i in 0..<userBudgetingData.wantsBucket.bucketCategories.count {
            let item = userBudgetingData.wantsBucket.bucketCategories[i]
            
            wantsBucket.append(ManualTrackingBucketData(index: item.index, categoryName: item.label, amount: 0.0, spendingDate: epochTimeForSpendingDateField, marchantName: ""))
        }
        
        for i in 0..<userBudgetingData.havesBucket.bucketCategories.count {
            let item = userBudgetingData.havesBucket.bucketCategories[i]
            
            havesBucket.append(ManualTrackingBucketData(index: item.index, categoryName: item.label, amount: 0.0, spendingDate: epochTimeForSpendingDateField, marchantName: ""))
        }
        
        self.manualTrackingData = ManualTrackingData(needsBucketData: needsBucket, wantsBucketData: wantsBucket, havesBucketData: havesBucket)
        
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    //MARK:- API for Manual Tracking
    func submitManualTrackingData(){
        
        let params = getManualTransactionParams()
        if params.isEmpty {
            self.view.makeToast(getLocalizedString(key: "no_data_for_tracking"))
            return
        }
        
        self.view.activityStartAnimating()
        
        let url = ApiUrl.SUBMIT_MANUAL_TRANSACTION
        let namespace = SPManager.sharedInstance.getCurrentUserNamespace()
        
        APIManager.sharedInstance.postRequest(serviceName: url, sendData: params, timeout: NetworkConstants.INCREASED_TIMEOUT_INTERVAL, success: {
            (response) in
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
            
            if APIManager.sharedInstance.isSuccess {
                DispatchQueue.main.async {
                    self.view.makeToast("Submitted successfully")
                    self.PopVC()
                }
            }else {
                DispatchQueue.main.async {
                    self.view.makeToast("Unable to submit data, please try later.")
                }
            }
            
        }, failure: {
            (err) in
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
            if APIManager.sharedInstance.error400 {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
            }
            if(APIManager.sharedInstance.error401)
            {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
            }else{
                self.showAlert(ConstantStrings.Error_msg_generic)
            }
        })
    }
    
    private func getManualTransactionParams() -> [String: AnyObject] {
        let namespace = SPManager.sharedInstance.getCurrentUserNamespace()
        var needsBucket = [AnyObject]()
        var wantsBucket = [AnyObject]()
        var havesBucket = [AnyObject]()
        
        for item in manualTrackingData.needsBucketData {
            if item.amount > 0.0 {

                let needsItem = ["category_name": item.categoryName,
                                 "date": item.spendingDate,
                                "amount": item.amount,
                                "merchant_name": item.marchantName] as [String: AnyObject]
                
                needsBucket.append(needsItem as AnyObject)
            }
        }
        
        for item in manualTrackingData.wantsBucketData {
            if item.amount > 0.0 {
                let wantsItem = ["category_name": item.categoryName,
                                 "date": item.spendingDate,
                                "amount": item.amount,
                                "merchant_name": item.marchantName] as [String: AnyObject]
                
                wantsBucket.append(wantsItem as AnyObject)
            }
            
        }
        
        for item in manualTrackingData.havesBucketData {
            if item.amount > 0.0 {
                let havesItem = ["category_name": item.categoryName,
                                 "date": item.spendingDate,
                                "amount": item.amount,
                                "merchant_name": item.marchantName] as [String: AnyObject]
                
                havesBucket.append(havesItem as AnyObject)
            }
        }
        
        if needsBucket.count == 0 && wantsBucket.count == 0 && havesBucket.count == 0 {
            return [:]
        }else {
            let manualTransactions = ["needs_bucket": needsBucket,
                                      "wants_bucket": wantsBucket,
                                      "haves_bucket": havesBucket] as [String: AnyObject]
            return ["namespace": namespace, "manual_transactions": manualTransactions] as [String: AnyObject]
        }
        
        
    }
    
    //Create Date Picker
    func createDatePicker(){
        let toolbar  = UIToolbar()
        toolbar.sizeToFit()
        
        let doneBtn = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(datePickerDonePressed))
        
        let cancelBtn = UIBarButtonItem(barButtonSystemItem: .cancel, target: nil, action: #selector(cancelPressed))
        
        let spacer = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        
        toolbar.setItems([cancelBtn, spacer, doneBtn], animated: true)
        dateTextField.inputAccessoryView = toolbar
        dateTextField.inputView = datePicker
        
        datePicker.datePickerMode = .date
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
        }
        
    }
    
    @objc func datePickerDonePressed(){
        epochTimeForSpendingDateField = Int64((datePicker.date.timeIntervalSince1970*1000).rounded())
        
        view.endEditing(true)
    }
    
    @objc func cancelPressed(){
        view.endEditing(true)
    }
    
}
extension SpendingTrackDetailVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if userBudgetingData == nil {
            return 0
        }
        return    1 + userBudgetingData.needsBucket.bucketCategories.count
                + 1 + userBudgetingData.wantsBucket.bucketCategories.count
                + 1 + userBudgetingData.havesBucket.bucketCategories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row < 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ExpenseTrackingHeadingCell") as! ExpenseTrackingHeadingCell

            cell.headingLabel.text = getLocalizedString(key: "_needs")
            cell.amountLabel.text = "$" + FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: self.userBudgetingData.needsBucket.bucketAmount)
            cell.parentView.backgroundColor = UIColor(named: "NeedsBucketColor")!
            cell.bucketIcon.image = FintechUtils.sharedInstance.getBucketIcon(bucketType: "needs")
            
            return cell
        }
        else if indexPath.row < 1 + userBudgetingData.needsBucket.bucketCategories.count {
            
            let index = indexPath.row - 1
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! AddCategorySpendingCell
            
            
            
            cell.amountSpentField.tag = needsAmountTag + index
            cell.amountSpentField.delegate = self
            
            cell.spentDateField.tag = needsDateTag + index
            cell.spentDateField.delegate = self
            
            cell.merchantNameField.tag = needsMerchantNameTag + index
            cell.merchantNameField.delegate = self
            
            cell.amountSpentField.text = String(manualTrackingData.needsBucketData[index].amount)
            cell.spentDateField.text = FintechUtils.sharedInstance.getDateInUSLocale(timestamp: manualTrackingData.needsBucketData[index].spendingDate/1000)
            cell.merchantNameField.text = manualTrackingData.needsBucketData[index].marchantName
            
            
            
            cell.categoryNameLbl.text = manualTrackingData.needsBucketData[index].categoryName
            cell.amountPlannedLabel.text = "$" + FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: userBudgetingData.needsBucket.bucketCategories[index].amount)
            cell.imgBgView.backgroundColor =  FintechUtils.sharedInstance.getColoredBackgroundForNeedsCategories(rowPosition: index)
            cell.imgView.image = FintechUtils.sharedInstance.getCaetgoryImageForBudgeting(value: manualTrackingData.needsBucketData[index].categoryName.lowercased())
            

            return cell
        }
        else if indexPath.row < 1 + userBudgetingData.needsBucket.bucketCategories.count + 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ExpenseTrackingHeadingCell") as! ExpenseTrackingHeadingCell

            cell.headingLabel.text = getLocalizedString(key: "_wants")
            cell.amountLabel.text = "$" + FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: self.userBudgetingData.wantsBucket.bucketAmount)
            cell.parentView.backgroundColor = UIColor(named: "WantsBucketColor")!
            cell.bucketIcon.image = FintechUtils.sharedInstance.getBucketIcon(bucketType: "wants")
            return cell
        }
        else if indexPath.row < 1 + userBudgetingData.needsBucket.bucketCategories.count + 1 + userBudgetingData.wantsBucket.bucketCategories.count {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! AddCategorySpendingCell
            
            let index = indexPath.row - 1 - userBudgetingData.needsBucket.bucketCategories.count - 1
            
            cell.amountSpentField.tag = wantsAmountTag + index
            cell.amountSpentField.delegate = self
            
            cell.spentDateField.tag = wantsDateTag + index
            cell.spentDateField.delegate = self
            
            cell.merchantNameField.tag = wantsMerchantNameTag + index
            cell.merchantNameField.delegate = self
            
            cell.amountSpentField.text = String(manualTrackingData.wantsBucketData[index].amount)
            cell.spentDateField.text = FintechUtils.sharedInstance.getDateInUSLocale(timestamp: manualTrackingData.wantsBucketData[index].spendingDate/1000)
            cell.merchantNameField.text = manualTrackingData.wantsBucketData[index].marchantName
            
            
            
            cell.categoryNameLbl.text = manualTrackingData.wantsBucketData[index].categoryName
            cell.amountPlannedLabel.text = "$" + FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: userBudgetingData.wantsBucket.bucketCategories[index].amount)
            cell.imgBgView.backgroundColor = FintechUtils.sharedInstance.getColoredBackgroundForWantsCategories(rowPosition: index)
            cell.imgView.image = FintechUtils.sharedInstance.getCaetgoryImageForBudgeting(value: manualTrackingData.wantsBucketData[index].categoryName.lowercased())

            return cell
        }
        else if indexPath.row < 1 + userBudgetingData.needsBucket.bucketCategories.count + 1 + userBudgetingData.wantsBucket.bucketCategories.count + 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ExpenseTrackingHeadingCell") as! ExpenseTrackingHeadingCell

            cell.headingLabel.text = getLocalizedString(key: "_haves")
            cell.amountLabel.text = "$" + FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: self.userBudgetingData.havesBucket.bucketAmount)
            cell.parentView.backgroundColor = UIColor(named: "HavesBucketColor")!
            cell.bucketIcon.image = FintechUtils.sharedInstance.getBucketIcon(bucketType: "haves")
            
            return cell
        }
        else if indexPath.row < 1 + userBudgetingData.needsBucket.bucketCategories.count + 1 + userBudgetingData.wantsBucket.bucketCategories.count + 1 +  userBudgetingData.havesBucket.bucketCategories.count {
            
            let index = indexPath.row - 1 - userBudgetingData.needsBucket.bucketCategories.count - 1 - userBudgetingData.wantsBucket.bucketCategories.count - 1
            
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! AddCategorySpendingCell
            
            
            cell.amountSpentField.tag = havesAmountTag + index
            cell.amountSpentField.delegate = self
            
            cell.spentDateField.tag = havesDateTag + index
            cell.spentDateField.delegate = self
            
            cell.merchantNameField.tag = havesMerchantNameTag + index
            cell.merchantNameField.delegate = self
            
            cell.amountSpentField.text = String(manualTrackingData.havesBucketData[index].amount)
            cell.spentDateField.text = FintechUtils.sharedInstance.getDateInUSLocale(timestamp: manualTrackingData.havesBucketData[index].spendingDate/1000)
            cell.merchantNameField.text = manualTrackingData.havesBucketData[index].marchantName
            
            
            
            cell.categoryNameLbl.text = manualTrackingData.havesBucketData[index].categoryName
            cell.amountPlannedLabel.text = "$" + FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: userBudgetingData.havesBucket.bucketCategories[index].amount)
            cell.imgBgView.backgroundColor = FintechUtils.sharedInstance.getColoredBackgroundForHavesCategories(rowPosition: index)
            cell.imgView.image = FintechUtils.sharedInstance.getCaetgoryImageForBudgeting(value: manualTrackingData.havesBucketData[index].categoryName.lowercased())

            return cell
        }
        
        return UITableViewCell()
        
    }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 165
//    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        let tag = textField.tag
        
        if tag >= needsDateTag && tag < needsMerchantNameTag {
            dateTextField = textField
            createDatePicker()
        }
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        print(textField.text ?? "Empty")
        
//        let needsMerchantNameTag = 1000
//        let needsDateTag = 500
//        let needsAmountTag = 100
//
//        let wantsMerchantNameTag = 2000
//        let wantsDateTag = 600
//        let wantsAmountTag = 200
//
//        let havesMerchantNameTag = 3000
//        let havesDateTag = 700
//        let havesAmountTag = 300
        
        let tag = textField.tag
        if tag >= needsAmountTag && tag < wantsAmountTag {
            let index = tag - needsAmountTag
            self.manualTrackingData.needsBucketData[index].amount = FintechUtils.sharedInstance.parseDouble(strValue: textField.text ?? "0.0")
            
        }else if tag >= wantsAmountTag && tag < havesAmountTag {
            let index = tag - wantsAmountTag
            self.manualTrackingData.wantsBucketData[index].amount = FintechUtils.sharedInstance.parseDouble(strValue: textField.text ?? "0.0")
        }else if tag >= havesAmountTag && tag < needsDateTag {
            let index = tag - havesAmountTag
            self.manualTrackingData.havesBucketData[index].amount = FintechUtils.sharedInstance.parseDouble(strValue: textField.text ?? "0.0")
        }
        else if tag >= needsDateTag && tag < wantsDateTag {
            let index = tag - needsDateTag
            self.manualTrackingData.needsBucketData[index].spendingDate = epochTimeForSpendingDateField
            
        }else if tag >= wantsDateTag && tag < havesDateTag {
            let index = tag - wantsDateTag
            self.manualTrackingData.wantsBucketData[index].spendingDate = epochTimeForSpendingDateField
            
        }else if tag >= havesDateTag && tag < needsMerchantNameTag {
            let index = tag - havesDateTag
            self.manualTrackingData.havesBucketData[index].spendingDate = epochTimeForSpendingDateField
            
        }else if tag >= needsMerchantNameTag && tag < wantsMerchantNameTag {
            let index = tag - needsMerchantNameTag
            self.manualTrackingData.needsBucketData[index].marchantName = textField.text ?? ""
            
        }else if tag >= wantsMerchantNameTag && tag < havesMerchantNameTag {
            let index = tag - wantsMerchantNameTag
            self.manualTrackingData.wantsBucketData[index].marchantName = textField.text ?? ""
        }else if tag >= havesMerchantNameTag {
            let index = tag - havesMerchantNameTag
            self.manualTrackingData.havesBucketData[index].marchantName = textField.text ?? ""
        }
        
        
        
        tableView.reloadData()

    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        if textField.keyboardType == .decimalPad {
            let regex = try! NSRegularExpression(pattern: "^(^[0-9]{0,7})*([.,][0-9]{0,2})?$", options: .caseInsensitive)

            if let newText = (textField.text as NSString?)?.replacingCharacters(in: range, with: string) {
                return regex.firstMatch(in: newText, options: [], range: NSRange(location: 0, length: newText.count)) != nil

            } else {
                return false
            }
        }else {
            return true
        }
    }
    
}
