//
//  StartChatWithAdvisorModel.swift
//  Fintech
//
//  Created by broadstone on 12/01/2022.
//  Copyright © 2022 Broadstone Technologies. All rights reserved.
//

import Foundation

struct StartChatWithAdvisorModel {
    let success: Bool
    let message: String
    let data: StartChatWithAdvisorData
}

struct StartChatWithAdvisorData {
    let chatUniqueId: String
}
