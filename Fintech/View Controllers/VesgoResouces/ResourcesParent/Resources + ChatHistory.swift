//
//  Resources + ChatHistory.swift
//  Fintech
//
//  Created by broadstone on 14/01/2022.
//  Copyright © 2022 Broadstone Technologies. All rights reserved.
//

import Foundation

protocol ChatHistoryApiDelegate {
    func chatHistoryFetched(successfully: Bool, data: [RecentChatLastMessage], chatId: String, advisorName: String)
}

extension ResourcesParent {
    func getSavedMessages(chatId: String, advisorName: String) {
        self.view.activityStartAnimating()
        let url = ApiUrl.GET_SAVED_MESSAGES
        let params = ["namespace": SPManager.sharedInstance.getCurrentUserNamespace(),
                      "chat_unique_id": chatId] as [String: AnyObject]
        
        APIManager.sharedInstance.postRequest(serviceName: url, sendData: params, success: { [self] (response) in
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
            let success = (response["status"] as? Bool) ?? false
            if success {
                let data = response["data"] as! [String: AnyObject]
                let messages = data["chat_history"] as! [AnyObject]
                
                let chatHistory = fetchPreviousMessage(data: messages)
                chatHistoryApiDelegate?.chatHistoryFetched(successfully: true, data: chatHistory, chatId: chatId, advisorName: advisorName)
            }else {
                chatHistoryApiDelegate?.chatHistoryFetched(successfully: true, data: [RecentChatLastMessage](), chatId: chatId, advisorName: advisorName)
            }
            
        }, failure: { (error) in
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
            
            DispatchQueue.main.async {
                self.view.makeToast("Unable to fetch previous messages")
                self.chatHistoryApiDelegate?.chatHistoryFetched(successfully: true, data: [RecentChatLastMessage](), chatId: chatId, advisorName: advisorName)
            }
        })
    }
    
    private func fetchPreviousMessage(data: [AnyObject]) -> [RecentChatLastMessage] {
        var messages = [RecentChatLastMessage]()
        for i in 0..<data.count {
            let lastMessage = data[i] as! [String: AnyObject]
            
            let senderNamespace = lastMessage["sender_namespace"] as! String
            let timestamp = lastMessage["timestamp"] as! Int64
            let message = lastMessage["message"] as! String
            
            let lastMessageData = RecentChatLastMessage.init(message: message, senderNamespace: senderNamespace, timestamp: timestamp)
            
            messages.append(lastMessageData)
        }
        
        return messages
    }
}


