//
//  MainPageLoginCell.swift
//  Fintech
//
//  Created by Apple on 09/06/2020.
//  Copyright © 2020 Broadstone Technologies. All rights reserved.
//

import UIKit
import Lottie

class MainPageLoginCell: UICollectionViewCell {
    
    @IBOutlet weak var Slider_img: UIImageView!
    
    @IBOutlet weak var sliderAnimation_view: AnimationView!
}
