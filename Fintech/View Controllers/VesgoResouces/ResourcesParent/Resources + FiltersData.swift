//
//  Resources + FiltersData.swift
//  Fintech
//
//  Created by broadstone on 21/01/2022.
//  Copyright © 2022 Broadstone Technologies. All rights reserved.
//

import Foundation

protocol FilterVCDelegte : AnyObject{
    func selection(option : [String], sortType: SortByAlphabets, availability: SortByAvailability, completeFiltersData: [FilterCategories])
}

enum Categories {
    case EXPERTISE, AVAILABILITY, NAME_SORTING
}

struct FilterCategories {
    var name: String
    var options: [FilterCategoryOptions]
    var isMultiSelection: Bool
    var selectedOptions: Any
    var category: Categories
}

struct FilterCategoryOptions {
    var name: String
    var key: Any
    var isSelected: Bool = false
}

enum SortByAvailability {
    case online, offline, notSelected, both
}
enum SortByAlphabets {
    case AtoZ, ZtoA, notSelected
}

extension ResourcesParent {
    func getExpertiseOptions() -> [FilterCategoryOptions] {
        let options: [FilterCategoryOptions] = [
            FilterCategoryOptions.init(name: "Bonds", key: "Bonds"),
            FilterCategoryOptions.init(name: "Stocks", key: "Stocks"),
            FilterCategoryOptions.init(name: "ETFs", key: "Etf"),
            FilterCategoryOptions.init(name: "ESG", key: "ESG"),
            FilterCategoryOptions.init(name: "Retirement planning", key: "Retirement Planning"),
            FilterCategoryOptions.init(name: "Crypto", key: "Crypto")
        ]
        
        return options
    }
    
    func getSortByNameOptions() -> [FilterCategoryOptions] {
        let options: [FilterCategoryOptions] = [
            FilterCategoryOptions.init(name: "Sort by name (A-Z)", key: SortByAlphabets.AtoZ),
            FilterCategoryOptions.init(name: "Sort by name (Z-A)", key: SortByAlphabets.ZtoA)
        ]
        return options
    }
    
    func getSortByAvailabilityOptions() -> [FilterCategoryOptions] {
        let options: [FilterCategoryOptions] = [
            FilterCategoryOptions.init(name: "Online", key: SortByAvailability.online),
            FilterCategoryOptions.init(name: "Offline", key: SortByAvailability.offline),
            FilterCategoryOptions.init(name: "Both", key: SortByAvailability.both)
        ]
        return options
    }
    
    func getFiltersData() -> [FilterCategories] {
        var filters = [FilterCategories]()
        filters.append(FilterCategories.init(name: "Sort by expertise", options: getExpertiseOptions(), isMultiSelection: true, selectedOptions: [String](), category: .EXPERTISE))
        filters.append(FilterCategories.init(name: "Sort by alphabet", options: getSortByNameOptions(), isMultiSelection: false, selectedOptions: SortByAlphabets.notSelected, category: .NAME_SORTING))
        filters.append(FilterCategories.init(name: "Sort by availability", options: getSortByAvailabilityOptions(), isMultiSelection: false, selectedOptions: SortByAvailability.notSelected, category: .AVAILABILITY))
        
        return filters
    }
}
