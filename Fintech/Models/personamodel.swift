//
//  personamodel.swift
//  Fintech
//
//  Created by Apple on 08/10/2020.
//  Copyright © 2020 Broadstone Technologies. All rights reserved.
//

// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let personaModel = try? newJSONDecoder().decode(PersonaModel.self, from: jsonData)

import Foundation

// MARK: - PersonaModel
struct PersonaModel: Codable {
    let messasge: Messasge3?
}

// MARK: - Messasge
struct Messasge3: Codable {
    let demographic, values, investment, risk: String?
}
