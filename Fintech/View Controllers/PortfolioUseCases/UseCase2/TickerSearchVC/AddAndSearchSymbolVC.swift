//
//  AddAndSearchSymbolVC.swift
//  Fintech
//
//  Created by broadstone on 06/08/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import UIKit
import Foundation
class AddAndSearchSymbolVC: PortfolioParent {

    @IBOutlet weak var searchDescLabel: UILabel!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var manualSearchTableView: UITableView!
    @IBOutlet weak var valueMappedTableView: UITableView!
    @IBOutlet weak var clearBtn: UIButton!
    
    @IBOutlet weak var nameSearchImg: UIImageView!
    @IBOutlet weak var symbolSearchImg: UIImageView!
    @IBOutlet weak var trendingTickersView: UIView!
    @IBOutlet weak var valueMappedView: UIView!
    @IBOutlet weak var segmentControl: UISegmentedControl!
    
    @IBOutlet weak var leaderLabel: UILabel!
    @IBOutlet weak var averageLabel: UILabel!
    @IBOutlet weak var laggardLabel: UILabel!
    
    @IBOutlet weak var envLabel: UILabel!
    @IBOutlet weak var socLabel: UILabel!
    @IBOutlet weak var govLabel: UILabel!
    
    var isSearchByNameSelected = true
    
    var countLabel: UILabel!
    
    var trendingTickersModel: PortfolioTickersModel!
    var tickersModel: PortfolioTickersModel!
    
    var tickersInCart = [PortfolioTickersData]()
    
    var isCallingTrendingAPI = true
    
    var cacheData = [ValueMappedTickersCacheData]()
    var valueMappedTickersModel: ValueMappedTickersModel!
    var selectedValue: TickerValueType = .Env
    var selectedBucket: BucketType = .leader
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        segmentControl.setTitle(getLocalizedString(key: "trending_assets"), forSegmentAt: 0)
        segmentControl.setTitle(getLocalizedString(key: "value_aligned"), forSegmentAt: 1)
        let titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        segmentControl.setTitleTextAttributes(titleTextAttributes, for: .selected)
        
        valueMappedView.isHidden = true
        
        searchBar.placeholder = getLocalizedString(key: "search_by_name")

        clearBtn.isHidden = true
        
        manualSearchTableView.delegate = self
        manualSearchTableView.dataSource = self
        
        valueMappedTableView.delegate = self
        valueMappedTableView.dataSource = self
        
        searchBar.delegate = self
        searchBar.searchTextField.delegate = self
        self.valueMappedDelegate = self
        
        getTickersFromAPI(url: ApiUrl.PORTFOLIO_TRENDING_TICKERS, params: getParamsForTrending())
        
        createNextButton(title: getLocalizedString(key: "_next"))
        countLabel.text = String(tickersInCart.count)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        setNavBarTitile(title: "Back", topItem: getLocalizedString(key: "add_assets"))
    }
    
    func createNextButton(title: String){
        //let label = UILabel(frame: CGRect(x: 55, y: 0, width: 20, height: 16))
        let label = UILabel(frame: CGRect(x: 60, y: 7, width: 30, height: 20))
        label.layer.borderColor = UIColor.clear.cgColor
        label.layer.borderWidth = 2
        label.layer.cornerRadius = label.bounds.size.height / 2
        //label.layer.cornerRadius = 5
        label.textAlignment = .center
        label.layer.masksToBounds = true
        label.font = UIFont(name: "HelveticaNeue-Light", size: 13)
        //label.textColor = UIColor(named: "AppMainColor")
        label.textColor = .black
        label.backgroundColor = .white
        label.text = String(tickersInCart.count)
        
        self.countLabel = label

          // button
        let rightButton = UIButton(frame: CGRect(x: 0, y: 0, width: 80, height: 16))
        rightButton.setTitle(title, for: .normal)
        rightButton.addTarget(self, action: #selector(viewAssetsPressed(_:)), for: .touchUpInside)
        rightButton.addSubview(countLabel)
        
        let rightBarButtomItem = UIBarButtonItem(customView: rightButton)
        self.navigationItem.rightBarButtonItem = rightBarButtomItem
    }
    
    @IBAction func viewAssetsPressed(_ sender: UIButton) {
        if tickersInCart.count == 0 {
            self.view.makeToast(getLocalizedString(key: "no_symbol_in_bucket"))
            return
        }
        pushAssetAllocationVC(tickersInCart: tickersInCart, useCaseType: .UseCase2, assetUpdateDalegate: self)
    }
    
    
    @IBAction func clearSearchPressed(_ sender: UIButton){
        headingLabel.text = getLocalizedString(key: "trending_tickers_for_u")
        clearBtn.isHidden = true
        self.searchBar.text = ""
        self.tickersModel = trendingTickersModel
        manualSearchTableView.reloadData()
        segmentControl.setTitle(getLocalizedString(key: "trending_assets"), forSegmentAt: 0)
    }
    
    @IBAction func searchByNamePressed(_ sender: Any) {
        isSearchByNameSelected = true
        nameSearchImg.image = UIImage(named: "radio_on_button")!
        symbolSearchImg.image = UIImage(named: "radio_off_button")!
        
        searchBar.placeholder = getLocalizedString(key: "search_by_name")
        
        setAnalyticsAction(ScreenName: "AddAndSearchSymbolVC", method: "SearchByName")
        
    }
    
    @IBAction func searchBySymbolPressed(_ sender: UIButton) {
        isSearchByNameSelected = false
        
        symbolSearchImg.image = UIImage(named: "radio_on_button")!
        nameSearchImg.image = UIImage(named: "radio_off_button")!
        
        searchBar.placeholder = getLocalizedString(key: "search_by_symbol")
        
        setAnalyticsAction(ScreenName: "AddAndSearchSymbolVC", method: "SearchBySymbol")
        
    }
    
    @IBAction func segmentIndexChanged(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
            case 0:
            trendingTickersView.isHidden = false
            valueMappedView.isHidden = true
                break
            case 1:
                if cacheData.count == 0 {
                    envBtnFunctionality()
                }
                trendingTickersView.isHidden = true
                valueMappedView.isHidden = false
                break
            default:
                break
        }
    }
    
    private func getTickersFromAPI(url: String, params: [String: AnyObject]){
        self.view.activityStartAnimating()
        print(params)
        
        APIManager.sharedInstance.postRequest(serviceName: url, sendData: params, success: { (response) in
            print(response)
            
            if APIManager.sharedInstance.isSuccess {
                DispatchQueue.main.async {
                    self.tickersModel = self.fetchTickersResponse(res: response)
                    if self.isCallingTrendingAPI {
                        self.trendingTickersModel = self.tickersModel
                        self.isCallingTrendingAPI = false
                        
                    }else {
                        self.clearBtn.isHidden = false
                        self.headingLabel.text = getLocalizedString(key: "search_results")
                        self.segmentControl.setTitle(getLocalizedString(key: "search_results"), forSegmentAt: 0)
                    }
                    self.manualSearchTableView.reloadData()
                }
            }else{
                
                DispatchQueue.main.async { [self] in
                    if response["message"] != nil {
                        self.showAlert(response["message"] as! String)
                    } else {
                        self.showAlert(ConstantStrings.NO_RESULT_FOUND_ALERT_TEXT)
                    }
                }
            }
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
        }, failure: { (data) in
            print(data)
            DispatchQueue.main.async { [self] in
                view.activityStopAnimating()
            }
            if APIManager.sharedInstance.error400 {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
            }
            if(APIManager.sharedInstance.error401)
            {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
            }else{
                self.showAlert(ConstantStrings.Error_msg_generic)
            }
        })
    }
    
    private func getParamsForTrending() -> [String: AnyObject] {
        return ["namespace": SPManager.sharedInstance.getCurrentUserNamespace()] as [String: AnyObject]
    }
    
    private func getParamsForSearch(searchQuery: String) -> [String: AnyObject] {
        
        let data = ["search_query": searchQuery.uppercased(),
                    "is_exact_match": !isSearchByNameSelected] as [String: AnyObject]
        
        let params = ["namespace": SPManager.sharedInstance.getCurrentUserNamespace(),
                      "data": data] as [String: AnyObject]
        
        return params
    }
    
    private func fetchTickersResponse(res: [String: AnyObject]) -> PortfolioTickersModel {
        let success = res["success"] as! Bool
        let message = res["message"] as! String
        let data = res["data"] as! [AnyObject]
        
        var symbols = [PortfolioTickersData]()
        for i in 0..<data.count {
            let asset = data[i] as! [String: String]
            let symbol = asset["symbol"]!
            let company = asset["company_name"]!
            
            symbols.append(PortfolioTickersData.init(symbol: symbol, CompanyName: company))
        }
        
        return PortfolioTickersModel.init(success: success, message: message, data: symbols)
    }
    
}
extension AddAndSearchSymbolVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == manualSearchTableView {
            if tickersModel == nil {
                return 0
            }
            return tickersModel.data.count
        }else {
            if valueMappedTickersModel == nil {
                return 0
            }
            return valueMappedTickersModel.data.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == manualSearchTableView {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SymbolCell") as! SymbolCell
            
            cell.symbolLabel.text = tickersModel.data[indexPath.row].symbol
            cell.companyLabel.text = tickersModel.data[indexPath.row].CompanyName
            
            if isTickerAlreadyInBucket(symbol: tickersModel.data[indexPath.row].symbol) {
                cell.parentView.backgroundColor = .systemGreen
                cell.companyLabel.textColor = .white
                
            }else {
                cell.parentView.backgroundColor = .white
                cell.companyLabel.textColor = .black
                
            }
            
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SymbolCell") as! SymbolCell
            let data = valueMappedTickersModel.data[indexPath.row]
            cell.companyLabel.text = data.CompanyName
            cell.symbolLabel.text = data.symbol
            
            if isTickerAlreadyInBucket(symbol: data.symbol) {
                cell.parentView.backgroundColor = .systemGreen
                cell.companyLabel.textColor = .white
                //cell.symbolLabel.textColor = .white
            }else {
                cell.parentView.backgroundColor = .white
                cell.companyLabel.textColor = .black
                //cell.symbolLabel.textColor = .black
            }
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == manualSearchTableView {
            
            if isTickerAlreadyInBucket(symbol: tickersModel.data[indexPath.row].symbol) {
                //self.view.makeToast("Ticker already added!")
                tickersInCart.removeAll(where: {
                    $0.symbol == tickersModel.data[indexPath.row].symbol
                })
            }else {
                tickersInCart.append(tickersModel.data[indexPath.row])
                
            }
            countLabel.text =  String(tickersInCart.count)
            tableView.reloadData()
            
        }else {
            let data = valueMappedTickersModel.data[indexPath.row]
            
            if isTickerAlreadyInBucket(symbol: data.symbol) {
                tickersInCart.removeAll(where: {
                    $0.symbol == data.symbol
                })
            }else {
                tickersInCart.append(data)
            }
            countLabel.text = String(tickersInCart.count)
            tableView.reloadData()
        }
        
        
    }
    
    func isTickerAlreadyInBucket(symbol: String) -> Bool {
        for ticker in tickersInCart {
            if ticker.symbol == symbol {
                return true
            }
        }
        
        return false
    }
    
}

extension AddAndSearchSymbolVC: UISearchBarDelegate {
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        DispatchQueue.main.async { [self] in
            if segmentControl.selectedSegmentIndex == 1 {
                let cacheModel = getCacheDataIfAvailable(valueName: selectedValue, bucketName: selectedBucket)
                self.valueMappedTickersModel = cacheModel!.model
                self.valueMappedTableView.reloadData()
            }
        }
        return true
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        if segmentControl.selectedSegmentIndex == 1 {
            let cacheModel = getCacheDataIfAvailable(valueName: selectedValue, bucketName: selectedBucket)
            self.valueMappedTickersModel = cacheModel!.model
            DispatchQueue.main.async {
                self.valueMappedTableView.reloadData()
            }
        }
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        let searchText = searchBar.text?.lowercased()
        
        if searchText!.isEmpty {
            
        }else {
            searchBar.endEditing(true)
            if segmentControl.selectedSegmentIndex == 0 {
                let params = getParamsForSearch(searchQuery: searchText!)
                self.getTickersFromAPI(url: ApiUrl.PORTFOLIO_SEARCH_TICKERS, params: params)
            }else {
                if isSearchByNameSelected {
                    valueMappedTickersModel.data.removeAll(where: {
                        !$0.CompanyName.lowercased().contains(searchText!)
                    })
                    
                    
                    valueMappedTableView.reloadData()
                }else {
                    valueMappedTickersModel.data.removeAll(where: {
                        $0.symbol.lowercased() != searchText!.lowercased()
                    })
                    
                    valueMappedTableView.reloadData()
                }
            }
        }
        
    }
}

extension AddAndSearchSymbolVC: AssetsUpdateDelegate {
    func onCartUpdate(tickersInCart: [PortfolioTickersData], proceedToAlloc: Bool) {
        self.tickersInCart = tickersInCart
        countLabel.text = String(tickersInCart.count)
        DispatchQueue.main.async {
            self.manualSearchTableView.reloadData()
        }
    }
    
    func cartDeleted() {
        tickersInCart.removeAll()
        countLabel.text = String(tickersInCart.count)
        DispatchQueue.main.async {
            self.manualSearchTableView.reloadData()
        }
    }
    
    
}
extension UIView{
    func animationZoom(scaleX: CGFloat, y: CGFloat) {
        self.transform = CGAffineTransform(scaleX: scaleX, y: y)
    }
    
    func animationRoted(angle : CGFloat) {
        self.transform = self.transform.rotated(by: angle)
    }
}
