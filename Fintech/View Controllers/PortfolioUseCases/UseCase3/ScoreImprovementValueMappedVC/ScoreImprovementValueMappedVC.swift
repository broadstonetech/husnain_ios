//
//  ScoreImprovementValueMappedVC.swift
//  Fintech
//
//  Created by broadstone on 14/03/2022.
//  Copyright © 2022 Broadstone Technologies. All rights reserved.
//

import UIKit

class ScoreImprovementValueMappedVC: PortfolioParent {
    
    @IBOutlet weak var tableView: UITableView!
    
    var linksData = [ScoreImprovementLinksStruct]()
    var templateData = [PortfolioValuesTemplate]()
    

    var delegate: ScoreImprovementTemplateDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        linksData = getScoreImprovementLinksData()
        templateData = getPortfolioTemplates()
        
        tableView.delegate = self
        tableView.dataSource = self
    }

}
extension ScoreImprovementValueMappedVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1 + templateData.count
        }else {
            return linksData.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ScoreImprovementVcHeadingCell") as! ScoreImprovementVcHeadingCell
                cell.headingLabel.text = getLocalizedString(key: "choose_template_to_build_portfolio")
                return cell
            }else {
                let index = indexPath.row - 1
                let cell = tableView.dequeueReusableCell(withIdentifier: "TemplateCell") as! TemplateCell
                cell.headingLabel.text = templateData[index].localizedName
                cell.templateImage.image = templateData[index].templateImage
                return cell
            }
        }else {
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ScoreImprovementVcHeadingCell") as! ScoreImprovementVcHeadingCell
                cell.headingLabel.text = linksData[indexPath.row].desc
                
                return cell
            }else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ScoreImprovementLinkCell") as! ScoreImprovementLinkCell
                cell.descLabel.text = linksData[indexPath.row].desc
                cell.linkLabel.text = linksData[indexPath.row].link
                
                return cell
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 0 {
            if indexPath.row > 0 {
                let index = indexPath.row - 1
                let data = templateData[index]
                self.dismiss(animated: true, completion: {
                    self.delegate?.scoreImrpovementTemplateSelected(data)
                })
                
            }
        }else {
            if indexPath.row > 0 {
                FintechUtils.sharedInstance.openUrl(url: linksData[indexPath.row].link)
            }
        }
    }
}
