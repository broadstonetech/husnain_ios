//
//  QuizExpertiselevel_VC.swift
//  Fintech
//
//  Created by Apple on 27/07/2020.
//  Copyright © 2020 Broadstone Technologies. All rights reserved.
//
////#################################################################
//  In This view take a quiz all data save in db files
//
//################################################################
import UIKit
import Lottie
import RingGraph
import Firebase
import FirebaseAnalytics
class QuizExpertiselevel_VC: UIViewController {
    // MArk: - class varibales
    var mainQuestion = Int()
    var totalquestion = Int()
    var doneFlag = Bool()
    var innnerQuestion = String()
    var lastQuestion = Int()
    var user = ""
    var option = ["A","B","C","D"]
    var index = 0
    var delegate: greencolorDelegate?
    var selectedIndex = -1
    var isAnsSelected = false
    var selectedAns = ""
    var questionArray = [quizStructure]()
    var question = 0
    var correctAnswer = Int()
    var Ronganswercount = Int()
    
    // MArk: -  IB outlet!
    //views
    @IBOutlet weak var circle_view: UIView!
    @IBOutlet weak var questionLbl: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var back_view: UIView!
    
    @IBOutlet weak var heading_quizResult_lbl: UILabel!
    @IBOutlet weak var done_btn: UIButton!
    
    @IBOutlet weak var descripation_quizresultView_lbl: UILabel!
    @IBOutlet weak var correctAnswersImage:UIImageView!
    @IBOutlet weak var correct_answer_lbl: UILabel!
    @IBOutlet weak var question_No: UILabel!
    
    let db = Databasehandler()
    @IBOutlet weak var backview: UIView!
    let shapeLayer = CAShapeLayer()
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        correct_answer_lbl.isHidden = true
        backview.isHidden = true
        tableView.delegate = self
        tableView.dataSource = self
        questionArray = db.readQuiz()
        // Do any additional setup after loading the view.
        DispatchQueue.main.async {
            
            self.questionLbl.text = self.questionArray[self.question].question
            
            self.tableView.reloadData()
        }
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        localization()
        self.navigationController?.isNavigationBarHidden = true
        setAnalytics()
    }
    

    
    func setAnalyticsAction(ScreenName: String, method: String){
           
           let screenClass = classForCoder.description()
           Analytics.logEvent(AnalyticsEventScreenView, parameters: [AnalyticsParameterScreenName: ScreenName, AnalyticsParameterScreenClass: screenClass, "Method" : method])
       }
       
       func setAnalytics(){
           
           let screenClass = classForCoder.description()
           Analytics.logEvent(AnalyticsEventScreenView, parameters: [AnalyticsParameterScreenClass: screenClass])
           
       }
    
    func localization(){
        // setNavBarTitile(title:getLocalizedString(key: "Education", comment: ""), topItem: getLocalizedString(key: "Education", comment: ""))
        
        let headingquizresult = getLocalizedString(key: "Quiz result", comment: "")
        let descripation = getLocalizedString(key: "des", comment: "")
        heading_quizResult_lbl.text = headingquizresult
        descripation_quizresultView_lbl.text = descripation
        
        
        done_btn.setTitle(getLocalizedString(key: "Done", comment: ""), for: .normal)
        
        //navigationController?.navigationBar.barStyle = .blackOpaque
    }
    
    
    
    // MARK: - button Acction
    @IBAction func Done_pressed(_ sender: Any) {
        if correctAnswer <= 5{
            SPManager.sharedInstance.setUserEducationExpertiseLevel(EducationExpertiseValues.levelBigginer)
            
            setAnalyticsAction(ScreenName: "QuizExpertiseLevel", method: "Beginner")
            

        }else if correctAnswer <= 10{
             SPManager.sharedInstance.setUserEducationExpertiseLevel(EducationExpertiseValues.levelIntermediate)
            setAnalyticsAction(ScreenName: "QuizExpertiseLevel", method: "Intermediate")
        }else{
            SPManager.sharedInstance.setUserEducationExpertiseLevel(EducationExpertiseValues.levelExpert)
            setAnalyticsAction(ScreenName: "QuizExpertiseLevel", method: "Expert")
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func retakequiz_pressed(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "QuizExpertiselevel_VC") as! QuizExpertiselevel_VC
        navigationController?.pushViewController(vc, animated: true)
        setAnalyticsAction(ScreenName: "QuizExpertiseLevel", method: "RetakeQuiz")
    }
    
    @IBAction func dismissQuizView_pressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc private func handleTap(){
        let basicAnimation = CABasicAnimation(keyPath: "strokeEnd")
        basicAnimation.toValue = 1
        basicAnimation.duration = 2
        basicAnimation.isRemovedOnCompletion = false
        
        shapeLayer.add(basicAnimation, forKey: "urSoBasic")
        
    }
    private func changeImageBasedOnCorrectAnswers(){
        
        if correctAnswer <= 5{
            
            correctAnswersImage.image = UIImage(named: "investment (1)")
            
        }else if correctAnswer <= 10{
            
            correctAnswersImage.image = UIImage(named: "img_medium")
            
        }else{
            
            correctAnswersImage.image = UIImage(named: "img_expert")
            
        }
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
//    func ringchart(){
//        let center = circle_view.center
//        let circularpath = UIBezierPath(arcCenter: center, radius: CGFloat(correctAnswer), startAngle: -CGFloat.pi / 2, endAngle: 2 * CGFloat.pi, clockwise: true)
//        shapeLayer.path = circularpath.cgPath
//        shapeLayer.strokeColor  = UIColor.red.cgColor
//        shapeLayer.lineWidth = 10
//        shapeLayer.strokeEnd = 0
//        shapeLayer.fillColor = UIColor.clear.cgColor
//        backview.layer.addSublayer(shapeLayer)
//        backview.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTap)))
//    }
    
    
}
// MARK: - Tableview
extension QuizExpertiselevel_VC: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return questionArray[question].options.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "QuizCell") as! Quizexpertiselevel_Cell
        let item = questionArray[question]
        cell.option_lbl.text = item.options[indexPath.row]
         totalquestion = self.questionArray.count
        print(totalquestion)
         var questionno = Int()
        if isAnsSelected {
            if selectedIndex == indexPath.row{
                cell.BackView.backgroundColor = #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1)
                cell.option_lbl.textColor = .white
                questionno = questionno+1
               // print()
                
                if selectedAns == item.answer{
                    correctAnswer = correctAnswer+1
                }else{
                    Ronganswercount = Ronganswercount+1
                }
            }else{
                cell.BackView.backgroundColor = .clear
                cell.option_lbl.textColor = .clear
                
            }
        }else{
            cell.BackView.backgroundColor = .white
            cell.option_lbl.textColor = .black
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if isAnsSelected == false {
            selectedIndex =  indexPath.row
            let item = questionArray[question].options
            selectedAns = item![indexPath.row]
            isAnsSelected = true
            
            tableView.reloadData()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
             self.question += 1
                print(self.question)
              
                if self.question == self.questionArray.count{
                   // self.ringchart()
                    print(self.correctAnswer)
                    print(self.totalquestion)
                    self.backview.isHidden = false
                    self.correct_answer_lbl.isHidden = false
                    
                    self.correct_answer_lbl.text = "\(self.correctAnswer) /15"
                    self.changeImageBasedOnCorrectAnswers()
                    let clor1 :UIColor = #colorLiteral(red: 0.03921568627, green: 0.5607843137, blue: 0.03137254902, alpha: 1)
                    let clor2 :UIColor = #colorLiteral(red: 0.3098039216, green: 0.6156862745, blue: 0.8666666667, alpha: 1)
                    // let  medal = UserDefaults.standard.float(forKey: "done")
                    // let  totalprenstage = UserDefaults.standard.float(forKey: "countprenstage")
                    //print("totalprenstage = \(totalprenstage)")
                    //print("medal==\(medal)")
                    //print(totalquestion)

                    if #available(iOS 13.0, *) {
                        let graphMeters = [RingMeter(title: "Total percentage", value: self.correctAnswer, maxValue: self.totalquestion, colors: [clor2], symbolProvider: ImageProvider(image: UIImage(named: "")), backgroundColor : .systemGray5)]
                        
                        
                        if let graph = RingGraph(meters: graphMeters) {
                            
                            let viewFrame = self.circle_view.frame
                            let ringFrame = CGRect(x: 0, y: 0, width: viewFrame.width, height: viewFrame.height)
                            
                            let ringGraphView = RingGraphView(frame: ringFrame, graph: graph, preset: .MetersDescription)
                            ringGraphView.backgroundColor = .clear
                            self.circle_view.addSubview(ringGraphView)
                            self.circle_view.backgroundColor = .clear
                            
                        }
                    } else {
                    }

                    //MARK: - move next page
                }else{
                     self.question_No.text = getLocalizedString(key: "question_", comment: "") +
                        " \(self.question+1) " + getLocalizedString(key: "of_", comment: "") + " 15"
                    
                    //self.selectedIndex = -1
                    self.selectedAns = ""
                    self.isAnsSelected = false
                    self.questionLbl.text = self.questionArray[self.question].question
                    self.tableView.reloadData()
                    
                }
            }
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
