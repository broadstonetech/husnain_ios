//
//  InvestmentValuesNewMainHeadingCell.swift
//  Fintech
//
//  Created by broadstone on 07/03/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import UIKit

class InvestmentValuesNewMainHeadingCell: UITableViewCell {
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var portfolioControversyLbl: UILabel!
    @IBOutlet weak var infoBtn: UIImageView!
    
    @IBOutlet weak var parentView: UIView!
    @IBOutlet weak var portfolioControversy: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let string = getLocalizedString(key: "comparison_with_peers") + "\n" + getLocalizedString(key: "excellent_percentile") + "\n" + getLocalizedString(key: "good_percentile") + "\n" + getLocalizedString(key: "average_percentile") + "\n" + getLocalizedString(key: "below_average_percentile") + "\n" + getLocalizedString(key: "needs_improvement_percentile")
//        infoBtn.addToolTip(description: string)
        
        infoBtn.addToolTip(description: string, gesture: .singleTap, isEnabled: true)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
