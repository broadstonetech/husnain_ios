//
//  EducationDetailsCell.swift
//  Fintech
//
//  Created by MacBook-Mubashar on 27/09/2019.
//  Copyright © 2019 Broadstone Technologies. All rights reserved.
//

import UIKit

class EducationDetailsCell: UITableViewCell {

    @IBOutlet var mainHeading_lbl: UILabel!
    @IBOutlet var subHeading_lbl: UILabel!
    @IBOutlet var description_lbl: UILabel!
    @IBOutlet var disclaimer_btn: UIButton!
    @IBOutlet weak var info_image: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
