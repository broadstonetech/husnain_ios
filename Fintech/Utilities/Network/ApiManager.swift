//
//  ApiManager.swift
//  Fintech
//
//  Created by MacBook-Mubashar on 22/08/2019.
//  Copyright © 2019 Broadstone Technologies. All rights reserved.
//

import Foundation
import UIKit
import SystemConfiguration
class ArrayLists {
    static let sharedInstance = ArrayLists()
    //strategy building
    var currentQuestionList: [QuestionnaireModel] = [QuestionnaireModel]()
    var currentAnswersList: [AnswersModel] = [AnswersModel]()
    //Library
    var LibraryMainDataList: [LibraryDataModel] = [LibraryDataModel]()
    var LibraryDocsDataList: [LibraryDataModel] = [LibraryDataModel]()
    var LibraryMediaDataList: [LibraryDataModel] = [LibraryDataModel]()
    //notifications
    var notificationsDataList : [NotificationDataModel] = [NotificationDataModel]()
    var newsFeedDataList : [NewsFeedDataModel] = [NewsFeedDataModel]()
    //lederboard
    var LederboardPastmontDataList: [leaderboardDataModel] = [leaderboardDataModel]()
    var  LederboardPastThreeYearDataList: [leaderboardDataModel] = [leaderboardDataModel]()
    var LederboardPastYearDataList: [leaderboardDataModel] = [leaderboardDataModel]()
    
    // dashboard
    var demographicsdashboarddatalist : [DashboardDataModel] = [DashboardDataModel]()
    var riskdashboarddatalist = [Double]()
    var portfoliocompositiondashboarddatalist : [DashboardDataModel] = [DashboardDataModel]()
    var userinfodashboarddatalist : [DashboardDataModel] = [DashboardDataModel]()

     var positivevaluesdatalist = [String]()
    var negativevaluesdatalist = [String]()
    //portfolio app data
    var portfolioSummaryDataList : [PortfolioDataModel] = [PortfolioDataModel]()
    var portfolioDayChartDataList : [PortfolioDataModel] = [PortfolioDataModel]()
    var portfolioWeekChartDataList : [PortfolioDataModel] = [PortfolioDataModel]()
    var portfolioMonthChartDataList : [PortfolioDataModel] = [PortfolioDataModel]()
    var portfolioYearChartDataList : [PortfolioDataModel] = [PortfolioDataModel]()
    var portfolioallChartDataList : [PortfolioDataModel] = [PortfolioDataModel]()

    var futurePredictionsList: [FuturePredictionsModel] = [FuturePredictionsModel]()
    var futurePredictionsTextualList: [FuturePredictionsModel] = [FuturePredictionsModel]()
    //portfolio banchmark data
    var portfolioBenchmarkDayChartDataList : [PortfolioDataModel] = [PortfolioDataModel]()
    var portfolioBenchmarkWeekChartDataList : [PortfolioDataModel] = [PortfolioDataModel]()
    var portfolioBenchmarkMonthChartDataList : [PortfolioDataModel] = [PortfolioDataModel]()
    var portfolioBenchmarkYearChartDataList : [PortfolioDataModel] = [PortfolioDataModel]()
    var portfolioBenchmarkallChartDataList : [PortfolioDataModel] = [PortfolioDataModel]()

    var portfolioHistoryDataList : [PortfolioDataModel] = [PortfolioDataModel]()
    var portfolioDetailsDataList : [NotificationDataModel] = [NotificationDataModel]()
    //Educational
    var EducationalDataList: [LibraryDataModel] = [LibraryDataModel]()
    
    //Questionnaire
    var ListOfSubmittedAnswers: [QuestionnaireModel] = [QuestionnaireModel]()
    var SELECTED_ANSWERS_DICT = [[String: Any]]()
    
    //Select list of charities
    var SelectedListOfCharies: [Charity] = [Charity]()
    
    //Budgetings Transaction History Detail
    var TransactionHistoryDetailList = [TransactionDetails]()
    
}

struct NewsfeedTickerColors {
    static let GENERAL_TICKER_COLOR = #colorLiteral(red: 0, green: 0.3764705882, blue: 0.3921568627, alpha: 1)
    static let AAPl_Ticker_color = #colorLiteral(red: 0.9607843137, green: 0.4862745098, blue: 0, alpha: 1)
    static let AMD_Ticker_color = #colorLiteral(red: 0.1058823529, green: 0.368627451, blue: 0.1254901961, alpha: 1)
    static let MSFT_Ticker_color = #colorLiteral(red: 0.1058823529, green: 0.3254901961, blue: 0.8352941176, alpha: 1)
    static let FB_Ticker_color = #colorLiteral(red: 0.2588235294, green: 0.4039215686, blue: 0.6980392157, alpha: 1)
}
struct NewsfeedTicker {
    static let AAPL_TICKER_TEXT = "AAPL"
    static let GENERAL_TICKER_TEXT = "General"
     static let FB_TICKER_TEXT = "FB"
     static let AMD_TICKER_TEXT = "AMD"
    static let MSFT_TICKER_TEXT = "MSFT"
}


struct portfolioTickerColor {
    static let GENERAL_TICKER_COLOR = #colorLiteral(red: 0, green: 0.3764705882, blue: 0.3921568627, alpha: 1)
    static let AAPl_Ticker_color = #colorLiteral(red: 0.9607843137, green: 0.4862745098, blue: 0, alpha: 1)
    static let AMD_Ticker_color = #colorLiteral(red: 0.1058823529, green: 0.368627451, blue: 0.1254901961, alpha: 1)
    static let MSFT_Ticker_color = #colorLiteral(red: 0.1058823529, green: 0.3254901961, blue: 0.8352941176, alpha: 1)
    static let FB_Ticker_color = #colorLiteral(red: 0.2588235294, green: 0.4039215686, blue: 0.6980392157, alpha: 1)
}
struct portfolioTicker {
    static let GENERAL_TICKER_Text = "SHY"
    static let AAPl_Ticker_Text = "AGREX"
    static let FB_TICKER_TEXT = "FB"
    static let AMD_TICKER_TEXT = "AMD"
    static let MSFT_TICKER_TEXT = "MSFT"
}

struct NetworkConstants {
    static let REQUEST_TIMEOUT_INTERVAL = 30
    static let INCREASED_TIMEOUT_INTERVAL = 60
}

struct InvestmentStrategyVars {
    
    static var isFromAccountVC : Bool = false
}


struct ApiUrl {
    
    //base url
    //static let BASE_URL = "https://dev.vesgo.io/"
    static let BASE_URL = "https://api.vesgo.io/"

    //on-boarding
    static let LOGIN_URL = BASE_URL + "login"
    static let SIGNUP_USER_URL = BASE_URL + "signup"
    static let SET_NEW_PASSWORS_URL = BASE_URL + "reset/password"
    static let FORGOT_PASSWORD_URL = BASE_URL + "forgot/password"
    static let FORGOT_PASSWORD_VERIFY_TOKEN_URL = BASE_URL + "verify/token"
    static let CHANGE_PASSWORD_URL = BASE_URL + "reset/password"
    
    //STRATEGY BUILDING
    static let GET_QUESTION = BASE_URL + "get/question"
    static let SUBMIT_QUESTIONNAIRE = BASE_URL + "submit/questionnaire"
    //Library
    static let GET_LIBRARY_DATA_URL = BASE_URL + "get/lib/data"
    //Notifications
    static let GET_NOTIFICATIONS_DATA_URL = BASE_URL + "get/notifications"
    //New feed
    static let GET_NEWSFEED_DATA_URL = BASE_URL + "get/newsfeed"
    //Portfolio
    static let GET_USER_PORTFOLIO_SUMMARY_DATA_URL = BASE_URL + "get/portfolio/return"
    static let GET_PORTFOLIO_FUTURE_PREDICTIONS_URL = BASE_URL + "get/future/data"
    static let GET_GUEST_PORTFOLIO_SUMMARY_DATA_URL = BASE_URL + "/get/guest/portfolio"
    static let GET_PORTFOLIO_CHART_DATA = BASE_URL + "get/portfolio/chart/data"
    
    static let GET_Stocks_Allocation_URL = BASE_URL + "stocks/allocation"
    static let GET_BACK_TEST_URL = BASE_URL + "get/portfolio/backtest"
    

    //Educational Section
    static let GET_EDUCATIONAL_DATA_URL = BASE_URL + "get/education/data"
    //Account Settings
    static let GET_HELP_URL = BASE_URL + "help"
    static let GET_ACCOUNT_SETTINGS = BASE_URL + "get/account/settings"
    static let NoftificationCheck = BASE_URL + "change/notification/settings"
    static let Language_Select = BASE_URL + "/change/profile/settings"
    //Dashboard
    static let GET_REGD_USER_DASHBOARD = BASE_URL + "/user/dashboard"
    
    
    // Charites view
    
    static let GET_lIST_OF_CHARITY_URL = BASE_URL + "charities/pre_made/portfolio"
    static let GET_CHARITY_DONATIONS_HISTORY_URL = BASE_URL + "get/charity/donation/history"
    static let GET_CHARITABLE_PLAN_URL = BASE_URL + "get/donation/goal"
    static let SET_CHARITABLE_PLAN_URL = BASE_URL + "set/donation/goal"
    static let DONATION_METHOD_SOLID = BASE_URL + "charities/method/solid"
    static let DONATION_METHOD_INVEST = BASE_URL + "charities/method/invest"
    
    //Budgeting URLS
    static let GET_BUDGETING_CATEGORIES = BASE_URL + "get/budgeting/categories"
    static let GET_BUDGETING_PLAN = BASE_URL + "get/users/budgeting/plan"
    static let SUBMIT_BUDGETING_PLAN = BASE_URL + "set/budgetings/data"
    static let GET_BUDGETING_DASHBOARD_DATA = BASE_URL + "get/category/Transaction/History"
    static let GET_BUDGETING_TRANSACTION_HISTORY_DETAILS = BASE_URL + "get/category/Transaction/History/details"
    static let CHECK_BUDGETING_PLAN_EXISTENCE = BASE_URL + "check/plan/existance"
    static let SUBMIT_MANUAL_TRANSACTION = BASE_URL + "set/manual/transactions"
    static let GET_PLAID_LINK_TOKEN = BASE_URL + "api/create/link/token"
    static let SEND_PLAID_TOKEN = BASE_URL + "set/plaid/token"
    static let CLASSIFY_TRANSACTION_HISTORY = BASE_URL + "classify/Transaction/History"
    
    static let GET_RUCURRING_CHARGES_API = BASE_URL + "insights/recurring/transactions"
    static let GET_OVERSPENT_CATEGORIES_API = BASE_URL + "calculate/overspend/underspend"
    static let GET_VALUE_MAPPINGS_INSIGHTS_API = BASE_URL + "value/mapping/violations"
    static let GET_CATEGORY_SPENDINGS_FOR_CHART = BASE_URL + "category/filter/data"
    static let GET_FINANCIAL_HELP_DATA = BASE_URL + "financial/help/suggestions"
    static let GET_BUDGETING_OLD_DATA = BASE_URL + "calculate/old/plans/graph"
    static let GET_SPENDING_CORELATION = BASE_URL + "insights/correlation"
    static let GET_OFFERS_AND_DEALS = BASE_URL + "get/offers"
    
    //Dashboard URLs
    static let GET_DASHBOARD_PORTFOLIO = BASE_URL + "dashboard/portfolio"
    static let GET_DASHBOARD_HOLDINGS = BASE_URL + "dashboard/holdings"
    static let GET_DASHBOARD_CHARITY = BASE_URL + "dashboard/charity"
    static let GET_DASHBOARD_BUDGETING = BASE_URL + "dashboard/budgeting"
    
    //Investment Details URLs
    static let GET_USER_INVESTMENT_VALUES = BASE_URL + "dashboard/user/values"
    static let GET_PORTFOLIO_COMPOSITION = BASE_URL + "portfolio/composition"
    static let GET_TICKET_SEARCH_DATA = BASE_URL + "ticker/search/data"
    static let GET_RISK_TOLERANCE =  BASE_URL + "risk/factor"
    static let GET_USER_INVESTMENT_VALUES_NEW = BASE_URL + "new/dashboard/user/values"
    static let GET_TICKER_SEARCH_DATA_NEW = BASE_URL + "new/ticker/search/data"
    
    //Investment Questionnaire URLs
    static let GET_QUESTIONNAIRE_STATUS = BASE_URL + "get/questionnaire/status"
    static let GET_DEMOGRAPHICS_QUESTION_DATA = BASE_URL + "get/questionnaire/demographics/data"
    static let GET_INVESTMENT_QUESTION_DATA = BASE_URL + "get/questionnaire/investment/data"
    static let GET_RISK_QUESTION_DATA = BASE_URL + "get/questionnaire/risk/data"
    static let GET_VALUES_QUESTION_DATA = BASE_URL + "get/questionnaire/values/data"
    static let SET_DEMOGRAPHICS_QUESTION_DATA = BASE_URL + "set/questionnaire/demographics/data"
    static let SET_INVESTMENT_QUESTION_DATA = BASE_URL + "set/questionnaire/investment/data"
    static let SET_RISK_QUESTION_DATA = BASE_URL + "set/questionnaire/risk/data"
    static let SET_VALUES_QUESTION_DATA = BASE_URL + "set/questionnaire/values/data"
    static let SET_PORTFOLIO_TYPE_API = BASE_URL + "portfolio/type"
 
    
    //Leaderboard
    static let GET_Leaderboard_URL = BASE_URL + "leaderboard"
    static let GET_AGE_GROUP_LEADERBOARD_DATA = BASE_URL + "/my/age/group/leaderboard"
    static let COPY_PORTFOLIO_URL = BASE_URL + "copy/portfolio"
    
    //Future prediction
    static let GET_future_prediction_URL = BASE_URL + "future/prediction"
    
    //OnBoarding
    static let SIGN_OUT_URL = BASE_URL + "signout"
    
    //New Portfolio UseCases
    static let EXTERNAL_PORTFOLIO_IMPACT = BASE_URL + "calculate/external/portfolio"
    static let PORTFOLIO_TRENDING_TICKERS = BASE_URL + "get/portfolio/trending/tickers"
    static let PORTFOLIO_SEARCH_TICKERS = BASE_URL + "search/portfolio/tickers"
    static let GET_PORTFOLIO_LIST = BASE_URL + "get/portfolio/list"
    static let DELETE_PORTFOLIO = BASE_URL + "delete/portfolio"
    static let GET_PORTFOLIO_DETAILS = BASE_URL + "get/portfolio/details"
    static let UPDATE_PORTFOLIO_NAME = BASE_URL + "update/portfolio/name"
    static let UPDATE_PORTFOLIO_ASSET = BASE_URL + "update/portfolio/assets"
    static let INTERNAL_PORTFOLIO_IMPACT = BASE_URL + "internal/portfolio/impact"
    static let GET_VALUE_MAPPED_TICKERS = BASE_URL + "get/value/mapped/tickers"
    static let CALCULATE_VALUE_MAPPED_PORTFOLIO = BASE_URL + "calculate/value/mapped/portfolio"
    static let PORTFOLIO_BACKTEST_API = BASE_URL + "portfolio/backtest"
    static let GET_TICKET_CONTROVERSY = BASE_URL + "get/controversy/ticker"
    static let GET_ESG_SUBFACTORS = BASE_URL + "get/ticker/esg/subfactor"
    static let GET_VALUE_MAPPED_PORTFOLIO_IMPACT = BASE_URL + "values/portfolio/impact"
    static let GET_PRE_MADE_PORTFOLIOS_LIST = BASE_URL + "get/pre/made/portfolios"
    
    static let GET_USER_NOTIFICATIONS = BASE_URL + "get/user/notification"
    
    //Advisors
    static let Top_Advisor = BASE_URL + "get/top/advisors"
    static let Get_All_Advisor = BASE_URL + "get/all/advisors"
    static let GET_RECENT_CHATS = BASE_URL + "get/recent/chats"
    static let START_CHAT_WITH_ADVISOR = BASE_URL + "start/chat/with/advisor"
    static let GET_RECENT_CHAT_WITH_ADVISOR = BASE_URL + "get/recent/chats/with/advisor"
    static let SEND_ADVISOR_CHAT_MESSAGE = BASE_URL + "send/chat/msg"
    static let GET_SAVED_MESSAGES = BASE_URL + "get/msgs"
}

class APIManager: NSObject {
    static let sharedInstance = APIManager()
    var app_ID : String? = UIDevice.current.identifierForVendor?.uuidString
    var deviceToken : Data? = nil
    var error400 = false
    var error401 = false
    var error403 = false
    var isSuccess = false
    //MARK: - Configuratrions
    //MARK: - Webservice contents to be saved + Parameters Scheme +  Project Specific
    
    func deviceId() -> String {
        let uniqueId=UIDevice.current.identifierForVendor!.uuidString as String
        print("Your device identifire =>\(uniqueId)")
        return "00:19:d1:f6:d0:7c"
    }
//GET INTERNET CONNECTIVITY CHECK
    func isInternetAvailable() -> Bool
    {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        return (isReachable && !needsConnection)
    }
    //get 8 chars alphanumeric random string
    func getRandomKeyString() -> String {
        
        let letters : NSString = "abcdefghijklmnopqrstuvwxyz!@#$%^&*()ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let len = UInt32(letters.length)
        var randomString = ""
        for _ in 0 ..< 8 {
            let rand = arc4random_uniform(len)
            var nextChar = letters.character(at: Int(rand))
            randomString += NSString(characters: &nextChar, length: 1) as String
        }
        
        return randomString //mMhHg9kM
    }
    func getRandomUUIDString() -> String {
        let letters : NSString = "abcdefghijklmnopqrstuvwxyz!@#$%^&*()ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let len = UInt32(letters.length)
        var randomString = ""
        for _ in 0 ..< 32 {
            let rand = arc4random_uniform(len)
            var nextChar = letters.character(at: Int(rand))
            randomString += NSString(characters: &nextChar, length: 1) as String
        }
        return randomString //mMhHg9kM
    }
    // Return IP address of WiFi interface (en0) as a String, or `nil`
//    func getIP()-> String? {
//        var address: String?
//        var ifaddr: UnsafeMutablePointer<ifaddrs>? = nil
//        if getifaddrs(&ifaddr) == 0 {
//            var ptr = ifaddr
//            while ptr != nil {
//                defer { ptr = ptr?.pointee.ifa_next } // memory has been renamed to pointee in swift 3 so changed memory to pointee
//                let interface = ptr?.pointee
//                let addrFamily = interface?.ifa_addr.pointee.sa_family
//                if addrFamily == UInt8(AF_INET) || addrFamily == UInt8(AF_INET6) {
//                    if let name: String = String(cString: (interface?.ifa_name)!), name == "en0" {  // String.fromCString() is deprecated in Swift 3. So use the following code inorder to get the exact IP Address.
//                        var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
//                        getnameinfo(interface?.ifa_addr, socklen_t((interface?.ifa_addr.pointee.sa_len)!), &hostname, socklen_t(hostname.count), nil, socklen_t(0), NI_NUMERICHOST)
//                        address = String(cString: hostname)
//                    }
//                    
//                }
//            }
//            freeifaddrs(ifaddr)
//        }
//        if address == nil || address == "" {
//            address = ""
//        }
//        return address
//    }
    //MARK:- URL Schemes
    //MARK:- Webservice Calling Functions POST - GET
    func postRequest(serviceName: String,sendData : [String : AnyObject], timeout: Int = NetworkConstants.REQUEST_TIMEOUT_INTERVAL ,success:@escaping ( _ data : [String : AnyObject])->Void, failure:@escaping ( _ data:[String : AnyObject])->Void) {
        print("url == \(serviceName)")
        print("params == \(sendData)")
        //show activity indicator
        //create the url with NSURL
        let url = NSURL(string: serviceName)
        //create the session object
        let session = URLSession.shared
        //now create the NSMutableRequest object using the url object
        let request = NSMutableURLRequest(url: url! as URL)
        request.httpMethod = "POST" //set http method as POST
        request.timeoutInterval = TimeInterval(timeout)
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: sendData, options: .prettyPrinted) // pass dictionary to nsdata object and set it as request body
            
        } catch let error {
            print(error.localizedDescription)
        }
        
        //HTTP Headers
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        //create dataTask using the session object to send data to the server
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            if let httpStatus = response as? HTTPURLResponse {
                // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                if httpStatus.statusCode == 200 {
                    self.isSuccess = true
                }else{
                    self.isSuccess = false
                }
                
                if httpStatus.statusCode == 400 {
                    self.error400 = true
                }
                else if(httpStatus.statusCode == 401)
                {
                    self.error401 = true
                }
                else if(httpStatus.statusCode == 403)
                {
                    self.error403 = true
                }
                
            }
            //end copied
            
            guard error == nil else {
                print("this is error")
                var errors = [String : AnyObject]()
                failure(errors)
                self.showFailureAlert(ConstantStrings.Error_msg_generic)
                return
            }
            guard let data = data else {
                var errors = [String : AnyObject]()
                failure(errors)
                return
            }
            
            do {
                //create json object from data
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: AnyObject]
                    
                    //                if let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String : AnyObject]
                {
                    success(json)
                    //handle json...
                    //hide activity indicator
                }
                //hide activity indicator
            } catch let error {
                //hide activity indicator
                print(error.localizedDescription)
                var errors = [String : AnyObject]()
                errors["Error"] = "Server Error" as AnyObject
                failure(errors)
            }
            
        })
        
        task.resume()
        
    }
    
    //MARK: - PostRequest for chat module
    func postChatRequest(serviceName: String,sendData : [String : AnyObject],success:@escaping ( _ data : [String : AnyObject])->Void, failure:@escaping ( _ data:[String : AnyObject])->Void) {

        
        //        if(NetworkHelper.sharedInstance.isDmoatOnline){
        
//        CozyLoadingActivity.show("Please Wait..", disableUI: true)
        
        let serviceUrl =  serviceName
        print("url == \(serviceUrl)")
        let url = NSURL(string: serviceUrl)
        
        //create the session object
        let session = URLSession.shared
        
        //now create the NSMutableRequest object using the url object
        let request = NSMutableURLRequest(url: url! as URL)
        request.httpMethod = "POST" //set http method as POST
//        request.timeoutInterval = 20
        
        do {
            
            let data = try JSONSerialization.data(withJSONObject: sendData ,options: .prettyPrinted)
            
            let dataString = String(data: data, encoding: .utf8)!
            print("data string is",dataString)
            request.httpBody = try JSONSerialization.data(withJSONObject: sendData, options: .prettyPrinted)
            
            
            
        } catch let error {
            print(error.localizedDescription)
        }
        
        //HTTP Headers
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        //create dataTask using the session object to send data to the server
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            print(" response is \(response)")
            
            //copied
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
                // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                
                UserDefaults.standard.set(httpStatus.statusCode, forKey: "statusCode")
                UserDefaults.standard.synchronize()
                
                if(httpStatus.statusCode == 401)
                {
                    APIManager.sharedInstance.error401 = true
                }
                else if(httpStatus.statusCode == 403)
                {
                    APIManager.sharedInstance.error403 = true
                }
                
            }
            let httpStats = response as? HTTPURLResponse
            UserDefaults.standard.set(httpStats?.statusCode, forKey: "statusCode")
            UserDefaults.standard.synchronize()
            //end copied
            
            guard error == nil else {
                print("this is error")
                DispatchQueue.main.async {
//                    CozyLoadingActivity.hide()
//                    self.showFailureAlert(ConstantStrings.internet_off)
                }
                return
            }
            guard let data = data else {
                return
            }
            
            do {
                //create json object from data
                
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: AnyObject] {
                    print("json is \(json)")
                    
                    success(json)
                    //handle json...
//                    CozyLoadingActivity.hide()
                }
                
//                CozyLoadingActivity.hide(true, animated: false)
            } catch let error {
//                CozyLoadingActivity.hide()
                print(error.localizedDescription)
                var errors = [String : AnyObject]()
                errors["Error"] = "Server Error" as AnyObject
                failure(errors)
            }
            
        })
        
        task.resume()
        
    }
    
    
    func getRequest(serviceName: String,sendData : [String : AnyObject],success:@escaping ( _ data : [String : AnyObject])->Void, failure:@escaping ( _ data:[String : AnyObject])->Void) {
        print("url == \(serviceName)")
        print("params == \(sendData)")
        
        
        //show activity indicator
        //        EZLoadingActivity.showWithDelay("Testing..", disableUI: false, seconds: 3)
        EZLoadingActivity.show("Loading...", disableUI: true)
        //create the url with NSURL
        let url = NSURL(string: serviceName)
        
        //create the session object
        let session = URLSession.shared
        
        //now create the NSMutableRequest object using the url object
        let request = NSMutableURLRequest(url: url! as URL)
        request.httpMethod = "GET" //set http method as POST
        //        request.timeoutInterval = 20
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: sendData, options: .prettyPrinted) // pass dictionary to nsdata object and set it as request body
            
        } catch let error {
            print(error.localizedDescription)
        }
        
        //HTTP Headers
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        //create dataTask using the session object to send data to the server
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            EZLoadingActivity.hide()
            
            if let httpStatus = response as? HTTPURLResponse {
                // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                if httpStatus.statusCode == 200 {
                    self.isSuccess = true
                }else{
                    self.isSuccess = false
                }
                
                if httpStatus.statusCode == 400 {
                    self.error400 = true
                }
                else if(httpStatus.statusCode == 401)
                {
                    self.error401 = true
                }
                else if(httpStatus.statusCode == 403)
                {
                    self.error403 = true
                }
                
            }
            //end copied
            
            guard error == nil else {
                print("this is error")
                //hide activity indicator...
                self.showFailureAlert("request failure text...")
                return
            }
            guard let data = data else {
                return
            }
            
            do {
                //create json object from data
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: AnyObject] {
                    success(json)
                    //handle json...
                    //hide activity indicator
                }
                //hide activity indicator
            } catch let error {
                //hide activity indicator
                print(error.localizedDescription)
                var errors = [String : AnyObject]()
                errors["Error"] = "Server Error" as AnyObject
                failure(errors)
            }
            
        })
        
        task.resume()
        
    }
    //MARK: - Generic Functions
    func showFailureAlert(_ message:String)
    {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: ConstantStrings.appName, message: message, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
        }
        
    }
    func getUrlStringLastIndex(urlString : String) -> String {
        let separators = NSCharacterSet(charactersIn: "/")
        let lastIndex = urlString.components(separatedBy: separators as CharacterSet).last
        return lastIndex ?? ""
    }
}
