//
//  TemplateCell.swift
//  Fintech
//
//  Created by broadstone on 14/03/2022.
//  Copyright © 2022 Broadstone Technologies. All rights reserved.
//

import UIKit

class TemplateCell: UITableViewCell {
    
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var templateImage: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
