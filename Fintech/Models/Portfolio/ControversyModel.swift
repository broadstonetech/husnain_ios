//
//  ControversyModel.swift
//  Fintech
//
//  Created by broadstone on 31/08/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import Foundation

struct ControversyModel {
    let success: Bool
    let message: String
    let data: ControversyData
}

struct ControversyData {
    let count: Int
    let controversies: [String]
    let peerControversy: Double
}
