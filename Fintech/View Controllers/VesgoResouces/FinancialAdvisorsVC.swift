//
//  FinancialAdvisorsVC.swift
//  EducationModuleSP
//
//  Created by SaifUllah Butt on 27/12/2021.
//

import UIKit
import Kingfisher
import ImageLoader

class FinancialAdvisorsVC: ResourcesParent {

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var advisorsTableView: UITableView!
    @IBOutlet weak var dataAvailableView: UIView!
    @IBOutlet weak var dataNotAvailableView: UIView!
    
    var allAdvisorsData = [AdvisorsModel]()
    var filteredAdvisorsData = [AdvisorsModel]()
    var searchedAdvisorsData = [AdvisorsModel]()
    var advName = [String]()
    var isSearching = false
    var isTableDataSearching = false
    
    var filtersData: [FilterCategories]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.filtersData = getFiltersData()
        self.advisorApiDelegate = self
        getAdvisors(url: ApiUrl.Get_All_Advisor)
        
        searchBar.delegate = self
        advisorsTableView.delegate = self
        advisorsTableView.dataSource = self
    
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        setNavBarTitile(title: "Back", topItem: getLocalizedString(key: "_advisors"))
    }
    
    @IBAction func sortBtnAction(_ sender: Any) {
//        let vc = storyboard?.instantiateViewController(withIdentifier: "FilterVC") as! FilterViewVC
//        vc.delegate = self
//        vc.selectedOption = selectedOptions
//        vc.availability = availablity
//        vc.sortType = sortType
//       self.present(vc, animated : true, completion: nil)
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "AdvisorFilterVC") as! AdvisorFilterVC
        vc.delegate = self
        vc.filters = self.filtersData
        self.present(vc, animated: true, completion: nil)
        
    }
    
    @IBAction func reloadBtnPressed(_ sender: UIButton) {
        getAdvisors(url: ApiUrl.Get_All_Advisor)
    }
    

}

extension FinancialAdvisorsVC: UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isTableDataSearching {
            return searchedAdvisorsData.count
        }else {
            if isSearching {
                return filteredAdvisorsData.count
            }
            else{
                return allAdvisorsData.count
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var data: AdvisorsModel
        
        if isTableDataSearching {
            data = searchedAdvisorsData[indexPath.row]
        }else {
            if isSearching {
                data = filteredAdvisorsData[indexPath.row]
            }else {
                data = allAdvisorsData[indexPath.row]
            }
        }
        
        let cell = advisorsTableView.dequeueReusableCell(withIdentifier: "AdvisorTVCell") as! AdvisorTVCell
        cell.advisorName.text = data.advisorName
        cell.advisorDescription.text = data.advisorDescription
        
        cell.advisorImage.kf.setImage(with: URL(string: data.advisorProfilePic), placeholder: UIImage(named: "caleb-dp"))
        
        
        if data.advisorAvailability == false{
            cell.advisorStatus.textColor = .darkGray
            cell.advisorStatus.text = getLocalizedString(key: "_offline")
        }
        
        else {
            cell.advisorStatus.textColor = .systemGreen
            cell.advisorStatus.text = getLocalizedString(key: "_online")
        }
        
        cell.expertiseTagListView.removeAllTags()
        cell.expertiseTagListView.addTags(data.advisorExpertise)
        
        return cell
        
        
    }

    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let objViewController = self.storyboard?.instantiateViewController(withIdentifier: "AdvisorProfileVC") as! AdvisorProfiileVC
        
        if isTableDataSearching {
            goToAdvisorProfile(advisorModel: searchedAdvisorsData[indexPath.row])
        }else {
            if isSearching{
                goToAdvisorProfile(advisorModel: filteredAdvisorsData[indexPath.row])
            }
            else{
                goToAdvisorProfile(advisorModel: allAdvisorsData[indexPath.row])
            }
        }
        
        
        
        
        self.present(objViewController, animated: true, completion: nil)
    }
    

    
}

extension FinancialAdvisorsVC : FilterVCDelegte{
    
    func selection(option: [String], sortType: SortByAlphabets, availability: SortByAvailability, completeFiltersData: [FilterCategories]) {
        self.filtersData = completeFiltersData
        isSearching = true
        
        
        filteredAdvisorsData.removeAll()
        var tempData = allAdvisorsData
        print(option)
        for item in option{
            for advsiorData in tempData {
                let isDataAvailable = advsiorData.advisorExpertise.contains(where: {
                    $0 == item
                })
                if isDataAvailable{
                    filteredAdvisorsData.append(advsiorData)
                    tempData.removeAll(where: {
                        $0.advisorExpertise.contains(item)
                    })
                }
            }
            
        }
        
        if filteredAdvisorsData.count == 0 {
            filteredAdvisorsData = allAdvisorsData
        }
        if sortType == .AtoZ {
            self.filteredAdvisorsData.sort { $0.advisorName.lowercased() < $1.advisorName.lowercased() }
        }
        else if sortType == .ZtoA {
            self.filteredAdvisorsData.sort { $0.advisorName.lowercased() > $1.advisorName.lowercased() }
        }
        
        if availability == .online{
            self.filteredAdvisorsData.removeAll(where: {
                $0.advisorAvailability == false
            })
        }
        
        else if availability == .offline{
            self.filteredAdvisorsData.removeAll(where: {
                $0.advisorAvailability == true
            })
        }
        
        
        advisorsTableView.reloadData()
    }
    
}




extension FinancialAdvisorsVC: UISearchBarDelegate {
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        isTableDataSearching = false
        DispatchQueue.main.async{
            self.advisorsTableView.reloadData()
            
        }
    }

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        DispatchQueue.main.async { [self] in
            if self.searchBar.text == ""{
                isTableDataSearching = false
                DispatchQueue.main.async{
                    self.advisorsTableView.reloadData()
                    
                }
            }else{
              
                isTableDataSearching = true
                if isSearching {
                    searchedAdvisorsData = filteredAdvisorsData.filter({$0.advisorName.lowercased().range(of: self.searchBar.text!.lowercased()) != nil})
                }else {
                    searchedAdvisorsData = allAdvisorsData.filter({$0.advisorName.lowercased().range(of: self.searchBar.text!.lowercased()) != nil})
                }
                 
                DispatchQueue.main.async{
                    self.advisorsTableView.reloadData()
                    
                }
            }
        }
    }
  
    
}
      
extension FinancialAdvisorsVC: AdvisorApiDelegate {
    func advisorApiCalled(isSuccessful: Bool, advisor: [AdvisorsModel]) {
        if isSuccessful {
            self.filteredAdvisorsData.append(contentsOf: advisor)
            self.allAdvisorsData.append(contentsOf: advisor)
            advisorsTableView.reloadData()
            
            dataAvailableView.isHidden = false
            dataNotAvailableView.isHidden = true
        }else {
            dataAvailableView.isHidden = true
            dataNotAvailableView.isHidden = false
        }
    }
    
    
    
}
