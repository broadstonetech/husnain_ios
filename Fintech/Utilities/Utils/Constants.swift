//
//  Constants.swift
//  Fintech
//
//  Created by MacBook-Mubashar on 22/08/2019.
//  Copyright © 2019 Broadstone Technologies. All rights reserved.
//

import Foundation

struct ConstantStrings {
    static let APP_LINK = "https://linktr.ee/vesgoapps"
    static let appName = "Vesgo"
    static let fill_all_field = getLocalizedString(key: "fill_all_field", comment: "")
    static let username_empty = getLocalizedString(key: "username_empty", comment: "")
    static let token_empty = getLocalizedString(key: "token_empty", comment: "")
    static let password_empty = getLocalizedString(key: "password_empty", comment: "")
    static let invalid_email = getLocalizedString(key: "invalid_email", comment: "")
    static let invalid_password = getLocalizedString(key: "invalid_password", comment: "")
    static let Password_mismatch = getLocalizedString(key: "Password_mismatch", comment: "")
    static let Error_msg_generic = getLocalizedString(key: "Error_msg_generic", comment: "")
    static let NO_INTERNET_AVAILABLE = getLocalizedString(key: "NO_INTERNET_AVAILABLE", comment: "")
    static let PASSWORD_RESET_SUCCESS = getLocalizedString(key: "PASSWORD_RESET_SUCCESS", comment: "")
    static let SIGNUP_SUCCESS_MSG = getLocalizedString(key: "SIGNUP_SUCCESS_MSG", comment: "")
    static let user_status_pending_movequestionpage = getLocalizedString(key: "user_status_pending_movequestionpage", comment: "")
    
    static let InvestmentStrategy_status_done = getLocalizedString(key: "InvestmentStrategy_status_done", comment: "")
    static let InvestmentStrategy_status_pending = getLocalizedString(key: "InvestmentStrategy_status_pending", comment: "")
    static let LOGIN_WITH_APPLE_FAILED_ALERT_TEXT = getLocalizedString(key: "LOGIN_WITH_APPLE_FAILED_ALERT_TEXT", comment: "")
    static let CHARITABLE_PLAN_NOT_SET = getLocalizedString(key: "CHARITABLE_PLAN_NOT_SET", comment: "")
    static let NO_ALIGNED_CHARITIES_FOUNND = getLocalizedString(key: "NO_ALIGNED_CHARITIES_FOUNND", comment: "")
    static let APPLE_PAY_NOT_SUPPORTED = getLocalizedString(key: "APPLE_PAY_NOT_SUPPORTED", comment: "")
    
    static let app_name = "Vesgo"
    static let MERCHANT_ID = "merchant.fintech"
    static let PAYMENT_CURRENCY = "USD"
    static let PAYMENT_COUNTRY = "US"
    static let TYPE_USER = "user"
    //search params
    static let SEARCH_ALL = "all"
    static let SEARCH_BY_KEYWORD = "keyword"
    static let SEARCH_BY_CATEGORY = "category"
    static let SEARCH_BY_QR = "qrcode"
    
    
    //
    static let NO_INTERNET_CONNECTION_FOUND = "No internet connection found. Make sure you're connected to network."
    // Mark : - set nevigation bar title
    static let News_Feed = getLocalizedString(key: "News_Feed", comment: "")
    
    //alert messages
    
    static let INVALID_URL_ALERT_TEXT = getLocalizedString(key: "INVALID_URL_ALERT_TEXT", comment: "")
    static let PURCHSAE_POPUP_TEXT = "Select a payment method to purchase this model."
    static let GENERIC_ALERT_TEXT_400 = getLocalizedString(key: "GENERIC_ALERT_TEXT_400", comment: "")
    static let GENERIC_ALERT_TEXT_401 = getLocalizedString(key: "GENERIC_ALERT_TEXT_401", comment: "")
    static let SIGN_IN_ALERT_TEXT = getLocalizedString(key: "SIGN_IN_ALERT_TEXT", comment: "")
    static let INVALID_USERNAME_OR_PASS_ALERT_TEXT  = getLocalizedString(key: "INVALID_USERNAME_OR_PASS_ALERT_TEXT", comment: "")
    static let INVALID_FINGER_PRINT_TEXT  = getLocalizedString(key: "INVALID_FINGER_PRINT_TEXT", comment: "")
    //Account Setup alert messages
    static let LOGIN_FAILED_ALERT_TEXT = getLocalizedString(key: "LOGIN_FAILED_ALERT_TEXT", comment: "")
    static let SIGN_UP_FAILED_ALERT_TEXT = getLocalizedString(key: "SIGN_UP_FAILED_ALERT_TEXT", comment: "")
    static let SIGNING_UP_ALERT_TEXT = getLocalizedString(key: "SIGNING_UP_ALERT_TEXT", comment: "")
    static let FORGET_PASSWORD_ALERT_TEXT = getLocalizedString(key: "FORGET_PASSWORD_ALERT_TEXT", comment: "")
    static let USER_MAIL_DIRECTED_ALERT_TEXT = getLocalizedString(key: "USER_MAIL_DIRECTED_ALERT_TEXT", comment: "")
    static let ENTER_TOKEN_ALERT_TEXT = getLocalizedString(key: "ENTER_TOKEN_ALERT_TEXT", comment: "")
    static let PASSWORD_CHANGED_SUCCESS_ALERT_TEXT = getLocalizedString(key: "PASSWORD_CHANGED_SUCCESS_ALERT_TEXT", comment: "")
    static let INVALID_TOKEN_ALERT_TEXT = getLocalizedString(key: "INVALID_TOKEN_ALERT_TEXT", comment: "")
    
    
    // Explore Model alert messages
    static let NO_RESULT_FOUND_ALERT_TEXT = getLocalizedString(key: "NO_RESULT_FOUND_ALERT_TEXT", comment: "")
    static let LOADING_RESULT_ALERT_TEXT = getLocalizedString(key: "LOADING_RESULT_ALERT_TEXT", comment: "")
    static let SEARCH_BY_KEYWORD_ALERT_TEXT = getLocalizedString(key: "SEARCH_BY_KEYWORD_ALERT_TEXT", comment: "")
    static let NO_RESULT_FOUND_WITH_KEYWORD_ALERT = getLocalizedString(key: "NO_RESULT_FOUND_WITH_KEYWORD_ALERT", comment: "")
    // Left menu drawer alert messages
    //Faqs
    static let SUMBIT_SUPPORT_REQUEST_SUCCESS_ALERT_TEXT = getLocalizedString(key: "SUMBIT_SUPPORT_REQUEST_SUCCESS_ALERT_TEXT", comment: "")
    static let REQUEST_SUPPORT_FAILURE_ALERT_TEXT = getLocalizedString(key: "REQUEST_SUPPORT_FAILURE_ALERT_TEXT", comment: "")
    // reward campaigns
    //refer friend
    static let FRIEND_INVITATION_ALERT_TEXT = getLocalizedString(key: "FRIEND_INVITATION_ALERT_TEXT", comment: "")
    static let FRIEND_INVITATION_FAILED_ALERT_TEXT = getLocalizedString(key: "FRIEND_INVITATION_FAILED_ALERT_TEXT", comment: "")
    
    static let SHARED_MODEL_LINK_EXPIRED_ALERT_TEXT = getLocalizedString(key: "SHARED_MODEL_LINK_EXPIRED_ALERT_TEXT", comment: "")
    static let EXCEEDING_LIMIT_ALERT_TEXT = getLocalizedString(key: "EXCEEDING_LIMIT_ALERT_TEXT", comment: "")
    
    
    // account alert message
     static let Sign_Out_alert_Title_message = getLocalizedString(key: "Sign_Out_alert_Title_message", comment: "")
    static let Sign_Out_alert_meage = getLocalizedString(key: "Sign_Out_alert_meage", comment: "")
    static let investmentStrategy_alert_mesage = getLocalizedString(key: "investmentStrategy_alert_mesage", comment: "")
    static let investmentStrategy_alert_unregister_user = getLocalizedString(key: "investmentStrategy_alert_unregister_user", comment: "")
    static let Lanaguage_titel = getLocalizedString(key: "Lanaguage_titel", comment: "")
    // resetpassword alert
    static let OLD_PASSWORD_emty = getLocalizedString(key: "OLD_PASSWORD_emty", comment: "")
    static let NEW_PASSWORD_Emty = getLocalizedString(key: "NEW_PASSWORD_Emty", comment: "")
    static let CONFIRM_PASSWORD_Emty = getLocalizedString(key: "CONFIRM_PASSWORD_Emty", comment: "")
    static let PASSWORD_MISMATCH = getLocalizedString(key: "PASSWORD_MISMATCH", comment: "")
    static let PASSWORD_SHOULD_BE_8_CHARACTERS_ONE_ALPHABET = getLocalizedString(key: "PASSWORD_SHOULD_BE_8_CHARACTERS_ONE_ALPHABET", comment: "")
    static let FIRST_NAME_Emty  = getLocalizedString(key: "FIRST_NAME_Emty", comment: "")
    static let LAST_NAME_Emty  = getLocalizedString(key: "LAST_NAME_Emty", comment: "")
    static let EMAIL_WAS_Emty  = getLocalizedString(key: "EMAIL_WAS_Emty", comment: "")
static let APPLE_SININ_REVOKED = getLocalizedString(key: "APPLE_SININ_REVOKED", comment: "")
}

struct CampaignsStrings {
    static let campaing_title_empty = "Campaign title was empty."
    static let moments_appID_empty = "Campaign title was empty."
    static let moments_apiKey_empty = "Campaign title was empty."
    static let rewardID_empty = "Campaign title was empty."
    static let campaingID_empty = "Campaign title was empty."
    static let xApiKey_empty = "Campaign title was empty."
    static let downloads_empty = "Campaign title was empty."
}

struct PaymentAPIKeys {
    //static let STRIPE_PUBLISHABLE_KEY = "pk_test_rRpw6uhmq41HVQGPsDsMgG1200CRKzDcaL"
    static let STRIPE_PUBLISHABLE_KEY = "pk_test_rRpw6uhmq41HVQGPsDsMgG1200CRKzDcaL"
    static let APPLE_MERCHANT_ID = "merchant.io.vesgo.app.charity"
}

struct PaymentConstants {
    static let COUNTRY_CODE = "US"
    static let CURRENCY = "USD"
    static let STRIPE_TRANSACTION_FEES = 2.9
    static let APPLE_PAY_TRANSACTION_FEES = 2.9
    static let STRIPE_CONSTANT_CHARGE = 0.3    //$0.30 charged every successfull transaction.
    static let CHARITY_METHOD_SOLID = "solid"
    static let CHARITY_METHOD_INVEST = "invest"
    
}

struct PaymentObservers {
    static let paymentCompleteNotificationKey = "paymentCompleteNotificationKey"
}

struct BudgetingConstants {
    static let DEFAULT_AMOUNT_FOR_BUDGETING = 0.0
    
    static let FREQUENCY_TYPE_DAY = "day"
    static let FREQUENCY_TYPE_WEEK = "week"
    static let FREQUENCY_TYPE_MONTH = "month"
    static let FREQUENCY_TYPE_ALL = "all"
    
}

struct FCM {
    static let NOTIFICATION_KEY = "loc_key"
    
    static let NOTIFICATION_TYPE_CHARITY = "charity"
    static let NOTIFICATION_TYPE_SPENDING = "spending"
    static let NOTIFICATION_TYPE_PORTFOLIO = "my_portfolio"
    static let NOTIFICATION_TYPE_NEWSFEED = "newsfeed"
    static let NOTIFICATION_TYPE_EDUCATION = "education"
    static let NOTIFICATION_TYPE_SPENDING_INSIGHTS = "spending_insights"
    static let NOTIFICATION_TYPE_LEADERBOARD = "leaderboard"
    static let NOTIFICATION_TYPE_GENERAL = "general"
    static let NOTIFICATION_TYPE_DEFAULT = "default"
    static let NOTIFICATION_TYPE_PORTFOLIO_CREATED = "portfolio_created"
    
    static let DEFAULT_NAMESPACE_FOR_NOTIFICATION = "default_abc"
}


