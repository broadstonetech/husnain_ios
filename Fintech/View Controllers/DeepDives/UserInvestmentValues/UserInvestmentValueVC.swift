//
//  UserInvestmentValueVC.swift
//  Fintech
//
//  Created by broadstone on 02/02/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import Foundation
import UIKit
class UserInvestmentValueVC: Parent, UITableViewDataSource, UITableViewDelegate{
    
    var userValuesModel: UserValuesModel!
    
    
    @IBOutlet weak var valueTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        valueTableView.delegate = self
        valueTableView.dataSource = self
        
        fetchUserValues()
    
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = false
        setNavBarTitile(title: "Investment values", topItem: "Investment values")
    }
    
    //MARK: -FetchUserValues()
    func  fetchUserValues(){
        view.activityStartAnimating()
        let url = ApiUrl.GET_USER_INVESTMENT_VALUES
        let namespace = SPManager.sharedInstance.getCurrentUserNamespace()

        postManagerUser(method: "POST", isThisURLEmbdedData: false, urlString: url, parameter: ["namespace": namespace], success: {(value) in

            do {
                self.userValuesModel = try JSONDecoder().decode(UserValuesModel?.self, from: value)
                DispatchQueue.main.async { [self] in
                    valueTableView.reloadData()
                }
                

            } catch { }

            
            DispatchQueue.main.async { [self] in
                view.activityStopAnimating()
            }

            self.setAnalyticsAction(ScreenName: "UserInvestmentValueVC", method: "UserValuesFetched")
            
        }, failure: {(err) in

            DispatchQueue.main.async { [self] in
                view.activityStopAnimating()
            }

            if APIManager.sharedInstance.error400 {

                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)

            }

            if(APIManager.sharedInstance.error401)

            {

                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)

            }else{
                self.showAlert(ConstantStrings.Error_msg_generic)
                

            }
        })

    }
    
    
    //MARK: Populating the TableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if userValuesModel == nil {
            return 0
        }
        else{
            let valuesCount =  userValuesModel.data.positiveValues.count + userValuesModel.data.negativeValues.count
            let esgCount = userValuesModel.data.portfolioComposition.esg.count
            let derivedCount = userValuesModel.data.portfolioComposition.derivedValues.count
            return 1 + valuesCount +  1 + esgCount + 1 + derivedCount
            
        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let valuesCount =  userValuesModel.data.positiveValues.count + userValuesModel.data.negativeValues.count
        let esgCount = userValuesModel.data.portfolioComposition.esg.count
        let derivedCount = userValuesModel.data.portfolioComposition.derivedValues.count
        
        
        if indexPath.row < 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "InvestmentValuesHeadingCell") as! InvestmentValuesHeadingCell
            cell.headingLabel.text = "Investment values"
            
            return cell
        }
        else if indexPath.row < (1 + userValuesModel.data.positiveValues.count) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "UserValuesCell") as! UserValuesCell
            cell.valuesLabel.text = userValuesModel.data.positiveValues[indexPath.row - 1]
            cell.image1.image = FintechUtils.sharedInstance.getInvestmentValuesIcons(investmentValue: (userValuesModel.data.positiveValues[indexPath.row - 1]) as! String)
            cell.image2.image = UIImage(named: "care")!
            return cell
        }
        
        else if indexPath.row < (1 + valuesCount){
            
            let index = indexPath.row - userValuesModel.data.positiveValues.count - 1
            let cell = tableView.dequeueReusableCell(withIdentifier: "UserValuesCell") as! UserValuesCell
            cell.valuesLabel.text = userValuesModel.data.negativeValues[index]
            cell.image1.image = FintechUtils.sharedInstance.getInvestmentValuesIcons(investmentValue: (userValuesModel.data.positiveValues[index]))
            cell.image2.image = UIImage(named: "angry")!
            return cell
            
        }else if indexPath.row < (1 + userValuesModel.data.positiveValues.count + userValuesModel.data.negativeValues.count + 1) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "InvestmentValuesHeadingCell") as! InvestmentValuesHeadingCell
            cell.headingLabel.text = "Investment impact"
            
            return cell
        }
        else if indexPath.row < (1 + valuesCount + 1 + userValuesModel.data.portfolioComposition.esg.count){
            
            let index = indexPath.row - 1 - userValuesModel.data.positiveValues.count - userValuesModel.data.negativeValues.count - 1
            let cell = tableView.dequeueReusableCell(withIdentifier: "UserValuesCell2") as! UserValuesCell2
            cell.headingLbl.text = userValuesModel.data.portfolioComposition.esg[index].value
            
            cell.impactLbl.text = String(userValuesModel.data.portfolioComposition.esg[index].percentile) + "percentile (" + String((userValuesModel.data.portfolioComposition.esg[index].score))  + ")"
            cell.iconImage.image = FintechUtils.sharedInstance.getInvestmentValuesIcons(investmentValue: (userValuesModel.data.portfolioComposition.esg[index].value))
            return cell
            
        }else if indexPath.row < (1 + valuesCount + 1 + esgCount + 1 ) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "InvestmentValuesHeadingCell") as! InvestmentValuesHeadingCell
            cell.headingLabel.text = "Investment impact"
            
            return cell
        }
        else if indexPath.row < (1 + valuesCount + 1 + esgCount + 1 + derivedCount){
            
            let index = indexPath.row - 1 - valuesCount - 1 - userValuesModel.data.portfolioComposition.esg.count - 1
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "UserValuesCell2") as! UserValuesCell2
            cell.headingLbl.text = userValuesModel.data.portfolioComposition.derivedValues[index].value
            
            cell.iconImage.image = FintechUtils.sharedInstance.getInvestmentValuesIcons(investmentValue: (userValuesModel.data.portfolioComposition.derivedValues[index].value))
            
            let score = userValuesModel.data.portfolioComposition.derivedValues[index].score
            if score > 0.00 {
                cell.impactLbl.text = "Some Impact"
            }
            else{
                cell.impactLbl.text = "No Impact"
            }
            
            return cell
            
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let valuesCount =  userValuesModel.data.positiveValues.count + userValuesModel.data.negativeValues.count
        let esgCount = userValuesModel.data.portfolioComposition.esg.count
        let derivedCount = userValuesModel.data.portfolioComposition.derivedValues.count
        
        if indexPath.row < 1 {
            
        }
        
        else if indexPath.row < (1 + userValuesModel.data.positiveValues.count) {
            
        }
        
        else if indexPath.row < (1 + valuesCount){
            
            let index = indexPath.row - 1 - userValuesModel.data.positiveValues.count
            
            
        }
        else if indexPath.row > (1 + valuesCount) && indexPath.row < (1 + valuesCount + 1 + esgCount){
            
            let index = indexPath.row - 1 - valuesCount - 1
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "personaValuesDetailsVC") as! personaValuesDetailsVC
            detailsname = (userValuesModel.data.portfolioComposition.esg[index].value)
            navigationController?.pushViewController(vc, animated: true)
            
            
        }
        else if indexPath.row > (1 + valuesCount + 1 + esgCount){
            
            let index = indexPath.row - 1 - valuesCount - 1 - userValuesModel.data.portfolioComposition.esg.count - 1
            
            
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "personaValuesDetailsVC") as! personaValuesDetailsVC
            detailsname = (userValuesModel.data.portfolioComposition.derivedValues[index].value)
            navigationController?.pushViewController(vc, animated: true)
            
        }
        
    }
    
}
