//
//  budgetingDashboardCell.swift
//  vesgo
//
//  Created by Apple on 05/01/2021.
//  Copyright © 2021 Apple. All rights reserved.
//

import UIKit

class budgetingDashboardCell: UICollectionViewCell {
    @IBOutlet weak var icon_img: UIImageView!
    @IBOutlet weak var heading_lbl: UILabel!
    @IBOutlet weak var amount_lbl: UILabel!
    @IBOutlet weak var bucketIcon: UIImageView!
    
    @IBOutlet weak var mainview: UIView!
    
    
}
