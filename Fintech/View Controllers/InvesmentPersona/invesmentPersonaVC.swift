//
//  invesmentPersonaVC.swift
//  Fintech
//
//  Created by Apple on 10/09/2020.
//  Copyright © 2020 Broadstone Technologies. All rights reserved.
//
import UIKit
import MTSlideToOpen
import Floaty
import LocalAuthentication
import AVFoundation
import AuthenticationServices
import Lottie
import MTSlideToOpen
import UIKit
import AVKit
import Charts
import RingGraph
var isLoop: Bool = false
let imgesCompssions = ["environment","social","governance","vegan","palm oil","child lb"]
let imgesAvoid = ["firearms","controversial_weapons","GMO"]
let imgesromove = ["care","care","care","care","care","angry","angry"]
let imojiicon = ["care","care","angry","angry"]
let imgesCareAbout = ["military_contract","catholic","animal_welfare","firearms","controversial_weapons","GMO"]
var guestuser  = ""
class invesmentPersonaVC: Parent,MTSlideToOpenDelegate{
    
    //MARK: -IB Outlets
    @IBOutlet weak var Sample_Portfolio_View: UIView!
    @IBOutlet weak var invesment_strategy_GuestUser_btn: UIButton!
    @IBOutlet weak var firstTableView_values: UITableView!
    @IBOutlet weak var Profile_img: UIImageView!
    @IBOutlet weak var SlideButtonView: UIView!
    @IBOutlet weak var animation_vesgoLogo_view: AnimationView!
    @IBOutlet weak var account_setup_guestuser_btn: UIStackView!
    @IBOutlet weak var upload_img_Btn: UIButton!
    @IBOutlet weak var guestUser_view: UIView!
    @IBOutlet weak var investment_impact_lbl: UILabel!
    @IBOutlet weak var secondTableview_portfoliocomposition: UITableView!
    @IBOutlet weak var investmentpersonaButton_view: UIView!
    @IBOutlet weak var avoid_tableView: UITableView!
    @IBOutlet weak var floaty_btn: Floaty!
    @IBOutlet weak var username_lbl: UILabel!
    @IBOutlet weak var portfolioReturn_lbl: UILabel!
    @IBOutlet weak var Risk_lbl: UILabel!
    @IBOutlet weak var heading_NameEmily_lbl: UILabel!
    @IBOutlet weak var Description_emiy_lbl: UILabel!
    @IBOutlet weak var heding_NameJason_lbl: UILabel!
    @IBOutlet weak var description_Jason_lbl: UILabel!
    @IBOutlet weak var heading_NameCaleb_lbl: UILabel!
    @IBOutlet weak var vesgoLeaderboard_guestUser_btn: UIButton!
    @IBOutlet weak var descripation_top_lbl_guestUser: UILabel!
    @IBOutlet weak var decripation_explore_lbl: UILabel!
    @IBOutlet weak var descripation_caleb_lbl: UILabel!
    @IBOutlet weak var description_esra_label: UILabel!
    @IBOutlet weak var portfolio_value_lbl: UILabel!
    @IBOutlet weak var value_default: UILabel!
    @IBOutlet weak var pastMonth_lbl: UILabel!
    @IBOutlet weak var portfolio_lbl: UILabel!
    @IBOutlet weak var background_portfolio_Return_view: UIView!
    @IBOutlet weak var portfolio_return_Disable_lbl: UILabel!
    @IBOutlet weak var risktolance_mainView: UIView!
    @IBOutlet weak var recalulateRishtolerance_view: UIView!
    @IBOutlet weak var lineview_risktolerance: UIView!
    @IBOutlet weak var Username_values: UILabel!
    @IBOutlet weak var portfolio_userkeyfaceValues_btn: UIButton!
    
    @IBOutlet weak var sample_portfolio_video_play_View: UIView!
    @IBOutlet weak var status: UILabel!
    @IBOutlet weak var gender_lbl: UILabel!
    @IBOutlet weak var score_lbl: UILabel!
    @IBOutlet weak var gustUserInfoText_lbl: UILabel!
    
    @IBOutlet weak var showView_invesmentStrategy_View: UIView!
    @IBOutlet weak var precentile_lbl: UILabel!
    @IBOutlet weak var mainDashboardView: UIView!
    @IBOutlet weak var dataNotAvailableView: UIView!
    
    //Start new dashboard variables
    
    @IBOutlet weak var lineChart_view: LineChartView!
    @IBOutlet weak var containerView: RingGraphView!
    @IBOutlet weak var prograssbarview: RingGraphView!
    //@IBOutlet weak var dashboardTableView: UITableView!
    
    @IBOutlet weak var portfolioReturnLabel: UILabel!
    @IBOutlet weak var portfolioValueLabel: UILabel!
    @IBOutlet weak var portfolioNewsLabel: UILabel!
    @IBOutlet weak var charityDescLabel: UILabel!
    @IBOutlet weak var budgetingDescLabel: UILabel!
    @IBOutlet weak var budgetingEducationBtn: UIButton!
    
    @IBOutlet weak var dashboardReturnView: UIView!
    @IBOutlet weak var holdingsView: UIView!
    
    @IBOutlet weak var portfolioIsGeneratingView: UIView!
    @IBOutlet weak var holdingsDialogParentView: HoldingsDialogView!
    @IBOutlet weak var holdingsBlurView: UIVisualEffectView!
    
    @IBOutlet weak var returnDataNotAvailableView: UIView!
    @IBOutlet weak var returnDataAvailableView: UIView!
    
    @IBOutlet weak var portfolioReturn: UILabel!
    @IBOutlet weak var pastDayLabel: UILabel!
    @IBOutlet weak var portfolioValue: UILabel!
    @IBOutlet weak var topHoldingsLabel: UILabel!
    @IBOutlet weak var chartiyHeadingLabel: UILabel!
    @IBOutlet weak var budgetingHeadingLabel: UILabel!
    @IBOutlet weak var returnNotAvailableLabel: UILabel!
    @IBOutlet weak var portfolioIsGeneratingLabel: UILabel!
    
    @IBOutlet weak var vesgoLoadingAnimation: AnimationView!
    @IBOutlet weak var notificationIcon: UIImageView!
    @IBOutlet weak var notificationView: UIView!
    @IBOutlet weak var notificationCounterView: UIView!
    @IBOutlet weak var notificationCountLabel: UILabel!
    
    @IBOutlet weak var holdingsCollectionView: UICollectionView!
    
    
    //MARK: -Class variables
    let months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun"]
    let unitsSold = [50.0, 25.0, 50.0, 75.0, 100.0, 75.0]
    var labeltext = ["Environment","Social","Goveranance"]
    var circularView: CircularProgressView!
    var duration: TimeInterval!
    var timer: Timer?
    var isBudgetingPlanExist = 0
    var imagePicker = UIImagePickerController()
    var player = AVPlayer()
    var  sample_username = ""
    var videoUrlll = ""
    //MARK: -Slide to open var
    lazy var slideToOpen: MTSlideToOpenView = {
        let slide = MTSlideToOpenView(frame: CGRect(x: 0, y: 0, width: 300, height: 50))
        slide.sliderViewTopDistance = 0
        slide.sliderCornerRadius = 20
        slide.showSliderText = true
        //        slide.thumbnailColor = UIColor(red:141.0/255, green:19.0/255, blue:65.0/255, alpha:1.0)
        //slide.thumbnailColor = #colorLiteral(red: 0.4705882353, green: 0.3725490196, blue: 0.9333333333, alpha: 0.6422838185)
        slide.slidingColor = #colorLiteral(red: 0.1513735056, green: 0.6249956489, blue: 0.8893578649, alpha: 1)
        slide.textColor = UIColor.white
        // slide.sliderBackgroundColor = UIColor(red:0.88, green:1, blue:0.98, alpha:1.0)
        slide.sliderBackgroundColor = #colorLiteral(red: 0.3098039216, green: 0.6156862745, blue: 0.8666666667, alpha: 1)
        slide.delegate = self
        let labeltext = getLocalizedString(key: "Start your journey now", comment: "")
        slide.labelText = labeltext
        slide.thumnailImageView.image = #imageLiteral(resourceName: "icon").imageFlippedForRightToLeftLayoutDirection()
        return slide
    }()
    
    //MARK: -Data model instances
    var dashboardBudgetingModel: DashboardBudgetingModel!
    var dashboardCharityModel: DashboardCharityModel!
    var dashboardHoldingsModel: DashboardHoldingsModel!
    var dashboardChartModel: DashboardChartModel!
    
    
    private func loadingAnimation(play: Bool){
        DispatchQueue.main.async { [self] in
            if !play {
                vesgoLoadingAnimation.stop()
                return
            }
            vesgoLoadingAnimation.loopMode = .loop
            vesgoLoadingAnimation.play()
        }
    }
    
    private func changeNotificationIcon(){
        UNUserNotificationCenter.current().getDeliveredNotifications { (notifications) in
            print("Notification: \(notifications)")
            print("Notification Count: \(notifications.count)")
            DispatchQueue.main.async { [self] in
                if notifications.count > 0 {
                    notificationCounterView.isHidden = false
                    notificationCounterView.widthConstaint?.constant = 40
                    notificationCountLabel.text = String(notifications.count)
                }else {
                    notificationCounterView.isHidden = true
                    notificationCounterView.widthConstaint?.constant = 0
                    notificationCountLabel.text = "0"
                }
            }
        }
        
    }
    
    
    @objc func notificationsBtnPressed() {
        let storyboard = UIStoryboard.init(name: "Notification", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func budgetingSectionPressed(_ sender: Any) {
        if self.dashboardBudgetingModel == nil {
            self.view.makeToast("Please wait, fetching data...")
            return
        }
        let currentDate = Int64((Date().timeIntervalSince1970*1000).rounded())
        if self.dashboardBudgetingModel.data.plannedAmount > 0.0 {
            
            if self.dashboardBudgetingModel.data.plannedStartDate > currentDate {
                let storyboard = UIStoryboard(name: "Budgeting", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "BudgetingPlan") as! plusBudgetingVC
                vc.budgetType = "update"
                self.navigationController?.pushViewController(vc, animated: true)
            }else {
                let storyboard = UIStoryboard(name: "Budgeting", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "BudgetingDashboard") as UIViewController
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }else{
            let storyboard = UIStoryboard(name: "Budgeting", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "BudgetingPlan") as UIViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    @IBAction func charitySectionPressed(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Charity", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CharityDashboardVC") as UIViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
        
    }
    
    @IBAction func goToHoldingsScreenBtnPressed(_ sender: Any) {
        self.tabBarController?.selectedIndex = 1
    }
    
    @IBAction func budgetingEducationBtnPressed(_ sender: Any) {
        self.tabBarController?.selectedIndex = 3
    }
    
   //MARK: -LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        lineview_risktolerance.isHidden = true
        sample_username = UserDefaults.standard.string(forKey: "sampleportfolio_username") ?? ""
        floaty_btn.isHidden = true
        dataNotAvailableView.isHidden = true
        
        holdingsCollectionView.register(UINib(nibName: "TopHoldingCell", bundle: .main), forCellWithReuseIdentifier: "TopHoldingCell")
        
        holdingsCollectionView.isScrollEnabled = true
        holdingsCollectionView.showsHorizontalScrollIndicator = false
        holdingsCollectionView.showsVerticalScrollIndicator = false
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        
        holdingsCollectionView.collectionViewLayout = layout
        holdingsCollectionView.delegate = self
        holdingsCollectionView.dataSource = self
        
        let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(notificationsBtnPressed))
        notificationView.isUserInteractionEnabled = true
        notificationView.addGestureRecognizer(tapGesture)
        
        if let value = UserDefaults.standard.value(forKey: "guestuser" ){
            guestuser = value as! String
            // Level_VIew.isHidden = true
            guestUser_view.isHidden = true
            mainDashboardView.isHidden = true
            // tabBarController?.tabBar.items![1].isEnabled = true
            if SPManager.sharedInstance.getUserPortfolioStatus(){
                mainDashboardView.isHidden = false
                portfolioIsGeneratingView.isHidden = false
                loadingAnimation(play: true)
                if NetworkReachability.isConnectedToNetwork() {
                    
                    
                    self.callDashboardChartAPI()
                    self.callDashboardHoldingsAPI()
                    //self.callDashboardCharityAPI()
                    //self.callDashboardBudgetingAPI()
                } else {
                    self.view.makeToast(ConstantStrings.NO_INTERNET_CONNECTION_FOUND)
                }
            } else {
                print("Api not calling")
            }
            
        }else{
            // Level_VIew.isHidden = false
            invesment_strategy_GuestUser_btn.isHidden = true
            guestUser_view.isHidden = false
            //play video here
            playBgIntroVideo()
            tabBarController?.tabBar.items![1].isEnabled = false
            
        }
        
        recalulateRishtolerance_view.isHidden = true
        // lineview_risktolerance.isHidden = true
        showView_invesmentStrategy_View.isHidden  = true
        risktolance_mainView!.heightConstaint?.constant = 115
        // showView_invesmentStrategy_View!.heightConstaint?.constant = 0
        //investmentpersonaButton_view!.heightConstaint?.constant = 0
        // investmentpersonaButton_view.isHidden = true
        self.imagePicker.delegate = self
        
        
        DispatchQueue.main.async {
            let data =  ArrayLists.sharedInstance.demographicsdashboarddatalist
            print(data)
        }
        self.navigationController?.isNavigationBarHidden = true
        if sample_username == "" {
            username_lbl.text = "Dashboard"
            
        }else {
            username_lbl.text = "\(sample_username)'s Dashboard"
            
        }
        if SPManager.sharedInstance.saveUserLoginStatus() {
            upload_img_Btn.isHidden = false
            
            username_lbl.text = "\(SPManager.sharedInstance.getCurrentUserNameForProfile())'s Dashboard"
            
        }else {
            upload_img_Btn.isHidden = true
        }
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        changeNotificationIcon()
        //getBudgetingExistencePlanAPI()
        sample_username = UserDefaults.standard.string(forKey: "sampleportfolio_username") ?? ""
        self.navigationController?.isNavigationBarHidden = true
        sample_portfolio_video_play_View.isHidden = true
        
        account_setup_guestuser_btn.isHidden = true
        Sample_Portfolio_View.isHidden = true
        if backpostion == "account"{
            backpostion = ""
            //self.tabBarController?.selectedIndex = 4
            
        }
        //
        //        animation_vesgoLogo_view.contentMode = .scaleAspectFit
        //        animation_vesgoLogo_view.animationSpeed = 1.0
        //        animation_vesgoLogo_view.play()
        //background_portfolio_Return_view.isHidden = true
        let img = UserDefaults.standard.string(forKey: "\(SPManager.sharedInstance.getCurrentUserEmail())img")
        self.guestuserr()
        
        if SPManager.sharedInstance.getUserPortfolioStatus(){
            if NetworkReachability.isConnectedToNetwork() {
                self.callDashboardCharityAPI()
                self.callDashboardBudgetingAPI()
            } else {
                print(ConstantStrings.NO_INTERNET_CONNECTION_FOUND)
            }
        } else {
            print("Api not calling")
        }
        
        // if SPManager.sharedInstance
        //print(img)
        
        // Profile_img.image = img
        var date = ""
        if SPManager.sharedInstance.saveUserLoginStatus() {
            print(SPManager.sharedInstance.getUserPortfolioStatus())
            Sample_Portfolio_View.isHidden = true
            
            if SPManager.sharedInstance.getUserPortfolioStatus(){
                recalulateRishtolerance_view.isHidden = false
                //lineview_risktolerance.isHidden = false
                //investmentpersonaButton_view.isHidden = false
                //playBgIntroVideo()
                tabBarController?.tabBar.items![1].isEnabled = true
                guestUser_view.isHidden = true
                //account_setup_guestuser_btn.isHidden = true
                showView_invesmentStrategy_View.isHidden  = true
                risktolance_mainView!.heightConstaint?.constant = 100
                print("user portfolio status Done ")
                
            }else{
                print("user portfolio status panding ")
                
                playBgIntroVideo()
                tabBarController?.tabBar.items![1].isEnabled = false
                guestUser_view.isHidden = false
                //account_setup_guestuser_btn.isHidden = true
                
                invesment_strategy_GuestUser_btn.isHidden = false
                showView_invesmentStrategy_View.isHidden = false
                mainDashboardView.isHidden = true
                recalulateRishtolerance_view.isHidden = true
                // lineview_risktolerance.isHidden = true
                risktolance_mainView!.heightConstaint?.constant = 60
            }
        }else {
            //  investmentpersonaButton_view.isHidden = true
            //tabBarController?.tabBar.items![1].isEnabled = true
            // guestUser_view.isHidden = true
            Sample_Portfolio_View.isHidden = false
            var toats = getLocalizedString(key: "Learn about investing and finance", comment: "")
            self.view.makeToast(toats)
            
            showView_invesmentStrategy_View.isHidden  = true
            recalulateRishtolerance_view.isHidden = true
            //lineview_risktolerance.isHidden = true
            risktolance_mainView!.heightConstaint?.constant = 45
            showView_invesmentStrategy_View!.heightConstaint?.constant = 0
        }
        
        if self.dashboardHoldingsModel == nil{
            tabBarController?.tabBar.items![1].isEnabled = false
            self.initTimer()
        }else {
            if self.dashboardHoldingsModel.success == false {
                tabBarController?.tabBar.items![1].isEnabled = false
                self.initTimer()
            }
        }
        
    }
    override func viewDidAppear(_ animated: Bool) {
        localization()
        SlideButtonView.addSubview(slideToOpen)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        invalidateTimer()
    }
    
    // MARK: - AV Player actions
    func playBgIntroVideo() {
        guard let videoPath = Bundle.main.path(forResource: "vesgo-first-run-intro-withoverlay", ofType:"mp4") else {
            print("error return")
            return
        }
        player = AVPlayer(url: NSURL(fileURLWithPath: videoPath) as URL)
        let playerLayer = AVPlayerLayer(player: player)
        playerLayer.frame = self.guestUser_view.frame
//        playerLayer.frame = self.showView_invesmentStrategy_View.frame
        playerLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        self.guestUser_view.layer.insertSublayer(playerLayer, at: 0)//(playerLayer)
        //self.showView_invesmentStrategy_View.layer.insertSublayer(playerLayer, at: 0)
        player.play()
        player.isMuted = true
        NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime, object: self.player.currentItem, queue: .main)
        { [weak self] _ in
            self?.player.seek(to: CMTime.zero)
            self?.player.play()
            self?.looppVideo(player: self!.player)
        }
    }
    func looppVideo(player : AVPlayer) {
        player.play()
        
    }

    // MARK: - Localization
    func localization(){
        let emilyDescripation = getLocalizedString(key: "emilydes", comment: "")
        let jasonDescripation = getLocalizedString(key: "jasondes", comment: "")
        let calebDescripation = getLocalizedString(key: "calebdes", comment: "")
        let esraDescription = getLocalizedString(key: "esra_desc")
        let toplblGuestuserDescripation = getLocalizedString(key: "topdeslbl", comment: "")
        let exploreDescripation = getLocalizedString(key: "exploredeslbl", comment: "")
        Description_emiy_lbl.text = emilyDescripation
        description_Jason_lbl.text = jasonDescripation
        descripation_caleb_lbl.text = calebDescripation
        description_esra_label.text = esraDescription
        descripation_top_lbl_guestUser.text = toplblGuestuserDescripation
        decripation_explore_lbl.text = exploreDescripation
        vesgoLeaderboard_guestUser_btn.setTitle(getLocalizedString(key: "Vesgo leaderboard", comment: ""), for: .normal)
        
        portfolioReturn.text = getLocalizedString(key: "portfolio_return", comment: "")
        pastDayLabel.text = getLocalizedString(key: "past_day", comment: "")
        portfolioValue.text = getLocalizedString(key: "portfolio_value", comment: "")
        topHoldingsLabel.text = getLocalizedString(key: "top_holdings", comment: "")
        chartiyHeadingLabel.text = getLocalizedString(key: "charity_heading_dashboard", comment: "")
        budgetingHeadingLabel.text = getLocalizedString(key: "budgeting_heading_dashboard", comment: "")
        returnNotAvailableLabel.text = getLocalizedString(key: "return_not_available", comment: "")
        portfolioIsGeneratingLabel.text = getLocalizedString(key: "portfolio_is_generating", comment: "")
    }
    
    func mtSlideToOpenDelegateDidFinish(_ sender: MTSlideToOpenView) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "Main_login_page") as UIViewController
        backpostion = "invesment"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func guestuserr(){
        if UserDefaults.standard.value(forKey: "guestuser" ) != nil{
            let value = UserDefaults.standard.value(forKey: "guestuser" )
            guestuser = value as! String
            print(guestuser)
            if guestuser == "aqib"{
                guestUser_view.isHidden = true
                tabBarController?.tabBar.items![1].isEnabled = true
            }else{
                guestUser_view.isHidden = false
                invesment_strategy_GuestUser_btn.isHidden = true
                playBgIntroVideo()
                
                tabBarController?.tabBar.items![1].isEnabled = false
                
            }
        }else{
            guestUser_view.isHidden = false
            invesment_strategy_GuestUser_btn.isHidden = true
            playBgIntroVideo()
            tabBarController?.tabBar.items![1].isEnabled = false
        }
    }
    
    //MARK: -IB Actions
    @IBAction func Emily_SamplePortfolio_pressed(_ sender: Any) {
        
        sample_portfolio_video_play_View.isHidden = false
        videoUrlll = "Vesgo-Emily-Profile"
        playIntroVideoSamplePortfolio()
        
    }
    
    @IBAction func Caleb_SamplePortfolio_Pressed(_ sender: Any) {
        sample_portfolio_video_play_View.isHidden = false
        videoUrlll = "Vesgo-Calebs-Profile"
        playIntroVideoSamplePortfolio()
    }
    
    @IBAction func Jason_SamplePortfolio_pressed(_ sender: Any) {
        sample_portfolio_video_play_View.isHidden = false
        videoUrlll = "Vesgo-jasons-Profile"
        playIntroVideoSamplePortfolio()
    }
    
    @IBAction func Esra_SamplePortfolio_pressed(_ sender: Any) {
        
        sample_portfolio_video_play_View.isHidden = false
        videoUrlll = "Vesgo-Esra-Profile"
        playIntroVideoSamplePortfolio()
        
        
        
    }
    
    @IBAction func vesgo_leaderboard_pressed(_ sender: Any) {
        tabBarController?.tabBar.items![1].isEnabled = true
        self.tabBarController?.selectedIndex = 1
        
    }
    @IBAction func invesmentStrategy_pressed(_ sender: Any) {
//        InvestmentStrategyVars.isFromAccountVC = true
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let vc = storyboard.instantiateViewController(withIdentifier: "HomeSurveyVC") as! HomeSurveyVC
//        self.navigationController?.pushViewController(vc, animated: true)
        self.getQuestionnaireStatusAPI()
        
    }
    @IBAction func modelportfolio_pressed(_ sender: Any) {
        if let tabbedbar = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "nav_bar_sampleportfolio") as? UINavigationController {
            UIApplication.shared.keyWindow?.rootViewController = tabbedbar
        }
        
    }
    
    @IBAction func accountsetup(_ sender: Any) {
        Sample_Portfolio_View.isHidden = true
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "Main_login_page") as UIViewController
        UIApplication.shared.keyWindow?.rootViewController = vc
    }
    
    @IBAction func invesmentpersona_pressed(_ sender: Any) {
        var date  = ""
        
        
        if SPManager.sharedInstance.saveUserLoginStatus() {
            if UserDefaults.standard.object(forKey: "\(SPManager.sharedInstance.getCurrentUserEmail())date") != nil {
                
                date = UserDefaults.standard.string(forKey: "\(SPManager.sharedInstance.getCurrentUserEmail())date")!
                if date != nil {
                    InvestmentStrategyVars.isFromAccountVC = true
                    let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CategorySummaryVC") as! CategorySummaryVC
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                } else {
                    date =  ""
                    // InvestmentStrategy_mesg_lbl.isHidden = true
                }
            } else {
                // key doesn't exist in defaults, use default value
                date = ""
                InvestmentStrategyVars.isFromAccountVC = true
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CategorySummaryVC") as! CategorySummaryVC
                self.navigationController?.pushViewController(vc, animated: true)
                
            }
        }else{
            InvestmentStrategyVars.isFromAccountVC = true
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CategorySummaryVC") as! CategorySummaryVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
    @IBAction func recalculaterisk_pressed(_ sender: Any) {
        InvestmentStrategyVars.isFromAccountVC = true
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "HomeSurveyVC") as! HomeSurveyVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func portfoliouserkeyfacevalues_pressed(_ sender: Any) {
        //self.tabBarController?.selectedIndex = 2
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "NotificationsVC") as! NotificationsVC
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    // MARK: - IMG Acction
    @IBAction func Edit_img_btn(_ sender: Any) {
        let image = UIImagePickerController()
        image.delegate = self
        image.sourceType = UIImagePickerController.SourceType.photoLibrary
        image.allowsEditing = false
        self.present(image, animated: true)
        {
            //After it is complete
        }
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        {
            UserDefaults.standard.set("\(image)" , forKey: "\(SPManager.sharedInstance.getCurrentUserEmail())img")
            Profile_img.image = image
        }
        else{
            //
        }
        self.dismiss(animated: true, completion: nil)
        do {
            //try //context.Save()
        } catch {
            print("Could not save. \(error), \(error.localizedDescription)")
        }
    }
    /*
     // MARK: - Navigation
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    // MARK: - floatybutton acction
    func floatybutton() {
        if SPManager.sharedInstance.saveUserLoginStatus() {
            floaty_btn.isHidden = true
        }else{
            floaty_btn.buttonColor = #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1)
            floaty_btn.itemButtonColor = #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1)
            floaty_btn.buttonImage = UIImage(named: "floatyicon")
            floaty_btn.addItem("Sample Portfolios",icon: UIImage(named: "services-portfolio1")!, handler: { item in
                if let tabbedbar = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "nav_bar_sampleportfolio") as? UINavigationController {
                    UIApplication.shared.keyWindow?.rootViewController = tabbedbar
                }
            })
            floaty_btn.addItem("Account Setup",icon: UIImage(named: "baseline_person_add_black")!, handler: { item in
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "Main_login_page") as UIViewController
                UIApplication.shared.keyWindow?.rootViewController = vc
            })
            DispatchQueue.main.async {
                self.view.addSubview(self.floaty_btn)
                
            }
        }
    }
    
    // Mark:- Call Dashboard API's
    
    func callDashboardChartAPI(){
        if !SPManager.sharedInstance.saveUserLoginStatus() {
            return
        }
        
//        DispatchQueue.main.async {
//            self.view.activityStartAnimating()
//        }
        let url = ApiUrl.GET_DASHBOARD_PORTFOLIO
        let namespace = SPManager.sharedInstance.getCurrentUserNamespace()
        let params:Dictionary<String,AnyObject> = ["namespace" : namespace  as AnyObject]
        print(params)
        
        postManagerUser(method: "POST", isThisURLEmbdedData: false, urlString: url, parameter: ["namespace": namespace], success: { [self](value) in
            
            do {
                self.dashboardChartModel = try JSONDecoder().decode(DashboardChartModel?.self, from: value)! as DashboardChartModel
                
                if self.dashboardChartModel.success {
                    DispatchQueue.main.async { [self] in
                        dataNotAvailableView.isHidden = true
                        mainDashboardView.isHidden = false
                        
                        portfolioIsGeneratingView.isHidden = true
                        tabBarController?.tabBar.items![1].isEnabled = true
                        populateDashboardChartViewData()
                        
                        loadingAnimation(play: false)
                        
                        returnDataAvailableView.isHidden = false
                        returnDataNotAvailableView.isHidden = true
                    }
                }else {
                    DispatchQueue.main.async { [self] in
                        self.portfolioIsGeneratingView.isHidden = false
                        tabBarController?.tabBar.items![1].isEnabled = false
                        
                        loadingAnimation(play: true)
                        
                        returnDataAvailableView.isHidden = false
                        returnDataNotAvailableView.isHidden = true
                    }
                }
                
                
            }
            catch {
                DispatchQueue.main.async { [self] in
                    returnDataNotAvailableView.isHidden = false
                    returnDataAvailableView.isHidden = true
                }
            }
    
//            DispatchQueue.main.async {
//                self.view.activityStopAnimating()
//            }
        }, failure: {(err) in
            DispatchQueue.main.async {
                self.portfolioIsGeneratingView.isHidden = false
                self.tabBarController?.tabBar.items![1].isEnabled = false
                //self.view.activityStopAnimating()
                self.returnDataAvailableView.isHidden = false
                self.returnDataNotAvailableView.isHidden = true
                
                self.loadingAnimation(play: true)
            }
            if APIManager.sharedInstance.error400 {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
            }
            if(APIManager.sharedInstance.error401)
            {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
            }else{
                self.showAlert(ConstantStrings.Error_msg_generic)
            }
        })
    }
    
    private func populateDashboardChartViewData(){
        if dashboardChartModel.data!.dataReturn.contains("+") || dashboardChartModel.data!.dataReturn.contains("-") ||
            dashboardChartModel.data!.dataReturn.contains("%"){
            portfolioReturnLabel.text = dashboardChartModel.data?.dataReturn
            portfolioValueLabel.text = "$" + FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: dashboardChartModel.data!.amount)
            
            setChart(chartView: lineChart_view, isLineChartForHoldings: false, arrayPosIndex: 0)
        }
        
        portfolioNewsLabel.text = dashboardChartModel.data!.userKeyFace
    }
    
    func callDashboardHoldingsAPI(){
        if !SPManager.sharedInstance.saveUserLoginStatus() {
            return
        }
        
//        DispatchQueue.main.async {
//            self.view.activityStartAnimating()
//        }
        let url = ApiUrl.GET_DASHBOARD_HOLDINGS
        let namespace = SPManager.sharedInstance.getCurrentUserNamespace()
        let params:Dictionary<String,AnyObject> = ["namespace" : namespace  as AnyObject]
        print(params)
        
        postManagerUser(method: "POST", isThisURLEmbdedData: false, urlString: url, parameter: ["namespace": namespace], success: { [self](value) in
            
            do {
                self.dashboardHoldingsModel = try JSONDecoder().decode(DashboardHoldingsModel?.self, from: value)! as DashboardHoldingsModel
                
                if self.dashboardHoldingsModel.success {
                    DispatchQueue.main.async { [self] in
                        dataNotAvailableView.isHidden = true
                        mainDashboardView.isHidden = false
                        holdingsCollectionView.reloadData()
                        //dashboardTableView.reloadData()
                        
                        portfolioIsGeneratingView.isHidden = true
                        tabBarController?.tabBar.items![1].isEnabled = true
                        
                        self.invalidateTimer()
                        
                        loadingAnimation(play: false)
                    }
                }else {
                    DispatchQueue.main.async {
                        self.initTimer()
                        self.portfolioIsGeneratingView.isHidden = false
                        tabBarController?.tabBar.items![1].isEnabled = false
                        
                        self.loadingAnimation(play: true)
                    }
                }
                
            }
            catch {
                
            }
            
            
//            DispatchQueue.main.async {
//                self.view.activityStopAnimating()
//            }
            
        }, failure: {(err) in
            DispatchQueue.main.async {
                self.initTimer()
                self.portfolioIsGeneratingView.isHidden = false
                self.tabBarController?.tabBar.items![1].isEnabled = false
                //self.view.activityStopAnimating()
                
                self.loadingAnimation(play: true)
            }
            if APIManager.sharedInstance.error400 {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
            }
            if(APIManager.sharedInstance.error401)
            {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
            }else{
                self.showAlert(ConstantStrings.Error_msg_generic)
            }
        })
    }
    
    func callDashboardCharityAPI(){
        if !SPManager.sharedInstance.saveUserLoginStatus() {
            return
        }
        
//        DispatchQueue.main.async {
//            self.view.activityStartAnimating()
//        }
        let url = ApiUrl.GET_DASHBOARD_CHARITY
        let namespace = SPManager.sharedInstance.getCurrentUserNamespace()
        let params:Dictionary<String,AnyObject> = ["namespace" : namespace  as AnyObject]
        print(params)
        
        postManagerUser(method: "POST", isThisURLEmbdedData: false, urlString: url, parameter: ["namespace": namespace], success: { [self](value) in
            
            do {
                self.dashboardCharityModel = try JSONDecoder().decode(DashboardCharityModel?.self, from: value)! as DashboardCharityModel
                
                
                if self.dashboardCharityModel.success {
                    DispatchQueue.main.async { [self] in
                        dataNotAvailableView.isHidden = true
                        mainDashboardView.isHidden = false
                        populateCharityViewWithData()
                    }
                }
                
                
                
            }
            catch {
                
            }
//            DispatchQueue.main.async {
//                self.view.activityStopAnimating()
//            }
            
        }, failure: {(err) in
//            DispatchQueue.main.async {
//                self.view.activityStopAnimating()
//
//            }
            if APIManager.sharedInstance.error400 {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
            }
            if(APIManager.sharedInstance.error401)
            {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
            }else{
                self.showAlert(ConstantStrings.Error_msg_generic)
            }
        })
    }
    
    func populateCharityViewWithData(){
        if dashboardCharityModel.data.hasDonated {
            charityDescLabel.text = String(format: getLocalizedString(key: "you_are_a_hero"), FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: dashboardCharityModel.data.donatedAmount))
//            charityDescLabel.text =  "You are a hero. You have donated $\(FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: dashboardCharityModel.data.donatedAmount)) to charity. Keep this streak going and support your favorite charities"
            if dashboardCharityModel.data.plannedAmount == 0.0 {
                containerView.isHidden = true
            }else {
                containerView.isHidden = false
                populateMovementsRingCharts(
                    currentValue: Int(dashboardCharityModel.data.donatedAmount),
                    maxValue: Int(dashboardCharityModel.data.plannedAmount),
                    currentChartView: containerView, isOverSpent: false)
            }
            
            
        }else{
            containerView.isHidden = true
            charityDescLabel.text = getLocalizedString(key: "show_your_support")
            
        }
        
    }
    
    func callDashboardBudgetingAPI(){
        if !SPManager.sharedInstance.saveUserLoginStatus() {
            return
        }
        
//        DispatchQueue.main.async {
//            self.view.activityStartAnimating()
//        }
        let url = ApiUrl.GET_DASHBOARD_BUDGETING
        let namespace = SPManager.sharedInstance.getCurrentUserNamespace()
        let params:Dictionary<String,AnyObject> = ["namespace" : namespace  as AnyObject]
        print(params)
        
        postManagerUser(method: "POST", isThisURLEmbdedData: false, urlString: url, parameter: ["namespace": namespace], success: { [self](value) in
            
            do {
                self.dashboardBudgetingModel = try JSONDecoder().decode(DashboardBudgetingModel?.self, from: value)! as DashboardBudgetingModel
                
                DispatchQueue.main.async { [self] in
                    dataNotAvailableView.isHidden = true
                    mainDashboardView.isHidden = false
                    populateBudgetingViewWithData()
                }
                
                
                
            }
            catch {
                
            }
        }, failure: {(err) in
            if APIManager.sharedInstance.error400 {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
            }
            if(APIManager.sharedInstance.error401)
            {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
            }else{
                self.showAlert(ConstantStrings.Error_msg_generic)
            }
        })
    }
    
    private func populateBudgetingViewWithData(){
        if !dashboardBudgetingModel.data.isPlanExist {
            budgetingDescLabel.text = getLocalizedString(key: "take_control_of_spending")
            prograssbarview.isHidden = true
        }else {
            if dashboardBudgetingModel.data.amountSpent <= dashboardBudgetingModel.data.plannedAmount {
//                budgetingDescLabel.text = "Your spending plan from \(FintechUtils.sharedInstance.getDateInUSLocale(timestamp: dashboardBudgetingModel.data.plannedStartDate/1000)) to \(FintechUtils.sharedInstance.getDateInUSLocale(timestamp: dashboardBudgetingModel.data.plannedTargetDate/1000)). You are doing great, check your progress."
                
                budgetingDescLabel.text = String(format: getLocalizedString(key: "you_are_doing_great"),FintechUtils.sharedInstance.getDateInUSLocale(timestamp: dashboardBudgetingModel.data.plannedStartDate/1000),FintechUtils.sharedInstance.getDateInUSLocale(timestamp: dashboardBudgetingModel.data.plannedTargetDate/1000))
                
                prograssbarview.isHidden = false
                populateMovementsRingCharts(
                    currentValue: Int(dashboardBudgetingModel.data.amountSpent),
                    maxValue: Int(dashboardBudgetingModel.data.plannedAmount),
                    currentChartView: prograssbarview, isOverSpent: false)
            }else {
                budgetingDescLabel.text = String(format: getLocalizedString(key: "over_budget"), FintechUtils.sharedInstance.getDateInUSLocale(timestamp: dashboardBudgetingModel.data.plannedStartDate/1000), FintechUtils.sharedInstance.getDateInUSLocale(timestamp: dashboardBudgetingModel.data.plannedTargetDate/1000))
//                budgetingDescLabel.text = "Looks like you are over the budget.\nYour spending plan from \(FintechUtils.sharedInstance.getDateInUSLocale(timestamp: dashboardBudgetingModel.data.plannedStartDate/1000)) to \(FintechUtils.sharedInstance.getDateInUSLocale(timestamp: dashboardBudgetingModel.data.plannedTargetDate/1000))."
                
                
                
                prograssbarview.isHidden = false
                populateMovementsRingCharts(
                    currentValue: Int(dashboardBudgetingModel.data.amountSpent),
                    maxValue: Int(dashboardBudgetingModel.data.plannedAmount),
                    currentChartView: prograssbarview, isOverSpent: true)
            }
        }
        
        budgetingEducationBtn.isHidden = false
        budgetingEducationBtn.setTitle(getLocalizedString(key: "learn_about_financial_management"), for: .normal)
    }
    
    
    //Mark:- Ring Graph for charity and Budgeting
    public func populateMovementsRingCharts(currentValue : Int, maxValue : Int, currentChartView: RingGraphView, isOverSpent: Bool) {
            if #available(iOS 13.0, *) {
                var singleGraphMeter: [RingMeter]
                if isOverSpent {
                    singleGraphMeter = [RingMeter(title: "Completed", value: currentValue, maxValue: maxValue, colors: [#colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)],symbolProvider: ImageProvider(image: UIImage(named: "")), backgroundColor : #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1))]
                }else {
                    singleGraphMeter = [RingMeter(title: "Completed", value: currentValue, maxValue: maxValue, colors: [#colorLiteral(red: 0.262745098, green: 0.6274509804, blue: 0.2784313725, alpha: 1)],symbolProvider: ImageProvider(image: UIImage(named: "")), backgroundColor : #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1))]
                }

                if let graph = RingGraph(meters: singleGraphMeter) {
                    let viewFrame = currentChartView.frame
                    let graphFrame = CGRect(x: 0, y: 0, width: viewFrame.width, height: viewFrame.height)
                    let ringGraphView = RingGraphView(frame: graphFrame, graph: graph, preset: .None)
                    ringGraphView.backgroundColor = .clear
//                    ringGraphView.shadowRadius = 0
//                    ringGraphView.shadowColor = .clear
//                    ringGraphView.shadowOffset = CGSize(width: 0, height: 0)
//                    ringGraphView.shadowOpacity = 0
                    currentChartView.addSubview(ringGraphView)
                    currentChartView.backgroundColor = .clear
//                    currentChartView.shadowRadius = 0
//                    currentChartView.shadowColor = .clear
//                    currentChartView.shadowOffset = CGSize(width: 0, height: 0)
//                    currentChartView.shadowOpacity = 0
                }
            } else {
                // Fallback on earlier versions
            }
    }
    
    //Mark:- Get Budgeting Plan Existence API
    private func getBudgetingExistencePlanAPI(){
        if !SPManager.sharedInstance.saveUserLoginStatus() {
            return
        }
        let url = ApiUrl.CHECK_BUDGETING_PLAN_EXISTENCE
        let namespace = SPManager.sharedInstance.getCurrentUserNamespace()
        
        let params:Dictionary<String,AnyObject> = ["namespace" : namespace  as AnyObject]
        print(params)
        APIManager.sharedInstance.postRequest(serviceName: url, sendData: params, success: { (response) in
            print(response)
            
            if APIManager.sharedInstance.isSuccess {
                
                DispatchQueue.main.async { [self] in
                    self.isBudgetingPlanExist = response["success"] as! Int
                    secondTableview_portfoliocomposition.reloadData()
                }
            }
            
        }, failure: { (data) in
            print(data)
            if APIManager.sharedInstance.error400 {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
            }
            if(APIManager.sharedInstance.error401)
            {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
            }else{
                
            }
        })
        
    }
    
    //Mark: - Timer
    
    private func invalidateTimer(){
        if timer != nil {
            timer?.invalidate()
            timer = nil
        }
    }
    
    private func initTimer(){
        if SPManager.sharedInstance.getCurrentUserQuestionaireStatus() != "done" {
            return
        }
        if timer == nil {
            print("Timer is nil")
            timer =  Timer.scheduledTimer(withTimeInterval: 30*1, repeats: true) { (timer) in  //30 seconds
                print("Timer initialized")
                self.callDashboardChartAPI()
                self.callDashboardHoldingsAPI()
                
                
                if self.dashboardCharityModel == nil {
                    self.callDashboardCharityAPI()
                }
                if self.dashboardBudgetingModel == nil {
                    self.callDashboardBudgetingAPI()
                }
            }
        }
    }
    
    // MARK: - fetchData
    
    func fetchMetaData(res : [String : AnyObject], table_view : UITableView) {
        ArrayLists.sharedInstance.demographicsdashboarddatalist.removeAll()
        ArrayLists.sharedInstance.riskdashboarddatalist.removeAll()
        ArrayLists.sharedInstance.portfoliocompositiondashboarddatalist.removeAll()
        ArrayLists.sharedInstance.negativevaluesdatalist.removeAll()
        ArrayLists.sharedInstance.positivevaluesdatalist.removeAll()
        ArrayLists.sharedInstance.userinfodashboarddatalist.removeAll()
        
        let demographics = res["demographics"] as? [String : AnyObject]
        let risk = res["risk"] as? Double ?? 0.0
        Risk_lbl.text = String(risk)
        
        let portfoliocomposition = res["portfolio_composition"] as? [String : AnyObject]
        let userinfo = res["user_info"] as? [String : AnyObject]
        
        let esgScore = portfoliocomposition?["ESG"] as? [[String : AnyObject]]
        let dreivedValues = portfoliocomposition?["derived_values"] as? [[String : AnyObject]]
        let positivevalues = res["positive_values"] as? [String]
        let negtivevalues = res["negative_values"] as? [String]
        
        DispatchQueue.main.async {
            
            ArrayLists.sharedInstance.positivevaluesdatalist = self.parsepostiveList(data: positivevalues!)
            ArrayLists.sharedInstance.negativevaluesdatalist = self.parenegativeList(data: negtivevalues!)
            ArrayLists.sharedInstance.userinfodashboarddatalist = self.userinofo(data: [userinfo!])
        }
        DispatchQueue.main.async{
            
            ArrayLists.sharedInstance.portfoliocompositiondashboarddatalist += self.parseJsonList(data: esgScore!,isDerivedvalue: false )
            
        }
        DispatchQueue.main.async{
            ArrayLists.sharedInstance.portfoliocompositiondashboarddatalist += self.parseJsonList(data: dreivedValues!,isDerivedvalue: true )
        }
        
        DispatchQueue.main.async {
            self.secondTableview_portfoliocomposition.reloadData()
        }
    }
    func parseDemographicsDataList(data : [String : AnyObject]) -> [DashboardDataModel] {
        var list : [DashboardDataModel] = [DashboardDataModel]()
        let model = DashboardDataModel()
        let agegroup = data["age_group"] as? String ?? "aa"
        let gender = data["gender"] as? String ?? "s"
        let maritalstatus = data["marital_status"] as? String ?? "d"
        let score = data["score"] as? Double ?? 0.0
        let percentile = data["percentile"] as? Int ?? 0
        let value = data["value"] as? String ?? "d"
        let scoremean = data["score_mean"] as? String ?? ""
        let scorecalculation = data["score_calculation"] as? String ?? "d"
        gender_lbl.text = gender
        status.text = maritalstatus
        
        model.setagegrouptext(agegroup)
        model.setagegrouptext(gender)
        model.setmaritalstatus(maritalstatus)
        model.setvalue(value)
        model.setscoremeantext(scoremean)
        model.setscorecalculation(scorecalculation)
        list.append(model)
        return list
        
    }
    func parseriskDataList(data : [String : AnyObject]) -> [DashboardDataModel] {
        var list : [DashboardDataModel] = [DashboardDataModel]()
        let model = DashboardDataModel()
        let percentile = data["risk"] as? Double ?? 0.0
        precentile_lbl.text = String(percentile)
        list.append(model)
        return list
    }
    func parseJsonList(data : [[String : AnyObject]], isDerivedvalue : Bool) -> [DashboardDataModel] {
        var list : [DashboardDataModel] = [DashboardDataModel]()
        for jsonData in data {
            let model = DashboardDataModel()
            let agegroup = jsonData["age_group"] as? String ?? "aa"
            let gender = jsonData["gender"] as? String ?? "s"
            let maritalstatus = jsonData["marital_status"] as? String ?? "d"
            let score = jsonData["score"] as? Double ?? 0.0
            let percentile = jsonData["percentile"] as? Int ?? 0
            let value = jsonData["value"] as? String ?? "d"
            let scoremean = jsonData["score_mean"] as? String ?? ""
            let scorecalculation = jsonData["score_calculation"] as? String ?? ""
            if isDerivedvalue{
                if score > 0{
                    model.setscore("Some impact")
                }else{
                    model.setscore("No impact")
                }
            }else{
                model.setscore(String(describing: score))
            }
            model.setagegrouptext(agegroup)
            model.setagegrouptext(gender)
            model.setmaritalstatus(maritalstatus)
            model.setvalue(value)
            model.setscoremeantext(scoremean)
            model.setscorecalculation(scorecalculation)
            list.append(model)
        }
        return list
    }
    
    func userinofo(data : [[String : AnyObject]]) -> [DashboardDataModel] {
        var list : [DashboardDataModel] = [DashboardDataModel]()
        for jsonData in data {
            let model = DashboardDataModel()
            let userinfotext = jsonData["return"] as? String ?? "aa"
            let userkeyfacetext = jsonData["user_key_face"] as? String ?? ""
            let risk = jsonData["risk"] as? Double ?? 0.0
            let amount = jsonData["amount"] as? Double ?? 0.0
            print(amount)
            
            portfolio_userkeyfaceValues_btn.setTitle(userkeyfacetext, for: .normal)
            var aaa = Double(userinfotext)
            
            
            if (userinfotext.contains("-") || userinfotext.first == "0"){
                background_portfolio_Return_view.isHidden = true
                portfolioReturn_lbl.text = userinfotext
                portfolioReturn_lbl.textColor = UIColor.red
                portfolio_value_lbl.text = "$\(amount)"
                
            }else if (userinfotext.contains("+")){
                background_portfolio_Return_view.isHidden = true
                portfolioReturn_lbl.text = userinfotext
                portfolioReturn_lbl.textColor = #colorLiteral(red: 0, green: 0.5628422499, blue: 0.3188166618, alpha: 1)
                portfolio_value_lbl.text = "$\(amount)"
                
            }else{
                portfolio_lbl.isHidden = true
                value_default.isHidden = true
                portfolioReturn_lbl.isHidden = true
                pastMonth_lbl.isHidden = true
                portfolio_value_lbl.isHidden = true
                background_portfolio_Return_view.isHidden = false
                portfolio_return_Disable_lbl.text = "Return will be calculated after next trading day"
            }
        }
        return list
    }
    func riskinfo(data : [Double]) -> [Double] {
        var list  = [Double]()
        for i in 0..<data.count
        {
            list.append(data[i])
            print(data[i])
        }
        return list
    }
    func parenegativeList(data : [String]) -> [String] {
        var list  = [String]()
        for i in 0..<data.count
        {
            list.append(data[i])
            print(data[i])
        }
        return list
    }
    
    
    func parsepostiveList(data : [String]) -> [String] {
        var list  = [String]()
        for j in 0..<data.count
        {
            list.append(data[j])
        }
        return list
    }
    
    //Mark:- Dashboard LineChart
    func setChart(chartView: LineChartView, isLineChartForHoldings: Bool, arrayPosIndex: Int){
        var dataEntries: [ChartDataEntry] = []
        
        if isLineChartForHoldings {
            for i in 0..<dashboardHoldingsModel.data[arrayPosIndex].chartData.data.count {
                let dataEntry = ChartDataEntry(
                    x: Double(i),
                    y: dashboardHoldingsModel.data[arrayPosIndex].chartData.data[i].yAxis,
                    data: dashboardHoldingsModel.data[arrayPosIndex].chartData.data[i].xAxis as AnyObject)
                dataEntries.append(dataEntry)
            }
        }else{
            for i in 0..<dashboardChartModel.data!.chartData.data.count {
                let dataEntry = ChartDataEntry(
                    x: Double(i),
                    y: dashboardChartModel.data!.chartData.data[i].yAxis,
                    data: dashboardChartModel.data!.chartData.data[i].xAxis as AnyObject)
                dataEntries.append(dataEntry)
            }
        }
        
        
        let chartDataSet = LineChartDataSet(entries: dataEntries, label: nil)
//        chartDataSet.circleRadius = 0
//        chartDataSet.circleHoleRadius = 2
        chartDataSet.drawCirclesEnabled = false
        chartDataSet.drawValuesEnabled = false
        chartDataSet.highlightEnabled = false
        
        let chartData = LineChartData(dataSets: [chartDataSet])
        
        let yAxis = chartView.rightAxis
        let xAxis = chartView.topAnchor
        
        yAxis.enabled = false
        yAxis.drawAxisLineEnabled = false
        yAxis.drawGridLinesEnabled = false
    
        chartDataSet.lineWidth = 2.0
        if isLineChartForHoldings {
//            let holdingsChartData = dashboardHoldingsModel.data[arrayPosIndex].chartData.data
//            if holdingsChartData[0].yAxis > holdingsChartData[holdingsChartData.count - 1].yAxis {
//                chartDataSet.colors = [#colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)]
//            }else{
//                chartDataSet.colors = [#colorLiteral(red: 0.1960784346, green: 0.3411764801, blue: 0.1019607857, alpha: 1)]
//            }
            chartDataSet.colors = [#colorLiteral(red: 0.1960784346, green: 0.3411764801, blue: 0.1019607857, alpha: 1)]
            
        }else {
            if dashboardChartModel.data!.dataReturn.contains("-") {
                chartDataSet.colors = [#colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)]
                
            }else {
                chartDataSet.colors = [#colorLiteral(red: 0.1960784346, green: 0.3411764801, blue: 0.1019607857, alpha: 1)]
            }
        }
        
        chartView.data = chartData
        chartView.xAxis.valueFormatter = IndexAxisValueFormatter(values: months)
        chartView.xAxis.labelPosition = .bottom
        
        chartView.xAxis.drawGridLinesEnabled = false
        chartView.xAxis.drawAxisLineEnabled = false
        
        chartView.xAxis.avoidFirstLastClippingEnabled = true
        
        chartView.xAxis.drawLabelsEnabled = false
        
        chartView.rightAxis.drawAxisLineEnabled = false
        
        chartView.rightAxis.drawLabelsEnabled = false
        chartView.leftAxis.drawLabelsEnabled = false
        chartView.leftAxis.drawAxisLineEnabled = true
        chartView.pinchZoomEnabled = false
        chartView.doubleTapToZoomEnabled = false
        chartView.legend.enabled = false
        chartView.drawBordersEnabled = false
        
        
        chartView.backgroundColor = UIColor.clear
        
        
        chartView.drawGridBackgroundEnabled = false
        
        
        chartView.leftAxis.enabled = false
        chartView.scaleYEnabled = false
        chartView.scaleXEnabled = false
        
    }
    
}

extension invesmentPersonaVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if dashboardHoldingsModel == nil {
            return 0
        }else{
            return dashboardHoldingsModel.data.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : TopHoldingCell = collectionView.dequeueReusableCell(withReuseIdentifier: "TopHoldingCell", for: indexPath) as! TopHoldingCell
        
        cell.configureCell(dashboardHoldingsModel.data[indexPath.row])
        setChart(chartView: cell.lineChart, isLineChartForHoldings: true, arrayPosIndex: indexPath.row)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        holdingsBlurView.isHidden = false
        holdingsDialogParentView.isHidden = false
        holdingsDialogParentView.closeBtn.addTarget(self, action: #selector(holdingsDialogCloseBtnPressed), for: .touchUpInside)
        let tap = UITapGestureRecognizer(target: self, action: #selector(holdingsDialogCloseBtnPressed))
        holdingsBlurView.addGestureRecognizer(tap)
        
        holdingsDialogParentView.tickerName.text = dashboardHoldingsModel.data[indexPath.row].tickerName
        holdingsDialogParentView.tickerSymbol.text = dashboardHoldingsModel.data[indexPath.row].tickers
        if dashboardHoldingsModel.data[indexPath.row].tickerIndustry == "" {
            holdingsDialogParentView.industryLbl.text = "--"
        }else{
            holdingsDialogParentView.industryLbl.text = dashboardHoldingsModel.data[indexPath.row].tickerIndustry
        }
        holdingsDialogParentView.amountInvestedLbl.text = "$" + FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: dashboardHoldingsModel.data[indexPath.row].amountInvested)
        holdingsDialogParentView.returnLbl.text = dashboardHoldingsModel.data[indexPath.row].totalReturn
        holdingsDialogParentView.returnFreq.text = "(Past " + dashboardHoldingsModel.data[indexPath.row].returnFreq.lowercased() + ")"
        holdingsDialogParentView.portfolioWeightLbl.text = dashboardHoldingsModel.data[indexPath.row].portfolioShare
        
        holdingsDialogParentView.localization()
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = CGSize.init(width: self.holdingsCollectionView.frame.width/3, height: self.holdingsCollectionView.frame.height)
        
        print("Width: \(size.width),    Height: \(size.height)")
        
        return size
    }
}

extension invesmentPersonaVC: UITableViewDelegate,UITableViewDataSource{
    // MARK: - TableView Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        if tableView == dashboardTableView {
//            if dashboardHoldingsModel == nil {
//                return 0
//            }else{
//                return dashboardHoldingsModel.data.count
//            }
//        }else {
//            return 1 + ArrayLists.sharedInstance.positivevaluesdatalist.count + 1 + ArrayLists.sharedInstance.portfoliocompositiondashboarddatalist.count
//        }
        return 1 + ArrayLists.sharedInstance.positivevaluesdatalist.count + 1 + ArrayLists.sharedInstance.portfoliocompositiondashboarddatalist.count
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
//        if tableView == dashboardTableView {
//            let cell =  tableView.dequeueReusableCell(withIdentifier: "cell") as! DashboardTableViewCell
//            cell.heading_lbl.text = dashboardHoldingsModel.data[indexPath.row].tickerName
//
//
//            if indexPath.row == (dashboardHoldingsModel.data.count - 1) {
//                cell.line_view.isHidden = true
//            }else{
//                cell.line_view.isHidden = false
//
//            }
//
//            setChart(chartView: cell.chartView_line, isLineChartForHoldings: true, arrayPosIndex: indexPath.row)
//
//            return cell
//
//        }else {
            if indexPath.row < 1 ,let cell =  self.secondTableview_portfoliocomposition.dequeueReusableCell(withIdentifier: "cellHeadingvalue") as? invesmetPersonaTableViewCell {
                if SPManager.sharedInstance.saveUserLoginStatus() {
                    cell.Heading_lbl.text = "\(SPManager.sharedInstance.getCurrentUserNameForProfile())'s investment values"
                }else{
                    
                    if sample_username == "" {
                        cell.Heading_lbl.text = " investment values"
                        
                    }else {
                        cell.Heading_lbl.text = "\(sample_username)'s investment values"
                        
                    }
                }
                return cell
                
            } else if indexPath.row < (1 + ArrayLists.sharedInstance.positivevaluesdatalist.count),let cell =  self.secondTableview_portfoliocomposition.dequeueReusableCell(withIdentifier: "cell1Value") as? invesmetPersonaTableViewCell {
                let indexToFollow = (indexPath.row) - 1
                cell.heading_values_lbl.text = ArrayLists.sharedInstance.positivevaluesdatalist[(indexPath.row) - 1]
                if ArrayLists.sharedInstance.positivevaluesdatalist[indexToFollow] == "Vegan"{
                    cell.img_values_img.image = UIImage(named: "vegan")
                    cell.Img_values_detailes.image = UIImage(named: "care")
                }else if ArrayLists.sharedInstance.positivevaluesdatalist[indexToFollow] == "Animal welfare"{
                    cell.img_values_img.image = UIImage(named: "animal_welfare")
                    cell.Img_values_detailes.image = UIImage(named: "care")
                }else if ArrayLists.sharedInstance.positivevaluesdatalist[indexToFollow] == "Water"{
                    cell.img_values_img.image = UIImage(named: "water100x100")
                    cell.Img_values_detailes.image = UIImage(named: "care")//"angry")
                }else if ArrayLists.sharedInstance.positivevaluesdatalist[indexToFollow] == "Family values"{
                    cell.img_values_img.image = UIImage(named: "family_values")
                    cell.Img_values_detailes.image = UIImage(named: "care")//angry")
                }else if ArrayLists.sharedInstance.positivevaluesdatalist[indexToFollow] == "Coal"{
                    cell.img_values_img.image = UIImage(named: "Coal")
                    cell.Img_values_detailes.image = UIImage(named: "care")
                }else if ArrayLists.sharedInstance.positivevaluesdatalist[indexToFollow] == "Gambling"{
                    cell.img_values_img.image = UIImage(named: "Gambling")
                    cell.Img_values_detailes.image = UIImage(named: "care")
                }else if ArrayLists.sharedInstance.positivevaluesdatalist[indexToFollow] == "Nuclear power"{
                    cell.img_values_img.image = UIImage(named: "Nuclear_Power")
                    cell.Img_values_detailes.image = UIImage(named: "care")
                }else{
                    cell.Img_values_detailes.image = UIImage(named: "care")
                    
                }
                return cell
                
            }
            
            else if indexPath.row < (1 + ArrayLists.sharedInstance.positivevaluesdatalist.count + ArrayLists.sharedInstance.negativevaluesdatalist.count),let cell =  self.secondTableview_portfoliocomposition.dequeueReusableCell(withIdentifier: "cell1Value") as? invesmetPersonaTableViewCell {
                let anngryIcon = UIImage(named: "angry")
                let indexToFollow = (indexPath.row) - 1 - ArrayLists.sharedInstance.positivevaluesdatalist.count
                //            let index =
                cell.heading_values_lbl.text = ArrayLists.sharedInstance.negativevaluesdatalist[(indexPath.row) - 1 - ArrayLists.sharedInstance.positivevaluesdatalist.count]
                if ArrayLists.sharedInstance.positivevaluesdatalist[indexToFollow] == "Vegan"{
                    cell.img_values_img.image = UIImage(named: "vegan")
                    cell.Img_values_detailes.image = anngryIcon
                }else if ArrayLists.sharedInstance.negativevaluesdatalist[indexToFollow] == "Animal welfare"{
                    cell.img_values_img.image = UIImage(named: "animal_welfare")
                    cell.Img_values_detailes.image = anngryIcon
                }else if ArrayLists.sharedInstance.negativevaluesdatalist[indexToFollow] == "Water"{
                    cell.img_values_img.image = UIImage(named: "water100x100")
                    cell.Img_values_detailes.image = anngryIcon
                }else if ArrayLists.sharedInstance.negativevaluesdatalist[indexToFollow] == "Family values"{
                    cell.img_values_img.image = UIImage(named: "family_values")
                    cell.Img_values_detailes.image = anngryIcon
                }else if ArrayLists.sharedInstance.negativevaluesdatalist[indexToFollow] == "Coal"{
                    cell.img_values_img.image = UIImage(named: "Coal")
                    cell.Img_values_detailes.image = anngryIcon
                }else if ArrayLists.sharedInstance.negativevaluesdatalist[indexToFollow] == "Gambling"{
                    cell.img_values_img.image = UIImage(named: "Gambling")
                    cell.Img_values_detailes.image = anngryIcon
                }else if ArrayLists.sharedInstance.negativevaluesdatalist[indexToFollow] == "Nuclear power"{
                    cell.img_values_img.image = UIImage(named: "Nuclear_Power")
                    cell.Img_values_detailes.image = anngryIcon
                }else{
                    cell.Img_values_detailes.image = anngryIcon
                    
                }
                return cell
                
            }
            
            else if indexPath.row < (1 + ArrayLists.sharedInstance.positivevaluesdatalist.count + ArrayLists.sharedInstance.negativevaluesdatalist.count + 1),let cell =  self.secondTableview_portfoliocomposition.dequeueReusableCell(withIdentifier: "cellcharity") as? invesmetPersonaTableViewCell {
                
                cell.charity_heading_lbl.text = "Show your support"
                cell.descripation_charity_lbl.text = "Donate to charities"
                cell.img_charity_descripation.image = UIImage(named: "charity")
                
                return cell
                
                
            }
            
            else if indexPath.row < (1 + ArrayLists.sharedInstance.positivevaluesdatalist.count + ArrayLists.sharedInstance.negativevaluesdatalist.count + 1 + 1),let cell =  self.secondTableview_portfoliocomposition.dequeueReusableCell(withIdentifier: "cellcharity") as? invesmetPersonaTableViewCell {
                
                cell.charity_heading_lbl.text = "Spending plan"
                
                if self.isBudgetingPlanExist == 1 {
                    cell.descripation_charity_lbl.text = "My spending"
                }else {
                    cell.descripation_charity_lbl.text = "Start now"
                }
                
                
                cell.img_charity_descripation.image = UIImage(named: "budgeting_colored")
                
                return cell
                
                
            }
            
            
            else if indexPath.row < (1 + ArrayLists.sharedInstance.positivevaluesdatalist.count + ArrayLists.sharedInstance.negativevaluesdatalist.count  + 1 + 1 + 1),let cell =  self.secondTableview_portfoliocomposition.dequeueReusableCell(withIdentifier: "cell2HeadingImpact") as? invesmetPersonaTableViewCell {
                
                if SPManager.sharedInstance.saveUserLoginStatus() {
                    
                    cell.heading_main_Impact_lbl.text = "\(SPManager.sharedInstance.getCurrentUserNameForProfile())'s investment impact"
                }else{
                    
                    if sample_username == "" {
                        cell.heading_main_Impact_lbl.text = "investment impact"
                        
                    }else {
                        cell.heading_main_Impact_lbl.text = "\(sample_username)'s investment impact"
                        
                    }
                }
                
                
                return cell
                
            }else if indexPath.row < (1 +  ArrayLists.sharedInstance.positivevaluesdatalist.count + ArrayLists.sharedInstance.negativevaluesdatalist.count  + 1 + 1 + 1 +  ArrayLists.sharedInstance.portfoliocompositiondashboarddatalist.count),let cell =  self.secondTableview_portfoliocomposition.dequeueReusableCell(withIdentifier: "cell3invesmentImpact") as? invesmetPersonaTableViewCell {
                let item = ArrayLists.sharedInstance.portfoliocompositiondashboarddatalist[(indexPath.row) - 1 - 1 -  ArrayLists.sharedInstance.positivevaluesdatalist.count - ArrayLists.sharedInstance.negativevaluesdatalist.count - 1]
                cell.heading1_portfoliocomposition_lbl.text = item.getvalue()
                
                //item.getvalue()
                print(item.getvalue())
                if item.getvalue() == "Social"{
                    cell.img_portfoliocomposition_img.image = UIImage(named: "social")
                    cell.details1_portfoliocomposition_lbl.text = String(item.getscore())
                }else if item.getvalue() == "Environment"{
                    cell.img_portfoliocomposition_img.image = UIImage(named: "environment")
                    cell.details1_portfoliocomposition_lbl.text = String(item.getscore())
                }else if item.getvalue() == "Vegan"{
                    cell.img_portfoliocomposition_img.image = UIImage(named: "vegan")
                    //cell.details1_portfoliocomposition_lbl.text = String(item.getscore())
                }else if item.getvalue() == "Governance"{
                    cell.img_portfoliocomposition_img.image = UIImage(named: "governance")
                    cell.details1_portfoliocomposition_lbl.text = String(item.getscore())
                }else if item.getvalue() == "Child labor"{
                    cell.img_portfoliocomposition_img.image = UIImage(named: "child lb")
                    cell.details1_portfoliocomposition_lbl.text = String(item.getscore())
                    
                    
                }else if item.getvalue() == "Palm oil" {
                    cell.img_portfoliocomposition_img.image = UIImage(named: "palm oil")
                    
                }else if item.getvalue() == "Firearms" {
                    cell.img_portfoliocomposition_img.image = UIImage(named: "firearms")
                    
                }else if item.getvalue() == "Gmo" {
                    cell.img_portfoliocomposition_img.image = UIImage(named: "GMO")
                    
                }else if item.getvalue() == "Animal welfare" {
                    cell.img_portfoliocomposition_img.image = UIImage(named: "animal_welfare")
                    
                }else if item.getvalue() == "Controversial weapons" {
                    cell.img_portfoliocomposition_img.image = UIImage(named: "controversial_weapons")
                    
                }else if item.getvalue() == "Military Contract" {
                    cell.img_portfoliocomposition_img.image = UIImage(named: "military_contract")
                    
                }else if item.getvalue() == "Climate change" {
                    cell.img_portfoliocomposition_img.image = UIImage(named: "climate_change")
                    
                }else if item.getvalue() == "Alignment with UN SDGs" {
                    cell.img_portfoliocomposition_img.image = UIImage(named: "alignment_UN")
                    
                }else if item.getvalue() == "Labor rights" {
                    cell.img_portfoliocomposition_img.image = UIImage(named: "child lb")
                    
                }else if item.getvalue() == "Diversity" {
                    cell.img_portfoliocomposition_img.image = UIImage(named: "diversity")
                    
                }else if item.getvalue() == "Civil liberties" {
                    cell.img_portfoliocomposition_img.image = UIImage(named: "civil_liberties")
                    
                }else if item.getvalue() == "Water" {
                    cell.img_portfoliocomposition_img.image = UIImage(named: "water100x100")
                    
                }else if item.getvalue() == "Wind power - positive" {
                    cell.img_portfoliocomposition_img.image = UIImage(named: "wind_power")
                    
                }else if item.getvalue() == "Carbon emissions" {
                    cell.img_portfoliocomposition_img.image = UIImage(named: "carbon_emission")
                    
                }else if item.getvalue() == "Stem cell research" {
                    cell.img_portfoliocomposition_img.image = UIImage(named: "stem_cell_research")
                    
                }else if item.getvalue() == "Alcohol" {
                    cell.img_portfoliocomposition_img.image = UIImage(named: "Alcohol-1")
                    
                }else if item.getvalue() == "Adult entertainment" {
                    // cell.img_portfoliocomposition_img.image = UIImage(named: "environment")
                    
                }else if item.getvalue() == "Contraceptives" {
                    cell.img_portfoliocomposition_img.image = UIImage(named: "contraceptives")
                    
                }else if item.getvalue() == "Abortion" {
                    cell.img_portfoliocomposition_img.image = UIImage(named: "Abortion")
                    
                }else if item.getvalue() == "Maternity leave (rights)" {
                    cell.img_portfoliocomposition_img.image = UIImage(named: "maternity_leaves")
                    
                }else if item.getvalue() == "Family values" {
                    cell.img_portfoliocomposition_img.image = UIImage(named: "family_values")
                    
                }else if item.getvalue() == "Immigration" {
                    cell.img_portfoliocomposition_img.image = UIImage(named: "Immigration")
                    
                }else if item.getvalue() == "LGBTQ" {
                    cell.img_portfoliocomposition_img.image = UIImage(named: "LGBTQ")
                    
                }else if item.getvalue() == "Gender equity" {
                    cell.img_portfoliocomposition_img.image = UIImage(named: "gender_equality")
                    
                }else if item.getvalue() == "Minority owned" {
                    cell.img_portfoliocomposition_img.image = UIImage(named: "minority_owned")
                    
                }else if item.getvalue() == "Interest bearing instruments" {
                    cell.img_portfoliocomposition_img.image = UIImage(named: "interest_bearing_instruments")
                    
                }else if item.getvalue() == "Coal" {
                    cell.img_portfoliocomposition_img.image = UIImage(named: "Coal")
                }else if item.getvalue() == "Nuclear power" {
                    cell.img_portfoliocomposition_img.image = UIImage(named: "Nuclear_Power")
                }else if item.getvalue() == "Pesticides" {
                    cell.img_portfoliocomposition_img.image = UIImage(named: "Pesticides")
                }else if item.getvalue() == "Tobacco" {
                    cell.img_portfoliocomposition_img.image = UIImage(named: "Tobacco")
                }
                else if item.getvalue() == "Fur leather" {
                    cell.img_portfoliocomposition_img.image = UIImage(named: "fur_leather")
                }
                else if item.getvalue() == "Gambling" {
                    cell.img_portfoliocomposition_img.image = UIImage(named: "Gambling")
                }else if item.getvalue() == "Clean energy" {
                    cell.img_portfoliocomposition_img.image = UIImage(named: "green_energy")
                }
                else {
                    cell.img_portfoliocomposition_img.image = UIImage(named: "environment")
                }
                cell.details1_portfoliocomposition_lbl.text = item.getscore()
                
                
                return cell
            }
        //}
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == firstTableView_values{
            print("first tableview selected == \(indexPath.row)")
        }else if tableView == secondTableview_portfoliocomposition {
            let charitybtn = 1 + ArrayLists.sharedInstance.positivevaluesdatalist.count + ArrayLists.sharedInstance.negativevaluesdatalist.count
            let spendingPlanBtn = 1 + ArrayLists.sharedInstance.positivevaluesdatalist.count + ArrayLists.sharedInstance.negativevaluesdatalist.count + 1
            let previousCount = 1 + ArrayLists.sharedInstance.positivevaluesdatalist.count + ArrayLists.sharedInstance.negativevaluesdatalist.count + 1 + 1
            if indexPath.row == charitybtn {
                
                let storyboard = UIStoryboard(name: "Charity", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "CharityDashboardVC") as UIViewController
                self.navigationController?.pushViewController(vc, animated: true)
                
            }
            if indexPath.row == spendingPlanBtn {
                
                if self.isBudgetingPlanExist == 1 {
                    let storyboard = UIStoryboard(name: "Budgeting", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "BudgetingDashboard") as UIViewController
                    self.navigationController?.pushViewController(vc, animated: true)
                }else{
                    let storyboard = UIStoryboard(name: "Budgeting", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "BudgetingPlan") as UIViewController
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                
                
                
            }
            
            
            if indexPath.row >= previousCount {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "personaValuesDetailsVC") as! personaValuesDetailsVC
                let item = ArrayLists.sharedInstance.portfoliocompositiondashboarddatalist[(indexPath.row) - 1 - ArrayLists.sharedInstance.positivevaluesdatalist.count - 2 - ArrayLists.sharedInstance.negativevaluesdatalist.count]
                detailsname = item.getvalue()
                if item.getvalue() == "Social"{
                    imgname = "social"
                }else if item.getvalue() == "Environment"{
                    imgname = "environment"
                    
                }else if item.getvalue() == "Vegan"{
                    imgname = "vegan"
                    
                }else if item.getvalue() == "Governance"{
                    imgname = "governance"
                }else if item.getvalue() == "Child labor"{
                    imgname = "child lb"
                    
                }else if item.getvalue() == "Palm oil" {
                    imgname = "palm oil"
                    
                }else if item.getvalue() == "Firearms" {
                    imgname = "firearms"
                }else if item.getvalue() == "Gmo" {
                    imgname = "GMO"
                    
                }else if item.getvalue() == "Animal welfare" {
                    imgname = "animal_welfare"
                }else if item.getvalue() == "Controversial weapons" {
                    imgname = "controversial_weapons"
                }else if item.getvalue() == "Military Contract" {
                    imgname = "military_contract"
                }else if item.getvalue() == "Climate change" {
                    imgname = "climate_change"
                    
                }else if item.getvalue() == "Alignment with UN SDGs" {
                    imgname = "alignment_UN"
                    
                }else if item.getvalue() == "Labor rights" {
                    imgname =  "child lb"
                    
                }else if item.getvalue() == "Diversity" {
                    imgname =  "diversity"
                    
                }else if item.getvalue() == "Civil liberties" {
                    imgname =  "civil_liberties"
                    
                }else if item.getvalue() == "Water" {
                    imgname =  "water100x100"
                    
                }else if item.getvalue() == "Wind power - positive" {
                    imgname = "wind_power"
                    
                }else if item.getvalue() == "Carbon emissions" {
                    imgname =  "carbon_emission"
                    
                }else if item.getvalue() == "Stem cell research" {
                    imgname = "stem_cell_research"
                    
                }else if item.getvalue() == "Clean energy" {
                    imgname =  "environment"
                    
                }else if item.getvalue() == "Alcohol" {
                    imgname = "Alcohol-1"
                    
                }else if item.getvalue() == "Adult entertainment" {
                    // cell.img_portfoliocomposition_img.image = UIImage(named: "environment")
                    
                }else if item.getvalue() == "Contraceptives" {
                    imgname = "contraceptives"
                    
                }else if item.getvalue() == "Abortion" {
                    imgname =  "Abortion"
                    
                }else if item.getvalue() == "Maternity leave (rights)" {
                    imgname =  "maternity_leaves"
                    
                }else if item.getvalue() == "Family values" {
                    imgname = "family_values"
                    
                }else if item.getvalue() == "Immigration" {
                    imgname = "Immigration"
                    
                }else if item.getvalue() == "LGBTQ" {
                    imgname = "LGBTQ"
                    
                }else if item.getvalue() == "Gender equity" {
                    imgname =  "gender_equality"
                    
                }else if item.getvalue() == "Millennial owned" {
                    // cell.img_portfoliocomposition_img.image = UIImage(named: "minority_owned")
                    
                }else if item.getvalue() == "Minority owned" {
                    imgname =  "minority_owned"
                    
                }else if item.getvalue() == "Interest bearing instruments" {
                    imgname =  "interest_bearing_instruments"
                    
                }else if item.getvalue() == "Coal" {
                    imgname =  "Coal"
                }
                else {
                    imgname = "environment"
                    
                }
                
                
                
                navigationController?.pushViewController(vc, animated: true)
                
            }
        }
//        else if tableView == dashboardTableView {
//            //Holdings TableView
//            
//            holdingsBlurView.isHidden = false
//            holdingsDialogParentView.isHidden = false
//            holdingsDialogParentView.closeBtn.addTarget(self, action: #selector(holdingsDialogCloseBtnPressed), for: .touchUpInside)
//            let tap = UITapGestureRecognizer(target: self, action: #selector(holdingsDialogCloseBtnPressed))
//            holdingsBlurView.addGestureRecognizer(tap)
//
//            holdingsDialogParentView.tickerName.text = dashboardHoldingsModel.data[indexPath.row].tickerName
//            holdingsDialogParentView.tickerSymbol.text = dashboardHoldingsModel.data[indexPath.row].tickers
//            if dashboardHoldingsModel.data[indexPath.row].tickerIndustry == "" {
//                holdingsDialogParentView.industryLbl.text = "--"
//            }else{
//                holdingsDialogParentView.industryLbl.text = dashboardHoldingsModel.data[indexPath.row].tickerIndustry
//            }
//            holdingsDialogParentView.amountInvestedLbl.text = "$" + FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: dashboardHoldingsModel.data[indexPath.row].amountInvested)
//            holdingsDialogParentView.returnLbl.text = dashboardHoldingsModel.data[indexPath.row].totalReturn
//            holdingsDialogParentView.returnFreq.text = "(Past " + dashboardHoldingsModel.data[indexPath.row].returnFreq.lowercased() + ")"
//            holdingsDialogParentView.portfolioWeightLbl.text = dashboardHoldingsModel.data[indexPath.row].portfolioShare
//
//            holdingsDialogParentView.localization()
//        }
        
    }
    
    @objc private func holdingsDialogCloseBtnPressed(){
        holdingsBlurView.isHidden = true
        holdingsDialogParentView.isHidden = true
    }
    
}
extension invesmentPersonaVC{
    func playIntroVideoSamplePortfolio() {
        let myFileManager = FileManager.default
        let mainBundle = Bundle.main
        let resourcesPath = mainBundle.resourcePath!
        guard let allItemsInTheBundle = try? myFileManager.contentsOfDirectory(atPath: resourcesPath) else {
            return
        }
        
        let videoName = videoUrlll
        let videoPath = Bundle.main.path(forResource: videoName, ofType: "m4v")
        let videoUrl = URL(fileURLWithPath: videoPath!)
        //        player = AVPlayer(url: NSURL(fileURLWithPath: videoPath) as URL)
        let player = AVPlayer(url: videoUrl)
        let controller=AVPlayerViewController()
        controller.player=player
        
        let playerLayer = AVPlayerLayer(player: self.player)
        playerLayer.videoGravity = AVLayerVideoGravity.resizeAspect;
        
        controller.showsPlaybackControls = false
        controller.view.frame = self.view.frame
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(playerItemDidReachEnd),
                                               name: NSNotification.Name.AVPlayerItemDidPlayToEndTime,
                                               object: player.currentItem) // Add observer
        self.tabBarController?.tabBar.layer.zPosition = -1
        self.sample_portfolio_video_play_View.addSubview(controller.view)
        
        self.addChild(controller)
        player.play()
    }
    @objc func playerItemDidReachEnd(notification: NSNotification) {
        print("playerItemDidReachEnd")
        player.pause()
        self.tabBarController?.tabBar.layer.zPosition = 0
        self.sample_portfolio_video_play_View.isHidden = true
        NotificationCenter.default.removeObserver(self)
        
    }
}
