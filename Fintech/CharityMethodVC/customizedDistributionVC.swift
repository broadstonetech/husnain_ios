//
//  customizedDistributionVC.swift
//  Fintech
//
//  Created by Apple on 23/12/2020.
//  Copyright © 2020 Broadstone Technologies. All rights reserved.
//

import UIKit


var pickerData: [Int] = [Int]()
var persentageAmountArray = [Double]()
var currentIndex = -1

class customizedDistributionVC: PaymentsBaseClass,UIPickerViewDelegate, UIPickerViewDataSource {
    var amountToBeDonated = 0.0
    var isFeesIncluded: Bool!
    var pickerDataRowSelectedIndex: Int =  0
    var checked = false
    
    var donatedIndex = [Bool]()
    
    @IBOutlet weak var tableview_customized_distribution: UITableView!
    @IBOutlet weak var view_picker: UIView!
    @IBOutlet weak var UIpickerView: UIPickerView!
    
    @IBOutlet weak var cancel_btn: UIButton!
    @IBOutlet weak var done_btn: UIButton!
//    @IBOutlet weak var heading_amount_lbl: UILabel!
//    @IBOutlet weak var amount_donate_lbl: UILabel!
    
    @IBOutlet weak var amountField: UITextField!
    @IBOutlet weak var include_transaction_fee_btn: UIButton!
    @IBOutlet weak var main_heading_customized_distribution_lbl: UILabel!
    
    @IBOutlet weak var continue_btn: UIButton!
    
    @IBOutlet weak var blurView: UIVisualEffectView!
    @IBOutlet weak var paymentView: paymentOptionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        persentageAmountArray.removeAll()
        donatedIndex.removeAll()
        for _ in List_values
        {
            //persentageAmountArray.append(0.0)
            donatedIndex.append(false)
            persentageAmountArray.append(100.0/Double(List_values.count))
            
        }
        view_picker.isHidden = true
        UIpickerView.delegate = self
        UIpickerView.dataSource = self
        
        pickerData = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100]
        
        amountField.delegate = self
        
        //createRightButton(title: getLocalizedString(key: "continue_btn"))
        
        //amount_donate_lbl.text = "$\(FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: amountToBeDonated))"
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(blurViewTapped))
        blurView.isUserInteractionEnabled = true
        blurView.addGestureRecognizer(tapGestureRecognizer)
        
        NotificationCenter.default.addObserver(self, selector: #selector(willEnterForeground),
                                               name: UIApplication.willEnterForegroundNotification,
                                                   object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        self.setNavBarTitile(title: "Customize", topItem: "Customize")
        localization()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.onPaymentTokenGenerated(_:)), name: NSNotification.Name(rawValue:PaymentObservers.paymentCompleteNotificationKey), object: nil)
        
        
        
    }
    
    
    @objc func willEnterForeground() {
        if isDonatedInAllCharities() {
            self.callPostTokenAPI(paymentToken: "", amount: amountToBeDonated)
        }else {
            print("False")
        }
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    func isDonatedInAllCharities() -> Bool {
        for i in donatedIndex {
            if i == false {
                return false
            }
        }
        
        return true
    }
    
    func showHidePaymentOptionsView(){
        if paymentView.isHidden {
            paymentView.isHidden = false
            blurView.isHidden = false
        }else{
            paymentView.isHidden = true
            blurView.isHidden = true
        }
    }
    
    @objc func blurViewTapped(){
        showHidePaymentOptionsView()
    }
    
    func localization(){
        //setNavBarTitile(title:getLocalizedString(key: "Portfolio", comment: ""), topItem: getLocalizedString(key: "Portfolio", comment: ""))
        ///navigationController?.navigationBar.barStyle = .blackOpaque
        
        
        let customized_distribution_lbl = getLocalizedString(key: "Charity_contributions_lbl", comment: "")
        let amount_lbl = getLocalizedString(key: "Amount_lbl", comment: "")
        main_heading_customized_distribution_lbl.text = customized_distribution_lbl
        //heading_amount_lbl.text = amount_lbl
        done_btn.setTitle(getLocalizedString(key: "done_btn", comment: ""), for: .normal)
        cancel_btn.setTitle(getLocalizedString(key: "cancel_btn", comment: ""), for: .normal)
        
        
        
    }
    
    @IBAction func include_transaction_fee_pressed(_ sender: Any) {
        if (amountField.text?.isEmpty)!{
            self.view.makeToast("Please enter amount")
            
        }else {
            if checked{
                include_transaction_fee_btn.setImage( UIImage(named:"Uncheak_box"), for: .normal)
                checked = false
            }else {
                include_transaction_fee_btn.setImage(UIImage(named:"cheak_box"), for: .normal)
                checked = true
            }
        }
        
    }
    
    func dismissPickerView() {
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let button = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.action))
        toolBar.setItems([button], animated: true)
        toolBar.isUserInteractionEnabled = true
        // amountField.inputAccessoryView = toolBar
    }
    @objc func action() {
        
        view.endEditing(true)
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return ("\(pickerData[row])")
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        pickerDataRowSelectedIndex = row
    }
    
    func createRightButton(title: String){
        
        let rightButton = UIButton(frame: CGRect(x: 0, y: 0, width: 45, height: 16))
        rightButton.setTitle(title, for: .normal)
        rightButton.addTarget(self, action: #selector(continue_pressed(_:)), for: .touchUpInside)
        //rightButton.titleLabel?.font = UIFont(name: "HelveticaNeue", size: 15)
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: rightButton)
    }
    
    @IBAction func back_pressed(_ sender: Any) {
    }
    @IBAction func done_pickerview_pressed(_ sender: Any) {
        view_picker.isHidden = true
        persentageAmountArray[currentIndex] = Double(pickerData[pickerDataRowSelectedIndex])
        tableview_customized_distribution.reloadData()
    }
    
    @IBAction func continue_pressed(_ sender: Any) {
        
        if amountToBeDonated == 0.0 {
            self.view.makeToast("Enter amount!")
            return
        }
        
        var sum = 0.0;
        for percent in persentageAmountArray{
            if percent != -1 {
                sum += percent
            }
        }
        if sum == 100.0{
            //showHidePaymentOptionsView()
            //paymentView.setData(isTransactionFeeIncluded: checked, amount: amountToBeDonated)
            PaymentAPIUtils.amount = amountToBeDonated
            callPostTokenAPI(paymentToken: "", amount: amountToBeDonated)
        }else {
            print(sum)
            self.view.makeToast("Percentage should equal 100%")
        }
    }
    
    @IBAction func cancel_pickerview_pressed(_ sender: Any) {
        view_picker.isHidden = true
        
        
    }
    
    
    @objc private func onPaymentTokenGenerated(_ notification: NSNotification){
        print("Charity method class #174")
        showHidePaymentOptionsView()
        if let token = notification.userInfo?["token"] as? String {
            callPostTokenAPI(paymentToken: token, amount: PaymentAPIUtils.stripeAmount)
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        if amountField.keyboardType == .decimalPad {
            let regex = try! NSRegularExpression(pattern: "^(^[0-9]{0,7})*([.,][0-9]{0,2})?$", options: .caseInsensitive)

            if let newText = (amountField.text as NSString?)?.replacingCharacters(in: range, with: string) {
                return regex.firstMatch(in: newText, options: [], range: NSRange(location: 0, length: newText.count)) != nil

            } else {
                return false
            }
        }else {
            return false
        }


    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == amountField {
            let text = amountField.text ?? ""
            if text == "" {
                amountToBeDonated = 0.0
            }else {
                amountToBeDonated = Double(text)!
            }
            DispatchQueue.main.async {
                self.tableview_customized_distribution.reloadData()
            }
        }
    }
    
    
}
extension customizedDistributionVC: UITableViewDelegate,UITableViewDataSource, CustomizeDistributionDeleage {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return List_values.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! customizedDistributionCell
        cell.heading_lbl.text = List_values[indexPath.row]
        cell.percentField.text = FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: persentageAmountArray[indexPath.row])
        let disbributionAmount = amountToBeDonated * (persentageAmountArray[indexPath.row] / 100)
        cell.amount_lbl.text = "$\(FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: disbributionAmount))"
        
        cell.amount_lbl.isHidden = false
        
        if donatedIndex[indexPath.row] {
            cell.tickImage.isHidden = false
        }else {
            cell.tickImage.isHidden = true
        }
        
        if amountToBeDonated == 0.0 {
            cell.donationView.isHidden = true
        }else {
            cell.donationView.isHidden = false
        }
        
        if disbributionAmount == 0.0 {
            cell.donateBtn.backgroundColor = .systemGray2
            cell.donateBtn.tag = -1
            
        }else {
            cell.donateBtn.backgroundColor = UIColor.init(named: "AppMainColor")
            cell.donateBtn.tag = indexPath.row
        }
        
        cell.donateBtn.addTarget(self, action: #selector(donateBtnPressed(_:)), for: .touchUpInside)
        
        cell.precentage_btn.tag = indexPath.row
        cell.delegate = self
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    
    @objc func donateBtnPressed(_ sender: UIButton) {
        let tag = sender.tag
        if tag != -1 {
            let selectedCharity = ArrayLists.sharedInstance.SelectedListOfCharies.first(where: {
                $0.charityName == List_values[tag]
            })
            
            if selectedCharity != nil {
                print(selectedCharity)
                FintechUtils.sharedInstance.openUrl(url: selectedCharity?.donationUrl ?? "")
                donatedIndex[tag] = true
                
                DispatchQueue.main.async {
                    self.tableview_customized_distribution.reloadData()
                }
            }else {
                print("Charity data not found")
            }
        }
    }
    
    func percentageBtnPressedAt(_ index: Int) {
        currentIndex = index
        view_picker.isHidden = false
    }
}
