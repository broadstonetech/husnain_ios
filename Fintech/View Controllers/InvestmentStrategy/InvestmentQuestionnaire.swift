//
//  InvestmentQuestionnaire.swift
//  Fintech
//
//  Created by broadstone on 31/03/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import UIKit
import RingGraph
class InvestmentQuestionnaire: Parent {
    
    /// Start localized  data
    
    //Mark:- Demograpphics Data

    let demographicsAnswersQ1 = [getLocalizedString(key: "1a", comment: ""), getLocalizedString(key: "1b", comment: ""), getLocalizedString(key: "1c", comment: ""), getLocalizedString(key: "1d", comment: "")]
    let demographicsAnswersIdQ1 = [1,2,3,4]

    let demographicsAnswersQ2 = [getLocalizedString(key: "2a", comment: ""), getLocalizedString(key: "2b", comment: ""), getLocalizedString(key: "2c", comment: "")]
    let demographicsAnswersIdQ2 = [1,2,3]

    let demographicsAnswersQ3 = [getLocalizedString(key: "3a", comment: ""), getLocalizedString(key: "3b", comment: ""), getLocalizedString(key: "3c", comment: ""), getLocalizedString(key: "3d", comment: "")]
    let demographicsAnswersIdQ3 = [1,2,3,4]

    //Mark:- Investment Data

    let investmentAnswersQ1 = [getLocalizedString(key: "4a", comment: ""), getLocalizedString(key: "4b", comment: ""), getLocalizedString(key: "4c", comment: ""), getLocalizedString(key: "4d", comment: ""), getLocalizedString(key: "4e", comment: "")]
    let investmentAnswersIdQ1 = [1,2,3,4,5]

    let investmentAnswersQ2 = [getLocalizedString(key: "5a", comment: ""), getLocalizedString(key: "5b", comment: ""), getLocalizedString(key: "5c", comment: ""), getLocalizedString(key: "5d", comment: ""), getLocalizedString(key: "5e", comment: "")]
    let investmentAnswersIdQ2 = [1,2,3,4,5]

    let investmentAnswersQ3 = [getLocalizedString(key: "6a", comment: ""), getLocalizedString(key: "6b", comment: ""), getLocalizedString(key: "6c", comment: ""), getLocalizedString(key: "6d", comment: "")]
    let investmentAnswersIdQ3 = [1,2,3,4]

    let investmentAnswersQ4 = [getLocalizedString(key: "7a", comment: ""), getLocalizedString(key: "7b", comment: ""), getLocalizedString(key: "7c", comment: ""), getLocalizedString(key: "7d", comment: "")]
    let investmentAnswersIdQ4 = [1,2,3,4]

    let investmentAnswersQ5 = [getLocalizedString(key: "8a", comment: ""), getLocalizedString(key: "8b", comment: ""), getLocalizedString(key: "8c", comment: ""), getLocalizedString(key: "8d", comment: ""), getLocalizedString(key: "8e", comment: "")]
    let investmentAnswersIdQ5 = [1,2,3,4,5]

    //Risk Data
    let riskAnswersQ1 = [getLocalizedString(key: "9a", comment: ""), getLocalizedString(key: "9b", comment: ""), getLocalizedString(key: "9c", comment: ""), getLocalizedString(key: "9d", comment: "")]
    let riskAnswersIdQ1 = [1,2,3,4]

    let riskAnswersQ2 = [getLocalizedString(key: "10a", comment: ""), getLocalizedString(key: "10b", comment: ""), getLocalizedString(key: "10c", comment: ""), getLocalizedString(key: "10d", comment: "")]
    let riskAnswersIdQ2 = [1,2,3,4]

    let riskAnswersQ3 = [getLocalizedString(key: "11a", comment: ""), getLocalizedString(key: "11b", comment: ""), getLocalizedString(key: "11c", comment: ""), getLocalizedString(key: "11d", comment: "")]
    let riskAnswersIdQ3 = [1,2,3,4]

    let riskAnswersQ4 = [getLocalizedString(key: "12a", comment: ""), getLocalizedString(key: "12b", comment: ""), getLocalizedString(key: "12c", comment: "")]
    let riskAnswersIdQ4 = [1,2,3]

    let riskAnswersQ5 = [getLocalizedString(key: "13a", comment: ""), getLocalizedString(key: "13b", comment: ""), getLocalizedString(key: "13c", comment: "")]
    let riskAnswersIdQ5 = [1,2,3]

    let riskAnswersQ6 = [getLocalizedString(key: "14a", comment: ""), getLocalizedString(key: "14b", comment: ""), getLocalizedString(key: "14c", comment: ""), getLocalizedString(key: "14d", comment: "")]
    let riskAnswersIdQ6 = [1,2,3,4]

    let riskAnswersQ7 = [getLocalizedString(key: "15a", comment: ""), getLocalizedString(key: "15b", comment: ""), getLocalizedString(key: "15c", comment: ""), getLocalizedString(key: "15d", comment: "")]
    let riskAnswersIdQ7 = [1,2,3,4]

    let riskAnswersQ8 = [getLocalizedString(key: "16a", comment: ""), getLocalizedString(key: "16b", comment: ""), getLocalizedString(key: "16c", comment: ""), getLocalizedString(key: "16d", comment: "")]
    let riskAnswersIdQ8 = [1,2,3,4]

    let riskAnswersQ9 = [getLocalizedString(key: "17a", comment: ""), getLocalizedString(key: "17b", comment: "")]
    let riskAnswersIdQ9 = [1,2]

    let riskAnswersQ10 = [getLocalizedString(key: "18a", comment: ""), getLocalizedString(key: "18b", comment: "")]
    let riskAnswersIdQ10 = [1,2]

    let riskAnswersQ11 = [getLocalizedString(key: "19a", comment: ""), getLocalizedString(key: "19b", comment: ""), getLocalizedString(key: "19c", comment: ""), getLocalizedString(key: "19d", comment: "")]
    let riskAnswersIdQ11 = [1,2,3,4]

    let riskAnswersQ12 = [getLocalizedString(key: "20a", comment: ""), getLocalizedString(key: "20b", comment: ""), getLocalizedString(key: "20c", comment: "")]
    let riskAnswersIdQ12 = [1,2,3]

    let riskAnswersQ13 = [getLocalizedString(key: "21a", comment: ""), getLocalizedString(key: "21b", comment: ""), getLocalizedString(key: "21c", comment: ""), getLocalizedString(key: "21d", comment: "")]
    let riskAnswersIdQ13 = [1,2,3,4]

    //Mark:- Values Data
    let valuesAnswersQ1 = [getLocalizedString(key: "22a", comment: ""), getLocalizedString(key: "22b", comment: "")]
    let valuesAnswersIdQ1 = [1,2]

    let valuesAnswersQ2 =
        [   getLocalizedString(key: "24a", comment: ""), getLocalizedString(key: "24b", comment: ""), getLocalizedString(key: "24c", comment: ""), getLocalizedString(key: "24d", comment: ""), getLocalizedString(key: "24e", comment: "")]
    let valuesAnswersIdQ2 = [1,2,3,4,5]

    let valuesAnswersQ3 =
        [   getLocalizedString(key: "23b", comment: ""), getLocalizedString(key: "23c", comment: ""), getLocalizedString(key: "23d", comment: ""), getLocalizedString(key: "23e", comment: ""), getLocalizedString(key: "23f", comment: ""), getLocalizedString(key: "23g", comment: ""), getLocalizedString(key: "23h", comment: ""), getLocalizedString(key: "23i", comment: ""), getLocalizedString(key: "23j", comment: ""), getLocalizedString(key: "23k", comment: ""), getLocalizedString(key: "23l", comment: ""), getLocalizedString(key: "23m", comment: "")]
    let valuesAnswersIdQ3 = [1,2,3,4,5,6,7,8,9,10,11,12]

    let valuesAnswersQ4 =
        [   getLocalizedString(key: "25a", comment: ""), getLocalizedString(key: "25b", comment: ""), getLocalizedString(key: "25c", comment: "")]
    let valuesAnswersIdQ4 = [1,2,3]

    let valuesAnswersQ5 =
        [   getLocalizedString(key: "26a", comment: ""), getLocalizedString(key: "26b", comment: ""), getLocalizedString(key: "26c", comment: ""), getLocalizedString(key: "26d", comment: ""), getLocalizedString(key: "26e", comment: "")]
    let valuesAnswersIdQ5 = [1,2,3,4,5]

    let valuesAnswersQ6 =
        [   getLocalizedString(key: "27a", comment: ""), getLocalizedString(key: "27b", comment: ""), getLocalizedString(key: "27c", comment: ""), getLocalizedString(key: "27d", comment: ""), getLocalizedString(key: "27e", comment: "")]
    let valuesAnswersIdQ6 = [1,2,3,4,5]

    let valuesAnswersQ7 =
        [   getLocalizedString(key: "28a", comment: ""), getLocalizedString(key: "28b", comment: "")]
    let valuesAnswersIdQ7 = [1,2]
    
    
    /// End localized data
    
    
    var tableViewData = [QuestionnaireCategoriesModel]()
    
    var currentCategoryIndex = 0
    var currentQuestionIndex = 0
    var isEditingMultiSelection = false
    
    @IBOutlet weak var investmentStrategyLabel: UILabel!
    @IBOutlet weak var skipButton: UIButton!
    
    @IBOutlet weak var demographicsLabel: UILabel!
    @IBOutlet weak var investmentLabel: UILabel!
    @IBOutlet weak var valuesLabel: UILabel!
    @IBOutlet weak var riskLabel: UILabel!
    
    @IBOutlet weak var demographicsQuestionsCountLbl: UILabel!
    @IBOutlet weak var investmentQuestionsCountLbl: UILabel!
    @IBOutlet weak var riskQuestionsCountLbl: UILabel!
    @IBOutlet weak var valuesQuestionsCountLbl: UILabel!
    
    @IBOutlet weak var demographicsPrgressView: UIProgressView!
    @IBOutlet weak var investmentPrgressView: UIProgressView!
    @IBOutlet weak var riskPrgressView: UIProgressView!
    @IBOutlet weak var valuesPrgressView: UIProgressView!
    
    @IBOutlet weak var demographicsCheckMarkImg: UIImageView!
    @IBOutlet weak var investmentCheckMarkImg: UIImageView!
    @IBOutlet weak var riskCheckMarkImg: UIImageView!
    @IBOutlet weak var valuesCheckMarkImg: UIImageView!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var submitButton: UIButton!
    
    @IBOutlet weak var dialogView: UIView!
    @IBOutlet weak var dialogViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var blurView: UIVisualEffectView!
    
    @IBOutlet weak var demographicsDialogBtn: UIImageView!
    @IBOutlet weak var investmentDialogBtn: UIImageView!
    @IBOutlet weak var riskDialogBtn: UIImageView!
    @IBOutlet weak var valuesDialogBtn: UIImageView!
    
    let effect = UIBlurEffect(style: .light)
    
    
    //API related variables
    static var questionnaireStatusModel: QuestionnaireStatusModel!
    var demographicsApiDataModel: QuestionnaireDataFromAPI!
    var investmentApiDataModel: QuestionnaireDataFromAPI!
    var riskApiDataModel: QuestionnaireDataFromAPI!
    var valuesApiDataModel: QuestionnaireDataFromAPI!
    
    var riskRawScore = 1
    
    var isValuesLoadedFromTemplate = false
    var templateType: ValuesTemplate = .NOT_SET
    
    override func viewDidLoad() {
        super.viewDidLoad()
        submitButton.isHidden = true
        populateData()
        setInitialViewData()
        valuesApiDataModel = QuestionnaireDataFromAPI.init(success: false, message: "", selectedAnswers: [], template: "")
        if InvestmentQuestionnaire.questionnaireStatusModel != nil {
            if InvestmentQuestionnaire.questionnaireStatusModel.success {
                if InvestmentQuestionnaire.questionnaireStatusModel.data.demographics == true {
                    getDemographicsDataFromAPI()
                }else {
                    if InvestmentQuestionnaire.questionnaireStatusModel.data.investment == true {
                        getInvestmentDataFromAPI()
                    }else {
                        if InvestmentQuestionnaire.questionnaireStatusModel.data.risk == true {
                            getRiskDataFromAPI()
                        }else {
                            if InvestmentQuestionnaire.questionnaireStatusModel.data.values == true {
                                getValuesDataFromAPI()
                            }
                        }
                    }
                }
            }
        }
        
        
        investmentStrategyLabel.text = getLocalizedString(key: "my_investment_strategy")
        skipButton.setTitle(getLocalizedString(key: "i_ll_do_it_later"), for: .normal)

        tableView.delegate = self
        tableView.dataSource = self
        //tableView.isScrollEnabled = false
        
//        let backgroundView = UIView(frame: view.bounds)
//        backgroundView.autoresizingMask = view.autoresizingMask
//        backgroundView.addSubview(self.buildImageView())
//        backgroundView.addSubview(self.buildBlurView())

//        tableView.backgroundView = backgroundView
//        tableView.separatorEffect = UIVibrancyEffect(blurEffect: effect)
//
//
        let demographicsGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(demographpicsDialogBtnPressed))
        let investmentGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(investmentDialogBtnPressed))
        let riskGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(riskDialogBtnPressed))
        let valuesGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(valuesDialogBtnPressed))
        demographicsDialogBtn.isUserInteractionEnabled = true
        investmentDialogBtn.isUserInteractionEnabled = true
        riskDialogBtn.isUserInteractionEnabled = true
        valuesDialogBtn.isUserInteractionEnabled = true
        demographicsDialogBtn.addGestureRecognizer(demographicsGestureRecognizer)
        investmentDialogBtn.addGestureRecognizer(investmentGestureRecognizer)
        riskDialogBtn.addGestureRecognizer(riskGestureRecognizer)
        valuesDialogBtn.addGestureRecognizer(valuesGestureRecognizer)
        
        
        let closeBtnGesture = UITapGestureRecognizer(target: self, action: #selector(closeBtnPressed))

        blurView.isUserInteractionEnabled = true
        blurView.addGestureRecognizer(closeBtnGesture)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    func buildImageView() -> UIImageView {
        let imageView = UIImageView(image: UIImage(named: "values"))
        imageView.frame = view.bounds
        imageView.autoresizingMask = view.autoresizingMask
        imageView.alpha = 0.2
        return imageView
    }

    func buildBlurView() -> UIVisualEffectView {
        let blurView = UIVisualEffectView(effect: effect)
        blurView.frame = view.bounds
        blurView.autoresizingMask = view.autoresizingMask
        return blurView
    }
    
    @objc func demographpicsDialogBtnPressed(){
        if tableViewData[0].isCompleted{
            //dialogView.frame.size.height = CGFloat(320)
            dialogViewHeightConstraint.constant = 320
            createDialogView(categoryIndex: 0)
        }
    }
    
    @objc func investmentDialogBtnPressed(){
        if tableViewData[1].isCompleted{
            //dialogView.frame.size.height = CGFloat(400)
            dialogViewHeightConstraint.constant = 400
            createDialogView(categoryIndex: 1)
        }
    }
    
    @objc func riskDialogBtnPressed(){
        if tableViewData[2].isCompleted{
            //dialogView.frame.size.height = CGFloat(400)
            dialogViewHeightConstraint.constant = 400
            createDialogView(categoryIndex: 2)
        }
    }
    
    @objc func valuesDialogBtnPressed(){
        if tableViewData[3].isCompleted{
            if tableViewData[3].questionsData.count == 1 {
                //dialogView.frame.size.height = CGFloat(320)
                dialogViewHeightConstraint.constant = 320
            }else{
                //dialogView.frame.size.height = CGFloat(520)
                //dialogView.frame.size.height = CGFloat(400)
                dialogViewHeightConstraint.constant = 400
            }
            createDialogView(categoryIndex: 3)
            
        }
    }
    
    private func createDialogView(categoryIndex: Int){
        if categoryIndex == 2 {
            let riskDialogView = RiskResultsDialog(frame: dialogView.bounds)
            let closeBtnGesture = UITapGestureRecognizer(target: self, action: #selector(closeBtnPressed))
            riskDialogView.closeBtn.isUserInteractionEnabled = true
            riskDialogView.closeBtn.addGestureRecognizer(closeBtnGesture)
            riskDialogView.recalculateRiskBtn.addTarget(self, action: #selector(riskButtonPressedLogic), for: .touchUpInside)
            riskDialogView.titleLabel.text = tableViewData[2].category
            riskDialogView.setInitialViewData(riskScore: Float(self.riskRawScore))
            riskDialogView.tag = 2
            dialogView.addSubview(riskDialogView)
        }else {
            let investmentStrategyDialog = InvestmentStrategyResultDialog(frame: dialogView.bounds)
            let closeBtnGesture = UITapGestureRecognizer(target: self, action: #selector(closeBtnPressed))
            investmentStrategyDialog.closeBtn.isUserInteractionEnabled = true
            investmentStrategyDialog.closeBtn.addGestureRecognizer(closeBtnGesture)
            
            investmentStrategyDialog.tagListView.removeAllTags()
            investmentStrategyDialog.titleLabel.text = tableViewData[categoryIndex].category
            investmentStrategyDialog.tagListView.addTags(getTagsData(categoryIndex: categoryIndex))
            
            if categoryIndex == 0 {
                investmentStrategyDialog.categoryImage.image = UIImage(named: "about_me_selected")
                investmentStrategyDialog.recalculateScoreBtn.addTarget(self, action: #selector(dempgraphicsButtonPressedLogic), for: .touchUpInside)
                
            }else if categoryIndex == 1 {
                investmentStrategyDialog.categoryImage.image = UIImage(named: "help_me_invest_selected")
                investmentStrategyDialog.recalculateScoreBtn.addTarget(self, action: #selector(investmentButtonPressedLogic), for: .touchUpInside)
            }else if categoryIndex == 3 {
                investmentStrategyDialog.categoryImage.image = UIImage(named: "values_I_care_about_selected")
                investmentStrategyDialog.recalculateScoreBtn.addTarget(self, action: #selector(valuesButtonPressedLogic), for: .touchUpInside)
                if tableViewData[3].questionsData.count == 1 {
                    if isValuesLoadedFromTemplate || valuesApiDataModel.template != "" {
                        investmentStrategyDialog.tagListView.removeAllTags()
                        investmentStrategyDialog.tagListView.addTag("Template: \(String(describing: templateType))")
                    }else {
                        investmentStrategyDialog.tagListView.removeAllTags()
                        investmentStrategyDialog.tagListView.addTag(getLocalizedString(key: "values_q1_tag_for_no"))
                    }
                    
                }
            }
            dialogView.addSubview(investmentStrategyDialog)
        }
        dialogView.isHidden = false
        blurView.isHidden = false
    }
    
    private func getTagsData(categoryIndex: Int) -> [String] {
        var tagsData = [String]()
        for i in 0..<tableViewData[categoryIndex].questionsData.count {
            let tag = tableViewData[categoryIndex].questionsData[i].tagsPrefix + (tableViewData[categoryIndex].questionsData[i].selectedAnswers.map{String($0)}).joined(separator: ",")
            tagsData.append(tag)
        }
        return tagsData
    }
    
    @objc func closeBtnPressed(){
        dialogView.isHidden = true
        blurView.isHidden = true
        
        let riskDialogView = dialogView.viewWithTag(2) as? RiskResultsDialog
        if riskDialogView != nil {
            let updatedScore = riskDialogView?.updatedRiskScore ?? 0
            if updatedScore == 0 || updatedScore == self.riskRawScore{
                
            }else {
                self.riskRawScore = updatedScore
                riskSetterAPI()
            }
        }
        
        for view in dialogView.subviews {
            view.removeFromSuperview()
        }
    }
    
    @IBAction func submitButtonPressed(_ sender: UIButton) {
        let storyboard = UIStoryboard.init(name: "InvestmentStrategy", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "QuestionnaireSummaryVC") as! QuestionnaireSummaryVC
        vc.tableViewData = tableViewData
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func demographicsButtonPressed(_ sender: Any) {
        if tableViewData[0].isCompleted {
            demographpicsDialogBtnPressed()
        }else {
            dempgraphicsButtonPressedLogic()
        }
    }
    
    @objc private func dempgraphicsButtonPressedLogic() {
        closeBtnPressed()
        setViewData(categoryIndex: currentCategoryIndex)
        currentCategoryIndex = 0
        currentQuestionIndex = getCurrentQuestionIndex()
        
        setViewData(categoryIndex: currentCategoryIndex)
        tableView.reloadData()
        if tableViewData[0].isCompleted {
            tableView.scrollToRow(at: NSIndexPath.init(row: 0, section: 0) as IndexPath, at: .top, animated: true)
        }else {
            tableView.scrollToRow(at: NSIndexPath.init(row: 0, section: currentQuestionIndex) as IndexPath, at: .top, animated: true)
        }
    }
    
    @IBAction func investmentButtonPressed(_ sender: Any) {
        
        if tableViewData[1].isCompleted {
            investmentDialogBtnPressed()
        }else {
            investmentButtonPressedLogic()
        }
        
        
    }
    
    @objc private func investmentButtonPressedLogic() {
        closeBtnPressed()
        setViewData(categoryIndex: currentCategoryIndex)
//        if tableViewData[1].isCompleted {
//
//        }
        currentCategoryIndex = 1
        currentQuestionIndex = getCurrentQuestionIndex()
        
        setViewData(categoryIndex: currentCategoryIndex)
        tableView.reloadData()
        if tableViewData[1].isCompleted {
            tableView.scrollToRow(at: NSIndexPath.init(row: 0, section: 0) as IndexPath, at: .top, animated: true)
        }else {
            tableView.scrollToRow(at: NSIndexPath.init(row: 0, section: currentQuestionIndex) as IndexPath, at: .top, animated: true)
        }
    }
    
    @IBAction func riskButtonPressed(_ sender: Any) {
        if tableViewData[2].isCompleted {
            riskDialogBtnPressed()
        }else {
            riskButtonPressedLogic()
        }
    }
    
    @objc private func riskButtonPressedLogic(){
        closeBtnPressed()
        setViewData(categoryIndex: currentCategoryIndex)
//        if tableViewData[1].isCompleted {
//
//        }
        currentCategoryIndex = 2
        currentQuestionIndex = getCurrentQuestionIndex()
        
        setViewData(categoryIndex: currentCategoryIndex)
        tableView.reloadData()
        if tableViewData[2].isCompleted {
            tableView.scrollToRow(at: NSIndexPath.init(row: 0, section: 0) as IndexPath, at: .top, animated: true)
        }else {
            tableView.scrollToRow(at: NSIndexPath.init(row: 0, section: currentQuestionIndex) as IndexPath, at: .top, animated: true)
        }
    }
    
    @IBAction func valuesButtonPressed(_ sender: Any) {
        
        if tableViewData[3].isCompleted {
            valuesDialogBtnPressed()
        }else {
            valuesButtonPressedLogic()
        }
        
        
    }
    
    @objc private func valuesButtonPressedLogic(){
        closeBtnPressed()
        setViewData(categoryIndex: currentCategoryIndex)
        currentCategoryIndex = 3
        currentQuestionIndex = getCurrentQuestionIndex()
        
        setViewData(categoryIndex: currentCategoryIndex)
        
        tableView.reloadData()
        if tableViewData[3].isCompleted {
            tableView.scrollToRow(at: NSIndexPath.init(row: 0, section: 0) as IndexPath, at: .top, animated: true)
        }else {
            tableView.scrollToRow(at: NSIndexPath.init(row: 0, section: currentQuestionIndex) as IndexPath, at: .top, animated: true)
        }
    }
    
    @IBAction func closeButtonPressed(_ sender: Any) {
        let alert = UIAlertController(title: getLocalizedString(key: "Investment strategy"), message: getLocalizedString(key: "skip_investment_strategy"),  preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: getLocalizedString(key: "_skip"), style: UIAlertAction.Style.default, handler: { _ in
            self.dismiss(animated: true, completion: nil)
            //self.PopVC()
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "nav_bar_start") as UIViewController
            UIApplication.shared.keyWindow?.rootViewController = vc
        }))
        alert.addAction(UIAlertAction(title: getLocalizedString(key: "cancel_btn"), style: UIAlertAction.Style.cancel, handler: { _ in
            self.dismiss(animated: true, completion: nil)
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
}
extension InvestmentQuestionnaire: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return tableViewData[currentCategoryIndex].questionsData.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableViewData[currentCategoryIndex].questionsData[section].isMultiSelection {
            return 1 + 1 + tableViewData[currentCategoryIndex].questionsData[section].answers.count
        }else {
            return 1 + tableViewData[currentCategoryIndex].questionsData[section].answers.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "QuestionStatementCell") as! QuestionStatementCell
            let questionData = tableViewData[currentCategoryIndex].questionsData[indexPath.section]
            cell.questionLabel.text = questionData.question
            
            if indexPath.section > currentQuestionIndex && tableViewData[currentCategoryIndex].isCompleted == false {
                cell.contentView.alpha = 0.2
            }else{
                cell.contentView.alpha = 1
            }

            
            return cell
        }else if indexPath.row >= 1 {
            if tableViewData[currentCategoryIndex].questionsData[indexPath.section].isMultiSelection {
                if indexPath.row == 1 {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "QuestionNumberCell") as! QuestionNumberCell
                    cell.questionNumberLbl.text = getLocalizedString(key: "select_all_that_applies")
                    
                    return cell
                }else{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "QuestionnaireAnswersCell") as! QuestionnaireAnswersCell
                    
                    cell.answerLabel.text = tableViewData[currentCategoryIndex].questionsData[indexPath.section].answers[indexPath.row - 2]
                    
                    let questionnaireData = tableViewData[currentCategoryIndex].questionsData[indexPath.section]
                    if questionnaireData.selectedAnswersIds.contains(indexPath.row - 1) {
                        cell.answerLabel.textColor = .white
                        cell.parentView.backgroundColor = .systemGreen
                    }else {
                        cell.answerLabel.textColor = .black
                        cell.parentView.backgroundColor = .clear
                    }
                    
                    if indexPath.section > currentQuestionIndex && tableViewData[currentCategoryIndex].isCompleted == false {
                        cell.contentView.alpha = 0.2
                    }else{
                        cell.contentView.alpha = 1
                    }
                    
                    return cell
                }
            }
            else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "QuestionnaireAnswersCell") as! QuestionnaireAnswersCell
                
                cell.answerLabel.text = tableViewData[currentCategoryIndex].questionsData[indexPath.section].answers[indexPath.row - 1]
                
                let questionnaireData = tableViewData[currentCategoryIndex].questionsData[indexPath.section]
                if questionnaireData.selectedAnswersIds.contains(indexPath.row) {
                    cell.answerLabel.textColor = .white
                    cell.parentView.backgroundColor = .systemGreen
                }else {
                    cell.answerLabel.textColor = .black
                    cell.parentView.backgroundColor = .clear
                }
                
                if indexPath.section > currentQuestionIndex && tableViewData[currentCategoryIndex].isCompleted == false {
                    cell.contentView.alpha = 0.2
                }else{
                    cell.contentView.alpha = 1
                }
                
                return cell
            }
        }
        
        return UITableViewCell()
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableViewData[currentCategoryIndex].isCompleted || indexPath.section <=  tableViewData[currentCategoryIndex].questionsData[currentQuestionIndex].questionId {
            
            var selectionIndex = 1
            var selectedAnswerRow = indexPath.row
            if tableViewData[currentCategoryIndex].questionsData[indexPath.section].isMultiSelection {
                selectionIndex = 2
                selectedAnswerRow -= 1
            }
            
            if indexPath.row >= selectionIndex {
                
                var questionnaireData = tableViewData[currentCategoryIndex].questionsData[indexPath.section]
                
                if questionnaireData.isMultiSelection {
                    if questionnaireData.selectedAnswersIds.contains(selectedAnswerRow) {
                        questionnaireData.selectedAnswersIds.remove(selectedAnswerRow)
                        questionnaireData.selectedAnswers.remove(questionnaireData.answers[indexPath.row - selectionIndex])
                    }else{
                        questionnaireData.selectedAnswersIds.append(selectedAnswerRow)
                        questionnaireData.selectedAnswers.append(questionnaireData.answers[indexPath.row - selectionIndex])
                    }
                    isEditingMultiSelection = true
                }else{
                    questionnaireData.selectedAnswers.removeAll()
                    questionnaireData.selectedAnswersIds.removeAll()
                    questionnaireData.selectedAnswers.append(questionnaireData.answers[indexPath.row - selectionIndex])
                    questionnaireData.selectedAnswersIds.append(selectedAnswerRow)
                    isEditingMultiSelection = false
                }
                
    //            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [self] in
    //
    //            }
                
                if currentCategoryIndex == 3 && indexPath.section == 0 {
                    if questionnaireData.selectedAnswersIds[0] == 1 {  //1 means answer is YES
                        showValuesBottomSheet()
                        return
                        
                    }else{  //otherwise answer is NO or Don't Care
                        tableViewData[3] = getValuesData()
                    }
                }
                
                tableViewData[currentCategoryIndex].questionsData[indexPath.section] = questionnaireData
                
                
                if currentQuestionIndex < tableViewData[currentCategoryIndex].questionsData.count - 1 || isEditingMultiSelection || indexPath.section != currentQuestionIndex{
                    if indexPath.section == currentQuestionIndex {
                        currentQuestionIndex += 1
                    }
                }else {
                    currentQuestionIndex = 0
                }
                setViewData(categoryIndex: currentCategoryIndex)
                
                if tableViewData[currentCategoryIndex].isCompleted {
                    callSetterApiBasedOnCategoryIndex()
                }
                
//                if indexPath.section == tableView.numberOfSections - 1 {
//                      showCompleteDialogIfComplete()
//                }
                tableView.reloadData()
                
                showCompleteDialogIfComplete()
                
                if currentQuestionIndex < tableView.numberOfSections && !questionnaireData.isMultiSelection{
                    tableView.scrollToRow(at: NSIndexPath.init(row: 0, section: currentQuestionIndex) as IndexPath, at: .top, animated: true)
                    
                }
                
                
            }
        }
        
        
    }
    
    private func showCompleteDialogIfComplete() {
        if isInvestmentStrategyCompleted() {
            if submitButton.isHidden {
                showStrategyCompletionDialog()
            }else {
                //Do nothing, becuase submit button is added
            }
        }else {
            if currentQuestionIndex == tableView.numberOfSections {
                currentCategoryIndex = getCurrentCategoryIndex()
            }
            currentQuestionIndex = getCurrentQuestionIndex()
            
            setViewData(categoryIndex: currentCategoryIndex)
            
            tableView.reloadData()
//            tableView.scrollToRow(at: NSIndexPath.init(row: 0, section: currentQuestionIndex) as IndexPath, at: .top, animated: true)
        }
    }
    
    
}

extension InvestmentQuestionnaire {
    private func openRiskBottomSheet() {
        let optionMenu = UIAlertController(title: nil, message: getLocalizedString(key: "I_am_choose_the_best"), preferredStyle: .actionSheet)
        
        let lowRisk = UIAlertAction(title: getLocalizedString(key: "low_risk_tolerant"), style: .default, handler: { [self]
            (alert: UIAlertAction!) -> Void in
            //bottomSheetsActions(isRiskButtonsPressed: true)
            self.tableViewData[2].isCompleted = true
            self.riskLabel.text = getLocalizedString(key: "risk_low_risk_tolerant")
            self.setViewData(categoryIndex: 2)
        })
        let averageRisk = UIAlertAction(title: getLocalizedString(key: "average_risk_tolerant"), style: .default, handler: { [self]
            (alert: UIAlertAction!) -> Void in
            self.tableViewData[2].isCompleted = true
            self.riskLabel.text = getLocalizedString(key: "risk_avg_risk_tolerant")
            self.setViewData(categoryIndex: 2)
        })
        
        let highRisk = UIAlertAction(title: getLocalizedString(key: "high_risk_tolerant"), style: .default, handler: { [self]
            (alert: UIAlertAction!) -> Void in
            self.tableViewData[2].isCompleted = true
            self.riskLabel.text = getLocalizedString(key: "risk_high_tolerant")
            self.setViewData(categoryIndex: 2)
            
        })
        
        let recalculate = UIAlertAction(title: getLocalizedString(key: "recalculate_risk"), style: .cancel, handler: { [self]
            (alert: UIAlertAction!) -> Void in
            
            //bottomSheetsActions(isRiskButtonsPressed: false)
            currentCategoryIndex = 2
            currentQuestionIndex = 0
            
            tableViewData[currentCategoryIndex].isCompleted = false
            for i in 0..<tableViewData[currentCategoryIndex].questionsData.count {
                tableViewData[currentCategoryIndex].questionsData[i].selectedAnswersIds.removeAll()
                tableViewData[currentCategoryIndex].questionsData[i].selectedAnswers.removeAll()
            }

            setViewData(categoryIndex: currentCategoryIndex)
            tableView.reloadData()
            
            tableView.scrollToRow(at: NSIndexPath.init(row: 0, section: currentQuestionIndex) as IndexPath, at: .top, animated: true)
            
        })
        
        let cancel = UIAlertAction(title: getLocalizedString(key: "cancel_btn"), style: .destructive, handler: { [self]
            (alert: UIAlertAction!) -> Void in
            
            self.dismiss(animated: true, completion: nil)
            
        })
        
        
        
        optionMenu.addAction(lowRisk)
        optionMenu.addAction(averageRisk)
        optionMenu.addAction(highRisk)
        optionMenu.addAction(recalculate)
        optionMenu.addAction(cancel)
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    private func showPortfolioTypeBottomSheet(){
        let optionMenu = UIAlertController(title: nil, message: getLocalizedString(key: "select_portfolio_type"), preferredStyle: .actionSheet)
        
        let stocks = UIAlertAction(title: getLocalizedString(key: "stocks_only"), style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.submitInvestmentQuestionnaire(portfolioType: "stocks")
            //self.apiupdateFuncation(portfolioType: "stocks")
            
        })
        let etf = UIAlertAction(title: getLocalizedString(key: "etf_only"), style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.submitInvestmentQuestionnaire(portfolioType: "etf")
            
        })
        
        let vesgo = UIAlertAction(title: getLocalizedString(key: "let_vesgo_decide"), style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.submitInvestmentQuestionnaire(portfolioType: "both")
        })
        
        
        
        
        optionMenu.addAction(stocks)
        optionMenu.addAction(etf)
        optionMenu.addAction(vesgo)
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    private func showStrategyCompletionDialog(){
        let alert = UIAlertController(title: getLocalizedString(key: "Investment strategy"), message: getLocalizedString(key: "your_investment_strategy_is_complete"),  preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: getLocalizedString(key: "_change"), style: UIAlertAction.Style.default, handler: { _ in
            self.dismiss(animated: true, completion: nil)
            self.submitButton.isHidden = false
        }))
        alert.addAction(UIAlertAction(title: getLocalizedString(key: "_submit"), style: UIAlertAction.Style.default, handler: { [self] _ in
            //self.showPortfolioTypeBottomSheet()
            //self.submitInvestmentQuestionnaire(portfolioType: "stocks")
            let storyboard = UIStoryboard.init(name: "InvestmentStrategy", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "QuestionnaireSummaryVC") as! QuestionnaireSummaryVC
            vc.tableViewData = tableViewData
            self.navigationController?.pushViewController(vc, animated: true)
        }))
        
        
        self.present(alert, animated: true, completion: nil)
    }
    
    private func showStrategyCompletionDialogOnGetterApis(){
        let alert = UIAlertController(title: getLocalizedString(key: "Investment strategy"), message: getLocalizedString(key: "your_investment_strategy_is_complete_with_restart"),  preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: getLocalizedString(key: "_change"), style: UIAlertAction.Style.default, handler: { _ in
            self.dismiss(animated: true, completion: nil)
            self.submitButton.isHidden = false
        }))
        alert.addAction(UIAlertAction(title: getLocalizedString(key: "start_from_beginning"), style: UIAlertAction.Style.default, handler: { _ in
            self.currentCategoryIndex = 0
            self.currentQuestionIndex = 0
            self.tableViewData.removeAll()
            self.populateData()
            self.setInitialViewData()
            self.tableView.reloadData()
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
}

extension InvestmentQuestionnaire {
    private func populateData(){
        
        let demographicsData = QuestionnaireCategoriesModel.init(category: getLocalizedString(key: "about_me"), categoryId: 0, questionsData: [
            Questionnaire(question: getLocalizedString(key: "Q1", comment: ""), questionId: 0, answers: demographicsAnswersQ1, answersId: demographicsAnswersIdQ1, isMultiSelection: false, tagsPrefix: getLocalizedString(key: "demographics_q1_tag_prefix")),
            Questionnaire(question: getLocalizedString(key: "Q2", comment: ""), questionId: 1, answers: demographicsAnswersQ2, answersId: demographicsAnswersIdQ2, isMultiSelection: false, tagsPrefix: getLocalizedString(key: "demographics_q2_tag_prefix")),
            Questionnaire(question: getLocalizedString(key: "Q3", comment: ""), questionId: 2, answers: demographicsAnswersQ3, answersId: demographicsAnswersIdQ3, isMultiSelection: false, tagsPrefix: getLocalizedString(key: "demographics_q3_tag_prefix"))
        ], isCompleted: false, questionIdPrefix: "qd")
        
        let investmentData = QuestionnaireCategoriesModel.init(category: getLocalizedString(key: "help_me_invest"), categoryId: 1, questionsData: [
            Questionnaire(question: getLocalizedString(key: "Q4", comment: ""), questionId: 0, answers: investmentAnswersQ1, answersId: investmentAnswersIdQ1, isMultiSelection: false, tagsPrefix: getLocalizedString(key: "investment_q1_tag_prefix")),
            Questionnaire(question: getLocalizedString(key: "Q5", comment: ""), questionId: 1, answers: investmentAnswersQ2, answersId: investmentAnswersIdQ2, isMultiSelection: false, tagsPrefix: getLocalizedString(key: "investment_q2_tag_prefix")),
            Questionnaire(question: getLocalizedString(key: "Q6", comment: ""), questionId: 2, answers: investmentAnswersQ3, answersId: investmentAnswersIdQ3, isMultiSelection: false, tagsPrefix: getLocalizedString(key: "investment_q3_tag_prefix")),
            Questionnaire(question: getLocalizedString(key: "Q8", comment: ""), questionId: 3, answers: investmentAnswersQ5, answersId: investmentAnswersIdQ5, isMultiSelection: true, tagsPrefix: getLocalizedString(key: "investment_q5_tag_prefix")),
            Questionnaire(question: getLocalizedString(key: "Q7", comment: ""), questionId: 4, answers: investmentAnswersQ4, answersId: investmentAnswersIdQ4, isMultiSelection: false, tagsPrefix: getLocalizedString(key: "investment_q4_tag_prefix"))
            
        ], isCompleted: false, questionIdPrefix: "qi")
        
        let riskData = QuestionnaireCategoriesModel.init(category: getLocalizedString(key: "my_risk_tolerance"), categoryId: 2, questionsData: [
            Questionnaire(question: getLocalizedString(key: "Q9", comment: ""), questionId: 0, answers: riskAnswersQ1, answersId: riskAnswersIdQ1, isMultiSelection: false, tagsPrefix: ""),
            Questionnaire(question: getLocalizedString(key: "Q10", comment: ""), questionId: 1, answers: riskAnswersQ2, answersId: riskAnswersIdQ2, isMultiSelection: false, tagsPrefix: ""),
            Questionnaire(question: getLocalizedString(key: "Q11", comment: ""), questionId: 2, answers: riskAnswersQ3, answersId: riskAnswersIdQ3, isMultiSelection: false, tagsPrefix: ""),
            Questionnaire(question: getLocalizedString(key: "Q12", comment: ""), questionId: 3, answers: riskAnswersQ4, answersId: riskAnswersIdQ4, isMultiSelection: false, tagsPrefix: ""),
            Questionnaire(question: getLocalizedString(key: "Q13", comment: ""), questionId: 4, answers: riskAnswersQ5, answersId: riskAnswersIdQ5, isMultiSelection: false, tagsPrefix: ""),
            Questionnaire(question: getLocalizedString(key: "Q14", comment: ""), questionId: 5, answers: riskAnswersQ6, answersId: riskAnswersIdQ6, isMultiSelection: false, tagsPrefix: ""),
            Questionnaire(question: getLocalizedString(key: "Q15", comment: ""), questionId: 6, answers: riskAnswersQ7, answersId: riskAnswersIdQ7, isMultiSelection: false, tagsPrefix: ""),
            Questionnaire(question: getLocalizedString(key: "Q16", comment: ""), questionId: 7, answers: riskAnswersQ8, answersId: riskAnswersIdQ8, isMultiSelection: false, tagsPrefix: ""),
            Questionnaire(question: getLocalizedString(key: "Q17", comment: ""), questionId: 8, answers: riskAnswersQ9, answersId: riskAnswersIdQ9, isMultiSelection: false, tagsPrefix: ""),
            Questionnaire(question: getLocalizedString(key: "Q18", comment: ""), questionId: 9, answers: riskAnswersQ10, answersId: riskAnswersIdQ10, isMultiSelection: false, tagsPrefix: ""),
            Questionnaire(question: getLocalizedString(key: "Q19", comment: ""), questionId: 10, answers: riskAnswersQ11, answersId: riskAnswersIdQ11, isMultiSelection: false, tagsPrefix: ""),
            Questionnaire(question: getLocalizedString(key: "Q20", comment: ""), questionId: 11, answers: riskAnswersQ12, answersId: riskAnswersIdQ12, isMultiSelection: false, tagsPrefix: ""),
            Questionnaire(question: getLocalizedString(key: "Q21", comment: ""), questionId: 12, answers: riskAnswersQ13, answersId: riskAnswersIdQ13, isMultiSelection: false, tagsPrefix: "")
        ], isCompleted: false, questionIdPrefix: "qr")
        
        let valuesData = QuestionnaireCategoriesModel.init(category: getLocalizedString(key: "values_i_care_about"), categoryId: 3, questionsData:[
            Questionnaire(question: getLocalizedString(key: "Q22", comment: ""), questionId: 0, answers: valuesAnswersQ1, answersId: valuesAnswersIdQ1, isMultiSelection: false, tagsPrefix: getLocalizedString(key: "values_q1_tag_prefix"))
        ], isCompleted: false, questionIdPrefix: "qv")
        
        
        tableViewData.append(demographicsData)
        tableViewData.append(investmentData)
        tableViewData.append(riskData)
        tableViewData.append(valuesData)
    }
    
    private func populateTemplateValuesData() {
        
        var questionnaireData = Questionnaire(question: getLocalizedString(key: "Q22", comment: ""), questionId: 0, answers: valuesAnswersQ1, answersId: valuesAnswersIdQ1, isMultiSelection: false, tagsPrefix: getLocalizedString(key: "values_q1_tag_prefix"))
        questionnaireData.selectedAnswers = [getLocalizedString(key: "22a", comment: "")]
        questionnaireData.selectedAnswersIds = [1]
        
        let valuesData = QuestionnaireCategoriesModel.init(category: "Values", categoryId: 3, questionsData:[questionnaireData], isCompleted: true, questionIdPrefix: "qv")
        
        tableViewData[3] = valuesData
    }
    
    private func getValuesData() -> QuestionnaireCategoriesModel {
        let valuesData = QuestionnaireCategoriesModel.init(category: "Values", categoryId: 3, questionsData:[
            Questionnaire(question: getLocalizedString(key: "Q22", comment: ""), questionId: 0, answers: valuesAnswersQ1, answersId: valuesAnswersIdQ1, isMultiSelection: false, tagsPrefix: getLocalizedString(key: "values_q1_tag_prefix"))
        ], isCompleted: false, questionIdPrefix: "qv")
        
        return valuesData
    }
    
    private func getValuesOptionalData() -> QuestionnaireCategoriesModel{
        let ValuesDataOptional = QuestionnaireCategoriesModel.init(category: "Values", categoryId: 3, questionsData: [
            Questionnaire(question: getLocalizedString(key: "Q22", comment: ""), questionId: 0, answers: valuesAnswersQ1, answersId: valuesAnswersIdQ1, isMultiSelection: false, tagsPrefix: getLocalizedString(key: "values_q1_tag_prefix")),
            Questionnaire(question: getLocalizedString(key: "Q24", comment: ""), questionId: 1, answers: valuesAnswersQ2, answersId: valuesAnswersIdQ2, isMultiSelection: true, tagsPrefix: getLocalizedString(key: "values_q2_tag_prefix")),
            Questionnaire(question: getLocalizedString(key: "Q23", comment: ""), questionId: 2, answers: valuesAnswersQ3, answersId: valuesAnswersIdQ3, isMultiSelection: true, tagsPrefix: getLocalizedString(key: "values_q3_tag_prefix")),
            Questionnaire(question: getLocalizedString(key: "Q25", comment: ""), questionId: 3, answers: valuesAnswersQ4, answersId: valuesAnswersIdQ4, isMultiSelection: false, tagsPrefix: getLocalizedString(key: "values_q4_tag_prefix")),
            Questionnaire(question: getLocalizedString(key: "Q26", comment: ""), questionId: 4, answers: valuesAnswersQ5, answersId: valuesAnswersIdQ5, isMultiSelection: false, tagsPrefix: getLocalizedString(key: "values_q5_tag_prefix")),
            Questionnaire(question: getLocalizedString(key: "Q27", comment: ""), questionId: 5, answers: valuesAnswersQ6, answersId: valuesAnswersIdQ6, isMultiSelection: false, tagsPrefix: getLocalizedString(key: "values_q6_tag_prefix")),
            Questionnaire(question: getLocalizedString(key: "Q28", comment: ""), questionId: 6, answers: valuesAnswersQ7, answersId: valuesAnswersIdQ7, isMultiSelection: false, tagsPrefix: getLocalizedString(key: "values_q7_tag_prefix"))
        ], isCompleted: false, questionIdPrefix: "qv")
        
        return ValuesDataOptional
    }
    
    private func showValuesBottomSheet(){
        let optionMenu = UIAlertController(title: nil,message: getLocalizedString(key: "pick_your_investment_values"), preferredStyle: .actionSheet)
        
        let template = UIAlertAction(title: getLocalizedString(key: "suggested_by_vesgo"), style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            let storyboard = UIStoryboard(name: "InvestmentStrategy", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "InvestmentValuesTemplateVC") as! InvestmentValuesTemplateVC
            vc.templateDelegate = self
            vc.selectedTemplateIndex = QuestionnaireCalculationHelper.sharedInstance.getSelectedTemplateIndex(self.valuesApiDataModel.template)
            self.navigationController?.pushViewController(vc, animated: true)
            
            
        })
        let chooseMyOwn = UIAlertAction(title: getLocalizedString(key: "choose_my_own"), style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            self.templateType = .NOT_SET
            self.isValuesLoadedFromTemplate = false
            
            var questionnaireData = self.tableViewData[self.currentCategoryIndex].questionsData[0]
            
            questionnaireData.selectedAnswersIds.removeAll()
            questionnaireData.selectedAnswers.removeAll()
            
            questionnaireData.selectedAnswersIds.append(1)
            questionnaireData.selectedAnswers.append(questionnaireData.answers[0])
            
            self.tableViewData[3] = self.getValuesOptionalData()
            
            self.tableViewData[self.currentCategoryIndex].questionsData[0] = questionnaireData
            
            
            self.currentQuestionIndex = 1
            
            self.setViewData(categoryIndex: self.currentCategoryIndex)
        })
        
        
        
        
        optionMenu.addAction(template)
        optionMenu.addAction(chooseMyOwn)
        self.present(optionMenu, animated: true, completion: nil)
    }
}

extension InvestmentQuestionnaire {
    private func setViewData(categoryIndex: Int){
        
        if categoryIndex == 0 {
            setDemographicsViewsData()
        }else if categoryIndex == 1 {
            setDemographicsViewsData()
            setInvestmentViewsData()
        }else if categoryIndex == 2 {
            setDemographicsViewsData()
            setInvestmentViewsData()
            setRiskViewsData()
        }else if categoryIndex == 3 {
            setDemographicsViewsData()
            setInvestmentViewsData()
            setRiskViewsData()
            setValuesViewsData()
        }
        
        //currentCategoryIndex = getCurrentCategoryIndex()
        currentQuestionIndex = getCurrentQuestionIndex()
        tableView.reloadData()
    }
    
    private func getCurrentCategoryIndex() -> Int {
        var index = -1
        for i in 0..<tableViewData.count {
            if tableViewData[i].isCompleted == false {
                index = tableViewData[i].categoryId
                break
            }
        }
        
        return index
    }
    
    private func isInvestmentStrategyCompleted() -> Bool {
        var isCompleted = true
        for i in 0..<tableViewData.count {
            if tableViewData[i].isCompleted == false {
                isCompleted = false
                break
            }
        }
        
        return isCompleted
    }
    
    private func getCurrentQuestionIndex() -> Int {
        var index = 0
        for i in 0..<tableViewData[currentCategoryIndex].questionsData.count {
            if tableViewData[currentCategoryIndex].questionsData[i].selectedAnswersIds.count > 0 {
                index = i + 1
            }
        }
        return index
    }
    
    private func setDemographicsViewsData(){
        var completedQuestions = 0
        var selectedAnswers = [String]()
        for i in 0..<tableViewData[0].questionsData.count {
            if !tableViewData[0].questionsData[i].selectedAnswers.isEmpty {
                completedQuestions += 1
                selectedAnswers.append(tableViewData[0].questionsData[i].selectedAnswers[0])
            }
        }
        //print(completedQuestions)
        let completedProgress = Float(completedQuestions) / Float(tableViewData[0].questionsData.count)
        demographicsPrgressView.setProgress(completedProgress, animated: true)
        
        print(demographicsPrgressView.progress)
        
        demographicsQuestionsCountLbl.text = String(completedQuestions) + "/" + String(tableViewData[0].questionsData.count)
        
        
        if completedQuestions == tableViewData[0].questionsData.count{
            tableViewData[0].isCompleted = true
//            let viewAbleText = "Demographics (You are " + (selectedAnswers.map{String($0)}).joined(separator: ",") + ")"
//            demographicsLabel.text = viewAbleText
            demographicsLabel.textColor = .systemGreen
            demographicsPrgressView.progressTintColor = .systemGreen
            
            demographicsCheckMarkImg.image = UIImage(named: "about_me_selected")
            
        }else if completedQuestions > 0 {
            demographicsCheckMarkImg.image = UIImage(named: "about_me_grey")
            if #available(iOS 13.0, *) {
                demographicsPrgressView.progressTintColor = .systemBlue
                if currentCategoryIndex == 0 {
                    demographicsLabel.textColor = .systemBlue
                }else{
                    demographicsLabel.textColor = .opaqueSeparator
                }
                
            } else {
                // Fallback on earlier versions
            }
        }
        else{
            demographicsCheckMarkImg.image = UIImage(named: "about_me_grey")
            if #available(iOS 13.0, *) {
                demographicsPrgressView.progressTintColor = .opaqueSeparator
                if currentCategoryIndex == 0 {
                    demographicsLabel.textColor = .systemBlue
                }else{
                    demographicsLabel.textColor = .opaqueSeparator
                }
                
            } else {
                // Fallback on earlier versions
            }
        }
    }
    
    private func setInvestmentViewsData(){
        var completedQuestions = 0
        var selectedAnswers = [String]()
        for i in 0..<tableViewData[1].questionsData.count {
            if !tableViewData[1].questionsData[i].selectedAnswers.isEmpty {
                completedQuestions += 1
                selectedAnswers.append(tableViewData[1].questionsData[i].selectedAnswers[0])
            }
        }
        //print(completedQuestions)
        let completedProgress = Float(completedQuestions) / Float(tableViewData[1].questionsData.count)
        investmentPrgressView.setProgress(completedProgress, animated: true)
        
        
        investmentQuestionsCountLbl.text = String(completedQuestions) + "/" + String(tableViewData[1].questionsData.count)
        
        
        if completedQuestions == tableViewData[1].questionsData.count{
            tableViewData[1].isCompleted = true
//            let viewAbleText = "Demographics (You are " + (selectedAnswers.map{String($0)}).joined(separator: ",") + ")"
            //demographicsLabel.text = viewAbleText
            investmentLabel.textColor = .systemGreen
            investmentPrgressView.progressTintColor = .systemGreen
            investmentCheckMarkImg.image = UIImage(named: "help_me_invest_selected")
        }else if completedQuestions > 0 {
            investmentCheckMarkImg.image = UIImage(named: "help_me_invest_grey")
            if #available(iOS 13.0, *) {
                investmentPrgressView.progressTintColor = .systemBlue
                if currentCategoryIndex == 1 {
                    investmentLabel.textColor = .systemBlue
                }else{
                    investmentLabel.textColor = .opaqueSeparator
                }
                
            } else {
                // Fallback on earlier versions
            }
        }else {
            investmentCheckMarkImg.image = UIImage(named: "help_me_invest_grey")
            if #available(iOS 13.0, *) {
                investmentPrgressView.progressTintColor = .opaqueSeparator
                if currentCategoryIndex == 1 {
                    investmentLabel.textColor = .systemBlue
                }else{
                    investmentLabel.textColor = .opaqueSeparator
                }
                
            } else {
                // Fallback on earlier versions
            }
        }
    }
    
    private func setRiskViewsData(){
        var completedQuestions = 0
        var selectedAnswers = [String]()
        for i in 0..<tableViewData[2].questionsData.count {
            if !tableViewData[2].questionsData[i].selectedAnswers.isEmpty {
                completedQuestions += 1
                selectedAnswers.append(tableViewData[2].questionsData[i].selectedAnswers[0])
            }
        }
        //print(completedQuestions)
        let completedProgress = Float(completedQuestions) / Float(tableViewData[2].questionsData.count)
        riskPrgressView.setProgress(completedProgress, animated: true)
        
        
        riskQuestionsCountLbl.text = String(completedQuestions) + "/" + String(tableViewData[2].questionsData.count)
        
        
        if completedQuestions == tableViewData[2].questionsData.count || tableViewData[2].isCompleted{
            tableViewData[2].isCompleted = true
//            let viewAbleText = "Demographics (You are " + (selectedAnswers.map{String($0)}).joined(separator: ",") + ")"
            //demographicsLabel.text = viewAbleText
            riskLabel.textColor = .systemGreen
            riskPrgressView.progressTintColor = .systemGreen
            riskPrgressView.setProgress(1, animated: true)
            riskCheckMarkImg.image = UIImage(named: "my_risk_tolerance_selected")
        }else{
            riskCheckMarkImg.image = UIImage(named: "my_risk_tolerance_grey")
            if #available(iOS 13.0, *) {
                riskPrgressView.progressTintColor = .systemBlue
                if currentCategoryIndex == 2 {
                    riskLabel.textColor = .systemBlue
                }else{
                    riskLabel.textColor = .opaqueSeparator
                }
                
            } else {
                // Fallback on earlier versions
            }
        }
    }
    
    private func setValuesViewsData(){
        var completedQuestions = 0
        var selectedAnswers = [String]()
        for i in 0..<tableViewData[3].questionsData.count {
            if !tableViewData[3].questionsData[i].selectedAnswers.isEmpty {
                completedQuestions += 1
                selectedAnswers.append(tableViewData[3].questionsData[i].selectedAnswers[0])
            }
        }
        //print(completedQuestions)
        let completedProgress = Float(completedQuestions) / Float(tableViewData[3].questionsData.count)
        valuesPrgressView.setProgress(completedProgress, animated: true)
        
        
        valuesQuestionsCountLbl.text = String(completedQuestions) + "/" + String(tableViewData[3].questionsData.count)
        
        
        if completedQuestions == tableViewData[3].questionsData.count{
            tableViewData[3].isCompleted = true
//            let viewAbleText = "Demographics (You are " + (selectedAnswers.map{String($0)}).joined(separator: ",") + ")"
            //demographicsLabel.text = viewAbleText
            valuesLabel.textColor = .systemGreen
            valuesPrgressView.progressTintColor = .systemGreen
            valuesCheckMarkImg.image = UIImage(named: "values_I_care_about_selected")
        }else{
            valuesCheckMarkImg.image = UIImage(named: "values_I_care_about_grey")
            if #available(iOS 13.0, *) {
                valuesPrgressView.progressTintColor = .systemBlue
                if currentCategoryIndex == 3 {
                    valuesLabel.textColor = .systemBlue
                }else{
                    valuesLabel.textColor = .opaqueSeparator
                }
                
            } else {
                // Fallback on earlier versions
            }
        }
    }
    
    private func setInitialViewData(){
        demographicsLabel.textColor = .systemBlue
        if #available(iOS 13.0, *) {
            investmentLabel.textColor = .opaqueSeparator
            riskLabel.textColor = .opaqueSeparator
            valuesLabel.textColor = .opaqueSeparator
            
        } else {
            // Fallback on earlier versions
        }
        demographicsPrgressView.setProgress(0, animated: true)
        investmentPrgressView.setProgress(0, animated: true)
        riskPrgressView.setProgress(0, animated: true)
        valuesPrgressView.setProgress(0, animated: true)
        
        demographicsQuestionsCountLbl.text = "0/" + String(tableViewData[0].questionsData.count)
        investmentQuestionsCountLbl.text = "0/" + String(tableViewData[1].questionsData.count)
        riskQuestionsCountLbl.text = "0/" + String(tableViewData[2].questionsData.count)
        valuesQuestionsCountLbl.text = "0/" + String(tableViewData[3].questionsData.count)
        
        demographicsPrgressView.sizeThatFits(CGSize.init(width: self.demographicsPrgressView.frame.width, height: 0.5))
        investmentPrgressView.sizeThatFits(CGSize.init(width: self.investmentPrgressView.frame.width, height: 0.5))
        riskPrgressView.sizeThatFits(CGSize.init(width: self.riskPrgressView.frame.width, height: 0.5))
        valuesPrgressView.sizeThatFits(CGSize.init(width: self.valuesPrgressView.frame.width, height: 0.5))
        
        valuesLabel.fontSize = 15
        
        demographicsLabel.text = tableViewData[0].category
        investmentLabel.text = tableViewData[1].category
        riskLabel.text = tableViewData[2].category
        valuesLabel.text = tableViewData[3].category
        
        demographicsCheckMarkImg.image = UIImage(named: "about_me_grey")
        investmentCheckMarkImg.image = UIImage(named: "help_me_invest_grey")
        riskCheckMarkImg.image = UIImage(named: "my_risk_tolerance_grey")
        valuesCheckMarkImg.image = UIImage(named: "values_I_care_about_grey")
    }
}
extension InvestmentQuestionnaire {
    
    private func submitInvestmentQuestionnaire(portfolioType: String){
        view.activityStartAnimating()
        let url = ApiUrl.SET_PORTFOLIO_TYPE_API

        let namespace = SPManager.sharedInstance.getCurrentUserNamespace()
        print(namespace)

        let params: Dictionary<String, AnyObject> =
            ["namespace": namespace as AnyObject,
             "type": portfolioType as AnyObject]
        
        print(params)
        
        postManagerUser(method: "POST", isThisURLEmbdedData: false, urlString: url, parameter: params, success: {(value) in

            do {
                print(value)
                DispatchQueue.main.async {
                    SPManager.sharedInstance.saveUserPortfolioStatus(isPortfolioComplete : true)
                    if let tabbedbar = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "nav_bar_start") as? UINavigationController {
                            self.view.window?.rootViewController = tabbedbar
                        }
                }
            }
            catch {
                print("Error parsing response")
            }
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
        }, failure: {(err) in
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
            if APIManager.sharedInstance.error400{
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
            }
            if(APIManager.sharedInstance.error401) {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
            }else{
                self.showAlert(ConstantStrings.Error_msg_generic)
            }
        })
    }
    
    private func getSelectedAnswersIds(selectedAnswers: [Int]) -> [String] {
        var ids = [String]()
        for i in 0..<selectedAnswers.count {
            if selectedAnswers[i] == 1 {
                ids.append("a")
            }else if selectedAnswers[i] == 2 {
                ids.append("b")
            }else if selectedAnswers[i] == 3 {
                ids.append("c")
            }else if selectedAnswers[i] == 4 {
                ids.append("d")
            }else if selectedAnswers[i] == 5 {
                ids.append("e")
            }else if selectedAnswers[i] == 6 {
                ids.append("f")
            }else if selectedAnswers[i] == 7 {
                ids.append("g")
            }else if selectedAnswers[i] == 8 {
                ids.append("h")
            }else if selectedAnswers[i] == 9 {
                ids.append("i")
            }else if selectedAnswers[i] == 10 {
                ids.append("j")
            }else if selectedAnswers[i] == 11 {
                ids.append("k")
            }else if selectedAnswers[i] == 12 {
                ids.append("l")
            }else if selectedAnswers[i] == 13 {
                ids.append("m")
            }else if selectedAnswers[i] == 14 {
                ids.append("n")
            }
        }
        
        return ids
    }
    
    private func getSelectedAnswersIdsFromString(selectedAnswers: [String]) -> [Int] {
        var ids = [Int]()
        for i in 0..<selectedAnswers.count {
            if selectedAnswers[i] == "a" {
                ids.append(1)
            }else if selectedAnswers[i] == "b" {
                ids.append(2)
            }else if selectedAnswers[i] == "c" {
                ids.append(3)
            }else if selectedAnswers[i] == "d" {
                ids.append(4)
            }else if selectedAnswers[i] == "e" {
                ids.append(5)
            }else if selectedAnswers[i] == "f" {
                ids.append(6)
            }else if selectedAnswers[i] == "g" {
                ids.append(7)
            }else if selectedAnswers[i] == "h" {
                ids.append(8)
            }else if selectedAnswers[i] == "i" {
                ids.append(9)
            }else if selectedAnswers[i] == "j" {
                ids.append(10)
            }else if selectedAnswers[i] == "k" {
                ids.append(11)
            }else if selectedAnswers[i] == "l" {
                ids.append(12)
            }else if selectedAnswers[i] == "m" {
                ids.append(13)
            }else if selectedAnswers[i] == "n" {
                ids.append(14)
            }
        }
        
        return ids
    }
}

//Mark:- Getter APIs
private extension InvestmentQuestionnaire {
    func getDemographicsDataFromAPI() {
        DispatchQueue.main.async {
            self.view.activityStartAnimating()
        }
        let url = ApiUrl.GET_DEMOGRAPHICS_QUESTION_DATA
        let namespace = SPManager.sharedInstance.getCurrentUserNamespace()
        
        let params:Dictionary<String,AnyObject> =
            ["namespace" : namespace  as AnyObject]
        print(params)
        
        APIManager.sharedInstance.postRequest(serviceName: url, sendData: params, success: { [self] (response) in
            print(response)
            
            demographicsApiDataModel = fetchQuestionnaireGetterData(response: response)
            
            DispatchQueue.main.async {
                if demographicsApiDataModel.success {
                    updateTableViewData(category: 0, modelFile: demographicsApiDataModel)
                }
            }
            
            
            if InvestmentQuestionnaire.questionnaireStatusModel.data.investment == true {
                getInvestmentDataFromAPI()
            }else {
                if InvestmentQuestionnaire.questionnaireStatusModel.data.risk == true {
                    getRiskDataFromAPI()
                }else {
                    if InvestmentQuestionnaire.questionnaireStatusModel.data.values == true {
                        getValuesDataFromAPI()
                    }
                }
            }
                
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
        }, failure: { (data) in
            print(data)
            DispatchQueue.main.async { [self] in
                view.activityStopAnimating()
            }
//            if APIManager.sharedInstance.error400 {
//                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
//            }
//            if(APIManager.sharedInstance.error401)
//            {
//                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
//            }else{
//                self.showAlert(ConstantStrings.Error_msg_generic)
//            }
            print("Error occured")
        })
    }
    
    func getInvestmentDataFromAPI() {
        DispatchQueue.main.async {
            self.view.activityStartAnimating()
        }
        let url = ApiUrl.GET_INVESTMENT_QUESTION_DATA
        let namespace = SPManager.sharedInstance.getCurrentUserNamespace()
        
        let params:Dictionary<String,AnyObject> =
            ["namespace" : namespace  as AnyObject]
        print(params)
        
        APIManager.sharedInstance.postRequest(serviceName: url, sendData: params, success: { [self] (response) in
            print(response)
            
            investmentApiDataModel = fetchQuestionnaireGetterData(response: response)
            DispatchQueue.main.async {
                if investmentApiDataModel.success {
                    updateTableViewData(category: 1, modelFile: investmentApiDataModel)
                }
            }
            if InvestmentQuestionnaire.questionnaireStatusModel.data.risk == true {
                getRiskDataFromAPI()
            }else {
                if InvestmentQuestionnaire.questionnaireStatusModel.data.values == true {
                    getValuesData()
                }
            }
                
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
        }, failure: { (data) in
            print(data)
            DispatchQueue.main.async { [self] in
                view.activityStopAnimating()
            }
//            if APIManager.sharedInstance.error400 {
//                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
//            }
//            if(APIManager.sharedInstance.error401)
//            {
//                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
//            }else{
//                self.showAlert(ConstantStrings.Error_msg_generic)
//            }
            print("Error occured")
        })
    }
    
    func getRiskDataFromAPI() {
        DispatchQueue.main.async {
            self.view.activityStartAnimating()
        }
        let url = ApiUrl.GET_RISK_QUESTION_DATA
        let namespace = SPManager.sharedInstance.getCurrentUserNamespace()
        
        let params:Dictionary<String,AnyObject> =
            ["namespace" : namespace  as AnyObject]
        print(params)
        
        APIManager.sharedInstance.postRequest(serviceName: url, sendData: params, success: { [self] (response) in
            print(response)
            
            riskApiDataModel = fetchQuestionnaireGetterData(response: response)
            DispatchQueue.main.async {
                
                if riskApiDataModel.success {
                    updateTableViewData(category: 2, modelFile: riskApiDataModel)
                }
            }
            
            if InvestmentQuestionnaire.questionnaireStatusModel.data.values == true {
                getValuesDataFromAPI()
            }
                
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
        }, failure: { (data) in
            print(data)
            DispatchQueue.main.async { [self] in
                view.activityStopAnimating()
            }
//            if APIManager.sharedInstance.error400 {
//                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
//            }
//            if(APIManager.sharedInstance.error401)
//            {
//                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
//            }else{
//                self.showAlert(ConstantStrings.Error_msg_generic)
//            }
            print("Error occured")
        })
    }
    
    func getValuesDataFromAPI() {
        DispatchQueue.main.async {
            self.view.activityStartAnimating()
        }
        let url = ApiUrl.GET_VALUES_QUESTION_DATA
        let namespace = SPManager.sharedInstance.getCurrentUserNamespace()
        
        let params:Dictionary<String,AnyObject> =
            ["namespace" : namespace  as AnyObject]
        print(params)
        
        APIManager.sharedInstance.postRequest(serviceName: url, sendData: params, success: { [self] (response) in
            print(response)
            
            valuesApiDataModel = fetchQuestionnaireGetterData(response: response)
            DispatchQueue.main.async {
                
                if valuesApiDataModel.success {
                    updateTableViewData(category: 3, modelFile: valuesApiDataModel)
                    self.templateType = QuestionnaireCalculationHelper.sharedInstance.getSelectedTemplateFromString(valuesApiDataModel.template)
                }
            }
                
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
        }, failure: { (data) in
            print(data)
            DispatchQueue.main.async { [self] in
                view.activityStopAnimating()
            }
//            if APIManager.sharedInstance.error400 {
//                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
//            }
//            if(APIManager.sharedInstance.error401)
//            {
//                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
//            }else{
//                self.showAlert(ConstantStrings.Error_msg_generic)
//            }
            print("Error occured")
        })
    }
    
    func fetchQuestionnaireGetterData(response: [String: AnyObject]) -> QuestionnaireDataFromAPI {
        let success = response["success"] as! Bool
        let message = response["message"] as! String
        
        let data = response["selected_answers"] as! [AnyObject]
        
        let template = (response["template"] as? String) ?? ""
        
        var selectedAnswerData = [QuestionnaireSelectedDataFromAPI]()
        if data.count > 0 {
            for i in 0..<data.count {
                let item = data[i] as! [String: AnyObject]
                
                let questionId = item["question_id"] as! String
                let answerId = item["answer_id"] as! [String]
                
                selectedAnswerData.append(QuestionnaireSelectedDataFromAPI(questionId: questionId, answerIds: answerId))
            }
        }
    
        return QuestionnaireDataFromAPI(success: success, message: message, selectedAnswers: selectedAnswerData, template: template)
    }
    
    func updateTableViewData(category: Int, modelFile: QuestionnaireDataFromAPI) {
        
        if category == 3 && modelFile.selectedAnswers.count > 0{
            if modelFile.selectedAnswers.count == 1 {
                //It means template is selected or No is selected.
            }else {
                for i in 0..<modelFile.selectedAnswers.count {
                    if modelFile.selectedAnswers[i].questionId == "qv1" && modelFile.selectedAnswers[i].answerIds[0] == "a" {
                        tableViewData[3] = getValuesOptionalData()
                    }
                }
            }
        }
        var questionnaireData = tableViewData[category].questionsData
        let prefix = tableViewData[category].questionIdPrefix
        
        for i in 0..<questionnaireData.count {
            let selectedAnswersFromApi = modelFile.selectedAnswers
            for j in 0..<selectedAnswersFromApi!.count {
                let isIdMatched = selectedAnswersFromApi![j].questionId == (prefix + String(questionnaireData[i].questionId + 1))
                if isIdMatched {
                    let selectedAnswerIds = self.getSelectedAnswersIdsFromString(selectedAnswers: selectedAnswersFromApi![j].answerIds)
                    let selectedAnswers = getSelectedAnswers(categoryId: category, questionId: i, selectedAnswerIds: selectedAnswerIds)
                    
                    questionnaireData[i].selectedAnswersIds = selectedAnswerIds
                    questionnaireData[i].selectedAnswers = selectedAnswers
                }
            }
        }
        tableViewData[category].questionsData = questionnaireData
        tableViewData[category].isCompleted = true
        setViewData(categoryIndex: category)
        
        if category == 2 {
            self.riskRawScore =  QuestionnaireCalculationHelper.sharedInstance.calculateUserRawRiskScore(riskQuestionnaireData: tableViewData[category].questionsData)
        }
        
        if isInvestmentStrategyCompleted() {
            showStrategyCompletionDialogOnGetterApis()
        }
    }
    
    func getSelectedAnswers(categoryId: Int, questionId: Int, selectedAnswerIds: [Int]) -> [String] {
        var selectedAnswers = [String]()
        for i in 0..<selectedAnswerIds.count {
            let answer = tableViewData[categoryId].questionsData[questionId].answers[selectedAnswerIds[i] - 1]
            selectedAnswers.append(answer)
        }
        return selectedAnswers
    }
}
//Mark:- Setter APIs
private extension InvestmentQuestionnaire {
    func selectedAnswersIDsGenericFunc(categoryId: Int) -> [AnyObject] {
        var selectedAnswerData = [AnyObject]()
        for i in 0..<tableViewData[categoryId].questionsData.count {
            let item = tableViewData[categoryId].questionsData[i]
            let questionId = tableViewData[categoryId].questionIdPrefix + String(item.questionId + 1)
            let answerId = getSelectedAnswersIds(selectedAnswers: item.selectedAnswersIds)
            //let data = QuestionnaireSelectedDataFromAPI(questionId: questionId, answerIds: answerId)
            let selectedAnswer = [
                "question_id": questionId,
                "answer_id": answerId
            ] as [String: AnyObject]
            selectedAnswerData.append(selectedAnswer as AnyObject)
        }
        
        return selectedAnswerData
    }
    
    func selectedAnswerGenericFunc(categoryId: Int, questionsId: Int) -> String {
        return FintechUtils.sharedInstance.ArrayToString(array: tableViewData[categoryId].questionsData[questionsId].selectedAnswers)
    }
    
    func demographicsSetterAPI(){
        let url = ApiUrl.SET_DEMOGRAPHICS_QUESTION_DATA
        let namespace = SPManager.sharedInstance.getCurrentUserNamespace()
        
        let calculationParams: Dictionary<String, AnyObject> = [
            "age": selectedAnswerGenericFunc(categoryId: 0, questionsId: 0) as AnyObject,
            "gender": selectedAnswerGenericFunc(categoryId: 0, questionsId: 1) as AnyObject,
            "marital_status": selectedAnswerGenericFunc(categoryId: 0, questionsId: 2) as AnyObject
        ]
        
        let dataParams: Dictionary<String, AnyObject> = [
            "selected_answers": selectedAnswersIDsGenericFunc(categoryId: 0) as AnyObject,
            "calculation": calculationParams as AnyObject
        ]
        
        let params:Dictionary<String,AnyObject> =
            ["namespace" : namespace  as AnyObject,
             "data" : dataParams as AnyObject]
        print(params)
        
        APIManager.sharedInstance.postRequest(serviceName: url, sendData: params, success: { (response) in
            print(response)
            
            let success = response["success"] as! Bool
            
            if success {
                DispatchQueue.main.async {
                    //self.view.makeToast("Demographics data submitted successfully")
                }
            }else {
                
            }
                
            
        }, failure: { (data) in
            print(data)
            if APIManager.sharedInstance.error400 {
//                DispatchQueue.main.async {
//                    self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
//                }
            }
            if(APIManager.sharedInstance.error401)
            {
//                DispatchQueue.main.async {
//                    self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
//                }
            }else{
//                DispatchQueue.main.async {
//                    self.showAlert(ConstantStrings.Error_msg_generic)
//                }
            }
            DispatchQueue.main.async {
                self.view.makeToast("Unable to submit questionnaire. Please try again later", duration: 3.0)
            }
        })
    }
    
    func investmentSetterAPI(){
        let url = ApiUrl.SET_INVESTMENT_QUESTION_DATA
        let namespace = SPManager.sharedInstance.getCurrentUserNamespace()
        
        let calculationParams: Dictionary<String, AnyObject> = [
            "i_make": selectedAnswerGenericFunc(categoryId: 1, questionsId: 0) as AnyObject,
            "i_spend": selectedAnswerGenericFunc(categoryId: 1, questionsId: 1) as AnyObject,
            "i_owe": selectedAnswerGenericFunc(categoryId: 1, questionsId: 2) as AnyObject,
            "investment_objectives": selectedAnswerGenericFunc(categoryId: 1, questionsId: 3) as AnyObject,
            "investment_horizon": selectedAnswerGenericFunc(categoryId: 1, questionsId: 4) as AnyObject
        ]
        
        let dataParams: Dictionary<String, AnyObject> = [
            "selected_answers": selectedAnswersIDsGenericFunc(categoryId: 1) as AnyObject,
            "calculation": calculationParams as AnyObject
        ]
        
        let params:Dictionary<String,AnyObject> =
            ["namespace" : namespace  as AnyObject,
             "data" : dataParams as AnyObject]
        print(params)
        
        APIManager.sharedInstance.postRequest(serviceName: url, sendData: params, success: { (response) in
            print(response)
            
            let success = response["success"] as! Bool
            
            if success {
                DispatchQueue.main.async {
                    //self.view.makeToast("Investment data submitted successfully")
                }
            }else {
                
            }
                
            
        }, failure: { (data) in
            print(data)
//            if APIManager.sharedInstance.error400 {
//                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
//            }
//            if(APIManager.sharedInstance.error401)
//            {
//                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
//            }else{
//                self.showAlert(ConstantStrings.Error_msg_generic)
//            }
            DispatchQueue.main.async {
                self.view.makeToast("Unable to submit questionnaire. Please try again later", duration: 3.0)
            }
        })
    }
    
    func riskSetterAPI(){
        let url = ApiUrl.SET_RISK_QUESTION_DATA
        let namespace = SPManager.sharedInstance.getCurrentUserNamespace()
        
        
        let calculationParams: Dictionary<String, AnyObject> = [
            "raw_score": riskRawScore as AnyObject,
            "type": QuestionnaireCalculationHelper.sharedInstance.getUserRiskType(rawScore: riskRawScore) as AnyObject,
            "scaled_score": QuestionnaireCalculationHelper.sharedInstance.getUserRiskScaledScore(rawScore: riskRawScore) as AnyObject
        ]
        
        let dataParams: Dictionary<String, AnyObject> = [
            "selected_answers": selectedAnswersIDsGenericFunc(categoryId: 2) as AnyObject,
            "calculation": calculationParams as AnyObject
        ]
        
        let params:Dictionary<String,AnyObject> =
            ["namespace" : namespace  as AnyObject,
             "data" : dataParams as AnyObject]
        print(params)
        
        APIManager.sharedInstance.postRequest(serviceName: url, sendData: params, success: { (response) in
            print(response)
            
            let success = response["success"] as! Bool
            
            if success {
                DispatchQueue.main.async {
                    //self.view.makeToast("Risk data submitted successfully")
                }
            }else {
                
            }
                
            
        }, failure: { (data) in
            print(data)
//            if APIManager.sharedInstance.error400 {
//                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
//            }
//            if(APIManager.sharedInstance.error401)
//            {
//                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
//            }else{
//                self.showAlert(ConstantStrings.Error_msg_generic)
//            }
            DispatchQueue.main.async {
                self.view.makeToast("Unable to submit questionnaire. Please try again later", duration: 3.0)
            }
        })
    }
    
    func getInvestmentValuesCalculationParams() -> Dictionary<String, AnyObject> {
        let valuesQuestionnaire = tableViewData[3].questionsData
        
        if isValuesLoadedFromTemplate {
            switch templateType {
                case .Catholic:
                    return QuestionnaireCalculationHelper.sharedInstance.getCatholicTemplateParams()
                    
                case .Environment:
                    return QuestionnaireCalculationHelper.sharedInstance.getEnvTemplateParams()
                    
                case .Islamic:
                    return QuestionnaireCalculationHelper.sharedInstance.getIslamicTemplateParams()
                    
                case .Methodist:
                    return QuestionnaireCalculationHelper.sharedInstance.getMethodistTemplateParams()
                    
                case .Peace:
                    return QuestionnaireCalculationHelper.sharedInstance.getPeaceTemplateParams()
                    
                case .Vegan:
                    return QuestionnaireCalculationHelper.sharedInstance.getVeganTemplateParams()
                    
                default:
                    return Dictionary<String, AnyObject>()
            }
        }
        
        if valuesQuestionnaire.count == 1 {
           return ["investment_values": QuestionnaireCalculationHelper.sharedInstance.getInvestmentValue(userAnswer: getSelectedAnswersIds(selectedAnswers: valuesQuestionnaire[0].selectedAnswersIds)) as AnyObject,
                   "positive_values": [String]() as AnyObject,
                   "negative_values": [String]() as AnyObject,
                   "esg_percentile": 0,
                   "env_percentile": 0,
                   "soc_percentile": 0,
                   "gov_percentile": 0,
                   "minorities": 0,
                   "interest_bearing": 0,
                   "template": ""] as [String: AnyObject]
        }else {
            return ["investment_values": QuestionnaireCalculationHelper.sharedInstance.getInvestmentValue(userAnswer: getSelectedAnswersIds(selectedAnswers: valuesQuestionnaire[0].selectedAnswersIds)),
                    "positive_values": valuesQuestionnaire[1].selectedAnswers,
                    "negative_values": QuestionnaireCalculationHelper.sharedInstance.getResultantNegativeValues(valuesModel: valuesQuestionnaire),
                    "esg_percentile": QuestionnaireCalculationHelper.sharedInstance.getEsgPercentile(userAnswer: getSelectedAnswersIds(selectedAnswers: valuesQuestionnaire[4].selectedAnswersIds)),
                    "env_percentile": QuestionnaireCalculationHelper.sharedInstance.getEnvPercentile(positiveValues: valuesQuestionnaire[1].selectedAnswers),
                    "soc_percentile": QuestionnaireCalculationHelper.sharedInstance.getSocialPercentile(positiveValues: valuesQuestionnaire[1].selectedAnswers),
                    "gov_percentile": QuestionnaireCalculationHelper.sharedInstance.getGovernancePercentile(valuesModel: valuesQuestionnaire),
                    "minorities": QuestionnaireCalculationHelper.sharedInstance.getMinorityOwnedPercentile(userAnswer: getSelectedAnswersIds(selectedAnswers: valuesQuestionnaire[3].selectedAnswersIds)),
                    "interest_bearing": QuestionnaireCalculationHelper.sharedInstance.getInterestBearingValue(negativeValues: valuesQuestionnaire[2].selectedAnswers) ]
                        as [String: AnyObject]
        }
        
    }
    
    func valuesSetterAPI(){
        let url = ApiUrl.SET_VALUES_QUESTION_DATA
        let namespace = SPManager.sharedInstance.getCurrentUserNamespace()
        
        let calculationParams = getInvestmentValuesCalculationParams()
        
        let dataParams: Dictionary<String, AnyObject> = [
            "selected_answers": selectedAnswersIDsGenericFunc(categoryId: 3) as AnyObject,
            "calculation": calculationParams as AnyObject
        ]
        
        let params:Dictionary<String,AnyObject> =
            ["namespace" : namespace  as AnyObject,
             "data" : dataParams as AnyObject]
        print(params)
        
        APIManager.sharedInstance.postRequest(serviceName: url, sendData: params, success: { (response) in
            print(response)
            
            let success = response["success"] as! Bool
            
            if success {
                DispatchQueue.main.async {
                    //self.view.makeToast("Values data submitted successfully")
                }
            }else {
                
            }
                
            
        }, failure: { (data) in
            print(data)
//            if APIManager.sharedInstance.error400 {
//                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
//            }
//            if(APIManager.sharedInstance.error401)
//            {
//                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
//            }else{
//                self.showAlert(ConstantStrings.Error_msg_generic)
//            }
            DispatchQueue.main.async {
                self.view.makeToast("Unable to submit questionnaire. Please try again later", duration: 3.0)
            }
        })
    }
    
    private func callSetterApiBasedOnCategoryIndex() {
        if currentCategoryIndex == 0 {
            demographicsSetterAPI()
        }else if currentCategoryIndex == 1 {
            investmentSetterAPI()
        }else if currentCategoryIndex == 2 {
            let riskRawScore = QuestionnaireCalculationHelper.sharedInstance.calculateUserRawRiskScore(riskQuestionnaireData: tableViewData[2].questionsData)
            self.riskRawScore = riskRawScore
            riskSetterAPI()
        }else if currentCategoryIndex == 3 {
            valuesSetterAPI()
        }
    }
}
extension InvestmentQuestionnaire: TemplateDelgate {
    func getTemplateType(type: ValuesTemplate) {
        isValuesLoadedFromTemplate = true
        templateType = type
        
        self.populateTemplateValuesData()
        
        self.callSetterApiBasedOnCategoryIndex()
        
        self.setViewData(categoryIndex: 3)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.showCompleteDialogIfComplete()
        }
    }
    
}
