//
//  listOfCharitiesVC.swift
//  Fintech
//
//  Created by Apple on 21/12/2020.
//  Copyright © 2020 Broadstone Technologies. All rights reserved.
//

import UIKit
var listofcharity:[MessagelistofCharites]?
var List_values = [String]()
var count_LIST_VALUES:Int = 0
var integerArray = [Int]()
var charityidarray = [String]()
var LIST_OFCHARITES_ALL_VALUES : [MessagelistofCharites]?
var SelectedUserValues =  [String]()
class listOfCharitiesVC: PaymentsBaseClass {
    
    var CHARITIES_LIST_COUNT : Int = 0
    
    var countLabel: UILabel!
    
    
    @IBOutlet weak var main_Heading_lbl: UILabel!
    
    @IBOutlet weak var listofcharity_tableview: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false

        ArrayLists.sharedInstance.SelectedListOfCharies.removeAll()
        SelectedUserValues.removeAll()
        List_values.removeAll()
        listofcharity?.removeAll()
        charityidarray.removeAll()
        
        createNextButton(title: getLocalizedString(key: "_next"))
        countLabel.text = String(ArrayLists.sharedInstance.SelectedListOfCharies.count)
        
          backtestcallapi()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setNavBarTitile(title: "", topItem: "Donate")
        localization()
    }
    
    func localization(){
        let heading_charities_to_donate_lbl = getLocalizedString(key: "Choose_from_these_charities_to_donate_lbl", comment: "")
        
        main_Heading_lbl.text = heading_charities_to_donate_lbl


    }
    
    func createNextButton(title: String){
        //let label = UILabel(frame: CGRect(x: 55, y: 0, width: 20, height: 16))
        let label = UILabel(frame: CGRect(x: 60, y: 7, width: 30, height: 20))
        label.layer.borderColor = UIColor.clear.cgColor
        label.layer.borderWidth = 2
        label.layer.cornerRadius = label.bounds.size.height / 2
        //label.layer.cornerRadius = 5
        label.textAlignment = .center
        label.layer.masksToBounds = true
        label.font = UIFont(name: "HelveticaNeue-Light", size: 13)
        //label.textColor = UIColor(named: "AppMainColor")
        label.textColor = .black
        label.backgroundColor = .white
        label.text = String(ArrayLists.sharedInstance.SelectedListOfCharies.count)
        
        self.countLabel = label

          // button
        let rightButton = UIButton(frame: CGRect(x: 0, y: 0, width: 80, height: 16))
        rightButton.setTitle(title, for: .normal)
        rightButton.addTarget(self, action: #selector(next_btn(_:)), for: .touchUpInside)
        rightButton.addSubview(countLabel)
        
        let rightBarButtomItem = UIBarButtonItem(customView: rightButton)
        self.navigationItem.rightBarButtonItem = rightBarButtomItem
    }
    
    @IBAction func next_btn(_ sender: Any) {
        count_LIST_VALUES = List_values.count
        if count_LIST_VALUES == 0 {
            self.view.makeToast("Please select atleast one charity")

        }else{
            PaymentAPIUtils.isEqualDistribution = false
            let storyboard = UIStoryboard(name: "Charity", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "customizedDistributionVC") as! customizedDistributionVC
            self.navigationController?.pushViewController(vc, animated: true)
//        let storyboard = UIStoryboard(name: "Charity", bundle: nil)
//        let vc = storyboard.instantiateViewController(withIdentifier: "CharityMethodVC") as! CharityMethodVC
//        // self.navigationController?.pushViewController(vc, animated: true)
//        count_LIST_VALUES = List_values.count
//        self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension listOfCharitiesVC {
    func  backtestcallapi(){

        self.view.activityStartAnimating()
        
        
        let url = ApiUrl.GET_lIST_OF_CHARITY_URL
        
        var namespace = ""
//        var charset = CharacterSet(charactersIn: "gst-one1")
        
        if SPManager.sharedInstance.saveUserLoginStatus() {
            //if namespace.rangeOfCharacter(from: charset) != nil {
            namespace = SPManager.sharedInstance.getCurrentUserNamespace()
        } else {
            self.view.makeToast(getLocalizedString(key: "user_not_logged_in", comment: ""))
        }
        postManagerUser(method: "POST", isThisURLEmbdedData: false, urlString: url, parameter: ["namespace": namespace], success: {(value) in
            
            do
            {
                
                let jsonObject = try JSONDecoder().decode(ListofcharitesModel?.self, from: value)
                
                DispatchQueue.main.async {
                    if let value = jsonObject?.message
                    {
                        listofcharity = value
                        for i in 0..<listofcharity!.count
                        {
                            print(listofcharity?[i].userValue)
                            
                            self.CHARITIES_LIST_COUNT += (listofcharity?[i].charities?.count)!
                            
                            print(listofcharity?[i].charities?.count)
                            
                        }
                        
                        
          
                       print(listofcharity)
                    
                        if listofcharity?.count ?? 0 > 0 {
                        self.listofcharity_tableview.reloadData()
                        } else {
                            DispatchQueue.main.async {                                
                                
                                if listofcharity?.count ?? 0 > 0 {
                                    self.listofcharity_tableview.reloadData()
                                } else {
                                    DispatchQueue.main.async {
                                        self.view.makeToast(ConstantStrings.NO_ALIGNED_CHARITIES_FOUNND, duration: 2.0, position: .center)
                                        DispatchQueue.main.asyncAfter(deadline: .now() + 2.1) {
                                            self.navigationController?.popViewController(animated: true)
                                        }
                                    }
                                }
                                
                            }
                        }
                    }
                    DispatchQueue.main.async {
                        self.view.activityStopAnimating()
                    }
                    
                }
                
            }
            catch
            {
                
            }
            
        }, failure: {(err) in
            
           
            if APIManager.sharedInstance.error400 {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
            }
            if(APIManager.sharedInstance.error401)
            {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
            }else{
                
            }
            
        })
    }
    
    
}
// MARK: - Tableview
extension listOfCharitiesVC: UITableViewDelegate,UITableViewDataSource, ListOfCharitiesBtnDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       // return listofcharity?.count ?? 0 + CHARITIES_LIST_COUNT
        
        return (listofcharity?[section].charities?.count ?? 0) + 1
        
       // return listofcharity?[0].userValue?.count ?? 0 + (listofcharity?[0].charities?.count)!
        //return 0
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return listofcharity?.count ?? 0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let rowIndex = indexPath.row
        let item = listofcharity?[indexPath.section]
//        for i in item?.charities?[0].charityID ?? ""
//          {
//            charityidarray.append("")
//        }
//
        //print("countlist\(charityidarray)")
        if indexPath.row == 0{
            let cell =  self.listofcharity_tableview.dequeueReusableCell(withIdentifier: "cellHeading") as! listOfCharitiesCell
            
            cell.heading_main_lbl.text = "\(item!.userValue ?? " ")" + " (\(item?.charities?.count ?? 0))"
            //"(\(item?.charities?.count))"
                    return cell
        }
        
        var items = item?.charities?[indexPath.row-1]
        var countcharityname = items?.charityName?.count
       
        let cell =  self.listofcharity_tableview.dequeueReusableCell(withIdentifier: "cell") as! listOfCharitiesCell
        
        cell.tick_img.isHidden = true
        cell.heading_lbl.text = items?.charityName ?? ""
        cell.descripation_lbl.text = items?.charityDescription
        cell.readmore_btn.setTitle("...", for: .normal)
        cell.indexPath = indexPath
        cell.other_Values_charity.isHidden = true
        cell.delegate = self

        
        if items!.otherValues!.count == 0 {
            cell.other_Values_charity.isHidden = true
            
        }else {
            var otherAssociatedValues = FintechUtils.sharedInstance.ArrayToString(array: items!.otherValues!)
            
            cell.other_Values_charity.isHidden = false
            cell.other_Values_charity.text =  "This charity also aligned with: \(otherAssociatedValues)"

        }
        
        if charityidarray.contains(items?.charityID ?? "")
        {
            cell.tick_img.isHidden = false

        }else{
            cell.tick_img.isHidden = true

        }

        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! listOfCharitiesCell
        if indexPath.row == 0{
        }else{
        var indexvaribale = -1
        let item = listofcharity?[indexPath.section]
        var items = item?.charities?[indexPath.row-1]
           indexvaribale = indexPath.section

            for i in items?.charityID ?? ""
            {
                if charityidarray.contains(items?.charityID ?? "") {
                    let index = List_values.index(of: items?.charityName ?? " ")
                    List_values.remove(object: items?.charityName ?? " ")
                    charityidarray.remove(object: items?.charityID ?? "")
                    
                    ArrayLists.sharedInstance.SelectedListOfCharies.remove(at: index!)
                    SelectedUserValues.remove(at: index!)
                    
                }else{
                    charityidarray.append(items?.charityID ?? "")
                    List_values.append(items?.charityName ?? " ")
                    
                    ArrayLists.sharedInstance.SelectedListOfCharies.append(items!)
                    SelectedUserValues.append((item?.userValue)!)


                }
            }
            
            print(charityidarray)

            countLabel.text = String(ArrayLists.sharedInstance.SelectedListOfCharies.count)
        listofcharity_tableview.reloadData()
        
        
        }
    }
    
    func readMoreBtnPressed(at path: IndexPath) {
        let sectionData = listofcharity![path.section]
        let rowData = sectionData.charities![path.row - 1]
        let url = rowData.url ?? ""
        if url == "" {
            self.view.makeToast("No more information")
            return
        }
        FintechUtils.sharedInstance.openUrl(url: url)
    }
}
