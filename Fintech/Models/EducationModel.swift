

import Foundation

class EducationModel{
    var id = Int()
    var mainHeadingText = String()
    var subHeadingText = String()
    var image = String()
    var type = String()
    var sortedID = Int()
    var bgColor = String()
    init(id:Int, heading:String, subheading:String,image: String, type: String, sortedID: Int, bgColor: String) {
        self.id = id
        mainHeadingText = heading
        subHeadingText = subheading
        self.image = image
        self.type = type
        self.sortedID = sortedID
        self.bgColor = bgColor
    }
}
class EducationDetailModel{
    var id = Int()
    var infoHeading = String()
    var infoSubHeading = String()
    var infoDescription = String()
    var infoDisclaimerImage = String()
    var infoDisclaimerText = String()
    init(id : Int,heading: String,subheading:String,des:String,image:String,Disclaimer:String) {
        self.id = id
        infoHeading = heading
        infoSubHeading = subheading
        infoDescription = des
        infoDisclaimerImage = image
        infoDisclaimerText = Disclaimer
    }
}

class LessonAnswerModel{
    var id = Int()
    var qid = String()
    var lesson = String()
    
    init(id : Int,qID: String,Lesson:String) {
        self.id = id
        qid = qID
        lesson = Lesson
    }
    
}


