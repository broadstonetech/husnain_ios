//
//  GetCharitablePlanModel.swift
//  Fintech
//
//  Created by broadstone on 06/01/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let getCharitablePlanModel = try? newJSONDecoder().decode(GetCharitablePlanModel.self, from: jsonData)

import Foundation

// MARK: - GetCharitablePlanModel
struct GetCharitablePlanModel: Codable {
    let success: Int
    let hasDonated: Bool
    let planData: PlanData

    enum CodingKeys: String, CodingKey {
        case success
        case hasDonated = "has_donated"
        case planData = "message"
    }
}

// MARK: - Message
struct PlanData: Codable {
    let donatedAmount, plannedAmount: Double
    let targetDate: Int64
    let timestamp: Int64?

    enum CodingKeys: String, CodingKey {
        case donatedAmount = "donated_amount"
        case plannedAmount = "planned_amount"
        case targetDate = "target_date"
        case timestamp
    }
}
