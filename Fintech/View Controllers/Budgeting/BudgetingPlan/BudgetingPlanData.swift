//
//  BudgetingPlanData.swift
//  Fintech
//
//  Created by broadstone on 05/07/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import Foundation

struct BudgetingPlanData {
    var totalAmount = 0.0
    var startDate: Int64 = 0
    var endDate: Int64 = 0
    var needsBucket: BudgetingPlanBucketData
    var wantsBucket: BudgetingPlanBucketData
    var havesBucket: BudgetingPlanBucketData
}

struct BudgetingPlanBucketData {
    var bucketAmount = 0.0
    var bucketPercentage = 0.0
    var bucketCategories = [BucketCategoryData]()
}

struct BucketCategoryData {
    var index = 0
    var amount = 0.0
    var label = ""
}
