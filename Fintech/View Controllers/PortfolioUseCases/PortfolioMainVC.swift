//
//  PortfolioMainVC.swift
//  Fintech
//
//  Created by broadstone on 08/08/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import UIKit

class PortfolioMainVC: PortfolioParent {
    
    @IBOutlet weak var useCase1Label: UILabel!
    @IBOutlet weak var useCase1Desc: UILabel!
    
    @IBOutlet weak var brokerLabel: UILabel!
    @IBOutlet weak var manualEntryLabel: UILabel!
    
    @IBOutlet weak var useCase3Label: UILabel!
    @IBOutlet weak var useCase3Desc: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        localization()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        //setNavBarTitile(title: "Back", topItem: getLocalizedString(key: "_integrations"))
    }
    
    private func localization() {
        //useCase1Label.text = getLocalizedString(key: "investmnet_check")
        //useCase1Desc.text = getLocalizedString(key: "find_invesmtment_check")
        brokerLabel.text = getLocalizedString(key: "connect_to_broker")
        manualEntryLabel.text = getLocalizedString(key: "manual_entry")
        useCase3Label.text = getLocalizedString(key: "want_build_portfolio")
        useCase3Desc.text = getLocalizedString(key: "build_value_based_portfolio")
    }
    
    
    @IBAction func useCase1Pressed(_ sender: UIButton) {
        if SPManager.sharedInstance.saveUserLoginStatus() {
            getPortfolioList(useCaseType: .UseCase1, isNavigatingBack: false)
        }else {
            self.view.makeToast(getLocalizedString(key: "join_now_for_portfolio_integration"))
        }

    }
    
    @IBAction func useCase2Pressed(_ sender: UIButton) {
        if SPManager.sharedInstance.saveUserLoginStatus() {
            getPortfolioList(useCaseType: .UseCase2, isNavigatingBack: false)
        }else {
            self.view.makeToast(getLocalizedString(key: "join_now_for_portfolio_integration"))
        }
        
    }
    
    @IBAction func useCase3Pressed(_ sender: UIButton) {
        if SPManager.sharedInstance.saveUserLoginStatus() {
            getPortfolioList(useCaseType: .UseCase3, isNavigatingBack: false)
        }else {
            self.view.makeToast(getLocalizedString(key: "join_now_for_portfolio_integration"))
        }
    }
}
