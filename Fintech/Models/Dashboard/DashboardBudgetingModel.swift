// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let dashboardBudgetingModel = try? newJSONDecoder().decode(DashboardBudgetingModel.self, from: jsonData)

import Foundation

// MARK: - DashboardBudgetingModel
struct DashboardBudgetingModel: Codable {
    let success: Bool
    let message: String
    let data: DashboardBudgetingData
}

// MARK: - DataClass
struct DashboardBudgetingData: Codable {
    let plannedStartDate, plannedTargetDate: Int64
    let amountSpent, plannedAmount: Double
    let isPlanExist: Bool
    

    enum CodingKeys: String, CodingKey {
        case plannedStartDate = "planned_start_date"
        case plannedTargetDate = "planned_target_date"
        case amountSpent = "amount_spent"
        case plannedAmount = "planned_amount"
        case isPlanExist  = "is_plan_exist"
    }
}
