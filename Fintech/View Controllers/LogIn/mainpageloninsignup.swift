import UIKit
import AuthenticationServices

var backpostion = ""
class mainpageloninsignup: Parent,UICollectionViewDelegate,UICollectionViewDataSource {
    // MARk: - IB outlets
    @IBOutlet weak var slider_collectionView: UICollectionView!
    @IBOutlet weak var Sign_with_apple_view: UIView!
    @IBOutlet weak var page_View: UIPageControl!
    @IBOutlet weak var signup_btn: UIButton!
    @IBOutlet weak var login_btn: UIButton!
    
    @IBOutlet weak var orConnectWithLabel: UILabel!
    
    //Mark: - class variables
    var  arr1 = ["img_1","img_2","img_3"]
    var timer = Timer()
    var counter = 0
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        createAppleLoginBtn()
        page_View.numberOfPages = arr1.count
        page_View.currentPage = 0
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        localization()
    }
    func localization(){
        
        
        login_btn.setTitle(getLocalizedString(key: "Login", comment: ""), for: .normal)
        signup_btn.setTitle(getLocalizedString(key: "Sign up", comment: ""), for: .normal)
        orConnectWithLabel.text = getLocalizedString(key: "or_connect_with", comment: "")
}

    //MARK : -CLASS FUNCS
    @objc func updateNotification(){
        if counter < arr1.count{
            let index = IndexPath.init(item: counter, section: 0)
            self.slider_collectionView.scrollToItem(at: index, at:.centeredHorizontally , animated: true)
            page_View.currentPage = counter
            counter += 1
        }else{
            counter = 0
            let index = IndexPath.init(item: counter, section: 0)
            self.slider_collectionView.scrollToItem(at: index, at:.centeredHorizontally , animated: false)
            page_View.currentPage = counter
        }
        
    }
    //MARK :- IB ACTIONS
    @IBAction func Cross_pressed(_ sender: Any) {
        if backpostion == "invesment"{
            if let tabbedbar = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "nav_bar_start") as? UINavigationController {
                backpostion = ""
                tabbedbar.modalPresentationStyle = .fullScreen
                UIApplication.shared.keyWindow?.rootViewController = tabbedbar
            }
        }else if backpostion == "account"{
            if let tabbedbar = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "nav_bar_start") as? UINavigationController {
                self.tabBarController?.selectedIndex = 4

               // backpostion = ""
                
                tabbedbar.modalPresentationStyle = .fullScreen
                UIApplication.shared.keyWindow?.rootViewController = tabbedbar
            }
        }
        else{
            if let tabbedbar = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "mainview") as? UINavigationController {
                tabbedbar.modalPresentationStyle = .fullScreen
                UIApplication.shared.keyWindow?.rootViewController = tabbedbar
            }
        }
        
        
        // mainview
    }
    @IBAction func Login_button(_ sender: Any) {
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "LogInVC") as!LogInVC
        vc.modalPresentationStyle = .fullScreen
        vc.modalTransitionStyle = .partialCurl
        UIApplication.shared.keyWindow?.rootViewController = vc
        
    }
    
    @IBAction func signup(_ sender: Any) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "SignUp") as!SignUpVC
        vc.modalPresentationStyle = .fullScreen
        
        vc.modalTransitionStyle = .partialCurl
        UIApplication.shared.keyWindow?.rootViewController = vc
    }
    func createAppleLoginBtn() {
        if #available(iOS 13.0, *) {
            let authorizationButton = ASAuthorizationAppleIDButton()
            authorizationButton.isUserInteractionEnabled = true
            authorizationButton.addTarget(self, action:
                #selector(handleAuthorizationAppleIDButtonPress),
                                          for: .touchUpInside)
            self.Sign_with_apple_view.addSubview(authorizationButton)
            authorizationButton.translatesAutoresizingMaskIntoConstraints = false
//            authorizationButton.centerXAnchor.constraint(equalTo: self.Sign_with_apple_view.centerXAnchor).isActive = true
            authorizationButton.topAnchor.constraint(equalTo: self.Sign_with_apple_view.topAnchor).isActive = true

            
            
            
            authorizationButton.trailingAnchor.constraint(equalTo: self.Sign_with_apple_view.trailingAnchor, constant: -30).isActive = true
            authorizationButton.leadingAnchor.constraint(equalTo: self.Sign_with_apple_view.leadingAnchor, constant: 30).isActive = true
            authorizationButton.topAnchor.constraint(equalTo: self.Sign_with_apple_view.topAnchor).isActive = true
            authorizationButton.bottomAnchor.constraint(equalTo: self.Sign_with_apple_view.bottomAnchor).isActive = true
//
            
//            authorizationButton.bottomAnchor.constraint(equalTo: self.Sign_with_apple_view.bottomAnchor, constant: 0).isActive = true
//            authorizationButton.topAnchor.constraint(equalTo: self.Sign_with_apple_view.topAnchor, constant: 0).isActive = true

        } else {
            // Fallback on earlier versions
        }

    }

    @objc func handleAuthorizationAppleIDButtonPress() {
        print("Apple login btn pressed")
        if #available(iOS 13.0, *) {
            let provider = ASAuthorizationAppleIDProvider()
            let request = provider.createRequest()
            request.requestedScopes = [.fullName, .email]

            let controller = ASAuthorizationController(authorizationRequests: [request])

            controller.delegate = self
            controller.presentationContextProvider = self

            controller.performRequests()

        } else {
            // Fallback on earlier versions
        }

    }

}

extension mainpageloninsignup: ASAuthorizationControllerDelegate {
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {

        switch authorization.credential {

        case let credentials as ASAuthorizationAppleIDCredential:
            let user = AppleUserData(credentials: credentials)
            print(user.debugDescription)

            if user.token != nil {
                let tokenString = NSString(data: user.token!, encoding: String.Encoding.utf8.rawValue)!
                callLoginApi(username: user.email, password: user.email.sha1(), firstName: user.firstName, lastName: user.lastName, apple_token: tokenString as String)
            }
//            else if  {
//                print("user details are fetched")
//                callLoginApi(username: user.email, password: user.email.sha1(), firstName: user.firstName, lastName: user.lastName)
//            }
            else{
                print("user blocked access")
                self.showAlert(ConstantStrings.APPLE_SININ_REVOKED)
            }
        default: break

        }
    }

    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        print("something bad happened", error)
    }
}
extension mainpageloninsignup: ASAuthorizationControllerPresentationContextProviding {
    @available(iOS 13.0, *)
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {

        return view.window!
    }


}
extension mainpageloninsignup {
    // MARK: -Save apple credentials API
    func callLoginApi(username : String, password : String, firstName : String, lastName : String, apple_token : String) {
        self.view.activityStartAnimating()
        let url = ApiUrl.LOGIN_URL
        let data:Dictionary<String,AnyObject> = ["first_name" : firstName as AnyObject, "last_name" : lastName as AnyObject, "username": username as AnyObject, "auth_token" : FintechUtils.sharedInstance.DEVICE_TOKEN as AnyObject, "password": password as AnyObject, "device_type": "ios" as AnyObject, "device_token":  FintechUtils.sharedInstance.DEVICE_TOKEN as AnyObject, "login_type" : "apple" as AnyObject, "joined_timestamp": getTimestamp() as AnyObject , "apple_token" : apple_token as AnyObject]
        let params:Dictionary<String,AnyObject> = ["login_type" : "apple" as AnyObject, "data" : data as AnyObject]
        print(params)
        APIManager.sharedInstance.postRequest(serviceName: url, sendData: params, success: { (response) in
            print(response)
            if APIManager.sharedInstance.isSuccess {
                let res = response["message"] as AnyObject
                if res["namespace"] != nil {
                    SPManager.sharedInstance.saveUserLoginStatus(isLogin: true)
                    DispatchQueue.main.async {
                        self.fetchLoginDetails(res: res)
                    }
                } else {
                    DispatchQueue.main.async {
                        self.showAlert(ConstantStrings.INVALID_USERNAME_OR_PASS_ALERT_TEXT)
                    }
                }
            }else{
                DispatchQueue.main.async {
                    self.showAlert(ConstantStrings.LOGIN_FAILED_ALERT_TEXT)
                }
            }
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
        }, failure: { (data) in
            print(data)
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
            if APIManager.sharedInstance.error400 {
                DispatchQueue.main.async {
                    self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
                }
            }
            if(APIManager.sharedInstance.error401)
            {
                DispatchQueue.main.async {
                    self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
                }
            }else{
            }
        })
    }
    func fetchLoginDetails(res : AnyObject) {
        let nameSpace = res["namespace"] as! String
        let firstname = res["first_name"] as! String
        let lastname = res["last_name"] as! String
        let email = res["email"] as! String
        let questionnaireStatus = res["questionnaire_status"] as! String
        let questionnaireExpiry = res["questionnaire_expiry"] as! Int
        SPManager.sharedInstance.saveCurrentUserEmail(email)
        SPManager.sharedInstance.saveCurrentUserNameForProfile(firstname)
        SPManager.sharedInstance.saveUserFullName(firstname+" "+lastname)
        SPManager.sharedInstance.saveCurrentUserNamespace(nameSpace)
        SPManager.sharedInstance.saveCurrentUserQuestionaireExpiry(expiry: String(describing: questionnaireExpiry))
        SPManager.sharedInstance.saveCurrentUserQuestionaireStatus(status: questionnaireStatus)
        DispatchQueue.main.async {
            if questionnaireStatus == "pending" { //go to questionnaire
                SPManager.sharedInstance.saveUserPortfolioStatus(isPortfolioComplete : false)
                let alert = UIAlertController(title: ConstantStrings.appName, message: ConstantStrings.user_status_pending_movequestionpage, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: { action in
                    switch action.style{
                    case .cancel:
                        print("cancel")
                        if let tabbedbar = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "nav_bar_start") as? UINavigationController {
                            UIApplication.shared.keyWindow?.rootViewController = tabbedbar
                        }
                    case .default:
                        print("default")
                    case .destructive:
                        print("destructive")
                    }}))
                alert.addAction(UIAlertAction(title: "yes", style: .default, handler: { action in
                    self.getQuestionnaireStatusAPI()
                }))
                self.present(alert, animated: true, completion: nil)
//                InvestmentStrategyVars.isFromAccountVC = false
//                let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                let vc = storyboard.instantiateViewController(withIdentifier: "homesurfay") as UIViewController
//                vc.modalPresentationStyle = .fullScreen
//                UIApplication.shared.keyWindow?.rootViewController = vc

            } else { // already built the strategy
                SPManager.sharedInstance.saveUserPortfolioStatus(isPortfolioComplete : true)
                if let tabbedbar = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "nav_bar_start") as? UINavigationController {
                    tabbedbar.modalPresentationStyle = .fullScreen
                    UIApplication.shared.keyWindow?.rootViewController = tabbedbar
                }
            }
        }
    }

}
extension mainpageloninsignup{
    //MARK:- COLLECTION VIEW DELEGATES
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arr1.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! MainPageLoginCell
        cell.Slider_img.image = UIImage(named: arr1[indexPath.row])
        cell.sliderAnimation_view.animationSpeed = 1
        cell.sliderAnimation_view.play()
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        page_View.currentPage = arr1.count
    }
}
