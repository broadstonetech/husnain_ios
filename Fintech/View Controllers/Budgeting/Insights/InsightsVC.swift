//
//  InsightsVC.swift
//  Fintech
//
//  Created by broadstone on 29/01/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import UIKit
class InsightsVC: BudgetingParent {
    struct TableViewData {
        var opened: Bool
        var title: String
        var desc: String
        var data: Any
    }
    
    
    var tableViewDataArray = [TableViewData](repeating: TableViewData.init(opened: false, title: "", desc: "", data: 0), count: 6)
    
    var rucurringChargesData = [RecurringChargesModel]()
    var budgetingOverspentData = [BudgetOverspentModel]()
    var valueMappingData = [ValueMappingModel]()
    var financialHelpData: TipAndAdviceModel!
    var messagesModel: GetOffersModels!
    var spendingCorelationModel: SpendingCorelationModel!
    var valueMappingSubHeadingsSections = 1
    var voilatedValuesCount = 0
    
    var corelationCategoryName = ""
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var dialogView: UIView!
    @IBOutlet weak var blurView: UIVisualEffectView!
    
    @IBOutlet weak var categoryPickerView: UIPickerView!
    @IBOutlet weak var pickerToolbar: UIView!
    
    let messages = ["McDonald's buy 1 Quarter Pounder, get a drink free. Offer expires: 12/31/21" ,
                "Union of concerned scientists: Match announced: 1:1 match for upto $500. Deadline: 01/15/22",
                "EdisonPower switch to sustainable power generation and lock in $0.17 kWh for 1 year. Offer expires: 01/31/22"]
    
    let categories = [
                    "Mortgage / Rent / Home Maintenance",
                    "Groceries",
                    "Utilities",
                    "Fuel / Transportation",
                    "Phone and Internet",
                    "Education",
                    "Healthcare",
                    "Shopping",
                    "Dining out",
                    "Hobbies",
                    "Streaming Services",
                    "Travel",
                    "Loan / Debt Payments",
                    "Charity",
                    "Savings",
                    "Insurance"
                    ]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.rowHeight = UITableView.automaticDimension
        //tableView.sectionHeaderHeight = UITableView.automaticDimension
        //tableView.estimatedSectionHeaderHeight = 200
        tableView.estimatedRowHeight = 200
        tableView.delegate = self
        tableView.dataSource = self
        
        DispatchQueue.main.async {
            self.categoryPickerView.backgroundColor = .systemGray5

        }
        
        categoryPickerView.isHidden = true
        pickerToolbar.isHidden = true
        
        categoryPickerView.dataSource = self
        categoryPickerView.delegate = self
        
        getRecurringChagesAPI()
        getOverspendCategoriesAPI()
        getValueMappingAPI()
        getFinancialHelpAPI()
        getMessagesAPI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        setNavBarTitile(title: "Back", topItem: "Insights")
    }
    
    
    //Get Recurring Charges API
    func  getRecurringChagesAPI(){
        self.view.activityStartAnimating()
        let url = ApiUrl.GET_RUCURRING_CHARGES_API
        let namespace = SPManager.sharedInstance.getCurrentUserNamespace()
        
        let params:Dictionary<String,AnyObject> =
            ["namespace" : namespace  as AnyObject]
        print(params)
        
        APIManager.sharedInstance.postRequest(serviceName: url, sendData: params, success: { (response) in
            print(response)
            
            
            DispatchQueue.main.async {
                
                //self.fetchRecurringChargesResponse(response: res as! [AnyObject])
                self.fetchRecurringTransactions(response: response)
            }
//            if APIManager.sharedInstance.isSuccess {
//
//            }else{
//
//                DispatchQueue.main.async { [self] in
//                    if response["message"] != nil {
//                        self.showAlert(response["message"] as! String)
//                    } else {
//                        self.showAlert(ConstantStrings.NO_RESULT_FOUND_ALERT_TEXT)
//                    }
//                }
//            }
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
        }, failure: { (data) in
            print(data)
            DispatchQueue.main.async { [self] in
                view.activityStopAnimating()
            }
            if APIManager.sharedInstance.error400 {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
            }
            if(APIManager.sharedInstance.error401)
            {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
            }else{
                self.showAlert(ConstantStrings.Error_msg_generic)
            }
        })
        
    }
    
    //MARK:- Fetch Details
    private func fetchRecurringChargesResponse(response: [AnyObject]){
        
        rucurringChargesData.removeAll()
        
        for i in 0..<response.count {
            let dataItem = response[i] as? [String: AnyObject]
            
            let marchantName = dataItem!["merchant_name"] as! String
            let frequency = dataItem!["frequency"] as! String
            let amount = dataItem!["aggregated_amount"] as! Double
            
            let transacs = dataItem!["transacs"] as! [AnyObject]
            
            var transactions = [RecurringChargesTransactions]()
            for j in 0..<transacs.count {
                let trans = transacs[j] as! [String: AnyObject]
                let transAmount = trans["amount"] as! Double
                let transDate = trans["date"] as! Int64
                
                transactions.append(RecurringChargesTransactions.init(amount: transAmount, date: transDate))
            }
            
            let recurringItem = RecurringChargesModel.init(marchantName: marchantName, amount: amount, frequency: frequency, transactions: transactions)
            
            rucurringChargesData.append(recurringItem)
            
        }
        let data = TableViewData.init(opened: false, title: getLocalizedString(key: "rec_charges") + " (\(rucurringChargesData.count))", desc: getLocalizedString(key: "view_rec_charges"), data: rucurringChargesData)
        tableViewDataArray[0] = data

        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
        
        
    }
    
    func fetchRecurringTransactions(response: [String: AnyObject]) {
        let success = response["success"] as! Bool
        _ = response["message"] as! String
        
        if !success {
            let tableData = TableViewData.init(opened: false, title: getLocalizedString(key: "rec_charges") + " (0)", desc: getLocalizedString(key: "view_rec_charges"), data: rucurringChargesData)
            tableViewDataArray[0] = tableData

            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
            return
        }
        
        rucurringChargesData.removeAll()
        
        let data = response["data"] as! [String: AnyObject]
        
        let days = data["days"] as! [AnyObject]
        let months = data["months"] as! [AnyObject]
        let quarters = data["quarters"] as! [AnyObject]
        let biAnnual = data["biannual"] as! [AnyObject]
        let annual = data["annual"] as! [AnyObject]
        
        
        for item in days {
            let transactions = item as! [AnyObject]
            var recurringTransactions = [RecurringChargesTransactions]()
            var merchantname = ""
            var amount = 0.0
            
            for transact in transactions {
                let transaction = transact as! [String: AnyObject]
                let merchantName = transaction["merchant_name"] as! String
                let transactAmount = transaction["amount"] as! Double
                let date = transaction["date"] as! Int64
                
                merchantname = merchantName
                amount = transactAmount
                
                recurringTransactions.append(RecurringChargesTransactions(amount: transactAmount, date: date))
            }
            
            if recurringTransactions.count > 0 {
                rucurringChargesData.append(RecurringChargesModel(marchantName: merchantname, amount: amount, frequency: getLocalizedString(key: "DAILY"), transactions: recurringTransactions))
            }
            
        }
        
        for item in months {
            let transactions = item as! [AnyObject]
            var recurringTransactions = [RecurringChargesTransactions]()
            var merchantname = ""
            var amount = 0.0
            
            for transact in transactions {
                let transaction = transact as! [String: AnyObject]
                let merchantName = transaction["merchant_name"] as! String
                let transactAmount = transaction["amount"] as! Double
                let date = transaction["date"] as! Int64
                
                
                merchantname = merchantName
                amount = transactAmount
                
                recurringTransactions.append(RecurringChargesTransactions(amount: transactAmount, date: date))
            }
            
            if recurringTransactions.count > 0 {
                rucurringChargesData.append(RecurringChargesModel(marchantName: merchantname, amount: amount, frequency: getLocalizedString(key: "MONTH"), transactions: recurringTransactions))
            }
            
        }
        
        for item in quarters {
            let transactions = item as! [AnyObject]
            var recurringTransactions = [RecurringChargesTransactions]()
            var merchantname = ""
            var amount = 0.0
            
            for transact in transactions {
                let transaction = transact as! [String: AnyObject]
                let merchantName = transaction["merchant_name"] as! String
                let transactAmount = transaction["amount"] as! Double
                let date = transaction["date"] as! Int64
                
                
                merchantname = merchantName
                amount = transactAmount
                
                recurringTransactions.append(RecurringChargesTransactions(amount: transactAmount, date: date))
            }
            
            if recurringTransactions.count > 0 {
                rucurringChargesData.append(RecurringChargesModel(marchantName: merchantname, amount: amount, frequency: getLocalizedString(key: "_quarterly"), transactions: recurringTransactions))
            }
            
        }
        
        for item in biAnnual {
            let transactions = item as! [AnyObject]
            var recurringTransactions = [RecurringChargesTransactions]()
            var merchantname = ""
            var amount = 0.0
            
            for transact in transactions {
                let transaction = transact as! [String: AnyObject]
                let merchantName = transaction["merchant_name"] as! String
                let transactAmount = transaction["amount"] as! Double
                let date = transaction["date"] as! Int64
                
                
                merchantname = merchantName
                amount = transactAmount
                
                recurringTransactions.append(RecurringChargesTransactions(amount: transactAmount, date: date))
            }
            
            if recurringTransactions.count > 0 {
                rucurringChargesData.append(RecurringChargesModel(marchantName: merchantname, amount: amount, frequency: getLocalizedString(key: "_biannual"), transactions: recurringTransactions))
            }
            
        }
        
        for item in annual {
            let transactions = item as! [AnyObject]
            var recurringTransactions = [RecurringChargesTransactions]()
            var merchantname = ""
            var amount = 0.0
            
            for transact in transactions {
                let transaction = transact as! [String: AnyObject]
                let merchantName = transaction["merchant_name"] as! String
                let transactAmount = transaction["amount"] as! Double
                let date = transaction["date"] as! Int64
                
                
                merchantname = merchantName
                amount = transactAmount
                
                recurringTransactions.append(RecurringChargesTransactions(amount: transactAmount, date: date))
            }
            
            if recurringTransactions.count > 0 {
                rucurringChargesData.append(RecurringChargesModel(marchantName: merchantname, amount: amount, frequency: getLocalizedString(key: "_annual"), transactions: recurringTransactions))
            }
            
        }
        
        
        let tableData = TableViewData.init(opened: false, title: getLocalizedString(key: "rec_charges") + " (\(rucurringChargesData.count))", desc: getLocalizedString(key: "view_rec_charges"), data: rucurringChargesData)
        
//        let tableData = TableViewData.init(opened: false, title: "Recurring charges (\(rucurringChargesData.count))", desc: "View your recurring charges", data: rucurringChargesData)
        tableViewDataArray[0] = tableData

        DispatchQueue.main.async {
            self.tableView.reloadData()
        }

    }
    
    
    //Get Overspent Categories API
    func  getOverspendCategoriesAPI(){
        self.tableView.isHidden = true
        self.view.activityStartAnimating()
        let url = ApiUrl.GET_OVERSPENT_CATEGORIES_API
        let namespace = SPManager.sharedInstance.getCurrentUserNamespace()
        //let namespace = "kh01-sdRt"
        
        let params:Dictionary<String,AnyObject> =
            ["namespace" : namespace  as AnyObject]
        print(params)
        
        APIManager.sharedInstance.postRequest(serviceName: url, sendData: params, success: { (response) in
            print(response)
            
            if APIManager.sharedInstance.isSuccess {
                let res = response["data"] as! [String: AnyObject]
                DispatchQueue.main.async {
                    if res.isEmpty {
                        self.fetchOverspentCategoriesResponse(response: [] as [AnyObject])
                    }else {
                        self.fetchOverspentCategoriesResponse(response: res["category_data"] as! [AnyObject])
                    }
                    
                }
            }else{
                
                DispatchQueue.main.async { [self] in
                    if response["message"] != nil {
                        self.showAlert(response["message"] as! String)
                    } else {
                        self.showAlert(ConstantStrings.NO_RESULT_FOUND_ALERT_TEXT)
                    }
                }
            }
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
                self.tableView.isHidden = false
            }
        }, failure: { (data) in
            print(data)
            DispatchQueue.main.async { [self] in
                view.activityStopAnimating()
                self.tableView.isHidden = false
            }
            if APIManager.sharedInstance.error400 {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
            }
            if(APIManager.sharedInstance.error401)
            {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
            }else{
                self.showAlert(ConstantStrings.Error_msg_generic)
            }
        })
        
    }
    
    //Mark:- Fetch Details
    private func fetchOverspentCategoriesResponse(response: [AnyObject]){
        
        budgetingOverspentData.removeAll()
        
        for i in 0..<response.count {
            let dataItem = response[i] as? [String: AnyObject]
            
            let categoryName = dataItem!["category_name"] as! String
            let plannedAmount = dataItem!["planned_amount"] as! Double
            let spentAmount = dataItem!["spent_amount"] as! Double
            
            let overspendItem = BudgetOverspentModel.init(categoryName: categoryName, amountSpent: spentAmount, amountPlanned: plannedAmount)
            budgetingOverspentData.append(overspendItem)
            
        }
        
        let data = TableViewData.init(opened: false, title: getLocalizedString(key: "_insights") + " (\(budgetingOverspentData.count))", desc: getLocalizedString(key: "budget_insights_by_vesgo"), data: budgetingOverspentData)
        tableViewDataArray[1]  = data

        DispatchQueue.main.async {
            self.tableView.reloadData()
        }

        
    
        
        
    }
    
    
    func  getValueMappingAPI(){
        self.view.activityStartAnimating()
        let url = ApiUrl.GET_VALUE_MAPPINGS_INSIGHTS_API
        let namespace = SPManager.sharedInstance.getCurrentUserNamespace()
        //let namespace = "kh01-sdRt"
        
        let params:Dictionary<String,AnyObject> =
            ["namespace" : namespace  as AnyObject]
        print(params)
        
        APIManager.sharedInstance.postRequest(serviceName: url, sendData: params, success: { (response) in
            print(response)
            
            if APIManager.sharedInstance.isSuccess {
                let res = response["data"] as! [AnyObject]
                DispatchQueue.main.async {
                    self.fetchValueMappingData(response: res)
                }
            }else{
                
                DispatchQueue.main.async { [self] in
                    if response["message"] != nil {
                        self.showAlert(response["message"] as! String)
                    } else {
                        self.showAlert(ConstantStrings.NO_RESULT_FOUND_ALERT_TEXT)
                    }
                }
            }
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
        }, failure: { (data) in
            print(data)
            DispatchQueue.main.async { [self] in
                view.activityStopAnimating()
            }
            if APIManager.sharedInstance.error400 {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
            }
            if(APIManager.sharedInstance.error401)
            {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
            }else{
                self.showAlert(ConstantStrings.Error_msg_generic)
            }
        })
        
    }
    
    //Mark:- Fetch Details
    private func fetchValueMappingData(response: [AnyObject]){
        
        valueMappingData.removeAll()
        
        for i in 0..<response.count {
            let dataItem = response[i] as? [String: AnyObject]
            
            let value = dataItem!["value"] as! String
            let numVoilation = dataItem!["num_violations"] as! Int
            let details = dataItem!["Transactions"] as! [AnyObject]
            
            var vaueMappingDetailsModel = [ValueMappingDetailsModel]()
            
            for j in 0..<details.count {
                let detail = details[j] as? [String: AnyObject]
                let marchantName = detail!["merchant_name"] as! String
                let amount = detail!["amount"] as! Double
                let date = detail!["date"] as! Int64
                
                let detailModel = ValueMappingDetailsModel.init(marchantName: marchantName, amount: amount, data: date)
                
                vaueMappingDetailsModel.append(detailModel)
            }
            
            valueMappingData.append(ValueMappingModel.init(valueName: value, NumVoilations: numVoilation, detail: vaueMappingDetailsModel))
            
//            let valueMappingDetail = TableViewData.init(opened: false, title: value + " (\(numVoilation))", desc: "", data: vaueMappingDetailsModel)
            
            
        }
        let valueMappingTableData = TableViewData.init(opened: false, title: getLocalizedString(key: "value_mappings"), desc: getLocalizedString(key: "discover_spending_alignment"), data: valueMappingData)
        tableViewDataArray[2]  = valueMappingTableData
        

        DispatchQueue.main.async {
            self.tableView.reloadData()
        }

        
    
        
        
    }
    
    //MARK:- Fetch Financial help API
    func  getFinancialHelpAPI(){
        view.activityStartAnimating()
        let url = ApiUrl.GET_FINANCIAL_HELP_DATA
        let namespace = SPManager.sharedInstance.getCurrentUserNamespace()

        postManagerUser(method: "POST", isThisURLEmbdedData: false, urlString: url, parameter: ["namespace": namespace], success: {(value) in

            do {
                self.financialHelpData = try JSONDecoder().decode(TipAndAdviceModel?.self, from: value)
                DispatchQueue.main.async { [self] in
                    let financialHelpDetails = TableViewData.init(opened: false, title: getLocalizedString(key: "tips_n_advices"), desc: getLocalizedString(key: "personalize_tips"), data: financialHelpData)
                    
                    
                    tableViewDataArray[3] = TableViewData.init(opened: false, title: getLocalizedString(key: "spending_map"), desc: getLocalizedString(key: "uncover_expense_relation"), data: 0)
                    tableViewDataArray[4] = financialHelpDetails
                    
                    tableView.reloadData()
                    
                }
                

            } catch {
                DispatchQueue.main.async { [self] in
                    let financialHelpDetails = TableViewData.init(opened: false, title: getLocalizedString(key: "tips_n_advices") + " (0)", desc: getLocalizedString(key: "personalize_tips"), data: financialHelpData)
                    
                    
                    tableViewDataArray[3] = TableViewData.init(opened: false, title: getLocalizedString(key: "spending_map"), desc: getLocalizedString(key: "uncover_expense_relation"), data: 0)
                    tableViewDataArray[4] = financialHelpDetails
                    
                    tableView.reloadData()
                }
            }

            
            DispatchQueue.main.async { [self] in
                view.activityStopAnimating()
            }

        }, failure: {(err) in

            DispatchQueue.main.async { [self] in
                view.activityStopAnimating()
            }

            if APIManager.sharedInstance.error400 {

                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)

            }

            if(APIManager.sharedInstance.error401)

            {

                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)

            }else{
                self.showAlert(ConstantStrings.Error_msg_generic)
                

            }
        })

    }
    
    //MARK:- Fetch Offers and Deals
    func  getMessagesAPI(){
        view.activityStartAnimating()
        let url = ApiUrl.GET_OFFERS_AND_DEALS
        let namespace = SPManager.sharedInstance.getCurrentUserNamespace()
        
        let params = ["namespace": namespace] as [String: AnyObject]
        
        APIManager.sharedInstance.postRequest(serviceName: url, sendData: params, success: { (response) in
            print(response)
            
            if APIManager.sharedInstance.isSuccess {
                DispatchQueue.main.async { [self] in
                    self.messagesModel = fetchMessagesResponse(res: response)
                    tableViewDataArray[5] = TableViewData.init(opened: false, title: getLocalizedString(key: "_messages") + " (\(self.messagesModel.data.count))", desc: getLocalizedString(key: "messages_desc"), data: self.messagesModel)
                    tableView.reloadData()
                }
            }else{
                DispatchQueue.main.async { [self] in
                    let model = GetOffersModels.init(success: false, message: "", data: [])
                    tableViewDataArray[5] = TableViewData.init(opened: false, title: getLocalizedString(key: "_messages") + " (0)", desc: getLocalizedString(key: "messages_desc"), data: model)
                    tableView.reloadData()
                }
                DispatchQueue.main.async { [self] in
                    if response["message"] != nil {
                        self.showAlert(response["message"] as! String)
                    } else {
                        self.showAlert(ConstantStrings.NO_RESULT_FOUND_ALERT_TEXT)
                    }
                }
            }
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
        }, failure: { (data) in
            print(data)
            DispatchQueue.main.async { [self] in
                let model = GetOffersModels.init(success: false, message: "", data: [])
                tableViewDataArray[5] = TableViewData.init(opened: false, title: getLocalizedString(key: "_messages") + " (0)", desc: getLocalizedString(key: "messages_desc"), data: model)
                tableView.reloadData()
            }
            DispatchQueue.main.async { [self] in
                view.activityStopAnimating()
            }
            if APIManager.sharedInstance.error400 {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
            }
            if(APIManager.sharedInstance.error401)
            {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
            }else{
                self.showAlert(ConstantStrings.Error_msg_generic)
            }
        })

    }
    
    private func fetchMessagesResponse(res: [String: AnyObject]) -> GetOffersModels {
        let success = res["success"] as! Bool
        let message = res["message"] as! String
        
        if success == false {
            return GetOffersModels.init(success: success, message: message, data: [])
        }
        
        let data = res["data"] as! [AnyObject]
        
        var offers = [GetOffersData]()
        
        for i in 0..<data.count {
            let item = data[i] as! [String: AnyObject]
            let desc = item["description"] as! String
            let price = item["price"] as! Double
            let offerCode = item["offer_code"] as! String
            let startDate = item["start_date"] as! Int64
            let endDate = item["end_date"] as! Int64
            let link = item["offer_link"] as! String
            let currency = item["currency"] as! String
            
            offers.append(GetOffersData.init(desc: desc, price: price, offerCode: offerCode, startDate: startDate, endDate: endDate, currency: currency, offerLink: link))
        }
        
        return GetOffersModels.init(success: success, message: message, data: offers)
     }
    
    
    private func reloadTableViewSection(section: Int) {
        DispatchQueue.main.async {
            let indexSet = IndexSet.init(integer: section)
            self.tableView.reloadSections(indexSet, with: .none)
        }
    }
    
    private func openDialogView(sectionIndex: Int, rowIndex: Int) {
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(closeBtnTapped))
        if sectionIndex == 0 {
            let RecurringChargesView = RecurringChargesDialogView(frame: dialogView.bounds)
            RecurringChargesView.closeBtn.isUserInteractionEnabled = true
            RecurringChargesView.closeBtn.addGestureRecognizer(tapGestureRecognizer)
            
            let data = tableViewDataArray[0].data as! [RecurringChargesModel]
            var modelData = data[rowIndex]
            
            modelData.transactions.sort{
                $0.date > $1.date
            }
            
            RecurringChargesView.merchantName.text = modelData.marchantName
            RecurringChargesView.frequency.text = modelData.frequency
            RecurringChargesView.transactions = modelData.transactions
            RecurringChargesView.tableView.reloadData()
            
            
            //dialogView = RecurringChargesView as! RecurringChargesDialogView
            dialogView.addSubview(RecurringChargesView)
        }
        else if sectionIndex == 1 {
            let overspentView = OverspentDialogView(frame: dialogView.bounds)
            
            let data = tableViewDataArray[1].data as! [BudgetOverspentModel]
            overspentView.amountPlanned.text = "$" + FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: data[rowIndex].amountPlanned)
            overspentView.categoryName.text = data[rowIndex].categoryName
            overspentView.amountSpent.text = "$" + FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: data[rowIndex].amountSpent)
            overspentView.categoryIcon.image = FintechUtils.sharedInstance.getCaetgoryImageForBudgeting(value: data[rowIndex].categoryName.lowercased())
            overspentView.educationBtn.addTarget(self, action: #selector(educationBtnPressed), for: .touchUpInside)
            
            let amountSaved = data[rowIndex].amountPlanned -  data[rowIndex].amountSpent
            if amountSaved >= 0 {
                overspentView.amountSaved.text = "$" + FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: amountSaved)
            }else {
                overspentView.amountSaved.text = "$(" + FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: data[rowIndex].amountSpent -  data[rowIndex].amountPlanned) + ")"
            }
            
            overspentView.closeBtn.isUserInteractionEnabled = true
            overspentView.closeBtn.addGestureRecognizer(tapGestureRecognizer)
            dialogView.addSubview(overspentView)
        }
        else if sectionIndex == 2 {
            let valueMappingDialog = ValueMappingDialogView(frame: dialogView.bounds)
            
            let data = tableViewDataArray[2].data as! [ValueMappingModel]
            valueMappingDialog.categoryName.text = data[rowIndex].valueName
            valueMappingDialog.valueMappingTransactions = data[rowIndex].detail
            valueMappingDialog.tableView.reloadData()
            
            valueMappingDialog.closeBtn.isUserInteractionEnabled = true
            valueMappingDialog.closeBtn.addGestureRecognizer(tapGestureRecognizer)
            dialogView.addSubview(valueMappingDialog)
        }
        else if sectionIndex == 5 {
//            let corelationDialog = CorelationDialogView(frame: dialogView.bounds)
//            corelationDialog.corelationModel = spendingCorelationModel
//            corelationDialog.corelationModel.data.sort {
//                $0.value > $1.value
//            }
//            corelationDialog.setCategoryName(name: corelationCategoryName)
//            corelationDialog.setViewValues()
//
//            dialogView.addSubview(corelationDialog)
            
            let storyboard = UIStoryboard(name: "Budgeting", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "CorrelationVC") as! CorrelationVC
            vc.corelationModel = spendingCorelationModel
            vc.corelationCategoryName = corelationCategoryName
            self.navigationController?.pushViewController(vc, animated: true)
            
            return
        }
        
        blurView.isUserInteractionEnabled = true
        blurView.addGestureRecognizer(tapGestureRecognizer)
        
        dialogView.isHidden = false
        blurView.isHidden = false
    }
    
    @objc private func closeBtnTapped(){
        dialogView.isHidden = true
        blurView.isHidden = true
        
        for view in dialogView.subviews {
            view.removeFromSuperview()
        }
    }
    
    @objc private func educationBtnPressed(){
        let resoucesStoryboard = UIStoryboard(name: "Resources", bundle: nil)
        let vc = resoucesStoryboard.instantiateViewController(withIdentifier: "ResourcesVC") as! ResourcesVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
extension InsightsVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return tableViewDataArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableViewDataArray[section].opened {
            
            if section == 0 {
                let items = tableViewDataArray[section].data as! [RecurringChargesModel]
                if items.count == 0 {
                    return 1
                }else {
                    return 1 + 1 + 1 + items.count
                }
            }else if section == 1 {
                let items = tableViewDataArray[section].data as! [BudgetOverspentModel]
                if items.count == 0 {
                    return 1
                }else {
                    return 1 + 1 + 1 + items.count
                }
                
            }else if section == 2 {
                let item = tableViewDataArray[section].data as! [ValueMappingModel]
                if item.count == 0 {
                    return 1
                }
                else {
                    return 1 + item.count
                }
            }
            else if section == 3 {
                return 1
            }
            else if section == 4{
                let data = tableViewDataArray[section].data as? TipAndAdviceModel
                if data == nil {
                    return 1
                }else {
                    return 4
                }
            }
            
            else if section == 5 {
                let item = tableViewDataArray[section].data as? GetOffersModels
                if item == nil {
                    return 1
                }else {
                    return 1 + item!.data.count
                }
                
            }
            
        }else {
            return 1
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderWithDescCell") as! InsightsHeadingCell
                
                cell.headingLbl.text = tableViewDataArray[indexPath.section].title
                cell.bodyLbl.text = tableViewDataArray[indexPath.section].desc
                cell.headerImage.image = UIImage(named: "recuring_charges")
                if rucurringChargesData.count == 0 {
                    cell.dropDownImg.isHidden = true
                }else {
                    cell.dropDownImg.isHidden = false
                }
                //cell.dropDownImg.isHidden = false
                if #available(iOS 13.0, *) {
                    cell.dropDownImg.image = UIImage(systemName: "chevron.down")
                } else {
                    // Fallback on earlier versions
                }
                
                return cell
            }else if indexPath.row == 1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "DescriptionCell") as! InsightsDescCell
                
                cell.descLable.text = getLocalizedString(key: "found_these_charges")
                
                return cell
            }
            else if indexPath.row == 2 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "InsightsNewHeadingCell") as! InsightsNewHeadingCell
                
                cell.heading1.text = getLocalizedString(key: "_merchant")
                cell.heading2.text = getLocalizedString(key: "_amount_insights")
                
                return cell
            }else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "InsightsNewDescCell") as! InsightsNewDescCell
                
                let arrayData = tableViewDataArray[indexPath.section].data as! [RecurringChargesModel]
                let item = arrayData[indexPath.row - 3]
                cell.label1.text = item.marchantName
                cell.label2.text = "$" + FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: item.amount)
                
                
                return cell
            }
        } else if indexPath.section == 1 {
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderWithDescCell") as! InsightsHeadingCell
                
                cell.headingLbl.text = tableViewDataArray[indexPath.section].title
                cell.bodyLbl.text = tableViewDataArray[indexPath.section].desc
                cell.headerImage.image = UIImage(named: "insights")
                
                if budgetingOverspentData.count == 0 {
                    cell.dropDownImg.isHidden = true
                }else {
                    cell.dropDownImg.isHidden = false
                }
                //cell.dropDownImg.isHidden = false
                if #available(iOS 13.0, *) {
                    cell.dropDownImg.image = UIImage(systemName: "chevron.down")
                } else {
                    // Fallback on earlier versions
                }
                return cell
            }
            else if indexPath.row == 1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "DescriptionCell") as! InsightsDescCell
                
                cell.descLable.text = getLocalizedString(key: "check_your_spending")
                
                return cell
            }
            else if indexPath.row == 2 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "InsightsNewHeadingCell") as! InsightsNewHeadingCell
                
                cell.heading1.text = getLocalizedString(key: "_category")
                cell.heading2.text = getLocalizedString(key: "spent_percent")
                
                return cell
            }else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "InsightsNewDescCell") as! InsightsNewDescCell
                
                let arrayData = tableViewDataArray[indexPath.section].data as! [BudgetOverspentModel]
                let item = arrayData[indexPath.row - 3]
                cell.label1.text = item.categoryName

                var plannedAmount = 1.0
                if item.amountPlanned != 0 {
                    plannedAmount = item.amountPlanned
                }
                let percentSpent = (item.amountSpent / plannedAmount ) * 100.0
                cell.label2.text = FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: percentSpent) + "%"
                if percentSpent > 100.0 {
                    cell.label2.textColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
                }
                
                
                return cell
            }
        }else if indexPath.section == 2 {
            let data = tableViewDataArray[indexPath.section].data as? [ValueMappingModel] ?? [ValueMappingModel]()
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderWithDescCell") as! InsightsHeadingCell
                
                if data.count == 0 {
                    cell.headingLbl.text = tableViewDataArray[indexPath.section].title + " (0)"
                    cell.dropDownImg.isHidden = true
                }else {
                    cell.headingLbl.text = tableViewDataArray[indexPath.section].title + " (\(data.count))"
                    cell.dropDownImg.isHidden = false
                }
                cell.bodyLbl.text = tableViewDataArray[indexPath.section].desc
                cell.headerImage.image = UIImage(named: "value_mapping")
                
                if #available(iOS 13.0, *) {
                    cell.dropDownImg.image = UIImage(systemName: "chevron.down")
                } else {
                    // Fallback on earlier versions
                }
                
                return cell
            }else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "HeadingCell") as! InsightsHeadingLabelCell
                
                
                
                
                cell.headingLbl.text = data[indexPath.row - 1].valueName + " (\(data[indexPath.row - 1].NumVoilations))"
                
                
                return cell
            }
            
        }
        else if indexPath.section == 3 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderWithDescCell") as! InsightsHeadingCell
            
            cell.headingLbl.text = tableViewDataArray[indexPath.section].title
            cell.bodyLbl.text = tableViewDataArray[indexPath.section].desc
            cell.headerImage.image = UIImage(named: "spending_maps")

            cell.dropDownImg.isHidden = false
            if #available(iOS 13.0, *) {
                cell.dropDownImg.image = UIImage(systemName: "ellipsis")
            } else {
                // Fallback on earlier versions
            }
            
            return cell
        }
        else if indexPath.section == 4 {
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderWithDescCell") as! InsightsHeadingCell
                
                cell.headingLbl.text = tableViewDataArray[indexPath.section].title
                cell.bodyLbl.text = tableViewDataArray[indexPath.section].desc
                cell.headerImage.image = UIImage(named: "financial_help")
                
                if financialHelpData != nil {
                    let arrayData = tableViewDataArray[indexPath.section].data as? TipAndAdviceModel
                    if arrayData != nil {
                        if arrayData!.data.categoryData.isEmpty {
                            cell.dropDownImg.isHidden = true
                        }else {
                            cell.dropDownImg.isHidden = false
                        }
                    }else {
                        cell.dropDownImg.isHidden = false
                    }
                    
                }else {
                    cell.dropDownImg.isHidden = true
                }
                if #available(iOS 13.0, *) {
                    cell.dropDownImg.image = UIImage(systemName: "chevron.down")
                } else {
                    // Fallback on earlier versions
                }
            
                return cell
            }else if indexPath.row == 1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "DescriptionCell") as! InsightsDescCell
                
                cell.descLable.text = getLocalizedString(key: "tips_we_crunched")
                
                return cell
            }
            else if indexPath.row == 2 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "InnerHeadingsCell") as! InnerHeadingsCell
                
                cell.heading1Lbl.text = getLocalizedString(key: "ur_spending")
                cell.headings2Lbl.text = getLocalizedString(key: "peer_spending")
                cell.heading3Lbl.text = getLocalizedString(key: "_difference")
                
                return cell
            }else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "CellRowDataCell") as! InsightsRowDataCell
                
                let item = tableViewDataArray[indexPath.section].data as! TipAndAdviceModel
                cell.categoryLbl.text = "$" + FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: item.data.totalSpending)
                
                let peerSpending = item.data.totalSpending - ( item.data.totalSpending * (item.data.totalDifference / 100.0))
                cell.budgetLbl.text = "$" + FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: peerSpending)

                
                if item.data.totalDifference >= 0 {
                    cell.spentLbl.text = "+" + FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: item.data.totalDifference) + "%"
                    cell.diffImage.image = UIImage(named: "unhappy")
                }else {
                    cell.spentLbl.text =  FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: item.data.totalDifference) + "%"
                    cell.diffImage.image = UIImage(named: "cheerful")
                }
                
                return cell
            }
        }else if indexPath.section == 5 {
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderWithDescCell") as! InsightsHeadingCell
                
                cell.headingLbl.text = tableViewDataArray[indexPath.section].title
                cell.bodyLbl.text = tableViewDataArray[indexPath.section].desc
                cell.headerImage.image = UIImage(named: "messages")

                if self.messagesModel != nil {
                    if self.messagesModel.data.count == 0 {
                        cell.dropDownImg.isHidden = true
                    }else {
                        cell.dropDownImg.isHidden = false
                    }
                }else {
                    cell.dropDownImg.isHidden = true
                }
                
                if #available(iOS 13.0, *) {
                    cell.dropDownImg.image = UIImage(systemName: "chevron.down")
                } else {
                    // Fallback on earlier versions
                }
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "DescriptionCell") as! InsightsDescCell
                
                let item = tableViewDataArray[indexPath.section].data as! GetOffersModels
                let expiry = FintechUtils.sharedInstance.getDateInUSLocale(timestamp: item.data[indexPath.row - 1].endDate/1000)
                let price = item.data[indexPath.row - 1].currency + FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: item.data[indexPath.row - 1].price)
                cell.descLable.text = item.data[indexPath.row - 1].desc + " Offer expires: \(expiry) Price: \(price)"
                
                return cell
            }
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "EmptyCell")!
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            if tableViewDataArray[indexPath.section].opened {
                tableViewDataArray[indexPath.section].opened = false
                
            }else {
                tableViewDataArray[indexPath.section].opened = true
                if indexPath.section == 0 {
                    let arrayData = tableViewDataArray[indexPath.section].data as! [RecurringChargesModel]
                    if arrayData.isEmpty {
                        tableViewDataArray[indexPath.section].opened = false
                    }else {
                        tableViewDataArray[indexPath.section].opened = true
                    }
                }else if indexPath.section == 1 {
                    let arrayData = tableViewDataArray[indexPath.section].data as! [BudgetOverspentModel]
                    if arrayData.isEmpty {
                        tableViewDataArray[indexPath.section].opened = false
                    }else {
                        tableViewDataArray[indexPath.section].opened = true
                    }
                }else if indexPath.section == 2 {
                    let arrayData = tableViewDataArray[indexPath.section].data as? [ValueMappingModel]
                    if arrayData?.isEmpty ?? true {
                        tableViewDataArray[indexPath.section].opened = false
                    }else {
                        tableViewDataArray[indexPath.section].opened = true
                    }
                }
                else if indexPath.section == 4 {
                    tableViewDataArray[indexPath.section].opened = true
                }else if indexPath.section == 5 {
                    let arrayData = tableViewDataArray[indexPath.section].data as! GetOffersModels
                    if arrayData.data.count == 0 {
                        tableViewDataArray[indexPath.section].opened = false
                    }else {
                        tableViewDataArray[indexPath.section].opened = true
                    }
                }
                
            }
            
            let sections = IndexSet.init(integer: indexPath.section)
            tableView.reloadSections(sections, with: .none)
        }
        
        if indexPath.section == 0 {
            if indexPath.row > 1 + 1 {
                openDialogView(sectionIndex: 0, rowIndex: indexPath.row - 3)
            }
        }else if indexPath.section == 1 {
            if indexPath.row > 1 + 1 {
                openDialogView(sectionIndex: 1, rowIndex: indexPath.row - 3)
            }
        }else if indexPath.section == 2 {
            if indexPath.row > 0 {
                openDialogView(sectionIndex: 2, rowIndex: indexPath.row - 1)
            }
        }
        else if indexPath.section == 3 {
            self.categoryPickerView.isHidden = false
            self.pickerToolbar.isHidden = false
            
        }
        else if indexPath.section == 4 {
            if indexPath.row > 1 {
                let vc = UIStoryboard(name: "Budgeting", bundle: nil).instantiateViewController(withIdentifier: "FinancialHelpVC") as! FinancialHelp
                vc.index = indexPath.row - 2
                vc.financialHelpModel = financialHelpData
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        else if indexPath.section == 5 {
            if indexPath.row > 0 {
                FintechUtils.sharedInstance.openUrl(url: self.messagesModel.data[indexPath.row - 1].offerLink)
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section > 2 + valueMappingSubHeadingsSections + 1 + 1 + 1 {
            return 0
        }
        return UITableView.automaticDimension
    }
    
}
extension InsightsVC: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return categories.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return categories[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        print(categories[row])
        
    }
    
//    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
//        return NSAttributedString(string: categories[row], attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
//    }
    
    
    @IBAction func dismissPickerView(_ sender: UIButton) {
        self.categoryPickerView.isHidden = true
        self.pickerToolbar.isHidden = true
        
    }
    @IBAction func donePressedPickerView(_ sender: UIButton) {
        self.categoryPickerView.isHidden = true
        self.pickerToolbar.isHidden = true
        
        let selectedIndex = categoryPickerView.selectedRow(inComponent: 0)
        spendingCorelationAPI(categoryName: categories[selectedIndex])
    }
    
    
    //MARK:- Spending corelation api
    func  spendingCorelationAPI(categoryName: String){
        self.corelationCategoryName = categoryName
        view.activityStartAnimating()
        
        let url = ApiUrl.GET_SPENDING_CORELATION
        let namespace = SPManager.sharedInstance.getCurrentUserNamespace()

        postManagerUser(method: "POST", isThisURLEmbdedData: false, urlString: url, parameter: ["namespace": namespace, "category_name": categoryName], success: {(value) in

            do {
                self.spendingCorelationModel = try JSONDecoder().decode(SpendingCorelationModel?.self, from: value)
                
                DispatchQueue.main.async {
                    self.openDialogView(sectionIndex: 5, rowIndex: 0)
                }

            } catch {
                print("Unable to fetch spending corelation data")
            }

            
            DispatchQueue.main.async { [self] in
                view.activityStopAnimating()
            }

        }, failure: {(err) in

            DispatchQueue.main.async { [self] in
                view.activityStopAnimating()
            }

            if APIManager.sharedInstance.error400 {

                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)

            }

            if(APIManager.sharedInstance.error401)

            {

                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)

            }else{
                self.showAlert(ConstantStrings.Error_msg_generic)
                

            }
        })

    }
    
}
