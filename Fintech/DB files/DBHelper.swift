//
//  DBHandler.swift
//  first design
//
//  Created by Haris Mir on 23/10/2019.
//  Copyright © 2019 haris. All rights reserved.
//

import Foundation
import SQLite3
var Dbname = "fintecheng"
class Databasehandler {
    
    
    
    //fintech
    var databaseName = "fintecheng"
    let databaseExtension : String = "db"
    var db : OpaquePointer! = nil
    var dbForProgress: OpaquePointer! = nil
    
    
    private func getDatebaseName() -> String {
        if AppPrefsManager.sharedInstance.getLanguageCode()  == LanguageCodeType.English {
            return "fintecheng"
        }
        else if AppPrefsManager.sharedInstance.getLanguageCode() == LanguageCodeType.Arabic {
            return "FintechArabic"
        }
        else if AppPrefsManager.sharedInstance.getLanguageCode() == LanguageCodeType.Spanish {
            return "fintspanish"
        }
        else if AppPrefsManager.sharedInstance.getLanguageCode() == LanguageCodeType.French {
            return "fintfrinch"
        }
        else if AppPrefsManager.sharedInstance.getLanguageCode() == LanguageCodeType.Chinese {
            return "fintchinese"
        }else {
            return "fintecheng"
        }
    }
    
    private func getDatebaseNameForProgress() -> String {
        return "fintecheng"
    }
    
    init(isForProgress: Bool = false) {
        prepareDatafile(isDbForProgress: isForProgress)
        
        dbForProgress = openDatabase(isDbForProgress: true)
        db = openDatabase(isDbForProgress: false)
        
    }
    
//    func readNeesFeed()->[NewsFeedModel]{
//
//        var data = [NewsFeedModel]()
//        let query = "SELECT * FROM newsFeed"
//        var queryStatement: OpaquePointer? = nil
//        if sqlite3_prepare_v2(db, query, -1, &queryStatement, nil) == SQLITE_OK {
//
//            while sqlite3_step(queryStatement) == SQLITE_ROW {
//                let title = String(describing: String(cString: sqlite3_column_text(queryStatement, 1)))
//                let ticker = String(describing: String(cString: sqlite3_column_text(queryStatement, 2)))
//                let publisher = String(describing: String(cString: sqlite3_column_text(queryStatement, 3)))
//                let iconUrl = String(describing: String(cString: sqlite3_column_text(queryStatement, 4)))
//                let link = String(describing: String(cString: sqlite3_column_text(queryStatement, 5)))
//                let time = String(describing: String(cString: sqlite3_column_text(queryStatement, 6)))
//
//
//                data.append(NewsFeedModel(title: title, ticker: ticker, publisher: publisher, iconurl: iconUrl, link: link, time: time))
//                print("Query Result:")
//
//            }
//        } else {
//            let errmsg = String(cString: sqlite3_errmsg(db)!)
//            print(errmsg)
//
//        }
//
//        sqlite3_finalize(queryStatement!)
//        return data
//    }
//
//    func readLibarary(type:String)->[LibraryModel]{
//
//        var data = [LibraryModel]()
//        let query = "SELECT * FROM libaray WHERE type = '\(type)'"
//        var queryStatement: OpaquePointer? = nil
//        if sqlite3_prepare_v2(db, query, -1, &queryStatement, nil) == SQLITE_OK {
//
//            while sqlite3_step(queryStatement) == SQLITE_ROW {
//                let type = String(describing: String(cString: sqlite3_column_text(queryStatement, 1)))
//                let head = String(describing: String(cString: sqlite3_column_text(queryStatement, 2)))
//                let des = String(describing: String(cString: sqlite3_column_text(queryStatement, 3)))
//                let infotype = String(describing: String(cString: sqlite3_column_text(queryStatement, 4)))
//                let infovalue = String(describing: String(cString: sqlite3_column_text(queryStatement, 5)))
//                let link = String(describing: String(cString: sqlite3_column_text(queryStatement, 6)))
//
//
//                data.append(LibraryModel(type: type, head: head, des: des, infotype: infotype, infovalue: infovalue, link: link))
//                print("Query Result:")
//
//            }
//        } else {
//            let errmsg = String(cString: sqlite3_errmsg(db)!)
//            print(errmsg)
//
//        }
//
//        sqlite3_finalize(queryStatement!)
//        return data
//    }
    
    func readEducation()->[EducationModel]{
        
        var data = [EducationModel]()
        let query = "SELECT * FROM education"
        var queryStatement: OpaquePointer? = nil
        if sqlite3_prepare_v2(db, query, -1, &queryStatement, nil) == SQLITE_OK {
            
            while sqlite3_step(queryStatement) == SQLITE_ROW {
                let id = String(describing: String(cString: sqlite3_column_text(queryStatement, 0)))
                let heading = String(describing: String(cString: sqlite3_column_text(queryStatement, 1)))
                let subheading = String(describing: String(cString: sqlite3_column_text(queryStatement, 2)))
                let image = String(describing: String(cString: sqlite3_column_text(queryStatement, 3)))
                 let type = String(describing: String(cString: sqlite3_column_text(queryStatement, 4)))
                let sortedID = String(describing: String(cString: sqlite3_column_text(queryStatement, 5)))
                let bgColor = String(describing: String(cString: sqlite3_column_text(queryStatement, 6)))
                
                
                data.append(EducationModel(id: Int(id)!, heading: heading, subheading: subheading, image: image, type: type, sortedID: Int(sortedID)!, bgColor: bgColor))
                print("Query Result:")
                
            }
        } else {
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print(errmsg)
            
        }
        
        sqlite3_finalize(queryStatement!)
        let sortedData = data.sorted {
            $0.sortedID < $1.sortedID
        }
        return sortedData
    }
    
    func readQuiz()->[quizStructure]{
        
        var data = [quizStructure]()
        let query = "SELECT * FROM quiz"
        var queryStatement: OpaquePointer? = nil
        if sqlite3_prepare_v2(db, query, -1, &queryStatement, nil) == SQLITE_OK {
            
            while sqlite3_step(queryStatement) == SQLITE_ROW {
                let question = String(describing: String(cString: sqlite3_column_text(queryStatement, 0)))
                let option = String(describing: String(cString: sqlite3_column_text(queryStatement, 1)))
                let answer = String(describing: String(cString: sqlite3_column_text(queryStatement, 2)))
                 let ansIndex = sqlite3_column_int(queryStatement, 4)
                
                
                let options =  option.components(separatedBy: "|")
                
                data.append(quizStructure(question: question, options: options, answer: answer, ansIndex: Int(ansIndex)))
                    
                    
                print("Query Result:")
                
            }
        } else {
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print(errmsg)
            
        }
        
        sqlite3_finalize(queryStatement!)
        return data
    }
    
    func getQuestion(user:String,mainQ:Int)-> [Int]{
        
        var  count = [Int]()
        let query = "SELECT * FROM appData where user = '\(user)'and mainQuestion = \(mainQ) "
        print(query)
        var queryStatement: OpaquePointer? = nil
        if sqlite3_prepare_v2(dbForProgress, query, -1, &queryStatement, nil) == SQLITE_OK {
            
            while sqlite3_step(queryStatement) == SQLITE_ROW {
            
                let amount = sqlite3_column_int(queryStatement, 3)
             
                count.append(Int(amount))
                
            }
        } else {
            let errmsg = String(cString: sqlite3_errmsg(dbForProgress)!)
            print(errmsg)
            
        }
        
        sqlite3_finalize(queryStatement!)
        count =  count.sorted(by:{$0<$1})
        return count
    }
    func readEducationDETAIL(id : Int)->[EducationDetailModel]{
        
        var data = [EducationDetailModel]()
        let query = "SELECT * from educationDetail where eid = \(id)"
        var queryStatement: OpaquePointer? = nil
        if sqlite3_prepare_v2(db, query, -1, &queryStatement, nil) == SQLITE_OK {
            
            while sqlite3_step(queryStatement) == SQLITE_ROW {
                let id = String(describing: String(cString: sqlite3_column_text(queryStatement, 1)))
                let heading = String(describing: String(cString: sqlite3_column_text(queryStatement, 2)))
                
                let subheading = String(describing: String(cString: sqlite3_column_text(queryStatement, 3)))
                let des = String(describing: String(cString: sqlite3_column_text(queryStatement, 4)))
                let image = String(describing: String(cString: sqlite3_column_text(queryStatement, 5)))
                let Disclaimer = String(describing: String(cString: sqlite3_column_text(queryStatement, 6)))
                
                data.append(EducationDetailModel(id: Int(id)!, heading: heading, subheading: subheading, des: des, image: image, Disclaimer: Disclaimer))
                print("Query Result:")
                
            }
        } else {
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print(errmsg)
            
        }
        
        sqlite3_finalize(queryStatement!)
        return data
    }
    
    
    func readLessonAnswer(id : String)->[LessonAnswerModel]{
        
        var data = [LessonAnswerModel]()
        let query = "SELECT * from lessonAnswer where qid = \"\(id)\""
        var queryStatement: OpaquePointer? = nil
        if sqlite3_prepare_v2(db, query, -1, &queryStatement, nil) == SQLITE_OK {
            
            while sqlite3_step(queryStatement) == SQLITE_ROW {
                let id = String(describing: String(cString: sqlite3_column_text(queryStatement, 0)))
                let qid = String(describing: String(cString: sqlite3_column_text(queryStatement, 1)))
                
                let lesson = String(describing: String(cString: sqlite3_column_text(queryStatement, 2)))
            
                data.append(LessonAnswerModel(id: Int(id) ?? 1, qID: qid, Lesson: lesson))
                print("Query Result:")
                
            }
        } else {
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print(errmsg)
            
        }
        
        sqlite3_finalize(queryStatement!)
        return data
    }
    
    
    
    func executeQuery(query: String, isForProgress: Bool = false)->Bool{
        
        var tempDb: OpaquePointer!
        if isForProgress {
            tempDb = dbForProgress
        }else {
            tempDb = db
        }
        
        var queryStatement: OpaquePointer? = nil
        if sqlite3_prepare_v2(tempDb, query, -1, &queryStatement, nil) == SQLITE_OK {
            if sqlite3_step(queryStatement) != SQLITE_DONE {
                let errmsg = String(cString: sqlite3_errmsg(db)!)
                print("failure inserting hero: \(errmsg)")
            }
            else{
                return true
            }
            
        } else {
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("'insert into ':: could not be prepared::\(errmsg)")
        }
        sqlite3_finalize(queryStatement)
        return false
    }
    
    //++++++++++++++++++++++ Deleting from database++++++++++++++++++
    
    func deleteQuery(query: String)->Bool{
        
        var deleteStatement: OpaquePointer? = nil
        if sqlite3_prepare_v2(dbForProgress, query, -1, &deleteStatement, nil) == SQLITE_OK {
            
            if sqlite3_step(deleteStatement) != SQLITE_DONE {
                let errmsg = String(cString: sqlite3_errmsg(db)!)
                print("failure inserting hero: \(errmsg)")
                
            } else {
                return true
                
                
            }
        } else {
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("'insert into ':: could not be prepared::\(errmsg)")
            
        }
        
        
        
        
        sqlite3_finalize(deleteStatement)
        
        
        print("delete")
        return false
        
        
    }
    
    /////////////////////////////
    //Copy database for fist time
    /////////////////////////////
    func prepareDatafile(isDbForProgress: Bool)
    {
        let docUrl=NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        print(docUrl)
        if isDbForProgress {
            databaseName = getDatebaseNameForProgress()
        }else {
            databaseName = getDatebaseName()
        }
        let fdoc_url=URL(fileURLWithPath: docUrl).appendingPathComponent("\(databaseName).\(databaseExtension)")

        let filemanager=FileManager.default

        if !FileManager.default.fileExists(atPath: fdoc_url.path){
            do{
                let localUrl=Bundle.main.url(forResource: databaseName, withExtension: databaseExtension)
                print(localUrl?.path ?? "")

                try filemanager.copyItem(atPath: (localUrl?.path)!, toPath: fdoc_url.path)

                print("Database copied to simulator-device")
            }catch
            {
                print("error while copying")
            }
        }
        else{
            print("Database alreayd exists in similator-device")
        }
    }
    
    
    
    /////////////////////////////////////
    /////Open Connection to Database
    ////////////////////////////////////
    func openDatabase(isDbForProgress: Bool) -> OpaquePointer? {
        
        let docUrl=NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        print(docUrl)
        if isDbForProgress {
            databaseName = getDatebaseNameForProgress()
        }else {
            databaseName = getDatebaseName()
        }
        let fdoc_url=URL(fileURLWithPath: docUrl).appendingPathComponent("\(databaseName).\(databaseExtension)")
        
        var db: OpaquePointer? = nil
        
        if sqlite3_open(fdoc_url.path, &db) == SQLITE_OK {
            print("DB Connection Opened, Path is :: \(fdoc_url.path)")
            return db
        } else {
            print("Unable to open database. Verify that you created the directory described " +
                "in the Getting Started section.")
            return nil
        }
        
    }
    
}


