//
//  FuturePredictionsCell.swift
//  Fintech
//
//  Created by MacBook-Mubashar on 23/01/2020.
//  Copyright © 2020 Broadstone Technologies. All rights reserved.
//

import Foundation
import UIKit


class FuturePredictionsCell: UITableViewCell {

    @IBOutlet weak var profit_lbl: UILabel!
    @IBOutlet weak var confidence_lbl: UILabel!
    @IBOutlet weak var plan_lbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
