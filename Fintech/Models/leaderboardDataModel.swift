//
//  leaderboardDataModel.swift
//  Fintech
//
//  Created by Aqib on 16/09/2020.
//  Copyright © 2020 Broadstone Technologies. All rights reserved.
//

import Foundation

class leaderboardDataModel: NSObject {
    
    fileprivate var Portfolioid : Int
    fileprivate var Rank : Int
    fileprivate var Score : Int
    fileprivate var percentile : Int
    var ESG = [[String : AnyObject]]()
    var derivedvalues = [[String : AnyObject]]()
   fileprivate var portfoliotitle : String
    fileprivate var returntype : String

    var Positivevalues = [String]()
   var negetive_values = [String]()
    
    
    override init() {
        portfoliotitle = ""
          Portfolioid = -1
           Rank = -1
           Score = -1
           percentile = -1
        returntype = ""

          
       }
    
    func setPortfolioid(_ text: Int){
           self.Portfolioid = text
       }
    func  getPortfolioid() ->Int {
           return self.Portfolioid
       }
    //
    func setRank(_ text: Int){
           self.Rank = text
       }
    func  getRank() ->Int {
           return self.Rank
       }
    //
    func setscore(_ text: Int){
           self.Score = text
       }
    func  getscore() ->Int {
           return self.Score
       }
    //
    func setpercentile(_ text: Int){
              self.percentile = text
          }
       func  getpercentile() ->Int {
              return self.percentile
          }
    //
    func setportfoliotitle(_ text: String){
                 self.portfoliotitle = text
             }
          func  getportfoliotitle() ->String {
                 return self.portfoliotitle
             }
    //
    func setreturntype(_ text: String){
           self.returntype = text
       }
    func  getreturntype() ->String {
           return self.returntype
       }
    
    
    
    
}
