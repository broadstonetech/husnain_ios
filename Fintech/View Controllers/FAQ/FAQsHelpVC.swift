//
//  FAQsHelpVC.swift
//  Fintech
//
//  Created by MacBook-Mubashar on 05/09/2019.
//  Copyright © 2019 Broadstone Technologies. All rights reserved.
//#################################################################
//               FAQs View
//
//##################################################################
import UIKit

class FAQsHelpVC: Parent {
    
    @IBOutlet var segmentControl: UISegmentedControl!
    @IBOutlet var tableview_faqs: UITableView!
    @IBOutlet var request_view: UIView!
    
    @IBOutlet var requestSubject_tf: UITextField!
    @IBOutlet var rewuestEmail_tf: UITextField!
    @IBOutlet var requestDescription_tv: UITextView!
    @IBOutlet var submitRequest_btn: UIButton!
    
    
    var questionsArr  = [String]()
    var ansArr  = [String]()
    
    //MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableview_faqs.delegate = self
        tableview_faqs.dataSource = self
        rewuestEmail_tf.text = SPManager.sharedInstance.getCurrentUserEmail()
        submitRequest_btn.addTarget(self, action: #selector(submitRequest_tapped), for: UIControl.Event.touchUpInside)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        localization()
        prepareData()
        navigationController?.isNavigationBarHidden = false

    }
    
    
    func localization(){
        setNavBarTitile(title:"", topItem: getLocalizedString(key: "FAQS", comment: ""))
        self.navigationItem.backButtonTitle = ""
        
    }
    
        
    @objc func submitRequest_tapped() {
        if isValidateForm() {
            submitRequest()
        } else {
            showAlert("Fill all indexes.")
        }
    }
    
    func isValidateForm() -> Bool {
        if requestSubject_tf.text?.isEmpty ?? false {
            self.showAlert("Subject was empty")
            return false
        } else if rewuestEmail_tf.text?.isEmpty ?? false {
            self.showAlert("Email was empty")
            return false
        } else if requestDescription_tv.text?.isEmpty ?? false {
            self.showAlert("Description empty")
            return false
        }
        return true
    }
    
    @IBAction func segmentValue_changed(sender: AnyObject) {
        switch segmentControl.selectedSegmentIndex {
        case 0:
            
            print("FAQs")
            if !request_view.isHidden {
                request_view.isHidden = true
            }
            
        case 1:
            print("support req")
            if request_view.isHidden {
                request_view.isHidden = false
            }
            
        default:
            break
        }
    }
    
    //MARK: -API calls - signup
    func submitRequest() {
        var orgType : String = ""
        
        EZLoadingActivity.show(ConstantStrings.LOADING_RESULT_ALERT_TEXT, disableUI: true)
        let url = ApiUrl.GET_HELP_URL
        let data:Dictionary<String,String> = ["subject": requestSubject_tf.text! as String ,
                                              "message": requestDescription_tv.text as! String,
                                              "email": rewuestEmail_tf.text! as String ,
                                              "timestamp": getTimestamp() as String ]
        
        let params:Dictionary<String,AnyObject> = ["data" : data as AnyObject, "namespace" : SPManager.sharedInstance.getCurrentUserNamespace() as AnyObject]
        
        APIManager.sharedInstance.postRequest(serviceName: url, sendData: params as [String : AnyObject], success: { (response) in
            print(response)
            
            if APIManager.sharedInstance.isSuccess {
                //signup sucessfull -- navigate to login
                self.view.makeToast(response["message"] as! String)
                
                
            } else {
                if response["message"] != nil {
                    self.showAlert(response["message"]! as! String)
                } else {
                    self.showAlert(ConstantStrings.Error_msg_generic)
                }
            }
            EZLoadingActivity.hide()
        }, failure: { (data) in
            print(data)
            EZLoadingActivity.hide()
            if APIManager.sharedInstance.error400 {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
            }
            if(APIManager.sharedInstance.error401)
            {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
            }else{
                
            }
        })
        
    }
    
    //MARK: -Add faqsData
    func prepareData() {
        
        questionsArr.append(getLocalizedString(key: "faqQ1", comment: ""))
        
        ansArr.append(getLocalizedString(key: "faqAnswer1", comment: ""))
        
        
        
        questionsArr.append(getLocalizedString(key: "faqQ2", comment: ""))
        
        ansArr.append(getLocalizedString(key: "faqAnswer2", comment: ""))
        
        
        
        questionsArr.append(getLocalizedString(key: "faqQ3", comment: ""))
        
        ansArr.append(getLocalizedString(key: "faqAnswer3", comment: ""))
        
        
        
        questionsArr.append(getLocalizedString(key: "faqQ4", comment: ""))
        
        ansArr.append(getLocalizedString(key: "faqAnswer4", comment: ""))
        
        
        
        questionsArr.append(getLocalizedString(key: "faqQ5", comment: ""))
        
        ansArr.append(getLocalizedString(key: "faqAnswer5", comment: ""))
        
        
        
        
        
        
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

//Mark: -Tableview delegates
extension FAQsHelpVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableview_faqs.deselectRow(at: indexPath, animated: true)
        print("rowSelected\(indexPath.row)")
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return questionsArr.count
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell =  self.tableview_faqs.dequeueReusableCell(withIdentifier: "faqs_cell") as! FaqsExpandableCell
        
        cell.questionLabel.text = questionsArr[indexPath.row]
        cell.ans_label.text = ansArr[indexPath.row]
        return cell
        
    }
}
