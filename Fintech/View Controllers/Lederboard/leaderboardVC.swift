//
//  leaderboardVC.swift
//  Fintech
//
//  Created by broadstone on 11/09/2020.
//  Copyright © 2020 Broadstone Technologies. All rights reserved.
//

import UIKit

class leaderboardVC: Parent {
var pastMonth,pastThreeYear, pastYear: [String]?
    //var pastMonth: [PastMonth]?
   // var pasttabledata: [PastMonth]?
   // var tableData:[String]?
    
    @IBOutlet weak var segmentcontrol: UISegmentedControl!
    
    @IBOutlet weak var segmentrol_AgeGroup_View: UIView!
    @IBOutlet weak var TableView_leaderboard: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        segmentrol_AgeGroup_View.isHidden = true
        segmentrol_AgeGroup_View!.heightConstaint?.constant = 0

        navigationController?.isNavigationBarHidden = true

         // getNotifications()
       // callapi()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
                navigationController?.isNavigationBarHidden = true

    }
    func getNotifications() {
     DispatchQueue.main.async {
     self.view.activityStartAnimating()
     }
        let url = "http://165.22.3.113/leaderboard"
        let namespace = SPManager.sharedInstance.getCurrentUserNamespace()
        let params:Dictionary<String,AnyObject> = ["namespace" : namespace as AnyObject]
        print(params)
        APIManager.sharedInstance.postRequest(serviceName: url, sendData: params, success: { (response) in
            print(response)
            
            if APIManager.sharedInstance.isSuccess {
                let res = response["message"] as AnyObject
                DispatchQueue.main.async {
                    
                     self.fetchMetaData(res: res as! [String : AnyObject], table_view: self.TableView_leaderboard)
                }
                
            }else{
                if response["message"] != nil {
                    self.showAlert(response["message"] as! String)
                } else {
                    self.showAlert(ConstantStrings.NO_RESULT_FOUND_ALERT_TEXT)
                }
            }
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
        }, failure: { (data) in
            print(data)
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
            if APIManager.sharedInstance.error400 {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
            }
            if(APIManager.sharedInstance.error401)
            {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
            }else{
                
            }
        })
        
    }
    
    func fetchMetaData(res : [String : AnyObject], table_view : UITableView) {
        ArrayLists.sharedInstance.LederboardPastmontDataList.removeAll()
        ArrayLists.sharedInstance.LederboardPastThreeYearDataList.removeAll()
        ArrayLists.sharedInstance.LederboardPastYearDataList.removeAll()
        
        let pastmonth = res["past_month"] as! [[String : AnyObject]]
        let pastthreeYear = res["past_three_year"] as! [[String : AnyObject]]
        let pastyear = res["past_year"] as! [[String : AnyObject]]
        
        DispatchQueue.main.async {
            
            ArrayLists.sharedInstance.LederboardPastmontDataList = self.parseJsonList(data: pastmonth)
            ArrayLists.sharedInstance.LederboardPastThreeYearDataList = self.parseJsonList(data: pastthreeYear)
            ArrayLists.sharedInstance.LederboardPastYearDataList = self.parseJsonList(data: pastyear)
        }
        
        DispatchQueue.main.async {
            self.TableView_leaderboard.reloadData()
        }
        DispatchQueue.main.async {
            //self.deepDivesTableview.reloadData()
        }
        
        DispatchQueue.main.async {
           // self.meditableView.reloadData()
        }
    }
    
    func parseJsonList(data : [[String : AnyObject]]) -> [leaderboardDataModel] {

        var list : [leaderboardDataModel] = [leaderboardDataModel]()

        for jsonData in data {

            let model = leaderboardDataModel()
            var SELECTED_DATA_TYPE : String = ""

            let portfolioid = jsonData["portfolio_id"] as? Int ?? 0
            let rank = jsonData["rank"] as? Int ?? 0
            //parse model scaling json
            let portfoliotitle = jsonData["portfolio_title"] as? String ?? "Tittle"
            let returnText = jsonData["return"] as? String ?? ""

            if jsonData["risk"] as? [String : AnyObject] != nil {
                let risk = jsonData["risk"] as! [String : AnyObject]
                //parse additional info
                let score = risk["score"] as? Int ?? 0
                let percentile = risk["percentile"] as? Int ?? 0
                model.setscore(score)
                model.setpercentile(percentile)
            } else if jsonData["portfolio_composition"] as? [String : AnyObject] != nil {
                let portfoliocomposition = jsonData["portfolio_composition"] as! [String : AnyObject]
                if jsonData["ESG"] as? [[String : AnyObject]] != nil {
                    model.ESG = (jsonData["ESG"] as? [[String : AnyObject]])!

                }else if jsonData["derived_values"] as? [[String : AnyObject]] != nil{
                    model.derivedvalues = (jsonData["derived_values"] as? [[String : AnyObject]])!
                }
//                var list  = [String]()
//                    for i in 0..<jsonData.count
//                    {
//                        list.append(jsonData[i])
//                        print(data[i])
//                    }
//                    return list


            }else if jsonData["Positive_values"] as? [String] != nil{
                model.Positivevalues = (jsonData["Positive_values"] as? [String])!
            }else if jsonData["negetive_values"] as? [String] != nil {
                model.negetive_values = (jsonData["negetive_values"] as? [String])!
            }

            model.setPortfolioid(portfolioid)
            model.setRank(rank)
            model.setportfoliotitle(portfoliotitle)
            model.setreturntype(returnText)


            list.append(model)
        }
        print(list)

        return list

    }
    @IBAction func Bak_btn_pressed(_ sender: Any) {
        navigationController?.popViewController(animated: true)

    }
    @IBAction func show_agegroup_segment(_ sender: Any) {
        if segmentrol_AgeGroup_View.isHidden == true{
                 segmentrol_AgeGroup_View.isHidden = false
                 let tablel = segmentrol_AgeGroup_View
                 tablel!.heightConstaint?.constant = 35
             }else{
                 segmentrol_AgeGroup_View.isHidden = true
                 segmentrol_AgeGroup_View!.heightConstaint?.constant = 0
             }
    }
    
    @IBAction func Segmentcontrol_agegroup(_ sender: Any) {
        navigationController?.popViewController(animated: true)

    }
    
    @IBAction func segmentcontrol(_ sender: Any) {
        let index = segmentcontrol.selectedSegmentIndex
        
        if index == 0 {
          //  self.tableData = pastMonth
        }else if index == 1 {
            //self.tableData = pastYear
        }else{
            //self.tableData = pastThreeYear
        }
        //self.TableView_leaderboard.reloadData()
        
    }
    
//    func callapi(){
//        let url = "http://165.22.3.113/leaderboard"
//        let namespace = SPManager.sharedInstance.getCurrentUserNamespace()
//        postManagerUser(method: "POST", isThisURLEmbdedData: false, urlString: url, parameter: ["namespace":namespace], success: {(value) in
//        do
//        {
//            let jsonObject = try JSONDecoder().decode(LeaderBoardModel?.self, from: value)
//            DispatchQueue.main.async {
//                if let value = jsonObject?.message
//                {
//                    if let value1 = value.pastMonth{
//                        self.pastMonth = value1
//                    }
//
//                    if let value2 = value.pastYear{
//                        self.pastYear = value2
//                    }
//
//                    if let value3 = value.pastThreeYear{
//                        self.pastThreeYear = value3
//                    }
//                    self.tableData=self.pastMonth
//                    self.TableView_leaderboard.reloadData()
//
//                    }
//                            }
//
//                        }
//                        catch
//                        {
//                            DispatchQueue.main.async {
//                               // self.btnEnable = true
//
//                               // self.POPUp(message: "No data found", time: 1.8)
//                            }
//                        }
//
//                    }, failure: {(err) in
//
//                        DispatchQueue.main.async {
//                            //self.btnEnable = true
//
//                           // self.POPUp(message: "Something went wrong", time: 1.8)
//                        }
//                    })
//    }
//
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
//extension leaderboardVC:UITableViewDelegate,UITableViewDataSource{
//    // MARK: - TableView Delegates
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        
//        return self.tableData?.count ?? 0
//    }
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! leaderBoardCell
//        //cell.Answer_lbl.text!.count
//        let item = self.tableData?[indexPath.row]
//        cell.Heading_lbl.text = item?.portfolioTitle ?? ""
//        cell.presentage_lbl.text = item?.pastReturn ?? ""
//        cell.Number_lbl.text = String(item?.rank ?? 0)
//        
//        //        cell.Question_lbl.text =  getLocalizedString(key: tableData[indexPath.row].questiontext!, comment: "")
//        //        cell.Answer_lbl.text =  getLocalizedString(key: tableData[indexPath.row].answertext!, comment: "")
//        return cell
//    }
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                           let vc = storyboard.instantiateViewController(withIdentifier: "lederboardDetailsPortfolioVC") as! lederboardDetailsPortfolioVC
//                      let item = self.tableData?[indexPath.row]
//
//                               score = item?.risk as! Double
//        
//                       presentaile = item?.pastReturn ?? ""
//                       Positivevalues = item?.positiveValues ?? []
//                       Negetivevalues = item?.negetiveValues ?? []
//                           navigationController?.pushViewController(vc, animated: true)
//       
//        
//    }
//    
//    
//    
//}
