//
//  GraphVC.swift
//  Fintech
//
//  Created by MacBook-Mubashar on 23/01/2020.
//  Copyright © 2020 Broadstone Technologies. All rights reserved.
//

import Foundation
import UIKit
import Charts
class GraphVC: Parent {
    
    //MARK:-Class Outlets
    
    @IBOutlet weak var lineChart: LineChartView!
    //MARK: -View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setLineGraph()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let value = UIInterfaceOrientation.landscapeLeft.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        let value = UIInterfaceOrientation.portrait.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    //MARK: -Class Functions
    
    func setLineGraph(){
        var xAxisValues1 = [String]()
        var xAxisValues2 = [String]()
        var xAxisValues3 = [String]()
        var yAxisValuesYr1 = [Double]()
        var yAxisValuesYr2 = [Double]()
        var yAxisValuesYr3 = [Double]()
        
        var legendsList = [String]()
        for item in ArrayLists.sharedInstance.futurePredictionsList{
            
            if item.getPlan() == "3 years" {
                yAxisValuesYr1.append(Double(item.getProfit()))
            }
            else if item.getPlan() == "5 years" {
                xAxisValues2.append("5 years")
                yAxisValuesYr2.append(Double(item.getProfit()))
            }
                
            else if item.getPlan() == "0 years" {
                xAxisValues3.append("0 years")
                yAxisValuesYr3.append(Double(item.getProfit()))
            }
            
            xAxisValues1.append("")
            xAxisValues1.append("3 years")
            xAxisValues1.append("5 years")
        }
        
        var lineChartEntry1  = [ChartDataEntry]()
        var lineChartEntry2 = [ChartDataEntry]()
        var lineChartEntry3 = [ChartDataEntry]()
        
        for i in 0..<yAxisValuesYr1.count {
            let yVal = yAxisValuesYr1[i]
            lineChart.xAxis.valueFormatter = IndexAxisValueFormatter(values: xAxisValues1)
            let values = BarChartDataEntry(x: Double(i), y: yVal)
            lineChartEntry1.append(values)
        }
        for i in 0..<yAxisValuesYr2.count {
            let yVal = yAxisValuesYr2[i]
            lineChart.xAxis.valueFormatter = IndexAxisValueFormatter(values: xAxisValues1)
            let values = BarChartDataEntry(x: Double(i), y: yVal)
            lineChartEntry2.append(values)
        }
        
        for i in 0..<yAxisValuesYr3.count {
            let yVal = yAxisValuesYr3[i]
            lineChart.xAxis.valueFormatter = IndexAxisValueFormatter(values: xAxisValues1)
            let values = BarChartDataEntry(x: Double(i), y: yVal)
            lineChartEntry3.append(values)
        }
        
        
        let line1 = LineChartDataSet(entries: lineChartEntry1, label: "25%")
        line1.drawCirclesEnabled = false
        line1.circleRadius = 0
        line1.colors = [UIColor.red]
        
        let line2 = LineChartDataSet(entries: lineChartEntry2, label: "50%")
        line2.drawCirclesEnabled = false
        line2.circleRadius = 0
        line2.colors = [UIColor.green]
        
        let line3 = LineChartDataSet(entries: lineChartEntry3, label: "75%")
        line3.drawCirclesEnabled = false
        line3.circleRadius = 0
        line3.colors = [UIColor.blue]
        
        
        let data = LineChartData()
        
        data.addDataSet(line1)
        data.addDataSet(line2)
        data.addDataSet(line3)
        lineChart.data = data
        lineChart.chartDescription?.text = ""
        lineChart.rightAxis.drawGridLinesEnabled = false
        lineChart.rightAxis.drawAxisLineEnabled = false
        lineChart.rightAxis.drawZeroLineEnabled = false
        lineChart.rightAxis.enabled = false
        lineChart.lineData?.setDrawValues(false)
        
        lineChart.legend.verticalAlignment = .bottom
        lineChart.legend.horizontalAlignment = .right
        lineChart.xAxis.labelPosition = .bottom
        lineChart.legend.wordWrapEnabled = true
        lineChart.minOffset = 0
    }
    
    
}
