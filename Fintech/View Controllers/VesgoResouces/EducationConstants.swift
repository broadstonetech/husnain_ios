//
//  EducationConstants.swift
//  Fintech
//
//  Created by broadstone on 05/01/2022.
//  Copyright © 2022 Broadstone Technologies. All rights reserved.
//

import Foundation

struct EducationExpertiseValues {
    static let levelBigginer = "Beginner"
    static let levelIntermediate = "Intermediate"
    static let levelExpert = "Expert"
    static let levelNotSet = "Not_Set"
}
