//
//  FilterViewVC.swift
//  EducationModuleSP
//
//  Created by SaifUllah Butt on 04/01/2022.
//

import UIKit



//enum SortByAvailability {
//    case online, offline, notSelected, both
//}
//enum SortByAlphabets {
//    case AtoZ, ZtoA, notSelected
//}

struct Filters {
    var name: String
    var options: [FilterOptions]
    var isMultiSelection: Bool
}

struct FilterOptions {
    var name: String
    var key: String
}


class FilterViewVC: ResourcesParent {

    @IBOutlet weak var stockImageView: UIImageView!
    @IBOutlet weak var etfImageView: UIImageView!
    @IBOutlet weak var bondImageView: UIImageView!
    @IBOutlet weak var cryptoImageView: UIImageView!
    
    
    @IBOutlet weak var AtoZ: UIImageView!
    @IBOutlet weak var ZtoA: UIImageView!
    
    @IBOutlet weak var online: UIImageView!
    @IBOutlet weak var offline: UIImageView!
    @IBOutlet weak var both: UIImageView!
    
    
    
    @IBOutlet weak var doneBtnOutlet: UIButton!
    
    weak var delegate : FilterVCDelegte?
    var selectedOption = [String]()
    var availability: SortByAvailability = .notSelected
    var sortType: SortByAlphabets = .notSelected
    
    var filters = [Filters]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        clearAllFilters()
        
        
        for option in selectedOption {
            switch option {
            case "Stocks":
                stockImageView.isHidden = false
                break
            case "ETF":
                etfImageView.isHidden = false
                break
            case "Bonds":
                bondImageView.isHidden = false
                break
            case "Crypto":
                cryptoImageView.isHidden = false
                break
            default:
                break
            }
        }
        
        if availability == .both {
            both.isHidden = false
        }else if availability == .offline {
            offline.isHidden = false
        }else if availability == .online {
            online.isHidden = false
        }
        
        if sortType == .AtoZ {
            AtoZ.isHidden = false
        }else if sortType == .ZtoA {
            ZtoA.isHidden = false
        }
    }
    
    
    private func clearAllFilters(){
        stockImageView.isHidden = true
        etfImageView.isHidden = true
        bondImageView.isHidden = true
        cryptoImageView.isHidden = true
        
        AtoZ.isHidden = true
        ZtoA.isHidden = true
        clearOnlineFilter()
        
//        selectedOption = []
//        availability = .notSelected
//        sortType = .notSelected
    }
    
   
    
    @IBAction func doneBtn(_ sender: Any) {
        
//        delegate?.selection(option: self.selectedOption, sortType: self.sortType, availability: self.availability)
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func cancelBtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func stockBtn(_ sender: UIButton) {
        if stockImageView.isHidden == false{
            stockImageView.isHidden = true
            selectedOption.removeAll(where: {
                $0 == "Stocks"
            })
        }
        else{
            stockImageView.isHidden = false
            selectedOption.append("Stocks")
        }
    }
    
    
    @IBAction func etfBtn(_ sender: Any) {
        if etfImageView.isHidden == false{
            etfImageView.isHidden = true
            
            selectedOption.removeAll(where: {
                $0 == "ETF"
            })
        }
        else{
            etfImageView.isHidden = false
            selectedOption.append("ETF")
        }
        
    }
    
    @IBAction func bondBtn(_ sender: Any) {
        if bondImageView.isHidden == false{
            bondImageView.isHidden = true
            
            selectedOption.removeAll(where: {
                $0 == "Bonds"
            })
        }
        else{
            bondImageView.isHidden = false
            selectedOption.append("Bonds")
        }
    }
    @IBAction func cryptoBtn(_ sender: Any) {
        if cryptoImageView.isHidden == false{
            cryptoImageView.isHidden = true
            
            selectedOption.removeAll(where: {
                $0 == "Crypto"
            })
        }
        else{
            cryptoImageView.isHidden = false
            
            selectedOption.append("Crypto")
            
        }
    }

    @IBAction func AtoZbtn(_ sender: Any) {
        if  AtoZ.isHidden == false{
            sortType = .notSelected
            AtoZ.isHidden = true
            ZtoA.isHidden = true
        }
        else{
            sortType = .AtoZ
            AtoZ.isHidden = false
            ZtoA.isHidden = true
        }
    }
    
    @IBAction func ZtoAbtn(_ sender: Any) {
        if ZtoA.isHidden == false{
            sortType = .notSelected
            ZtoA.isHidden = true
            AtoZ.isHidden = true
        }
        else{
            sortType = .ZtoA
            ZtoA.isHidden = false
            AtoZ.isHidden = true
        }
    }
    
    
    @IBAction func onlinebtn(_ sender: Any) {
        clearOnlineFilter()
        availability = .online
        online.image = UIImage(systemName: "checkmark")
        online.isHidden = false
        
    }
    @IBAction func offlinebtn(_ sender: Any) {
        clearOnlineFilter()
        availability = .offline
        offline.image = UIImage(systemName: "checkmark")
        offline.isHidden = false

    }
    @IBAction func bothbtn(_ sender: Any) {
        clearOnlineFilter()
        availability = .offline
        
        both.image = UIImage(systemName: "checkmark")
        both.isHidden = false
    }
    
    private func clearOnlineFilter(){
        availability = .notSelected
        online.isHidden = true
        offline.isHidden = true
        both.isHidden = true
    }
    
    @IBAction func clearFilterButtonPressed(_ sender: UIButton) {
        clearAllFilters()
    }
    
}



