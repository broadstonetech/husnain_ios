//
//  PortfolioRiskVC.swift
//  Fintech
//
//  Created by broadstone on 11/08/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import UIKit

class PortfolioRiskVC: PortfolioParent {
    
    //Risk Data
    let riskAnswersQ1 = [getLocalizedString(key: "9a", comment: ""), getLocalizedString(key: "9b", comment: ""), getLocalizedString(key: "9c", comment: ""), getLocalizedString(key: "9d", comment: "")]
    let riskAnswersIdQ1 = [1,2,3,4]

    let riskAnswersQ2 = [getLocalizedString(key: "10a", comment: ""), getLocalizedString(key: "10b", comment: ""), getLocalizedString(key: "10c", comment: ""), getLocalizedString(key: "10d", comment: "")]
    let riskAnswersIdQ2 = [1,2,3,4]

    let riskAnswersQ3 = [getLocalizedString(key: "11a", comment: ""), getLocalizedString(key: "11b", comment: ""), getLocalizedString(key: "11c", comment: ""), getLocalizedString(key: "11d", comment: "")]
    let riskAnswersIdQ3 = [1,2,3,4]

    let riskAnswersQ4 = [getLocalizedString(key: "12a", comment: ""), getLocalizedString(key: "12b", comment: ""), getLocalizedString(key: "12c", comment: "")]
    let riskAnswersIdQ4 = [1,2,3]

    let riskAnswersQ5 = [getLocalizedString(key: "13a", comment: ""), getLocalizedString(key: "13b", comment: ""), getLocalizedString(key: "13c", comment: "")]
    let riskAnswersIdQ5 = [1,2,3]

    let riskAnswersQ6 = [getLocalizedString(key: "14a", comment: ""), getLocalizedString(key: "14b", comment: ""), getLocalizedString(key: "14c", comment: ""), getLocalizedString(key: "14d", comment: "")]
    let riskAnswersIdQ6 = [1,2,3,4]

    let riskAnswersQ7 = [getLocalizedString(key: "15a", comment: ""), getLocalizedString(key: "15b", comment: ""), getLocalizedString(key: "15c", comment: ""), getLocalizedString(key: "15d", comment: "")]
    let riskAnswersIdQ7 = [1,2,3,4]

    let riskAnswersQ8 = [getLocalizedString(key: "16a", comment: ""), getLocalizedString(key: "16b", comment: ""), getLocalizedString(key: "16c", comment: ""), getLocalizedString(key: "16d", comment: "")]
    let riskAnswersIdQ8 = [1,2,3,4]

    let riskAnswersQ9 = [getLocalizedString(key: "17a", comment: ""), getLocalizedString(key: "17b", comment: "")]
    let riskAnswersIdQ9 = [1,2]

    let riskAnswersQ10 = [getLocalizedString(key: "18a", comment: ""), getLocalizedString(key: "18b", comment: "")]
    let riskAnswersIdQ10 = [1,2]

    let riskAnswersQ11 = [getLocalizedString(key: "19a", comment: ""), getLocalizedString(key: "19b", comment: ""), getLocalizedString(key: "19c", comment: ""), getLocalizedString(key: "19d", comment: "")]
    let riskAnswersIdQ11 = [1,2,3,4]

    let riskAnswersQ12 = [getLocalizedString(key: "20a", comment: ""), getLocalizedString(key: "20b", comment: ""), getLocalizedString(key: "20c", comment: "")]
    let riskAnswersIdQ12 = [1,2,3]

    let riskAnswersQ13 = [getLocalizedString(key: "21a", comment: ""), getLocalizedString(key: "21b", comment: ""), getLocalizedString(key: "21c", comment: ""), getLocalizedString(key: "21d", comment: "")]
    let riskAnswersIdQ13 = [1,2,3,4]
    
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var changeRiskHeadingLbl: UILabel!
    @IBOutlet weak var riskSliderMinLbl: UILabel!
    @IBOutlet weak var riskSliderMaxLbl: UILabel!
    @IBOutlet weak var riskSlider: UISlider!
    @IBOutlet weak var riskSliderResultLbl: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var riskValueLabel: UILabel!
    @IBOutlet weak var doneBtn: UIButton!
    @IBOutlet weak var calculateBtn: UIButton!
    @IBOutlet weak var orLabel: UILabel!
    
    var riskQuestions = [Questionnaire]()
    var currentQuestionIndex = 0
    
    var riskScore: Int!
    var template = ""
    var portfolioType = ""
    var positiveVals = [String]()
    var negativeVals = [String]()
    
    var apiCounter = 0
    
    
    
    var valueMappedTickersModel: ValueMappedTickersModel!
    
    var valueMappedSuggestedPortfolios = [ValueMappedPortfolioData]()

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.isHidden = true
        
        tableView.delegate = self
        tableView.dataSource = self
        
        riskSlider.minimumTrackTintColor = .clear
        riskSlider.maximumTrackTintColor = .clear
        riskSlider.minimumValue = 1
        riskSlider.maximumValue = 47
        updateRiskLabels(value: 1)
        riskSlider.addTarget(self, action: #selector(sliderValueDidChange(sender:)),for: .valueChanged)
        
        populateRiskQuestions()
        
        createRightButton(title: getLocalizedString(key: "_next"), action: #selector(doneBtnPressed(_:)))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        setNavBarTitile(title: "Back", topItem: "Risk factor")
    }
    
    @IBAction func doneBtnPressed(_ sender: UIButton){
        if riskScore == 0 {
            self.view.makeToast("Please update your risk score")
            return
        }
        showPortfolioTypeBottomSheet()
        //callValueMappedTickerAPI(bucketType: "leader", isNavigatingToNextVC: true, valueName: "Environment", risk: riskScore)
    }
    
    @IBAction func calculateBtnPressed(_ sender: UIButton){
        if tableView.isHidden {
            tableView.isHidden = false
        }else {
            tableView.isHidden = true
        }
    }
    
    @objc func sliderValueDidChange(sender: UISlider) {
        updateRiskLabels(value: sender.value)
    }
    
    private func updateRiskLabels(value: Float){
        self.riskScore = Int(value)
        if Int(value) == 0 {
            self.riskValueLabel.text = "--"
            riskSlider.value = 1
            riskSlider.thumbTintColor = .white
            riskSliderResultLbl.text = "--"
            return
        }
        riskSlider.value = value
        if value < 23 {
            riskSlider.thumbTintColor = .systemGreen
            riskSliderResultLbl.text = getLocalizedString(key: "risk_averse")
        } else if value >= 23 && value < 33 {
            riskSlider.thumbTintColor = .systemYellow
            riskSliderResultLbl.text = getLocalizedString(key: "risk_neutral")
        } else {
            riskSlider.thumbTintColor = .systemRed
            riskSliderResultLbl.text = getLocalizedString(key: "risk_loving")
        }
        self.riskValueLabel.text = "\(Int(value))"
    }
        
    
    private func populateRiskQuestions(){
        riskQuestions.removeAll()
        
        riskQuestions = [
            Questionnaire(question: getLocalizedString(key: "Q9", comment: ""), questionId: 0, answers: riskAnswersQ1, answersId: riskAnswersIdQ1, isMultiSelection: false, tagsPrefix: ""),
            Questionnaire(question: getLocalizedString(key: "Q10", comment: ""), questionId: 1, answers: riskAnswersQ2, answersId: riskAnswersIdQ2, isMultiSelection: false, tagsPrefix: ""),
            Questionnaire(question: getLocalizedString(key: "Q11", comment: ""), questionId: 2, answers: riskAnswersQ3, answersId: riskAnswersIdQ3, isMultiSelection: false, tagsPrefix: ""),
            Questionnaire(question: getLocalizedString(key: "Q12", comment: ""), questionId: 3, answers: riskAnswersQ4, answersId: riskAnswersIdQ4, isMultiSelection: false, tagsPrefix: ""),
            Questionnaire(question: getLocalizedString(key: "Q13", comment: ""), questionId: 4, answers: riskAnswersQ5, answersId: riskAnswersIdQ5, isMultiSelection: false, tagsPrefix: ""),
            Questionnaire(question: getLocalizedString(key: "Q14", comment: ""), questionId: 5, answers: riskAnswersQ6, answersId: riskAnswersIdQ6, isMultiSelection: false, tagsPrefix: ""),
            Questionnaire(question: getLocalizedString(key: "Q15", comment: ""), questionId: 6, answers: riskAnswersQ7, answersId: riskAnswersIdQ7, isMultiSelection: false, tagsPrefix: ""),
            Questionnaire(question: getLocalizedString(key: "Q16", comment: ""), questionId: 7, answers: riskAnswersQ8, answersId: riskAnswersIdQ8, isMultiSelection: false, tagsPrefix: ""),
            Questionnaire(question: getLocalizedString(key: "Q17", comment: ""), questionId: 8, answers: riskAnswersQ9, answersId: riskAnswersIdQ9, isMultiSelection: false, tagsPrefix: ""),
            Questionnaire(question: getLocalizedString(key: "Q18", comment: ""), questionId: 9, answers: riskAnswersQ10, answersId: riskAnswersIdQ10, isMultiSelection: false, tagsPrefix: ""),
            Questionnaire(question: getLocalizedString(key: "Q19", comment: ""), questionId: 10, answers: riskAnswersQ11, answersId: riskAnswersIdQ11, isMultiSelection: false, tagsPrefix: ""),
            Questionnaire(question: getLocalizedString(key: "Q20", comment: ""), questionId: 11, answers: riskAnswersQ12, answersId: riskAnswersIdQ12, isMultiSelection: false, tagsPrefix: ""),
            Questionnaire(question: getLocalizedString(key: "Q21", comment: ""), questionId: 12, answers: riskAnswersQ13, answersId: riskAnswersIdQ13, isMultiSelection: false, tagsPrefix: "")
        ]
        
        tableView.reloadData()
    }
}
extension PortfolioRiskVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return riskQuestions.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1 + riskQuestions[section].answers.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 0
        }
        return 20
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int)
    {
        view.tintColor = #colorLiteral(red: 0.9411764706, green: 0.9411764706, blue: 0.9411764706, alpha: 1)
        view.backgroundColor = #colorLiteral(red: 0.9411764706, green: 0.9411764706, blue: 0.9411764706, alpha: 1)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "QuestionStatementCell") as! QuestionStatementCell
            let questionData = riskQuestions[indexPath.section]
            cell.questionLabel.text = questionData.question
            
            if indexPath.section > currentQuestionIndex {
                cell.contentView.alpha = 0.2
            }else{
                cell.contentView.alpha = 1
            }

            
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "QuestionnaireAnswersCell") as! QuestionnaireAnswersCell
            
            cell.answerLabel.text = riskQuestions[indexPath.section].answers[indexPath.row - 1]
            
            let questionnaireData = riskQuestions[indexPath.section]
            if questionnaireData.selectedAnswersIds.contains(indexPath.row) {
                cell.answerLabel.textColor = .white
                cell.parentView.backgroundColor = .systemGreen
            }else {
                cell.answerLabel.textColor = .black
                cell.parentView.backgroundColor = .clear
            }
            
            if indexPath.section > currentQuestionIndex {
                cell.contentView.alpha = 0.2
            }else{
                cell.contentView.alpha = 1
            }
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section <=  riskQuestions[currentQuestionIndex].questionId {
            if indexPath.row != 0 {
                var questionnaireData = riskQuestions[indexPath.section]
                questionnaireData.selectedAnswers.removeAll()
                questionnaireData.selectedAnswersIds.removeAll()
                questionnaireData.selectedAnswers.append(questionnaireData.answers[indexPath.row - 1])
                questionnaireData.selectedAnswersIds.append(indexPath.row)
                
                riskQuestions[indexPath.section] = questionnaireData
                
                if currentQuestionIndex < riskQuestions.count - 1 || indexPath.section != currentQuestionIndex{
                    if indexPath.section == currentQuestionIndex {
                        currentQuestionIndex += 1
                    }
                }
                
                tableView.reloadData()
                
                if currentQuestionIndex < tableView.numberOfSections {
                    let isCompleted = calculateRiskFromQuestionnaire()
                    if isCompleted {
                        
                    }else {
                        tableView.scrollToRow(at: NSIndexPath.init(row: 0, section: currentQuestionIndex) as IndexPath, at: .top, animated: true)
                    }
                    
                }
                
                //calculateRiskFromQuestionnaire()
            }
        }
    }
    
    func calculateRiskFromQuestionnaire() -> Bool{
        var isQuestionnaireCompleted = true
        for item in riskQuestions {
            if item.selectedAnswersIds.count == 0 {
                isQuestionnaireCompleted = false
                break
            }
        }
        
        if isQuestionnaireCompleted {
            let score = QuestionnaireCalculationHelper.sharedInstance.calculateUserRawRiskScore(riskQuestionnaireData: riskQuestions)
            updateRiskLabels(value: Float(score))
            self.view.makeToast("Risk score updated")
        }
        
        return isQuestionnaireCompleted
    }
    
    
    
    private func showPortfolioTypeBottomSheet(){
        let optionMenu = UIAlertController(title: nil, message: getLocalizedString(key: "select_portfolio_type"), preferredStyle: .actionSheet)
        
        let stocks = UIAlertAction(title: getLocalizedString(key: "stocks_only"), style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.portfolioType = "stocks"
            self.callValueMappedApi()
            
        })
        let etf = UIAlertAction(title: getLocalizedString(key: "etf_only"), style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.portfolioType = "etf"
            self.callValueMappedApi()
        })
        
        let cancel = UIAlertAction(title: getLocalizedString(key: "cancel_btn"), style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            self.dismiss(animated: true)
        })
        
        
        
        
        optionMenu.addAction(stocks)
        optionMenu.addAction(etf)
        optionMenu.addAction(cancel)
        self.present(optionMenu, animated: true, completion: nil)
    }
}
