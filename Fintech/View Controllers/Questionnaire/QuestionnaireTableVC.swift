//
//  QuestionnaireTableVC.swift
//  Fintech
//
//  Created by MacBook-Mubashar on 22/07/2019.
//  Copyright © 2019 Broadstone Technologies. All rights reserved.
//

import UIKit
import Foundation

class QuestionnaireTableVC: UIViewController, UITableViewDataSource, UITableViewDelegate {

    //outlets
    @IBOutlet weak var questionHeading_lbl: UILabel!
    @IBOutlet weak var questionInfo_lbl: UILabel!
    @IBOutlet weak var table_view: UITableView!
    
    //variables
    var ListOfQuestions: [QuestionnaireModel] = [QuestionnaireModel]()
    var ListOfAnswers: [AnswersModel] = [AnswersModel]()
    var ListOfSubmittedAnswers: [QuestionnaireModel] = [QuestionnaireModel]()
    var clickIndex = -1
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.table_view.delegate = self
        self.table_view.dataSource = self
        self.table_view.estimatedRowHeight = 300
        self.table_view.rowHeight = UITableView.automaticDimension
        
        DispatchQueue.main.async {
            self.PopulateQuestionnaire()
            self.PopulateAnswers()
            
            self.questionHeading_lbl.text = self.ListOfQuestions[0].getQuestionText()
            self.questionInfo_lbl.text = self.ListOfQuestions[0].getQuestionGuidedText()
            self.table_view.reloadData()
        }
        
        // Do any additional setup after loading the view.
    }
    
    // MARK: - Populate Data
    
    func PopulateQuestionnaire() {
        let item = QuestionnaireModel()
        item.setQuestionText(text: "this is question text1")
        item.setQuestionGuidedText(text: "question 1 info")
        ListOfQuestions.append(item)
        
        
        let item2 = QuestionnaireModel()
        item2.setQuestionText(text: "this is question text2")
        item2.setQuestionGuidedText(text: "question 2 info")
        ListOfQuestions.append(item2)
    }
    
    func PopulateAnswers() {
        
        let item = AnswersModel()
        item.setAnswerHeadingText(text: "answer heading 1")
        item.setAnswerSubHeadingText(text: "answer subheading")
        item.setAnswerBodyText(text: "lorem ipsum dollar imuet signaure lorem ipsum dollar imuet signaure lorem ipsum dollar imuet signaure")
        item.setAnswerImageUrlString(text: "")
        item.setAnswerDisclaimerText(text: "")
        item.setIsAnswerSelected(status: false)
        ListOfAnswers.append(item)


        let item2 = AnswersModel()
        item2.setAnswerHeadingText(text: "anwer2 heading1")
        item2.setAnswerSubHeadingText(text: "answer2 subheading")
item.setAnswerBodyText(text: "lorem ipsum dollar imuet signaure lorem ipsum dollar imuet signaure lorem ipsum dollar imuet signaure")
        item2.setAnswerImageUrlString(text: "banner_sample")
        item2.setAnswerDisclaimerText(text: "view disclosure")
        item2.setIsAnswerSelected(status: false)
        ListOfAnswers.append(item2)


        let item3 = AnswersModel()
        item3.setAnswerHeadingText(text: "answer3 heading 1")
        item3.setAnswerSubHeadingText(text: "answer3 subheading")
        item3.setAnswerBodyText(text: "lorem ipsum dollar imuet signaure lorem ipsum dollar imuet signaure lorem ipsum dollar imuet signaure")
        item3.setAnswerImageUrlString(text: "")
        item3.setAnswerDisclaimerText(text: "View disclaimer")
        item3.setIsAnswerSelected(status: false)
        ListOfAnswers.append(item3)

        let item4 = AnswersModel()
        item4.setAnswerHeadingText(text: "answer3 heading 1")
        item4.setAnswerSubHeadingText(text: "answer3 subheading")
//        item.setAnswerBodyText(text: )
        item4.setAnswerImageUrlString(text: "banner_sample")
        item4.setAnswerDisclaimerText(text: "")
        item4.setIsAnswerSelected(status: false)
        ListOfAnswers.append(item4)
    }

    
    // MARK: - TableView Delegates

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return ListOfAnswers.count
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
      
        let item = ListOfAnswers[indexPath.row]

        if clickIndex == indexPath.row{
         
            var height : CGFloat = 200.0
            if item.getAnswerHeadingText() == "" || item.getAnswerHeadingText().isEmpty {
                height -= 20
            }
            if item.getAnswerSubHeadingText() == "" || item.getAnswerSubHeadingText().isEmpty {
                height -= 20
            }
            if item.getAnswerBodyText() == "" || item.getAnswerBodyText().isEmpty {
                height -= 30
            }
            if item.getAnswerImageUrlString() == "" || item.getAnswerImageUrlString().isEmpty {
                height -= 70
            }
            if item.getAnswerDisclaimerText() == "" || item.getAnswerDisclaimerText().isEmpty {
                height -= 20
            }
                return height
            
        }else{
            
            return 50
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("selected index == \(indexPath.row)")
        let modelsCell =  self.table_view.dequeueReusableCell(withIdentifier: "questionnaire_cell") as! QuestionnaireCell
        
//        modelsCell.collapseAble_view.isHidden = false
        DispatchQueue.main.async {
        
//        modelsCell.selectedAnswer_img.image = UIImage(named: "tick_marked")
//            modelsCell.backgroundColor = UIColor.red
            
            if self.clickIndex == indexPath.row{
                
                self.clickIndex = -1
            }else{
                self.clickIndex = indexPath.row
            }
            self.table_view.reloadData()
        }
//        tableView.reloadData()
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let modelsCell =  self.table_view.dequeueReusableCell(withIdentifier: "questionnaire_cell") as! QuestionnaireCell
        
        let item = ListOfAnswers[indexPath.row]
        modelsCell.heading1_lbl.text = item.getAnswerHeadingText()
        modelsCell.heading2_lbl.text = item.getAnswerSubHeadingText()
        modelsCell.headingbody_lbl.text = item.getAnswerBodyText()
        modelsCell.body_img.image = UIImage(named: "banner_sample")
        modelsCell.callback_lbl.text = item.getAnswerDisclaimerText()
        
        if item.getAnswerHeadingText() == "" || item.getAnswerHeadingText().isEmpty {
            modelsCell.heading1_lbl.isHidden = true
        }
        if item.getAnswerSubHeadingText() == "" || item.getAnswerSubHeadingText().isEmpty {
            modelsCell.heading2_lbl.isHidden = true
        }
        if item.getAnswerBodyText() == "" || item.getAnswerBodyText().isEmpty {
            modelsCell.headingbody_lbl.isHidden = true
        }
        if item.getAnswerImageUrlString() == "" || item.getAnswerImageUrlString().isEmpty {
            modelsCell.body_img.isHidden = true
        }
        if item.getAnswerDisclaimerText() == "" || item.getAnswerDisclaimerText().isEmpty {
            modelsCell.callback_lbl.isHidden = true
        }
        
        return modelsCell
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
