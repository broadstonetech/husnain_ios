//
//  SubmissionModel.swift
//  SecDemo
//
//  Created by SaifUllah Butt on 26/10/2021.
//

import Foundation

struct SubmissionModel {
    
    var companyName = "" //name
    var cik = "" //cik
    var ticker = [""] //ticker
    var exchange = [""] //exchange
    var address = "" //addresses -> mailings -> Street1
    var number = "" //phone
    
}


struct companiesSubmissionDataModel {
    
    var title = ""
    var units = [CompaniesUnit]()
    
}

struct CompaniesUnit {
    var value = ""
    var fiscalYear = 0
    var fiscalPeriod = ""
    
}

//struct CompaniesParentModel {
//    let accountPayable: companiesSubmissionDataModel
//    let accountReceiveable: companiesSubmissionDataModel
//    let accruedLiabilities: companiesSubmissionDataModel
//    let AccruedLiabilitiesCurrentAndNoncurrent: companiesSubmissionDataModel
//    let accDep: companiesSubmissionDataModel
//    let accOtherCompInc: companiesSubmissionDataModel
//    let APCIS: companiesSubmissionDataModel
//    let ATNCA: companiesSubmissionDataModel
//    let ATPICECOfConvertibleDebt: companiesSubmissionDataModel
//    let AdjustmentsToAdditionalPaid: companiesSubmissionDataModel
//    
//    
//}
