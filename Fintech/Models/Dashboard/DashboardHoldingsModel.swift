// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let dashboardHoldingsModel = try? newJSONDecoder().decode(DashboardHoldingsModel.self, from: jsonData)

import Foundation

// MARK: - DashboardHoldingsModel
struct DashboardHoldingsModel: Codable {
    let success: Bool
    let message: String
    var data: [DashboardHoldingsModelDatum]
}

// MARK: - DashboardHoldingsModelDatum
struct DashboardHoldingsModelDatum: Codable {
    let tickers, tickerName, tickerIndustry, portfolioShare: String
    let amountInvested: Double
    let totalReturn, returnFreq: String
    let chartData: ChartData

    enum CodingKeys: String, CodingKey {
        case tickers
        case tickerName = "ticker_name"
        case tickerIndustry = "ticker_industry"
        case amountInvested = "amount_invested"
        case totalReturn = "total_return"
        case returnFreq = "return_freq"
        case chartData = "chart_data"
        case portfolioShare = "portfolio_share"
    }
}

// MARK: - ChartData
struct ChartData: Codable {
    let lastRefresh: Int
    let data: [ChartDataDatum]

    enum CodingKeys: String, CodingKey {
        case lastRefresh = "last_refresh"
        case data
    }
}

// MARK: - ChartDataDatum
struct ChartDataDatum: Codable {
    let xAxis: String
    let yAxis: Double

    enum CodingKeys: String, CodingKey {
        case xAxis = "x_axis"
        case yAxis = "y_axis"
    }
}
