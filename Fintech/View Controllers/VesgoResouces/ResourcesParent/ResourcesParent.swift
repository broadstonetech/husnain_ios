//
//  ResourcesParent.swift
//  Fintech
//
//  Created by broadstone on 10/01/2022.
//  Copyright © 2022 Broadstone Technologies. All rights reserved.
//

import UIKit

protocol AdvisorApiDelegate{
    func advisorApiCalled(isSuccessful: Bool, advisor : [AdvisorsModel])
}

class ResourcesParent: Parent {
    
    var advisorApiDelegate : AdvisorApiDelegate?
    var advisorStartChatApiDelegate: StartChatWithAdvisorDelegate?
    var chatHistoryApiDelegate: ChatHistoryApiDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        // Do any additional setup after loading the view.
    }
    
    
    func getAdvisors(url: String) {
        self.view.activityStartAnimating()
        let params:Dictionary<String,AnyObject> = ["namespace" : SPManager.sharedInstance.getCurrentUserNamespace() as AnyObject]
        
        APIManager.sharedInstance.postRequest(serviceName: url, sendData: params) { (response) in
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
            if APIManager.sharedInstance.isSuccess{
                DispatchQueue.main.async { [self] in
                    parseAdvisors(res: response)
                }
            }else {
                DispatchQueue.main.async {
                    self.advisorApiDelegate?.advisorApiCalled(isSuccessful: false, advisor: [AdvisorsModel]())
                }
            }
        } failure: { data in
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
            print("Failed to hit allAdvApi")
            DispatchQueue.main.async {
                self.advisorApiDelegate?.advisorApiCalled(isSuccessful: false, advisor: [AdvisorsModel]())
            }
        }

        
    }
    
    func parseAdvisors(res: [String: AnyObject]){
        
        let success = res["success"] as! Bool
        
        var advisorObject = [AdvisorsModel]()
        
        if success {
        
        let data = res["data"] as! [AnyObject]
            for i in 0..<data.count {
                let advisor = data[i] as! [String: AnyObject]
                let namespace = advisor["advisor_namespace"] as! String
                let name = advisor["name"] as! String
                let pic = advisor["profile_pic"] as! String
                let expertise = advisor["expertise"] as! [String]
                let description = advisor["description"] as! String
                let availability = advisor["is_online"] as! Bool
                let certifications = advisor["certifications"] as! [String]
                let email = advisor["email"] as! String
                let socialLinks = advisor["social_links"] as! [String: AnyObject]
                let facebook = socialLinks["facebook"] as! String
                let twitter = socialLinks["twitter"] as! String
                let linkedIn = socialLinks["linkedIn"] as! String
                let timestamp = advisor["joined_timestamp"] as! Int64
                
                //advTvData.removeAll()
                advisorObject.append(AdvisorsModel.init(advisorNamespace: namespace, advisorName: name, advisorProfilePic: pic, advisorAccountStatus: "nil", advisorDescription: description, advisorExpertise: expertise, advisorAvailability: availability, advisorCertificates: certifications, advisorEmail: email, advisorSocialLinks: AdvisorSocialLinks.init(facebook: facebook, twitter: twitter, linkedIn: linkedIn) , advisorTimestamp: timestamp))
                
                
            }
            advisorApiDelegate?.advisorApiCalled(isSuccessful: true, advisor: advisorObject)
            
        }
        else {
            print("Message is Data Not Found for Advisors")
            advisorApiDelegate?.advisorApiCalled(isSuccessful: false, advisor: advisorObject)
        }
        
        
    }
    
}
