//
//  leaderBoardCell.swift
//  Fintech
//
//  Created by broadstone on 11/09/2020.
//  Copyright © 2020 Broadstone Technologies. All rights reserved.
//

import UIKit

class leaderBoardCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBOutlet weak var icon_img: UIImageView!
    @IBOutlet weak var presentage_lbl: UILabel!
    
    @IBOutlet weak var Heading_lbl: UILabel!
    @IBOutlet weak var Number_lbl: UILabel!
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
