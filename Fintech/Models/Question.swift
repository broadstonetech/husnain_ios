//
//  Question.swift
//  Fintech
//
//  Created by apple on 13/04/2020.
//  Copyright © 2020 Broadstone Technologies. All rights reserved.
//

import Foundation
import UIKit
struct Question {
    var qestionNo = String()
    var question = String()
    var category = Int()
    var answer = [String]()
    var option = [String]()
    var multiSelection = Bool()
    var imagName = String()
    mutating func appendAswer(ans:String){
        answer.append(ans)
    }
}
var QuestionAaray:[Question] = [Question]()
extension UIViewController
{
    
    func saveUserData(value:[summary]?, key : String)

    {

        let encoder = JSONEncoder()

        if let encoded = try? encoder.encode(value) {

            let defaults = UserDefaults.standard

            defaults.set(encoded, forKey: key)

        }

    }

    

    func getUserData(key : String)->[summary]?

    {

        var value:[summary]?

        let defaults = UserDefaults.standard

        if let data = defaults.object(forKey: key) as? Data {

            let decoder = JSONDecoder()

            if let loadeddata = try? decoder.decode([summary].self, from: data) {

                value = loadeddata

            }

        }

        return value

    }
}
