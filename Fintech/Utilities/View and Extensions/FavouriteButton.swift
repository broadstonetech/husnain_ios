//
//  FavouriteButton.swift
//  AR_jedi
//
//  Created by MacBook-Mubashar on 21/05/2019.
//  Copyright © 2019 Broadstone Technologies. All rights reserved.
//

import Foundation
import UIKit

class FavouriteButton : UIButton {
    // Images
    let checkedImage = UIImage(named: "fav_filled")! as UIImage
    let uncheckedImage = UIImage(named: "fav_unfilled")! as UIImage
    
    // Bool property
    var isChecked: Bool = false {
        didSet{
            if isChecked == true {
                self.setImage(checkedImage, for: UIControl.State.normal)
            } else {
                self.setImage(uncheckedImage, for: UIControl.State.normal)
            }
        }
    }
    
    override func awakeFromNib() {
        self.addTarget(self, action:#selector(buttonClicked(sender:)), for: UIControl.Event.touchUpInside)
        self.isChecked = false
    }
    
    @objc func buttonClicked(sender: UIButton) {
        if sender == self {
            isChecked = !isChecked
        }
    }
}

