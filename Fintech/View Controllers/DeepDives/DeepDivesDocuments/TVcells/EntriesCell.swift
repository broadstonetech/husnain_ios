//
//  EntriesCell.swift
//  SecDemo
//
//  Created by SaifUllah Butt on 27/10/2021.
//

import UIKit

class EntriesCell: UITableViewCell {

    @IBOutlet weak var value: UILabel!
    @IBOutlet weak var year: UILabel!
    
    @IBOutlet weak var usdView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
