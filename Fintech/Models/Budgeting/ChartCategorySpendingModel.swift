// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let chartCategorySpendingModel = try? newJSONDecoder().decode(ChartCategorySpendingModel.self, from: jsonData)

import Foundation

// MARK: - ChartCategorySpendingModel
struct ChartCategorySpendingModel: Codable {
    let success: Int?
    let data: DataClass?
}

// MARK: - DataClass
struct DataClass: Codable {
    let needsBucket, havesBucket, wantsBucket, miscBucket: [ChartBucket]?

    enum CodingKeys: String, CodingKey {
        case needsBucket = "needs_bucket"
        case havesBucket = "haves_bucket"
        case wantsBucket = "wants_bucket"
        case miscBucket = "misc_bucket"
    }
}

// MARK: - Bucket
struct ChartBucket: Codable {
    let categoryName: String?
    let amount, percentage: Double?

    enum CodingKeys: String, CodingKey {
        case categoryName = "category_name"
        case amount, percentage
    }
}
