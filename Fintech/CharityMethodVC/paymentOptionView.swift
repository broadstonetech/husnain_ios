//
//  paymentOptionView.swift
//  Fintech
//
//  Created by Apple on 22/12/2020.
//  Copyright © 2020 Broadstone Technologies. All rights reserved.
//
import Foundation
import UIKit
import Stripe
import PassKit

class paymentOptionView: UIView {
    @IBOutlet weak var parentView: UIView!
    @IBOutlet weak var paymentOptionsLabel: UILabel!
    @IBOutlet weak var stripeView: UIView!
    @IBOutlet weak var applePayView: UIView!
    @IBOutlet weak var payViaStripeLabel: UILabel!
    @IBOutlet weak var stripeAmountLabel: UILabel!
    @IBOutlet weak var stripeDescLabel: UILabel!
    
    @IBOutlet weak var applePayLabel: UILabel!
    
    @IBOutlet weak var appleAmountLabel: UILabel!
    @IBOutlet weak var applePayDesc: UILabel!
    
    var isTransactionFeeIncluded = true
    var amount = 0.0
    
    private var paymentRequest: PKPaymentRequest = {
            let request = PKPaymentRequest()
            request.merchantIdentifier = "merchant.io.vesgo.app.charity"
            request.supportedNetworks = [.visa, .masterCard,.amex,.discover, .JCB, .chinaUnionPay]
            request.supportedCountries = ["US"]
            request.merchantCapabilities = .capability3DS
            request.countryCode = "US"
            request.currencyCode = "USD"
            return request
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    private func commonInit(){
        Bundle.main.loadNibNamed("paymentOptionView", owner: self, options: nil)
        addSubview(parentView)
        parentView.frame = self.bounds
        parentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        
        addTapGesturesOnViews()
        
        paymentOptionsLabel.text = getLocalizedString(key: "payment_option_lbl", comment: "")
        
    }
    
    private func addTapGesturesOnViews(){
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(stripeViewTapped))
        stripeView.isUserInteractionEnabled = true
        stripeView.addGestureRecognizer(tapGestureRecognizer)
        
        let tapGestureRecognizerForApplePay = UITapGestureRecognizer(target: self, action: #selector(applePayViewTapped))
        applePayView.isUserInteractionEnabled = true
        applePayView.addGestureRecognizer(tapGestureRecognizerForApplePay)
    }
    
    
    @objc func stripeViewTapped(){
        let stipeVC = StipePaymentIntegration()
        self.parentContainerViewController()?.present(stipeVC, animated: true)
        
    }
    
    @objc func applePayViewTapped(){
        print("apple payment")
        paymentRequest.paymentSummaryItems = [PKPaymentSummaryItem(label: "Vesgo", amount: NSDecimalNumber(value: amount))]
        if let controller = PKPaymentAuthorizationViewController(paymentRequest: paymentRequest) {
            controller.delegate = self
            
            self.parentContainerViewController()?.present(controller, animated: true, completion: nil)

        }else {
            DispatchQueue.main.async {
                self.makeToast(ConstantStrings.APPLE_PAY_NOT_SUPPORTED, duration: 3.0, position: .center)
            }
        }
    }
    
    func setData(isTransactionFeeIncluded: Bool, amount: Double) {
        if isTransactionFeeIncluded {
            stripeAmountLabel.text = "$\(FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: amount))"
            appleAmountLabel.text = "$\(FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: amount))"
            
            let stripeDonatedAmount = amount - ((amount * PaymentConstants.STRIPE_TRANSACTION_FEES)/100) - PaymentConstants.STRIPE_CONSTANT_CHARGE
            
            let applePayDonatedAmount = amount - ((amount * PaymentConstants.APPLE_PAY_TRANSACTION_FEES)/100) - PaymentConstants.STRIPE_CONSTANT_CHARGE
            
            PaymentAPIUtils.stripeAmount = stripeDonatedAmount
            PaymentAPIUtils.applePayAmount = applePayDonatedAmount
            
            stripeDescLabel.text = "$\(FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: stripeDonatedAmount)) will be donated"
            applePayDesc.text = "$\(FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: applePayDonatedAmount)) will be donated"
            
            stripeDescLabel.isHidden = false
            applePayDesc.isHidden = false
        }else{
            let applePayAmount = amount + ((amount * PaymentConstants.APPLE_PAY_TRANSACTION_FEES)/100) + PaymentConstants.STRIPE_CONSTANT_CHARGE
            
            let stripePayAmount = amount + ((amount * PaymentConstants.STRIPE_TRANSACTION_FEES)/100) + PaymentConstants.STRIPE_CONSTANT_CHARGE
            
            stripeAmountLabel.text = "$\(FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: stripePayAmount))"
            appleAmountLabel.text = "$\(FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: applePayAmount))"
            
            PaymentAPIUtils.stripeAmount = stripePayAmount
            PaymentAPIUtils.applePayAmount = applePayAmount
            
            stripeDescLabel.isHidden = true
            applePayDesc.isHidden = true
        }
    }
    
}
extension paymentOptionView: PKPaymentAuthorizationViewControllerDelegate {

    func paymentAuthorizationViewController(_ controller: PKPaymentAuthorizationViewController, didAuthorizePayment payment: PKPayment, handler completion: @escaping (PKPaymentAuthorizationResult) -> Void) {

        let paymentToken = payment.token
        print(paymentToken.paymentData)
        print("************")
        if #available(iOS 13.4, *) {
            print(paymentToken.paymentMethod.billingAddress)
            print(paymentToken.paymentMethod.secureElementPass)
        }
        print(paymentToken.paymentMethod.displayName)
        print(paymentToken.paymentMethod.network)
        print(paymentToken.paymentMethod.paymentPass)
        
        print("************")
        print(paymentToken.transactionIdentifier)
        
        makeToast("Token: \(paymentToken) \n Data: \(paymentToken.paymentData)", duration: 10)
        
        completion(PKPaymentAuthorizationResult(status: .success, errors: nil))

    }

    func paymentAuthorizationViewControllerDidFinish(_ controller: PKPaymentAuthorizationViewController) {

        controller.dismiss(animated: true, completion: nil)

        

    }
}

