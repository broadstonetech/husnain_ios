//
//  ValueMappingDialogCell.swift
//  Fintech
//
//  Created by broadstone on 11/03/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import UIKit

class ValueMappingDialogCell: UITableViewCell {

    @IBOutlet weak var merchantName: UILabel!
    @IBOutlet weak var amountSpent: UILabel!
    @IBOutlet weak var date: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
