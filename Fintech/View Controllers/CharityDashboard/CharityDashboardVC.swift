//
//  CharityDashboardVC.swift
//  Fintech
//
//  Created by broadstone on 29/12/2020.
//  Copyright © 2020 Broadstone Technologies. All rights reserved.
//

import Foundation
import UIKit
import RingGraph

class CharityDashboardVC:PaymentsBaseClass{
    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet weak var bottomSheetContainerView:UIView!
    @IBOutlet weak var dialogView:SetPlanDialogView!
    
    @IBOutlet weak var blurView: UIVisualEffectView!
    @IBOutlet weak var historyNotAvailableView: UIView!
    var expandableItemsIndex = [Int]()
    
    
    var charityNameArr = [String]()
    var charityAmountArr = [String]()
    var charityMethodArr = [String]()
    var charityDateArr = [String]()
    
    var donationsHistoryData:CharityHistoryDonationsModel!
    var isApiCalled = false
    
    var getPlanData:GetCharitablePlanModel!
    
    override func viewDidLoad() {
        
        
        //removeAllPreviousVC()
        mainTableView.delegate = self
        mainTableView.dataSource = self
        mainTableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        setNavBarTitile(title: "", topItem: "Charitable givings")
        
        createRightButton(title: getLocalizedString(key: "Donate_btn"))
        
        
        bottomSheetContainerView.isHidden = true
        
        //Add close action for dialog view
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(closeButtonTapped))
        blurView.isUserInteractionEnabled = true
        blurView.addGestureRecognizer(tapGestureRecognizer)

        
        //Add action for set plan button
        dialogView.setPlanBtn.addTarget(self, action: #selector(setPlanButtonPressed), for: .touchUpInside)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        setNavBarTitile(title: "", topItem: "Charitable givings")
        
        DispatchQueue.main.async { [self] in
            getCharitablePlanAPI()
            getDonationsHistoryAPI()
        }
    }
    
//    private func removeAllPreviousVC(){
//        navigationController?.viewControllers.removeAll(where: { (vc) -> Bool in
//            if vc.isKind(of: UITabBarController.self) || vc.isKind(of: CharityDashboardVC.self) {
//                return false
//            } else {
//                return true
//            }
//        })
//    }
    
    private func loadLocalData(){
        charityNameArr.append("Three Square")
        charityNameArr.append("Second Harvest Bank of Metrolina")
        charityNameArr.append("Atlanta Community Food Bank")
        
        charityAmountArr.append("$500")
        charityAmountArr.append("$400")
        charityAmountArr.append("$350")
        
        charityMethodArr.append("Direct cash donation")
        charityMethodArr.append("Direct cash donation")
        charityMethodArr.append("Direct cash donation")
        
        charityDateArr.append("08/12/2020")
        charityDateArr.append("11/12/2020")
        charityDateArr.append("12/12/2020")
    }
    
    func createRightButton(title: String){
        
        let rightButton = UIButton(frame: CGRect(x: 0, y: 0, width: 45, height: 16))
        rightButton.setTitle(title, for: .normal)
        rightButton.addTarget(self, action: #selector(btnDonatedPressed(_:)), for: .touchUpInside)
        //rightButton.titleLabel?.font = UIFont(name: "HelveticaNeue", size: 15)
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: rightButton)
    }
    
    func showSetPlanDialogView(isHide: Bool){
        if isHide {
            dialogView.cancelPressed()
            blurView.isHidden = true
            dialogView.isHidden = true
        }else{
            blurView.isHidden = false
            dialogView.isHidden = false
        }
    }
    
    
    @objc func setGoalButtonPressed(){
        showSetPlanDialogView(isHide: false)
    }
    
    @IBAction func btnDonatedPressed(_ sender: UIButton) {
        
        let storyboard = UIStoryboard(name: "Charity", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "listOfCharitiesVC") as! listOfCharitiesVC
        // self.navigationController?.pushViewController(vc, animated: true)
        self.navigationController?.pushViewController(vc, animated: true)
        
        setAnalyticsAction(ScreenName: "CharityDashboard", method: "DonateBtnPressed")
    }
    
    @objc func closeButtonTapped(){
        showSetPlanDialogView(isHide: true)
    }
    
    @objc func setPlanButtonPressed(){
        if isTextFieldDataValid() {
            let amount = Int(dialogView.targetAmount.text!)
            setDonationPlanAPI(plannedAmount:amount!,
                               targetDate: dialogView.targetDateInUNIX)
        }
        
    }
    
    //Mark: Field Validation
    private func isTextFieldDataValid() -> Bool {
        let amount = Int(dialogView.targetAmount.text!)
        if (dialogView.targetAmount.text!.isEmpty  || amount == 0) {
            view.makeToast(getLocalizedString(key: "please_enter_amount", comment: ""))
            return false
        }
        if dialogView.targetDate.text!.isEmpty {
            view.makeToast(getLocalizedString(key: "please_enter_date", comment: ""))
            return false
        }
        return true
    }
    
    // Mark: Set Charitable Plan API
    
    func setDonationPlanAPI(plannedAmount: Int, targetDate: Int64){
        self.view.activityStartAnimating()
        let url = ApiUrl.SET_CHARITABLE_PLAN_URL
        
        let namespace = SPManager.sharedInstance.getCurrentUserNamespace()
        let data: Dictionary<String, AnyObject> = ["planned_amount": plannedAmount as AnyObject, "target_date": targetDate as AnyObject, "timestamp": Int64(NSDate().timeIntervalSince1970)*1000 as AnyObject]
        let params: Dictionary<String, AnyObject> = ["namespace": namespace as AnyObject,
                                                     "data": data as AnyObject]
        
        print(params)
        APIManager.sharedInstance.postRequest(serviceName: url, sendData: params, success: { (response) in
            print(response)
            
            if APIManager.sharedInstance.isSuccess {
                if response["success"] as? Bool ?? false{
                   print("Success")
                    
                } else {
                        //self.showAlert(ConstantStrings.NO_RESULT_FOUND_ALERT_TEXT)
                }
                DispatchQueue.main.async {[self] in
                    self.view.activityStopAnimating()
                    showSetPlanDialogView(isHide: true)
                }
                DispatchQueue.main.async {
                    self.getCharitablePlanAPI()
                    self.getDonationsHistoryAPI()
                }
            }
        }, failure: { (data) in
            print(data)
            DispatchQueue.main.async {[self] in
                self.view.activityStopAnimating()
                showSetPlanDialogView(isHide: true)
            }
            if APIManager.sharedInstance.error400 {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
            }
            if(APIManager.sharedInstance.error401)
            {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
            }else{
                self.showAlert(ConstantStrings.Error_msg_generic)
            }
        })
    }
    
    
    // Mark: Get Donations History API
    
    func  getDonationsHistoryAPI(){
        historyNotAvailableView.isHidden = true
        self.view.activityStopAnimating()
        let url = ApiUrl.GET_CHARITY_DONATIONS_HISTORY_URL
        
        let namespace = SPManager.sharedInstance.getCurrentUserNamespace()
    
        postManagerUser(method: "POST", isThisURLEmbdedData: false, urlString: url, parameter: ["namespace": namespace], success: { [self](value) in
            
            do
            {
                let json = try? JSONSerialization.jsonObject(with: value, options: []) as! [String:Any]
                let hasDonated = json?["has_donated"] as? Bool
                if hasDonated ?? false {
                    self.donationsHistoryData = try JSONDecoder().decode(CharityHistoryDonationsModel?.self, from: value)! as CharityHistoryDonationsModel
                    
                    self.donationsHistoryData.data.sort {
                        $0.donationTime > $1.donationTime
                    }
                    
                
                    DispatchQueue.main.async {
                        self.view.activityStopAnimating()
//                        if self.getPlanData != nil {
//
//                        }
                        self.isApiCalled = true
                        self.mainTableView.reloadData()
                        self.historyNotAvailableView.isHidden = true
                        
                    }

                }else {
                    DispatchQueue.main.async {
                        self.historyNotAvailableView.isHidden = false
                    }
                }
                
            }
            catch{
                print("catch block of get/donation/history")
            }
            DispatchQueue.main.async {[self] in
                self.view.activityStopAnimating()
            }
            
        }, failure: {(err) in
            DispatchQueue.main.async {[self] in
                self.view.activityStopAnimating()
            }
            if APIManager.sharedInstance.error400 {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
            }
            if(APIManager.sharedInstance.error401)
            {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
            }else{
                self.showAlert(ConstantStrings.Error_msg_generic)
            }
        })
    }
    
    
    // Mark: Get Charitable Plan API
    func  getCharitablePlanAPI(){
        DispatchQueue.main.async {
            self.view.activityStartAnimating()
        }
        let url = ApiUrl.GET_CHARITABLE_PLAN_URL
        
        let namespace = SPManager.sharedInstance.getCurrentUserNamespace()
    
        postManagerUser(method: "POST", isThisURLEmbdedData: false, urlString: url, parameter: ["namespace": namespace], success: {(value) in
            
            
            do
            {
                let json = try? JSONSerialization.jsonObject(with: value, options: []) as! [String:Any]
                let success = (json?["success"] as? Int) ?? 0
                
                if success == 1 {
                    self.getPlanData = try JSONDecoder().decode(GetCharitablePlanModel?.self, from: value)! as GetCharitablePlanModel
                
                    DispatchQueue.main.async { [self] in
                        
                        if donationsHistoryData != nil {
                            isApiCalled = true
                            
                        }
                        
                    }
                    
                    DispatchQueue.main.async {
                        self.mainTableView.reloadData()
                    }

                }
                    
                
            }
            catch{
                print("catch block of get/donation/goal")
            }
            
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
        }, failure: {(err) in
            DispatchQueue.main.async { [self] in
                self.view.activityStopAnimating()
            }
            
            if APIManager.sharedInstance.error400 {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
            }
            if(APIManager.sharedInstance.error401)
            {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
            }else{
                self.showAlert(ConstantStrings.Error_msg_generic)
            }
        })
    }
          
}
extension CharityDashboardVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //return 1 + 1 + donationsHistoryData.data.count
        if section < 2 {
            return  1
        }
        if expandableItemsIndex.contains(section){
            if !donationsHistoryData.data.isEmpty {
            return (donationsHistoryData.data[section - 2].charitiesDistribution.count) + 1
            } else {
                return 0
            }
        }else {
            return  1
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if isApiCalled{
            return donationsHistoryData.data.count + 2
        }else{
            return 1
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        //New Code
        if indexPath.section == 0 && indexPath.row == 0 {
            
            if self.getPlanData != nil {
                let cell = self.mainTableView.dequeueReusableCell(withIdentifier: "RingGraphCell") as! CharityGoalGraphCell
                DispatchQueue.main.async {[self] in
                    cell.populateMovementsRingCharts(currentValue: Int(getPlanData.planData.donatedAmount)
                                                     ,maxValue: Int(getPlanData.planData.plannedAmount))
                    cell.targetData.text = FintechUtils.sharedInstance.getDateInUSLocale(timestamp: getPlanData.planData.targetDate / 1000)
                    
                    cell.goalNotSetView.isHidden = true
                    cell.goalAvailableView.isHidden = false
                    cell.targetDateView.isHidden = false
                    
                    cell.amountSpent.text = "$" + FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: getPlanData.planData.donatedAmount)
                    cell.totalAmount.text = "$" + FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: getPlanData.planData.plannedAmount)
                    
                    
                    let percentDonated = (getPlanData.planData.donatedAmount / getPlanData.planData.plannedAmount) * 100
                    
                    cell.amountDonatedLblForGraph.text = FintechUtils.sharedInstance.doubleToPercentageOutput(doubleVal: percentDonated) + " %"
                }
                
                return cell
            
            }else{
                let cell = self.mainTableView.dequeueReusableCell(withIdentifier: "RingGraphCell") as! CharityGoalGraphCell
                cell.amountDonatedLblForGraph.text = "0%"
                cell.goalNotSetView.isHidden = false
                cell.goalAvailableView.isHidden = true
                cell.targetDateView.isHidden = true
                cell.populateMovementsRingCharts(currentValue: 0, maxValue: 100)
                cell.setGoalBtn.addTarget(self, action: #selector(setGoalButtonPressed), for: .touchUpInside)
                
                
                return cell
            }
            
        } else if indexPath.section == 1 && indexPath.row == 0 {
            let cell = self.mainTableView.dequeueReusableCell(withIdentifier: "DonationLabelCell") as! DonationLabelCell
            
            return cell
            
//        } else {
        } else if !donationsHistoryData.data.isEmpty {
            
            if expandableItemsIndex.contains(indexPath.section){
                
                if indexPath.row == 0 {
                    
                    let cell =  self.mainTableView.dequeueReusableCell(withIdentifier: "DonationHistoryHeaderCell") as! DonationHistoryHeaderCell
                    let item = donationsHistoryData.data[indexPath.section - 2]
                    
                    cell.donationDate.text = FintechUtils.sharedInstance.getOnlyDate(timestamp: item.donationTime / 1000)
                    cell.donationMonth.text = FintechUtils.sharedInstance.getMonth(timestamp: item.donationTime / 1000)
                    cell.donationTotalAmountLabel.text = "$\(FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: item.donatedAmount))"
                    if #available(iOS 13.0, *) {
                        cell.dropdownImage.image = UIImage(systemName: "chevron.up")
                    } else {
                        // Fallback on earlier versions
                    }
                    return cell
                    
                }else {
                    
                    let cell =  self.mainTableView.dequeueReusableCell(withIdentifier: "DonationHistoryCell") as! DonationHistoryCell
                    
                    let item = donationsHistoryData.data[indexPath.section - 2]
                    
                    cell.charityName.text = item.charitiesDistribution[indexPath.row-1].charityName
                    cell.donationAmount.text = "$\(FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: item.charitiesDistribution[indexPath.row-1].shareAmount))"
                    
                    if item.plan == "invest" {
                        cell.donationMethod.text = "Invest charitable amount"
                    }else{
                        cell.donationMethod.text = "Direct cash donation"
                    }
                    
                    
                    return cell
                }
                
            } else {
                
                let cell =  self.mainTableView.dequeueReusableCell(withIdentifier: "DonationHistoryHeaderCell") as! DonationHistoryHeaderCell
                let item = donationsHistoryData.data[indexPath.section - 2]
                
                cell.donationDate.text = FintechUtils.sharedInstance.getOnlyDate(timestamp: item.donationTime / 1000)
                cell.donationMonth.text = FintechUtils.sharedInstance.getMonth(timestamp: item.donationTime / 1000)
                cell.donationTotalAmountLabel.text = "$\(FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: item.donatedAmount))"
                if #available(iOS 13.0, *) {
                    cell.dropdownImage.image = UIImage(systemName: "chevron.down")
                } else {
                    // Fallback on earlier versions
                }
                return cell
            }
        }
        
        return UITableViewCell()
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 && self.getPlanData != nil {
            self.showSetPlanDialogView(isHide: false)
        }
        else{
            if indexPath.section > 1 {
                if indexPath.row == 0 {
                    if expandableItemsIndex.contains(indexPath.section){
                        let index = expandableItemsIndex.firstIndex(of: indexPath.section)
                        expandableItemsIndex.remove(at: index!)
                    }else {
                        expandableItemsIndex.append(indexPath.section)
                    }
                    mainTableView.reloadData()
                }else{
                    let item = donationsHistoryData.data[indexPath.section - 2]
                    openBottomActionSheet(charityData: item.charitiesDistribution[indexPath.row-1])
                }
                
            }
        }
        
  
    }
    
}
extension CharityDashboardVC{
    func openBottomActionSheet(charityData: CharitiesDistribution) {
        // if NetworkHelper.sharedInstance.isDmoatOnline {
        let optionMenu = UIAlertController(title: nil, message: charityData.charityName, preferredStyle: .actionSheet)
        
        let share = UIAlertAction(title: "Tell your friends", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            let shareText = "I donated to " + charityData.charityName + " that supports my values. Check them out and help if you can.\n" + ConstantStrings.APP_LINK
            self.shareTextualData(textData: shareText)
            
        })
        let donateAgain = UIAlertAction(title: "Donate again", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            charityidarray.removeAll()
            List_values.removeAll()
            ArrayLists.sharedInstance.SelectedListOfCharies.removeAll()
            SelectedUserValues.removeAll()
            
            charityidarray.append(charityData.charityID ?? "")
            List_values.append(charityData.charityName ?? " ")
            
            let charity = Charity.self.init(charityID: charityData.charityID,
                         charityName: charityData.charityName,
                         url: charityData.url, donationUrl: charityData.donationUrl ?? "",
                         charityDescription: charityData.charitiesDistributionDescription, otherValues: nil)
            
            ArrayLists.sharedInstance.SelectedListOfCharies.append(charity)
            
            PaymentAPIUtils.isEqualDistribution = false
            let storyboard = UIStoryboard(name: "Charity", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "customizedDistributionVC") as! customizedDistributionVC
            count_LIST_VALUES = 1
            self.navigationController?.pushViewController(vc, animated: true)
            
//            let storyboard = UIStoryboard(name: "Charity", bundle: nil)
//            let vc = storyboard.instantiateViewController(withIdentifier: "CharityMethodVC") as! CharityMethodVC
//            // self.navigationController?.pushViewController(vc, animated: true)
//            count_LIST_VALUES = 1
//            self.navigationController?.pushViewController(vc, animated: true)
            
        })
        
        let visitWeb = UIAlertAction(title: "Visit website", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            FintechUtils.sharedInstance.openUrl(url: charityData.url)
            
        })
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            
            self.dismiss(animated: true, completion: nil)
            
        })
        
        
        
        optionMenu.addAction(donateAgain)
        optionMenu.addAction(share)
        optionMenu.addAction(visitWeb)
        optionMenu.addAction(cancel)
        self.present(optionMenu, animated: true, completion: nil)
    }
}
