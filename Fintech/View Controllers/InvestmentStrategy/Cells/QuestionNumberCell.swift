//
//  QuestionNumberCell.swift
//  Fintech
//
//  Created by broadstone on 31/03/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import UIKit

class QuestionNumberCell: UITableViewCell {
    @IBOutlet weak var questionNumberLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
