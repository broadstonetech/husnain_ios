//
//  samplePortrfolio.swift
//  Fintech
//
//  Created by Apple on 02/07/2020.
//  Copyright © 2020 Broadstone Technologies. All rights reserved.
//

import UIKit
import MTSlideToOpen


var sampleportfolio_username = ""
struct profiledata{
    var username : String!
    var agegroup : String!
    var income :String!
    var invesment :String!
    var risk :String!
    var descripation :String!
    var img : String!
    var Investmenthorizon : String!
    var infocaresabout :String!
    var infoavoid : String!
    var headingcaresabout :String!
    var headingavoid : String!
    // var ansIndex : Int!
}
class samplePortrfolio: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,MTSlideToOpenDelegate{
  
    
  
    @IBOutlet weak var SlideButtonView: UIView!
    
    var Userdata = [profiledata]()
    @IBOutlet weak var pagecontroler: UIPageControl!
    @IBOutlet weak var CollectionView: UICollectionView!
    
     lazy var slideToOpen: MTSlideToOpenView = {
            let slide = MTSlideToOpenView(frame: CGRect(x: 0, y: 0, width: 300, height: 40))
            slide.sliderViewTopDistance = 0
            slide.sliderCornerRadius = 20
            slide.showSliderText = true
    //        slide.thumbnailColor = UIColor(red:141.0/255, green:19.0/255, blue:65.0/255, alpha:1.0)
            //slide.thumbnailColor = #colorLiteral(red: 0.4705882353, green: 0.3725490196, blue: 0.9333333333, alpha: 0.6422838185)
            slide.slidingColor = #colorLiteral(red: 0.1513735056, green: 0.6249956489, blue: 0.8893578649, alpha: 1)
            slide.textColor = UIColor.white
       // slide.sliderBackgroundColor = UIColor(red:0.88, green:1, blue:0.98, alpha:1.0)
            slide.sliderBackgroundColor = #colorLiteral(red: 0.4745098054, green: 0.8392156959, blue: 0.9764705896, alpha: 1)
            slide.delegate = self
            slide.labelText = "Slide to Sign in"
            slide.thumnailImageView.image = #imageLiteral(resourceName: "next").imageFlippedForRightToLeftLayoutDirection()

            return slide
        }()


    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Saim Did
        
        SlideButtonView.addSubview(slideToOpen)
        
        let upwardSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))
        
        upwardSwipe.direction = .up
        
        
        UIView.animate(withDuration: 0.5, animations: {//do your stuff here
            
            self.view.addGestureRecognizer(upwardSwipe)
        })
        
        
        
        self.navigationController?.isNavigationBarHidden = true
        pagecontroler.currentPage = 0
        if SPManager.sharedInstance.saveUserLoginStatus() {
            if let tabbedbar = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "nav_bar_start") as? UINavigationController {
                UIApplication.shared.keyWindow?.rootViewController = tabbedbar
            }
        }else{
            
        }
        Userdata = [profiledata(username: "Emily", agegroup: "Millennial", income: "$65,000", invesment: "$10,000", risk:"High", descripation: "Emily wants to invest in companies that support her values.Vesgo designed a portfolio that is aligned with Emily values, risk tolerance, and time horizon.", img: "f_gen_x", Investmenthorizon: "10+ years", infocaresabout: "Animal welfare,Vegan,UN SDP goals,climate change,Green tech,child labor ", infoavoid: "Fossil fuel,Polluting technologies",headingcaresabout: "Emily cares about", headingavoid: "Emily wants to avoid"), profiledata(username: "Jason", agegroup: "Gen X", income: "$95,000", invesment: "$10,000", risk:"Medium", descripation: "Vesgo designed a portfolio that is aligned with Jason values, risk tolerance, and time horizon.", img: "m_gen_z", Investmenthorizon: "5-7 years", infocaresabout: "GMO,Firearms,Tobacco", infoavoid: "Gender equality,Minority ownership and representation",headingcaresabout: "Jason wants to avoid", headingavoid: "Jason supports"),profiledata(username: "Caleb", agegroup: "Gen Z", income: "$35,000", invesment: "$10,000", risk:"High", descripation: "Vesgo designed a portfolio that is aligned with Caleb’s values, risk tolerance, and time horizon", img: "m_gen_x", Investmenthorizon: "15+ years", infocaresabout: "Wind power,Diversity,Labor rights", infoavoid: "Weapon manufacturers,Military contractors",headingcaresabout: "Caleb cares about", headingavoid: "Caleb wants to avoid")]
        
        // Do any additional setup after loading the view.
    }
    
    
    
    @objc func handleSwipes(_ sender:UISwipeGestureRecognizer) {
        guestuser = "aqib"
        UserDefaults.standard.set(guestuser, forKey: "guestuser")
        if (sender.direction == .up) {
            print("up")
            var ab = pagecontroler.currentPage
            if ab == 0{
                sampleportfolio_username = "Emily"
                UserDefaults.standard.set(sampleportfolio_username, forKey: "sampleportfolio_username")
                SPManager.sharedInstance.saveCurrentUserNamespace("gst-one1")
            }else if ab == 1{
                sampleportfolio_username = "Jason"
                UserDefaults.standard.set(sampleportfolio_username, forKey: "sampleportfolio_username")
                SPManager.sharedInstance.saveCurrentUserNamespace("gst-two2")
            }else if ab == 2{
                sampleportfolio_username = "Caleb"
                UserDefaults.standard.set(sampleportfolio_username, forKey: "sampleportfolio_username")
                SPManager.sharedInstance.saveCurrentUserNamespace("gst-three")
            }
            
            if let tabbedbar = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "nav_bar_start") as? UINavigationController {
                self.view.window?.rootViewController = tabbedbar
            }
            
        }
    }
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Userdata.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! SamplePortfolio_cell
        cell.age_group.text = Userdata[indexPath.item].agegroup
        cell.Income_lbl.text = Userdata[indexPath.item].income
        cell.Invesment_lbl.text = Userdata[indexPath.item].invesment
        cell.risk_lbl.text = Userdata[indexPath.item].risk
        cell.descripation_lbl.text = Userdata[indexPath.item].descripation
        cell.username.text = Userdata[indexPath.item].username
        cell.profile_img.image = UIImage(named: Userdata[indexPath.item].img)
        cell.InfoAvoid_lbl.text = Userdata[indexPath.item].infoavoid
        cell.infoCaresAbout_lbl.text = Userdata[indexPath.item].infocaresabout
        cell.invesmentHorizon_lbl.text = Userdata[indexPath.item].Investmenthorizon
        cell.headingCaresAbout_lbl.text = Userdata[indexPath.item].headingcaresabout
        cell.headingAvoid_lbl.text = Userdata[indexPath.item].headingavoid
        
        //Userdata[indexPath.item].img
        
        return cell
        
    }
    
    
    //Saim Did
    func mtSlideToOpenDelegateDidFinish(_ sender: MTSlideToOpenView) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "Main_login_page") as UIViewController
        UIApplication.shared.keyWindow?.rootViewController = vc
        
    }
    
    
    
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        var visibleRect = CGRect()
        
        visibleRect.origin = CollectionView.contentOffset
        visibleRect.size = CollectionView.bounds.size
        
        let visiblePoint = CGPoint(x: visibleRect.minX, y: visibleRect.minY)
        
        guard let indexPath = CollectionView.indexPathForItem(at: visiblePoint) else { return }
        
        print(indexPath)
        pagecontroler.currentPage = indexPath.item
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
   
    @IBAction func CancelButton(_ sender: Any) {
        print("Back")
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "nav_bar_start") as UIViewController
        UIApplication.shared.keyWindow?.rootViewController = vc
        
    }
    
}
extension samplePortrfolio: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = collectionView.frame.size
        
        return CGSize(width: size.width, height: size.height)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}

