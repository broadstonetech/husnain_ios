//
//  ControversyDialogView.swift
//  Fintech
//
//  Created by broadstone on 22/03/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import UIKit

class ControversyDialogView: UIView {
    
    let companies = ["Apple Inc", "Facebook Inc", "BeiGene Ltd"]
    let tickers = ["AAPL", "FB", "BGNE"]
    let controversies = [5, 4, 2]
    
    
    @IBOutlet weak var parentView: UIView!
    @IBOutlet weak var controversyLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var relatedControveriesLabel: UILabel!
    @IBOutlet weak var peerControveriesSV: UIStackView!
    @IBOutlet weak var seperatorView: UIView!
    var controversyModel: ControversyModel!

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    private func commonInit(){
        Bundle.main.loadNibNamed("ControversyDialogView", owner: self, options: nil)
        addSubview(parentView)
        parentView.frame = self.bounds
        parentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        
        let identifier = "ControversyDialogViewCell"
        collectionView.register(UINib(nibName: "ControversyDialogViewCell", bundle: nil), forCellWithReuseIdentifier: identifier)
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        let layout = TagFlowLayout()
        layout.estimatedItemSize = CGSize(width: 140, height: 40)
        collectionView.collectionViewLayout = layout
        
        
    }
    
    func setValues(){
        controversyLabel.text = FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: controversyModel.data.peerControversy)
        relatedControveriesLabel.text = getLocalizedString(key: "related_controversies") + " (\(controversyModel.data.controversies.count))"
        
        if controversyModel.data.peerControversy == 0.0 {
            peerControveriesSV.isHidden = true
            seperatorView.isHidden = true
            peerControveriesSV.heightConstaint?.constant = 0
        }else {
            peerControveriesSV.isHidden = false
            seperatorView.isHidden = false
            peerControveriesSV.heightConstaint?.constant = 40
        }
    }
    
    
//    @objc func closeBtnTapped(){
//        parentView.removeFromSuperview()
//    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    
    
    
    func show(){
        UIApplication.shared.keyWindow?.addSubview(parentView)
    }

}

extension ControversyDialogView: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.controversyModel == nil {
            return 0
        }else {
            return controversyModel.data.controversies.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let identifier = "ControversyDialogViewCell"
        var cell: ControversyDialogViewCell? = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath) as? ControversyDialogViewCell
        
        if cell == nil {
            collectionView.register(UINib(nibName: "ControversyDialogViewCell", bundle: nil), forCellWithReuseIdentifier: identifier)
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath) as? ControversyDialogViewCell
        }
        
        cell!.tagLabel.text = controversyModel.data.controversies[indexPath.row]
        
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        print("kind = \(kind)")
        
        collectionView.backgroundColor = .clear
        
        return UICollectionReusableView()
    }
    
}
