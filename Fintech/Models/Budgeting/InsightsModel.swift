//
//  InsightsModel.swift
//  Fintech
//
//  Created by broadstone on 12/02/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import Foundation
// MARK: - RecurringChargesModel
struct RecurringChargesModel {
    var marchantName = ""
    var amount = 0.0
    var frequency = ""
    var transactions = [RecurringChargesTransactions]()
}

struct RecurringChargesTransactions {
    var amount = 0.0
    var date: Int64 = 0
}

struct BudgetOverspentModel {
    var categoryName = ""
    var amountSpent = 0.0
    var amountPlanned = 0.0
}

struct ValueMappingModel {
    var valueName = ""
    var NumVoilations = 0
    var detail = [ValueMappingDetailsModel]()
}

struct ValueMappingDetailsModel {
    var marchantName = ""
    var amount = 0.0
    var data: Int64 = 0
}
