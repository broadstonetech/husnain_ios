//
//  PortfolioChartModel.swift
//  Fintech
//
//  Created by broadstone on 20/03/2022.
//  Copyright © 2022 Broadstone Technologies. All rights reserved.
//

import Foundation

struct PortfolioChartModel {
    let success: Bool
    let message: String
    var data: PortfolioChartData
}

struct PortfolioChartData {
    var vesgoData: [PortfolioChartDataPoints]
    var benchMarkData: [PortfolioChartDataPoints]
}

struct PortfolioChartDataPoints {
    let xAxis: Int64
    let yAxis: Double
}
