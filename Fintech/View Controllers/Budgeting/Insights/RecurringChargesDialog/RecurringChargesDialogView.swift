//
//  RecurringChargesDialogView.swift
//  Fintech
//
//  Created by broadstone on 10/03/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import UIKit

class RecurringChargesDialogView: UIView {
    
    let dates = ["01/01/21", "02/01/21", "03/01/21"]
    let amount = "$9.99"
    var transactions = [RecurringChargesTransactions]()

    @IBOutlet weak var parentView: UIView!
    @IBOutlet weak var merchantName: UILabel!
    @IBOutlet weak var freqLabel: UILabel!
    @IBOutlet weak var frequency: UILabel!
    @IBOutlet weak var dateHeadingLabel: UILabel!
    @IBOutlet weak var amountHeadingLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var closeBtn: UIButton!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    private func commonInit(){
        Bundle.main.loadNibNamed("RecurringChargesView", owner: self, options: nil)
        addSubview(parentView)
        parentView.frame = self.bounds
        parentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        tableView.delegate = self
        tableView.dataSource = self
        
        
        
    }
    
    
//    @objc func closeBtnTapped(){
//        parentView.removeFromSuperview()
//    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    
    
    
    func show(){
        UIApplication.shared.keyWindow?.addSubview(parentView)
    }

}

extension RecurringChargesDialogView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if transactions.count >= 5 {
            return 5
        }
        return transactions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let identifier = "RecurringChargesNibCell"
        var cell: RecurringChargesNibCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? RecurringChargesNibCell
        if cell == nil {
            tableView.register(UINib(nibName: "RecurringChargesNibDialogCell", bundle: nil), forCellReuseIdentifier: identifier)
            cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? RecurringChargesNibCell
        }
        
//        let cell = tableView.dequeueReusableCell(withIdentifier: "RecurringChargesNibCell") as! RecurringChargesNibCell
        
        cell.date.text = FintechUtils.sharedInstance.getDateInUSLocale(timestamp: transactions[indexPath.row].date / 1000 )
        cell.amount.text = "$" + FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: transactions[indexPath.row].amount)
        
        return cell
    }
    
    
    
}
