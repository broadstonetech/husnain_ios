//
//  SignUpVC.swift
//  Fintech
//
//  Created by MacBook-Mubashar on 22/08/2019.
//  Copyright © 2019 Broadstone Technologies. All rights reserved.
//////#################################################################
//  In This view create new account user signup
//    first name
//    last name
//    user name
//    Email
//    password
//    confirm password
//    Signupn button
//    hide and show password button (eye)
//    api call Signup
//    Textfield alert box
//################################################################

import UIKit
import Toast_Swift

class SignUpVC: Parent {
    // MArk: - TextField outlets
    @IBOutlet var firstname_tf: UITextField!
    @IBOutlet var lastname_tf: UITextField!
    @IBOutlet var username_tf: UITextField!
    @IBOutlet var email_tf: UITextField!
    @IBOutlet var password_tf: UITextField!
    @IBOutlet var conformpassword_tf: UITextField!
    // MArk: - IB outlets
    @IBOutlet var signup_btn: UIButton!
    @IBOutlet var passwordtoggle_btn: UIButton!
    @IBOutlet var conformpasswordtoggle_btn: UIButton!
    
    @IBOutlet weak var createAccount_lbl: UILabel!
    
    @IBOutlet weak var alreadyAccount_btn: UIButton!
    
    //Text View
    @IBOutlet weak var privacyTv: UILabel!
    
    @IBOutlet weak var checkBoxLbl: UIButton!
    
    var Flag = false
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // MARK:- textfield delegates
        privacyAttributedString()
        
        firstname_tf.delegate = self
        lastname_tf.delegate = self
        username_tf.delegate = self
        email_tf.delegate = self
        password_tf.delegate = self
        conformpassword_tf.delegate = self
        // MARK:- Buttons Actions
        signup_btn.addTarget(self, action: #selector(signupTapped), for: UIControl.Event.touchUpInside)
        passwordtoggle_btn.addTarget(self, action: #selector(viewPasswordTapped), for: UIControl.Event.touchUpInside)
        conformpasswordtoggle_btn.addTarget(self, action: #selector(viewConfirmPasswordTapped), for: UIControl.Event.touchUpInside)
        // Do any additional setup after loading the view.
        
        let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(openPrivacyPolicyLink))
        privacyTv.isUserInteractionEnabled = true
        privacyTv.addGestureRecognizer(tapGesture)
    }
    override func viewWillAppear(_ animated: Bool) {
        localization()
    }
    func localization(){
        // setNavBarTitile(title:getLocalizedString(key: "Education", comment: ""), topItem: getLocalizedString(key: "Education", comment: ""))
        
        let createaccount = getLocalizedString(key: "Create account", comment: "")
        createAccount_lbl.text = createaccount
        signup_btn.setTitle(getLocalizedString(key: "Sign up", comment: ""), for: .normal)
        alreadyAccount_btn.setTitle(getLocalizedString(key: "Already have an account?", comment: ""), for: .normal)
        //navigationController?.navigationBar.barStyle = .blackOpaque
        firstname_tf.placeholder = getLocalizedString(key: "first_name", comment: "")
        lastname_tf.placeholder = getLocalizedString(key: "last_name", comment: "")
        username_tf.placeholder = getLocalizedString(key: "username_", comment: "")
        email_tf.placeholder = getLocalizedString(key: "email_", comment: "")
        password_tf.placeholder = getLocalizedString(key: "password_", comment: "")
        conformpassword_tf.placeholder = getLocalizedString(key: "confirmPassword", comment: "")
        
    }
    
    //MARK: - Controlling the Keyboard
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == firstname_tf {
            firstname_tf.resignFirstResponder()
            lastname_tf.becomeFirstResponder()
        } else if textField == lastname_tf {
            lastname_tf.resignFirstResponder()
            username_tf.becomeFirstResponder()
        }else if textField == username_tf {
            username_tf.resignFirstResponder()
            email_tf.becomeFirstResponder()
        } else if textField == email_tf {
            email_tf.resignFirstResponder()
            password_tf.becomeFirstResponder()
        } else if textField == password_tf {
            password_tf.resignFirstResponder()
            conformpassword_tf.becomeFirstResponder()
        } else if textField == conformpassword_tf {
            conformpassword_tf.resignFirstResponder()
        } else{
            textField.resignFirstResponder()
            
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == username_tf {
            return string.rangeOfCharacter(from: CharacterSet(charactersIn: "§±!#$%^&*+()-= []{};:/?>,<")) == nil
        }
        return string.rangeOfCharacter(from: CharacterSet(charactersIn: "")) == nil
        
    }
    
    
    // MARK: - IB Actions
    @objc func signupTapped() {
        print("Signup tapped")
        if validateSignUpForm() {
            
            if NetworkReachability.isConnectedToNetwork() {
                
                callSignUP()
                
                self.setAnalyticsAction(ScreenName:"SignUpVc", method: "SignUpMade")
                
                
            } else {
                showAlert(ConstantStrings.NO_INTERNET_AVAILABLE)
            }
        } else {
            print("sign up failed")
        }
    }
    
    @objc func viewPasswordTapped() {
        showHidePassword(textfield: password_tf, btnPwd: passwordtoggle_btn)
    }
    
    @objc func viewConfirmPasswordTapped() {
        showHidePassword(textfield: conformpassword_tf, btnPwd: conformpasswordtoggle_btn)
    }
    
    
    @IBAction func checkBoxAction(_ sender: UIButton) {
        
        if Flag == false {
            let img = UIImage(systemName: "checkmark.square.fill")
            checkBoxLbl.setImage(img!, for: .normal)
            Flag = true
        } else if Flag == true {
            let img = UIImage(systemName: "square")
            checkBoxLbl.setImage(img!, for: .normal)
            Flag = false
        }
        
    }
    
    func privacyAttributedString(){
        let PrivAttributedStringA = NSMutableAttributedString.init(string: getLocalizedString(key: "accept_to_vesgo"))
        let PrivAttributedStringB = NSMutableAttributedString.init(string: getLocalizedString(key: "terms_conditions"))
        let myRangeA = NSRange(location: 0, length: PrivAttributedStringA.length)
        let myRangeB = NSRange(location: 0, length: PrivAttributedStringB.length)
        

        let FinalAttributedString = NSMutableAttributedString()
        
        let AttributesA = [
            NSMutableAttributedString.Key.font: UIFont(name: "Helvetica Neue", size: 15), NSAttributedString.Key.foregroundColor: UIColor.white
            ]
        let AttributesB = [
            NSAttributedString.Key.link: NSURL(string: "https://bstlegal.s3.amazonaws.com/Fintech/Vesgo-Privacy-Policy.pdf")!,
            NSAttributedString.Key.foregroundColor: UIColor.white,
            NSMutableAttributedString.Key.font: UIFont(name: "Helvetica Neue", size: 15) as Any,
            NSAttributedString.Key.underlineStyle: NSUnderlineStyle.thick.rawValue
        ] as [NSAttributedString.Key : Any]
        PrivAttributedStringA.setAttributes(AttributesA as [NSAttributedString.Key : Any], range: myRangeA)
        PrivAttributedStringB.setAttributes(AttributesB, range: myRangeB)
        FinalAttributedString.append(PrivAttributedStringA)
        FinalAttributedString.append(PrivAttributedStringB)
        
        //privacyTv.linkTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        privacyTv.attributedText = FinalAttributedString
    }
    
    @objc func openPrivacyPolicyLink(){
        FintechUtils.sharedInstance.openUrl(url: "https://bstlegal.s3.amazonaws.com/Fintech/Vesgo-Privacy-Policy.pdf")
    }
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool {
        // Here write your code of navigation
        return false
    }
    
    
    @IBAction func alredayaccount_pressed(_ sender: Any) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "LogInVC") as!LogInVC
        vc.modalPresentationStyle = .fullScreen
        
        // vc.modalTransitionStyle = .partialCurl
        UIApplication.shared.keyWindow?.rootViewController = vc
        
    }
    
    // MARK: - Class Functions
    //MARK: - textfield alert box
    func validateSignUpForm() -> Bool {
        if firstname_tf.text?.isEmpty ?? false {
            self.showAlert(ConstantStrings.FIRST_NAME_Emty)
            return false
        }
        else if lastname_tf.text?.isEmpty ?? false {
            self.showAlert(ConstantStrings.LAST_NAME_Emty)
            return false
        } else if username_tf.text?.isEmpty ?? false {
            self.showAlert(ConstantStrings.username_empty)
            return false
        }  else if email_tf.text?.isEmpty ?? false {
            self.showAlert(ConstantStrings.EMAIL_WAS_Emty)
            return false
        } else if password_tf.text?.isEmpty ?? false {
            showAlert(ConstantStrings.password_empty)
            return false
        } else if conformpassword_tf.text?.isEmpty ?? false {
            showAlert(ConstantStrings.CONFIRM_PASSWORD_Emty)
            return false
        } else if password_tf.text! != conformpassword_tf.text! {
            showAlert(ConstantStrings.Password_mismatch)
            return false
        } else if !self.isPasswordValid(password_tf.text!) {
            self.showAlert(ConstantStrings.PASSWORD_SHOULD_BE_8_CHARACTERS_ONE_ALPHABET)
            return false
        }
        else if !self.isValidEmail(email_tf.text!){
            showAlert(ConstantStrings.invalid_email)
            return false
        }
        else if !Flag {
            showAlert(getLocalizedString(key: "accept_to_terms_condtions"))
            return false
        }
        else
        {
            return true
        }
    }
    func validateSignUpAgreement() -> Bool {
        return false
    }
    
    //MARK: -API calls - signup
    func callSignUP() {
        self.view.activityStartAnimating()
        let url = ApiUrl.SIGNUP_USER_URL
        let passwordSHA = password_tf.text!.data(using: String.Encoding.utf8)?.sha1()
        let firstName = firstname_tf.text!.capitalized as String
        let lastName = lastname_tf.text!.capitalized as String
        let data:Dictionary<String,String> = ["first_name":  firstName,
                                              "last_name": lastName,
                                              "username": username_tf.text! as String ,
                                              "email": email_tf.text! as String ,
                                              "password": passwordSHA! as String,
                                              "language": "EN" as String,
                                              "joined_timestamp": getTimestamp() as String ]
        
        let params:Dictionary<String,AnyObject> = ["data" : data as AnyObject]
        
        APIManager.sharedInstance.postRequest(serviceName: url, sendData: params as [String : AnyObject], success: { (response) in
            print(response)
            
            if APIManager.sharedInstance.isSuccess {
                //signup sucessfull -- navigate to login
                DispatchQueue.main.async {
                    self.view.makeToast(ConstantStrings.SIGNUP_SUCCESS_MSG)
                }
                DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2) ) {
                    
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "LogInVC") as! LogInVC
                    vc.modalPresentationStyle = .fullScreen
                    vc.modalTransitionStyle = .partialCurl
                    UIApplication.shared.keyWindow?.rootViewController = vc
                }
                
            } else {
                if response["message"] != nil {
                    DispatchQueue.main.async {
                        self.showAlert(response["message"]! as! String)
                    }
                } else {
                    DispatchQueue.main.async {
                        self.showAlert(ConstantStrings.SIGN_UP_FAILED_ALERT_TEXT)
                    }
                }
            }
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
        }, failure: { (data) in
            print(data)
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
            if APIManager.sharedInstance.error400 {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
            }
            if(APIManager.sharedInstance.error401)
            {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
            }else{
                
            }
        })
        
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    //MARK:- keyboard func
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        firstname_tf.resignFirstResponder()
        lastname_tf.resignFirstResponder()
        username_tf.resignFirstResponder()
        email_tf.resignFirstResponder()
        password_tf.resignFirstResponder()
        conformpassword_tf.resignFirstResponder()
        
    }
    
}
