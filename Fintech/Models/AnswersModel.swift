//
//  AnswersModel.swift
//  Fintech
//
//  Created by MacBook-Mubashar on 23/07/2019.
//  Copyright © 2019 Broadstone Technologies. All rights reserved.
//

import Foundation

class AnswersModel: NSObject {
    
    fileprivate var parentQuestionID : Int
    fileprivate var answerID : Int
    
    fileprivate var answerHeadingText : String
    fileprivate var answerSubHeadingText : String
    fileprivate var answerBodyText : String
    fileprivate var answerImageUrlString : String
    fileprivate var answerDisclaimerText : String
    fileprivate var isAnswerSelected : Bool
    
    fileprivate var attributedAnswerHeadingStartIndex : Int
    fileprivate var attributedAnswerHeadingEndIndex : Int
    fileprivate var attributedAnswerHeadingLink : String
    fileprivate var attributedAnswerHeadingLinkType : String
    
    fileprivate var attributedAnswerBodyStartIndex : Int
    fileprivate var attributedAnswerBodyEndIndex : Int
    fileprivate var attributedAnswerBodyLink : String
    fileprivate var attributedAnswerBodyLinkType : String
    
    fileprivate var attributedAnswerSubHeadingStartIndex : Int
    fileprivate var attributedAnswerSubHeadingEndIndex : Int
    fileprivate var attributedAnswerSubHeadingLink : String
    fileprivate var attributedAnswerSubHeadingLinkType : String
    
    override init() {
        parentQuestionID = -1
        answerID = -1
        answerHeadingText = ""
        answerSubHeadingText = ""
        answerBodyText = ""
        answerImageUrlString = ""
        answerDisclaimerText = ""
        isAnswerSelected = false
        
        attributedAnswerHeadingStartIndex = -1
        attributedAnswerHeadingEndIndex = -1
        attributedAnswerHeadingLink = ""
        attributedAnswerHeadingLinkType = ""
        
        attributedAnswerBodyStartIndex = -1
        attributedAnswerBodyEndIndex = -1
        attributedAnswerBodyLink = ""
        attributedAnswerBodyLinkType = ""
        
        attributedAnswerSubHeadingStartIndex = -1
        attributedAnswerSubHeadingEndIndex = -1
        attributedAnswerSubHeadingLink = ""
        attributedAnswerSubHeadingLinkType = ""
    }
    
    
    //
    func getAttributedAnswerSubHeadingStartIndex() -> Int {
        return attributedAnswerSubHeadingStartIndex
    }
    func setAttributedAnswerSubHeadingStartIndex(index : Int) {
        self.attributedAnswerSubHeadingStartIndex = index
    }

    //
    func getAttributedAnswerSubHeadingEndIndex() -> Int {
        return attributedAnswerSubHeadingEndIndex
    }
    func setAttributedAnswerSubHeadingEndIndex(index : Int) {
        self.attributedAnswerSubHeadingEndIndex = index
    }
    //
    func getAttributedAnswerSubHeadingLink() -> String {
        return attributedAnswerSubHeadingLink
    }
    func setAttributedAnswerSubHeadingLink(text : String) {
        self.attributedAnswerSubHeadingLink = text
    }
    //
    func getAttributedAnswerSubHeadingLinkType() -> String {
        return attributedAnswerSubHeadingLinkType
    }
    func setAttributedAnswerSubHeadingLinkType(text : String) {
        self.attributedAnswerSubHeadingLinkType = text
    }
    //
    func getAttributedAnswerBodyStartIndex() -> Int {
        return attributedAnswerBodyStartIndex
    }
    func setAttributedAnswerBodyStartIndex(index : Int) {
        self.attributedAnswerBodyStartIndex = index
    }

    //
    func getAttributedAnswerBodyEndIndex() -> Int {
        return attributedAnswerBodyEndIndex
    }
    func setAttributedAnswerBodyEndIndex(index : Int) {
        self.attributedAnswerBodyEndIndex = index
    }
    //
    func getAttributedAnswerBodyLink() -> String {
        return attributedAnswerBodyLink
    }
    func setAttributedAnswerBodyLink(text : String) {
        self.attributedAnswerBodyLink = text
    }
    //
    func getAttributedAnswerBodyLinkType() -> String {
        return attributedAnswerBodyLinkType
    }
    func setAttributedAnswerBodyLinkType(text : String) {
        self.attributedAnswerBodyLinkType = text
    }
    //
    func getAttributedAnswerHeadingStartIndex() -> Int {
        return attributedAnswerHeadingStartIndex
    }
    func setAttributedAnswerHeadingStartIndex(index : Int) {
        self.attributedAnswerHeadingStartIndex = index
    }
    
    //
    func getAttributedAnswerHeadingEndIndex() -> Int {
        return attributedAnswerHeadingEndIndex
    }
    func setAttributedAnswerHeadingEndIndex(index : Int) {
        self.attributedAnswerHeadingEndIndex = index
    }
    //
    func getAttributedAnswerHeadingLink() -> String {
        return attributedAnswerHeadingLink
    }
    func setAttributedAnswerHeadingLink(text : String) {
        self.attributedAnswerHeadingLink = text
    }
    //
    func getAttributedAnswerHeadingLinkType() -> String {
        return attributedAnswerHeadingLinkType
    }
    func setAttributedAnswerHeadingLinkType(text : String) {
        self.attributedAnswerHeadingLinkType = text
    }
    //
    func getParentQuestionID() -> Int {
        return parentQuestionID
    }
    func setParentQuestionID(id : Int) {
        self.parentQuestionID = id
    }
    //
    func getAnswerHeadingText() -> String {
        return answerHeadingText
    }
    func setAnswerHeadingText(text : String) {
        self.answerHeadingText = text
    }
    //
    func getAnswerSubHeadingText() -> String {
        return answerSubHeadingText
    }
    func setAnswerSubHeadingText(text : String) {
        self.answerSubHeadingText = text
    }
    //
    func getAnswerBodyText() -> String {
        return answerBodyText
    }
    func setAnswerBodyText(text : String) {
        self.answerBodyText = text
    }
    //
    func getAnswerImageUrlString() -> String {
        return answerImageUrlString
    }
    func setAnswerImageUrlString(text : String) {
        self.answerImageUrlString = text
    }
    //
    func getAnswerDisclaimerText() -> String {
        return answerDisclaimerText
    }
    func setAnswerDisclaimerText(text : String) {
        self.answerDisclaimerText = text
    }
    
    func getIsAnswerSelected() -> Bool {
        return isAnswerSelected
    }
    
    func setIsAnswerSelected(status : Bool) {
        self.isAnswerSelected = status
    }
    
    func getAnswerId () -> Int {
        return answerID
    }
    func setAnswerId (id : Int) {
        self.answerID = id
    }
}
