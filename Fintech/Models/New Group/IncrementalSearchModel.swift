// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let incrementalSearchModel = try? newJSONDecoder().decode(IncrementalSearchModel.self, from: jsonData)

import Foundation

// MARK: - IncrementalSearchModel
struct IncrementalSearchModel: Codable {
    let success: Int
    let message: [IncrementalSearchMessage]
}

// MARK: - Message
struct IncrementalSearchMessage: Codable {
    let symbolName: String
    let esgValues: [IncrementalSearchEsgValue]
    let derivedValues: [SearchDerivedValue]

    enum CodingKeys: String, CodingKey {
        case symbolName = "Symbol_name"
        case esgValues = "Esg_values"
        case derivedValues = "derived_values"
    }
}

// MARK: - DerivedValue
struct SearchDerivedValue: Codable {
    let value: String
    let score: Double
    let scoreMean, scoreCalculation: String
    let scoreImprovement: IncrementalSearchDerivedValue

    enum CodingKeys: String, CodingKey {
        case value, score
        case scoreMean = "score_mean"
        case scoreCalculation = "score_calculation"
        case scoreImprovement = "score_improvement"
    }
}

// MARK: - ScoreImprovement
struct IncrementalSearchDerivedValue: Codable {
}

// MARK: - EsgValue
struct IncrementalSearchEsgValue: Codable {
    let value: String
    let score, percentile: Double
    let grade, scoreMean, scoreCalculation: String
    let scoreImprovement: IncrementalSearchDerivedValue

    enum CodingKeys: String, CodingKey {
        case value, score, percentile, grade
        case scoreMean = "score_mean"
        case scoreCalculation = "score_calculation"
        case scoreImprovement = "score_improvement"
    }
}
