//
//  backTestModel.swift
//  Fintech
//
//  Created by Apple on 16/12/2020.
//  Copyright © 2020 Broadstone Technologies. All rights reserved.
//

// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let backtestModel = try? newJSONDecoder().decode(BacktestModel.self, from: jsonData)

import Foundation

// MARK: - BacktestModel
struct BacktestModel: Codable {
    let success: Bool?
    let message: Messagebacktest
}

// MARK: - Message
struct Messagebacktest: Codable {
    let backtest: Backtest?
    let performance: [Performance]?
}

// MARK: - Backtest
struct Backtest: Codable {
    let amout, startDate, endDate: String?
    
    enum CodingKeys: String, CodingKey {
        case amout
        case startDate = "start_date"
        case endDate = "end_date"
    }
}

// MARK: - Performance
struct Performance: Codable {
    let portfolio, performanceReturn: String?
    
    enum CodingKeys: String, CodingKey {
        case portfolio = "portfolio"
        case performanceReturn = "return"
    }
}
