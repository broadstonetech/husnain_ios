//
//  DonationsBottomSheet.swift
//  Fintech
//
//  Created by broadstone on 01/01/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import Foundation
import UIKit

class DonationsBottomSheet: Parent {
    @IBOutlet weak var charityName:UILabel!
    var charityNameLbl:String! = nil
    
    override func viewDidLoad() {
        charityName.text = charityNameLbl
    }
    
    
}
