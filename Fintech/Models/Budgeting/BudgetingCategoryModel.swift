
// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let budgetingCategoriesModel = try? newJSONDecoder().decode(BudgetingCategoriesModel.self, from: jsonData)

import Foundation

// MARK: - BudgetingCategoriesModel
struct BudgetingCategoriesModel: Codable {
    let success: Int?
    let message: [budgetingCategoryMessage]?
}

// MARK: - Message
struct budgetingCategoryMessage: Codable {
    let categoryName: String?
    let subCategory = [String]()
    
    enum CodingKeys: String, CodingKey {
        case categoryName = "category_name"
        case subCategory = "sub_category"
    }
}
