//
//  FinancialHelpPlanInfoCell.swift
//  Fintech
//
//  Created by broadstone on 16/02/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import UIKit

class FinancialHelpPlanInfoCell: UITableViewCell {
    @IBOutlet weak var numberOfDaysText: UILabel!
    @IBOutlet weak var amountPlannedText: UILabel!
    @IBOutlet weak var amountSpentText: UILabel!
    @IBOutlet weak var numberOfDaysLbl: UILabel!
    @IBOutlet weak var amountSpentLabel: UILabel!
    @IBOutlet weak var peerSpentLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
