//
//  AccountSettings.swift
//  Fintech
//
//  Created by MacBook-Mubashar on 05/09/2019.
//  Copyright © 2019 Broadstone Technologies. All rights reserved.
//

import Foundation


class AccountSettings: NSObject {
    
    static let sharedInstance = AccountSettings()
    
    var FirstName : String = ""
    var LastName : String = ""
    var Email : String = ""
    var Notifications : String = ""
    var ThemeSelected : String = ""
    var LanguageSelected : String = ""
    var JoinedDate : Int = -1
    var RiskResultSummary : String = ""
    var QuestionnaireStatus : String = ""
    var QuestionnaireExpiry : String = ""
    var notifactiontype : [NotificationType] = []
    
    
} 
