//
//  ViewAssetVC.swift
//  Fintech
//
//  Created by broadstone on 08/08/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import UIKit



class ViewAssetVC: Parent {

    @IBOutlet weak var tableView: UITableView!
    
    var tickersInCart = [PortfolioTickersData]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
    }
    
    override func endAppearanceTransition() {
        if isBeingDismissed {
            //delegate?.onCartUpdate(tickersInCart: tickersInCart, proceedToAlloc: false)
        }
    }
    
    
    @IBAction func continueBtnPressed(_ sender: UIButton){
        self.dismiss(animated: true, completion: { [self] in
            //delegate?.onCartUpdate(tickersInCart: tickersInCart, proceedToAlloc: true)
        })
        
    }
    
}
extension ViewAssetVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return tickersInCart.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 5
        }
        return 20
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int)
    {
        view.tintColor = #colorLiteral(red: 0.9411764706, green: 0.9411764706, blue: 0.9411764706, alpha: 1)
        view.backgroundColor = #colorLiteral(red: 0.9411764706, green: 0.9411764706, blue: 0.9411764706, alpha: 1)
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SymbolCell") as! SymbolCell
        
        cell.symbolLabel.text = tickersInCart[indexPath.section].symbol
        cell.companyLabel.text = tickersInCart[indexPath.section].CompanyName
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let delete = UIContextualAction(style: .destructive, title:  "Delete", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in

            self.removeFromCart(rowIndex: indexPath.section)
            success(true)
            
            
        })
        delete.backgroundColor = #colorLiteral(red: 1, green: 0.231372549, blue: 0.1882352941, alpha: 1)
        if #available(iOS 13.0, *) {
            delete.image = UIImage(systemName: "xmark.bin")
        } else {
            // Fallback on earlier versions
        }

        return UISwipeActionsConfiguration(actions: [delete])
    }
    
    private func removeFromCart(rowIndex: Int) {
        self.tickersInCart.remove(at: rowIndex)
        self.tableView.reloadData()
        if tickersInCart.count > 0 {
            
        }
        else {
            self.dismiss(animated: true, completion: nil)
            //self.delegate?.cartDeleted()
        }
        
    }
}
