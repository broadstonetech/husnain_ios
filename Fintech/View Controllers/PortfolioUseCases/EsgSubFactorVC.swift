//
//  EsgSubFactorVC.swift
//  Fintech
//
//  Created by broadstone on 30/08/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import UIKit

class EsgSubFactorVC: Parent, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var tickerLabel: UILabel!
    

    var tagsLabelColor: UIColor!
    var tagsBackgroundColor: UIColor!
    var envTagsData = [SwiftTagData]()
    var socTagsData = [SwiftTagData]()
    var govTagsData = [SwiftTagData]()
    var companyName = ""
    
    var isFromValueMappedVC = false
    var valuesTagsData = [SwiftTagData]()
    var valueType: TickerValueType = .Env
    private var valueBgColor: UIColor!
    private var impactTags = [SwiftTagData]()
    private var noImpactTags = [SwiftTagData]()
    
    var tagsData: [SwiftTagData]!
    
    var headers = [getLocalizedString(key: "env"),
                   getLocalizedString(key: "soc"),
                   getLocalizedString(key: "gov") ]
    
    var headerColors = [ UIColor(named: "EnvColor"),
                         UIColor(named: "SocColor"),
                         UIColor(named: "GovColor")]
    
    var envImpactTags = [SwiftTagData]()
    var socImpactTags = [SwiftTagData]()
    var govImpactTags = [SwiftTagData]()
    
    var envNoImpactTags = [SwiftTagData]()
    var socNoImpactTags = [SwiftTagData]()
    var govNoImpactTags = [SwiftTagData]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tickerLabel.text = companyName
        
        segmentedControl.setTitle(getLocalizedString(key: "some_impact"), forSegmentAt: 0)
        segmentedControl.setTitle(getLocalizedString(key: "no_impact"), forSegmentAt: 1)
        
        collectionView.contentInset = UIEdgeInsets(top: 0, left: -10, bottom: 0, right: 0)
        collectionView.delegate = self
        collectionView.dataSource = self
        
        let layout = TagFlowLayout()
        layout.estimatedItemSize = CGSize(width: 140, height: 40)
        collectionView.collectionViewLayout = layout
        
        impactTags.removeAll()
        noImpactTags.removeAll()
        envImpactTags.removeAll()
        envNoImpactTags.removeAll()
        socImpactTags.removeAll()
        socNoImpactTags.removeAll()
        govImpactTags.removeAll()
        govNoImpactTags.removeAll()
        
        if isFromValueMappedVC {
            for item in valuesTagsData {
                if item.value == 1.0 {
                    impactTags.append(item)
                }else {
                    noImpactTags.append(item)
                }
            }
        }else {
            for item in envTagsData {
                if item.value == 1.0 {
                    envImpactTags.append(item)
                }else {
                    envNoImpactTags.append(item)
                }
            }
            
            for item in socTagsData {
                if item.value == 1.0 {
                    socImpactTags.append(item)
                }else {
                    socNoImpactTags.append(item)
                }
            }
            
            for item in govTagsData {
                if item.value == 1.0 {
                    govImpactTags.append(item)
                }else {
                    govNoImpactTags.append(item)
                }
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        setNavBarTitile(title: "Back", topItem: getLocalizedString(key: "esg_subfactors"))
    }
    
    @IBAction func segmentControlValueChanged(_ sender: UISegmentedControl) {
        collectionView.reloadData()
    }
    
    private func getMappedSelectedValue() -> String {
        if valueType == .Env {
            valueBgColor = headerColors[0]
            return getLocalizedString(key: "env")
        }else if valueType == .Soc {
            valueBgColor = headerColors[1]
            return getLocalizedString(key: "soc")
        }else {
            valueBgColor = headerColors[2]
            return getLocalizedString(key: "gov")
        }
    }

}
extension EsgSubFactorVC {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if isFromValueMappedVC {
            return 1
        }else {
            return 3
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if isFromValueMappedVC {
            if segmentedControl.selectedSegmentIndex == 0 {
                if impactTags.count == 0 {
                    return 1
                }else {
                    return impactTags.count
                }
            }else {
                if noImpactTags.count == 0 {
                    return 1
                }else {
                    return noImpactTags.count
                }
            }
        }else {
            if section == 0 {
                if segmentedControl.selectedSegmentIndex == 0 {
                    if envImpactTags.count == 0 {
                        return 1
                    }else {
                        return envImpactTags.count
                    }
                }else {
                    if envNoImpactTags.count == 0 {
                        return 1
                    }else {
                        return envNoImpactTags.count
                    }
                }
            }else if section == 1 {
                if segmentedControl.selectedSegmentIndex == 0 {
                    if socImpactTags.count == 0 {
                        return 1
                    }else {
                        return socImpactTags.count
                    }
                }else {
                    if socNoImpactTags.count == 0 {
                        return 1
                    }else {
                        return socNoImpactTags.count
                    }
                }
            }else if section == 2 {
                if segmentedControl.selectedSegmentIndex == 0 {
                    if govImpactTags.count == 0 {
                        return 1
                    }else {
                        return govImpactTags.count
                    }
                }else {
                    if govNoImpactTags.count == 0 {
                        return 1
                    }else {
                        return govNoImpactTags.count
                    }
                }
            }
        }
        
        
        return 0
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        
        if indexPath.section == 0 {
            if isFromValueMappedVC {
                if segmentedControl.selectedSegmentIndex == 0 {
                    tagsData =  impactTags
                }else {
                    tagsData = noImpactTags
                }
            }else {
                if segmentedControl.selectedSegmentIndex == 0 {
                    tagsData =  envImpactTags
                }else {
                    tagsData = envNoImpactTags
                }
            }
        }else if indexPath.section == 1 {
            if segmentedControl.selectedSegmentIndex == 0 {
                tagsData = socImpactTags
            }else {
                tagsData = socNoImpactTags
            }
        }else {
            if segmentedControl.selectedSegmentIndex == 0 {
                tagsData =  govImpactTags
            }else {
                tagsData = govNoImpactTags
            }
        }
        
        if tagsData.count == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NoDataCell", for: indexPath)
            return cell
        }else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SwiftTagCell", for: indexPath) as! SwiftTagCell
            
            print("Cell")
            cell.tagLabel.text = tagsData[indexPath.row].label
            cell.tagImage.image = tagsData[indexPath.row].tagImage
            cell.tagLabel.preferredMaxLayoutWidth = collectionView.frame.width - 32
            //cell.tagLabel.textColor = tagsLabelColor
            //cell.parentView.backgroundColor = tagsBackgroundColor
            
            return cell
        }
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        print("kind = \(kind)")
        if (kind == UICollectionView.elementKindSectionHeader) {
            collectionView.backgroundColor = .white
            if let sectionHeader = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "TagCollectionReusableView", for: indexPath) as? TagCollectionReusableView {
                if isFromValueMappedVC {
                    sectionHeader.tagHeaderLabel.text = getMappedSelectedValue()
                    sectionHeader.parentView.backgroundColor = valueBgColor
                    sectionHeader.tagHeaderLabel.textColor = .white
                }else {
                    sectionHeader.parentView.backgroundColor = headerColors[indexPath.section]
                    sectionHeader.tagHeaderLabel.text = headers[indexPath.section]
                    sectionHeader.tagHeaderLabel.textColor = .white
                }
                
                    return sectionHeader
            }else {
                collectionView.backgroundColor = .clear
            }
        }
        
        return UICollectionReusableView()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: 260, height: 60)
    }
}
