//
//  PaymentParentClass.swift
//  Fintech
//
//  Created by broadstone on 08/01/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import Foundation
import UIKit
import Stripe

class StipePaymentIntegration: PaymentsBaseClass, STPPaymentCardTextFieldDelegate {
    
    var isPayButtonClick = false
    
    lazy var cardTextField: STPPaymentCardTextField = {
        let cardTextField = STPPaymentCardTextField()
        return cardTextField
    }()
    
    lazy var payButton: UIButton = {
        let button = UIButton(type: .custom)
        button.layer.cornerRadius = 5
        button.backgroundColor = .systemBlue
        button.titleLabel?.font = UIFont.systemFont(ofSize: 22)
        button.setTitle("Pay", for: .normal)
        button.addTarget(self, action: #selector(pay), for: .touchUpInside)
        return button
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        let stackView = UIStackView(arrangedSubviews: [cardTextField, payButton])
        stackView.axis = .vertical
        stackView.spacing = 20
        stackView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(stackView)
        cardTextField.delegate = self
        NSLayoutConstraint.activate([
            stackView.leftAnchor.constraint(equalToSystemSpacingAfter: view.leftAnchor, multiplier: 2),
            view.rightAnchor.constraint(equalToSystemSpacingAfter: stackView.rightAnchor, multiplier: 2),
            stackView.topAnchor.constraint(equalToSystemSpacingBelow: view.topAnchor, multiplier: 2),
        ])
    }
    
    //Mark: Open Stripe View
    
    func openStripePaymentView(){
        //        let config = STPPaymentConfiguration.shared
        //        config.requiredBillingAddressFields = .full
        //        let viewController = STPAddCardViewController(configuration: config, theme: STPTheme.defaultTheme)
        //        viewController.delegate = self
        //        let navigationController = UINavigationController(rootViewController: viewController)
        //        present(navigationController, animated: true, completion: nil)
        
    }
    
    @objc func pay() {
        if isPayButtonClick {
            return
        }
        isPayButtonClick = true
        // Create an STPCardParams instance
        let cardParams = STPCardParams()
        cardParams.number = cardTextField.cardNumber
        cardParams.expMonth = UInt(cardTextField.expirationMonth)
        cardParams.expYear = UInt(cardTextField.expirationYear)
        cardParams.cvc = cardTextField.cvc
        
        
        // Pass it to STPAPIClient to create a Token
        STPAPIClient.shared.createToken(withCard: cardParams) { token, error in
            guard let token = token else {
                return
            }
            // Send the token identifier to your server
            let tokenID = token.tokenId
            print(tokenID)
//            let dataDict: [String: String] = ["token": tokenID]
            //self.navigationController?.popViewController(animated: true)
            self.dismiss(animated: true, completion: nil)
            let data:[String: String] = ["token": tokenID]
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: PaymentObservers.paymentCompleteNotificationKey), object: nil, userInfo: data)
            //self.callPostTokenAPI(paymentToken: tokenID, amount: PaymentAPIUtils.amount)
            
        }
    }
    
    func paymentCardTextFieldDidBeginEditing(_ textField: STPPaymentCardTextField) {
        isPayButtonClick = false
    }
}
//extension StipePaymentIntegration: STPAddCardViewControllerDelegate {
//    func addCardViewControllerDidCancel(_ addCardViewController: STPAddCardViewController) {
//        print("Cancel the payment flow")
//    }
//
//    func addCardViewController(_ addCardViewController: STPAddCardViewController, didCreatePaymentMethod paymentMethod: STPPaymentMethod, completion: @escaping STPErrorBlock) {
//        print(paymentMethod.allResponseFields["card"] ?? "No token found")
//
//    }
//
//
//}
extension StipePaymentIntegration: STPAuthenticationContext {
    func authenticationPresentingViewController() -> UIViewController {
        return self
    }
}
