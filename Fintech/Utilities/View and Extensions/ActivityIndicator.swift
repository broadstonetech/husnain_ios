//
//  ActivityIndicator.swift
//  Fintech
//
//  Created by MacBook-Mubashar on 23/01/2020.
//  Copyright © 2020 Broadstone Technologies. All rights reserved.
//

import Foundation
import UIKit


extension UIView{

func activityStartAnimating() {
    print("activityStartAnimating")
    let backgroundView = UIView()
    backgroundView.frame = CGRect.init(x: 0, y: 0, width: self.bounds.width, height: self.bounds.height)
    backgroundView.backgroundColor = UIColor.clear
    backgroundView.tag = 475647

    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    activityIndicator = UIActivityIndicatorView(frame: CGRect.init(x: 0, y: 0, width: 80, height: 80))
    activityIndicator.center = self.center
    activityIndicator.hidesWhenStopped = true
    if #available(iOS 13.0, *) {
        activityIndicator.style = UIActivityIndicatorView.Style.large
    } else {
        // Fallback on earlier versions
        activityIndicator.style = UIActivityIndicatorView.Style.gray
    }
//    activityIndicator.color = #colorLiteral(red: 0.408691138, green: 0.6668302417, blue: 0.8831040263, alpha: 1)
    activityIndicator.color = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    activityIndicator.backgroundColor = #colorLiteral(red: 0.408691138, green: 0.6668302417, blue: 0.8831040263, alpha: 0.4951011203)
    activityIndicator.layer.cornerRadius = 10
    activityIndicator.startAnimating()
    self.isUserInteractionEnabled = false

    backgroundView.addSubview(activityIndicator)

    self.addSubview(backgroundView)
}

func activityStopAnimating() {
    if let background = viewWithTag(475647){
        background.removeFromSuperview()
    }
    self.isUserInteractionEnabled = true
}
}

