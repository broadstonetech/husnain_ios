//
//  InvestmentQuestionnaireApiModel.swift
//  Fintech
//
//  Created by broadstone on 27/05/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import Foundation

//model for questionnaire status

struct QuestionnaireStatusModel {
    var success: Bool!
    var message: String!
    var data: QuestionnaireStatusDataModel!
}

struct QuestionnaireStatusDataModel {
    var demographics: Bool!
    var investment: Bool!
    var risk: Bool!
    var values: Bool!
}



//Model for Getter data from API
struct QuestionnaireDataFromAPI {
    var success: Bool!
    var message: String!
    var selectedAnswers: [QuestionnaireSelectedDataFromAPI]!
    var template = ""     //This key is only valid for get values questionnaire api
}

struct QuestionnaireSelectedDataFromAPI {
    var questionId: String!
    var answerIds: [String]
    
}
