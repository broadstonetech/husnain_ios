//
//  InsightsHeadingCell.swift
//  Task1
//
//  Created by Billal on 29/01/2021.
//

import UIKit

class InsightsHeadingCell: UITableViewCell {

    @IBOutlet weak var headingLbl: UILabel!
    @IBOutlet weak var bodyLbl: UILabel!
    @IBOutlet weak var parentView: UIView!
    @IBOutlet weak var dropDownImg: UIImageView!
    @IBOutlet weak var headerImage: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
