//
//  AccountVC.swift
//  Fintech
//
//  Created by MacBook-Mubashar on 30/08/2019.
//  Copyright © 2019 Broadstone Technologies. All rights reserved.
////#################################################################
//  In This view show all button in account
//    User profile
//    Investment strategy
//    Language selection
//    Push notification
//    Meet the team
//    legal Documents
//    share App
//    Signin button
//################################################################

import UIKit
import UserNotifications
import Floaty
var lng = ""
var arrynoti = [NotificationType]()
class ButtonWithImage: UIButton {
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if imageView != nil {
            print(bounds.width)
            imageEdgeInsets = UIEdgeInsets(top: 5, left: (bounds.width - 35), bottom: 5, right: 5)
        }
    }
}
class AccountVC: Parent {
    //MARK: -IB Outlets
    @IBOutlet weak var eng_tick_img: UIImageView!
    @IBOutlet weak var personal_btn: UIButton!
    @IBOutlet weak var version_lbl: UILabel!
    @IBOutlet weak var french_tick_img: UIImageView!
    @IBOutlet weak var helpandsport_btn: UIButton!
    @IBOutlet weak var appsetting_btn: UIButton!
    @IBOutlet weak var spanish_tick_img: UIImageView!
    @IBOutlet weak var chinese_tick_img : UIImageView!
    @IBOutlet weak var arabic_tick_img : UIImageView!
    @IBOutlet weak var change_Password_btn: UIButton!
    @IBOutlet weak var username_lbl: UILabel!
    @IBOutlet weak var UserEmail_lbl: UILabel!
    @IBOutlet weak var floty_btn: Floaty!
    @IBOutlet var signoutBtn_view: UIView!
    @IBOutlet var signinBtn_view: UIView!
    @IBOutlet weak var presnol_view: UIView!
    @IBOutlet weak var appSetting_view: UIView!
    @IBOutlet weak var Pushnotifaction_lineview: UIView!
    
    @IBOutlet weak var pushNotifaction_view: UIView!
    
    @IBOutlet weak var line_View_Appsetting: UIView!
    @IBOutlet weak var presnol_Btn_view: UIView!
    @IBOutlet weak var help_and_support_view: UIView!
    // Mark:- Buttons outlets
    @IBOutlet weak var StrategyBuilding_btn: UIButton!
    @IBOutlet weak var Faqs_hlp_btn: UIButton!
    @IBOutlet weak var LegalDocuments_btn: UIButton!
    @IBOutlet weak var privacyPolicy_btn: UIButton!
    @IBOutlet weak var shareApp_btn: UIButton!
    @IBOutlet weak var Darkmood_Switch: UISwitch!
    @IBOutlet weak var bicomatic_btn: UIButton!
    @IBOutlet weak var Pushnotification_switch: UISwitch!
    @IBOutlet weak var signOut_btn: UIButton!
    @IBOutlet weak var biometricSwitch: UISwitch!
    @IBOutlet var signIn_btn: UIButton!
    @IBOutlet weak var tutorialsBtn: UIButton!
    
    //MArk :- Class variables
    var buttonn = UIButton()
    var buttonn1 = UIButton()
    var buttonn2 = UIButton()
    var buttonn3 = UIButton()
    var label = UILabel()
    var label1 = UILabel()
    var label2 = UILabel()
    var juiceBool = Bool()
    var FoodBool = Bool()
    var FurnitureBool = Bool()
    var localstorelanguage  = ""
    var sample_username = ""
    var date  = ""
    var Email = ""
    
    @IBOutlet weak var languageLabel: UILabel!
    @IBOutlet weak var englishBtn: UIButton!
    @IBOutlet weak var spanishBtn: UIButton!
    @IBOutlet weak var frenchBtn: UIButton!
    @IBOutlet weak var chineseBtn: UIButton!
    @IBOutlet weak var arabicBtn: UIButton!
    @IBOutlet weak var pushNotificationLabel: UILabel!
    @IBOutlet weak var liveChatSupport: UIButton!
    
    //Mark :- Acction buttons
    func language(type:String,value:String) {
     if SPManager.sharedInstance.saveUserLoginStatus() {
        self.view.activityStartAnimating()
        let url = ApiUrl.Language_Select
        let namespace = SPManager.sharedInstance.getCurrentUserNamespace()
        let data:Dictionary<String,AnyObject> = ["type":type as AnyObject,"value":value as AnyObject]
        let params:Dictionary<String,AnyObject> = ["namespace" : namespace as AnyObject, "data":data as AnyObject]
        print(data)
        APIManager.sharedInstance.postRequest(serviceName: url, sendData: params, success: { (response) in
            if APIManager.sharedInstance.isSuccess {
                let res = response["message"] as AnyObject
                DispatchQueue.main.async {
                }
            }else{
                if response["message"] != nil {
                    DispatchQueue.main.async {
                        self.showAlert(response["message"] as! String)
                    }
                } else {
                    DispatchQueue.main.async {
                        //self.showAlert(ConstantStrings.NO_RESULT_FOUND_ALERT_TEXT)
                    }
                }
            }
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
            DispatchQueue.main.async {
                self.applanuage()
            }
            
        }, failure: { (data) in
            DispatchQueue.main.async {
                self.applanuage()
            }
            print(data)
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
            if APIManager.sharedInstance.error400 {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
            }
            if(APIManager.sharedInstance.error401)
            {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
            }else{
                
            }
        })
     }else{
        self.applanuage()
        }
        
    }
    //Mark :- Language buttons acction
    @IBAction func chinese_btn(_ sender: Any) {
//        localstorelanguage = "zh-Hans"
//        Dbname = "fintchinese"
//        self.language(type:"language",value:"zh-Hans")
//        UserDefaults.standard.set(Dbname, forKey: "Dbname")
//        eng_tick_img.isHidden = true
//        spanish_tick_img.isHidden = true
//        french_tick_img.isHidden = true
//        chinese_tick_img.isHidden = false
//        arabic_tick_img.isHidden = true
//        UserDefaults.standard.set(localstorelanguage, forKey: "localstorelanguage")
//
//        UserDefaults.standard.synchronize()
        
        AppPrefsManager.sharedInstance.saveLanguageCode(languageCode: LanguageCodeType.Chinese)
        //self.localization()
        self.language(type:"language",value: AppPrefsManager.sharedInstance.getLanguageCode())
        NotificationCenter.default.post(Notification(name: .refreshAllTabs))
        
        setAnalyticsAction(ScreenName: "Account", method: "chineseSelected")
        
    }
    

    @IBAction func spanish_btn(_ sender: Any) {
//        localstorelanguage = "es"
//        Dbname = "fintspanish"
//        self.language(type:"language",value:"es")
//        UserDefaults.standard.set(Dbname, forKey: "Dbname")
//        eng_tick_img.isHidden = true
//        spanish_tick_img.isHidden = false
//        french_tick_img.isHidden = true
//        chinese_tick_img.isHidden = true
//        arabic_tick_img.isHidden = true
//        UserDefaults.standard.set(localstorelanguage, forKey: "localstorelanguage")
//
//        UserDefaults.standard.synchronize()
         AppPrefsManager.sharedInstance.saveLanguageCode(languageCode: LanguageCodeType.Spanish)
        //self.localization()
        print("LanguageCode: \(AppPrefsManager.sharedInstance.getLanguageCode())")
        self.language(type:"language",value: AppPrefsManager.sharedInstance.getLanguageCode())
        NotificationCenter.default.post(Notification(name: .refreshAllTabs))
        
        setAnalyticsAction(ScreenName: "Account", method: "spanishSelected")
    }
    
    @IBAction func arabic_btn(_ sender: Any) {
//        localstorelanguage = "ar"
//        Dbname = "FintechArabic"
//
//        eng_tick_img.isHidden = true
//        spanish_tick_img.isHidden = true
//        french_tick_img.isHidden = true
//        chinese_tick_img.isHidden = true
//        arabic_tick_img.isHidden = false
//        self.language(type:"language",value:"ar")
//        UserDefaults.standard.set(Dbname, forKey: "Dbname")
//
//        UserDefaults.standard.set(localstorelanguage, forKey: "localstorelanguage")
//
//        UserDefaults.standard.synchronize()

        AppPrefsManager.sharedInstance.saveLanguageCode(languageCode: LanguageCodeType.Arabic)
        print("LanguageCode: \(AppPrefsManager.sharedInstance.getLanguageCode())")
        self.language(type:"language",value: AppPrefsManager.sharedInstance.getLanguageCode())
        //self.localization()
        NotificationCenter.default.post(Notification(name: .refreshAllTabs))
        
        setAnalyticsAction(ScreenName: "Account", method: "arabicSelected")
    }
    
    @IBAction func english_btn(_ sender: Any) {
//        localstorelanguage = "en"
//        Dbname = "fintecheng"
//
//        eng_tick_img.isHidden = false
//        spanish_tick_img.isHidden = true
//        french_tick_img.isHidden = true
//        chinese_tick_img.isHidden = true
//        arabic_tick_img.isHidden = true
//        self.language(type:"language",value:"en")
//        UserDefaults.standard.set(Dbname, forKey: "Dbname")
//
//        UserDefaults.standard.set(localstorelanguage, forKey: "localstorelanguage")
//
//        UserDefaults.standard.synchronize()

        AppPrefsManager.sharedInstance.saveLanguageCode(languageCode: LanguageCodeType.English)
        print("LanguageCode: \(AppPrefsManager.sharedInstance.getLanguageCode())")
        self.language(type:"language",value: AppPrefsManager.sharedInstance.getLanguageCode())
        //self.localization()
        NotificationCenter.default.post(Notification(name: .refreshAllTabs))
        setAnalyticsAction(ScreenName: "Account", method: "englishSelected")
    }
    
    @IBAction func French_btn(_ sender: Any) {
//        localstorelanguage = "fr"
//        Dbname = "fintfrinch"
//
//        eng_tick_img.isHidden = true
//        spanish_tick_img.isHidden = true
//        french_tick_img.isHidden = false
//        chinese_tick_img.isHidden = true
//        arabic_tick_img.isHidden = true
//        self.language(type:"language",value:"fr")
//        UserDefaults.standard.set(Dbname, forKey: "Dbname")
//
//        UserDefaults.standard.set(localstorelanguage, forKey: "localstorelanguage")
        AppPrefsManager.sharedInstance.saveLanguageCode(languageCode: LanguageCodeType.French)
        print("LanguageCode: \(AppPrefsManager.sharedInstance.getLanguageCode())")
        self.language(type:"language",value: AppPrefsManager.sharedInstance.getLanguageCode())
        NotificationCenter.default.post(Notification(name: .refreshAllTabs))
        
        setAnalyticsAction(ScreenName: "Account", method: "frenchSelected")
    }
    @IBAction func Darkmood_switch(_ sender: Any) {
    }
    //Mark :- Pushnotification buttons acction
    @IBAction func Pushnotification_switch(_ sender: Any) {
        if SPManager.sharedInstance.saveUserLoginStatus() {
           if Pushnotification_switch.isOn{
                self.setNotifications(value: "1",avalue: "enabled")
           }else{
                self.setNotifications(value: "0",avalue: "disable")
            }
        }else{
            SPManager.sharedInstance.setPushNotificationStatus(Pushnotification_switch.isOn)
        }
    }
    func setNotifications(value: String,avalue: String) {
        self.view.activityStartAnimating()
        let url = ApiUrl.NoftificationCheck
        let namespace = SPManager.sharedInstance.getCurrentUserNamespace()
        let data:Dictionary<String,AnyObject> = [
            "notification_types":[["name":"Investment","value":value],["name":"Risk","value":value],["name":"Islamic","value":value],["name":"Marketing","value":value]] as AnyObject, "notifications":[avalue] as AnyObject]

        let params:Dictionary<String,AnyObject> = ["namespace" : namespace as AnyObject, "data":data as AnyObject]
        print(data)
        APIManager.sharedInstance.postRequest(serviceName: url, sendData: params, success: { (response) in
            print(response)
            
            if APIManager.sharedInstance.isSuccess {
                let res = response["message"] as AnyObject
                DispatchQueue.main.async {
                    
                      //self.showAlert(res as! String)
                }
            }else{
                if response["message"] != nil {
                   //  self.showAlert(response["message"] as! String)
                } else {
                        //self.showAlert(ConstantStrings.NO_RESULT_FOUND_ALERT_TEXT)
                }
            }
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
        }, failure: { (data) in
            print(data)
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
            if APIManager.sharedInstance.error400 {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
            }
            if(APIManager.sharedInstance.error401)
            {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
            }else{
                
            }
        })
        
    }
    //Mark :-  buttons acction
    
    @IBAction func biometricBtnPressed(_ sender: UISwitch) {
        if sender.isOn {
            SPManager.sharedInstance.setShowConsetDialogForBiometric(true)
        }else {
            SPManager.sharedInstance.setShowConsetDialogForBiometric(true)
        }
    }
    
    

    @IBAction func change_password_pressd(_ sender: Any) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        
        let vc = storyBoard.instantiateViewController(withIdentifier: "ChangePasswordVC") as! ChangePasswordVC
        vc.isFromForgotpassword = false
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func PersonalBtn_Pressed(_ sender: Any) {
        if presnol_view.isHidden == true{
            presnol_view.isHidden = false
            let tablel = presnol_view
            tablel!.heightConstaint?.constant = 45
        }else{
            presnol_view.isHidden = true
            presnol_view!.heightConstaint?.constant = 0
        }
    }
    
    @IBAction func AppSettingbtn_pressed(_ sender: Any) {
        if SPManager.sharedInstance.saveUserLoginStatus() {

        if appSetting_view.isHidden == true {
            appSetting_view.isHidden = false
            let tablel = appSetting_view
            tablel!.heightConstaint?.constant = 220
        }else{
            appSetting_view.isHidden = true
            appSetting_view!.heightConstaint?.constant = 0
        }
        }else{
            if appSetting_view.isHidden == true {
                appSetting_view.isHidden = false
                let tablel = appSetting_view
                tablel!.heightConstaint?.constant = 190
            }else{
                appSetting_view.isHidden = true
                appSetting_view!.heightConstaint?.constant = 0
            }
        }
    }
    @IBAction func help_and_Support_pressed(_ sender: Any) {
        if help_and_support_view.isHidden == true{
            help_and_support_view.isHidden = false
            let tablel = help_and_support_view
            tablel!.heightConstaint?.constant = 250
            
            setAnalyticsAction(ScreenName: "Account", method: "helpAndSupportPressed")
            
        }else{
            help_and_support_view.isHidden = true
            help_and_support_view!.heightConstaint?.constant = 0
        }
    }
    @IBAction func user_leaderboard_btn(_ sender: Any) {
    }
    
    @IBAction func vesgoTutorialBtnPressed(_ sender: UIButton) {
        let storyboard = UIStoryboard.init(name: "VesgoTutorial", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "VesgoTutorialVC") as! VesgoTutorialVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        floty_btn.isHidden = true
        sample_username = UserDefaults.standard.string(forKey: "sampleportfolio_username") ?? ""
          //AppPrefsManager.sharedInstance.saveLanguageCode(languageCode: LanguageCodeType.English)
        if sample_username == "" {
              }else {
              }
              if SPManager.sharedInstance.saveUserLoginStatus() {
               username_lbl.text = "\(SPManager.sharedInstance.getCurrentUserFullname())"
                pushNotifaction_view.isHidden = false
              }else {
              }
        navigationController?.isNavigationBarHidden = true       
        presnol_view!.heightConstaint?.constant = 0
        appSetting_view!.heightConstaint?.constant = 0
        help_and_support_view!.heightConstaint?.constant = 0
        appSetting_view.isHidden = true
        help_and_support_view.isHidden = true
        presnol_view.isHidden = true
      if UserDefaults.standard.object(forKey: "\(SPManager.sharedInstance.getCurrentUserEmail())date") != nil {
            date = UserDefaults.standard.string(forKey: "\(SPManager.sharedInstance.getCurrentUserEmail())date")!
            if date != nil {
            } else {
                date =  ""
            }
        } else {
            date = ""
        }
        if SPManager.sharedInstance.saveUserLoginStatus() {
            print("user is logged in")
            line_View_Appsetting.isHidden = false
            UserEmail_lbl.text = SPManager.sharedInstance.getCurrentUserEmail()
        } else {
            print("user not logged in")

            line_View_Appsetting.isHidden = true
        }
        setUpInfoLabels()
        setUpButtonsActions()
        // Do any additional setup after loading the view.
        juiceBool = false
        FurnitureBool = false
        FoodBool = false
        
        
        self.getAccountDataa()
        
    }
    func floatybutton() {
        if SPManager.sharedInstance.saveUserLoginStatus() {
            floty_btn.isHidden = true
        }else{
            floty_btn.buttonColor = #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1)
            floty_btn.itemButtonColor = #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1)
            floty_btn.buttonImage = UIImage(named: "floatyicon")
            floty_btn.addItem("Sample Portfolios",icon: UIImage(named: "services-portfolio1")!, handler: { item in
                if let tabbedbar = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "nav_bar_sampleportfolio") as? UINavigationController {
                    UIApplication.shared.keyWindow?.rootViewController = tabbedbar
                }
            })
            floty_btn.addItem("Account Setup",icon: UIImage(named: "baseline_person_add_black")!, handler: { item in
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "Main_login_page") as UIViewController
                UIApplication.shared.keyWindow?.rootViewController = vc
            })
            DispatchQueue.main.async {
                self.view.addSubview(self.floty_btn)

            }
        }
    }
    //Mark :- Set title
    func localization(){
        Faqs_hlp_btn.setTitle(getLocalizedString(key: "FAQS", comment: ""), for: .normal)
        signOut_btn.setTitle(getLocalizedString(key: "SignOut", comment: ""), for: .normal)
        signIn_btn.setTitle(getLocalizedString(key: "Join now", comment: ""), for: .normal)
        shareApp_btn.setTitle(getLocalizedString(key: "Share app", comment: ""), for: .normal)
        privacyPolicy_btn.setTitle(getLocalizedString(key: "privacypolicy", comment: ""), for: .normal)
        // setNavBarTitile(title: "Account", topItem: "Account")
        LegalDocuments_btn.setTitle(getLocalizedString(key: "Legal documents", comment: ""), for: .normal)
        setNavBarTitile(title:getLocalizedString(key: "Account", comment: ""), topItem: getLocalizedString(key: "Account", comment: ""))
        
         appsetting_btn.setTitle(getLocalizedString(key: "App settings", comment: ""), for: .normal)
        helpandsport_btn.setTitle(getLocalizedString(key: "Help and support", comment: ""), for: .normal)
        personal_btn.setTitle(getLocalizedString(key: "Personal", comment: ""), for: .normal)
        change_Password_btn.setTitle(getLocalizedString(key: "Change password", comment: ""), for: .normal)
        bicomatic_btn.setTitle(getLocalizedString(key: "Biometric unlock", comment: ""), for: .normal)
        
        pushNotificationLabel.text = getLocalizedString(key: "PushNotification", comment: "")
        languageLabel.text = getLocalizedString(key: "Language", comment: "")
        
        englishBtn.setTitle(getLocalizedString(key: "englsih_", comment: ""), for: .normal)
        frenchBtn.setTitle(getLocalizedString(key: "french_", comment: ""), for: .normal)
        spanishBtn.setTitle(getLocalizedString(key: "spanish_", comment: ""), for: .normal)
        chineseBtn.setTitle(getLocalizedString(key: "chinese_", comment: ""), for: .normal)
        arabicBtn.setTitle(getLocalizedString(key: "arabic_"), for: .normal)
        liveChatSupport.setTitle(getLocalizedString(key: "chat_live_support"), for: .normal)
        tutorialsBtn.setTitle(getLocalizedString(key: "_tutorials"), for: .normal)
        version_lbl.text = getAppVersionString()
        
        if SPManager.sharedInstance.saveUserLoginStatus() {
            username_lbl.text = "\(SPManager.sharedInstance.getCurrentUserFullname())"
            pushNotifaction_view.isHidden = false
            
        }else {
            //                var guest_user = (getLocalizedString(key: "privacypolicy", comment: ""),
            var titleheading = getLocalizedString(key: "Guest user", comment: "")
            username_lbl.text = titleheading
            //"Guest user"
            //pushNotifaction_view.isHidden = true
            
        }
       
        
    }
    // MARK: - Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        showTickMarkForLanguage()
       
        navigationController?.isNavigationBarHidden = true
        
        if SPManager.sharedInstance.shouldShowConsetDialogForBiometric() {
            biometricSwitch.isOn = true
        }else {
            biometricSwitch.isOn = false
        }

      //  self.navigationController?.navigationBar.isHidden = false
        setNavBarTitile(title:getLocalizedString(key: "Account", comment: ""), topItem: getLocalizedString(key: "Account", comment: ""))
        if SPManager.sharedInstance.saveUserLoginStatus() {
            print("user is logged in")
            signinBtn_view.isHidden = true
            presnol_Btn_view.isHidden = false
            pushNotifaction_view.isHidden = false
            Pushnotifaction_lineview.isHidden = false

            signinBtn_view!.heightConstaint?.constant = 0
            presnol_Btn_view!.heightConstaint?.constant = 30
            signoutBtn_view!.heightConstaint?.constant = 50

            if SPManager.sharedInstance.getUserPortfolioStatus() {
            } else {
            }
        } else {
            print("user not logged in")
            pushNotifaction_view.isHidden = true
            Pushnotifaction_lineview.isHidden = true
            //appSetting_view!.heightConstaint?.constant = 190

            signinBtn_view.isHidden = false
            signinBtn_view!.heightConstaint?.constant = 50
            presnol_Btn_view.isHidden = true
            presnol_Btn_view!.heightConstaint?.constant = 0
            signoutBtn_view!.heightConstaint?.constant = 0
            signoutBtn_view.isHidden = true
        }
    }
//    override func viewDidAppear(_ animated: Bool) {
//        self.applanuage()
//        self.localization()
//
//    }
    
    // MARK: - Class funcs
    func setUpInfoLabels() {
    }
    func setUpButtonsActions() {
        StrategyBuilding_btn.addTarget(self, action: #selector(startegybullinding), for: UIControl.Event.touchUpInside)
        Faqs_hlp_btn.addTarget(self, action: #selector(helpBtn_tapped), for: UIControl.Event.touchUpInside)
        shareApp_btn.addTarget(self, action: #selector(shareAppBtn_tapped), for: UIControl.Event.touchUpInside)
        LegalDocuments_btn.addTarget(self, action: #selector(legalDocsBtn_tapped), for: UIControl.Event.touchUpInside)
        privacyPolicy_btn.addTarget(self, action: #selector(privacypolicy), for: UIControl.Event.touchUpInside)
        signOut_btn.addTarget(self, action: #selector(signOutBtn_tapped), for: UIControl.Event.touchUpInside)
        signIn_btn.addTarget(self, action: #selector(signInBtn_tapped), for: UIControl.Event.touchUpInside)
        liveChatSupport.addTarget(self, action: #selector(liveChatSupport_tapped), for: UIControl.Event.touchUpInside)
    }
    // MARK: - IB Actions
    @objc func profileBtn_tapped() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "profile") as! Profile_VC
        navigationController?.pushViewController(vc, animated: true)
    }
    @objc func historyBtn_tapped() {
    }
    
    @objc func Language_tapped() {
    }
    
    @objc func helpBtn_tapped() {
        //self.view.makeToast("help pressed")
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "FAQsHelpVC") as! FAQsHelpVC
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func meetTeamBtn_tapped() {
        UIApplication.shared.open(URL(string: "http://www.broadstonetech.com")!, options: [:]
            , completionHandler: nil)
    }
    
    @objc func shareAppBtn_tapped() {
        shareSocialURL(url: ConstantStrings.APP_LINK)
    }
    var selected  = true
    @objc func legalDocsBtn_tapped() {
        UIApplication.shared.open(URL(string: "https://bstlegal.s3.amazonaws.com/Fintech/Vesgo-Privacy-Policy.pdf")!, options: [:]
            , completionHandler: nil)
        
    }
    @objc func privacypolicy(){
        UIApplication.shared.open(URL(string: "https://bstlegal.s3.amazonaws.com/Fintech/Vesgo-Privacy-Policy.pdf")!, options: [:]
            , completionHandler: nil)
    }
    @objc func Termsofservice(){
        UIApplication.shared.open(URL(string: "https://bstlegal.s3.amazonaws.com/Fintech/Vesgo-Privacy-Policy.pdf")!, options: [:]
            , completionHandler: nil)
    }
    
    @objc func startegybullinding() {
        if SPManager.sharedInstance.saveUserLoginStatus() {
            if getUserData(key: SPManager.sharedInstance.getCurrentUserEmail()) == nil{
                let alert = UIAlertController(title: "Investment Strategy", message: ConstantStrings.investmentStrategy_alert_mesage,         preferredStyle: UIAlertController.Style.alert)
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "homesurfay") as UIViewController
                UIApplication.shared.keyWindow?.rootViewController = vc
                alert.addAction(UIAlertAction(title: "No", style: UIAlertAction.Style.default, handler: { _ in
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "AccountVC") as! AccountVC
                    // navigationController?.pushViewController(vc, animated: true)
                    UIApplication.shared.keyWindow?.rootViewController = vc
                    //Cancel Action
                }))
                alert.addAction(UIAlertAction(title: "yes", style: UIAlertAction.Style.default, handler: { _ in
                    
                    //Cancel Action
                }))
                
                self.present(alert, animated: true, completion: nil)
                
            }else{
                let alert = UIAlertController(title: "Investment Strategy", message: "Your current investment strategy will expire on \(date). You can create and save a new one for later use. Do you want to continue?",         preferredStyle: UIAlertController.Style.alert)
                //" + DATE_HERE + ".
                
                alert.addAction(UIAlertAction(title: "View", style: UIAlertAction.Style.default, handler: { _ in
                    //Cancel Action
                    InvestmentStrategyVars.isFromAccountVC = true
                    let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CategorySummaryVC") as! CategorySummaryVC
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                }))
                alert.addAction(UIAlertAction(title: "Create New", style: UIAlertAction.Style.default, handler: { _ in
                    
                    InvestmentStrategyVars.isFromAccountVC = true
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "HomeSurveyVC") as! HomeSurveyVC
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                }))
                
                alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: { _ in
                    //Cancel Action
                }))
                
                self.present(alert, animated: true, completion: nil)
            }
            
            
        }else {
            //user not login
            self.view.makeToast(ConstantStrings.investmentStrategy_alert_unregister_user)
        }
    }
    
    @objc func signOutBtn_tapped() {
        // self.view.makeToast("sign out pressed")
        let alert = UIAlertController(title: getLocalizedString(key: "sign_out", comment: ""),
                                      message: getLocalizedString(key: "are_you_sure_you_want_to_cont", comment: ""),
                                      preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: getLocalizedString(key: "no_", comment: ""),
                                      style: UIAlertAction.Style.default, handler: { _ in
           
            //Cancel Action
        }))
        alert.addAction(UIAlertAction(title: getLocalizedString(key: "yes_", comment: ""),
                                      style: UIAlertAction.Style.default, handler: { _ in
            
                                        self.performSignOutFunctionality()
        }))
             self.present(alert, animated: true, completion: nil)
       
        
    }
    @objc func signInBtn_tapped() {
        self.view.makeToast("sign in pressed")
        SPManager.sharedInstance.clearUserAppSettings()
        SPManager.sharedInstance.clearUserData()
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "Main_login_page") as! mainpageloninsignup
           backpostion = "account"
        vc.modalTransitionStyle = .partialCurl
        self.PushVC(vcIdentifier: "Main_login_page", storyboard: "Main")
    }
    
    @objc func liveChatSupport_tapped(){
        if SPManager.sharedInstance.saveUserLoginStatus() {
            if SPManager.sharedInstance.getCurrentFirebaseId() == ""{
                self.saveToServer()
            }
            else{
                let firebaseId = SPManager.sharedInstance.getCurrentFirebaseId()
                print("firebase id is ......\(firebaseId)")
                let channel = Channel(id: firebaseId)
                let vc = ChatViewController(channel: channel)
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }else {
            Alert()
        }
    }
    
    func Alert() {
        var textField = UITextField()
        let alert = UIAlertController(title: "Support", message: "", preferredStyle: .alert)
        alert.addTextField { alertTextField in
            alertTextField.placeholder = "Please enter your email"
            textField = alertTextField
            
        }
        let doneAction = UIAlertAction(title: "Submit", style: .default) { action in
            if self.isValidEmail(textField.text!) {
                self.Email = textField.text!
                if SPManager.sharedInstance.getCurrentFirebaseId() == ""{
                    self.saveToServer()
                }
                else{
                    let firebaseId = SPManager.sharedInstance.getCurrentFirebaseId()
                    print("firebase id is ......\(firebaseId)")
                    let channel = Channel(id: firebaseId)
                    let vc = ChatViewController(channel: channel)
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                
            }else {
                self.view.makeToast("Please enter valid email")
                
            }
            
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { action in
            return
            
        }
        alert.addAction(doneAction)
        alert.addAction(cancelAction)
        present(alert, animated: true, completion: nil)
    }
    
    // MARK: - Get Account Data-API
    func getAccountDataa() {
        if SPManager.sharedInstance.saveUserLoginStatus() {

       self.view.activityStartAnimating()
        let url = ApiUrl.GET_ACCOUNT_SETTINGS
        let namespace = SPManager.sharedInstance.getCurrentUserNamespace()
        let params:Dictionary<String,AnyObject> = ["namespace" : namespace as AnyObject ]
        print(params)
        APIManager.sharedInstance.postRequest(serviceName: url, sendData: params, success: { (response) in
            //print(response)
            
            if APIManager.sharedInstance.isSuccess {
                let res = response["message"] as AnyObject
                DispatchQueue.main.async {
                    //print("response message is \(res)")
               
                    AccountSettings.sharedInstance.FirstName = res["first_name"] as! String
                    AccountSettings.sharedInstance.LastName = res["last_name"] as! String
                    AccountSettings.sharedInstance.Email = res["email"] as! String
//                    AccountSettings.sharedInstance.JoinedDate = res["joined_timestamp"] as! Int
                    AccountSettings.sharedInstance.LanguageSelected = res["language"] as! String
                    lng = AccountSettings.sharedInstance.LanguageSelected
                    //AccountSettings.sharedInstance.RiskResultSummary = res["risk_result_summary"] as! String
                    AccountSettings.sharedInstance.Notifications = res["notifications"] as! String
                    AccountSettings.sharedInstance.ThemeSelected = res["theme"] as! String
                    //AccountSettings.sharedInstance.QuestionnaireStatus = res["questionnaire_status"] as! String
                   let jsondata = res["notification_types"] as! [[String : AnyObject]]
                    var notiarray:[NotificationType] = []
                    for item in jsondata{
                        let type = item["name"] as! String
                         let value = item["value"] as? String ?? "0"
                        notiarray.append(NotificationType(name:type, value: value))
                    }
                    AccountSettings.sharedInstance.notifactiontype = notiarray
                  arrynoti = AccountSettings.sharedInstance.notifactiontype
                    
                    let langCode = (res["language"] as? String) ?? LanguageCodeType.English
                    AppPrefsManager.sharedInstance.saveLanguageCode(languageCode: langCode)
                    
                    print(arrynoti)
                    print(lng)
                    self.pushnotifaction()
                    //self.applanuage()
                }
                
            }else{
                if response["message"] != nil {
                    self.showAlert(response["message"] as! String)
                } else {
                    self.showAlert(ConstantStrings.NO_RESULT_FOUND_ALERT_TEXT)
                }
            }
            DispatchQueue.main.async {
                self.applanuage()
            }
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
        }, failure: { (data) in
            print(data)
            DispatchQueue.main.async {
                self.applanuage()
            }
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
            if APIManager.sharedInstance.error400 {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
            }
            if(APIManager.sharedInstance.error401)
            {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
            }else{
                
            }
        })
        }else{
            applanuage()
        }

    }
    
    //MARK:- SignOut API
    func performSignOutFunctionality() {
        if !SPManager.sharedInstance.saveUserLoginStatus() {
            return
        }

        self.view.activityStartAnimating()
        let url = ApiUrl.SIGN_OUT_URL
        let namespace = SPManager.sharedInstance.getCurrentUserNamespace()
        let dataParams: Dictionary<String, AnyObject> = ["device_type": "ios" as AnyObject, "device_token": FintechUtils.sharedInstance.DEVICE_TOKEN as AnyObject]
        let params:Dictionary<String,AnyObject> = ["namespace" : namespace as AnyObject, "data": dataParams as AnyObject ]
        print(params)
        APIManager.sharedInstance.postRequest(serviceName: url, sendData: params, success: { (response) in
            print(response)
            
            if APIManager.sharedInstance.isSuccess {
                
                DispatchQueue.main.async {
                    SPManager.sharedInstance.clearUserAppSettings()
                    SPManager.sharedInstance.clearUserData()
                    SPManager.sharedInstance.saveUserLoginStatus(isLogin: false)
                    SPManager.sharedInstance.saveUserPortfolioStatus(isPortfolioComplete: false)
                    guestuser = ""
                    user = ""

                    UserDefaults.standard.set(guestuser, forKey: "guestuser")
                    UserDefaults.standard.set("", forKey: "userLevel")

                    UserDefaults.standard.set("" , forKey: "\(SPManager.sharedInstance.getCurrentUserEmail())date")
                    UserDefaults.standard.set("", forKey: "sampleportfolio_username")
                    let db = Databasehandler()
                    _ = db.deleteQuery(query: "DELETE FROM appData")
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "nav_bar_start") as UIViewController
                    UIApplication.shared.keyWindow?.rootViewController = vc
                }
                
            }else{
                print("Unable to perform signout")
            }
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
        }, failure: { (data) in
            print(data)
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
            if APIManager.sharedInstance.error400 {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
            }
            if(APIManager.sharedInstance.error401)
            {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
            }else{
                print("Line 807. Unable to perform signout")
            }
        })

    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    
    func saveToServer(){
        
        self.view.activityStartAnimating()
        
        let sentDate = Date()
        var userName:String
        var email:String
        var phone:String?
        var namespace:String
        if SPManager.sharedInstance.saveUserLoginStatus() {
            phone = "123456879"
            userName = SPManager.sharedInstance.getCurrentUserUsername()
            email = SPManager.sharedInstance.getCurrentUserEmail()
            namespace = SPManager.sharedInstance.getCurrentUserNamespace()

        }else{
            phone = ""
            userName = "Guest_\(FintechUtils.sharedInstance.getDeviceId())"
            email = Email
            namespace = "Guest_\(FintechUtils.sharedInstance.getDeviceId())"
        }
        var device_info : Dictionary <String, String> = [:]
//        var receiver_info : Dictionary <String, String> = [:]

         device_info = [
            "device_manufacturar": "Apple" as String,
          "device_name":UIDevice.current.name as String,
          "device_os":UIDevice.current.systemName as String,
            "device_type": "iPhone" as String,
          "device_version":UIDevice.current.systemVersion as String
        ]

        var data: Dictionary<String, Any>
        if SPManager.sharedInstance.saveUserLoginStatus() {
            data =
                ["creator" : SPManager.sharedInstance.getCurrentUserFullname() as String,
                "creator_name": SPManager.sharedInstance.getCurrentUserFullname() as String,
                "phone":phone as Any,
                "creator_email":email as String,
                "creator_namespace":namespace as String,
                "creater_device_info":device_info as Any,
                "app_id":"vesgo",
                "creator_device_token":FintechUtils.sharedInstance.DEVICE_TOKEN as Any,
                "msg_timestamp":  Int64((sentDate.timeIntervalSince1970).rounded())]
        }else {
            data =
                ["creator" :"Guest User \(FintechUtils.sharedInstance.getDeviceId())" as String,
                "creator_name": "Guest User \(FintechUtils.sharedInstance.getDeviceId())" as String,
                "phone":phone as Any,
                "creator_email":email as String,
                "creator_namespace":namespace as String,
                "creater_device_info":device_info as Any,
                "app_id":"vesgo",
                "creator_device_token":FintechUtils.sharedInstance.DEVICE_TOKEN as Any,
                "msg_timestamp":  Int64((sentDate.timeIntervalSince1970).rounded())]
        }

        print(data)

        APIManager.sharedInstance.postChatRequest(serviceName:"https://api.vesgo.io/start/chat", sendData: data as [String : AnyObject], success: { (response) in
            print(response)
            let defaults = UserDefaults.standard
            let statusCode = defaults.string(forKey: "statusCode")

            if ((statusCode?.range(of: "200")) != nil){
                DispatchQueue.main.async {
                   // self.showAlert("Success")
                    print("successly saved to server")
                    let status = response["status"] as! Bool
                    if status{
                        let data = response["data"] as! NSDictionary
                        let firebaseId = data["firebase_id"] as! String
                        print("firebase id is ......\(firebaseId)")
                        SPManager.sharedInstance.saveFirebaseid(firebaseId)
                        let channel = Channel(id: firebaseId)
                        let vc = ChatViewController(channel: channel)
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                    else{
                        self.showAlert(response["message"] as! String)
                    }
                    
                }

            }
            
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }

        }, failure: { (data) in
            print(data)
            
            DispatchQueue.main.async {
                self.view.makeToast("Unable to start chat")
                self.view.activityStopAnimating()
            }
        })
    }
    
    func  applanuage(){
        showTickMarkForLanguage()
        
        
        self.localization()
        
    }
    
    func showTickMarkForLanguage(){
        eng_tick_img.isHidden = true
        spanish_tick_img.isHidden = true
        french_tick_img.isHidden = true
        chinese_tick_img.isHidden = true
        arabic_tick_img.isHidden = true
        
        if AppPrefsManager.sharedInstance.getLanguageCode()  == LanguageCodeType.English {
            eng_tick_img.isHidden = false
        }
        else if AppPrefsManager.sharedInstance.getLanguageCode() == LanguageCodeType.Arabic {
            arabic_tick_img.isHidden = false
        }
        else if AppPrefsManager.sharedInstance.getLanguageCode() == LanguageCodeType.Spanish {
            spanish_tick_img.isHidden = false
        }
        else if AppPrefsManager.sharedInstance.getLanguageCode() == LanguageCodeType.French {
            french_tick_img.isHidden = false
        }
        
        else if AppPrefsManager.sharedInstance.getLanguageCode() == LanguageCodeType.Chinese {
            chinese_tick_img.isHidden = false
        }else {
            eng_tick_img.isHidden = false
        }
    }
    
    //MARk: Push notifaction function
    func pushnotifaction(){
        if SPManager.sharedInstance.saveUserLoginStatus() {

                if arrynoti[0].value == "1"{
                    Pushnotification_switch.setOn(true, animated: true)
                    SPManager.sharedInstance.setPushNotificationStatus(true)
                }else {
                    Pushnotification_switch.setOn(false, animated: false)
                    SPManager.sharedInstance.setPushNotificationStatus(false)
                }
        }else{
            Pushnotification_switch.setOn(SPManager.sharedInstance.getPushNotificationStatus(), animated: true)
        }
    }
}

class NotificationType: NSObject {
    let name: String
    let value: String
    
    init(name: String, value: String) {
        self.name = name
        self.value = value
    }
}


