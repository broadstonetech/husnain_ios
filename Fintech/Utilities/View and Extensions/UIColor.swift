//
//  UIColor.swift
//  BTech
//
//  Created by Mufaza Majeed on 5/27/21.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import UIKit

extension UIColor {
  
  static var primary: UIColor {
    return UIColor(red: 1 / 255, green: 93 / 255, blue: 48 / 255, alpha: 1)
  }
  
  static var incomingMessage: UIColor {
    return UIColor(red: 230 / 255, green: 230 / 255, blue: 230 / 255, alpha: 1)
  }
  
}
