//
//  PortfolioParent + VCNavigation.swift
//  Fintech
//
//  Created by broadstone on 14/03/2022.
//  Copyright © 2022 Broadstone Technologies. All rights reserved.
//

import Foundation
import UIKit

extension PortfolioParent {
    func goToGeneratedPortfolioVC(useCaseType: UseCaseType, model: PortfolioImpact?, isViewingPreMade: Bool = false) {
        let storyboard = UIStoryboard(name: "Portfolio", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "PortfolioImpactViewController") as! PortfolioImpactViewController
        vc.useCaseType = useCaseType
        if model == nil {
            vc.portfolioImpactModel = self.portfolioImpactModel
        }else {
            vc.portfolioImpactModel = model
        }
        vc.isViewingPreMadePortfolios = isViewingPreMade
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func goToPortfolioListVC(useCaseType: UseCaseType) {
        let storyboard = UIStoryboard(name: "Portfolio", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "PortfolioListVC") as! PortfolioListVC
        vc.portfolioListData = self.portfolioListModel.data
        vc.useCaseType = useCaseType
        self.navigationController?.pushViewController(vc, animated: true)
    }

    func pushAddSymbolVC() {
        let storyboard = UIStoryboard(name: "Portfolio", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "AddAndSearchSymbolVC") as! AddAndSearchSymbolVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func pushPortfolioTemplateVC() {
        let storyboard = UIStoryboard(name: "Portfolio", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "PortfolioValuesTemplateVC") as! PortfolioValuesTemplateVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func pushPortfolioRiskVC(positiveVals: [String], negativeVals: [String], template: String) {
        let storyboard = UIStoryboard(name: "Portfolio", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "PortfolioRiskVC") as! PortfolioRiskVC
        vc.positiveVals = positiveVals
        vc.negativeVals = negativeVals
        vc.template = template
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func pushAssetAllocationVC(tickersInCart: [PortfolioTickersData], useCaseType: UseCaseType, assetUpdateDalegate: AssetsUpdateDelegate?){
        let storyboard = UIStoryboard(name: "Portfolio", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "AssetAllocationVC") as! AssetAllocationVC
        vc.tickersInCart = tickersInCart
        vc.useCaseType = useCaseType
        vc.delegate = assetUpdateDalegate
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func pushValueMappedTickerVC(tickersData: ValueMappedTickersModel, risk: Int ){
        DispatchQueue.main.async { [self] in
            let storyboard = UIStoryboard(name: "Portfolio", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ValueMappedTickersVC") as! ValueMappedTickersVC
            DispatchQueue.main.async {
                vc.tickersModel = tickersData
                vc.risk = risk
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
        }
    }
    
    func pushPortfolioBacktestVC(backtestModel: PortfolioBacktestModel){
        DispatchQueue.main.async { [self] in
            let storyboard = UIStoryboard(name: "Portfolio", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "PortfolioBacktestVC") as! PortfolioBacktestVC
            vc.backtestModel = backtestModel
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func pushAssetAllocationVCForUpdate(useCaseType: UseCaseType){
        var tickersInCart = [PortfolioTickersData]()
        var allocations = [AssetAllocationStruct]()
        for item in portfolioImpactModel.data.assets {
            tickersInCart.append(PortfolioTickersData.init(symbol: item.ticker, CompanyName: item.symbolName))
            allocations.append(AssetAllocationStruct.init(symbol: item.ticker, allocation: item.allocation))
        }
        
        if tickersInCart.count == 0 {
            self.view.makeToast(getLocalizedString(key: "tickers_not_available"))
            return
        }
        
        let storyboard = UIStoryboard(name: "Portfolio", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "AssetAllocationVC") as! AssetAllocationVC
        vc.useCaseType = useCaseType
        vc.portfolioId = portfolioImpactModel.data.portfolioInfo.portfolioId
        vc.portfolioName = portfolioImpactModel.data.portfolioInfo.portfolioName
        vc.allocations = allocations
        vc.tickersInCart = tickersInCart
        vc.update = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func pushValueSuggestedPortfolioVC(data: [ValueMappedPortfolioData], portfolioType: String, posVals: [String], negVals: [String]) {
        let storyboard = UIStoryboard(name: "Portfolio", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ValueMappedSuggestedPortfolioVC") as! ValueMappedSuggestedPortfolioVC
        vc.valueMappedSuggestedPortfolios = data
        vc.portfolioType = portfolioType
        vc.negativeVals = negVals
        vc.positiveVals = posVals
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func pushPreMadePortfolioVC(useCaseType: UseCaseType) {
        let storyboard = UIStoryboard(name: "Portfolio", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "PreMadePortfolioListVC") as! PreMadePortfolioListVC
        vc.useCase = useCaseType
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
