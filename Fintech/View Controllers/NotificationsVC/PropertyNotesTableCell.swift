//
//  PropertyNotesTableCell.swift
//  TableViewExpandable
//
//  Created by Fahad Ali Awan on 24/07/2019.
//  Copyright © 2019 Org Technologies. All rights reserved.
//

import UIKit

class PropertyNotesTableCell: UITableViewCell {

  
    @IBOutlet var imgImage: UIImageView!
    @IBOutlet weak var labelFirst: UILabel!
    @IBOutlet weak var labelSecond: UILabel!
    @IBOutlet weak var utton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
