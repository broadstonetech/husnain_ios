//
//  NewsFeedDataModel.swift
//  Fintech
//
//  Created by Apple on 17/04/2020.
//  Copyright © 2020 Broadstone Technologies. All rights reserved.
//

import Foundation


class NewsFeedDataModel: NSObject {
    
    //compaies data
    fileprivate var titleText : String
    fileprivate var tickerText : String
    //chart data
    fileprivate var summaryText : String
    fileprivate var publisherName : String
    fileprivate var iconUrl : String
    fileprivate var newsLink : String
    fileprivate var timestamp : Double
    
    override init() {
        titleText = ""
        tickerText = ""
        summaryText = ""
        publisherName = ""
        iconUrl = ""
        newsLink = ""
        timestamp = -1
    }
    
    func setTitleText(_ text: String){
        self.titleText = text
    }
    
    func getTitleText() ->String {
        return self.titleText
    }
    func settickerText(_ text: String){
        self.tickerText = text
    }
    
    func gettickerText() ->String {
        return self.tickerText
    }
    func setsummaryText(_ text: String){
        self.summaryText = text
    }
    
    func getsummaryText() ->String {
        return self.summaryText
    }
    func setpublishname(_ text: String){
        self.publisherName = text
    }
    
    func  getpublishname() ->String {
        return self.publisherName
    }
    func setnewslink(_ text: String){
        self.newsLink = text
    }
    
    func  getnewslink() ->String {
        return self.newsLink
    }
    func settimestamp(_ text: Double){
        self.timestamp = text
    }
    
    func  gettimestamp() ->Double {
        return self.timestamp
    }
    
    func  getIconUrl() ->String {
        return self.iconUrl
    }
    func setIconUrl(_ text: String){
        self.iconUrl = text
    }
}
