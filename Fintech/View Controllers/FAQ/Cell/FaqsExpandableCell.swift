//
//  FaqsExpandableCell.swift
//  Fintech
//
//  Created by MacBook-Mubashar on 05/09/2019.
//  Copyright © 2019 Broadstone Technologies. All rights reserved.
//

import UIKit

class FaqsExpandableCell: UITableViewCell {

    
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var ans_label: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
