//
//  Answer_views.swift
//  Fintech
//
//  Created by Apple on 06/07/2020.
//  Copyright © 2020 Broadstone Technologies. All rights reserved.
//

import UIKit
import Koloda
import Lottie

struct quizStructure{
    var question : String!
    var options :[String]!
    var answer :String!
    var ansIndex : Int!
}
class Answer_views: Parent {
    var mainQuestion = Int()
    var doneFlag = Bool()
    var innnerQuestion = String()
    var lastQuestion = Int()
    var user = ""
    var option = ["A","B","C","D"]
    var index = 0
    var delegate: greencolorDelegate?
    var selectedIndex = -1
    var isAnsSelected = false
    var selectedAns = ""
    var questionArray = [quizStructure]()
    var question = 0
    var topicName = String()
    
    var topicID = 0
    
    @IBOutlet weak var Animation_backmain_view: AnimationView!
    @IBOutlet weak var animation_tophy_view: AnimationView!
    
    @IBOutlet weak var main_view: UIView!
    @IBOutlet weak var awardsBackground_img: UIImageView!
    @IBOutlet weak var Background_view: UIView!
    @IBOutlet weak var Backbtn_lesson: UIButton!
    @IBOutlet weak var tableView:UITableView!
    @IBOutlet weak var questionLbl:UILabel!
    
    @IBOutlet weak var heading_lbl: UILabel!
    
    @IBOutlet weak var descripation_lbl: UILabel!
    @IBOutlet weak var continue_btn: UIButton!
    
    @IBOutlet weak var share_btn: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Background_view.isHidden = true
        tableView.delegate = self
        tableView.dataSource = self
      
        
        print("Quiz index \(index)")
        
        if topicID == 1 {
            questionArray = [
                quizStructure(question: "People who own stocks are guaranteed to increase their investment?", options: ["True","False"], answer: "False",ansIndex: 1),
            quizStructure(question: "How do you make money when you trade stocks?", options: ["By holding on forever","By selling for a higher price than the purchase price","The Dow Jones Industrial Average Index determines who gets paid"], answer: "By selling for a higher price than the purchase price",ansIndex: 1),
            quizStructure(question: "If a business wants to raise capital but not increase its debt on the balance sheet, it can:", options: ["Issue a bond","Issue new stock","Borrow money from a bank","Reprice its existing debt"], answer: "Issue new stock",ansIndex: 1),
             quizStructure(question: "What type of investment is a bond?", options: ["Ownership in a company","Investment that can’t be traded","Lending money to a company for a fixed time","A promise to get paid when a company goes public"], answer: "Lending money to a company for a fixed time",ansIndex: 2)
            ]
        }
        else if topicID == 7 {
            questionArray = [
                quizStructure(question: getLocalizedString(key: "quiz_begineer_2_Q1", comment: ""), options:[getLocalizedString(key: "quiz_begineer_2_Q1_A1", comment: ""),
                getLocalizedString(key: "quiz_begineer_2_Q1_A2", comment: ""),
                getLocalizedString(key: "quiz_begineer_2_Q1_A3", comment: "")], answer: getLocalizedString(key: "quiz_begineer_2_Q1_A1", comment: ""), ansIndex: 0),
                
                quizStructure(question: getLocalizedString(key: "quiz_begineer_2_Q2", comment: ""), options:
                    [
                        getLocalizedString(key: "quiz_begineer_2_Q2_A1", comment: ""),
                        getLocalizedString(key: "quiz_begineer_2_Q2_A2", comment: ""),
                        getLocalizedString(key: "quiz_begineer_2_Q2_A3", comment: "")
                    ], answer: getLocalizedString(key: "quiz_begineer_2_Q2_A2", comment: ""), ansIndex: 1),
                
                quizStructure (question: getLocalizedString(key: "quiz_begineer_2_Q3", comment: ""), options:
                [
                    getLocalizedString(key: "quiz_begineer_2_Q3_A1", comment: ""),
                    getLocalizedString(key: "quiz_begineer_2_Q3_A2", comment: "")
                
                ], answer: getLocalizedString(key: "quiz_begineer_2_Q3_A1", comment: ""), ansIndex: 0),
                
                quizStructure (question: getLocalizedString(key: "quiz_intermediate_4_Q4", comment: ""), options:
                [
                    getLocalizedString(key: "quiz_intermediate_4_Q4_A1", comment: ""),
                    getLocalizedString(key: "quiz_intermediate_4_Q4_A2", comment: ""),
                    getLocalizedString(key: "quiz_intermediate_4_Q4_A3", comment: ""),
                    getLocalizedString(key: "quiz_intermediate_4_Q4_A4", comment: "")
                
                ], answer: getLocalizedString(key: "quiz_intermediate_4_Q4_A4", comment: ""), ansIndex: 3)
            ]
        } else if topicID == 8 {
            
            questionArray = [
                quizStructure(question: getLocalizedString(key: "quiz_intermediate_4_Q5", comment: ""),options:
                [
                    getLocalizedString(key: "quiz_intermediate_4_Q5_A1", comment: ""),
                    getLocalizedString(key: "quiz_intermediate_4_Q5_A2", comment: ""),
                    getLocalizedString(key: "quiz_intermediate_4_Q5_A3", comment: ""),
                    getLocalizedString(key: "quiz_intermediate_4_Q5_A4", comment: "")
                ], answer: getLocalizedString(key: "quiz_intermediate_4_Q5_A1", comment: ""), ansIndex: 0),
                
                quizStructure(question: getLocalizedString(key: "quiz_intermediate_4_Q6", comment: ""),options:
                [
                    getLocalizedString(key: "quiz_intermediate_4_Q6_A1", comment: ""),
                    getLocalizedString(key: "quiz_intermediate_4_Q6_A2", comment: ""),
                    getLocalizedString(key: "quiz_intermediate_4_Q6_A3", comment: ""),
                    getLocalizedString(key: "quiz_intermediate_4_Q6_A4", comment: "")
                ], answer: getLocalizedString(key: "quiz_intermediate_4_Q6_A4", comment: ""), ansIndex: 3),
                
                quizStructure(question: getLocalizedString(key: "quiz_intermediate_5_Q7", comment: ""),options:
                [
                    getLocalizedString(key: "quiz_intermediate_5_Q7_A1", comment: ""),
                    getLocalizedString(key: "quiz_intermediate_5_Q7_A2", comment: ""),
                    getLocalizedString(key: "quiz_intermediate_5_Q7_A3", comment: "")
                ], answer: getLocalizedString(key: "quiz_intermediate_5_Q7_A1", comment: ""), ansIndex: 0)
            ]
            
        } else if topicID == 9 {
            
            questionArray = [
                quizStructure(question: getLocalizedString(key: "quiz_intermediate_5_Q8", comment: ""), options:
                [
                    getLocalizedString(key: "quiz_intermediate_5_Q8_A1", comment: ""),
                    getLocalizedString(key: "quiz_intermediate_5_Q8_A2", comment: "")
                ], answer: getLocalizedString(key: "quiz_intermediate_5_Q8_A1", comment: ""), ansIndex: 0),
                
                quizStructure(question: getLocalizedString(key: "quiz_intermediate_5_Q9", comment: ""), options:
                [
                    getLocalizedString(key: "quiz_intermediate_5_Q9_A1", comment: ""),
                    getLocalizedString(key: "quiz_intermediate_5_Q9_A2", comment: ""),
                    getLocalizedString(key: "quiz_intermediate_5_Q9_A3", comment: ""),
                    getLocalizedString(key: "quiz_intermediate_5_Q9_A4", comment: "")
                ], answer: getLocalizedString(key: "quiz_intermediate_5_Q9_A4", comment: ""), ansIndex: 3),
                
                quizStructure(question: getLocalizedString(key: "quiz_skilled_3_Q10", comment: ""),options:
                [
                    getLocalizedString(key: "quiz_skilled_3_Q10_A1", comment: ""),
                    getLocalizedString(key: "quiz_skilled_3_Q10_A2", comment: ""),
                    getLocalizedString(key: "quiz_skilled_3_Q10_A3", comment: "")
                ], answer: getLocalizedString(key: "quiz_skilled_3_Q10_A1", comment: ""), ansIndex: 0)
            ]
            
        }else if topicID == 10 {
            questionArray = [
                quizStructure(question: getLocalizedString(key: "quiz_skilled_3_Q11", comment: ""), options:
                [
                    getLocalizedString(key: "quiz_skilled_3_Q11_A1", comment: ""),
                    getLocalizedString(key: "quiz_skilled_3_Q11_A1", comment: "")
                ], answer: getLocalizedString(key: "quiz_skilled_3_Q11_A2", comment: ""), ansIndex: 1),
                
                quizStructure(question: getLocalizedString(key: "quiz_skilled_3_Q12", comment: ""), options:
                [
                    getLocalizedString(key: "quiz_skilled_3_Q12_A1", comment: ""),
                    getLocalizedString(key: "quiz_skilled_3_Q12_A2", comment: "")
                ], answer: getLocalizedString(key: "quiz_skilled_3_Q12_A1", comment: ""), ansIndex: 0),
                
                quizStructure(question: getLocalizedString(key: "quiz_skilled_3_Q13", comment: ""),options:
                [
                    getLocalizedString(key: "quiz_skilled_3_Q12_A1", comment: ""),
                    getLocalizedString(key: "quiz_skilled_3_Q12_A2", comment: "")
                ], answer: getLocalizedString(key: "quiz_skilled_3_Q12_A1", comment: ""), ansIndex: 0)
            ]
        }
        else {
            questionArray = [
                quizStructure(question: "People who own stocks are guaranteed to increase their investment?", options: ["True","False"], answer: "False",ansIndex: 1),
            quizStructure(question: "How do you make money when you trade stocks?", options: ["By holding on forever","By selling for a higher price than the purchase price","The Dow Jones Industrial Average Index determines who gets paid"], answer: "By selling for a higher price than the purchase price",ansIndex: 1),
            quizStructure(question: "If a business wants to raise capital but not increase its debt on the balance sheet, it can:", options: ["Issue a bond","Issue new stock","Borrow money from a bank","Reprice its existing debt"], answer: "Issue new stock",ansIndex: 1),
             quizStructure(question: "What type of investment is a bond?", options: ["Ownership in a company","Investment that can’t be traded","Lending money to a company for a fixed time","A promise to get paid when a company goes public"], answer: "Lending money to a company for a fixed time",ansIndex: 2)
            ]
        }
        
        self.questionLbl.text = questionArray[question].question
        tableView.reloadData()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
       // let image = UIImage(systemName: "arrow.left")
        localization()
      //  Backbtn_lesson.setImage(image, for: .Normal)
    }
    
    
    func localization(){
        //        setNavBarTitile(title:getLocalizedString(key: "Education", comment: ""), topItem: getLocalizedString(key: "Education", comment: ""))
        continue_btn.setTitle(getLocalizedString(key: "Continue", comment: ""), for: .normal)
        share_btn.setTitle(getLocalizedString(key: "Share certificate", comment: ""), for: .normal)
        heading_lbl.text = getLocalizedString(key: "Congratulations", comment: "")
        descripation_lbl.text = getLocalizedString(key: "wo", comment: "")
        
        //navigationController?.navigationBar.barStyle = .blackOpaque
    }
    
    
    
    @IBAction func continue_pressed(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "EducationalSectionVC") as! EducationalSectionVC
        // UIApplication.shared.keyWindow?.rootViewController = vc
        self.delegate?.checkLast(at: index)
        
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func share_pressed(_ sender: Any) {
        
        let text = "I just finished learning about " + topicName + ". You can learn about " + topicName
            + " and many other financial literacy topics on Vesgo.\n" + ConstantStrings.APP_LINK
        shareTextualData(textData: text)
        
//        let image = UIImage(named: "Badge1")
//
//        // set up activity view controller
//        let imageToShare = [ image! ]
//        let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
//        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
//
//        // exclude some activity types from the list (optional)
//        activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook ]
//
//        // present the view controller
//        self.present(activityViewController, animated: true, completion: nil)
        
    }
    @IBAction func back_lesson(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "EducationalSectionVC") as! EducationalSectionVC
       // UIApplication.shared.keyWindow?.rootViewController = vc
        self.delegate?.checkLast(at: index)

        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func back(){
        self.navigationController?.popViewController(animated: true)
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     
     
     
     */var timer2 = Timer()
    var countdown:Int = 16
    
    @objc func updateNotification(){
        countdown -= 1
        if countdown == 0{
            Animation_backmain_view.isHidden = true
        }else{
            Animation_backmain_view.contentMode = .scaleAspectFit
            Animation_backmain_view.animationSpeed = 1.0
            Animation_backmain_view.play()
            
        }
    }
    
}
extension Answer_views: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return questionArray[question].options.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "QuizCell") as! QuizCell
        let item = questionArray[question]
        cell.optionLbl.text = item.options[indexPath.row]
        if isAnsSelected {
            
            if selectedIndex == indexPath.row{
                if selectedAns == item.answer{
                     
                    cell.backView.backgroundColor = #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1)
                    cell.optionLbl.textColor = .white

                    
                }else{
                   
                    cell.backView.backgroundColor = #colorLiteral(red: 0.5725490451, green: 0, blue: 0.2313725501, alpha: 1)
                    cell.optionLbl.textColor = .white

                    
            }
            }else if indexPath.row == item.ansIndex{
                cell.backView.backgroundColor = #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1)
                cell.optionLbl.textColor = .white


            }else{
                cell.backView.backgroundColor = .clear
                cell.optionLbl.textColor = .clear

                //cell.backView.backgroundColor = .white

            }
            //cell.backView.backgroundColor = #colorLiteral(red: 0.4352941176, green: 0.4549019608, blue: 0.8666666667, alpha: 1)

        }else{
            cell.backView.backgroundColor = .white
            cell.optionLbl.textColor = .black
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if isAnsSelected == false {
           selectedIndex =  indexPath.row
            let item = questionArray[question].options
            selectedAns = item![indexPath.row]
        isAnsSelected = true
        
        tableView.reloadData()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.question += 1
            if self.question == self.questionArray.count{
                self.Background_view.isHidden = false
                 self.main_view.isHidden = true
                self.savequestion()
                self.animation_tophy_view.contentMode = .scaleAspectFit
                self.animation_tophy_view.animationSpeed = 0.5
                self.animation_tophy_view.play()
                
                
                DispatchQueue.main.async {
                    self.timer2 = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(self.updateNotification), userInfo: nil, repeats: true)
                }
               
                //move
            }else{
                self.selectedIndex = -1
                self.selectedAns = ""
                self.isAnsSelected = false
                self.questionLbl.text = self.questionArray[self.question].question
                self.tableView.reloadData()
                
            }
        }
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func savequestion(){
        let db = Databasehandler.init(isForProgress: true)
        if !doneFlag{
            
               user = SPManager.sharedInstance.getCurrentUserEmail()
               if user == "" {
                   user = "test"
               }
               let query = "INSERT INTO appData (user,mainQuestion,innerQuestion,lastQestion) VALUES ('\(user)','\(mainQuestion)','\(innnerQuestion)',\(lastQuestion))"
                         let result = db.executeQuery(query: query, isForProgress: true)
                         print(result)
               if result{
                   print("save")
//                 awardsBackground_img.image = UIImage(named: "backgoundimg")
               }else{
                   print("not save")
               // awardsBackground_img.image = UIImage(named: "awards")
               }
    }
       

    }
}
