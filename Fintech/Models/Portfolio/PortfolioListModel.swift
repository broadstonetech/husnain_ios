//
//  PortfolioListModel.swift
//  Fintech
//
//  Created by broadstone on 09/08/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import Foundation

struct PortfolioListModel {
    let success: Bool
    let message: String
    let data: [PortfolioListData]
}

struct PortfolioListData {
    let portfolioId: Int
    let portfolioName: String
}
