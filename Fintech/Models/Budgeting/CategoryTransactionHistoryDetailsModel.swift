// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let categoryTransactionHistoryDetailsModel = try? newJSONDecoder().decode(CategoryTransactionHistoryDetailsModel.self, from: jsonData)

import Foundation

// MARK: - CategoryTransactionHistoryDetailsModel
struct CategoryTransactionHistoryDetailsModel: Codable {
    let success: Int?
    let message: String?
    var data: TransactionData?
}

// MARK: - DataClass
struct TransactionData: Codable {
    let plannedStartDate, plannedDueDate: Int64?
    let cardDetails: [CardDetail]?
    var tansactionHistory: [TansactionHistory]?

    enum CodingKeys: String, CodingKey {
        case plannedStartDate = "planned_start_date"
        case plannedDueDate = "planned_due_date"
        case cardDetails = "card_details"
        case tansactionHistory
    }
}

// MARK: - CardDetail
struct CardDetail: Codable {
    let cardType: String?
    let lastdigits: Int?

    enum CodingKeys: String, CodingKey {
        case cardType = "card_type"
        case lastdigits
    }
}

// MARK: - TansactionHistory
struct TansactionHistory: Codable {
    let categoryName: String?
    let date: Int64?
    let merchantName, subCategory: String?
    let amount: Double?

    enum CodingKeys: String, CodingKey {
        case categoryName = "category_name"
        case date
        case merchantName = "merchant_name"
        case subCategory = "sub_category"
        case amount
    }
}
