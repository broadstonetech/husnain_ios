//
//  searchBarCell.swift
//  Vesgi-Search
//
//  Created by Billal on 03/02/2021.
//

import UIKit

class SearchBarCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
