//
//  ExternalPortfolioVC.swift
//  Fintech
//
//  Created by broadstone on 05/08/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import UIKit
import LinkKit
protocol LinkOAuthHandling {
    var linkHandler: Handler? { get }
    var oauthRedirectUri: URL? { get }
}
class ExternalPortfolioVC: Parent {
    
    var oauthRedirectUri: URL? = { URL(string: "https://vesgo.io/plaid_redirect") }()
    var linkHandler: Handler?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        self.getPlaidLinkToken()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    
    func createLinkTokenConfiguration(linkToken: String) -> LinkTokenConfiguration{
        
        
        var linkConfiguration = LinkTokenConfiguration(token: linkToken) { success in
            print("public-token: \(success.publicToken) metadata: \(success.metadata)")
            self.sendPlaidToken(token: success.publicToken)
        }
        linkConfiguration.onExit = { exit in
            if let error = exit.error {
                print("exit with \(error)\n\(exit.metadata)")
            } else {
                print("exit with \(exit.metadata)")
            }
            //self.dismiss(animated: true, completion: nil)
            self.PopVC()
            self.PopVC()
        }
        return linkConfiguration
    }
    
    func getPlaidLinkToken(){
        self.view.activityStartAnimating()
        let url = ApiUrl.GET_PLAID_LINK_TOKEN
        
        let namespace = SPManager.sharedInstance.getCurrentUserNamespace()
        
        let params: Dictionary<String, AnyObject> =
                        ["namespace": namespace as AnyObject]
        
        print(params)
        APIManager.sharedInstance.postRequest(serviceName: url, sendData: params, success: { (response) in
            print(response)
            
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
            
            if APIManager.sharedInstance.isSuccess {
                    
                    DispatchQueue.main.async {
                        let message = response["message"] as? [String: AnyObject]
                        let linkToken = message!["link_token"] as? String ?? ""
                        print("LinkToken: \(linkToken)")
                        self.presentPlaidLinkUsingLinkToken(linkToken: linkToken)
                    }
                    
            } else {
                DispatchQueue.main.async {
                    self.view.makeToast("Not supported")
                    self.PopVC()
                }
            }
            
        }, failure: { (data) in
            DispatchQueue.main.async {
                print("IN FALIURE")
                self.view.activityStopAnimating()
            }
            if APIManager.sharedInstance.error400 {
                DispatchQueue.main.async {
                    self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
                }
            }
            if(APIManager.sharedInstance.error401)
            {
                DispatchQueue.main.async {
                    self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
                }
            }else{
                DispatchQueue.main.async {
                    self.showAlert(ConstantStrings.Error_msg_generic)
                }
            }
            
            DispatchQueue.main.async {
                self.view.makeToast("Not supported")
                self.PopVC()
            }
        })
    }
    
    func presentPlaidLinkUsingLinkToken(linkToken: String) {
        let linkConfiguration = createLinkTokenConfiguration(linkToken: linkToken)
        let result = Plaid.create(linkConfiguration)
        switch result {
            case .failure(let error):
                print("Unable to create Plaid handler due to: \(error)")
            case .success(let handler):
                linkHandler = handler
                handler.open(presentUsing: .viewController(self))
        }
       
    }
    
    func sendPlaidToken(token: String){
        self.view.activityStartAnimating()
        
        let url = ApiUrl.SEND_PLAID_TOKEN
        
        let namespace = SPManager.sharedInstance.getCurrentUserNamespace()
        
        let params: Dictionary<String, AnyObject> =
                        ["namespace": namespace as AnyObject,
                         "public_token": token as AnyObject,
                         "card_type": "money market" as AnyObject,
                         "lastdigits": 0000 as AnyObject]
        
        print(params)
        APIManager.sharedInstance.postRequest(serviceName: url, sendData: params, timeout: NetworkConstants.INCREASED_TIMEOUT_INTERVAL, success: { (response) in
            print(response)
            
            if APIManager.sharedInstance.isSuccess {
                    
                    DispatchQueue.main.async { [self] in
                        let storyboard = UIStoryboard(name: "Portfolio", bundle: nil)
                        let vc = storyboard.instantiateViewController(withIdentifier: "PortfolioImpactViewController") as! PortfolioImpactViewController
                        vc.useCaseType = .UseCase1
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                    
            } else {
                
            }
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
        }, failure: { (data) in
            DispatchQueue.main.async {
                print("IN FALIURE")
                self.view.activityStopAnimating()
            }
            if APIManager.sharedInstance.error400 {
                DispatchQueue.main.async {
                    self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
                }
            }
            if(APIManager.sharedInstance.error401)
            {
                DispatchQueue.main.async {
                    self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
                }
            }else{
                DispatchQueue.main.async {
                    self.showAlert(ConstantStrings.Error_msg_generic)
                }
            }
        })
    }

}
