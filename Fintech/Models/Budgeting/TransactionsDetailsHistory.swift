//
//  TransactionsDetailsHistory.swift
//  Fintech
//
//  Created by broadstone on 25/01/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import Foundation

struct TransactionDetails {
    var categoryName = ""
    var subCategoryName = ""
    var amountSpent = 0.0
    var data:Int64 = 0
    var merchantName = ""
    var transactionId = ""
}
