//
//  InsightsNewHeadingCell.swift
//  Fintech
//
//  Created by broadstone on 11/03/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import UIKit

class InsightsNewHeadingCell: UITableViewCell {
    @IBOutlet weak var heading1: UILabel!
    @IBOutlet weak var heading2: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
