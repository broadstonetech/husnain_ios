//
//  CharityDonationsHistoryModel.swift
//  Fintech
//
//  Created by broadstone on 06/01/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let charityHistoryDonationsModel = try? newJSONDecoder().decode(CharityHistoryDonationsModel.self, from: jsonData)

import Foundation

// MARK: - CharityHistoryDonationsModel
struct CharityHistoryDonationsModel: Codable {
    let success: Int
    let hasDonated: Bool
    var data: [Datum]

    enum CodingKeys: String, CodingKey {
        case success
        case hasDonated = "has_donated"
        case data
    }
}

// MARK: - Datum
struct Datum: Codable {
    let plan: String
    let donationTime: Int64
    let donatedAmount: Double
    let charitiesDistribution: [CharitiesDistribution]

    enum CodingKeys: String, CodingKey {
        case plan
        case donationTime = "donation_time"
        case donatedAmount = "donated_amount"
        case charitiesDistribution = "charities_distribution"
    }
}

// MARK: - CharitiesDistribution
struct CharitiesDistribution: Codable {
    let charityName, charityID: String
    let shareAmount, sharePercentage: Double
    let url: String
    let donationUrl: String?
    let charitiesDistributionDescription: String

    enum CodingKeys: String, CodingKey {
        case charityName = "charity_name"
        case charityID = "charity_id"
        case shareAmount = "share_amount"
        case sharePercentage = "share_percentage"
        case url
        case donationUrl = "donation_url"
        case charitiesDistributionDescription = "description"
    }
}
