//
//  listOfCharitiesCell.swift
//  Fintech
//
//  Created by Apple on 21/12/2020.
//  Copyright © 2020 Broadstone Technologies. All rights reserved.
//

import UIKit

protocol ListOfCharitiesBtnDelegate {
    func readMoreBtnPressed(at path: IndexPath)
}

class listOfCharitiesCell: UITableViewCell {
    
    var delegate: ListOfCharitiesBtnDelegate?
    
    var indexPath: IndexPath!

    @IBOutlet weak var heading_main_lbl: UILabel!
    @IBOutlet weak var descripation_lbl: UILabel!
    @IBOutlet weak var readmore_btn: UIButton!
    
    @IBOutlet weak var other_Values_charity: UILabel!
    @IBOutlet weak var heading_lbl: UILabel!
    @IBOutlet weak var tick_img: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func readMoreBtnPressed(_ sender: UIButton) {
        delegate?.readMoreBtnPressed(at: indexPath)
    }

}
