//
//  AdvisorChatVC.swift
//  Fintech
//
//  Created by broadstone on 13/01/2022.
//  Copyright © 2022 Broadstone Technologies. All rights reserved.
//

import Foundation

import UIKit
import MessageKit
import InputBarAccessoryView
import SwiftyToolTip

struct Sender: SenderType {
    var senderId: String
    var displayName: String
}

struct AdvisorChatMessage: MessageType {
    var sender: SenderType
    var messageId: String
    var sentDate: Date
    var kind: MessageKind
}

class AdvisorChatVC: MessagesViewController, MessagesDataSource, MessagesLayoutDelegate, MessagesDisplayDelegate, InputBarAccessoryViewDelegate {
    
    var advisorName: String = ""
    var history = [RecentChatLastMessage]()
    var chatId = ""
    
    var message = [MessageType]()

    let currentUser = Sender.init(senderId: "2", displayName: "")
    let serverUser = Sender.init(senderId: "1", displayName: "")
    
    weak var timer: Timer?

    deinit {
        timer?.invalidate()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.init(named: "AppBgColor")
        self.navigationItem.setHidesBackButton(false, animated: true)
        
        messageInputBar.delegate = self
        
        messagesCollectionView.messagesDataSource = self
        messagesCollectionView.messagesLayoutDelegate = self
        messagesCollectionView.messagesDisplayDelegate = self
        
        navigationItem.largeTitleDisplayMode = .never
        
        maintainPositionOnKeyboardFrameChanged = true
        messageInputBar.inputTextView.tintColor = .primary
        messageInputBar.sendButton.setTitleColor(.primary, for: .normal)
        
        if let layout = messagesCollectionView.collectionViewLayout as? MessagesCollectionViewFlowLayout {
            layout.textMessageSizeCalculator.outgoingAvatarSize = .zero
            layout.textMessageSizeCalculator.incomingAvatarSize = .zero
        }
        
        messageInputBar.leftStackView.alignment = .center
        messageInputBar.setLeftStackViewWidthConstant(to: 10, animated: false)
        messageInputBar.cornerRadius = 5
        messageInputBar.borderWidth = 2
        messageInputBar.borderColor = .gray
        
        populateChatMessages()
        timer = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(getSavedMessages), userInfo: nil, repeats: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        self.title = advisorName
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        timer?.invalidate()
    }
    
    private func populateChatMessages(){
        message.removeAll()
        for i in 0..<history.count {
            let item = history[i]
            var sender: Sender
            if item.senderNamespace == SPManager.sharedInstance.getCurrentUserNamespace() {
                sender = currentUser
            }else {
                sender = serverUser
            }
            message.append(AdvisorChatMessage.init(sender: sender, messageId: String(i), sentDate: Date.init(milliseconds: item.timestamp), kind: .text(item.message) ))
        }
                           
        DispatchQueue.main.async { [self] in
            message.sorted(by: {
                $0.sentDate < $1.sentDate
            })
            let shouldScrollToBottom = messagesCollectionView.isAtBottom
            
            messagesCollectionView.reloadData()
            
            if shouldScrollToBottom {
                self.messagesCollectionView.scrollToBottom(animated: true)
            }
        }
    }
    
    func currentSender() -> SenderType {
        return currentUser
    }
    
    func messageForItem(at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> MessageType {
        return message[indexPath.section]
    }
    
    func numberOfSections(in messagesCollectionView: MessagesCollectionView) -> Int {
        return message.count
    }
    
    func backgroundColor(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> UIColor {
        
        return isFromCurrentSender(message: message) ? UIColor.init(named: "AppMainColor")! : UIColor.gray
    }
    
    func inputBar(_ inputBar: InputBarAccessoryView, didPressSendButtonWith text: String) {

        let text = text.trimmingCharacters(in: .newlines)
        print(text)
        sendChatMessage(text)
        inputBar.inputTextView.text = ""

    }
}

extension AdvisorChatVC {
    private func sendChatMessage(_ sentMessage: String) {
        let url = ApiUrl.SEND_ADVISOR_CHAT_MESSAGE
        let params = getChatmessageParams(sentMessage)
        
        APIManager.sharedInstance.postRequest(serviceName: url, sendData: params, success: { (response) in
            
            if APIManager.sharedInstance.isSuccess {
                let success = (response["status"] as? Bool) ?? false
                if success {
                    DispatchQueue.main.async { [self] in
                        self.message.append(AdvisorChatMessage.init(sender: currentUser, messageId: "", sentDate: Date.init(milliseconds: Date().millisecondsSince1970), kind: .text(sentMessage)))
                        messagesCollectionView.reloadData()
                    }
                }
            }else {
                
            }
            
            
        }, failure: {
            (error) in
            DispatchQueue.main.async {
                self.view.makeToast("Unable to send message")
            }
        })
    }
    
    private func getChatmessageParams(_ message: String) -> [String: AnyObject] {
        let params = [
            "sender_namespace": SPManager.sharedInstance.getCurrentUserNamespace(),
            "message": message,
            "timestamp": Date().millisecondsSince1970,
            "chat_unique_id": chatId
        ] as [String: AnyObject]
        
        return params
    }
    
    
    
}
extension AdvisorChatVC {
    @objc func getSavedMessages() {
        
        let url = ApiUrl.GET_SAVED_MESSAGES
        let params = ["namespace": SPManager.sharedInstance.getCurrentUserNamespace(),
                      "chat_unique_id": chatId] as [String: AnyObject]
        
        APIManager.sharedInstance.postRequest(serviceName: url, sendData: params, success: { [self] (response) in
            
            let success = (response["status"] as? Bool) ?? false
            if success {
                let data = response["data"] as! [String: AnyObject]
                let messages = data["chat_history"] as! [AnyObject]
                
                DispatchQueue.main.async {[self] in
                    history = fetchPreviousMessage(data: messages)
                    populateChatMessages()
                }
                
                
            }else {
               
            }
            
        }, failure: { (error) in
            
        })
    }
    
    private func fetchPreviousMessage(data: [AnyObject]) -> [RecentChatLastMessage] {
        var messages = [RecentChatLastMessage]()
        for i in 0..<data.count {
            let lastMessage = data[i] as! [String: AnyObject]
            
            let senderNamespace = lastMessage["sender_namespace"] as! String
            let timestamp = lastMessage["timestamp"] as! Int64
            let message = lastMessage["message"] as! String
            
            let lastMessageData = RecentChatLastMessage.init(message: message, senderNamespace: senderNamespace, timestamp: timestamp)
            
            messages.append(lastMessageData)
        }
        
        return messages
    }
}
