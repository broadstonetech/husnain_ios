    //
    //  LibraryDataModel.swift
    //  Fintech
    //
    //  Created by MacBook-Mubashar on 27/08/2019.
    //  Copyright © 2019 Broadstone Technologies. All rights reserved.
    //
    
    import Foundation
    
    class LibraryDataModel: NSObject {
        fileprivate var mainHeadingText : String
        fileprivate var subHeadingText : String
        fileprivate var descriptionText : String
        fileprivate var iconImage : String
        fileprivate var progressValue : Double
        fileprivate var dataType : String
        fileprivate var infoType : String
        fileprivate var infoValue : String
        fileprivate var infoHeading : String
        fileprivate var infoSubHeading : String
        fileprivate var infoDescription : String
        fileprivate var infoDisclaimerImage : String
        fileprivate var infoDisclaimerText : String
        fileprivate var infoDisclaimerLink : String
        fileprivate var infoDisclaimerLinkType : String
        fileprivate var timestamp : Double
        var additionalInfoList : [[String : AnyObject]] = [[String : AnyObject]]()
        
        override init() {
            mainHeadingText = ""
            subHeadingText = ""
            descriptionText = ""
            iconImage = ""
            progressValue = -1
            dataType = ""
            infoType = ""
            infoValue = ""
            infoHeading = ""
            infoSubHeading = ""
            infoDescription = ""
            infoDisclaimerImage = ""
            infoDisclaimerText = ""
            infoDisclaimerLink = ""
            infoDisclaimerLinkType = ""
            timestamp = -1
        }
        
        //
        func getMainHeadingText() -> String {
            return mainHeadingText
        }
        func setMainHeadingText(text : String) {
            self.mainHeadingText = text
        }
        //
        func getSubHeadingText() -> String {
            return subHeadingText
        }
        func setSubHeadingText(text : String) {
            self.subHeadingText = text
        }
        //
        func getDescriptionText() -> String {
            return descriptionText
        }
        func setDescriptionText(text : String) {
            self.descriptionText = text
        }
        //
        func getIconImage() -> String {
            return iconImage
        }
        func setIconImage(text : String) {
            self.iconImage = text
        }
        //
        func getProgressValue() -> Double {
            return progressValue
        }
        func setProgressValue(text : Double) {
            self.progressValue = text
        }
        //
        func getDataType() -> String {
            return dataType
        }
        func setDataType(text : String) {
            self.dataType = text
        }
        //
        func getInfoType() -> String {
            return infoType
        }
        func setInfoType(type : String) {
            self.infoType = type
        }
        //
        func getInfoValue() -> String {
            return infoValue
        }
        func setInfoValue(value : String) {
            self.infoValue = value
        }
        //
        func getInfoHeading() -> String {
            return infoHeading
        }
        func setInfoHeading(text : String) {
            self.infoHeading = text
        }
        //
        func getInfoSubHeading() -> String {
            return infoSubHeading
        }
        func setInfoSubHeading(text : String) {
            self.infoSubHeading = text
        }
        //
        func getInfoDescription() -> String {
            return infoDescription
        }
        func setInfoDescription(text : String) {
            self.infoDescription = text
        }
        //
        func getInfoDisclaimerText() -> String {
            return infoDisclaimerText
        }
        func setInfoDisclaimerText(text : String) {
            self.infoDisclaimerText = text
        }
        //
        func getInfoDisclaimerImage() -> String {
            return infoDisclaimerImage
        }
        func setInfoDisclaimerImage(text : String) {
            self.infoDisclaimerImage = text
        }
        //
        func getInfoDisclaimerLink() -> String {
            return infoDisclaimerLink
        }
        func setInfoDisclaimerLink(text : String) {
            self.infoDisclaimerLink = text
        }
        //
        func getInfoDisclaimerLinkType() -> String {
            return infoDisclaimerLinkType
        }
        func setInfoDisclaimerLinkType(text : String) {
            self.infoDisclaimerLinkType = text
        }
        //
        func getUnixTimestamp() -> Double {
            return timestamp
        }
        func setUnixTimestamp(timestamp : Double) {
            self.timestamp = timestamp
        }
    }
