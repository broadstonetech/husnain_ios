//
//  ValuesTemplateDetailCell.swift
//  Fintech
//
//  Created by broadstone on 01/06/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import UIKit

class ValuesTemplateDetailCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var parentView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
