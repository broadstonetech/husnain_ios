//
//  AssetsListVC.swift
//  Fintech
//
//  Created by broadstone on 01/11/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import UIKit

class AssetsListVC: Parent {
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var dataAvailableView: UIView!
    @IBOutlet weak var dataNotAvailableView: UIView!
    
    var model: TickerModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dataNotAvailableView.isHidden = true
        
        
        tableView.delegate = self
        tableView.dataSource = self
        
        callStockAllocationAPI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        setNavBarTitile(title: "Deep dive", topItem: "Deep dive")
    }
    
}
private extension AssetsListVC {
    func callStockAllocationAPI(){
        self.view.activityStartAnimating()
        let url = ApiUrl.GET_Stocks_Allocation_URL
        
        let namespace = SPManager.sharedInstance.getCurrentUserNamespace()
        postManagerUser(method: "POST", isThisURLEmbdedData: false, urlString: url, parameter: ["namespace": namespace], success: {(value) in
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
            do
            {
                self.model = try JSONDecoder().decode(TickerModel?.self, from: value)
                DispatchQueue.main.async {
                    
                    self.model.message?.removeAll(where: {
                        $0.name == "Etf"
                    })
                    let count = (self.model.message?.count) ?? 0
                    
                    if count > 0 {
                        for i in 0..<self.model.message!.count {
                            self.model.message?[i].companies?.removeAll(where: {
                                $0.cikNumber == -1
                            })
                        }
                    }
                    
                    
                    
                    if self.model.message?.count == 0 {
                        self.dataNotAvailableView.isHidden = false
                        self.dataAvailableView.isHidden = true
                    }else {
                        self.tableView.reloadData()
                        self.dataNotAvailableView.isHidden = true
                        self.dataAvailableView.isHidden = false
                    }
                }
                
            }
            
            catch
            {
                DispatchQueue.main.async {
                    //self.view.makeToast("No data found")
                    self.dataNotAvailableView.isHidden = false
                    self.dataAvailableView.isHidden = true
                }
                
            }
            
        }, failure: {(err) in
            
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
            if APIManager.sharedInstance.error400 {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
            }
            if(APIManager.sharedInstance.error401)
            {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
            }
            
            DispatchQueue.main.async {
                self.dataNotAvailableView.isHidden = false
                self.dataAvailableView.isHidden = true
            }
        })
    }
}
extension AssetsListVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if model == nil {
            return 0
        }else {
            return model.message?[0].companies?.count ?? 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CompaniesTableViewCell") as! CompaniesTableViewCell
        cell.companyNameLabel.text = model.message?[0].companies?[indexPath.row].tickerTitle
        cell.tickerOutlet.text = model.message?[0].companies?[indexPath.row].ticker
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard.init(name: "SEC", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "SecDataVC") as! SecDataVC
        vc.CIK = generateCIK(String((model.message?[0].companies?[indexPath.row].cikNumber)!))
        vc.comName = model.message?[0].companies?[indexPath.row].tickerTitle ?? ""
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    private func generateCIK(_ cik: String) -> String {
        let length = 10
        let cikLength = cik.count
        
        if cikLength == length {
            return cik
        }else {
            let appendedLength = length - cikLength
            var generatedCik = ""
            for _ in 0..<appendedLength {
                generatedCik += "0"
            }
            generatedCik += cik
            return generatedCik
        }
    }
}
