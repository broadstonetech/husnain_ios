//
//  CompaniesTableViewCell.swift
//  SecDemo
//
//  Created by SaifUllah Butt on 26/10/2021.
//

import UIKit

class CompaniesTableViewCell: UITableViewCell {

    @IBOutlet weak var tickerOutlet: UILabel!
    @IBOutlet weak var companyNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
