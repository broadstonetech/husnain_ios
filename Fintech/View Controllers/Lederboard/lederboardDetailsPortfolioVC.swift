//
//  lederboardDetailsPortfolioVC.swift
//  Fintech
//
//  Created by Aqib on 18/09/2020.
//  Copyright © 2020 Broadstone Technologies. All rights reserved.
//
import UIKit
var score = Double()
var presentaile = String()
var Positivevalues = [String]()
var Negetivevalues = [String]()
var allvalues = [String]()
var stockallocation = [String]()
var risk = Double()
var heading = ""
var stockallocationname = [String]()
var  stockallocationvalue = String()
var headingname = ""
 var pastTableDateRecived: [StockAllocation]?
  var cmpyy : [Companyy]?
var portfolioComposstion: PortfolioComposition?
var esg : [Esg]?
var drivevaluess : [derivedvalues]?
class lederboardDetailsPortfolioVC: Parent {
    var tickertabledta:[Message1]?
    @IBOutlet weak var stockallocation_lbl: UILabel!
    @IBOutlet weak var stockAllocation_tableview: UITableView!
    @IBOutlet weak var main_heading_lbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.isNavigationBarHidden = true
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        stockallocation_lbl.text = "\(headingname)'s Stock Allocation"
       main_heading_lbl.text = heading
        
        print("positive= \(Positivevalues.count)")
        print("NegativeValue= \(Negetivevalues.count)")
        
    }
    
    @IBAction func cancel_button(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    /*
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
     */
    @IBAction func copyPortfolio(_ sender: Any) {
        
        if !SPManager.sharedInstance.saveUserLoginStatus(){
            let toast = getLocalizedString(key: "Join now for a custom profile", comment: "")
            self.view.makeToast(toast)
        }else{
            copyPortfolioAPI()
        }
    }

    private func copyPortfolioAPI(){
        DispatchQueue.main.async {
            self.view.activityStartAnimating()
        }
        let namespace = SPManager.sharedInstance.getCurrentUserNamespace()
        let params:Dictionary<String,AnyObject> = ["namespace" : namespace as AnyObject]

        APIManager.sharedInstance.postRequest(serviceName: ApiUrl.COPY_PORTFOLIO_URL, sendData: params,
                                              success: { (response) in
                                                DispatchQueue.main.async {
                                                    self.view.activityStopAnimating()
                                                    let toast = getLocalizedString(key: "portfolio_copied",comment: "")
                                                    self.view.makeToast(toast)
                                                }
        },
                                              failure: { (data) in
                                                DispatchQueue.main.async {
                                                    self.view.activityStopAnimating()
                                                    let toast = getLocalizedString(key: "portfolio_copied",comment: "")
                                                    self.view.makeToast(toast)


                                                }
        })
    }
}
extension lederboardDetailsPortfolioVC:UITableViewDelegate,UITableViewDataSource{
    // MARK: - TableView Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (pastTableDateRecived?.count)! + (cmpyy?.count)! + 1 + 1 + allvalues.count + 1 + (esg?.count)! + (drivevaluess?.count)!
    }
    func numberOfSections(in tableView: UITableView) -> Int {
    return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row < (pastTableDateRecived?.count)! ,let cell =  self.stockAllocation_tableview.dequeueReusableCell(withIdentifier: "cell1") as? lederboardDetailsPortfolioCell {
            // cell.Heading_lbl.text =  "Hading1"
             let item = pastTableDateRecived?[indexPath.row]
               print(item)
            cell.name_stockallocation_lbl.text = item?.name ?? ""
            cell.values_stockallocation_lbl.text = "\(String(item?.value ?? 0))%"
            return cell
        } else if indexPath.row < (pastTableDateRecived?.count)! + (cmpyy?.count)! ,let cell =  self.stockAllocation_tableview.dequeueReusableCell(withIdentifier: "cell2") as? lederboardDetailsPortfolioCell {
            let item2 = cmpyy?[indexPath.row - (pastTableDateRecived?.count)!]
            print(item2)
            cell.ticker_stockallocation_lbl.text = item2?.ticker ?? ""
            cell.tickerTitle_stockallocation_lbl.text = item2?.tickerTitle ?? ""
            cell.tickerSahre_stockAllocation_lbl.text = "\(String(item2?.tickerShare ?? 0))%"
            return cell
        }else if indexPath.row < (pastTableDateRecived?.count)! + (cmpyy?.count)! + 1 ,let cell =  self.stockAllocation_tableview.dequeueReusableCell(withIdentifier: "cell3") as? lederboardDetailsPortfolioCell {
            cell.score_lbl.text = String(score)
            return cell
        }else if indexPath.row < (pastTableDateRecived?.count)! + (cmpyy?.count)! + 1 + 1 ,let cell =  self.stockAllocation_tableview.dequeueReusableCell(withIdentifier: "cell4") as? lederboardDetailsPortfolioCell {
            cell.main_heading_values.text = "\(headingname)'s investment values"
    
            return cell
        }else if indexPath.row < (pastTableDateRecived?.count)! + (cmpyy?.count)! + 1 + 1 + allvalues.count ,let cell =  self.stockAllocation_tableview.dequeueReusableCell(withIdentifier: "cell5") as? lederboardDetailsPortfolioCell {
            //cell.main_heading_values.text = "aaaaaa"
            cell.heading_lbl_value.text = allvalues[indexPath.item - 1 - 1 - (cmpyy?.count)! - (pastTableDateRecived?.count)! ]
            if allvalues[indexPath.item - 1 - 1 - (cmpyy?.count)! - (pastTableDateRecived?.count)! ] == "Animal welfare"{
                cell.img_value.image = UIImage(named: "animal_welfare")
                cell.details_img_values.image = UIImage(named: "care")
            }else if allvalues[indexPath.item - 1 - 1 - (cmpyy?.count)! - (pastTableDateRecived?.count)! ] == "Coal"{
                cell.details_img_values.image = UIImage(named: "care")
                cell.img_value.image = UIImage(named: "Coal")
                
            }else if allvalues[indexPath.item - 1 - 1 - (cmpyy?.count)! - (pastTableDateRecived?.count)! ] == "Vegan"{
                cell.details_img_values.image = UIImage(named: "care")
                cell.img_value.image = UIImage(named: "vegan")
            }else if allvalues[indexPath.item - 1 - 1 - (cmpyy?.count)! - (pastTableDateRecived?.count)! ] == "Gambling"{
                cell.details_img_values.image = UIImage(named: "care")
                cell.img_value.image = UIImage(named: "Gambling")
            }else if allvalues[indexPath.item - 1 - 1 - (cmpyy?.count)! - (pastTableDateRecived?.count)! ] == "Nuclear power"{
                cell.details_img_values.image = UIImage(named: "care")
                cell.img_value.image = UIImage(named: "Nuclear_Power")
            }else if allvalues[indexPath.item - 1 - 1 - (cmpyy?.count)! - (pastTableDateRecived?.count)! ] == "Family values"{
                cell.img_value.image = UIImage(named: "family_values")
                cell.details_img_values.image = UIImage(named: "care")
            }else if allvalues[indexPath.item - 1 - 1 - (cmpyy?.count)! - (pastTableDateRecived?.count)! ] == "Water" {
                cell.img_value.image = UIImage(named: "water100x100")
                cell.details_img_values.image = UIImage(named: "care")
            }else {
                cell.img_value.image = UIImage(named: "environment")
            }
            return cell
        }
        else if indexPath.row < (pastTableDateRecived?.count)! + (cmpyy?.count)! + 1 + 1 + allvalues.count + Negetivevalues.count ,let cell =  self.stockAllocation_tableview.dequeueReusableCell(withIdentifier: "cell5") as? lederboardDetailsPortfolioCell {
            //cell.main_heading_values.text = "aaaaaa"
            cell.heading_lbl_value.text = Negetivevalues[indexPath.item - 1 - 1 - allvalues.count - (cmpyy?.count)! - (pastTableDateRecived?.count)! ]
            if Negetivevalues[indexPath.item - 1 - 1 - allvalues.count - (cmpyy?.count)! - (pastTableDateRecived?.count)! ] == "Animal welfare"{
                cell.img_value.image = UIImage(named: "animal_welfare")
                cell.details_img_values.image = UIImage(named: "angry")
            }else if allvalues[indexPath.item - 1 - 1 - allvalues.count - (cmpyy?.count)! - (pastTableDateRecived?.count)! ] == "Coal"{
                cell.details_img_values.image = UIImage(named: "angry")
                cell.img_value.image = UIImage(named: "Coal")
                
            }else if allvalues[indexPath.item - 1 - 1 - allvalues.count - (cmpyy?.count)! - (pastTableDateRecived?.count)! ] == "Vegan"{
                cell.details_img_values.image = UIImage(named: "angry")
                cell.img_value.image = UIImage(named: "vegan")
            }else if allvalues[indexPath.item - 1 - 1 - allvalues.count  - (cmpyy?.count)! - (pastTableDateRecived?.count)! ] == "Gambling"{
                cell.details_img_values.image = UIImage(named: "angry")
                cell.img_value.image = UIImage(named: "Gambling")
            }else if allvalues[indexPath.item - 1 - 1 - allvalues.count  - (cmpyy?.count)! - (pastTableDateRecived?.count)! ] == "Nuclear power"{
                cell.details_img_values.image = UIImage(named: "angry")
                cell.img_value.image = UIImage(named: "Nuclear_Power")
            }else if allvalues[indexPath.item - 1 - 1 - allvalues.count - (cmpyy?.count)! - (pastTableDateRecived?.count)! ] == "Family values"{
                cell.img_value.image = UIImage(named: "family_values")
                cell.details_img_values.image = UIImage(named: "angry")
            }else if allvalues[indexPath.item - 1 - 1 - allvalues.count - (cmpyy?.count)! - (pastTableDateRecived?.count)! ] == "Water" {
                cell.img_value.image = UIImage(named: "water100x100")
                cell.details_img_values.image = UIImage(named: "angry")
            }else {
                cell.img_value.image = UIImage(named: "environment")
            }
            return cell
        }
        else if indexPath.row < (pastTableDateRecived?.count)! + (cmpyy?.count)! + 1 + 1 + allvalues.count +  Negetivevalues.count + 1 ,let cell =  self.stockAllocation_tableview.dequeueReusableCell(withIdentifier: "cell6") as? lederboardDetailsPortfolioCell {
            //cell.main_heading_values.text = "aaaaaa"
            cell.main_heading_Invesmentimpact.text = "\(headingname)'s investment impact"
            return cell
        }
        else if indexPath.row < (pastTableDateRecived?.count)! + (cmpyy?.count)! + 1 + 1 + allvalues.count +  Negetivevalues.count + 1 + (esg?.count)!,let cell =  self.stockAllocation_tableview.dequeueReusableCell(withIdentifier: "cell7") as? lederboardDetailsPortfolioCell {
            //cell.main_heading_values.text = "aaaaaa"
            let item = esg?[(indexPath.row) - 1 - allvalues.count - Negetivevalues.count - 1 - 1 - (cmpyy?.count)! - (pastTableDateRecived?.count)!]
            cell.heading_lbl_impact.text = item?.value
            if item?.value == "Social"{
                cell.img_heading_impact.image = UIImage(named: "social")
                cell.presentage_impact_lbl.text = String(item?.score ?? 0.0)
            }else if item?.value == "Environment"{
                cell.img_heading_impact.image = UIImage(named: "environment")
                cell.presentage_impact_lbl.text = String(item?.score ?? 0.0)
            }else if item?.value == "Governance"{
                cell.img_heading_impact.image = UIImage(named: "governance")
                cell.presentage_impact_lbl.text = String(item?.score ?? 0.0)
            }else {
            }
        return cell
} else if indexPath.row < (pastTableDateRecived?.count)! + (cmpyy?.count)! + 1 + 1 + allvalues.count + 1 + Negetivevalues.count + (esg?.count)! + (drivevaluess?.count)!,let cell =  self.stockAllocation_tableview.dequeueReusableCell(withIdentifier: "cell") as? lederboardDetailsPortfolioCell {
             let item = drivevaluess?[(indexPath.row) - 1 - allvalues.count - Negetivevalues.count - 1 - 1 - (cmpyy?.count)! - (pastTableDateRecived?.count)! - (esg?.count)!]
            cell.heading_derivevalues_impact_lbl.text = item?.value
             if item?.value == "Vegan"{
                    cell.img_drivevalues_impact.image = UIImage(named: "vegan")
                }else if item?.value == "Child labor"{
                    cell.img_drivevalues_impact.image = UIImage(named: "child lb")
                }else if item?.value == "Palm oil" {
                    cell.img_drivevalues_impact.image = UIImage(named: "palm oil")
                }else if item?.value == "Firearms" {
                    cell.img_drivevalues_impact.image = UIImage(named: "firearms")
                }else if item?.value == "Gmo" {
                    cell.img_drivevalues_impact.image = UIImage(named: "GMO")
                }else if item?.value == "Animal welfare" {
                    cell.img_drivevalues_impact.image = UIImage(named: "animal_welfare")
                }else if item?.value == "Controversial weapons" {
                    cell.img_drivevalues_impact.image = UIImage(named: "controversial_weapons")
                }else if item?.value == "Military Contract" {
                    cell.img_drivevalues_impact.image = UIImage(named: "military_contract")
                }else if item?.value == "Climate change" {
                    cell.img_drivevalues_impact.image = UIImage(named: "climate_change")
                }else if item?.value == "Alignment with UN SDGs" {
                    cell.img_drivevalues_impact.image = UIImage(named: "alignment_UN")
                
                }else if item?.value == "Labor rights" {
                    cell.img_drivevalues_impact.image = UIImage(named: "child lb")
                
                }else if item?.value == "Diversity" {
                   cell.img_drivevalues_impact.image = UIImage(named: "diversity")
                
                }else if item?.value == "Civil liberties" {
                    cell.img_drivevalues_impact.image = UIImage(named: "civil_liberties")
                
                }else if item?.value == "Water" {
                    cell.img_drivevalues_impact.image = UIImage(named: "water100x100")
                
                }else if item?.value == "Wind power - positive" {
                   cell.img_drivevalues_impact.image = UIImage(named: "wind_power")
                
                }else if item?.value == "Carbon emissions" {
                    cell.img_drivevalues_impact.image = UIImage(named: "carbon_emission")
                
                }else if item?.value == "Stem cell research" {
                    cell.img_drivevalues_impact.image = UIImage(named: "stem_cell_research")
                
                }else if item?.value == "Alcohol" {
                    cell.img_drivevalues_impact.image = UIImage(named: "Alcohol-1")
                
                }else if item?.value == "Adult entertainment" {
                cell.img_drivevalues_impact.image = UIImage(named: "vegan")
                
                }else if item?.value == "Contraceptives" {
                    cell.img_drivevalues_impact.image = UIImage(named: "contraceptives")
                
                }else if item?.value == "Abortion" {
                    cell.img_drivevalues_impact.image = UIImage(named: "Abortion")
                
                }else if item?.value == "Maternity leave (rights)" {
                    cell.img_drivevalues_impact.image = UIImage(named: "maternity_leaves")
                
                }else if item?.value == "Family values" {
                    cell.img_drivevalues_impact.image = UIImage(named: "family_values")
                
                }else if item?.value == "Immigration" {
                    cell.img_drivevalues_impact.image = UIImage(named: "Immigration")
                
                }else if item?.value == "LGBTQ" {
                    cell.img_drivevalues_impact.image = UIImage(named: "LGBTQ")
                
                }else if item?.value == "Gender equity" {
                    cell.img_drivevalues_impact.image = UIImage(named: "gender_equality")
                
                }else if item?.value == "Millennial owned" {
              cell.img_drivevalues_impact.image = UIImage(named: "environment")
                
                }else if item?.value == "Minority owned" {
                    cell.img_drivevalues_impact.image = UIImage(named: "minority_owned")
                
                }else if item?.value == "Interest bearing instruments" {
                    cell.img_drivevalues_impact.image = UIImage(named: "interest_bearing_instruments")
                
                }else if item?.value == "Coal" {
                    cell.img_drivevalues_impact.image = UIImage(named: "Coal")
             }else if item?.value == "Nuclear power" {
                cell.img_drivevalues_impact.image = UIImage(named: "Nuclear_Power")
             }else if item?.value == "Pesticides" {
                cell.img_drivevalues_impact.image = UIImage(named: "Pesticides")
             }else if item?.value == "Tobacco" {
                cell.img_drivevalues_impact.image = UIImage(named: "Tobacco")
             }
             else if item?.value == "Fur leather" {
                cell.img_drivevalues_impact.image = UIImage(named: "fur_leather")
             }
             else if item?.value == "Gambling" {
                cell.img_drivevalues_impact.image = UIImage(named: "Gambling")
             }else if item?.value == "Clean energy" {
                cell.img_drivevalues_impact.image = UIImage(named: "green_energy")
             }else{
                    cell.img_drivevalues_impact.image = UIImage(named: "environment")
                
                }
            if (item?.score ?? 0.0) < 0.8 {
            cell.persentage_derivevalues_impact_lbl.text = "No impact"
                
                    }else if (item?.score ?? 0.0) >= 1.0{
                
                        cell.persentage_derivevalues_impact_lbl.text = "Some impact"
                
                    }
            
            
            return cell
}
        return UITableViewCell()
}
}
