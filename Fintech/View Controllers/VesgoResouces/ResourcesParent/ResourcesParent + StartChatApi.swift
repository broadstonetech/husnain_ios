//
//  ResourcesParent + StartChatApi.swift
//  Fintech
//
//  Created by broadstone on 12/01/2022.
//  Copyright © 2022 Broadstone Technologies. All rights reserved.
//

import Foundation
import UIKit

protocol StartChatWithAdvisorDelegate {
    func startChatApiCalled(successfully: Bool, with model: StartChatWithAdvisorModel?)
}

extension ResourcesParent {
    
    func startChatWithAdvisor(advisorModel: AdvisorsModel) {
        self.view.activityStartAnimating()
        let url = ApiUrl.START_CHAT_WITH_ADVISOR
        let params = getAdvisorChatParams(advisorModel: advisorModel)
        
        APIManager.sharedInstance.postRequest(serviceName: url, sendData: params) { (response) in
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
            if APIManager.sharedInstance.isSuccess{
                DispatchQueue.main.async { [self] in
                    parseResponse(res: response, advisorName: advisorModel.advisorName)
                }
            }else {
                DispatchQueue.main.async {
                    self.advisorStartChatApiDelegate?.startChatApiCalled(successfully: false, with: nil)
                }
            }
        } failure: { data in
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
            print("Failed to hit StartChatWithAdvisorAPI")
            DispatchQueue.main.async {
                self.advisorStartChatApiDelegate?.startChatApiCalled(successfully: false, with: nil)
            }
        }

        
    }
    
    private func getAdvisorChatParams(advisorModel: AdvisorsModel) -> [String: AnyObject] {
        
        let creatorDeviceInfo = [
            "device_manufacturar": "Apple",
            "device_name": UIDevice.current.name,
            "device_os": UIDevice.current.systemName,
            "device_type": UIDevice.current.localizedModel,
            "device_version": String(UIDevice.current.systemVersion),
        ] as [String: AnyObject]
        
        let params = [
            "app_id": "Vesgo",
            "creator_name": SPManager.sharedInstance.getCurrentUserFullname(),
            "creator_device_token": FintechUtils.sharedInstance.DEVICE_TOKEN,
            "creator_email": SPManager.sharedInstance.getCurrentUserEmail(),
            "phone": "",
            "creator_namespace": SPManager.sharedInstance.getCurrentUserNamespace(),
            "msg_timestamp": Date().millisecondsSince1970,
            "creater_device_info": creatorDeviceInfo,
            "advisor_namespace": advisorModel.advisorNamespace
        ] as [String: AnyObject]
        
        return params
    }
    
    
    private func parseResponse(res: [String: AnyObject], advisorName: String) {
        let success = res["status"] as! Bool
        let message = res["message"] as! String
        
        if !success {
            advisorStartChatApiDelegate?.startChatApiCalled(successfully: false, with: nil)
            return
        }
        
        let data = res["data"] as! [String: AnyObject]
        
        let chatUniqueId = data["chat_unique_id"] as! String
        
        let advisorData = StartChatWithAdvisorData.init(chatUniqueId: chatUniqueId)
        
        let model = StartChatWithAdvisorModel.init(success: success, message: message, data: advisorData)
        
        goToChatVC(advisorName: advisorName, chatId: chatUniqueId, chatHistory: [RecentChatLastMessage]())
        
        advisorStartChatApiDelegate?.startChatApiCalled(successfully: true, with: model)
    }
}
