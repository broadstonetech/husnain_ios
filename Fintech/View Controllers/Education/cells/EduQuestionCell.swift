//
//  EduQuestionCell.swift
//  Fintech
//
//  Created by apple on 07/07/2020.
//  Copyright © 2020 Broadstone Technologies. All rights reserved.
//

import UIKit

class EduQuestionCell: UITableViewCell {

    @IBOutlet weak var circleView: UIView!
    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var heading: UILabel!
    @IBOutlet weak var question: UILabel!
    @IBOutlet weak var circleImage:UIImageView!



    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
