import UIKit
import CoreData
import Firebase
import UserNotifications
import Firebase
import FirebaseMessaging
//import FirebaseInstanceID
import IQKeyboardManagerSwift
import Stripe
import AppTrackingTransparency
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate,MessagingDelegate {
var orientationLock = UIInterfaceOrientationMask.all
    
    let gcmMessageIDKey = "gcm.message_id"
    
    var window: UIWindow?
//    func applicationReceivedRemoteMessage(_ remoteMessage: MessagingRemoteMessage) {
//        print(remoteMessage.appData)
//    }
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        if #available(iOS 15, *) {
            let tabBar = UITabBar()
            let tabBarAppearance = UITabBarAppearance()
            tabBarAppearance.backgroundColor = UIColor.init(named: "AppBgColor")!
            tabBarAppearance.stackedLayoutAppearance.selected.titleTextAttributes = [.foregroundColor: UIColor.init(named: "AppMainColor")!]
            tabBarAppearance.stackedLayoutAppearance.normal.titleTextAttributes = [.foregroundColor: UIColor.init(named: "AppBgColor")!]
            tabBar.standardAppearance = tabBarAppearance
            tabBar.scrollEdgeAppearance = tabBarAppearance
            UITableView.appearance().sectionHeaderTopPadding = 0.0
        }
        
        //Stripe key
        STPAPIClient.shared.publishableKey = PaymentAPIKeys.STRIPE_PUBLISHABLE_KEY
    
        // Override point for customization after application launch.
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.disabledDistanceHandlingClasses.append(AdvisorChatVC.self)

        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor :  UIColor(red: 255.0/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1.0)]
        FirebaseApp.configure()
        
        
        // MARK: - Notification Allow
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            // For iOS 10 data message (sent via FCM

            Messaging.messaging().delegate = self
           // Messaging.messaging().remoteMessageDelegate = self
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }

        application.registerForRemoteNotifications()
        Messaging.messaging().delegate = self
        //Messaging.messaging().shouldEstablishDirectChannel = true
        
        if #available(iOS 14, *) {
            ATTrackingManager.requestTrackingAuthorization(completionHandler: { status in
                //you got permission to track
                if status == ATTrackingManager.AuthorizationStatus.authorized {
                    print("got permissions for ios14+")
                } else {
                    print("permissions denied for ios14+")
                }

            })

        } else {
            //you got permission to track, iOS 14 is not yet installed
            print("got permission below ios14")

        }
        
        
        return true
        
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        application.applicationIconBadgeNumber = 0
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }
    
    var myOrientation: UIInterfaceOrientationMask = .portrait
    
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        return myOrientation
    }
    
    func application(_ application: UIApplication,
                     continue userActivity: NSUserActivity,
                     restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void
    ) -> Bool {
        guard userActivity.activityType == NSUserActivityTypeBrowsingWeb, let webpageURL = userActivity.webpageURL else {
            return false
        }

        // Check that the userActivity.webpageURL is the oauthRedirectUri
        // configured in the Plaid dashboard.
        guard let linkOAuthHandler = window?.rootViewController as? LinkOAuthHandling,
            let handler = linkOAuthHandler.linkHandler,
            webpageURL.host == linkOAuthHandler.oauthRedirectUri?.host &&
            webpageURL.path == linkOAuthHandler.oauthRedirectUri?.path
        else {
            return false
        }

        // Continue the Link flow
        if let error = handler.continueFrom(redirectUri: webpageURL) {
            print("Unable to continue from redirect due to: \(error)")
        }

        return true
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        
        print(userInfo)

        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        print("didReceiveRemoteNotification")
        //coordinateToSomeVC(userInfo: userInfo)
        
        if SPManager.sharedInstance.getPushNotificationStatus() {
            print("Notifications On")
            if SPManager.sharedInstance.saveUserLoginStatus() {
                print("Notifications Logged in")
                let namespace = (userInfo["namespace"] as? String) ?? ""
                if namespace == SPManager.sharedInstance.getCurrentUserNamespace() {
                    print("Namespace matches")
                    coordinateToSomeVC(userInfo: userInfo)
                }else if namespace == FCM.DEFAULT_NAMESPACE_FOR_NOTIFICATION {
                    print("Namespace default received")
                    coordinateToSomeVC(userInfo: userInfo)
                }else {
                    print("Namespace not matches")
                    //DO NOTHING
                }
                
            }else {
                print("Notifications Logged out")
                //DO NOTHING
            }
        }else {
            print("Notifications Off")
            //DO NOTHING
        }
    }

    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {

        print("Unable to register for remote notifications: \(error.localizedDescription)")

    }

    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {

                print("Firebase registration token: \(fcmToken ?? "")")
                let dataDict:[String: String] = ["token": fcmToken ?? ""]
                NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
                FintechUtils.sharedInstance.DEVICE_TOKEN = fcmToken ?? ""
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().apnsToken = deviceToken
        Messaging.messaging().token { (token, error) in
            if let error = error {
                print("Error fetching remote instance ID: \(error.localizedDescription)")
            } else if let token = token {
                print("Token is \(token)")
                
                FintechUtils.sharedInstance.DEVICE_TOKEN = token
            }
        }
    }
    

        

        func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void)

        {

            print("82")
            print("withCompletionHandler")

            let userInfo = response.notification.request.content.userInfo

            

            
            
            if SPManager.sharedInstance.getPushNotificationStatus() {
                if SPManager.sharedInstance.saveUserLoginStatus() {
                    let namespace = (userInfo["namespace"] as? String) ?? ""
                    if namespace == SPManager.sharedInstance.getCurrentUserNamespace() {
                        coordinateToSomeVC(userInfo: userInfo)
                        completionHandler()
                    }else if namespace == FCM.DEFAULT_NAMESPACE_FOR_NOTIFICATION {
                        coordinateToSomeVC(userInfo: userInfo)
                        completionHandler()
                    }else {
                        //DO NOTHING
                    }
                    
                }else {
                    //DO NOTHING
                }
            }else {
                //DO NOTHING
            }
            
        }



        private func coordinateToSomeVC(userInfo : [AnyHashable : Any])

        {
            print("100")
            
            let loc_key = userInfo["loc_key"] as! String

            guard let window = UIApplication.shared.keyWindow else { return }
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "tab_bar_start") as! MainTabBarController
            vc.notificationType = loc_key
            let navController = UINavigationController(rootViewController: vc)
            navController.navigationBar.barTintColor = UIColor(named: "AppMainColor")!
            navController.modalPresentationStyle = .fullScreen
            window.rootViewController = navController
            window.makeKeyAndVisible()

        }
    
    
    // MARK :- notifaction
    func getNotificationSettings() {
        UNUserNotificationCenter.current().getNotificationSettings { settings in
            print("Notification settings: \(settings)")
        }
    }
    
    
//
    
   
    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "Fintech")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

}

//// MARK: - Push notification
func registerForPushNotifications() {
    UNUserNotificationCenter.current() // 1
        .requestAuthorization(options: [.alert, .sound, .badge]) { // 2
            granted, error in
            print("Permission granted: \(granted)") // 3
    }
}
