//
//  TopHoldingCell.swift
//  Fintech
//
//  Created by broadstone on 07/01/2022.
//  Copyright © 2022 Broadstone Technologies. All rights reserved.
//

import UIKit
import Charts

class TopHoldingCell: UICollectionViewCell {
    
    @IBOutlet weak var parentView: UIView!
    @IBOutlet weak var symbolLabel: UILabel!
    @IBOutlet weak var portfolioReturn: UILabel!
    @IBOutlet weak var tickerName: UILabel!
    @IBOutlet weak var assetPercent: UILabel!
    @IBOutlet weak var lineChart: LineChartView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureCell(_ model: DashboardHoldingsModelDatum){
        symbolLabel.text = model.tickers
        
        if model.totalReturn.contains("-") {
            portfolioReturn.text = model.totalReturn
            portfolioReturn.textColor = .systemRed
        }else {
            if model.totalReturn.contains("+") {
                portfolioReturn.text = model.totalReturn
            }else {
                portfolioReturn.text = "+" + model.totalReturn
            }
            portfolioReturn.textColor = .systemGreen
        }
        tickerName.text = model.tickerName
        assetPercent.text = "$" + FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: model.amountInvested) + "\n\(model.portfolioShare)"
        
        
    }

}
