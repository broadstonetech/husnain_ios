//
//  PortfolioParent + BarButton.swift
//  Fintech
//
//  Created by broadstone on 14/03/2022.
//  Copyright © 2022 Broadstone Technologies. All rights reserved.
//

import Foundation
import UIKit

extension PortfolioParent {
    func createRightButton(title: String, action: Selector){
        
        let rightButton = UIButton(frame: CGRect(x: 0, y: 0, width: 45, height: 16))
        rightButton.setTitle(title, for: .normal)
        rightButton.addTarget(self, action: action, for: .touchUpInside)
        //rightButton.titleLabel?.font = UIFont(name: "HelveticaNeue", size: 15)
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: rightButton)
    }
    
    func createPlusButton(action: Selector){
        let rightButton = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 20))
        
        if #available(iOS 13.0, *) {
            rightButton.setBackgroundImage(UIImage(systemName: "plus")?.withTintColor(.white), for: .normal)
        } else {
            rightButton.setTitle(getLocalizedString(key: "_new"), for: .normal)
        }
        rightButton.addTarget(self, action: action, for: .touchUpInside)
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: rightButton)
    }
}
