//
//  AdvisorsModel.swift
//  EducationModuleSP
//
//  Created by SaifUllah Butt on 29/12/2021.
//

import Foundation

struct AdvisorsModel {
   
    var advisorNamespace = ""
    var advisorName = ""
    var advisorProfilePic = ""
    var advisorAccountStatus = ""
    var advisorDescription = ""
    var advisorExpertise = [String]()
    var advisorAvailability : Bool = false
    var advisorCertificates = [String]()
    var advisorEmail = ""
    var advisorSocialLinks : AdvisorSocialLinks //make separate model
    var advisorTimestamp : Int64 = 0
    
}

struct AdvisorSocialLinks {
    
    var facebook = ""
    var twitter = ""
    var linkedIn = ""
    
}
