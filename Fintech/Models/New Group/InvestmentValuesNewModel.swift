//
//  InvestmentValuesNewModel.swift
//  Fintech
//
//  Created by broadstone on 12/03/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import Foundation

struct OverallESGModel {
    var obtainedScore = 0.0
    var maxScore = 0.0
    var controversy = 0.0
}

struct OverallNewEsgModel {
    var controversy = 0
    var impact = ""
    var betterPeers = ""
    var badPeers = ""
    var zScore = 0.0
}

struct EsgScore {
    var obtainedScore = 0.0
    var maxScore = 0.0
    var grade = ""
    var percentile = 0.0
    var derivedValues = [EsgDerivedValues]()
}

struct NewEsgScoreModel {
    var impact = ""
    var betterPeers = ""
    var badPeers = ""
    var zScore = 0.0
    var derivedValues = [EsgDerivedValues]()
}

struct EsgDerivedValues {
    var valueName = ""
    var score = 0.0
}
