//
//  ExpenseDetailCell.swift
//  vesgo
//
//  Created by Apple on 05/01/2021.
//  Copyright © 2021 Apple. All rights reserved.
//

import UIKit

class ExpenseDetailCell: UICollectionViewCell {
    @IBOutlet weak var card_number_lbl: UILabel!
    
    @IBOutlet weak var username_lbl: UILabel!
    @IBOutlet weak var amount_lbl: UILabel!
    @IBOutlet weak var background_card_img: UIImageView!
}
