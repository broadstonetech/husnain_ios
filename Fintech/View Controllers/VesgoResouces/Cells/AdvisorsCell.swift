//
//  AdvisorsCell.swift
//  Fintech
//
//  Created by broadstone on 05/01/2022.
//  Copyright © 2022 Broadstone Technologies. All rights reserved.
//

import UIKit

class AdvisorsCell: UICollectionViewCell {
    
    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    override class func awakeFromNib() {
        super.awakeFromNib()
    }
}
