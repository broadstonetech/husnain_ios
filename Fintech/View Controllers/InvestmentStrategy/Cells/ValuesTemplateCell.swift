//
//  ValuesTemplateCell.swift
//  Fintech
//
//  Created by broadstone on 31/05/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import UIKit

protocol ValuesTemplateClickDelegate {
    func detailButtonPressed(cellIndex: Int)
}

class ValuesTemplateCell: UITableViewCell {
    
    @IBOutlet weak var templateLabel: UILabel!
    @IBOutlet weak var parentView: UIView!
    @IBOutlet weak var templateImage: UIImageView!
//    @IBOutlet weak var detailBtn: UIButton!
    
    var cellIndex: Int?
    //var clickDelegate: ValuesTemplateClickDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
//    @IBAction func detailButtonPressed(_ sender: UIButton) {
//        clickDelegate?.detailButtonPressed(cellIndex: cellIndex!)
//    }

}
