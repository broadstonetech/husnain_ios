//
//  AdvisorFilterVC.swift
//  Fintech
//
//  Created by broadstone on 19/01/2022.
//  Copyright © 2022 Broadstone Technologies. All rights reserved.
//

import UIKit



        

class AdvisorFilterVC: ResourcesParent {
    
    var filters = [FilterCategories]()
    var delegate: FilterVCDelegte?
    
    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        
        
    }
    
    @IBAction func applyBtnPressed(_ sender: UIButton) {
        let expertiseOptions = (filters[0].selectedOptions as? [String]) ?? [String]()
        let alphabetOption = (filters[1].selectedOptions as? SortByAlphabets) ?? SortByAlphabets.notSelected
        let availabilityOption = (filters[2].selectedOptions as? SortByAvailability) ?? SortByAvailability.notSelected
        
        delegate?.selection(option: expertiseOptions, sortType: alphabetOption, availability: availabilityOption, completeFiltersData: filters)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cancelBtnPressed(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    
}




extension AdvisorFilterVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return filters.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1 + filters[section].options.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FilterHeaderCell") as! FilterHeaderCell
            cell.headingLabel.text = filters[indexPath.section].name
            return cell
            
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FilterOptionsCell") as! FilterOptionsCell
            let data = filters[indexPath.section].options[indexPath.row - 1]
            cell.optionLabel.text = data.name
            
            if data.isSelected {
                cell.optionImage.isHidden = false
            }else {
                cell.optionImage.isHidden = true
            }
            
            return cell
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            return
        }else {
            
            let data = filters[indexPath.section].options[indexPath.row - 1]
            if filters[indexPath.section].isMultiSelection {
                var selectedOptions = (filters[indexPath.section].selectedOptions as? [String]) ?? [String]()
                if data.isSelected {
                    filters[indexPath.section].options[indexPath.row - 1].isSelected = false
                    selectedOptions.removeAll(where: {
                        $0 == (data.key as! String)
                    })
                }else {
                    filters[indexPath.section].options[indexPath.row - 1].isSelected = true
                    selectedOptions.append(data.key as! String)
                }
                filters[indexPath.section].selectedOptions = selectedOptions
            }else {
                if filters[indexPath.section].category == .NAME_SORTING {
                    filters[indexPath.section].options = getSortByNameOptions()
                }else if filters[indexPath.section].category == .AVAILABILITY {
                    filters[indexPath.section].options = getSortByAvailabilityOptions()
                }
                filters[indexPath.section].options[indexPath.row - 1].isSelected = true
                filters[indexPath.section].selectedOptions = filters[indexPath.section].options[indexPath.row - 1].key
            }
            
            tableView.reloadData()
        }
    }
}
