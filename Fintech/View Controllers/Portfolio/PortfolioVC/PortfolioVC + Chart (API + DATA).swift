//
//  PortfolioVC + Chart (API + DATA_.swift
//  Fintech
//
//  Created by broadstone on 20/03/2022.
//  Copyright © 2022 Broadstone Technologies. All rights reserved.
//

import Foundation
import UIKit
import Charts

extension PortfolioVC: ChartViewDelegate {
    func callNewChartAPI() {
        self.view.activityStartAnimating()
        let url = ApiUrl.GET_PORTFOLIO_CHART_DATA
        let params = ["namespace": SPManager.sharedInstance.getCurrentUserNamespace()] as [String: AnyObject]
        
        APIManager.sharedInstance.postRequest(serviceName: url, sendData: params, success: { (response) in
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
            if APIManager.sharedInstance.isSuccess {
                self.chartDataModel = self.fetchChartData(from: response)
                DispatchQueue.main.async {
                    self.Btns_Day_Week_all_view.isHidden = false
                    self.chartDataNotAvailable.isHidden = true
                    self.loadDayData(self)
                }
            }
            
        }, failure: { (error) in
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
        })
    }
    
    private func fetchChartData(from res: [String: AnyObject]) -> PortfolioChartModel {
        let sucess = res["success"] as! Bool
        let message = res["message"] as! String
        
        let data = res["data"] as! [String: AnyObject]
        let vesgoData = data["vesgo_data"] as! [AnyObject]
        let benchMarkData = data["benchmarkData"] as! [AnyObject]
        
        var vesgo = fetchChartDataPoints(vesgoData)
        var benchMark = fetchChartDataPoints(benchMarkData)
        
        vesgo.sort(by: {
            $0.xAxis < $1.xAxis
        })
        
        benchMark.sort(by: {
            $0.xAxis < $1.xAxis
        })
        
        let chartData = PortfolioChartData.init(vesgoData: vesgo, benchMarkData: benchMark)
        
        let chartModel = PortfolioChartModel.init(success: sucess, message: message, data: chartData)
        
        return chartModel
        
    }
    
    private func fetchChartDataPoints(_ data: [AnyObject]) -> [PortfolioChartDataPoints] {
        var points = [PortfolioChartDataPoints]()
        for i in 0..<data.count {
            let item = data[i] as! [String: AnyObject]
            let xAxis = item["x-axis"] as! Int64
            let yAxis = item["y-axis"] as! Double
            
            points.append(PortfolioChartDataPoints.init(xAxis: xAxis, yAxis: yAxis))
        }
        
        return points
    }
    
    func setChart(values: [PortfolioChartDataPoints], values2:[PortfolioChartDataPoints]) {
        
        lineChartView.highlightPerTapEnabled = true // disable tap gesture to highlight
        lineChartView.highlightPerDragEnabled = true // disable pan gesture
        
        let data = LineChartData()
        var lineChartEntry1 = [ChartDataEntry]()
        var lineChartEntry2 = [ChartDataEntry]()
        for i in 0..<values.count {
            lineChartEntry1.append(ChartDataEntry(x: Double(values[i].xAxis), y: values[i].yAxis))
        }
        let line1 = LineChartDataSet(entries: lineChartEntry1, label: "Vesgo")
        line1.lineWidth=2.0
        line1.colors=[EZLoadingActivity.Settings.vesgoChartLineColor]
        //   line1.circleColors=[UIColor.brown]
        let gradient1 = getGradientFilling(color: #colorLiteral(red: 0.1529411765, green: 0.6549019608, blue: 0.4117647059, alpha: 0.7460669949))
        line1.fill = Fill.fillWithLinearGradient(gradient1, angle: 90.0)
        line1.drawFilledEnabled = true
        line1.drawValuesEnabled = false
        line1.drawCirclesEnabled = false
        line1.drawHorizontalHighlightIndicatorEnabled = false
        //line1.mode = .cubicBezier
        data.addDataSet(line1)
        
        
        for i in 0..<values2.count {
            lineChartEntry2.append(ChartDataEntry(x: Double(values2[i].xAxis), y: values2[i].yAxis))
        }
        let line2 = LineChartDataSet(entries: lineChartEntry2, label: benchMarkTitle)
        //line2.mode = .cubicBezier
        line2.drawFilledEnabled = false
        line2.drawValuesEnabled = false
        line2.drawCirclesEnabled = false
        line2.drawHorizontalHighlightIndicatorEnabled = false
        line2.lineWidth=2.0
        line2.colors = [#colorLiteral(red: 0.5921568627, green: 0.737254902, blue: 0.3843137255, alpha: 1)]
        
        data.addDataSet(line2)
        data.setValueTextColor(NSUIColor.black)
        data.setValueFont(UIFont.init(name: "Verdana", size: 12.0)!)
        self.lineChartView.data=data
        
        //lineChartView.xAxis.valueFormatter = IndexAxisValueFormatter(values:dataPoints)
        lineChartView.xAxis.granularityEnabled = true
        lineChartView.xAxis.labelPosition = .bottom
        
        lineChartView.xAxis.labelTextColor = UIColor.black
        lineChartView.leftAxis.labelTextColor = UIColor.black
        
        lineChartView.xAxis.labelCount = 30
        lineChartView.xAxis.granularity = 1
        lineChartView.leftAxis.enabled = false
        lineChartView.xAxis.enabled = false
        lineChartView.pinchZoomEnabled = true
        lineChartView.scaleYEnabled = true
        lineChartView.scaleXEnabled = true
//        lineChartView.highlighter = ChartHighlighter.init(chart: lineChartView.data)
        lineChartView.legend.horizontalAlignment = Legend.HorizontalAlignment.right
        
        //lineChartView.setVisibleXRangeMaximum(5)
        //lineChartView.setVisibleXRangeMinimum(4)
        
        
        /// lineChartView.setVisibleXRangeMaximum(11)
        
        
        //lineChartView.moveViewToX(90)
        lineChartView.leftAxis.accessibilityScroll(UIAccessibilityScrollDirection(rawValue: Int(20))!)
        lineChartView.doubleTapToZoomEnabled = false
        lineChartView.backgroundColor = UIColor.clear
        lineChartView.chartDescription?.text = ""
        lineChartView.leftAxis.drawGridLinesEnabled = false
        lineChartView.rightAxis.drawGridLinesEnabled = false
        lineChartView.xAxis.drawGridLinesEnabled = false
        //lineChartView.description = "data is being processed"
        
        lineChartView.leftAxis.granularity = 1.0
        
        
        lineChartView.rightAxis.enabled = false
        lineChartView.rightAxis.drawLabelsEnabled = false
        lineChartView.rightAxis.drawAxisLineEnabled = false
        lineChartView.rightAxis.drawGridLinesEnabled = false
        self.lineChartView.xAxis.labelFont = UIFont.init(name: "Verdana", size: 12.0)!
        
        let legend = lineChartView.legend
        legend.font = UIFont(name: "Verdana", size: 13.0)!
        lineChartView.legend.textColor = UIColor.black
        //lineChartView.animate(xAxisDuration: 0.5, yAxisDuration: 1.0, easingOption: .easeInOutQuart)
        lineChartView.drawGridBackgroundEnabled = false
        
        let valFormatter = NumberFormatter()
        valFormatter.numberStyle = .currency
        valFormatter.maximumFractionDigits = 2
        valFormatter.currencySymbol = "$"

        lineChartView.leftAxis.valueFormatter = DefaultAxisValueFormatter(formatter: valFormatter)
        lineChartView.delegate = self
        
    }
    
    
    func getGradientFilling(color:CGColor) -> CGGradient {
        // MARK :-Setting fill gradient color
        let coloTop = UIColor(red: 39/255, green: 167/255, blue: 105/255, alpha: 0.6).cgColor
        let colorMid = UIColor(red: 39/255, green: 167/255, blue: 105/255, alpha: 0.3).cgColor
        let colorBottom = UIColor(red: 39/255, green: 167/255, blue: 105/255, alpha: 0).cgColor
        //MARK :- Colors of the gradient
        let gradientColors = [coloTop, colorMid, colorBottom] as CFArray
        //MARK :- Positioning of the gradient
        let colorLocations: [CGFloat] = [0.7, 0.0, 0.0]
        
        return CGGradient.init(colorsSpace: CGColorSpaceCreateDeviceRGB(), colors: gradientColors, locations: colorLocations)!
        
    }
    
    func chartValueSelected(_ chartView: ChartViewBase, entry: ChartDataEntry, highlight: Highlight) {
        print(entry)
        print(highlight)
        let data = chartView.data as! LineChartData
        let dataSets = data.dataSets
        
        let vesgoData = dataSets[0]
        let benchmarkData = dataSets[1]
        
        var index = vesgoData.entryIndex(entry: entry)
        if index >= 0 {
            //Nothing to do
        }else {
            index = benchmarkData.entryIndex(entry: entry)
        }
        let vesgoText = "Vesgo: $" + FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: vesgoData.entryForIndex(index)!.y)
        let benchmarkText = "S&P 500: $" + FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: benchmarkData.entryForIndex(index)!.y)
        vesgoHighlighterLabel.text = vesgoText + "\n" + benchmarkText
        let timestamp = Int64(vesgoData.entryForIndex(index)!.x)
        dateHighlighterLabel.text = FintechUtils.sharedInstance.getDateInUSLocale(timestamp: timestamp/1000)
        highlighterSV.isHidden = false
    }
    
    
    func chartViewDidEndPanning(_ chartView: ChartViewBase) {
        print("End panning")
        lineChartView.highlightValue(nil)
        highlighterSV.isHidden = true
    }
    
    func get5DayChartData() -> PortfolioChartData{
        let maxItems = 5
        
        var vesgo = [PortfolioChartDataPoints]()
        var benchMark = [PortfolioChartDataPoints]()
        
        for i in 0..<self.chartDataModel.data.vesgoData.count {
            if i < maxItems {
                vesgo.append(self.chartDataModel.data.vesgoData[i])
                benchMark.append(self.chartDataModel.data.benchMarkData[i])
            }
        }
        
        
        let data = PortfolioChartData.init(vesgoData: vesgo, benchMarkData: benchMark)
        return data
    }
    
    func getMonthChartData() -> PortfolioChartData{
        let maxItems = 30
        
        var vesgo = [PortfolioChartDataPoints]()
        var benchMark = [PortfolioChartDataPoints]()
        
        for i in 0..<self.chartDataModel.data.vesgoData.count {
            if i < maxItems {
                vesgo.append(self.chartDataModel.data.vesgoData[i])
                benchMark.append(self.chartDataModel.data.benchMarkData[i])
            }
        }
        
        
        let data = PortfolioChartData.init(vesgoData: vesgo, benchMarkData: benchMark)
        return data
    }
    
    
    func getYearChartData() -> PortfolioChartData{
        let maxItems = 50
        
        var vesgo = [PortfolioChartDataPoints]()
        var benchMark = [PortfolioChartDataPoints]()
        
        if self.chartDataModel.data.vesgoData.count <= maxItems {
            for i in 0..<self.chartDataModel.data.vesgoData.count {
                if i < maxItems {
                    vesgo.append(self.chartDataModel.data.vesgoData[i])
                    benchMark.append(self.chartDataModel.data.benchMarkData[i])
                }
            }
        }else if self.chartDataModel.data.vesgoData.count <= (maxItems*2) {
            var index = 0
            for i in 0..<self.chartDataModel.data.vesgoData.count {
                if index < maxItems && i%2 == 0 {
                    vesgo.append(self.chartDataModel.data.vesgoData[i])
                    benchMark.append(self.chartDataModel.data.benchMarkData[i])
                    index += 1
                }
            }
        }
        else if self.chartDataModel.data.vesgoData.count <= (maxItems*3) {
            var index = 0
            for i in 0..<self.chartDataModel.data.vesgoData.count {
                if index < maxItems && i%3 == 0 {
                    vesgo.append(self.chartDataModel.data.vesgoData[i])
                    benchMark.append(self.chartDataModel.data.benchMarkData[i])
                    index += 1
                }
            }
        }
        else if self.chartDataModel.data.vesgoData.count <= (maxItems*4) {
            var index = 0
            for i in 0..<self.chartDataModel.data.vesgoData.count {
                if index < maxItems && i%4 == 0 {
                    vesgo.append(self.chartDataModel.data.vesgoData[i])
                    benchMark.append(self.chartDataModel.data.benchMarkData[i])
                    index += 1
                }
            }
        }
        else {
            var index = 0
            for i in 0..<self.chartDataModel.data.vesgoData.count {
                if index < maxItems && i%5 == 0 {
                    vesgo.append(self.chartDataModel.data.vesgoData[i])
                    benchMark.append(self.chartDataModel.data.benchMarkData[i])
                    index += 1
                }
            }
        }
        
        let data = PortfolioChartData.init(vesgoData: vesgo, benchMarkData: benchMark)
        return data
    }
    
    func getMaxChartData() -> PortfolioChartData{
        let maxItems = 60
        
        var vesgo = [PortfolioChartDataPoints]()
        var benchMark = [PortfolioChartDataPoints]()
        
        if self.chartDataModel.data.vesgoData.count <= maxItems {
            for i in 0..<self.chartDataModel.data.vesgoData.count {
                if i < maxItems {
                    vesgo.append(self.chartDataModel.data.vesgoData[i])
                    benchMark.append(self.chartDataModel.data.benchMarkData[i])
                }
            }
        }else if self.chartDataModel.data.vesgoData.count <= (maxItems*2) {
            var index = 0
            for i in 0..<self.chartDataModel.data.vesgoData.count {
                if index < maxItems && i%2 == 0 {
                    vesgo.append(self.chartDataModel.data.vesgoData[i])
                    benchMark.append(self.chartDataModel.data.benchMarkData[i])
                    index += 1
                }
            }
        }
        else if self.chartDataModel.data.vesgoData.count <= (maxItems*3) {
            var index = 0
            for i in 0..<self.chartDataModel.data.vesgoData.count {
                if index < maxItems && i%3 == 0 {
                    vesgo.append(self.chartDataModel.data.vesgoData[i])
                    benchMark.append(self.chartDataModel.data.benchMarkData[i])
                    index += 1
                }
            }
        }
        else if self.chartDataModel.data.vesgoData.count <= (maxItems*4) {
            var index = 0
            for i in 0..<self.chartDataModel.data.vesgoData.count {
                if index < maxItems && i%4 == 0 {
                    vesgo.append(self.chartDataModel.data.vesgoData[i])
                    benchMark.append(self.chartDataModel.data.benchMarkData[i])
                    index += 1
                }
            }
        }
        else {
            var index = 0
            for i in 0..<self.chartDataModel.data.vesgoData.count {
                if index < maxItems && i%5 == 0 {
                    vesgo.append(self.chartDataModel.data.vesgoData[i])
                    benchMark.append(self.chartDataModel.data.benchMarkData[i])
                    index += 1
                }
            }
        }
        
        let data = PortfolioChartData.init(vesgoData: vesgo, benchMarkData: benchMark)
        return data
    }
}
