//
//  EduSectionDetailsVC.swift
//  Fintech
//
//  Created by MacBook-Mubashar on 26/09/2019.
//  Copyright © 2019 Broadstone Technologies. All rights reserved.
//
////#################################################################
//  In This view show all education data and take quiz and move next page cards and quiz
//  Take Quiz
//  calculate  presentage
//################################################################

import UIKit
import Lottie
var medal = Int()
var countprenstage = Int()
var selectedIndex = 0

class EduSectionDetailsVC: Parent {
    
    @IBAction func share_btn(_ sender: Any) {
        let text = "I just finished learning about " + titleString + ". You can learn about " + titleString
            + " and many other financial literacy topics on Vesgo.\n" + ConstantStrings.APP_LINK
        shareTextualData(textData: text)
    }
    @IBOutlet weak var animation_sharebtn_view: AnimationView!
    @IBOutlet weak var Animationtrophy_view: AnimationView!
    @IBOutlet weak var Title_lbl: UILabel!
    @IBOutlet weak var counter: UILabel!
    @IBOutlet weak var badge_animation_view: AnimationView!
    @IBOutlet weak var percentage: UILabel!

    @IBOutlet var table_view: UITableView!
    var data = [EducationDetailModel]()
    var mainQuestion = Int()
    var innnerQuestion = String()
    var lastQuestion = Int()
    var db = Databasehandler()
    @IBAction func Back_btn(_ sender: Any) {
    }
    @IBAction func back_btn(_ sender: Any) {
     navigationController?.popViewController(animated: true)
    }
    var titleString : String = ""
    var additionalInfo = Int()
    var listAdditioanlInfo : [LibraryDataModel] = [LibraryDataModel]()
    var doneArray:[Int] = []
    var user = ""
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.isNavigationBarHidden = true
        Title_lbl.text = titleString
        initTableViews()
        user = SPManager.sharedInstance.getCurrentUserEmail()
              if user == "" {
                //test
                  user = "test"
              }
        data = db.readEducationDETAIL(id: additionalInfo)
        doneArray = db.getQuestion(user: user, mainQ: additionalInfo)
        if doneArray.count > 0{
        selectedIndex = doneArray[(doneArray.count-1)] + 1
        }else{
            selectedIndex = 0
        }
        DispatchQueue.main.async {
            
            self.table_view.reloadData()
        }
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        db = Databasehandler()

        user = SPManager.sharedInstance.getCurrentUserEmail()
        self.badge_animation_view.contentMode = .scaleAspectFit
        self.badge_animation_view.animationSpeed = 0.5
        self.badge_animation_view.play()
                  if user == "" {
                    //test
                      user = "test"
                  }
        doneArray = db.getQuestion(user: user, mainQ: additionalInfo)
               if doneArray.count > 0{
               selectedIndex = doneArray[(doneArray.count-1)] + 1

               }
        let done = Float(self.doneArray.count)
        UserDefaults.standard.set(done, forKey: "done")
        
        let total = Float(self.data.count)
        let perc = (done*100)/total
        print(perc)
        DispatchQueue.main.async {
            self.counter.text = "\(self.doneArray.count) " + getLocalizedString(key: "of_", comment: "") + " \(self.data.count)"
            medal = (self.doneArray.count)
            let done = Float(self.doneArray.count)
            let total = Float(self.data.count)
            let percent = (done*100)/total
            self.percentage.text = "\(Int(percent))%"
            var pre = percent
            self.table_view.reloadData()
        }
     countprenstage = Int(perc)
     UserDefaults.standard.set(countprenstage, forKey: "countprenstage")
      print("aqib\(countprenstage)")
        if countprenstage == 100 {
            let tablel = self.Animationtrophy_view
            tablel!.heightConstaint?.constant = 200
            self.Animationtrophy_view.contentMode = .scaleAspectFit
            self.Animationtrophy_view.loopMode = .loop
            self.Animationtrophy_view.animationSpeed = 0.5
            self.Animationtrophy_view.play()
            self.animation_sharebtn_view.contentMode = .scaleAspectFit
            self.animation_sharebtn_view.animationSpeed = 0.5
            self.animation_sharebtn_view.play()

        }
    }
        //     MARK: - Class Functions
    func initTableViews() {
        self.table_view.delegate = self
        self.table_view.dataSource = self
        self.table_view.rowHeight = UITableView.automaticDimension
        self.table_view.estimatedRowHeight = 300
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    
}

// MARK: - Ext. TableView Delegates
extension EduSectionDetailsVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        print("SelectedIndex: \(selectedIndex)")
        if indexPath.row == selectedIndex{
            let cell =  tableView.dequeueReusableCell(withIdentifier:  "EduStartCell") as! EduStartCell
             let item = data[indexPath.row]
            cell.heading.text = "Lesson"
            cell.question.text = item.infoHeading
            //cell.circleImage.image =
            cell.startBtn.setTitle(getLocalizedString(key: "Start learning", comment: ""), for: .normal)
            cell.startBtn.tag = indexPath.row
            cell.startBtn.addTarget(self, action: #selector(self.pressStartButton(_:)), for: .touchUpInside) //<- use `#selector(...)`

            if indexPath.row == (data.count-1){
                cell.lineView.isHidden = true
                
            }else{
                cell.lineView.isHidden = false

            }
            return cell
        }else{
            let cell =  tableView.dequeueReusableCell(withIdentifier:  "EduQuestionCell") as! EduQuestionCell
             let item = data[indexPath.row]
            //cell.heading.text = "Lesson"
            cell.question.text = item.infoHeading
            //cell.circleImage.image =

            if indexPath.row == (data.count-1) && item.infoSubHeading == "" {
                cell.lineView.isHidden = true
                cell.circleImage.image = UIImage(named: "questionMark")

                cell.heading.text = "Quiz"

                
            }else{
                cell.circleImage.image = UIImage(named: "ic_lesson")
                cell.lineView.isHidden = false
                cell.heading.text = "Lesson"


            }
            print(indexPath.row)
            if doneArray.contains(indexPath.row)
            {
                cell.circleImage.image = #imageLiteral(resourceName: "icons8-checkmark-50")
                cell.circleView.backgroundColor = #colorLiteral(red: 0.07450980392, green: 0.7098039216, blue: 0.4901960784, alpha: 1)
                cell.lineView.backgroundColor = #colorLiteral(red: 0.07450980392, green: 0.7098039216, blue: 0.4901960784, alpha: 1)
            }else{
               // cell.circleImage.image =
               // cell.circleImage.image = UIImage(named: "ic_lesson")
                cell.circleView.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
                cell.lineView.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
            }
            return cell
        }
    }
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    
            return UITableView.automaticDimension
        }
    @objc func pressStartButton(_ sender: UIButton){
        print("\(sender)")
        selectedIndex += 1
        if selectedIndex == data.count && data[sender.tag].infoSubHeading == "" {
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                 let vc = storyBoard.instantiateViewController(withIdentifier: "Answer_views") as! Answer_views
                 vc.index = sender.tag
            vc.mainQuestion = additionalInfo
            vc.topicID = additionalInfo
            vc.topicName = titleString
            vc.lastQuestion = sender.tag
            vc.innnerQuestion = data[sender.tag].infoSubHeading
            vc.doneFlag = false
            

                 vc.modalTransitionStyle = .partialCurl
                 navigationController?.pushViewController(vc, animated: true)
        }else{
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "CardsViewController") as! CardsViewController
            vc.index = sender.tag
            vc.lessonTitle = data[sender.tag].infoHeading
            vc.answerId = data[sender.tag].infoSubHeading
            vc.mainQuestion = additionalInfo
            vc.lastQuestion = sender.tag
            vc.innnerQuestion = data[sender.tag].infoSubHeading
            vc.doneFlag = false
            vc.modalTransitionStyle = .partialCurl
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if doneArray.contains(indexPath.row){
            if indexPath.row == (data.count-1) && data[indexPath.row].infoSubHeading == "" {
                print("244: If Statement")
                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                let vc = storyBoard.instantiateViewController(withIdentifier: "Answer_views") as! Answer_views
                // vc.delegate = self
                vc.index = indexPath.row
                vc.mainQuestion = additionalInfo
                vc.topicID = additionalInfo
                vc.lastQuestion = indexPath.row
                vc.innnerQuestion = data[indexPath.row].infoSubHeading
                vc.doneFlag = true
                vc.modalTransitionStyle = .partialCurl
                navigationController?.pushViewController(vc, animated: true)
            }else{
                print("244: Else Statement")
                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                let vc = storyBoard.instantiateViewController(withIdentifier: "CardsViewController") as! CardsViewController
               
                //  vc.delegate = self
                vc.index = indexPath.row
                vc.lessonTitle = data[indexPath.row].infoHeading
                vc.answerId = data[indexPath.row].infoSubHeading
                vc.mainQuestion = additionalInfo
                vc.lastQuestion = indexPath.row
                vc.innnerQuestion = data[indexPath.row].infoSubHeading
                vc.doneFlag = true
                vc.modalTransitionStyle = .partialCurl
                navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
}

