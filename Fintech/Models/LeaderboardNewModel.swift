//
//  LeaderboardNewModel.swift
//  Fintech
//
//  Created by broadstone on 18/06/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import Foundation

enum SelectedSegmentForLeaderboard {
    case PAST_MONTH
    case PAST_YEAR
    case PAST_3YEAR
}

enum SelectedLeaderboardType {
    case ALL
    case MY_AGE_GROUP
}

struct LeaderboardModel {
    var success: Bool!
    var message: String!
    var data: LeaderboardData!
}

struct LeaderboardData {
    var pastMonth: [LeaderboardUsersList]!
    var pastYear: [LeaderboardUsersList]!
    var past3Year: [LeaderboardUsersList]!
}

struct LeaderboardUsersList {
    var portfolioId: Int!
    var rank: Int!
    var name: String!
    var portfolioReturn: String!
    var riskScore: Double!
    var assets: [LeaderboardAssets]!
    var positiveValues: [String]!
    var negativeValues: [String]!
    var portfolioComposition: UserPortfolioComposition!
}

struct LeaderboardAssets {
    var symbol: String!
    var symbolName: String!
    var shareAmount: Double!
    var sharePercent: Double!
}

struct UserPortfolioComposition {
    var impact: String!
    var zScore: Double!
    var controversy: ControversyData!
    var environment: NewEsgScoreModel!
    var socialScore: NewEsgScoreModel!
    var governanceScore: NewEsgScoreModel!
}
