//
//  Profile VC.swift
//  Fintech
//
//  Created by Apple on 01/04/2020.
//  Copyright © 2020 Broadstone Technologies. All rights reserved.
//#################################################################
//  In This view USer Personal Details
//    Username_lbl Show user Email
//   user complete Investment strategy then show user Summary
//    user change password and signout
//#################################################################
import UIKit


class Profile_VC:UIViewController {
    // MArk: - IB outlets
    @IBOutlet weak var Username_lbl: UILabel!
    @IBOutlet weak var PhoneNo_lbl: UILabel!
    @IBOutlet weak var TestAcount_lbl: UILabel!
    @IBOutlet weak var changepassword_lbl: UIButton!
    @IBOutlet weak var Signout_lbl: UIButton!
    @IBOutlet weak var personnel_btn: UIButton!
    @IBOutlet weak var investissement_btn: UIButton!
    @IBOutlet weak var valeurs_btn: UIButton!
    @IBOutlet weak var risque_btn: UIButton!
    @IBOutlet weak var summaryButton_view: UIView!
    @IBOutlet weak var SummaryText_lbl: UIView!
    @IBOutlet weak var ValuebtnStack_btn: UIStackView!
    @IBOutlet weak var tableview: UITableView!
    
    //Mark: - class variables data summary view
    var personalList = [summary]()
    var investemenList = [summary]()
    var valuesList = [summary]()
    var riskList = [summary]()
    var tableData = [summary]()
    var data = [summary]()
    
    
    @IBAction func Changepassword_btn(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ResetPasswordVC") as! ChangePasswordVC
        UIApplication.shared.keyWindow?.rootViewController = vc

       // navigationController?.pushViewController(vc, animated: true)
    }
    //Mark :- Actions button summary view
    @IBAction func personal_btnn(_ sender: Any) {
        
        tableData = personalList
        tableview.reloadData()
        tableview.isHidden = false
        let tablel = tableview
        tablel!.heightConstaint?.constant = 250
    }
    @IBAction func invesment_btn(_ sender: Any) {
        tableData = investemenList
        tableview.reloadData()
        tableview.isHidden = false
        let tablel = tableview
      //  tablel!.heightConstaint?.constant = 250
    }
    @IBAction func valures_btn(_ sender: Any) {
        tableData = valuesList
        tableview.reloadData()
        tableview.isHidden = false
        let tablel = tableview
       // tablel!.heightConstaint?.constant = 130
    }
    @IBAction func risuq_btn(_ sender: Any) {
        tableData = riskList
        tableview.reloadData()
        tableview.isHidden = false
        let tablel = tableview
       // tablel!.heightConstaint?.constant = 300
    }
    
    @IBAction func Signout_btn(_ sender: Any) {
        SPManager.sharedInstance.clearUserAppSettings()
        SPManager.sharedInstance.clearUserData()
        SPManager.sharedInstance.saveUserLoginStatus(isLogin: false)
        SPManager.sharedInstance.saveUserPortfolioStatus(isPortfolioComplete: false)
        
        //   self.present(vc, animated:true, completion:nil)
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "LogInVC") as UIViewController
        UIApplication.shared.keyWindow?.rootViewController = vc
    }
    
    // MARK: - Life Cycle
    override func viewDidAppear(_ animated: Bool) {
        localization()
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        if getUserData(key: SPManager.sharedInstance.getCurrentUserEmail()) == nil{
            ValuebtnStack_btn.isHidden = true
        }else{
            SummaryText_lbl.isHidden = true
            ValuebtnStack_btn.isHidden = false
        }
        changepassword_lbl.layer.cornerRadius = 20
        Signout_lbl.layer.cornerRadius = 20
        summaryButton_view.layer.cornerRadius = 10
        tableview.isHidden = true
        Username_lbl.text = SPManager.sharedInstance.getCurrentUserEmail()
        TestAcount_lbl.text = SPManager.sharedInstance.getCurrentUserNameForProfile()
       // print(testusername)
        // Do any additional setup after loading the view.
        data = self.getUserData(key: SPManager.sharedInstance.getCurrentUserEmail()) ?? [summary]()
        for sum in data{
            if sum.catogries == 1 {
                personalList.append(sum)
            }else
                if sum.catogries == 2 {
                    investemenList.append(sum)
                }else
                    if sum.catogries == 3 {
                        valuesList.append(sum)
                    }else
                        if sum.catogries == 4 {
                            riskList.append(sum)
            }
        }
        tableData = personalList
        tableview.reloadData()
    }
    
    func localization(){
        
        changepassword_lbl.setTitle(getLocalizedString(key: "Chnage password", comment: ""), for: .normal)
        Signout_lbl.setTitle(getLocalizedString(key: "SignOut", comment: ""), for: .normal)
        //TestAcount_lbl.text = getLocalizedString(key: "TestAccount", comment: "")
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    
    
    
    // Mark :- api call
    func getAccountData() {
        
        self.view.activityStartAnimating()
        let url = ApiUrl.GET_ACCOUNT_SETTINGS
        let namespace = SPManager.sharedInstance.getCurrentUserNamespace()
        let params:Dictionary<String,AnyObject> = ["namespace" : namespace as AnyObject ]
        print(params)
        APIManager.sharedInstance.postRequest(serviceName: url, sendData: params, success: { (response) in
            print(response)
            
            if APIManager.sharedInstance.isSuccess {
                let res = response["message"] as AnyObject
                DispatchQueue.main.async {
                    print("response message is \(res)")
                    AccountSettings.sharedInstance.FirstName = res["first_name"] as! String
                    AccountSettings.sharedInstance.LastName = res["last_name"] as! String
                    AccountSettings.sharedInstance.Email = res["email"] as! String
                    AccountSettings.sharedInstance.JoinedDate = res["joined_timestamp"] as! Int
                    AccountSettings.sharedInstance.LanguageSelected = res["language"] as! String
                    AccountSettings.sharedInstance.RiskResultSummary = res["risk_result_summary"] as! String
                    AccountSettings.sharedInstance.Notifications = res["notifications"] as! String
                    AccountSettings.sharedInstance.ThemeSelected = res["theme"] as! String
                    AccountSettings.sharedInstance.QuestionnaireStatus = res["questionnaire_status"] as! String
                    AccountSettings.sharedInstance.QuestionnaireExpiry = res["questionnaire_expiry"] as! String
                    
                }
                
            }else{
                if response["message"] != nil {
                    // self.showAlert(response["message"] as! String)
                } else {
                    // self.showAlert(ConstantStrings.NO_RESULT_FOUND_ALERT_TEXT)
                }
            }
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
        }, failure: { (data) in
            print(data)
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
            if APIManager.sharedInstance.error400 {
                // self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
            }
            if(APIManager.sharedInstance.error401)
            {
                // self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
            }else{
                
            }
        })
        
    }
    
    
}

extension Profile_VC:UITableViewDelegate,UITableViewDataSource{
    // MARK: - TableView Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return tableData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! profilesummaryVC
        //cell.Answer_lbl.text!.count
        cell.Question_lbl.text =  getLocalizedString(key: tableData[indexPath.row].questiontext!, comment: "")
        cell.Answer_lbl.text =  getLocalizedString(key: tableData[indexPath.row].answertext!, comment: "")
        return cell
    }
}


//Mark :- Summary Tableview hight control
//extension UIView {
//    var heightConstaint: NSLayoutConstraint? {
//        get {
//            return constraints.first(where: {
//                $0.firstAttribute == .height && $0.relation == .equal
//            })
//        }
//        set { setNeedsLayout() }
//    }
//    var widthConstaint: NSLayoutConstraint? {
//        get {            return constraints.first(where: {
//            $0.firstAttribute == .width && $0.relation == .equal
//        })
//        }
//        set { setNeedsLayout() }
//    }
//}
