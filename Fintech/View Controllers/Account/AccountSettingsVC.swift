//
//  AccountSettingsVC.swift
//  Fintech
//
//  Created by MacBook-Mubashar on 30/08/2019.
//  Copyright © 2019 Broadstone Technologies. All rights reserved.
//

import UIKit

class AccountSettingsVC: Parent {
    
    //MARK: - IB outlets
    //views
    @IBOutlet weak var updatePortfolio_view: UIView!
    @IBOutlet weak var changePassword_view: UIView!
    @IBOutlet weak var pushNotifications_view: UIView!
    @IBOutlet weak var languages_view: UIView!
    @IBOutlet weak var themeSelection_view: UIView!
    
    @IBOutlet weak var updatePortfolio_btn: UIButton!
    @IBOutlet weak var potfiolioInfoText_lbl: UILabel!
    @IBOutlet var changePassword_btn: UIButton!
    @IBOutlet var pushNotifications_btn: UIButton!
    @IBOutlet var pushNotificationInfoText_lbl: UILabel!
    @IBOutlet var pushNotifications_switch: CustomSwitchButton!
    @IBOutlet var languageSelection_btn: UIButton!
    @IBOutlet var languageInfoText_lbl: UILabel!
    @IBOutlet var themeSelection_btn: UIButton!
    @IBOutlet var themeInfoText_lbl: UILabel!
    @IBOutlet var themeSelection_switch: CustomSwitchButton!
    // MARK: - Calss Variables
    var buttonn = UIButton()
    var buttonn1 = UIButton()
    var buttonn2 = UIButton()
    
    var label = UILabel()
    var label1 = UILabel()
    var label2 = UILabel()
    
    var juiceBool = Bool()
    var FoodBool = Bool()
    var FurnitureBool = Bool()
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpInfoLabels()
        setUpButtonsAction()
        
        juiceBool = false
        FurnitureBool = false
        FoodBool = false
        
        buttonn = UIButton(frame: CGRect(x: 20, y: 30, width: 200, height: 40))
        buttonn.backgroundColor = .clear
        buttonn.tag = 1
        buttonn.setImage(#imageLiteral(resourceName: "radio_off_button"), for: UIControl.State.normal)
        buttonn.setImage(#imageLiteral(resourceName: "radio_on_button"), for: UIControl.State.selected)
        buttonn.setTitle(" English (US)", for: .normal)
        buttonn.setTitleColor(UIColor.black, for: .normal)
        buttonn.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.left
        buttonn.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        buttonn1 = UIButton(frame: CGRect(x: 20, y: 60, width: 200, height: 40))
        buttonn1.backgroundColor = .clear
        buttonn1.tag = 2
        buttonn1.setImage(#imageLiteral(resourceName: "radio_off_button"), for: UIControl.State.normal)
        buttonn1.setImage(#imageLiteral(resourceName: "radio_on_button"), for: UIControl.State.selected)
        buttonn1.setTitle(" Deutsch)", for: .normal)
        buttonn1.setTitleColor(UIColor.black, for: .normal)
        buttonn1.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.left
        buttonn1.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        
        
        buttonn2 = UIButton(frame: CGRect(x: 20, y: 90, width: 200, height: 40))
        buttonn2.backgroundColor = .clear
        buttonn2.tag = 3
        buttonn2.setImage(#imageLiteral(resourceName: "radio_off_button"), for: UIControl.State.normal)
        buttonn2.setImage(#imageLiteral(resourceName: "radio_on_button"), for: UIControl.State.selected)
        buttonn2.setTitle(" French", for: .normal)
        buttonn2.setTitleColor(UIColor.black, for: .normal)
        buttonn2.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.left
        buttonn2.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        showAleert()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setNavBarTitile(title: "", topItem: "Account Settings")
    }
    //MARK: - Swich Actions
    
    func setUpInfoLabels() {
        potfiolioInfoText_lbl.text = "portfolio info label"
        pushNotificationInfoText_lbl.text = "push notifications info label"
        languageInfoText_lbl.text = "language info label"
        themeInfoText_lbl.text = "Theme selection"
    }
    
    func setUpButtonsAction() {
        
    }
    
    //MARK: - Swich Actions
    
    @IBAction func NotificationSwitch_pressed(sender: CustomSwitchButton) {
        print("notification switch value changed")
        
        setAnalyticsAction(ScreenName: "AccountSettings", method: "NotificationSwitchTrigerred")
    }
    
    @IBAction func themeSwitch_pressed(sender: AnyObject) {
        print("Theme switch value changed")
        if themeSelection_switch.isOn {
        } else {
        }
    }
    
    
    func showAleert(){
        let alertController = UIAlertController(title: "Select Lanaguage", message: "", preferredStyle: UIAlertController.Style.actionSheet) //if you increase actionSheet height to move default button down increase \n \n count and get your layout proper
        
        
        let margin:CGFloat = 0.0
        let rect = CGRect(x: margin, y: margin, width: alertController.view.bounds.size.width - margin * 4.0, height: 200)
        let customView = UIView(frame: rect)
        
        customView.backgroundColor = .clear //Background colour as clear
        
        customView.addSubview(buttonn)
        customView.addSubview(buttonn1)
        customView.addSubview(buttonn2)
        
        alertController.view.addSubview(customView)
        let ExitAction = UIAlertAction(title: "Done", style: .default, handler: {(alert: UIAlertAction!) in
            
            if self.juiceBool == true {
                //go to juice Controller
                print("juice")
            }
            if self.FoodBool == true{
                //go to food controller
                print("food")
            }
            if self.FurnitureBool == true{
                //go to furniture controller
                print("furniture")
            }
            if (self.juiceBool == false) && (self.FurnitureBool == false) && (self.FoodBool == false){
                //peform your operation like naything you want safety side
                print("Not selected Any")
            }
            
            
        })
        
        let height:NSLayoutConstraint = NSLayoutConstraint(item: alertController.view, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: self.view.frame.height * 0.35) //here you can define new height for action Sheet
        alertController.view.addConstraint(height);
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {(alert: UIAlertAction!) in print("cancel")})
        alertController.addAction(ExitAction)
        alertController.addAction(cancelAction)
        DispatchQueue.main.async {
            self.present(alertController, animated: true, completion:{})
        }
    }
    
    @objc func buttonAction(sender: UIButton!) {
        print("Button tapped at \(sender.tag)")
        
        switch sender.tag {
        case 1:
            
            if buttonn.isSelected == true{
                buttonn.isSelected = false
                juiceBool = false
                FoodBool = false
                FurnitureBool = false
            }
            else{
                buttonn.isSelected = true
                buttonn1.isSelected = false
                buttonn2.isSelected = false
                juiceBool = true
                FoodBool = false
                FurnitureBool = false
            }
            
            break
            
        case 2:
            
            if buttonn1.isSelected == true{
                buttonn1.isSelected = false
                FoodBool = false
                FurnitureBool = false
                juiceBool = false
            }
            else{
                buttonn1.isSelected = true
                buttonn.isSelected = false
                buttonn2.isSelected = false
                juiceBool = false
                FoodBool = true
                FurnitureBool = false
            }
            
            break
            
        case 3:
            
            if buttonn2.isSelected == true{
                buttonn2.isSelected = false
                FurnitureBool = false
                juiceBool = false
                FoodBool = false
            }
            else{
                buttonn2.isSelected = true
                buttonn1.isSelected = false
                buttonn.isSelected = false
                juiceBool = false
                FoodBool = false
                FurnitureBool = true
            }
            
            break
            
        default:
            break
        }
        
        
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
