//
//  Cell1TableViewCell.swift
//  Vesgo-Search1
//
//  Created by Billal on 03/02/2021.
//

import UIKit

class Cell1: UITableViewCell {
    
//MARK: - Outlets
    @IBOutlet weak var imageCell: UIImageView!
    @IBOutlet weak var data1Lbl: UILabel!
    @IBOutlet weak var data2Lbl: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
