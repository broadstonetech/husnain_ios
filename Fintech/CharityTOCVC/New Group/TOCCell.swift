//
//  TOCCell.swift
//  Fintech
//
//  Created by Apple on 18/12/2020.
//  Copyright © 2020 Broadstone Technologies. All rights reserved.
//

import UIKit

class TOCCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBOutlet weak var heading_lbl: UILabel!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
