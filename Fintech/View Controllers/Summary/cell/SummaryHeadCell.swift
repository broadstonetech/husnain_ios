//
//  SummaryHeadCell.swift
//  Fintech
//
//  Created by apple on 17/05/2020.
//  Copyright © 2020 Broadstone Technologies. All rights reserved.
//

import UIKit

class SummaryHeadCell: UITableViewCell {
    @IBOutlet weak var name_lbl: UILabel!
    @IBOutlet weak var backView :UIView!
    @IBOutlet weak var innerView :UIView!
    @IBOutlet weak var stackView: UIStackView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setColor (color: UIColor){
        innerView.backgroundColor = color
        backView.borderColor = color
    }
}
