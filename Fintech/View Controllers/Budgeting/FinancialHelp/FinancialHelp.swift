//
//  FinancialHelp.swift
//  Fintech
//
//  Created by broadstone on 16/02/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import UIKit

class FinancialHelp: BudgetingParent {
    
    var index = 0
    var financialHelpModel: TipAndAdviceModel!
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 200
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        setNavBarTitile(title: "Insights", topItem: "Insights")
    }
    
}
extension FinancialHelp: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1 + financialHelpModel.data.categoryData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PlanInfoCell") as! FinancialHelpPlanInfoCell
            
            cell.numberOfDaysLbl.text = "30"
            cell.amountSpentLabel.text = "$" + FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: financialHelpModel.data.totalSpending)
            
            let peerSpending = financialHelpModel.data.totalSpending -  (financialHelpModel.data.totalSpending * (financialHelpModel.data.totalDifference / 100.0))
            cell.peerSpentLabel.text = "$" + FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: peerSpending)
            
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryRowCell") as! FinancialHelpCategoryCell
        
            let item = financialHelpModel.data.categoryData[indexPath.row - 1]
            cell.amountSpentLabel.text = "$" + FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: item.totalSpent)
            let peerSpent = item.totalSpent - (item.totalSpent * (item.difference / 100.0))
            cell.peerSpentLabel.text = "$" + FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: peerSpent)
            cell.categoryLabel.text = item.categoryName
            
            cell.imgBgView.backgroundColor = FintechUtils.sharedInstance.getColoredBackgroundForBudgeting(rowPosition: indexPath.row)
            cell.imgView.image = FintechUtils.sharedInstance.getCaetgoryImageForBudgeting(value: item.categoryName.lowercased())
            
            
            
            if item.difference >= 0 {
                cell.diffLabel.text = "+" + FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: item.difference) + "%"
                cell.diffImage.image = UIImage(named: "unhappy")
            }else {
                cell.diffLabel.text =  FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: item.difference) + "%"
                cell.diffImage.image = UIImage(named: "cheerful")
            }
            
            
            return cell
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
extension FinancialHelp {
    
}
