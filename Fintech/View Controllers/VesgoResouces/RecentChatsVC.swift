//
//  RecentChatsVC.swift
//  Fintech
//
//  Created by broadstone on 10/01/2022.
//  Copyright © 2022 Broadstone Technologies. All rights reserved.
//

import UIKit
import SwiftyJSON
import Kingfisher
class RecentChatsVC: ResourcesParent,UITableViewDelegate,UITableViewDataSource {
    
    //let searchController = UISearchController(searchResultsController: nil)


    
    @IBOutlet weak var recentChatTableView: UITableView!
    @IBOutlet weak var dataNotAvailableView: UIView!
    @IBOutlet weak var searchBar: UISearchBar!
    

    var cellAnimate = true
    private var recentChatModel: RecentChatModel!
    var recentChat = [RecentChat]()
    var filterRecentChat = [RecentChat]()
    var filter = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.chatHistoryApiDelegate = self
        
        dataNotAvailableView.isHidden = true
        
        self.uiserachBar()
        self.recentChatTableView.delegate = self
        self.recentChatTableView.dataSource = self
       
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        setNavBarTitile(title: "Back", topItem: getLocalizedString(key: "_chats"))
        
        self.getRecentChatApiData()
    }
    
    @IBAction func browseAdvisorBtnPressed(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "FinancialAdvisorsVC") as! FinancialAdvisorsVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
 
    // MARK :- SET SEARCH BAT PARAMETERS
    func uiserachBar(){
        searchBar.delegate = self
        //searchController.searchResultsUpdater = self
        //searchController.obscuresBackgroundDuringPresentation = true
        //searchController.searchBar.placeholder = "Search recent chats"
        //navigationItem.searchController = searchController
       
        //definesPresentationContext = true

    }

    //MARK: - API RESPONSE METHOD
    func getRecentChatApiData(){
        recentChat.removeAll()
        self.view.activityStartAnimating()
        
        let url = ApiUrl.GET_RECENT_CHAT_WITH_ADVISOR
        
        
        let params = ["namespace": SPManager.sharedInstance.getCurrentUserNamespace()] as [String: AnyObject]
        
        APIManager.sharedInstance.postRequest(serviceName: url, sendData: params) { (response) in
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
            if APIManager.sharedInstance.isSuccess{
                DispatchQueue.main.async { [self] in
                    fetchAllChatData(res: response)
                }
            }else {
                DispatchQueue.main.async {
                    self.dataNotAvailableView.isHidden = false
                }
            }
        } failure: { data in
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
            print("Failed to hit allAdvApi")
            DispatchQueue.main.async {
                self.dataNotAvailableView.isHidden = false
            }
        }
        
        
        
        
}
    
    private func fetchAllChatData(res: [String: AnyObject]) {
       // print("json",res)
        let success = res["status"] as! Bool
        let message = res["message"] as! String
        let data = res["data"] as! [NSDictionary]
     
        //print(data)
        
        if !success {
            dataNotAvailableView.isHidden = false
            return
        }
        recentChat.removeAll()
        dataNotAvailableView.isHidden = true
        for i in data{
            let profile = i
            ///print(profile)
            let advisor_namespace = profile["advisor_namespace"] as! String
            let name = profile["name"] as! String
            let profile_pic = profile["profile_pic"] as! String
            let uniqueId = profile["chat_unique_id"] as! String
            let lastMessage = profile["last_message"] as! [String: AnyObject]
            
            let senderNamespace = (lastMessage["sender_namespace"] as? String) ?? ""
            let timestamp = (lastMessage["timestamp"] as? Int64) ?? 0
            let message = (lastMessage["message"] as? String) ?? ""
            
            let lastMessageData = RecentChatLastMessage.init(message: message, senderNamespace: senderNamespace, timestamp: timestamp)
            
            recentChat.append(RecentChat.init(advisor_namespace: advisor_namespace, chatUniqueId: uniqueId, lastMessage: lastMessageData, name: name, profile_pic: profile_pic))
        }
        DispatchQueue.main.async{
            self.recentChat.sort(by: {
                $0.lastMessage.timestamp > $1.lastMessage.timestamp
            })
            self.recentChatTableView.reloadData()
            
        }
     
        recentChatModel = RecentChatModel.init(success: success, message: message , data: recentChat)
   
   }
}

//MARK: - TABLE VIEW EXTENSION

extension RecentChatsVC {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if filter == false{
  
            return recentChat.count
        }else{
   
            return filterRecentChat.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        if filter == false{
     
            let cell = recentChatTableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! RecentChatCell
            cell.cellImg.layer.cornerRadius = cell.cellImg.frame.height/2
            cell.bgView.layer.shadowColor = UIColor.lightGray.cgColor
                  cell.bgView.layer.shadowRadius = 4
                  cell.bgView.layer.shadowOpacity = 2
                  cell.bgView.layer.shadowOffset = CGSize(width: 0, height: 0)
                 cell.bgView.layer.cornerRadius = 5
            cell.nameLbl.text = recentChat[indexPath.row].name
            cell.lastTextLbl.text = recentChat[indexPath.row].lastMessage.message
            
            if recentChat[indexPath.row].lastMessage.senderNamespace == SPManager.sharedInstance.getCurrentUserNamespace() {
                cell.lastTextLbl.text = "You: " + recentChat[indexPath.row].lastMessage.message
            }else {
                cell.lastTextLbl.text = recentChat[indexPath.row].lastMessage.message
            }
            
            let time = recentChat[indexPath.row].lastMessage.timestamp
            cell.timeLbl.text = FintechUtils.sharedInstance.unixToLocalTime(timestamp: Double(time/1000))


            cell.cellImg.kf.setImage(with: URL(string: recentChat[indexPath.row].profile_pic), placeholder: UIImage(named: "placeholder-images-image_large"))

            return cell
        }else{
            print("11==")
            let cell = recentChatTableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! RecentChatCell
            cell.cellImg.layer.cornerRadius = cell.cellImg.frame.height/2
            cell.bgView.layer.shadowColor = UIColor.lightGray.cgColor
                  cell.bgView.layer.shadowRadius = 4
                  cell.bgView.layer.shadowOpacity = 2
                  cell.bgView.layer.shadowOffset = CGSize(width: 0, height: 0)
                 cell.bgView.layer.cornerRadius = 5
            cell.nameLbl.text = filterRecentChat[indexPath.row].name
            
            if filterRecentChat[indexPath.row].lastMessage.senderNamespace == SPManager.sharedInstance.getCurrentUserNamespace() {
                cell.lastTextLbl.text = "You: " + filterRecentChat[indexPath.row].lastMessage.message
            }else {
                cell.lastTextLbl.text = filterRecentChat[indexPath.row].lastMessage.message
            }
            
            let time = filterRecentChat[indexPath.row].lastMessage.timestamp
            cell.timeLbl.text = FintechUtils.sharedInstance.unixToLocalTime(timestamp: Double(time/1000))
            
            cell.cellImg.kf.setImage(with: URL(string: filterRecentChat[indexPath.row].profile_pic), placeholder: UIImage(named: "caleb-dp"))
            
          
            return cell
            
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if filter == false {
            getSavedMessages(chatId: recentChat[indexPath.row].chatUniqueId, advisorName: recentChat[indexPath.row].name)
        }else {
            getSavedMessages(chatId: filterRecentChat[indexPath.row].chatUniqueId, advisorName: filterRecentChat[indexPath.row].name)
        }
        
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if cellAnimate == true{

            let rotationTransform = CATransform3DTranslate(CATransform3DIdentity, -200, 0, 0)
            cell.layer.transform = rotationTransform
            cell.alpha = 0.5

            UIView.animate(withDuration: 0.5){
                cell.layer.transform = CATransform3DIdentity
                cell.alpha = 1.0
            }
        }
    }
    
}

//MARK :- SEARCH BAR
extension RecentChatsVC: UISearchBarDelegate {
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        filter = false
        print(recentChat)
        cellAnimate = true
        DispatchQueue.main.async{
            self.recentChatTableView.reloadData()
            
        }
    }

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        DispatchQueue.main.async { [self] in
            if self.searchBar.text == ""{
                filter = false
                print(recentChat)
                cellAnimate = true
                DispatchQueue.main.async{
                    self.recentChatTableView.reloadData()
                    
                }
            }else{
              
                filter = true
                 filterRecentChat = recentChat.filter({$0.name.lowercased().range(of: self.searchBar.text!.lowercased()) != nil})
                print(filterRecentChat)
                cellAnimate = false
                DispatchQueue.main.async{
                    self.recentChatTableView.reloadData()
                    
                }
            }
        }
    }
}
extension RecentChatsVC: ChatHistoryApiDelegate {
    func chatHistoryFetched(successfully: Bool, data: [RecentChatLastMessage], chatId: String, advisorName: String) {
        DispatchQueue.main.async { [self] in
            goToChatVC(advisorName: advisorName, chatId: chatId, chatHistory: data)
        }
    }
}
