//
//  myProfile_view.swift
//  Fintech
//
//  Created by Apple on 21/08/2020.
//  Copyright © 2020 Broadstone Technologies. All rights reserved.
//
////#################################################################
//  In This view show user profile user name show progress chart change user level and award and bage count show
//
//################################################################

import UIKit
import Lottie
import RingGraph
class myProfile_view: UIViewController {
    // MARK: - IB outlet
    @IBOutlet weak var animation_badge_view: AnimationView!
    @IBOutlet weak var chart_View: RingGraphView!
    @IBOutlet weak var animation_award_view: AnimationView!
    @IBOutlet weak var medal_lbl: UILabel!
    @IBOutlet weak var award_lbl: UILabel!
    @IBOutlet weak var Username_lbl: UILabel!
    
    @IBOutlet weak var Bigner_tick_img: UIImageView!
    @IBOutlet weak var intermediat_tick_img: UIImageView!
    
    @IBOutlet weak var takequiz_tick_img: UIImageView!
    @IBOutlet weak var expert_tick_btn: UIImageView!
    
    @IBOutlet weak var goodJobLabel: UILabel!
    @IBOutlet weak var awardsTxt: UILabel!
    @IBOutlet weak var medalsTxt: UILabel!
    @IBOutlet weak var shareProgressBtn: UIButton!
    @IBOutlet weak var chooseAnotherGoalLabel: UILabel!
    @IBOutlet weak var unsureLabel: UILabel!
    @IBOutlet weak var easyBtn: UIButton!
    @IBOutlet weak var mediumBtn: UIButton!
    @IBOutlet weak var hardBtn: UIButton!
    @IBOutlet weak var quizBtn: UIButton!
    @IBOutlet weak var shareBtnView: UIView!
    
    @IBOutlet weak var userExpertiseView: UIView!
    @IBOutlet weak var userExpertiseLevelView: UIView!
    @IBOutlet weak var userExpertiseDropDownImage: UIImageView!
    
    var isProgressAvailable = false
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        Bigner_tick_img.isHidden = true
        intermediat_tick_img.isHidden = true
        takequiz_tick_img.isHidden = true
        expert_tick_btn.isHidden = true
        
        showHideExpertiseView(true)
        

        
        animation_badge_view.contentMode = .scaleAspectFit
        animation_badge_view.animationSpeed = 0.5
        animation_badge_view.play()
        
        animation_award_view.contentMode = .scaleAspectFit
        animation_award_view.animationSpeed = 0.5
        animation_award_view.play()
        
        
     
          
        // Do any additional setup after loading the view.
    }
    
    // MARK: - viewWillAppear
     override func viewWillAppear(_ animated: Bool) {
        //self.showprogress()
        let  me = UserDefaults.standard.float(forKey: "medal")
       var sample_username = UserDefaults.standard.string(forKey: "sampleportfolio_username") ?? ""

        navigationController?.isNavigationBarHidden = true
        if sample_username == "" {
            Username_lbl.text = getLocalizedString(key: "Profile", comment: "")
            
        }else {
            Username_lbl.text = "\(sample_username)'s " + getLocalizedString(key: "Profile", comment: "")
            
        }
        if SPManager.sharedInstance.saveUserLoginStatus() {
         Username_lbl.text = "\(SPManager.sharedInstance.getCurrentUserNameForProfile())'s " + getLocalizedString(key: "Profile", comment: "")
        }else {
            
        }
         let userExpertise = SPManager.sharedInstance.getUserEducationExpertiseLevel()
         if userExpertise == EducationExpertiseValues.levelBigginer{
            Bigner_tick_img.isHidden = false
            intermediat_tick_img.isHidden = true
            expert_tick_btn.isHidden = true

        }else if userExpertise == EducationExpertiseValues.levelIntermediate {
            intermediat_tick_img.isHidden = false
            Bigner_tick_img.isHidden = true
            expert_tick_btn.isHidden = true
            
        }else if userExpertise == EducationExpertiseValues.levelExpert{
            expert_tick_btn.isHidden = false
            Bigner_tick_img.isHidden = true
            intermediat_tick_img.isHidden = true

        }
        populateGraphView()
        localization()
        
    }
    
    
    private func populateGraphView() {
        isProgressAvailable = get()
        
        
        if isProgressAvailable {
            shareBtnView.backgroundColor = UIColor(named: "AppMainColor")
        }else {
            shareBtnView.backgroundColor = .systemGray2
        }
    }
    
    private func localization(){
        goodJobLabel.text = getLocalizedString(key: "good_job_keep_it_up", comment: "")
        awardsTxt.text = getLocalizedString(key: "awards_", comment: "")
        medalsTxt.text = getLocalizedString(key: "medals_", comment: "")
        shareProgressBtn.setTitle(getLocalizedString(key: "share_progress", comment: ""), for: .normal)
        chooseAnotherGoalLabel.text = getLocalizedString(key: "choose_another_goal", comment: "")
        unsureLabel.text = getLocalizedString(key: "if_you_are_unsure", comment: "")
        easyBtn.setTitle(getLocalizedString(key: "beginner", comment: ""), for: .normal)
        mediumBtn.setTitle(getLocalizedString(key: "intermediate", comment: ""), for: .normal)
        hardBtn.setTitle(getLocalizedString(key: "expert", comment: ""), for: .normal)
        quizBtn.setTitle(getLocalizedString(key: "takequiz", comment: ""), for: .normal)
    }
    
    private func showHideExpertiseView(_ shouldHide: Bool) {
        if shouldHide {
            userExpertiseLevelView.isHidden = true
            userExpertiseView.heightConstaint?.constant = 50
            userExpertiseDropDownImage.image = UIImage(systemName: "chevron.down")
        }else {
            userExpertiseLevelView.isHidden = false
            userExpertiseView.heightConstaint?.constant = 540
            userExpertiseDropDownImage.image = UIImage(systemName: "chevron.up")
        }
    }
    
    @IBAction func userExpertiseBtnPressed(_ sender: UIButton) {
        if userExpertiseLevelView.isHidden {
            showHideExpertiseView(false)
        }else {
            showHideExpertiseView(true)
        }
    }
    
    // MARK: - Button acction
    @IBAction func cancel_btn(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func share_progress_Pressed(_ sender: Any) {
        if !isProgressAvailable {
            self.view.makeToast(getLocalizedString(key: "complete_lesson_to_share_progress"))
            return
        }
        let image = UIImage(named: "Badge1")
        
        // set up activity view controller
        let imageToShare = [ image! ]
        let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        // exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook ]
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
    }
    @IBAction func Beginner_btn(_ sender: Any) {
        SPManager.sharedInstance.setUserEducationExpertiseLevel(EducationExpertiseValues.levelBigginer)
//        userlevel = "Beginner"
//        UserDefaults.standard.set(userlevel, forKey: "userLevel")
        Bigner_tick_img.isHidden = false
        intermediat_tick_img.isHidden = true
        expert_tick_btn.isHidden = true
        takequiz_tick_img.isHidden = true

        populateGraphView()
        
    }
    @IBAction func intermediate(_ sender: Any) {
        SPManager.sharedInstance.setUserEducationExpertiseLevel(EducationExpertiseValues.levelIntermediate)
        Bigner_tick_img.isHidden = true
        expert_tick_btn.isHidden = true
        takequiz_tick_img.isHidden = true
        intermediat_tick_img.isHidden = false

        populateGraphView()
    }
    @IBAction func Expert_btn(_ sender: Any) {
        
        SPManager.sharedInstance.setUserEducationExpertiseLevel(EducationExpertiseValues.levelExpert)
        expert_tick_btn.isHidden = false
        Bigner_tick_img.isHidden = true
        takequiz_tick_img.isHidden = true
        intermediat_tick_img.isHidden = true
        
        populateGraphView()
    }
    @IBAction func takeQuiz_btn(_ sender: Any) {
        takequiz_tick_img.isHidden = false
        expert_tick_btn.isHidden = true
        Bigner_tick_img.isHidden = true
        intermediat_tick_img.isHidden = true
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "QuizExpertiselevel_VC") as! QuizExpertiselevel_VC
        
        navigationController?.pushViewController(vc, animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension myProfile_view{
    func get() -> Bool{
        let db = Databasehandler()
        var totalcount = 0
        var award1 = 0
        var  user = SPManager.sharedInstance.getCurrentUserEmail()
        if user == "" {
            user = "test"
        }
        let data = db.readEducation()
        for item in data{
            let id = item.id
            let data1 = db.readEducationDETAIL(id: id)
            let doneArray = db.getQuestion(user: user, mainQ: id)
            totalcount = totalcount + doneArray.count
            
            if doneArray.count == data1.count{
                award1 += 1
            }
        }
        
        
        medal_lbl.text = "\(totalcount)"
        award_lbl.text = "\(award1)"
        
        
        let clor1 :UIColor = #colorLiteral(red: 0.03921568627, green: 0.5607843137, blue: 0.03137254902, alpha: 1)
        let clor2 :UIColor = #colorLiteral(red: 0.3098039216, green: 0.6156862745, blue: 0.8666666667, alpha: 1)
        // let  medal = UserDefaults.standard.float(forKey: "done")
        // let  totalprenstage = UserDefaults.standard.float(forKey: "countprenstage")
        //print("totalprenstage = \(totalprenstage)")
        //print("medal==\(medal)")
        if #available(iOS 13.0, *) {
            let graphMeters = [RingMeter(title: "Total percentage", value: award1, maxValue: 6, colors: [clor2], symbolProvider: ImageProvider(image: UIImage(named: "")), backgroundColor: #colorLiteral(red: 0.9411764706, green: 0.9411764706, blue: 0.9411764706, alpha: 1)),
                               
                               RingMeter(title: "Medal", value: totalcount , maxValue: 28, colors : [ clor1], symbolProvider: ImageProvider(image: UIImage(named: "")), backgroundColor: #colorLiteral(red: 0.9411764706, green: 0.9411764706, blue: 0.9411764706, alpha: 1))]
            
            
            if let graph = RingGraph(meters: graphMeters) {
                
                var subview = chart_View.subviews
                for view in subview {
                    if view.tag == 1111 {
                        view.removeFromSuperview()
                    }
                }
                
                let viewFrame = chart_View.frame
                let ringFrame = CGRect(x: 0, y: 0, width: viewFrame.width, height: viewFrame.height)
                
                let ringGraphView = RingGraphView(frame: ringFrame, graph: graph, preset: .MetersDescription)
                ringGraphView.backgroundColor = UIColor.clear
                ringGraphView.tag = 1111
                chart_View.addSubview(ringGraphView)
                chart_View.backgroundColor = .clear
                
            }
        } else {
        }
        
        if totalcount == 0 && award1 == 0 {
            return false
        }else {
            return true
        }
    }
}
