//
//  ScoreImprovementCell.swift
//  Fintech
//
//  Created by broadstone on 09/04/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import UIKit

class ScoreImprovementCell: UITableViewCell {
    
    @IBOutlet weak var headingCell: UILabel!
    @IBOutlet weak var charitiesButton: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func charityBtnPressed(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Charity", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "listOfCharitiesVC") as! listOfCharitiesVC
        // self.navigationController?.pushViewController(vc, animated: true)
        self.parentContainerViewController()?.navigationController?.pushViewController(vc, animated: true)
    }
}
