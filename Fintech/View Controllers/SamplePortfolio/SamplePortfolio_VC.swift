//
//  SamplePortfolio_VC.swift
//  Fintech
//
//  Created by Apple on 01/07/2020.
//  Copyright © 2020 Broadstone Technologies. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAnalytics
class SamplePortfolio_VC: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource {
    
    
    
    
    var  arr1 = ["sample_portfolio_img_1","sample_portfolio_img_2","sample_portfolio_img_3"]
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
         return arr1.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! SamplePortfolio_cell
        cell.UIImge.image = UIImage(named: arr1[indexPath.row])
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        PageControler.currentPage = arr1.count
    }
    
    var timer = Timer()
    var counter = 0
    
    @IBOutlet weak var PageControler: UIPageControl!
    
    @IBOutlet weak var collection_view: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
       self.navigationController?.isNavigationBarHidden = true
       // page_View.numberOfPages = arr1.count
         PageControler.numberOfPages = arr1.count
        PageControler.currentPage = 0
        //        DispatchQueue.main.async {
        //            self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector:
        //                Selector(changeimage), userInfo: nil, repeats: true)
        //
        //        }
//        DispatchQueue.main.async {
//            self.timer = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(self.updateNotification), userInfo: nil, repeats: true)
//        }
        
        
       
        
        if SPManager.sharedInstance.saveUserLoginStatus() {
            if let tabbedbar = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "nav_bar_start") as? UINavigationController {
                self.view.window?.rootViewController = tabbedbar
            }
        }else{
           
        }

        // Do any additional setup after loading the view.
    }
    
    
    func setAnalytics(){
         
         let screenClass = classForCoder.description()
         Analytics.logEvent(AnalyticsEventScreenView, parameters: [AnalyticsParameterScreenClass: screenClass])
         
     }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        setAnalytics()
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        var visibleRect = CGRect()
        
        visibleRect.origin = collection_view.contentOffset
        visibleRect.size = collection_view.bounds.size
        
        let visiblePoint = CGPoint(x: visibleRect.minX, y: visibleRect.minY)
        
        guard let indexPath = collection_view.indexPathForItem(at: visiblePoint) else { return }
        
        print(indexPath)
        PageControler.currentPage = indexPath.item
    }
    @objc func updateNotification(){
//        if counter < arr1.count{
//            let index = IndexPath.init(item: counter, section: 0)
//            self.collection_view.scrollToItem(at: index, at:.centeredHorizontally , animated: true)
//            PageControler.currentPage = counter
//
//            counter += 1
//
//
//        }else{
//            counter = 0
//            let index = IndexPath.init(item: counter, section: 0)
//            self.collection_view.scrollToItem(at: index, at:.centeredHorizontally , animated: false)
//            PageControler.currentPage = counter
//
//        }
        
    }
    
    
    
    
    @IBAction func PageControler_btn(_ sender: Any) {
       /// let page = scrol
        
    }
    
    @IBAction func ViewPortfolio_btn(_ sender: Any) {
        if let tabbedbar = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "nav_bar_start") as? UINavigationController {
            self.view.window?.rootViewController = tabbedbar
        }
        
        setAnalyticsAction(ScreenName: "SamplePortfolio", method: "ViewPortfolioPresed")
        
    }
    
    func setAnalyticsAction(ScreenName: String, method: String){
        
        let screenClass = classForCoder.description()
        Analytics.logEvent(AnalyticsEventScreenView, parameters: [AnalyticsParameterScreenName: ScreenName, AnalyticsParameterScreenClass: screenClass, "Method" : method])
    }
    
    @IBAction func signIN_btn(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "Main_login_page") as UIViewController
        UIApplication.shared.keyWindow?.rootViewController = vc
        
        setAnalyticsAction(ScreenName: "SamplePortfolio", method: "UserSignedIn")
        
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension SamplePortfolio_VC: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = collectionView.frame.size
        
        return CGSize(width: size.width, height: size.height)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 5)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
}
