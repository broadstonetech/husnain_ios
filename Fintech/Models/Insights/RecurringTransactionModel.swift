//
//  RecurringTransactionModel.swift
//  Fintech
//
//  Created by broadstone on 17/07/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import Foundation

struct RecurringTransactionModel {
    let success: Bool
    let message: String
    let data: RecurringTransactions
}

struct RecurringTransactions {
    let days: [RecurringTransactionsMerchantWise]
    let months: [RecurringTransactionsMerchantWise]
    let quarters: [RecurringTransactionsMerchantWise]
    let biAnnual: RecurringTransactionsMerchantWise
    let annual: [RecurringTransactionsMerchantWise]
    
}

struct RecurringTransactionsMerchantWise {
    let merchantWiseTransactions: [RecurringTransaction]
}

struct RecurringTransaction {
    let merchantName: String
    let amount: Double
    let date: Int64
}
