//
//  customizedDistributionCell.swift
//  Fintech
//
//  Created by Apple on 23/12/2020.
//  Copyright © 2020 Broadstone Technologies. All rights reserved.
//

import UIKit

protocol CustomizeDistributionDeleage {
    func percentageBtnPressedAt(_ index: Int)
}

class customizedDistributionCell: UITableViewCell {
    
    @IBOutlet weak var donateBtn: UIButton!
    @IBOutlet weak var donationView: UIView!
    @IBOutlet weak var tickImage: UIImageView!
    
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var precentage_btn: UIButton!
    
    @IBOutlet weak var percentField: UITextField!
    
    @IBOutlet weak var amount_lbl: UILabel!
    @IBOutlet weak var heading_lbl: UILabel!
    
    var delegate: CustomizeDistributionDeleage?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func setPercentageBtnPressed(_ sender: UIButton) {
        delegate?.percentageBtnPressedAt(precentage_btn.tag)
    }

}
