//
//  AddCategorySpendingCell.swift
//  vesgo
//
//  Created by broadstone on 15/01/2021.
//  Copyright © 2021 Apple. All rights reserved.
//

import Foundation
import UIKit

class AddCategorySpendingCell: UITableViewCell {
    
    @IBOutlet weak var imgBgView: UIView!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var categoryNameLbl: UILabel!
    @IBOutlet weak var amountPlannedLabel: UILabel!
    @IBOutlet weak var amountSpentField: UITextField!
    @IBOutlet weak var merchantNameField: UITextField!
    @IBOutlet weak var spentDateField: UITextField!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
