//
//  FilterHeaderCell.swift
//  Fintech
//
//  Created by broadstone on 19/01/2022.
//  Copyright © 2022 Broadstone Technologies. All rights reserved.
//

import UIKit

class FilterHeaderCell: UITableViewCell {
    
    @IBOutlet weak var headingLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
