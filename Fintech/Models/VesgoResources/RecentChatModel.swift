//
//  recentChatModel.swift
//  vesgoRecChat
//
//  Created by Hussnain Ali on 04/01/2022.
//

import Foundation

struct RecentChatModel{
    let success: Bool
    let message: String
    let data: [RecentChat]
}

// MARK: - text
struct RecentChat {
    let advisor_namespace : String
    let chatUniqueId: String
    let lastMessage: RecentChatLastMessage
    let name: String
    let profile_pic: String
 
}

struct RecentChatLastMessage {
    let message: String
    let senderNamespace: String
    let timestamp: Int64
}


