//
//  VesgoTutorialVC.swift
//  Fintech
//
//  Created by broadstone on 18/03/2022.
//  Copyright © 2022 Broadstone Technologies. All rights reserved.
//

import UIKit
import AVFoundation

struct VesgoTutorialsStruct {
    let label: String
    let videoLink: String
    let videoID: String
}

class VesgoTutorialVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var links = [VesgoTutorialsStruct]()

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        
        links = getTutorialsLinks()
        tableView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        self.title = getLocalizedString(key: "_tutorials")
    }
    
    private func getTutorialsLinks() -> [VesgoTutorialsStruct] {
        var data = [VesgoTutorialsStruct]()
        
        data.append(VesgoTutorialsStruct.init(label: "Powerful Budgeting Tools - Spending Insights", videoLink: "https://youtu.be/N0DNYUm-sVI", videoID: "N0DNYUm-sVI"))
        data.append(VesgoTutorialsStruct.init(label: "Financial Literacy", videoLink: "https://youtu.be/0XU8hKdFbU8", videoID: "0XU8hKdFbU8"))
        data.append(VesgoTutorialsStruct.init(label: "Value Based Investments", videoLink: "https://youtu.be/H1A7rTpm5eQ", videoID: "H1A7rTpm5eQ"))

        data.append(VesgoTutorialsStruct.init(label: "Value & Faith Based Investments - AI Based Portfolio Creation & Management", videoLink: "https://youtu.be/8GVOEVKkhJg", videoID: "8GVOEVKkhJg"))
        return data
    }
    

}
extension VesgoTutorialVC: UITableViewDelegate, UITableViewDataSource {
   
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return links.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "VesgoTutorialCell") as! VesgoTutorialCell
        cell.videoLabel.text = links[indexPath.section].label
        cell.playerView.load(withVideoId: links[indexPath.section].videoID)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! VesgoTutorialCell
        cell.playerView.playVideo()
    }

    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int)
    {
        view.tintColor = #colorLiteral(red: 0.9411764706, green: 0.9411764706, blue: 0.9411764706, alpha: 1)
        view.backgroundColor = #colorLiteral(red: 0.9411764706, green: 0.9411764706, blue: 0.9411764706, alpha: 1)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 20
    }
    
    
    
    
}
