//
//  HomeSurveyVC.swift
//  Fintech
//
//  Created by MacBook-Mubashar on 24/07/2019.
//  Copyright © 2019 Broadstone Technologies. All rights reserved.
//#####################################################################
//      In This view show questionnaire data
//       load question data
//       save all selected answer
//       selected all question answer then show alert mesg and show
//         two button change / submit. click submit button move categorysummary
//
//################################################################

import UIKit
import Lottie
var Datee:[String]=[String]()
struct summary: Codable{
    var questionNo : Int?
    var  questiontext : String?
    var answertext : String?
    
    var catogries : Int?
}
var SELECTED_ANSWERS_DICT = [[String: Any]]()

var answelists = [SubmitQDataModel]()
var flag = false
var answerSelected = [String]()
var answerUpdateFlag = false
var summaryList = [summary]()
var ListOfSubmittedAnswers: [QuestionnaireModel] = [QuestionnaireModel]()
var questiondate = [String]()
class HomeSurveyVC: Parent, UICollectionViewDelegate,UICollectionViewDataSource, TagListViewDelegate {
    var ischange = false
    var personalList = [summary]()
    var investemenList = [summary]()
    var valuesList = [summary]()
    var riskList = [summary]()
    var categories = ["Personal details","Investment related","Values","Risk assessment"]
    var answersArray:[String] = []
    var lastquestionUpdate = false
    
    @IBOutlet weak var Animation_img_view: AnimationView!
    
    @IBOutlet weak var cancel_btn: UIButton!
    
    @IBAction func cancel_presssd(_ sender: Any) {
        let alert = UIAlertController(title: "Do you want to skip strategy building?", message: "",preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: { _ in
            
            //Cancel Action
        }))
        alert.addAction(UIAlertAction(title: "Skip", style: UIAlertAction.Style.default, handler: { _ in
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "nav_bar_start") as UIViewController
            UIApplication.shared.keyWindow?.rootViewController = vc
            
            //        navigationController?.pushViewController(vc, animated: true)
            //Cancel Action
        }))
        self.present(alert, animated: true, completion: nil)
        
        
    }
    
    
    
    @IBOutlet weak var submitBtn_view: UIView!
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return ShowAnswer.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! PortfolioShowanswerCell
        cell.Showanswer_lbl.text = ShowAnswer[indexPath.item]
        return cell
    }
    
    //MARK: - IB OUTLETS
    // VIEWS
    @IBOutlet weak var viewBackground_image: UIImageView!
    @IBOutlet var questionGuided_image: UIImageView!
    @IBOutlet weak var question_lbl: UILabel!
    @IBOutlet weak var questoinInfo_lbl: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var submitQuestion_btn: UIButton!
    @IBOutlet weak var Background_img: UIImageView!
    @IBOutlet var skipQuestionnaire_btn: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var Properties_view: UIView!
    @IBOutlet weak var Tages_view: TagListView!
    @IBOutlet weak var Cunt_question_lbl: UILabel!
    @IBOutlet weak var totalQuestion: UILabel!
    @IBOutlet weak var Progress_Bar: UIProgressView!
    // MARK: - VARIABLES
    var tag = 0
    var Qno = 1
    var propertyNotesArray = ["Current strategy and recommended","Tax-loss harvesting, stock level tax-loss harvesting, Smart beta","Tax-loss harvesting", "None"]
    var ShowAnswer = [String]()
    var Questionnum = Int()
    var Q = [String]()
    var questionarry = ""
    
    var selectedSection = -1
    var isAnswerSelected : Bool = false
    var CURRENT_QUESTION_INDEX : Int = 0
    var SELECTED_ANSWER_INDEX : Int = -1
    var ANSWERS_DICTIONARY : Dictionary <String, String> = [:]
    //MARK: - store current question number
    var questionNumber = -1
    
    //MARK: - store question number to change
    var updateNumber = -1
    var updateFalg = false
    var index = 0
    var currentQuestion = Question()
    var extraQuestion :[Question] = [Question]()
    var timer2 = Timer()
    var countdown:Int = 13
    
    //MARK: -Life cycle
    override func viewDidLoad() {
        DispatchQueue.main.async {
            self.timer2 = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(self.updateNotification), userInfo: nil, repeats: true)
        }
        
        
        
        totalQuestion?.text = "/22"
        for sum in summaryList{
            answerSelected.append(sum.answertext ?? "a")
            if sum.catogries == 1 {
                personalList.append(sum)
            }else
                if sum.catogries == 2 {
                    investemenList.append(sum)
                }else
                    if sum.catogries == 3 {
                        riskList.append(sum)
                    }else
                        if sum.catogries == 4 {
                            valuesList.append(sum)
            }
            
        }
        
        //MARK: - LOAD QUESTION DATA
        extraQuestion = [
            Question(qestionNo: "23", question: "Q24", category: 4, answer: [], option: ["24a","24b","24c","24d","24e"],multiSelection: true, imagName: ""),
            
            Question(qestionNo: "24", question: "Q23", category: 4, answer: [], option: ["23a","23b","23c","23d","23e","23f","23g","23h","23i","23j","23k","23m","23n"],multiSelection: true, imagName: ""),
            
            Question(qestionNo: "25", question: "Q25", category: 4, answer: [], option: ["25a","25b","25c"],multiSelection: false, imagName: ""),
            Question(qestionNo: "26", question: "Q26", category: 4, answer: [], option: ["26a","26b","26c","26d","26e"],multiSelection: false, imagName: ""),
            Question(qestionNo: "27", question: "Q27", category: 4, answer: [], option: ["27a","27b", "27c","27d","27e"],multiSelection: false, imagName: ""),
//            Question(qestionNo: "28", question: "Q28", category: 4, answer: [], option: ["28a","28b"],multiSelection: false, imagName: ""),
              Question(qestionNo: "28", question: "Q28", category: 4, answer: [], option: ["28a","28b"],multiSelection: false, imagName: "")
        ]
        
        
        QuestionAaray = [
            Question(qestionNo: "1", question: "Q1", category: 1, answer: [], option: ["1a","1b","1c","1d"],multiSelection: false, imagName: ""),
            
            Question(qestionNo: "2", question: "Q2", category: 1, answer: [], option: ["2a","2b","2c"],multiSelection: false, imagName: ""),
            
            Question(qestionNo: "3", question: "Q3", category: 1,answer: [],option: ["3a","3b","3c","3d"],multiSelection: false, imagName: ""),
            
            Question(qestionNo: "4", question: "Q4", category: 2,answer: [],option: ["4a","4b","4c","4d","4e"],multiSelection: false, imagName: ""),
            
            Question(qestionNo: "5", question: "Q5", category: 2, answer: [], option: ["5a","5b","5c","5d","5e"],multiSelection: false, imagName: ""),
            
            Question(qestionNo: "6", question: "Q6", category: 2, answer: [], option: ["6a","6b","6c","6d"],multiSelection: false, imagName: ""),
            
            Question(qestionNo: "7", question: "Q7", category: 2, answer: [], option: ["7a","7b","7c","7d"],multiSelection: false, imagName: ""),
            
            Question(qestionNo: "8", question: "Q8", category: 2, answer: [], option: ["8a","8b","8c","8d","8e"],multiSelection: true, imagName: ""),
            
            Question(qestionNo: "9", question: "Q9", category: 3, answer: [], option: ["9a","9b","9c","9d"],multiSelection: false, imagName: ""),
            Question(qestionNo: "10", question: "Q10", category: 3, answer: [], option: ["10a","10b","10c","10d"],multiSelection: false, imagName: ""),
            Question(qestionNo: "11", question: "Q11", category: 3, answer: [], option: ["11a","11b","11c","11d"],multiSelection: false, imagName: ""),
            Question(qestionNo: "12", question: "Q12", category: 3, answer: [], option: ["12a","12b","12c"],multiSelection: false, imagName: ""),
            Question(qestionNo: "13", question: "Q13", category: 3, answer: [], option: ["13a","13b","13c"],multiSelection: false, imagName: ""),
            Question(qestionNo: "14", question: "Q14", category: 3, answer: [], option: ["14a","14b","14c","14d"],multiSelection: false, imagName: ""),
            
            Question(qestionNo: "15", question: "Q15", category: 3, answer: [], option: ["15a","15b","15c","15d"],multiSelection: false, imagName: ""),
            
            Question(qestionNo: "16", question: "Q16", category: 3, answer: [], option: ["16a","16b","16c","16d"],multiSelection: false, imagName: ""),
            
            Question(qestionNo: "17", question: "Q17", category: 3, answer: [], option: ["17a","17b"],multiSelection: false, imagName: ""),
            
            Question(qestionNo: "18", question: "Q18", category: 3, answer: [], option: ["18a","18b"],multiSelection: false, imagName: ""),
            
            
            Question(qestionNo: "19", question: "Q19", category: 3, answer: [], option: ["19a","19b","19c","19d"],multiSelection: false, imagName: ""),
            
            Question(qestionNo: "20", question: "Q20", category: 3, answer: [], option: ["20a","20b","20c"],multiSelection: false, imagName: ""),
            Question(qestionNo: "21", question: "Q21", category: 3, answer: [], option: ["21a","21b","21c","21d"],multiSelection: false, imagName: ""),
            
            Question(qestionNo: "22", question: "Q22", category: 4, answer: [], option: ["22a","22b","22c"],multiSelection: false, imagName: ""),
            
            
        ]
        super.viewDidLoad()
        collectionView.reloadData()
        currentQuestion = QuestionAaray[CURRENT_QUESTION_INDEX]
        ChangeImage(categoryId: (currentQuestion.category))
        
        question_lbl.text = getLocalizedString(key: (currentQuestion.question), comment: "")
        
        self.navigationController?.isNavigationBarHidden = false
        self.navigationItem.leftBarButtonItem = nil;
        self.navigationItem.setHidesBackButton(true, animated: false)

        setNavBarTitile(title: "Investment strategy", topItem: "Investment strategy")
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        skipQuestionnaire_btn.addTarget(self, action: #selector(skipQuestionaireTapped), for: UIControl.Event.touchUpInside)
        //    skipQuestionnaire_btn.isHidden = true
        //getStrategyQuestion()
        //   loadNextQuestion()
    }
    @objc func updateNotification(){
        countdown -= 1
        if countdown == 0{
            Animation_img_view.isHidden = true
        }else{
            Animation_img_view.contentMode = .scaleAspectFit
            //  self.Animationtrophy_view.loopMode = .loop
            Animation_img_view.animationSpeed = 1.5
            Animation_img_view.play()
            
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        if SelectedQuestionNO != -1{
            answerUpdateFlag = true
            if summaryList.count == 35{
                if SelectedQuestionNO == 9{
                    currentQuestion = extraQuestion[0]
                }else if SelectedQuestionNO >= 10{
                    //SelectedQuestionNO -= 1
                    currentQuestion = QuestionAaray[SelectedQuestionNO-1]
                }else{
                    currentQuestion = QuestionAaray[SelectedQuestionNO]
                }
            }else{
                currentQuestion = QuestionAaray[SelectedQuestionNO]
            }
            question_lbl.text = getLocalizedString(key: (currentQuestion.question), comment: "")
            ChangeImage(categoryId: (currentQuestion.category))
            
            tableView.reloadData()
            Tages_view.isHidden = true
        }else{
            summaryList = []
            answerUpdateFlag = false
            Tages_view.isHidden = false
        }
        Tages_view.delegate = self
        Tages_view.textFont = .systemFont(ofSize: 15)
        Tages_view.shadowRadius = 2
        Tages_view.tagBackgroundColor = #colorLiteral(red: 0.2235294118, green: 0.2862745098, blue: 0.6705882353, alpha: 1)
        // colorLiteral(red: 0.2235294118, green: 0.2862745098, blue: 0.6705882353, alpha: 1)
        Tages_view.shadowOpacity = 0.4
        Tages_view.shadowColor = UIColor.black
        Tages_view.shadowOffset = CGSize(width: 1, height: 1)
        Tages_view.addTags(ShowAnswer)
        
        Tages_view.textFont = .systemFont(ofSize: 17)
        Tages_view.alignment = .left
        Tages_view.tagLineBreakMode = .byTruncatingTail
    }
    // MARK: TagListViewDelegate
    func tagPressed(_ title: String, tagView: TagView, sender: TagListView) {
        if !updateFalg{
            print("Tag pressed: \(title), \(sender),tag = \(tagView.tag)")
            updateFalg = true
            let array =  title.split(separator: ".")
            let index = Int(array[0])! - 1
            
            questionNumber = CURRENT_QUESTION_INDEX
            updateNumber = index
            if totalQuestion.text == "/28" && updateNumber == 9{
                currentQuestion = extraQuestion[0]
            }else{
                currentQuestion = QuestionAaray[index]
            }
            question_lbl.text = getLocalizedString(key: (currentQuestion.question), comment: "")

            tableView.reloadData()
            tagView.isSelected = !tagView.isSelected
        }
    }
    func tagRemoveButtonPressed(_ title: String, tagView: TagView, sender: TagListView) {
        print("Tag Remove pressed: \(title), \(sender)")
        sender.removeTagView(tagView)
    }
    override func viewDidAppear(_ animated: Bool) {
        
        setNavBarTitile(title: "Investment strategy", topItem: "Investment strategy")
        navigationController?.navigationBar.barStyle = .blackOpaque
    }
    //MARK: -Textview delegates
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        UIApplication.shared.open(URL)
        return false
    }
    //MARK: -Tableview Action
    @IBAction func MultiQuestion(){
        if ischange && updateFalg == false{
            let alert = UIAlertController(title: "Investment strategy", message: "Your investment strategy is complete. Do you want to change or submit?",  preferredStyle: UIAlertController.Style.alert)
            
            alert.addAction(UIAlertAction(title: "Change", style: UIAlertAction.Style.default, handler: { _ in
                self.ischange = true
                self.updateFalg = false
                //Cancel Action
            }))
            alert.addAction(UIAlertAction(title: "Submit", style: UIAlertAction.Style.default, handler: { _ in
                self.submitQuestinnairee()
            }))
            
            self.present(alert, animated: true, completion: nil)
        }else{
            MultiSelection(answerArray: currentQuestion.answer )
        }
        
        
    }
    @IBAction func actionbutton(_ sender: UIButton) {
        self.selectedSection = sender.tag
        print("button taped")
        self.tableView.reloadData()
    }
    func MultiSelection(answerArray: [String]){
        
        if answerArray.count > 0{
            Tages_view.removeAllTags()
            
            let answer = answerArray.joined(separator: ",")
            if currentQuestion.qestionNo == "28"{
                extraQuestion[0].answer = currentQuestion.answer
            }else{
                let index = Int((currentQuestion.qestionNo))! - 1
                QuestionAaray[index].answer = currentQuestion.answer
            }
            if answerUpdateFlag{
                print(SelectedQuestionNO)
                if summaryList.count == 22 && currentQuestion.qestionNo == "28"{
                    
                    var obj_summary:summary = summary()
                    obj_summary.questiontext = (currentQuestion.question)
                    
                    obj_summary.catogries = currentQuestion.category
                    obj_summary.answertext = answer
                    print(answer)
                    obj_summary.questionNo = 16
                    summaryList.insert(obj_summary, at: 9)
                }else{
                    print(summaryList[SelectedQuestionNO])
                    summaryList[SelectedQuestionNO].answertext = answer
                    print(answer)
                }
                
                answerUpdateFlag = false
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SummaryVC") as! SummaryVC
                self.navigationController?.pushViewController(vc, animated: true)
                SelectedQuestionNO = -1
            } else{
                var tapviewAnswer:[String] = []
                for item in answerArray{
                    tapviewAnswer.append(getLocalizedString(key: item, comment: ""))
                }
                let anwerForShow = tapviewAnswer.joined(separator: ",")
                
                if updateFalg {
                    if currentQuestion.qestionNo == "28" {
                        var obj_summary:summary = summary()
                        obj_summary.questiontext = (currentQuestion.question)
                        
                        obj_summary.catogries = currentQuestion.category
                        obj_summary.answertext = answer
                        obj_summary.questionNo = 28
                        if summaryList.count >= 10{
                            summaryList.insert(obj_summary, at: 9)
                            ShowAnswer.insert("10.\(anwerForShow)", at: 9)
                            var templist = [String]()
                            var i = 1
                            for ans in ShowAnswer{
                                let str =  ans.split(separator: ".")[1]
                                templist.append("\(i).\(str)")
                                i += 1
                            }
                            ShowAnswer = templist
                        }else{
                            summaryList.append(obj_summary)
                            ShowAnswer.append("10.\(anwerForShow)")
                        }
                    }else{
                        ShowAnswer[updateNumber] = "\(updateNumber+1).\(anwerForShow)"
                        summaryList[updateNumber].answertext = answer
                    }
                    
                    
                    Tages_view.addTags(ShowAnswer)
                    if CURRENT_QUESTION_INDEX < 22{
                        currentQuestion = QuestionAaray[CURRENT_QUESTION_INDEX]
                        ChangeImage(categoryId: (currentQuestion.category))
                    }
                    tableView.reloadData()
                    updateFalg = false
                    isAnswerSelected = false
                    
                }else{
                    // MARK: - append answers
                    ShowAnswer.append("\(Qno).\(anwerForShow)")
                    Tages_view.addTags(ShowAnswer)
                    self.collectionView.reloadData()
                    isAnswerSelected = false
                    let qId = CURRENT_QUESTION_INDEX+1
                    Qno += 1
                    var obj_summary:summary = summary()
                    obj_summary.questiontext = (currentQuestion.question)
                    
                    obj_summary.catogries = currentQuestion.category
                    obj_summary.answertext = answer
                    if currentQuestion.qestionNo == "28"{
                        obj_summary.questionNo = 28
                    }else{
                        obj_summary.questionNo = qId
                    }
                    summaryList.append(obj_summary)
                    
                    // Mark: - SAVE THE SELECTED ANSWER IN A LIST, DICT
                    
                    CURRENT_QUESTION_INDEX += 1
                    Progress_Bar.progress += 0.0444
                    if CURRENT_QUESTION_INDEX < 28 {
                        // Progress_Bar.setProgress(0.1, animated: true)
                        Cunt_question_lbl.text = "\( Qno)"
                        currentQuestion = QuestionAaray[CURRENT_QUESTION_INDEX]
                        question_lbl.text = getLocalizedString(key: (currentQuestion.question), comment: "")
                        ChangeImage(categoryId: (currentQuestion.category))
                        tableView.reloadData()
                    }else{
                        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SummaryVC") as! SummaryVC
                        
                        // mana abe chnage kia ha
                 
                        self.navigationController?.pushViewController(vc, animated: true)
                       // SelectedQuestionNO = -1
                        
                    }
                }
            }
            if ischange{
                let alert = UIAlertController(title: "Investment strategy", message: "Your investment strategy is complete. Do you want to change or submit?",  preferredStyle: UIAlertController.Style.alert)
                
                alert.addAction(UIAlertAction(title: "Change", style: UIAlertAction.Style.default, handler: { _ in
                    self.ischange = true
                    //Cancel Action
                }))
                alert.addAction(UIAlertAction(title: "Submit", style: UIAlertAction.Style.default, handler: { _ in
                    self.submitQuestinnairee()
                }))
                
                self.present(alert, animated: true, completion: nil)
            }
        }else{
            self.view.makeToast("No answer was selected.")
        }
        
        
        
    }
    func gotoNext(indexrow: Int){
        Tages_view.removeAllTags()
        let index = Int((currentQuestion.qestionNo))! - 1
        QuestionAaray[index].answer = currentQuestion.answer
        let answer = getLocalizedString(key: (currentQuestion.answer[0]), comment: "")
        
        if answerUpdateFlag{
            
            if flag{
                let qId = CURRENT_QUESTION_INDEX+1
                
                var obj_summary:summary = summary()
                obj_summary.questiontext = currentQuestion.question
                
                obj_summary.catogries = currentQuestion.category
                obj_summary.answertext = currentQuestion.answer[0]
                obj_summary.questionNo = qId
                summaryList.append(obj_summary)
                
                if CURRENT_QUESTION_INDEX < 27  {
                    // Progress_Bar.setProgress(0.1, animated: true)
                    CURRENT_QUESTION_INDEX += 1
                    Cunt_question_lbl.text = "\(Qno)"
                    currentQuestion = QuestionAaray[CURRENT_QUESTION_INDEX]
                    currentQuestion.answer = []
                    
                    question_lbl.text = getLocalizedString(key: (currentQuestion.question), comment: "")
                    ChangeImage(categoryId: (currentQuestion.category))
                    print(currentQuestion)
                    tableView.reloadData()
                    
                }else{
                    flag = false
                    let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SummaryVC") as! SummaryVC
                    self.navigationController?.pushViewController(vc, animated: true)
                    SelectedQuestionNO = -1
                    answerUpdateFlag = false
                }
            }
            else
                if summaryList.count == 28 && currentQuestion.qestionNo == "22" && currentQuestion.answer[0] != "22a"{
                    summaryList[SelectedQuestionNO].answertext = (currentQuestion.answer[0])
                    
                    
                    for _ in 0...5{
                        summaryList.removeLast()
                    }
                    
                    
                    let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SummaryVC") as! SummaryVC
                    self.navigationController?.pushViewController(vc, animated: true)
                    SelectedQuestionNO = -1
                    answerUpdateFlag = false
                }else if summaryList.count == 22 && currentQuestion.qestionNo == "22" && currentQuestion.answer[0] == "22a" {
                    summaryList[SelectedQuestionNO].answertext = (currentQuestion.answer[0])
                    
                    QuestionAaray.append(contentsOf: extraQuestion)
                    CURRENT_QUESTION_INDEX = 22
                    currentQuestion = QuestionAaray[22]
                    currentQuestion.answer = []
                    
                    flag = true
                    answerUpdateFlag = false
                    //ischange = true

    
                    tableView.reloadData()
                    
                }else{

                    let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SummaryVC") as! SummaryVC
                    
                    
                    
                    summaryList[SelectedQuestionNO].answertext = (currentQuestion.answer[0])
                    SelectedQuestionNO = -1
                    answerUpdateFlag = false
                    
                    self.navigationController?.pushViewController(vc, animated: true)
                 
            }
            
        } else{
            
            if updateFalg {
                ShowAnswer[updateNumber] = "\(updateNumber+1).\(answer)"
                summaryList[updateNumber].answertext = currentQuestion.answer[0]
                if totalQuestion?.text == "/28" && currentQuestion.qestionNo == "22" && currentQuestion.answer[0] != "22a"{
                    for _ in 0...5{
                        summaryList.removeLast()
                        QuestionAaray.removeLast()
                    }
                    var templist = [String]()
                    var i = 1
                    for ans in ShowAnswer{
                        let str =  ans.split(separator: ".")[1]
                        templist.append("\(i).\(str)")
                        i += 1
                    }
                    Qno -= 1
                    totalQuestion.text = "/22"
                    Cunt_question_lbl.text = "\(Qno)"
                    ShowAnswer =  templist
                    currentQuestion = QuestionAaray[21]
                    updateFalg = false
                }else if totalQuestion?.text == "/22" && currentQuestion.qestionNo == "22" && currentQuestion.answer[0] == "22a" {
                    Qno += 1
                    totalQuestion.text = "/28"
                    Cunt_question_lbl.text = "\(Qno)"
                    //QuestionAaray.append(extraQuestion)
                      QuestionAaray.append(contentsOf: extraQuestion)
                    currentQuestion = extraQuestion[0]
                    //mana add kia hain
                    //ShowAnswer.append("\(Qno).\(answer)")
                    currentQuestion = QuestionAaray[CURRENT_QUESTION_INDEX]

                    isAnswerSelected = false

                    updateFalg = false
                }else{
                    if CURRENT_QUESTION_INDEX < 22{
                        currentQuestion = QuestionAaray[CURRENT_QUESTION_INDEX]
                    }
                    updateFalg = false
                    isAnswerSelected = false
                }
                ChangeImage(categoryId: (currentQuestion.category))
                Tages_view.addTags(ShowAnswer)
                    tableView.reloadData()
            }else{
                
                ShowAnswer.append("\(Qno).\(answer)")
                Tages_view.addTags(ShowAnswer)
                //print(questiontext)
                print("aaabab\(ShowAnswer)")
                self.collectionView.reloadData()
                isAnswerSelected = false
                
                let qId = CURRENT_QUESTION_INDEX+1
                Qno += 1
                var obj_summary:summary = summary()
                obj_summary.questiontext = currentQuestion.question
                
                obj_summary.catogries = currentQuestion.category
                obj_summary.answertext = currentQuestion.answer[0]
                obj_summary.questionNo = qId
                summaryList.append(obj_summary)
                //Mark: - SAVE THE SELECTED ANSWER IN A LIST, DICT
                if currentQuestion.answer[0] == "22a"{
                    QuestionAaray.append(contentsOf: extraQuestion)
                    totalQuestion.text = "/28"
                    Cunt_question_lbl.text = "\(Qno)"
                    CURRENT_QUESTION_INDEX += 1
                    currentQuestion = QuestionAaray[CURRENT_QUESTION_INDEX]
                    
                    question_lbl.text = getLocalizedString(key: (currentQuestion.question), comment: "")
                    // ChangeImage(categoryId: (currentQuestion.category))
                    tableView.reloadData()
                    
                }else{
                    
                    CURRENT_QUESTION_INDEX += 1
                    Progress_Bar.progress += 0.0444
                    
                    if CURRENT_QUESTION_INDEX < QuestionAaray.count  {
                        // Progress_Bar.setProgress(0.1, animated: true)
                        Cunt_question_lbl.text = "\(Qno)"
                        currentQuestion = QuestionAaray[CURRENT_QUESTION_INDEX]
                        question_lbl.text = getLocalizedString(key: (currentQuestion.question), comment: "")
                        ChangeImage(categoryId: (currentQuestion.category))
                        currentQuestion.answer = []
                        print(currentQuestion)
                        tableView.reloadData()
                        
                    }else{
                        
                        let alert = UIAlertController(title: "Investment strategy", message: "Your investment strategy is complete. Do you want to change or submit?",  preferredStyle: UIAlertController.Style.alert)
                        
                        alert.addAction(UIAlertAction(title: "Change", style: UIAlertAction.Style.default, handler: { _ in
                            //mana add kia ha
                            self.ischange = false
                            //Cancel Action
                        }))
                        alert.addAction(UIAlertAction(title: "Submit", style: UIAlertAction.Style.default, handler: { _ in
                            self.submitQuestinnairee()
                        }))
                        
                        self.present(alert, animated: true, completion: nil)
                        
                    }
                }
            }
        }
    }
    
    //MARK: - IB Actions
    @objc func loadQuestionTapped() {
        if isAnswerSelected {
            if ArrayLists.sharedInstance.currentQuestionList == nil {
                self.view.makeToast("No answer was selected.")
            } else {
                for i in (0..<ArrayLists.sharedInstance.currentAnswersList.count) {
                    let item = ArrayLists.sharedInstance.currentAnswersList[i]
                    if item.getIsAnswerSelected() == true {
                        let Answers = ArrayLists.sharedInstance.currentAnswersList[i].getAnswerHeadingText()
                        
                        Tages_view.removeAllTags()
                        if answerUpdateFlag{
                            summaryList[CURRENT_QUESTION_INDEX].answertext = Answers
                            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SummaryVC") as! SummaryVC
                            self.navigationController?.pushViewController(vc, animated: true)
                            SelectedQuestionNO = -1
                         } else{
                            if updateFalg {
                                
                                ShowAnswer[updateNumber] = "\(updateNumber+1).\(Answers)"
                                summaryList[updateNumber].answertext = Answers
                                Tages_view.addTags(ShowAnswer)
                                CURRENT_QUESTION_INDEX = questionNumber
                                loadNextQuestion()
                                updateFalg = false
                                isAnswerSelected = false
                                break
                            }else{
                                ShowAnswer.append("\(CURRENT_QUESTION_INDEX+1).\(Answers)")
                                Tages_view.addTags(ShowAnswer)
                                //print(questiontext)
                                print("aaabab\(ShowAnswer)")
                                self.collectionView.reloadData()
                                print("selected answer is \(String(describing: i)) and text is \(ArrayLists.sharedInstance.currentAnswersList[i].getAnswerHeadingText())")
                                isAnswerSelected = false
                                let qId = ArrayLists.sharedInstance.currentQuestionList[CURRENT_QUESTION_INDEX].getQuestionID()
                                
                                let aId = ArrayLists.sharedInstance.currentAnswersList[selectedSection].getAnswerId()
                                let answerString = ArrayLists.sharedInstance.currentAnswersList[selectedSection].getAnswerBodyText()
                                print("aaaaaaaa\(answerString)")
                                let categoryId = ArrayLists.sharedInstance.currentQuestionList[CURRENT_QUESTION_INDEX].getCategoryId()
                                var obj_summary:summary = summary()
                                obj_summary.questiontext = questionarry
                                
                                obj_summary.catogries = categoryId
                                obj_summary.answertext = Answers
                                
                                obj_summary.questionNo = qId
                                print(Answers)
                                print(aId)
                                summaryList.append(obj_summary)
                                //Mark: - SAVE THE SELECTED ANSWER IN A LIST, DICT
                                let model = QuestionnaireModel()
                                model.setQuestionID(id: qId)
                                model.setAnswerId(id: aId)
                                model.setCategoryId(id: categoryId)
                                ListOfSubmittedAnswers.append(model)
                                CURRENT_QUESTION_INDEX += 1
                                print("ListOfSubmittedAnswers dictionary == \(categoryId)")
                                // print("ListOfSubmittedAnswers dictionary == \(ListOfSubmittedAnswers)")
                                print("jjjjjjjjjjjj\(CURRENT_QUESTION_INDEX)")
                                Q.append(String(CURRENT_QUESTION_INDEX))
                                print(Q)
                                
                                Cunt_question_lbl.text = String(CURRENT_QUESTION_INDEX)
                                if categoryId == 1 {
                                    Background_img.image = UIImage(named:"personal_details")
                                }else if categoryId == 2 {
                                    Background_img.image = UIImage(named: "investment_related")
                                }else if categoryId == 3 {
                                    Background_img.image = UIImage(named: "values (1)")
                                }else if categoryId == 4 {
                                    Background_img.image = UIImage(named: "risk_assesment")
                                }
                                if CURRENT_QUESTION_INDEX <= 22 {
                                    // Progress_Bar.setProgress(0.1, animated: true)
                                    Progress_Bar.progress += 0.0444
                                }else{
                                    
                                }
                            }
                        }
                    }
                }
                selectedSection = -1
                loadNextQuestion()
            }
        } else {
            self.view.makeToast("No answer was selected.")
        }
    }
    
    func ChangeImage(categoryId:Int){
        if categoryId == 1 {
            Background_img.image = UIImage(named: "personal_details")
        }else if categoryId == 2 {
            Background_img.image = UIImage(named: "investment_related")
        }else if categoryId == 3 {
            Background_img.image = UIImage(named: "risk_assesment")
        }else if categoryId == 4 {
            Background_img.image = UIImage(named: "values (1)")
        }
        
    }
    @objc func skipQuestionaireTapped() {
        //        SPManager.sharedInstance.saveUserPortfolioStatus(isPortfolioComplete: true)
        if let tabbedbar = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "nav_bar_start") as? UINavigationController {
            UIApplication.shared.keyWindow?.rootViewController = tabbedbar
        }
    }
    //MARK: - API + load Questionnaire
    func getStrategyQuestion() {
        //        self.skipQuestionnaire_btn.isHidden = false
        self.view.activityStartAnimating()
        let url = ApiUrl.GET_QUESTION
        let namespace = SPManager.sharedInstance.getCurrentUserNamespace()
        let data:Dictionary<String,AnyObject> = ["parent_answer_id": 328 as AnyObject, "parent_question_id" : 444 as AnyObject, "question_type" : 55 as AnyObject ]
        
        let params:Dictionary<String,AnyObject> = ["namespace" : namespace as AnyObject, "data" : data as AnyObject]
        print(data)
        APIManager.sharedInstance.postRequest(serviceName: url, sendData: params, success: { (response) in
            print(response)
            if APIManager.sharedInstance.isSuccess {
                let res = response["message"] as AnyObject
                if response["survey_completed"] != nil {
                    if let tabbedbar = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "nav_bar_start") as? UINavigationController {
                        UIApplication.shared.keyWindow?.rootViewController = tabbedbar
                    }
                } else {
                    DispatchQueue.main.async {
                        self.submitQuestion_btn.isHidden = false
                        self.submitBtn_view.isHidden = false
                        self.fetchMetaData(jsonData: res as! [[String : AnyObject]], table_view: self.tableView)
                    }
                }
            }else{
                if response["message"] != nil {
                    self.showAlert(response["message"] as! String)
                } else {
                    self.showAlert(ConstantStrings.NO_RESULT_FOUND_ALERT_TEXT)
                }
            }
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
        }, failure: { (data) in
            print(data)
            self.view.activityStopAnimating()
            if APIManager.sharedInstance.error400 {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
            }
            if(APIManager.sharedInstance.error401)
            {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
            }else{
                
            }
        })
        
    }
    
    func fetchMetaData(jsonData: [[String : AnyObject]], table_view: UITableView) {
        ArrayLists.sharedInstance.currentQuestionList.removeAll()
        ArrayLists.sharedInstance.currentAnswersList.removeAll()
        for json in jsonData {
            //MARK: - parse question JSON
            let question = QuestionnaireModel()
            let questionID = json["question_id"] as! Int
            let categoryID = json["category_id"] as! Int
            let questionText = json["question_text"] as! String
            let questionGuidedText = json["question_guided_text"] as! String
            let questionGuidedAttributedText = json["attributed_text"] as! String
            let questionbackgroundImage = json["background_image"] as! String
            let questionGuidedImage = json["guided_image"] as! String
            
            question.setQuestionID(id: questionID)
            question.setCategoryId(id: categoryID)
            question.setQuestionText(text: questionText)
            question.setQuestionGuidedText(text: questionGuidedText)
            question.setQuestionAttributedText(text: questionGuidedAttributedText)
            question.setQuestionBackgroundImage(text: questionbackgroundImage)
            question.setQuestionGuidedImage(text: questionGuidedImage)
            
            let options = json["options"]
            question.answersList = options as! [[String : AnyObject]]
            ArrayLists.sharedInstance.currentQuestionList.append(question)
        }
        DispatchQueue.main.async {
            self.selectedSection = -1
            self.CURRENT_QUESTION_INDEX = 0
            self.loadNextQuestion()
            table_view.reloadData()
        }
    }
    func loadNextQuestion() {
        DispatchQueue.main.async {
            self.questionGuided_image.isHidden = true
        }
        if CURRENT_QUESTION_INDEX == QuestionAaray.count {
            //            self.view.makeToast("questionnaire complete")
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SummaryVC") as! SummaryVC
            self.navigationController?.pushViewController(vc, animated: true)
            //submitQuestinnaire()
        } else {
            
        }
    }
    
    
    func submitQuestinnaire() {
        
        //self.skipQuestionnaire_btn.isHidden = false
        self.view.activityStartAnimating()
        let url = ApiUrl.SUBMIT_QUESTIONNAIRE
        let namespace = SPManager.sharedInstance.getCurrentUserNamespace()
        let answersDict = HomeSurveyVC.conertToDictionary(selectedHost: ListOfSubmittedAnswers)
        
        let params:Dictionary<String,AnyObject> = ["namespace" : namespace as AnyObject, "data" : answersDict as AnyObject ]
        print(params)
        APIManager.sharedInstance.postRequest(serviceName: url, sendData: params, success: { (response) in
            print(response)
            
            if APIManager.sharedInstance.isSuccess {
                
                let res = response["message"] as AnyObject
                SPManager.sharedInstance.saveUserPortfolioStatus(isPortfolioComplete : true)
                
                DispatchQueue.main.async {
                    if let tabbedbar = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "nav_bar_start") as? UINavigationController {
                        self.view.window?.rootViewController = tabbedbar
                    }
                }
                //MARK: - move to portfolio vc
                
            } else {
                if response["message"] != nil {
                    DispatchQueue.main.async {
                        self.showAlert(response["message"] as! String)
                    }
                } else {
                    DispatchQueue.main.async {
                        self.showAlert(ConstantStrings.REQUEST_SUPPORT_FAILURE_ALERT_TEXT)
                    }
                }
            }
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
        }, failure: { (data) in
            print(data)
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
            if APIManager.sharedInstance.error400 {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
            }
            if(APIManager.sharedInstance.error401)
            {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
            }else{
                
            }
        })
        
    }
    func parseAnswerData() {
        ArrayLists.sharedInstance.currentAnswersList.removeAll()
        for jsonData in ArrayLists.sharedInstance.currentQuestionList[CURRENT_QUESTION_INDEX].answersList {
            //                let question = QuestionnaireModel()
            let answer = AnswersModel()
            
            print("jsonDATA \(jsonData)")
            let answerID = jsonData["answer_id"] as! Int
            //MARK: - parse model scaling json
            let answerHeadingText = jsonData["answer_heading"] as! String
            let answerSubHeadingText = jsonData["answer_subheading"] as! String
            let answerBodyText = jsonData["answer_body"] as! String
            let attributedAnswerSubHeadingStartIndex = jsonData["attributed_answer_subheading_start_index"] as! Int
            //MARK: - parse analytics json
            let attributedAnswerSubHeadingEndIndex = jsonData["attributed_answer_subheading_end_index"] as! Int
            let attributedAnswerBodyStartIndex = jsonData["attributed_answer_body_start_index"] as! Int
            
            let attributedAnswerBodyEndIndex = jsonData["attributed_answer_body_end_index"] as! Int
            let attributedAnswerSubheadingLink = jsonData["attributed_answer_subheading_link"] as! String
            let attributedAnswerBodyLink = jsonData["attributed_answer_body_link"] as! String
            let attributedAnswerSubheadingLinkType = jsonData["attributed_answer_subheading_link_type"] as! String
            let attributedAnswerBodyLinkType = jsonData["attributed_answer_body_link_type"] as! String
            
            answer.setAttributedAnswerSubHeadingStartIndex(index: attributedAnswerSubHeadingStartIndex)
            answer.setAttributedAnswerSubHeadingEndIndex(index: attributedAnswerSubHeadingEndIndex)
            answer.setAttributedAnswerSubHeadingLink(text: attributedAnswerSubheadingLink)
            answer.setAttributedAnswerSubHeadingLinkType(text: attributedAnswerSubheadingLinkType)
            answer.setAttributedAnswerBodyStartIndex(index: attributedAnswerBodyStartIndex)
            answer.setAttributedAnswerBodyEndIndex(index: attributedAnswerBodyEndIndex)
            answer.setAttributedAnswerBodyLink(text: attributedAnswerBodyLink)
            answer.setAttributedAnswerBodyLinkType(text: attributedAnswerBodyLinkType)
            

            
            answer.setAnswerId(id: answerID)
            answer.setAnswerHeadingText(text: answerHeadingText)
            answer.setAnswerSubHeadingText(text: answerSubHeadingText)
            answer.setAnswerBodyText(text: answerBodyText)
            answer.setIsAnswerSelected(status: false)
            ArrayLists.sharedInstance.currentAnswersList.append(answer)
            print(answer)
            
        }
        DispatchQueue.main.async {
            self.tableView.reloadData()
            print("TOTAL QUESTIONS ARE == \(ArrayLists.sharedInstance.currentQuestionList.count)")
        }
    }
    
    // MARK: - Save question data
    func submitQuestinnairee() {
        count = count+1
        
        pcount.append("aaa")
        
        // self.skipQuestionnaire_btn.isHidden = false
        // self.view.activityStartAnimating()
        let url = ApiUrl.SUBMIT_QUESTIONNAIRE
        let namespace = SPManager.sharedInstance.getCurrentUserNamespace()
        let answersDict = HomeSurveyVC.conertToDictionary(selectedHost: ListOfSubmittedAnswers)
        
        let params:Dictionary<String,AnyObject> = ["namespace" : namespace as AnyObject, "data" : answersDict as AnyObject ]
        print(params)
        //           APIManager.sharedInstance.postRequest(serviceName: url, sendData: params, success: { (response) in
        //               print(response)
        
        // if APIManager.sharedInstance.isSuccess {
        if true{
            self.saveUserData(value: summaryList, key: SPManager.sharedInstance.getCurrentUserEmail())
            //let res = response["message"] as AnyObject
            SPManager.sharedInstance.saveUserPortfolioStatus(isPortfolioComplete : true)
            let monthsToAdd = 1
            
            var newDate = Calendar.current.date(byAdding: .month, value: monthsToAdd, to: Date())
            let formatter = DateFormatter()
            formatter.dateFormat = "dd-MM-yyyy"
            let result = formatter.string(from: newDate!)
            print(result)
            UserDefaults.standard.set("\(result)" , forKey: "\(SPManager.sharedInstance.getCurrentUserEmail())date")
            
            let date = UserDefaults.standard.string(forKey: "\(SPManager.sharedInstance.getCurrentUserEmail())date")
            print(date)
            
            data = self.getUserData(key: SPManager.sharedInstance.getCurrentUserEmail()) ?? [summary]()
            DispatchQueue.main.async {
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SummaryVC") as! SummaryVC
                self.navigationController?.pushViewController(vc, animated: true)
                //                if let tabbedbar = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "nav_bar_start") as? UINavigationController {
                //                          self.view.window?.rootViewController = tabbedbar
                //                      }
            }
            // MARK: - move to portfolio vc
        } else {
            
        }
        DispatchQueue.main.async {
            self.view.activityStopAnimating()
        }
        //           }, failure: { (data) in
        //               print(data)
        //               DispatchQueue.main.async {
        //                   self.view.activityStopAnimating()
        //               }
        //               if APIManager.sharedInstance.error400 {
        //                   self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
        //               }
        //               if(APIManager.sharedInstance.error401)
        //               {
        //                   self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
        //               }else{
        //
        //               }
        //           })
        
    }
    //func conertToDictionary() -> [[String: String]] {
    //
    //        var list = [[String: String]]()
    //
    //      //  for e in selectedHost {
    //
    //            var dic = [String : String]()
    //
    //            dic["hostname"] = e.getHostName_cd()
    //
    //            dic["macaddress"] = e.getMacAdress_cd()
    //
    //            dic["ip"] = e.getHostIP_cd()
    //
    //
    //
    //            list.append(dic)
    //
    //
    //
    //        }
    //
    //        return list
    //
    //    }
    
    
}

extension HomeSurveyVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currentQuestion.option.count
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if currentQuestion.multiSelection {
            let array = currentQuestion.answer
            if array.contains((currentQuestion.option[indexPath.row])){
                //  Properties_view.layer.cornerRadius = 20
                let index = array.firstIndex(of: (currentQuestion.option[indexPath.row]))
                currentQuestion.answer.remove(at: index!)
                
            }else{
                currentQuestion.appendAswer(ans: (currentQuestion.option[indexPath.row]))
            }
            tableView.reloadData()
        }else{
            self.currentQuestion.answer = []
            self.currentQuestion.appendAswer(ans: (self.currentQuestion.option[indexPath.row]))
            self.gotoNext(indexrow: indexPath.row)
            
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "PropertyNotesHeadingTableCell") as? PropertyNotesHeadingTableCell {
            print("tableview==viewForHeaderInSection called")
            //let item = ArrayLists.sharedInstance.currentAnswersList[section]
            cell.headingName.text = getLocalizedString(key: currentQuestion.option[indexPath.row] ?? "", comment: "")
            //print(item.getAnswerHeadingText())
            cell.Properties_view.layer.cornerRadius = 20
            cell.Properties_view.layer.borderWidth = 1
            questionGuided_image.isHidden = true
            submitQuestion_btn.isHidden = true
            submitBtn_view.isHidden = true
            questoinInfo_lbl.isHidden = true
            cell.Properties_view.backgroundColor = UIColor.white
            if currentQuestion.imagName != ""{
                questionGuided_image.isHidden = false
                questionGuided_image.image = UIImage(named: currentQuestion.imagName)
            }
            if currentQuestion.multiSelection{
                questoinInfo_lbl.isHidden = false
                questoinInfo_lbl.text = "select all that apply"
                
                submitQuestion_btn.isHidden = false
                submitBtn_view.isHidden = false
            }
            let array = currentQuestion.answer
            if array.contains((currentQuestion.option[indexPath.row])){
                cell.Properties_view.backgroundColor = #colorLiteral(red: 0.4352941176, green: 0.4549019608, blue: 0.8666666667, alpha: 1)
                cell.headingName.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                
            }else{
                cell.headingName.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                cell.Properties_view.backgroundColor = UIColor.white
            }
            cell.editButton.tag = indexPath.row
            
            if ischange{
                submitQuestion_btn.isHidden = false
                submitBtn_view.isHidden = false
            }
            
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    
    //convert submitted answers arrayList to dict
    class func conertToDictionary(selectedHost : [QuestionnaireModel]) -> [[String: Int]] {
        var list = [[String: Int]]()
        
        for e in selectedHost {
            var dic = [String : Int]()
            dic["question_id"] = e.getQuestionID()
            dic["category_id"] = e.getCategoryId()
            dic["answer_id"] = e.getAnswerId()
            
            list.append(dic)
            
        }
        return list
    }
}

extension UIImageView {
    func downloaded(from url: URL, contentMode mode: UIView.ContentMode = .scaleToFill) {  // for swift 4.2 syntax just use ===> mode: UIView.ContentMode
        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() {
                self.image = image
            }
            }.resume()
    }
    func downloaded(from link: String, contentMode mode: UIView.ContentMode = .scaleToFill) {  // for swift 4.2 syntax just use ===> mode: UIView.ContentMode
        guard let url = URL(string: link) else { return }
        downloaded(from: url, contentMode: mode)
    }
}

extension HomeSurveyVC{
    func conertToDictionary(selectedHost : [SubmitQDataModel]) -> [[String: String]] {
        
        var list = [[String: String]]()
        
        
        
        for e in selectedHost {
            var dic = [String : String]()
            //
            //            dic["question_id"] = e.getHostName_cd()
            //
            //            dic["answer_id"] = e.getMacAdress_cd()
            //

            //            dic["category_id"] = e.getHostIP_cd()
            list.append(dic)
        }
        return list
    }
 
}
// New file

