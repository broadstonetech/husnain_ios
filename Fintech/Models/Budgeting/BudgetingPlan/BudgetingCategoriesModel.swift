//
//  BudgetingCategoriesModel.swift
//  Fintech
//
//  Created by broadstone on 03/07/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import Foundation
struct BudgetingCategoriesClass {
    var success: Bool
    var message: String
    var data: BudgetingCategoriesData
}

struct BudgetingCategoriesData {
    var needsBucketCategories: [String]
    var wantsBucketCategories: [String]
    var havesBucketCategories: [String]
}
