import Foundation

// MARK: - LeaderBoardModel
struct LeaderBoardModel: Codable {
    let message: Message?
}


// MARK: - Message
struct Message: Codable {
    let pastMonth, pastThreeYear, pastYear: [Past]?
    
    enum CodingKeys: String, CodingKey {
        case pastMonth = "past_month"
        case pastThreeYear = "past_three_year"
        case pastYear = "past_year"
    }
}

// MARK: - Past
struct Past: Codable {
    let portfolioID: Int?
    let stockAllocation: [StockAllocation]?
    let rank: Int
    let portfolioTitle, pastReturn: String?
    let portfolioComposition: PortfolioComposition?
    let risk: Double?
    let positiveValues: [String]?
    let negativeValues: [String]?
    
    enum CodingKeys: String, CodingKey {
        case portfolioID = "portfolio_id"
        case stockAllocation = "stock_allocation"
        case rank
        case portfolioTitle = "portfolio_title"
        case pastReturn = "return"
        case portfolioComposition = "portfolio_composition"
        case risk
        case positiveValues = "positive_values"
        case negativeValues = "negative_values"
    }
}

enum NegativeValue: String, Codable {
    case familyValues = "Family values"
    case water = "Water"
}

// MARK: - PortfolioComposition
struct PortfolioComposition: Codable {
    let esg : [Esg]?
    let derivedValues: [derivedvalues]?
    
    enum CodingKeys: String, CodingKey {
        case esg = "ESG"
        case derivedValues = "derived_values"
    }
}

// MARK: - Esg
struct Esg: Codable {
    let value: String?
    let score: Double?
    let scoreMean, scoreCalculation: String?
    let scoreImprovement: ScoreImprovement?
    
    enum CodingKeys: String, CodingKey {
        case value, score
        case scoreMean = "score_mean"
        case scoreCalculation = "score_calculation"
        case scoreImprovement = "score_improvement"
    }
}

struct derivedvalues: Codable {
    let value: String?
    let score: Double?
    let scoreMean, scoreCalculation: String?
    let scoreImprovement: ScoreImprovement?
    
    enum CodingKeys: String, CodingKey {
        case value, score
        case scoreMean = "score_mean"
        case scoreCalculation = "score_calculation"
        case scoreImprovement = "score_improvement"
    }
}


// MARK: - ScoreImprovement
struct ScoreImprovement: Codable {
}

enum PositiveValue: String, Codable {
    case animalWelfare = "Animal welfare"
    case vegan = "Vegan"
}

// MARK: - StockAllocation
struct StockAllocation: Codable {
    let name: String?
    let companies: [Companyy]?
    let value, categoryID, amount: Int?
    
    enum CodingKeys: String, CodingKey {
        case name, companies, value
        case categoryID = "category_id"
        case amount
    }
}

// MARK: - Company
struct Companyy: Codable {
    let ticker, tickerTitle: String?
    let tickerShare, tickerAmmount: Double?
    enum CodingKeys: String, CodingKey {
        case ticker
        case tickerTitle = "ticker_title"
        case tickerShare = "ticker_share"
        case tickerAmmount = "ticker_ammount"
    }
}

enum Name: String, Codable {
    case stock = "Stock"
}
