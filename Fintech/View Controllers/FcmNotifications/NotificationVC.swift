//
//  NotificationVC.swift
//  Fintech
//
//  Created by broadstone on 15/12/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import UIKit

class NotificationVC: Parent {
    
    struct NotificationData {
        let namespace: String
        let message: String
        let loc_key: String
        let time: Int64
    }
    
    struct PickerData {
        let loc_key: String
        let categoryName: String
    }
    
    let pickerViewData: [PickerData] =
    [
        PickerData.init(loc_key: FCM.NOTIFICATION_TYPE_DEFAULT, categoryName: getLocalizedString(key: "All")),
        PickerData.init(loc_key: FCM.NOTIFICATION_TYPE_CHARITY, categoryName: getLocalizedString(key: "_charity")),
        PickerData.init(loc_key: FCM.NOTIFICATION_TYPE_EDUCATION, categoryName: getLocalizedString(key: "Education")),
        PickerData.init(loc_key: FCM.NOTIFICATION_TYPE_GENERAL, categoryName: getLocalizedString(key: "_general")),
        PickerData.init(loc_key: FCM.NOTIFICATION_TYPE_LEADERBOARD, categoryName: getLocalizedString(key: "leaderboard_key")),
        PickerData.init(loc_key: FCM.NOTIFICATION_TYPE_NEWSFEED, categoryName: getLocalizedString(key: "News Feed")),
        PickerData.init(loc_key: FCM.NOTIFICATION_TYPE_PORTFOLIO, categoryName: getLocalizedString(key: "Portfolio")),
        PickerData.init(loc_key: FCM.NOTIFICATION_TYPE_SPENDING, categoryName: getLocalizedString(key: "_spending")),
        PickerData.init(loc_key: FCM.NOTIFICATION_TYPE_SPENDING_INSIGHTS, categoryName: getLocalizedString(key: "spending_insights"))
        
    ]
    
    let dropDownOptionsArray =
    [
        getLocalizedString(key: "All"),
        getLocalizedString(key: "_charity"),
        getLocalizedString(key: "Education"),
        getLocalizedString(key: "_general"),
        getLocalizedString(key: "leaderboard_key"),
        getLocalizedString(key: "News Feed"),
        getLocalizedString(key: "Portfolio"),
        getLocalizedString(key: "_spending"),
        getLocalizedString(key: "spending_insights")
    ]
    
    let dropDownOptionsIds = [0,1,2,3,4,5,6,7,8]
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UITextField!
    
    @IBOutlet weak var pickerParentView: UIView!
    @IBOutlet weak var pickerView: UIPickerView!
    
    @IBOutlet weak var notificationAvailableView: UIView!
    @IBOutlet weak var notificationNotAvailableView: UIView!
    
    var notifications = [NotificationData]()
    var searchedNotifications = [NotificationData]()
    var isSearching = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchBar.delegate = self
        searchBar.text = getLocalizedString(key: "All")
        pickerParentView.isHidden = true
        
        getUserNotifications()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        pickerView.delegate = self
        pickerView.dataSource = self
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        UNUserNotificationCenter.current().removeAllDeliveredNotifications()
        UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
        self.navigationController?.isNavigationBarHidden = false
        setNavBarTitile(title: "Back", topItem: getLocalizedString(key: "_notifications"))
    }
    
    
    @IBAction func pickerDoneBtnPressed(_ sender: UIButton) {
        pickerParentView.isHidden = true
        let selectedRow = pickerView.selectedRow(inComponent: 0)
        let data = pickerViewData[selectedRow]
        
        searchedNotifications.removeAll()
        searchBar.text = data.categoryName
        
        
        if selectedRow == 0 {
            isSearching = false
        }else {
            isSearching = true
            for item in notifications {
                if data.loc_key == FCM.NOTIFICATION_TYPE_PORTFOLIO {
                    if item.loc_key == data.loc_key || item.loc_key == FCM.NOTIFICATION_TYPE_PORTFOLIO_CREATED {
                        searchedNotifications.append(item)
                    }
                }else {
                    if item.loc_key == data.loc_key {
                        searchedNotifications.append(item)
                    }
                }
                
            }
        }
        
        
        
        tableView.reloadData()
        
    }
    
    @IBAction func pickerCancelBtnPressed(_ sender: UIButton) {
        pickerParentView.isHidden = true
    }
    
    private func getUserNotifications(){
        self.view.activityStartAnimating()
        
        let url = ApiUrl.GET_USER_NOTIFICATIONS
        let params = ["namespace": SPManager.sharedInstance.getCurrentUserNamespace()] as [String: AnyObject]
        
        APIManager.sharedInstance.postRequest(serviceName: url, sendData: params, success: { (response) in
            print(response)
            
            if APIManager.sharedInstance.isSuccess {
                DispatchQueue.main.async {
                    self.fetchUserNotificationsData(res: response)
                    self.tableView.reloadData()
                    
                }
            }else{
                
                DispatchQueue.main.async { [self] in
                    if response["message"] != nil {
                        self.showAlert(response["message"] as! String)
                    } else {
                        self.showAlert(ConstantStrings.NO_RESULT_FOUND_ALERT_TEXT)
                    }
                }
            }
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
            
            DispatchQueue.main.async { [self] in
                if notifications.count > 0 {
                    notificationAvailableView.isHidden = false
                    notificationNotAvailableView.isHidden = true
                }else {
                    notificationAvailableView.isHidden = true
                    notificationNotAvailableView.isHidden = false
                }
            }
            
        }, failure: { (data) in
            print(data)
            DispatchQueue.main.async { [self] in
                view.activityStopAnimating()
            }
            if APIManager.sharedInstance.error400 {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
            }
            if(APIManager.sharedInstance.error401)
            {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
            }else{
                self.showAlert(ConstantStrings.Error_msg_generic)
            }
            
            DispatchQueue.main.async { [self] in
                if notifications.count > 0 {
                    notificationAvailableView.isHidden = false
                    notificationNotAvailableView.isHidden = true
                }else {
                    notificationAvailableView.isHidden = true
                    notificationNotAvailableView.isHidden = false
                }
            }
        })
    }
    
    private func fetchUserNotificationsData(res: [String: AnyObject]) {
        let success = res["success"]  as! Bool
        let message = res["message"] as! String
        let data = (res["data"] as? [AnyObject]) ?? [AnyObject]()
        
        notifications.removeAll()
        
        for i in 0..<data.count {
            let item = data[i] as! [String: AnyObject]
            let namespace = item["namespace"] as! String
            let loc_key = item["loc_key"] as! String
            let message = item["message"] as! String
            let time = item["time"] as! Int64
            
            notifications.append(NotificationData.init(namespace: namespace, message: message, loc_key: loc_key, time: time))
        }
        
        notifications.sort(by: {
            $0.time > $1.time
        })
        
    }
    
    private func getFormattedCategory(from loc_key: String) -> String {
        switch loc_key {
            case FCM.NOTIFICATION_TYPE_CHARITY:
                return getLocalizedString(key: "_charity")
            
            case FCM.NOTIFICATION_TYPE_SPENDING:
                return getLocalizedString(key: "_spending")
            
            case FCM.NOTIFICATION_TYPE_PORTFOLIO:
                return getLocalizedString(key: "Portfolio")
            
            case FCM.NOTIFICATION_TYPE_NEWSFEED:
                return getLocalizedString(key: "News Feed")
            
            case FCM.NOTIFICATION_TYPE_EDUCATION:
                return getLocalizedString(key: "Education")
            
            case FCM.NOTIFICATION_TYPE_SPENDING_INSIGHTS:
                return getLocalizedString(key: "spending_insights")
            
            case FCM.NOTIFICATION_TYPE_LEADERBOARD:
                return getLocalizedString(key: "leaderboard_key")
            
            case FCM.NOTIFICATION_TYPE_PORTFOLIO_CREATED:
                return getLocalizedString(key: "Portfolio")
            
            default:
                return getLocalizedString(key: "_general")
        }
    }

}
extension NotificationVC: UITableViewDelegate, UITableViewDataSource, UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerViewData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerViewData[row].categoryName
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearching {
            return searchedNotifications.count
        }else {
            return notifications.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell") as! NotificationCell
        
        if isSearching {
            cell.messageDesc.text = searchedNotifications[indexPath.row].message
            cell.dateLabel.text = FintechUtils.sharedInstance.unixToLocalTime(timestamp: Double(searchedNotifications[indexPath.row].time))
            cell.categoryLabel.text = getFormattedCategory(from: searchedNotifications[indexPath.row].loc_key)
        }else  {
            cell.messageDesc.text = notifications[indexPath.row].message
            cell.dateLabel.text = FintechUtils.sharedInstance.unixToLocalTime(timestamp: Double(notifications[indexPath.row].time))
            cell.categoryLabel.text = getFormattedCategory(from: notifications[indexPath.row].loc_key)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isSearching {
            goToSomeVC(searchedNotifications[indexPath.row])
        }else {
            goToSomeVC(notifications[indexPath.row])
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
        pickerParentView.isHidden = false
    }
    
    
    private func goToSomeVC(_ data: NotificationData) {
        let notificationType = data.loc_key
        if !SPManager.sharedInstance.saveUserLoginStatus() {
            return
        }
        if notificationType == FCM.NOTIFICATION_TYPE_CHARITY {
            let storyboard = UIStoryboard(name: "Charity", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "CharityDashboardVC") as! CharityDashboardVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if notificationType == FCM.NOTIFICATION_TYPE_SPENDING {
            let storyboard = UIStoryboard(name: "Budgeting", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "BudgetingDashboard") as! BudgetingDashboard
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if notificationType == FCM.NOTIFICATION_TYPE_PORTFOLIO ||
                    notificationType == FCM.NOTIFICATION_TYPE_PORTFOLIO_CREATED {
            self.PopVC()
            //self.tabBarController?.tabBar.items![1].isEnabled = true
            //self.tabBarController?.selectedIndex = 1
        }
        else if notificationType == FCM.NOTIFICATION_TYPE_NEWSFEED {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "NotificationsVC") as! NotificationsVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if notificationType == FCM.NOTIFICATION_TYPE_EDUCATION {
            self.tabBarController?.tabBar.items![3].isEnabled = true
            self.tabBarController?.selectedIndex = 3
        }
        else if notificationType == FCM.NOTIFICATION_TYPE_SPENDING_INSIGHTS {
            let vc = UIStoryboard(name: "Budgeting", bundle: nil).instantiateViewController(withIdentifier: "InsightsScene")
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if notificationType == FCM.NOTIFICATION_TYPE_LEADERBOARD {
            self.PopVC()
            self.tabBarController?.tabBar.items![1].isEnabled = true
            self.tabBarController?.selectedIndex = 1
        }else if notificationType == FCM.NOTIFICATION_TYPE_GENERAL {
            
        }
    }
    
}
