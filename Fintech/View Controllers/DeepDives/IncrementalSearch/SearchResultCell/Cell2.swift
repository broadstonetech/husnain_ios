//
//  Cell2.swift
//  Vesgo-Search1
//
//  Created by Billal on 03/02/2021.
//

import UIKit

class Cell2: UITableViewCell {
    //MARK: -Outlets
    @IBOutlet weak var imageCell2: UIImageView!
    @IBOutlet weak var dataLbl: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
