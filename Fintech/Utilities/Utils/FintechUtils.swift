//
//  FintechUtils.swift
//  Fintech
//
//  Created by MacBook-Mubashar on 22/08/2019.
//  Copyright © 2019 Broadstone Technologies. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
import AVKit

class FintechUtils: NSObject {
    
    static let sharedInstance = FintechUtils()
    
    
    var DECRYPTED_SIGNATURE_STRING : String = ""
    var DEVICE_TOKEN : String = "nil"
    var MODEL_ID_TO_PURCHASE : String = ""
    var MODEL_PRICE_TO_PURCHASE : Double = 0.0
    
    var isSocialSharedURLTapped : Bool = false // check that url open of shared model in ar projection
    
    func getDeviceId() -> String {
        let deviceId = UIDevice.current.identifierForVendor?.uuidString
        return deviceId ?? ""
    }
    
    func fetchButtonTitle(button : UIButton) -> String {
        let title = button.title(for: .normal)
        return title ?? ""
    }
    
    func parseFileName(filename : String, letterToReplace : String) -> String {
        
        let fullNameArr = filename.characters.split(separator: ".")
        let firstName = String(fullNameArr[0])
        return firstName
        
        //        let separators = NSCharacterSet(charactersIn: letterToReplace)
        //        var words = filename.components(separatedBy: separators as CharacterSet) // returns a list in words
        //
        //        return words[0]
    }
    
    //UNIX to Current Time conversion
    func unixToLocalTime(timestamp : Double) -> String {
        let date = Date(timeIntervalSince1970: timestamp)
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = DateFormatter.Style.medium //Set time style
        dateFormatter.dateStyle = DateFormatter.Style.medium //Set date style
        dateFormatter.timeZone = .current
        dateFormatter.dateFormat = "MM/dd/yy HH:MM"
        let localDate = dateFormatter.string(from: date)
        return localDate
    }
    
    func ArrayToString(array: [String]) -> String {
        var formattedString = ""
        for i in 0..<array.count {
            if i < array.count-1 {
                formattedString += array[i] + ", "
            }else{
                formattedString += array[i]
            }
        }
        return formattedString
    }
    
    func getDateInUSLocale(timestamp: Int64) -> String {
        let date = Date(timeIntervalSince1970: TimeInterval(timestamp))
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT") //Set timezone that you want
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "MM/dd/yy" //Specify your format that you want
        let strDate = dateFormatter.string(from: date)
        
        return strDate
    }
    
    func getOnlyDate(timestamp: Int64) -> String {
        let date = Date(timeIntervalSince1970: TimeInterval(timestamp))
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT") //Set timezone that you want
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "dd" //Specify your format that you want
        let strDate = dateFormatter.string(from: date)
        
        return strDate
    }
    
    func getMonth(timestamp: Int64) -> String {
        let date = Date(timeIntervalSince1970: TimeInterval(timestamp))
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT") //Set timezone that you want
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "MMM" //Specify your format that you want
        let strDate = dateFormatter.string(from: date)
        
        return strDate
    }
    
    func getShortDate(fromSeconds timestamp: Int64) -> String {
        let date = Date(timeIntervalSince1970: TimeInterval(timestamp))
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT") //Set timezone that you want
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "MMM-yy" //Specify your format that you want
        let strDate = dateFormatter.string(from: date)
        
        return strDate
    }
    
    func openUrl(url: String){
        if let Url = URL(string: url) {
            UIApplication.shared.open(Url)
        }
    }
    
    func getColoredBackgroundForBudgeting(rowPosition: Int) -> UIColor{
        if rowPosition % 14 == 0  {
            return #colorLiteral(red: 0.09951760109, green: 0.5861195266, blue: 0.4112889913, alpha: 1)
        }
        else if rowPosition % 14 == 1  {
            return #colorLiteral(red: 0.1607843137, green: 0.3254901961, blue: 0.6078431373, alpha: 1)
        }
        else if rowPosition % 14 == 2  {
            return #colorLiteral(red: 0.07450980392, green: 0.05882352941, blue: 0.2509803922, alpha: 1)
        }
        else if rowPosition % 14 == 3  {
            return #colorLiteral(red: 0.6784313725, green: 0.1843137255, blue: 0.1490196078, alpha: 1)
        }
        else if rowPosition % 14 == 4  {
            return #colorLiteral(red: 0.2831554339, green: 0.5120084718, blue: 0.693838948, alpha: 1)
        }
        else if rowPosition % 14 == 5  {
            return #colorLiteral(red: 0.1725490196, green: 0.3254901961, blue: 0.3921568627, alpha: 1)
        }
        else if rowPosition % 14 == 6  {
            return #colorLiteral(red: 0.2901960784, green: 0, blue: 0.8784313725, alpha: 1)
        }
        else if rowPosition % 14 == 7  {
            return #colorLiteral(red: 0.3058823529, green: 0.3294117647, blue: 0.7843137255, alpha: 1)
        }
        else if rowPosition % 14 == 8  {
            return #colorLiteral(red: 0.5324445365, green: 0.5874769423, blue: 0.2198536716, alpha: 1)
        }
        else if rowPosition % 14 == 9  {
            return #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
        }
        else if rowPosition % 14 == 10  {
            return #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1)
        }
        else if rowPosition % 14 == 11  {
            return #colorLiteral(red: 0.1960784346, green: 0.3411764801, blue: 0.1019607857, alpha: 1)
        }
        else if rowPosition % 14 == 12  {
            return #colorLiteral(red: 0.5058823824, green: 0.3372549117, blue: 0.06666667014, alpha: 1)
        }
        else if rowPosition % 14 == 13  {
            return #colorLiteral(red: 0.06359129788, green: 0.2587811735, blue: 0.4075724284, alpha: 1)
        }
        else {
            return #colorLiteral(red: 0.2823529412, green: 0.3294117647, blue: 0.3803921569, alpha: 1)
        }
    }
    
    func getColoredBackgroundPortfolioComposition(rowPosition: Int) -> UIColor{
        if rowPosition % 20 == 0  {
            return #colorLiteral(red: 0.09951760109, green: 0.5861195266, blue: 0.4112889913, alpha: 1)
        }
        else if rowPosition % 20 == 1  {
            return #colorLiteral(red: 0.1607843137, green: 0.3254901961, blue: 0.6078431373, alpha: 1)
        }
        else if rowPosition % 20 == 2  {
            return #colorLiteral(red: 0.07450980392, green: 0.05882352941, blue: 0.2509803922, alpha: 1)
        }
        else if rowPosition % 20 == 3  {
            return #colorLiteral(red: 0.6784313725, green: 0.1843137255, blue: 0.1490196078, alpha: 1)
        }
        else if rowPosition % 20 == 4  {
            return #colorLiteral(red: 0.2831554339, green: 0.5120084718, blue: 0.693838948, alpha: 1)
        }
        else if rowPosition % 20 == 5  {
            return #colorLiteral(red: 0.1725490196, green: 0.3254901961, blue: 0.3921568627, alpha: 1)
        }
        else if rowPosition % 20 == 6  {
            return #colorLiteral(red: 0.2901960784, green: 0, blue: 0.8784313725, alpha: 1)
        }
        else if rowPosition % 20 == 7  {
            return #colorLiteral(red: 0.3058823529, green: 0.3294117647, blue: 0.7843137255, alpha: 1)
        }
        else if rowPosition % 20 == 8  {
            return #colorLiteral(red: 0.5324445365, green: 0.5874769423, blue: 0.2198536716, alpha: 1)
        }
        else if rowPosition % 20 == 9  {
            return #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
        }
        else if rowPosition % 20 == 10  {
            return #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1)
        }
        else if rowPosition % 20 == 11  {
            return #colorLiteral(red: 0.1960784346, green: 0.3411764801, blue: 0.1019607857, alpha: 1)
        }
        else if rowPosition % 20 == 12  {
            return #colorLiteral(red: 0.5058823824, green: 0.3372549117, blue: 0.06666667014, alpha: 1)
        }
        else if rowPosition % 20 == 13  {
            return #colorLiteral(red: 0.06359129788, green: 0.2587811735, blue: 0.4075724284, alpha: 1)
        }else if rowPosition % 20 == 14  {
            return #colorLiteral(red: 1, green: 0.7568627451, blue: 0.02745098039, alpha: 1)
        }else if rowPosition % 20 == 15  {
            return #colorLiteral(red: 0, green: 0.5882352941, blue: 0.5333333333, alpha: 1)
        }else if rowPosition % 20 == 16  {
            return #colorLiteral(red: 1, green: 0.4352941176, blue: 0, alpha: 1)
        }else if rowPosition % 20 == 17  {
            return #colorLiteral(red: 0.521568656, green: 0.1098039225, blue: 0.05098039284, alpha: 1)
        }else if rowPosition % 20 == 18  {
            return #colorLiteral(red: 0.9098039269, green: 0.4784313738, blue: 0.6431372762, alpha: 1)
        }else if rowPosition % 20 == 19  {
            return #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        }
        else {
            return #colorLiteral(red: 0.2823529412, green: 0.3294117647, blue: 0.3803921569, alpha: 1)
        }
    }
    
    func getColoredBackgroundForNeedsCategories(rowPosition: Int) -> UIColor{
        if rowPosition % 7 == 0  {
            return #colorLiteral(red: 0.09951760109, green: 0.5861195266, blue: 0.4112889913, alpha: 1)
        }
        else if rowPosition % 7 == 1  {
            return #colorLiteral(red: 0.262745098, green: 0.6274509804, blue: 0.2784313725, alpha: 1)
        }
        else if rowPosition % 7 == 2  {
            return #colorLiteral(red: 0.4039215686, green: 0.2274509804, blue: 0.7176470588, alpha: 1)
        }
        else if rowPosition % 7 == 3  {
            return #colorLiteral(red: 0.1294117647, green: 0.5882352941, blue: 0.9529411765, alpha: 1)
        }
        else if rowPosition % 7 == 4  {
            return #colorLiteral(red: 0, green: 0.5882352941, blue: 0.5333333333, alpha: 1)
        }
        else {
            return #colorLiteral(red: 1, green: 0.7568627451, blue: 0.02745098039, alpha: 1)
        }
    }
    
    func getColoredBackgroundForWantsCategories(rowPosition: Int) -> UIColor{
        if rowPosition % 5 == 0  {
            return #colorLiteral(red: 0.2470588235, green: 0.5960784314, blue: 0.862745098, alpha: 1)
        }
        else if rowPosition % 5 == 1  {
            return #colorLiteral(red: 0, green: 0.5921568627, blue: 0.6549019608, alpha: 1)
        }
        else if rowPosition % 5 == 2  {
            return #colorLiteral(red: 0.2196078431, green: 0.5568627451, blue: 0.2352941176, alpha: 1)
        }
        else if rowPosition % 5 == 3  {
            return #colorLiteral(red: 0.9843137255, green: 0.7529411765, blue: 0.1764705882, alpha: 1)
        }
        else if rowPosition % 5 == 4  {
            return #colorLiteral(red: 0.9607843137, green: 0.4862745098, blue: 0, alpha: 1)
        }
        else {
            return #colorLiteral(red: 0.4823529412, green: 0.1215686275, blue: 0.6352941176, alpha: 1)
        }
    }
    
    func getColoredBackgroundForHavesCategories(rowPosition: Int) -> UIColor{
        if rowPosition % 4 == 0  {
            return #colorLiteral(red: 0.1019607843, green: 0.137254902, blue: 0.4941176471, alpha: 1)
        }
        else if rowPosition % 4 == 1  {
            return #colorLiteral(red: 0.05098039216, green: 0.2784313725, blue: 0.631372549, alpha: 1)
        }
        else if rowPosition % 4 == 2  {
            return #colorLiteral(red: 0, green: 0.3764705882, blue: 0.3921568627, alpha: 1)
        }
        else if rowPosition % 4 == 3  {
            return #colorLiteral(red: 0.1058823529, green: 0.368627451, blue: 0.1254901961, alpha: 1)
        }
        else {
            return #colorLiteral(red: 1, green: 0.4352941176, blue: 0, alpha: 1)
        }
    }
    
    
    func getCaetgoryImageForBudgeting(value: String) -> UIImage {
        if (value.contains("mortgage")){
            
            return UIImage(named: "home_maintaincne_mortage_rent")!
            
        }
        else if (value.contains("groceries")){
            
            return UIImage(named: "grocieries")!
            
        }
        else if (value.contains("utilities")){
            
            return UIImage(named: "utilities")!
            
        }
        else if (value.contains("fuel")){
            
            return UIImage(named: "fuel")!
            
        }
        else if (value.contains("phone")){
            
            return UIImage(named: "phone_svg")!
            
        }
        else if (value.contains("education")){
            
            return UIImage(named: "education")!
            
        }
        else if (value.contains("health")){
            
            return UIImage(named: "healthcare")!
            
        }
        else if (value.contains("loan")){
            
            return UIImage(named: "loan_svg")!
            
        }
        else if (value.contains("charity")){
            
            return UIImage(named: "charity_svg")!
            
        }
        else if (value.contains("savings")){
            
            return UIImage(named: "saving_svg")!
            
        }
        else if (value.contains("insurance")){
            
            return UIImage(named: "insurance")!
            
        }
        else if (value.contains("shopping")){
            
            return UIImage(named: "shopping_svg")!
            
        }
        else if (value.contains("dining")){
            
            return UIImage(named: "dinning_out")!
            
        }
        else if (value.contains("hobbies")){
            
            return UIImage(named: "hobbies_svg")!
            
        }
        else if (value.contains("streaming")){
            
            return UIImage(named: "streaming_services")!
            
        }
        else if (value.contains("travel")){
            
            return UIImage(named: "travel_svg")!
            
        }
        else{
            return UIImage(named: "miscellaneous")!
        }
    }
    
    
    func getCaetgoryColoredImageForBudgeting(value: String) -> UIImage {
        if (value.contains("mortgage")){
            
            return UIImage(named: "mortgage_colored")!
            
        }
        else if (value.contains("groceries")){
            
            return UIImage(named: "groceries_colored")!
            
        }
        else if (value.contains("utilities")){
            
            return UIImage(named: "utlilities_colored")!
            
        }
        else if (value.contains("fuel")){
            
            return UIImage(named: "fuel_trans_colored")!
            
        }
        else if (value.contains("phone")){
            
            return UIImage(named: "phone_internet_colored")!
            
        }
        else if (value.contains("education")){
            
            return UIImage(named: "education_colored")!
            
        }
        else if (value.contains("health")){
            
            return UIImage(named: "healthcare_colored")!
            
        }
        else if (value.contains("loan")){
            
            return UIImage(named: "loan_colored")!
            
        }
        else if (value.contains("charity")){
            
            return UIImage(named: "charity_colored")!
            
        }
        else if (value.contains("savings")){
            
            return UIImage(named: "savings_colored")!
            
        }
        else if (value.contains("insurance")){
            
            return UIImage(named: "insurance_colored")!
            
        }
        else if (value.contains("shopping")){
            
            return UIImage(named: "shopping_colored")!
            
        }
        else if (value.contains("dining")){
            
            return UIImage(named: "dining_out_colored")!
            
        }
        else if (value.contains("hobbies")){
            
            return UIImage(named: "hobbies_colored")!
            
        }
        else if (value.contains("streaming")){
            
            return UIImage(named: "streaming_colored")!
            
        }
        else if (value.contains("travel")){
            
            return UIImage(named: "travel_colored")!
            
        }
        else{
            return UIImage(named: "misc_colored")!
        }
    }
    
    
    func doubleToRoundedOutput(doubleVal: Double) -> String {
        let roundedValue = (doubleVal * 100).rounded(.toNearestOrEven) / 100
        
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.maximumFractionDigits = 2
        formatter.minimumFractionDigits = 2
        formatter.decimalSeparator = "."
        formatter.groupingSeparator = ","
        return formatter.string(from: NSNumber(value: roundedValue))!
        
        //return String(roundedValue)
        
    }
    
    func doubleToPercentageOutput(doubleVal: Double) -> String {
        let roundedValue = (doubleVal * 100).rounded(.toNearestOrEven) / 100
        
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.maximumFractionDigits = 2
        formatter.minimumFractionDigits = 2
        formatter.decimalSeparator = "."
        formatter.groupingSeparator = .none
        return formatter.string(from: NSNumber(value: roundedValue))!
        
        //return String(roundedValue)
        
    }
 
    func parseDouble(strValue: String) -> Double {
        if strValue.isEmpty {
            return 0.0
        }
        return Double(strValue.replacingOccurrences(of: ",", with: "")
                        .replacingOccurrences(of: "$", with: "")
                        .replacingOccurrences(of: " ", with: "")
                        .replacingOccurrences(of: "%", with: ""))!
    }
    
    func getInvestmentValuesIcons(investmentValue: String) -> UIImage {
        if investmentValue == "Social"{
            return UIImage(named: "social")!
        }else if investmentValue == "Environment"{
            return UIImage(named: "environment")!
        }else if investmentValue == "Vegan"{
            return UIImage(named: "vegan")!

        }else if investmentValue == "Governance"{
            return UIImage(named: "governance")!
        }else if investmentValue == "Child labor"{
            return UIImage(named: "child lb")!
            
            
        }else if investmentValue == "Palm oil" {
            return UIImage(named: "palm oil")!
            
        }else if investmentValue == "Firearms" {
            return UIImage(named: "firearms")!
            
        }else if investmentValue == "GMO" {
            return UIImage(named: "GMO")!
            
        }else if investmentValue.contains("Animal") {
            return UIImage(named: "animal_welfare")!
            
        }else if investmentValue == "Controversial weapons" {
            return UIImage(named: "controversial_weapons")!
            
        }else if investmentValue == "Military Contract" {
            return UIImage(named: "military_contract")!
            
        }else if investmentValue.contains("Climate"){
            return UIImage(named: "climate_change")!
            
        }else if investmentValue == "Alignment with UN SDGs" {
            return UIImage(named: "alignment_UN")!
            
        }else if investmentValue == "Labor rights" {
            return UIImage(named: "child lb")!
            
        }else if investmentValue == "Diversity" {
            return UIImage(named: "diversity")!
            
        }else if investmentValue == "Civil liberties" {
            return UIImage(named: "civil_liberties")!
            
        }else if investmentValue == "Water" {
            return UIImage(named: "water100x100")!
            
        }else if investmentValue == "Wind power - positive" {
            return UIImage(named: "wind_power")!
            
        }else if investmentValue == "Carbon emissions" {
            return UIImage(named: "carbon_emission")!
            
        }else if investmentValue == "Stem cell research" {
            return UIImage(named: "stem_cell_research")!
            
        }else if investmentValue == "Alcohol" {
            return UIImage(named: "Alcohol-1")!
            
        }else if investmentValue == "Adult Entertainment" {
             return UIImage(named: "adult")!
            
        }else if investmentValue == "Contraceptives" {
            return UIImage(named: "contraceptives")!
            
        }else if investmentValue == "Abortion" {
            return UIImage(named: "Abortion")!
            
        }else if investmentValue == "Maternity leave (rights)" {
            return UIImage(named: "maternity_leaves")!
            
        }else if investmentValue.contains("Family") {
            return UIImage(named: "family_values")!
            
        }else if investmentValue == "Immigration" {
            return UIImage(named: "Immigration")!
            
        }else if investmentValue == "LGBTQ" {
            return UIImage(named: "LGBTQ")!
            
        }else if investmentValue.contains("Gender") {
            return UIImage(named: "gender_equality")!
            
        }else if investmentValue == "Minority owned" {
            return UIImage(named: "minority_owned")!
            
        }else if investmentValue == "Interest bearing instruments" {
            return UIImage(named: "interest_bearing_instruments")!
            
        }else if investmentValue == "Coal" {
            return UIImage(named: "Coal")!
        }else if investmentValue == "Nuclear Power" {
            return UIImage(named: "Nuclear_Power")!
        }else if investmentValue == "Pesticides" {
            return UIImage(named: "Pesticides")!
        }else if investmentValue == "Tobacco" {
            return UIImage(named: "Tobacco")!
        }
        else if investmentValue == "Fur leather" {
            return UIImage(named: "fur_leather")!
        }
        else if investmentValue == "Gambling" {
            return UIImage(named: "Gambling")!
        }else if investmentValue == "Clean energy" {
            return UIImage(named: "green_energy")!
        }else if investmentValue.contains("Firearms") {
            return UIImage(named: "firearms")!
        }else if investmentValue.contains("Air and water") {
            return UIImage(named: "air_water_pollution")!
        }else if investmentValue.contains("Biodiversity"){
            return UIImage(named: "biodiversity")!
        }else if investmentValue.contains("Deforestation"){
            return UIImage(named: "deforestation")!
        }else if investmentValue.contains("Biodiversity"){
            return UIImage(named: "biodiversity")!
        }else if investmentValue.contains("Energy efficiency"){
            return UIImage(named: "energy_efficiency")!
        }else if investmentValue.contains("Waste management"){
            return UIImage(named: "waste_management")!
        }else if investmentValue.contains("Water scarcity"){
            return UIImage(named: "water_scarcity")!
        }else if investmentValue.contains("Board composition"){
            return UIImage(named: "board_composition")!
        }else if investmentValue.contains("Audit"){
            return UIImage(named: "audit_committe")!
        }else if investmentValue.contains("Bribery"){
            return UIImage(named: "bribery_corruption")!
        }else if investmentValue.contains("Executive compensation"){
            return UIImage(named: "executive_compensation")!
        }else if investmentValue.contains("Lobbying"){
            return UIImage(named: "lobbying")!
        }else if investmentValue.contains("Political contribution"){
            return UIImage(named: "political_contributions")!
        }else if investmentValue.contains("Whistle blower"){
            return UIImage(named: "whistle_blower_schemes")!
        }
        return UIImage(named: "environment")!
    }
    
    func getInvestmentStrategySelectedAnswerIDs(selectedAnswers: [Int]) -> [String] {
        var ids = [String]()
        for i in 0..<selectedAnswers.count {
            if selectedAnswers[i] == 1 {
                ids.append("a")
            }else if selectedAnswers[i] == 2 {
                ids.append("b")
            }else if selectedAnswers[i] == 3 {
                ids.append("c")
            }else if selectedAnswers[i] == 4 {
                ids.append("d")
            }else if selectedAnswers[i] == 5 {
                ids.append("e")
            }else if selectedAnswers[i] == 6 {
                ids.append("f")
            }else if selectedAnswers[i] == 7 {
                ids.append("g")
            }else if selectedAnswers[i] == 8 {
                ids.append("h")
            }else if selectedAnswers[i] == 9 {
                ids.append("i")
            }else if selectedAnswers[i] == 10 {
                ids.append("j")
            }else if selectedAnswers[i] == 11 {
                ids.append("k")
            }else if selectedAnswers[i] == 12 {
                ids.append("l")
            }else if selectedAnswers[i] == 13 {
                ids.append("m")
            }else if selectedAnswers[i] == 14 {
                ids.append("n")
            }
        }
        
        return ids
    }
    
    func getBucketIcon(bucketType: String) -> UIImage {
        if bucketType == "needs" {
            return UIImage(named: "needs")!
        }else if bucketType == "wants" {
            return UIImage(named: "wants")!
        }else if bucketType == "haves" {
            return UIImage(named: "haves")!
        }else {
            return UIImage(named: "miscellaneous")!
        }
    }
    
    func getImpactForTickerSearch(_ zScore: Double) -> String {
        if zScore <= -1.282 {
            return getLocalizedString(key: "_excellent")
        }else if zScore > -1.282 && zScore <= -0.674 {
            return getLocalizedString(key: "_good")
        }else if zScore > -0.674 && zScore <= 0.0 {
            return getLocalizedString(key: "_average")
        }else if zScore > 0 && zScore <= 0.674 {
            return getLocalizedString(key: "below_average")
        }else {
            return getLocalizedString(key: "needs_improvement")
        }
    }
    
    func getImpactForPortfolio(_ zScore: Double) -> String {
        if zScore == 5.0 {
            return getLocalizedString(key: "_excellent")
        }else if zScore == 4.0 {
            return getLocalizedString(key: "_good")
        }else if zScore == 3.0 {
            return getLocalizedString(key: "_average")
        }else if zScore == 2.0 {
            return getLocalizedString(key: "below_average")
        }else {
            return getLocalizedString(key: "needs_improvement")
        }
    }
    
    func playVideo(url: String, target vc: UIViewController) {
        let videoUrl = URL.init(string: url)

        //Create player first using your URL
        let yourplayer = AVPlayer(url: videoUrl!)

        //Create player controller and set it’s player
        let playerController = AVPlayerViewController()
        playerController.player = yourplayer


        //Final step To present controller  with player in your view controller
        vc.present(playerController, animated: true, completion: {
           playerController.player!.play()
        })
    }
    
    func getThumbnailImage(forUrl url: URL) -> UIImage? {
        
        let asset: AVAsset = AVAsset(url: url)
        let imageGenerator = AVAssetImageGenerator(asset: asset)
        do {
            let thumbnailImage = try imageGenerator.copyCGImage(at: CMTimeMake(value: 1, timescale: 60) , actualTime: nil)
            return UIImage(cgImage: thumbnailImage)
            
        } catch let error {
            print(error)
        }
            return nil
        
    }
}


