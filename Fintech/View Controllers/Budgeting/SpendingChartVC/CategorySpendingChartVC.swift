//
//  CategorySpendingChartVC.swift
//  Fintech
//
//  Created by broadstone on 13/02/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import Foundation
import UIKit
import Charts
class CategorySpendingChartVC: BudgetingParent {
    
    var spendingCategory = [ChartBucket]()
    var needsSpending = [ChartBucket]()
    var wantsSpending = [ChartBucket]()
    var havesSpending = [ChartBucket]()
    var miscSpending = [ChartBucket]()
    
    var isShowingPercentages = false
    
    var needsCount = 0, wantsCount = 0, havesCount = 0, miscCount = 0
    
    var needsAmountSum = 0.0, havesAmountSum = 0.0, wantsAmountSum = 0.0, miscAmountSum = 0.0
    
    var totalSum = 0.0
    
    var chartValuesSum = 0.0
    
    var selectedFilter = "Current"
    
    var pieChartColors = [UIColor]()
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var chartView: UIView!
    @IBOutlet weak var noDataView: UIView!
    
    override func viewDidLoad() {
        tableView.delegate = self
        tableView.dataSource = self
        //tableView.rowHeight = UITableView.automaticDimension
        //tableView.estimatedRowHeight = 200

        getCategorySpendingsData(filter: "current")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        setNavBarTitile(title: "Categories", topItem: "Categories")
    }
    
    @IBAction func changeFilterBtnPressed(_ sender: UIButton) {
        openBottomActionSheetForFilteringData()
    }
    
    
    func  getCategorySpendingsData(filter: String){
        
        
        view.activityStartAnimating()
        let url = ApiUrl.GET_CATEGORY_SPENDINGS_FOR_CHART
        let namespace = SPManager.sharedInstance.getCurrentUserNamespace()

        postManagerUser(method: "POST", isThisURLEmbdedData: false, urlString: url, parameter: ["namespace": namespace, "frequency": filter], success: {(value) in

            do {
                let res = try JSONDecoder().decode(ChartCategorySpendingModel?.self, from: value)
                DispatchQueue.main.async { [self] in
                    fetchData(spendingData: res!)
                }
                

            } catch { }

            
            DispatchQueue.main.async { [self] in
                view.activityStopAnimating()
            }

        }, failure: {(err) in

            DispatchQueue.main.async { [self] in
                view.activityStopAnimating()
            }

            if APIManager.sharedInstance.error400 {

                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)

            }

            if(APIManager.sharedInstance.error401)

            {

                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)

            }else{
                self.showAlert(ConstantStrings.Error_msg_generic)
                

            }
        })

    }
    
    private func fetchData(spendingData: ChartCategorySpendingModel){
        spendingCategory.removeAll()
        needsSpending.removeAll()
        wantsSpending.removeAll()
        havesSpending.removeAll()
        miscSpending.removeAll()
        pieChartColors.removeAll()
        
        needsAmountSum = 0.0
        havesAmountSum = 0.0
        wantsAmountSum = 0.0
        miscAmountSum = 0.0
        
        var internalIndex = 0
        for i in 0..<(spendingData.data?.needsBucket!.count)! {
            if spendingData.data?.needsBucket![i].amount != 0.0 {
                spendingCategory.append((spendingData.data?.needsBucket![i])!)
                needsSpending.append((spendingData.data?.needsBucket![i])!)
                needsAmountSum += (spendingData.data?.needsBucket![i].amount)!
                
                pieChartColors.append(FintechUtils.sharedInstance.getColoredBackgroundForNeedsCategories(rowPosition: internalIndex))
                internalIndex += 1
            }
        }
        
        internalIndex = 0
        for i in 0..<(spendingData.data?.wantsBucket!.count)! {
            if spendingData.data?.wantsBucket![i].amount != 0.0 {
                spendingCategory.append((spendingData.data?.wantsBucket![i])!)
                wantsSpending.append((spendingData.data?.wantsBucket![i])!)
                wantsAmountSum += (spendingData.data?.wantsBucket![i].amount)!
                
                pieChartColors.append(FintechUtils.sharedInstance.getColoredBackgroundForWantsCategories(rowPosition: internalIndex))
                internalIndex += 1
            }
        }
        
        internalIndex = 0
        for i in 0..<(spendingData.data?.havesBucket!.count)! {
            if spendingData.data?.havesBucket![i].amount != 0.0 {
                spendingCategory.append((spendingData.data?.havesBucket![i])!)
                havesSpending.append((spendingData.data?.havesBucket![i])!)
                havesAmountSum += (spendingData.data?.havesBucket![i].amount)!
                
                pieChartColors.append(FintechUtils.sharedInstance.getColoredBackgroundForHavesCategories(rowPosition: internalIndex))
                internalIndex += 1
            }
        }
        
        for i in 0..<(spendingData.data?.miscBucket!.count)! {
            if spendingData.data?.miscBucket![i].amount != 0.0 {
                spendingCategory.append((spendingData.data?.miscBucket![i])!)
                miscSpending.append((spendingData.data?.miscBucket![i])!)
                miscAmountSum += (spendingData.data?.miscBucket![i].amount)!
                
                pieChartColors.append(.systemTeal)
            }
        }
        
        totalSum = needsAmountSum + wantsAmountSum + havesAmountSum + miscAmountSum
        
        DispatchQueue.main.async { [self] in
            if needsSpending.count == 0 && wantsSpending.count == 0 && havesSpending.count == 0 {
                noDataView.isHidden = false
                chartView.isHidden = true
            }else {
                noDataView.isHidden = true
                chartView.isHidden = false
            }
        }
        
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
}
extension CategorySpendingChartVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if needsSpending.count > 0 {
            needsCount = 1 + needsSpending.count
        }else {
            needsCount = 0
        }
        if wantsSpending.count > 0 {
            wantsCount = 1 + wantsSpending.count
        }else {
            wantsCount = 0
        }
        
        if havesSpending.count > 0 {
            havesCount = 1 + havesSpending.count
        }else {
            havesCount = 0
        }
        if miscSpending.count > 0 {
            miscCount = 1 + miscSpending.count
        }else {
            miscCount = 0
        }
        
        if spendingCategory.isEmpty {
            return 0
        }else {
            return 1 + 1 + needsCount + wantsCount + havesCount + miscCount
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row < 1 {
            //Pie Chart
            let cell = tableView.dequeueReusableCell(withIdentifier: "PieChartCell") as! CategoryPieChartCell
            
            populatePieChart(pieChartView: cell.pieChartView)
            cell.filterBtn.setTitle(selectedFilter, for: .normal)
            cell.filterBtn.addTarget(self, action: #selector(filterBtnPressed), for: .touchUpInside)
            
            return cell
        } else if indexPath.row < 1 + 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "BucketCardsCell") as! BucketCardsCell
            
            if isShowingPercentages {
                let needPercent = (needsAmountSum / totalSum) * 100.0
                let wantsPercent = (wantsAmountSum / totalSum) * 100.0
                let havesPercent = (havesAmountSum / totalSum) * 100.0
                cell.needsAmount.text = FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: needPercent) + "%"
                cell.wantsAmount.text = FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: wantsPercent) + "%"
                cell.havesAmount.text = FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: havesPercent) + "%"
            }else {
                cell.needsAmount.text = "$" + FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: needsAmountSum)
                cell.wantsAmount.text = "$" + FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: wantsAmountSum)
                cell.havesAmount.text = "$" + FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: havesAmountSum)
            }
            
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(bucketCardsTapped))
            cell.bucketCardsStack.isUserInteractionEnabled = true
            cell.bucketCardsStack.addGestureRecognizer(tapGesture)
            
            return cell
        }
        else if indexPath.row < 1 + 1 + needsCount {
            if indexPath.row < 1 + 1 + 1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ExpenseTrackingHeadingCell") as! ExpenseTrackingHeadingCell
                
                cell.headingLabel.text = getLocalizedString(key: "_needs")
                cell.amountLabel.text = "$" + FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: needsAmountSum)
                cell.parentView.backgroundColor = UIColor(named: "NeedsBucketColor")!
                cell.bucketIcon.image = FintechUtils.sharedInstance.getBucketIcon(bucketType: "needs")
                return cell
            }
            else if needsCount > 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryRowCell") as! CategoryRowCell
                let index = indexPath.row - 1 - 1 - 1
                cell.categoryName.text = needsSpending[index].categoryName
                cell.amountSpent.text = "$" + FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: needsSpending[index].amount!) + "\n" +
                    FintechUtils.sharedInstance.doubleToPercentageOutput(doubleVal: needsSpending[index].percentage!) + "%"
                
                cell.imageBGView.backgroundColor = FintechUtils.sharedInstance.getColoredBackgroundForNeedsCategories(rowPosition: index)
                cell.imgView.image = FintechUtils.sharedInstance.getCaetgoryImageForBudgeting(value: needsSpending[index].categoryName!.lowercased())
                
                return cell
            }
                
            
        }
        else if indexPath.row < 1 + 1 + needsCount + wantsCount {
            if indexPath.row < 1 + 1 + needsCount + 1{
                let cell = tableView.dequeueReusableCell(withIdentifier: "ExpenseTrackingHeadingCell") as! ExpenseTrackingHeadingCell
                
                cell.headingLabel.text = getLocalizedString(key: "_wants")
                cell.amountLabel.text = "$" + FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: wantsAmountSum)
                cell.parentView.backgroundColor = UIColor(named: "WantsBucketColor")!
                cell.bucketIcon.image = FintechUtils.sharedInstance.getBucketIcon(bucketType: "wants")
                return cell
            }
            else if wantsCount > 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryRowCell") as! CategoryRowCell
                let index = indexPath.row - 1 - 1 - needsCount - 1
                cell.categoryName.text = wantsSpending[index].categoryName
                cell.amountSpent.text = "$" + FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: wantsSpending[index].amount!) + "\n" +
                    FintechUtils.sharedInstance.doubleToPercentageOutput(doubleVal: wantsSpending[index].percentage!) + "%"
                
                cell.imageBGView.backgroundColor = FintechUtils.sharedInstance.getColoredBackgroundForWantsCategories(rowPosition: index)
                cell.imgView.image = FintechUtils.sharedInstance.getCaetgoryImageForBudgeting(value: wantsSpending[index].categoryName!.lowercased())
                
                return cell
            }
                
            
        }
        else if indexPath.row < 1 + 1 + needsCount + wantsCount + havesCount{
            if indexPath.row < 1 + 1 + needsCount + wantsCount + 1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ExpenseTrackingHeadingCell") as! ExpenseTrackingHeadingCell
                
                cell.headingLabel.text = getLocalizedString(key: "_haves")
                cell.amountLabel.text = "$" + FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: havesAmountSum)
                cell.parentView.backgroundColor = UIColor(named: "HavesBucketColor")!
                cell.bucketIcon.image = FintechUtils.sharedInstance.getBucketIcon(bucketType: "haves")
                return cell
            }
            else if havesCount > 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryRowCell") as! CategoryRowCell
                let index = indexPath.row - 1 - 1 - needsCount - wantsCount - 1
                cell.categoryName.text = havesSpending[index].categoryName
                cell.amountSpent.text = "$" + FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: havesSpending[index].amount!) + "\n" +
                    FintechUtils.sharedInstance.doubleToPercentageOutput(doubleVal: havesSpending[index].percentage!) + "%"
                
                cell.imageBGView.backgroundColor = FintechUtils.sharedInstance.getColoredBackgroundForHavesCategories(rowPosition: index)
                cell.imgView.image = FintechUtils.sharedInstance.getCaetgoryImageForBudgeting(value: havesSpending[index].categoryName!.lowercased())
                
                return cell
            }
                
            
        }
        else if indexPath.row < 1 + 1 + needsCount + wantsCount + havesCount + miscCount {
            if indexPath.row < 1 + 1 + needsCount + wantsCount + havesCount + 1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ExpenseTrackingHeadingCell") as! ExpenseTrackingHeadingCell
                
                cell.headingLabel.text = "Uncategorized"
                cell.amountLabel.text = "$" + FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: miscAmountSum)
                cell.parentView.backgroundColor = .systemTeal
                cell.bucketIcon.image = FintechUtils.sharedInstance.getBucketIcon(bucketType: "misc")
                return cell
            }
            else if miscCount > 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryRowCell") as! CategoryRowCell
                let index = indexPath.row - 1 - 1 - needsCount - wantsCount - havesCount - 1
                cell.categoryName.text = miscSpending[index].categoryName
                cell.amountSpent.text = "$" + FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: miscSpending[index].amount!) + "\n" +
                    FintechUtils.sharedInstance.doubleToPercentageOutput(doubleVal: miscSpending[index].percentage!) + "%"
                
                cell.imageBGView.backgroundColor = .systemTeal
                cell.imgView.image = FintechUtils.sharedInstance.getCaetgoryImageForBudgeting(value: miscSpending[index].categoryName!.lowercased())
                
                return cell
            }
                
            
        }
        return UITableViewCell()
        
    }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return UITableView.automaticDimension
//    }
    
    @objc func filterBtnPressed() {
        //Filter btn pressed, open bottom sheet for filters
        openBottomActionSheetForFilteringData()
    }
    
    @objc func bucketCardsTapped(_ sender: UIStackView) {
        let cell = tableView.cellForRow(at: IndexPath.init(item: 1, section: 0)) as! BucketCardsCell
        if isShowingPercentages {
            isShowingPercentages = false
        }else {
            isShowingPercentages = true
        }
        UIView.transition(with: cell.bucketCardsStack, duration: 1, options: [.transitionFlipFromBottom], animations: nil, completion: { _ in
            self.tableView.reloadData()
        })
        
        
    }
    
   
    

    
}
extension CategorySpendingChartVC: ChartViewDelegate {
    private func populatePieChart(pieChartView: PieChartView) {
        
//        spendingCategory.sort(){
//            $1.amount! < $0.amount!
//        }
      
      // 1. Set ChartDataEntry
      var dataEntries: [ChartDataEntry] = []
        for i in 0..<spendingCategory.count {
            let dataEntry = PieChartDataEntry(value: spendingCategory[i].amount!, label: spendingCategory[i].categoryName, data: spendingCategory[i].categoryName as AnyObject)
            dataEntries.append(dataEntry)
            chartValuesSum += spendingCategory[i].amount!
      }
      // 2. Set ChartDataSet
        let pieChartDataSet = PieChartDataSet(entries: dataEntries, label: nil)
        //pieChartDataSet.colors = ChartColorTemplates.colorful()
        pieChartDataSet.colors = pieChartColors
        pieChartDataSet.drawValuesEnabled = false
        
      // 3. Set ChartData
      let pieChartData = PieChartData(dataSet: pieChartDataSet)
      let format = NumberFormatter()
        format.numberStyle = .currency
        format.maximumFractionDigits = 2
        format.minimumFractionDigits = 2
        format.decimalSeparator = "."
        format.groupingSeparator = .none
        format.multiplier = 1.0
        
      let formatter = DefaultValueFormatter(formatter: format)
      pieChartData.setValueFormatter(formatter)
      // 4. Assign it to the chart’s data
      pieChartView.data = pieChartData
        pieChartView.drawEntryLabelsEnabled = false
        pieChartView.legend.enabled = false
        pieChartView.highlightValue(Highlight.init(x: 0, dataSetIndex: 0, stackIndex: 0))
        if pieChartView.highlighted.count > 0 {
            let text = spendingCategory[pieChartView.highlighted[0].dataSetIndex].categoryName! + "\n" + FintechUtils.sharedInstance.doubleToPercentageOutput(doubleVal: spendingCategory[pieChartView.highlighted[0].dataSetIndex].percentage!) + "%"
            pieChartView.centerAttributedText = getAttributedCenterText(text: text)
            //pieChartView.centerText = spendingCategory[pieChartView.highlighted[0].dataSetIndex].categoryName
        }
        pieChartView.delegate = self
    }
    
    func getAttributedCenterText(text: String) -> NSAttributedString{
        let style = NSMutableParagraphStyle()
        style.alignment = NSTextAlignment.center
        let chartAttribute = [ NSAttributedString.Key.font: UIFont(name: "HelveticaNeue", size: 17.0)!,
                               .paragraphStyle: style]
        let chartAttrString = NSAttributedString(string: text, attributes: chartAttribute)
        
        return chartAttrString
    }

    
//    func chartValueSelected(_ chartView: PieChartView, entry: ChartDataEntry, highlight: Highlight, _ nonUseableIndex: Int) {
//        chartView.highlightValue(highlight)
//        if chartView.highlighted.count > 0 {
//            let pieChartEntry = entry as! PieChartDataEntry
//            let text = pieChartEntry.label! + "\n" + FintechUtils.sharedInstance.doubleToPercentageOutput(doubleVal: (pieChartEntry.value / chartValuesSum)*100) + "%"
//            chartView.centerAttributedText = getAttributedCenterText(text: text)
//        }
//    }
    func chartValueSelected(_ chartView: ChartViewBase, entry: ChartDataEntry, highlight: Highlight) {
        let chartView = chartView as! PieChartView
        chartView.highlightValue(highlight)
        if chartView.highlighted.count > 0 {
            let pieChartEntry = entry as! PieChartDataEntry
            let text = pieChartEntry.label! + "\n" + FintechUtils.sharedInstance.doubleToPercentageOutput(doubleVal: (pieChartEntry.value / chartValuesSum)*100) + "%"
            chartView.centerAttributedText = getAttributedCenterText(text: text)
        }
    }

}

extension CategorySpendingChartVC {
    private func openBottomActionSheetForFilteringData() {
        let optionMenu = UIAlertController(title: nil, message: "Spending categorized by date range", preferredStyle: .actionSheet)
        
        
        let current = UIAlertAction(title: "Current", style: .default, handler: { [self]
            (alert: UIAlertAction!) -> Void in
            getCategorySpendingsData(filter: "current")
            selectedFilter = "Current"
            
            setAnalyticsAction(ScreenName: "CategorySpendingChartVC", method: "CategorySpendingsDataCurrent")
            
        })
        
        
        let threeMonth = UIAlertAction(title: "3 months", style: .default, handler: { [self]
            (alert: UIAlertAction!) -> Void in
            getCategorySpendingsData(filter: "3months")
            selectedFilter = "3 months"
            
            setAnalyticsAction(ScreenName: "CategorySpendingChartVC", method: "CategorySpendingsData3months")
            
        })
        
        let ytd = UIAlertAction(title: "YTD", style: .default, handler: { [self]
            (alert: UIAlertAction!) -> Void in
            getCategorySpendingsData(filter: "ytd")
            selectedFilter = "YTD"
            
            setAnalyticsAction(ScreenName: "CategorySpendingChartVC", method: "CategorySpendingsDataYTD")
        })
        let year = UIAlertAction(title: "1 year", style: .default, handler: { [self]
            (alert: UIAlertAction!) -> Void in
            
            getCategorySpendingsData(filter: "1year")
            selectedFilter = "1 year"
            setAnalyticsAction(ScreenName: "CategorySpendingChartVC", method: "CategorySpendingsData1Year")
            
        })
        
        let all = UIAlertAction(title: "All", style: .default, handler: { [self]
            (alert: UIAlertAction!) -> Void in
            
            getCategorySpendingsData(filter: "all")
            selectedFilter = "All"
            setAnalyticsAction(ScreenName: "CategorySpendingChartVC", method: "CategorySpendingsDataALL")
            
        })
        
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            
            self.dismiss(animated: true, completion: nil)
            
        })
        
        if selectedFilter == "All" {
            setSelectedCheckmark(action: all)
        }else if selectedFilter == "1 year" {
            setSelectedCheckmark(action: year)
        } else if selectedFilter == "YTD" {
            setSelectedCheckmark(action: ytd)
        } else if selectedFilter == "3 months" {
            setSelectedCheckmark(action: threeMonth)
        }
        else {
            setSelectedCheckmark(action: current)
        }
        
        
        optionMenu.addAction(current)
        optionMenu.addAction(threeMonth)
        optionMenu.addAction(ytd)
        optionMenu.addAction(year)
        optionMenu.addAction(all)
        optionMenu.addAction(cancel)
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    private func setSelectedCheckmark(action: UIAlertAction){
        
        if #available(iOS 13.0, *) {
            action.setValue(UIImage(systemName: "checkmark"), forKey: "image")
        } else {
            // Fallback on earlier versions
        }
        
    }
}
