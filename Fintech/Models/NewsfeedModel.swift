//
//  NewsfeedModel.swift
//  Fintech
//
//  Created by Apple on 04/05/2020.
//  Copyright © 2020 Broadstone Technologies. All rights reserved.
//

import Foundation
class newsFeedModel{
    var id = Int()
    var notificationHeading = String()
    var ticker = String()
    init(id:Int, heading:String, subheading:String) {
        self.id = id
        notificationHeading = heading
        ticker = subheading
    }
}
