//
//  Parent.swift
//  Jedi
//
//  Created by MacBook-Mubashar on 09/01/2019.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit
import Security
import CommonCrypto
import PDFKit
import Lottie
import Firebase
import FirebaseAnalytics
class Parent: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate , UINavigationControllerDelegate, UITabBarDelegate, UITabBarControllerDelegate {
    
    var navigationBarAppearace = UINavigationBar.appearance()
    
    var questionnaireStatus: QuestionnaireStatusModel!
    
    
    //    var socket = WebSocket(url: URL(string: ApiUrl.WEB_SOCKET_URL)!)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let appearance = UINavigationBarAppearance()
        appearance.configureWithOpaqueBackground()
        appearance.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        appearance.backgroundColor = UIColor(named: "AppMainColor")!
        self.navigationController?.navigationBar.barTintColor = UIColor(named: "AppMainColor")!
        navigationController?.navigationBar.standardAppearance = appearance
        navigationController?.navigationBar.scrollEdgeAppearance = appearance
        hideKeyboardWhenTappedAround()
        // Do any additional setup after loading the view.
        
        //self.navigationController?.navigationItem.backBarButtonItem?.title = ""
        setAnalytics()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        setLandscapeOrientation(false)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        setLandscapeOrientation(false)
    }
    
    
    func getThumbnailImage(forUrl url: URL) -> UIImage? {
        
        let asset: AVAsset = AVAsset(url: url)
        let imageGenerator = AVAssetImageGenerator(asset: asset)
        do {
            let thumbnailImage = try imageGenerator.copyCGImage(at: CMTimeMake(value: 1, timescale: 60) , actualTime: nil)
            return UIImage(cgImage: thumbnailImage)
            
        } catch let error {
            print(error)
        }
            return nil
        
    }

    
    
    
    //MARK: - UI AlertControllers
    
    func navBarCustomisation() {
        
        //        navigationBarAppearace.tintColor = UIColorfromhe(0xffffff)
        //        navigationBarAppearace.barTintColor = uicolorFromHex(0x034517)
    }
    
    func setNavBarTitile(title : String, topItem : String) {
        
        self.navigationItem.title = title
        if title == "" {
            self.navigationController?.navigationBar.topItem?.title = title
            self.navigationItem.title = topItem
        }else{
            self.navigationController?.navigationBar.topItem?.title = topItem
            if title == "" {
                self.navigationItem.title = title
            }else{
                self.navigationItem.title = topItem
            }
        }
        self.navigationController?.navigationBar.tintColor = UIColor.init(red: 255, green: 255, blue: 255, alpha: 1)
        
        self.navigationController?.navigationBar.fixAppearance()
    }
    
    //MARK: - UI AlertControllers
    func showAlert(_ message: String) {
        DispatchQueue.main.async {
            self.showAlertDialog(message)
        }
    }
    
    func showAlert(_ message: String, action: UIAlertAction) {
        DispatchQueue.main.async {
            self.showAlertDialog(message, action: action)
        }
    }
    
    
    
    private func showAlertDialog(_ message:String)
    {
        let alertController = UIAlertController(title: ConstantStrings.appName, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: getLocalizedString(key: "ok", comment: ""), style: .default) { (action) in
            alertController.dismiss(animated: true, completion: nil)
        }
        alertController.addAction(OKAction)
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = NSTextAlignment.left
        
        let messageText = NSMutableAttributedString(
            string: message,
            attributes: [
                NSAttributedString.Key.paragraphStyle: paragraphStyle,
                NSAttributedString.Key.font: UIFont.systemFont(ofSize: 13.0)
            ]
        )
        
        alertController.setValue(messageText, forKey: "attributedMessage")
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    private func showAlertDialog(_ message:String, action: UIAlertAction)
    {
        let alertController = UIAlertController(title: ConstantStrings.appName, message: message, preferredStyle: .alert)
        
        alertController.addAction(action)
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = NSTextAlignment.left
        
        let messageText = NSMutableAttributedString(
            string: message,
            attributes: [
                NSAttributedString.Key.paragraphStyle: paragraphStyle,
                NSAttributedString.Key.font: UIFont.systemFont(ofSize: 13.0)
            ]
        )
        
        alertController.setValue(messageText, forKey: "attributedMessage")
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    //MARK: - form validations
    func isValidEmail(_ testStr:String) -> Bool {
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
        
    }
    
    //    1 - Password length is 8.
    //    2 - One Alphabet in Password.
    //    3 - One Special Character in Password.
    //
    //    ^                              - Start Anchor.
    //    (?=.*[a-z])              -Ensure string has one character.
    //    (?=.[$@$#!%?&])   -Ensure string has one special character.
    //    {8,}                            -Ensure password length is 8.
    //    $                               -End Anchor.
    func isPasswordValid(_ password : String) -> Bool {
        
        let passwordRegex = "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*()\\-_=+{}|?>.<,:;~`’]{8,}$"
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", passwordRegex)
        return passwordTest.evaluate(with: password)
    }
    
    //MARK:- ClassFunc -- show/hide pwd
    func showHidePassword(textfield : UITextField, btnPwd : UIButton) {
        if textfield.isSecureTextEntry {
            textfield.isSecureTextEntry = false
            if #available(iOS 13.0, *) {
                
                //MARK: - uncomment jab sir ko send karna ha
                      btnPwd.setBackgroundImage(UIImage(named: "eyevisibility"), for: UIControl.State.normal)
            } else {
                // Fallback on earlier versions 
                btnPwd.setBackgroundImage(UIImage(named: "eyevisibility"), for: UIControl.State.normal)
            }
        } else {
            textfield.isSecureTextEntry = true
            if #available(iOS 13.0, *) {
                 //MARK: - uncomment jab sir ko send karna ha
                         btnPwd.setBackgroundImage(UIImage(named: "eyeinvisible"), for: UIControl.State.normal)
                
                   } else {
                //eyevisibility
                       // Fallback on earlier versions
                       btnPwd.setBackgroundImage(UIImage(named: "eyeinvisible"), for: UIControl.State.normal)
                   }        }
    }
    
    //MARK:- ClassFunc -- show/hide pwd

    func showHidePwd(textfield : UITextField, btnPwd : UIButton) {
        if textfield.isSecureTextEntry {
            textfield.isSecureTextEntry = false
            if #available(iOS 13.0, *) {
                
                //MARK: - uncomment jab sir ko send karna ha  visibility
                btnPwd.setBackgroundImage(UIImage(named: "invisible"), for: UIControl.State.normal)
            } else {
                // Fallback on earlier versions
                btnPwd.setBackgroundImage(UIImage(named: "invisible"), for: UIControl.State.normal)
            }
        } else {
            textfield.isSecureTextEntry = true
            if #available(iOS 13.0, *) {
                //MARK: - uncomment jab sir ko send karna ha  invisible
                btnPwd.setBackgroundImage(UIImage(named: "visibility"), for: UIControl.State.normal)
                
            } else {
                //eyevisibility
                // Fallback on earlier versions
                btnPwd.setBackgroundImage(UIImage(named: "visibility"), for: UIControl.State.normal)
            }        }
    }
    
    
    //MARK: - ViewContrller slide animation
    func goToNextVC(vc : UIViewController, slide : String) {
        
        //vc = view controller where to move
        //Slide = sliding animation L,R,N (left slide, right or override)
        
    }
    
    func PushVC(vcIdentifier : String, storyboard: String? = "Main") {
        
        let storyboard = UIStoryboard(name: storyboard!, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: vcIdentifier) as UIViewController
        navigationController?.pushViewController(vc, animated: true)

    }
    func PopVC() {
        
        if let navigator = navigationController {
            navigator.popViewController(animated: true)
        }
        
    }
    
    func popAllViewsAndNavigateTo(vcIdentifier : String) {
        var currentVCStack = self.navigationController?.viewControllers
        currentVCStack?.removeAll()
        
        let nextVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: vcIdentifier)
        currentVCStack?.append(nextVC)
        
        self.navigationController?.setViewControllers(currentVCStack!, animated: true)
    }
    // MARK: - UIview animations
    func setView(view: UIView, hidden: Bool) {
        UIView.transition(with: view, duration: 0.5, options: .transitionCrossDissolve, animations: {
            view.isHidden = hidden
        })
    } //transitionCrossDissolve
    
    func viewPdfFile(urlString : String, urlType: String) {
        print("url== \(urlString)")
        DispatchQueue.main.async {
        if urlType == "video" {
            if urlString != "" {
                self.playVideo(url: URL.init(string: urlString)!)
            }
        } else if urlType == "image" {
            
        } else {
        UIApplication.shared.open(URL(string:urlString)!)
        }
        }
    }
    
    func playVideo(url: URL) {
        let player = AVPlayer(url: url)

        let vc = AVPlayerViewController()
        vc.player = player

        self.present(vc, animated: true) { vc.player?.play() }
    }
    
    func shareSocialURL(url : String) {
        print("Social URL == \(url)")
        let items = [URL(string: url)!]
        let ac = UIActivityViewController(activityItems: items, applicationActivities: nil)
        
        if let popoverController = ac.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
        }
        
        present(ac, animated: true)
    }
    
    func shareModelURLwithText (shareText : String) {
        let image = UIImage(named: "add")
        SocialShare.sharedInstance.socialShare(sender: self, sView: self.view, sharingText: shareText, sharingImage: image, sharingURL: URL(string: "https://broadstonetech.com"), sharingURL1: URL(string: "https://catalisse.com")) { (activityType, success, info, error) in
            print("socially shared successfully")
        }
    }
    
    func highlightAttributedtext(stringText : String, linkValue : String, startIndex : Int, endIndex : Int) -> NSMutableAttributedString {
        let attributedString = NSMutableAttributedString(string: stringText)
        attributedString.addAttribute(.link, value: linkValue, range: NSRange(location: startIndex, length: endIndex))
        return attributedString
    }
    
    func getTimestamp() -> String {
        return String(describing: Int(NSDate().timeIntervalSince1970))
    }
    
    func getAppVersionString() -> String {
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            if let build = Bundle.main.infoDictionary?["CFBundleVersion"] as? String {
                return getLocalizedString(key: "version_", comment: "") + " \(version)(\(build))"
            } else {
                return getLocalizedString(key: "version_", comment: "") + " \(version)"
            }
        }
        return ""
    }
    
    // MARK: - Alert for payment confirmation
    
    func openActionSheet() {
        
        let alertController = UIAlertController(title: ConstantStrings.appName, message: ConstantStrings.PURCHSAE_POPUP_TEXT, preferredStyle: .actionSheet)
        
        let applePayAction = UIAlertAction(title: "Pay with Apple", style: .default, handler: { (alert: UIAlertAction!) -> Void in
            
        })
        
        let stripePayAction = UIAlertAction(title: "Pay with Stripe", style: .default, handler: { (alert: UIAlertAction!) -> Void in
            
            
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: { (alert: UIAlertAction!) -> Void in
            //  Do something here upon cancellation.
        })
        
        alertController.addAction(applePayAction)
        alertController.addAction(stripePayAction)
        alertController.addAction(cancelAction)
        //
        if let popoverController = alertController.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
        }
        
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    //Play Animations
    func playAnimation(animationView: AnimationView, playInLoop: Bool){
        animationView.contentMode = .scaleAspectFit
        animationView.animationSpeed = 1
        animationView.play()
        if playInLoop {
            animationView.loopMode = .loop
        }
    }
    
    
    //Share Intent
    func shareTextualData(textData: String){
        let activityViewController = UIActivityViewController(activityItems: [textData], applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        // exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop ]
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    //MARK: - Firebase Analytics
    func setAnalyticsAction(ScreenName: String, method: String){
        let screenClass = classForCoder.description()
        Analytics.logEvent(AnalyticsEventScreenView, parameters: [AnalyticsParameterScreenName: ScreenName, AnalyticsParameterScreenClass: screenClass, "Method" : method, "Namespace" : SPManager.sharedInstance.getCurrentUserNamespace()])
       }

       //This method send analytics data automatically in each class when class is called

    func setAnalytics(){

        let screenClass = classForCoder.description()
        if SPManager.sharedInstance.saveUserLoginStatus() {
            Analytics.logEvent(AnalyticsEventScreenView, parameters: [AnalyticsParameterScreenClass: screenClass, "Namespace" : SPManager.sharedInstance.getCurrentUserNamespace()])
        }else {
            Analytics.logEvent(AnalyticsEventScreenView, parameters: [AnalyticsParameterScreenClass: screenClass, "Namespace" : "Guest user"])
        }
        
       }
    
}
   

// MARK: -Promotion Video Extension
extension Parent {
    
    func playPromotionVideo(urlString : String) {
        
        let videoURL = URL(string: urlString)
        let player = AVPlayer(url: videoURL!)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        self.present(playerViewController, animated: true) {
            playerViewController.player!.play()
        }
    }
    

    
}
// MARK: - SHA! of DATA
extension Data {
    func sha1() -> String {
        do {
            var digest = [UInt8](repeating: 0, count:Int(CC_SHA1_DIGEST_LENGTH))
            self.withUnsafeBytes {
                _ = CC_SHA1($0, CC_LONG(self.count), &digest)
            }
            let hexBytes = digest.map { String(format: "%02hhx", $0) }
            return hexBytes.joined()
        } catch {
            print(error)
        }
    }
}
/*
 // MARK: - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
 // Get the new view controller using segue.destination.
 // Pass the selected object to the new view controller.
 }
 */


extension UIView {
    var heightConstaint: NSLayoutConstraint? {
        get {
            return constraints.first(where: {
                $0.firstAttribute == .height && $0.relation == .equal
            })
        }
        set { setNeedsLayout() }
    }
    var widthConstaint: NSLayoutConstraint? {
        get {            return constraints.first(where: {
            $0.firstAttribute == .width && $0.relation == .equal
        })
        }
        set { setNeedsLayout() }
    }
}
extension Parent {
    func getQuestionnaireStatusAPI() {
        self.view.activityStartAnimating()
        let url = ApiUrl.GET_QUESTIONNAIRE_STATUS
        let namespace = SPManager.sharedInstance.getCurrentUserNamespace()
        
        let params:Dictionary<String,AnyObject> =
            ["namespace" : namespace  as AnyObject]
        print(params)
        
        APIManager.sharedInstance.postRequest(serviceName: url, sendData: params, success: { [self] (response) in
            print(response)
            
            questionnaireStatus = fetchQuestionnaireStatusApiData(response: response)
            
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
            
            DispatchQueue.main.async {
                self.goToInvestmentStrategyScreen(statusModel: questionnaireStatus)
            }
                
            
        }, failure: { (data) in
            print(data)
            DispatchQueue.main.async { [self] in
                view.activityStopAnimating()
            }
            DispatchQueue.main.async {
                self.goToInvestmentStrategyScreen(statusModel: nil)
            }
        })
    }
    
    func fetchQuestionnaireStatusApiData(response: [String: AnyObject]) -> QuestionnaireStatusModel {
        let success = response["success"] as! Bool
        let message = response["message"] as! String
        
        let data = response["data"] as! [String: Bool]
        
        let demographics = data["demographics"]
        let investment = data["investment"]
        let risk = data["risk"]
        let values = data["values"]
        
        let questionnaireDataModel = QuestionnaireStatusDataModel(demographics: demographics, investment: investment, risk: risk, values: values)
        
        return QuestionnaireStatusModel(success: success, message: message, data: questionnaireDataModel)
    }
    
    func goToInvestmentStrategyScreen(statusModel: QuestionnaireStatusModel?) {
        let vc = UIStoryboard(name: "InvestmentStrategy", bundle: nil).instantiateViewController(withIdentifier: "InvestmentQuestionnaire")
        InvestmentQuestionnaire.questionnaireStatusModel = statusModel
        //let navController = UINavigationController(rootViewController: vc)
        vc.modalPresentationStyle = .fullScreen
        UIApplication.shared.keyWindow?.rootViewController = vc
        //self.navigationController?.pushViewController(vc, animated: true)
    }
}
public extension UINavigationBar {
    func fixAppearance() {
        if #available(iOS 14.0, *) {
            topItem?.backButtonDisplayMode = .minimal
        } else if #available(iOS 13.0, *) {
            let newAppearance = standardAppearance.copy()
            newAppearance.backButtonAppearance.normal.titleTextAttributes = [
                .foregroundColor: UIColor.clear,
                .font: UIFont.systemFont(ofSize: 0)
            ]
            standardAppearance = newAppearance
        } else {
            topItem?.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        }
    }
}
extension Parent {
    func setLandscapeOrientation(_ flag: Bool) {
        if flag {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.myOrientation = .landscapeLeft
                    UIDevice.current.setValue(UIInterfaceOrientation.landscapeLeft.rawValue, forKey: "orientation")
                    UIView.setAnimationsEnabled(true)
//            UIDevice.current.setValue(UIInterfaceOrientation.landscapeLeft.rawValue, forKey: "orientation")
        }else {
            //UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.myOrientation = .portrait
                    UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")
                    UIView.setAnimationsEnabled(true)
        }
    }
}
