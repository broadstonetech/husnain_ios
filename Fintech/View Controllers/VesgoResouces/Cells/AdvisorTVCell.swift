//
//  AdvisorTVCell.swift
//  EducationModuleSP
//
//  Created by SaifUllah Butt on 28/12/2021.
//

import UIKit
import TagListView

class AdvisorTVCell: UITableViewCell {

    
    @IBOutlet weak var advisorImage: UIImageView!
    @IBOutlet weak var advisorName: UILabel!
    @IBOutlet weak var advisorStatus: UILabel!
    @IBOutlet weak var advisorDescription: UILabel!
  //  @IBOutlet weak var expertise: UILabel!
    
    @IBOutlet weak var expertiseTagListView: TagListView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        expertiseTagListView.removeAllTags()
       // expertiseTagListView.addTags(["Stock", "ETF", "Crypto"])
        expertiseTagListView.textFont = UIFont.systemFont(ofSize: 14)
        expertiseTagListView.alignment = .left
        
        
//        expertise.layer.masksToBounds = true
//        expertise.layer.cornerRadius = 5
//
        
        advisorImage.layer.borderWidth = 1.0
        advisorImage.layer.masksToBounds = false
        advisorImage.layer.borderColor = UIColor.white.cgColor
        advisorImage.layer.cornerRadius = advisorImage.frame.size.width / 2
        advisorImage.clipsToBounds = true
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
