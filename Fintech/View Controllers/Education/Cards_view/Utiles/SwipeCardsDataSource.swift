//
//  SwipeCardsDataSource.swift
//  Fintech
//
//  Created by Apple on 10/07/2020.
//  Copyright © 2020 Broadstone Technologies. All rights reserved.
//

import UIKit


protocol SwipeCardsDataSource {
    func numberOfCardsToShow() -> Int
    func card(at index: Int) -> SwipeCardView
    func emptyView() -> UIView?
    
}

protocol SwipeCardsDelegate {
   
    func swipeDidEnd(on view: SwipeCardView)
}
protocol CheckLastCardDelegate {
    func checkLast()
}
