//
//  DonationCompleteVC.swift
//  Fintech
//
//  Created by broadstone on 08/01/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import Foundation
import UIKit
import Lottie

class DonationCompleteVC: PaymentsBaseClass {
    @IBOutlet weak var celebrationAnim: AnimationView!
    @IBOutlet weak var familyAnim: AnimationView!
    @IBOutlet weak var continueBtn: UIButton!
    @IBOutlet weak var shareBtn: UIButton!
    @IBOutlet weak var completionTextLabel: UILabel!
    
    override func viewDidLoad() {
        removeAllPreviousVC()
        
        playAnimation(animationView: celebrationAnim, playInLoop: true)
        playAnimation(animationView: familyAnim, playInLoop: true)
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        setCompletionLabelText()
    }
    
    private func setCompletionLabelText(){
        //You donated <$amount> to <charity name>.  Thank you. Your support will make a big impact.
    
        let text = "You donated $" + String(PaymentAPIUtils.amount) + " to " + FintechUtils.sharedInstance.ArrayToString(array: List_values) + ".\n" + "Thank you.\nYour support will make a big impact"
        completionTextLabel.text = text
    }
    
    @IBAction func continueBtnPressed(_ sender: UIButton){
        let storyboard = UIStoryboard(name: "Charity", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CharityDashboardVC") as UIViewController
        self.navigationController?.popViewController(animated: true)
        
        setAnalyticsAction(ScreenName: "CharityDashboardVC", method: "instantiate")
        
    }
    
    @IBAction func shareBtnPressed(_ sender: UIButton){
//        I donated to <charity name, charity name2> that supports <value1, value2>. Check them out and help if you can. <Vesgo link>
        var text = ""
        if SelectedUserValues.count == 0 || SelectedUserValues.isEmpty {
            text = "I donated to " + FintechUtils.sharedInstance.ArrayToString(array: List_values) + " that support " + "my values"
                + "." + " Check them out and help if you can.\n" + ConstantStrings.APP_LINK
        }else{
            text = "I donated to " + FintechUtils.sharedInstance.ArrayToString(array: List_values) + " that supports " + FintechUtils.sharedInstance.ArrayToString(array: SelectedUserValues)
                + "." + " Check them out and help if you can.\n" + ConstantStrings.APP_LINK
        }
        
        shareTextualData(textData: text)
        
        setAnalyticsAction(ScreenName: "CharityDashboardVC", method: "SharedCharity")
        
    }
    
    
    private func removeAllPreviousVC(){
        navigationController?.viewControllers.removeAll(where: { (vc) -> Bool in
            if vc.isKind(of: UITabBarController.self) || vc.isKind(of: CharityDashboardVC.self)
                || vc.isKind(of: DonationCompleteVC.self) {
                return false
            } else {
                return true
            }
        })
    }
    
}
