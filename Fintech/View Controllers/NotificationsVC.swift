//
//  NotificationsVC.swift
//  Fintech
//
//  Created by MacBook-Mubashar on 24/07/2019.
//  Copyright © 2019 Broadstone Technologies. All rights reserved.
//

import UIKit
//import Toast_Swift

class NotificationsVC: Parent {
    
    @IBOutlet var tableview: UITableView!
    
    //class variables
    private var filteredNotifications : [String] = []
    private var searchText = String()
    
    private let searchController: UISearchController = {
        
        let searchController = UISearchController(searchResultsController: nil)
        searchController.searchBar.translatesAutoresizingMaskIntoConstraints = false
        searchController.searchBar.accessibilityIdentifier = "SearchBar"
        searchController.searchBar.placeholder = "Search"
        searchController.searchBar.tintColor = #colorLiteral(red: 0.8360546875, green: 0.8360546875, blue: 0.8360546875, alpha: 1)
        searchController.searchBar.barStyle = .black
        searchController.searchBar.sizeToFit()
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.enablesReturnKeyAutomatically = false
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.searchBar.returnKeyType = .done
        
        return searchController
        
    }()
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchController.searchBar.isTranslucent = false
        navigationItem.searchController = searchController
        
        searchController.searchBar.delegate = self
        
        self.tableview.delegate = self
        self.tableview.dataSource = self
        self.tableview.estimatedRowHeight = 150
        self.tableview.rowHeight = UITableView.automaticDimension
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        guard let items = self.tabBarController?.tabBar.items else { // Only if you use tab bar controller, if no delete this scope and uncomment next
            
            print("Don't have tab bar controller")
            return
        }
        
        if SPManager.sharedInstance.getUserPortfolioStatus() {
            print("user portfolio is complete")
        } else {
            print("user portfolio is not complete")
            if items[1].title! == "Portfolio" {
                self.tabBarController!.viewControllers?.remove(at: 1)
            }
        }
        //api calling
        if NetworkReachability.isConnectedToNetwork() {
            getNotificationsData()
        } else {
            self.view.makeToast(ConstantStrings.NO_INTERNET_CONNECTION_FOUND)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        setNavBarTitile(title: "News Feed", topItem: "News Feed")
    }
    
    // MARK: - API calls
    func getNotificationsData() {
        
        self.view.activityStartAnimating()
        let url = ApiUrl.GET_NOTIFICATIONS_DATA_URL
        let namespace = SPManager.sharedInstance.getCurrentUserNamespace()
        let data:Dictionary<String,AnyObject> = [:]
        
        let params:Dictionary<String,AnyObject> = ["namespace" : "test" as AnyObject, "data" : data as AnyObject]
        print(data)
        APIManager.sharedInstance.postRequest(serviceName: url, sendData: params, success: { (response) in
            print(response)
            
            if APIManager.sharedInstance.isSuccess {
                let res = response["message"] as AnyObject
                DispatchQueue.main.async {
                    
                    //                    QuestionnaireParser.sharedInstance.fetchMetaData(json: res as! [String : AnyObject], table_view: self.tableView)
                    
                    self.fetchMetaData(res: res as! [[String : AnyObject]], table_view: self.tableview)
                }
                
            }else{
                if response["message"] != nil {
                    self.showAlert(response["message"] as! String)
                } else {
                    self.showAlert(ConstantStrings.NO_RESULT_FOUND_ALERT_TEXT)
                }
            }
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
        }, failure: { (data) in
            print(data)
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
            if APIManager.sharedInstance.error400 {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
            }
            if(APIManager.sharedInstance.error401)
            {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
            }else{
                
            }
        })
        
    }
    
    
    // MARK: - fetchData
    func fetchMetaData(res : [[String : AnyObject]], table_view : UITableView) {
        ArrayLists.sharedInstance.notificationsDataList.removeAll()
        
        for jsonData in res {
            let model = NotificationDataModel()
            
            let mainHeading = jsonData["main_heading"] as! String
            let subHeading = jsonData["subheading"] as! String
            let description = jsonData["description"] as! String
            let iconUrl = jsonData["icon_url"] as! String
            //            let progressValue = jsonData["progress_value"] as! Double //umair is changing string-> float
            let dataType = jsonData["type"] as! String
            let timestamp = jsonData["eve_sec"] as! Double
            
            if jsonData["additional_info"] as? [String : AnyObject] != nil {
                let additionalInfo = jsonData["additional_info"] as! [String : AnyObject]
                //parse additional info
                //            if additionalInfo != nil {
                let infoType = additionalInfo["type"] as! String
                model.setInfoType(type: infoType)
                if infoType == "text" {
                    let infoValue = additionalInfo["value"] as! [String : AnyObject]
                    let infoHeading = infoValue["info_heading"] as! String
                    let infoSubHeading = infoValue["info_sub_heading"] as! String
                    let infoDescription = infoValue["info_description"] as! String
                    let infoDisclaimerText = infoValue["info_disclaimer_text"] as! String
                    let infoDisclaimerLink = infoValue["info_disclaimer_link"] as! String
                    let infoDisclaimerLinkType = infoValue["info_disclaimer_link_type"] as! String
                    
                    model.setInfoHeading(text: infoHeading)
                    model.setInfoSubHeading(text: infoSubHeading)
                    model.setInfoDescription(text: infoDescription)
                    model.setInfoDisclaimerText(text: infoDisclaimerText)
                    model.setInfoDisclaimerLink(text: infoDisclaimerLink)
                    model.setInfoDisclaimerLinkType(text: infoDisclaimerLinkType)
                } else {
                    let infoValue = additionalInfo["value"] as! String
                    model.setInfoValue(value: infoValue)
                }
            } else {
                model.setInfoType(type: "none")
            }
            
            model.setMainHeadingText(text: mainHeading)
            model.setSubHeadingText(text: subHeading)
            model.setDescriptionText(text: description)
            model.setIconImage(text: iconUrl)
            model.setProgressValue(text: 2.3)
            model.setDataType(text: dataType)
            model.setUnixTimestamp(timestamp: timestamp)
            
            ArrayLists.sharedInstance.notificationsDataList.append(model)
        }
        
        DispatchQueue.main.async {
            table_view.reloadData()
        }
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension NotificationsVC : UITableViewDelegate, UITableViewDataSource {
    
    // MARK: - TableView Delegates
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return ArrayLists.sharedInstance.notificationsDataList.count
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("selected index == \(indexPath.row)")
        let item = ArrayLists.sharedInstance.notificationsDataList[indexPath.row]
        if item.getInfoType() == "text" {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "NotificationDetailsVC") as! NotificationDetailsVC
            vc.selectedNotificationIndex = indexPath.row
            navigationController?.pushViewController(vc, animated: true)
            
        } else if item.getInfoValue() != "none" {
            viewPdfFile(urlString: item.getInfoValue(), urlType: item.getInfoType())
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  self.tableview.dequeueReusableCell(withIdentifier: "NotificationsCell") as! NotificationsCell
        let item = ArrayLists.sharedInstance.notificationsDataList[indexPath.row]
        
        //        if item.getInfoType() != "none" {
        //            cell.accessoryType = .disclosureIndicator
        //        } else {
        //            cell.accessoryType = .none
        //        }
        
        cell.notificationHeading_lbl.text = item.getMainHeadingText()
        cell.subHeading_lbl.text = item.getSubHeadingText()
        cell.notificationDesc_lbl.text = item.getDescriptionText()
        cell.timestamp_lbl.text = FintechUtils.sharedInstance.unixToLocalTime(timestamp: item.getUnixTimestamp())
        
        return cell
        
    }
}

extension NotificationsVC: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        let searchText = searchText.lowercased()
        //        let filtered = source.filter({ $0.lowercased().contains(searchText) })
        //        self.filtered = filtered.isEmpty ? source : filtered
        tableview.reloadData()
        
    }
    
}
