//
//  NotificationsVC.swift
//  Fintech
//
//  Created by MacBook-Mubashar on 24/07/2019.
//  Copyright © 2019 Broadstone Technologies. All rights reserved.
//########################################################################
//      In This view show newsfeed data
//      all data though api
//########################################################################
import UIKit
import ImageLoader
import Floaty
//import Toast_Swift

class NotificationsVC: Parent {
     // MARK: - class outets
    @IBOutlet weak var newsfeedTable: UITableView!
    @IBOutlet weak var floatyBtn: Floaty!
    
    // MARK: - class variables
    private var filteredNotifications : [String] = []
    private var searchText = String()
    private var dis = String()
    
    private let searchController: UISearchController = {
        let searchController = UISearchController(searchResultsController: nil)
        searchController.searchBar.translatesAutoresizingMaskIntoConstraints = false
        searchController.searchBar.accessibilityIdentifier = "SearchBar"
        searchController.searchBar.placeholder = "Search"
        searchController.searchBar.tintColor = #colorLiteral(red: 0.8352941176, green: 0.8352941176, blue: 0.8352941176, alpha: 1)
        searchController.searchBar.barStyle = .black
        searchController.searchBar.sizeToFit()
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.enablesReturnKeyAutomatically = false
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.searchBar.returnKeyType = .done
        
        return searchController
        
    }()
   // let button: UIButton = UIButton()
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // self.floatybutton()
        floatyBtn.isHidden = true
        self.setNeedsStatusBarAppearanceUpdate()
        //searchController.searchBar.isTranslucent = false
        //navigationItem.searchController = searchController
        //searchController.searchBar.delegate = self
        self.newsfeedTable.delegate = self
        self.newsfeedTable.dataSource = self
        self.newsfeedTable.estimatedRowHeight = 300
        self.newsfeedTable.rowHeight = UITableView.automaticDimension
        // MARK: - api calling
        if NetworkReachability.isConnectedToNetwork(){
            getNotificationsData()
        } else {
            self.view.makeToast(ConstantStrings.NO_INTERNET_CONNECTION_FOUND)
        }
        // Do any additional setup after loading the view.
    }
    @objc func buttonAction(sender: UIButton!) {
        print("Button tapped")
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "Main_login_page") as UIViewController
        UIApplication.shared.keyWindow?.rootViewController = vc
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        setNavBarTitile(title: getLocalizedString(key: "News_Feed"), topItem: getLocalizedString(key: "News_Feed"))
        
//        guard let items = self.tabBarController?.tabBar.items else {
//        // Only if you use tab bar controller, if no delete this scope and uncomment next
//            print("Don't have tab bar controller")
//            return
//
//        }
    }
//    override func viewDidAppear(_ animated: Bool) {
//        localization()
//    }
//    // MARK: - Navigation
//    func localization(){
//        setNavBarTitile(title:getLocalizedString(key:"News Feed", comment: ""), topItem: getLocalizedString(key: "News Feed", comment: ""))
//        navigationController?.navigationBar.barStyle = .blackOpaque
//    }
    
    // MARK: - API calls
    func getNotificationsData() {
        // despacth Q
        self.view.activityStartAnimating()
        let url = ApiUrl.GET_NEWSFEED_DATA_URL
        let namespace = SPManager.sharedInstance.getCurrentUserNamespace()
        let data:Dictionary<String,AnyObject> = [:]
        
        let params:Dictionary<String,AnyObject> = ["namespace" : namespace as AnyObject, "data" : data as AnyObject]
        print(data)
        APIManager.sharedInstance.postRequest(serviceName: url, sendData: params, success: { (response) in
            print(response)
            
            if APIManager.sharedInstance.isSuccess {
                let res = response["message"] as AnyObject
                DispatchQueue.main.async {
                    
                    self.fetchMetaData(res: res as! [[String : AnyObject]], table_view: self.newsfeedTable)
                }
                
            }else{
                if response["message"] != nil {
                    self.showAlert(response["message"] as! String)
                } else {
                    self.showAlert(ConstantStrings.NO_RESULT_FOUND_ALERT_TEXT)
                }
            }
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
        }, failure: { (data) in
            print(data)
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
            if APIManager.sharedInstance.error400 {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
            }
            if(APIManager.sharedInstance.error401)
            {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
            }else{
                
            }
        })
        
    }
    
    // MARK: - fetchData
    func fetchMetaData(res : [[String : AnyObject]], table_view : UITableView) {
        ArrayLists.sharedInstance.newsFeedDataList.removeAll()
        
        for jsonData in res {
            let model = NewsFeedDataModel()
            
            let title = jsonData["title"] as! String
            let tickers = jsonData["ticker"] as! String
            let summary = jsonData["summary"] as! String
            let iconUrl = jsonData["icon_url"] as! String
            let link = jsonData["link"] as! String
            let publisher = jsonData["publisher"] as! String
            //            let progressValue = jsonData["progress_value"] as! Double //umair is changing string-> float
            let timestamp = jsonData["eve_sec"] as! Double
            model.setTitleText(title)
            model.settickerText(tickers)
            model.setsummaryText(summary)
            model.setpublishname(publisher)
            model.settimestamp(timestamp)
            model.settickerText(tickers)
            model.setnewslink(link)
            model.setIconUrl(iconUrl)
            ArrayLists.sharedInstance.newsFeedDataList.append(model)
        }
        
        DispatchQueue.main.async {
             ArrayLists.sharedInstance.newsFeedDataList =  ArrayLists.sharedInstance.newsFeedDataList.sorted {(a,b)-> Bool in
                let aDate = Date(timeIntervalSince1970: a.gettimestamp())
                let bDate = Date(timeIntervalSince1970: b.gettimestamp())
                
                return aDate > bDate
            }
            table_view.reloadData()
        }
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    //Mark: - PDF File URL Load
    @objc func viewDiscalimerTapped() {
        viewPdfFile(urlString: dis,urlType: "")
    }
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    func floatybutton() {

        if SPManager.sharedInstance.saveUserLoginStatus() {
            floatyBtn.isHidden = true

        }else{
            floatyBtn.buttonColor = #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1)
            floatyBtn.itemButtonColor = #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1)
            floatyBtn.buttonImage = UIImage(named: "floatyicon")

            floatyBtn.addItem("Sample Portfolios",icon: UIImage(named: "services-portfolio1")!, handler: { item in
                if let tabbedbar = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "nav_bar_sampleportfolio") as? UINavigationController {
                    tabbedbar.modalPresentationStyle = .fullScreen

                    UIApplication.shared.keyWindow?.rootViewController = tabbedbar
                }
            })

            floatyBtn.addItem("Account Setup",icon: UIImage(named: "baseline_person_add_black")!, handler: { item in
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "Main_login_page") as UIViewController
                vc.modalPresentationStyle = .fullScreen

                UIApplication.shared.keyWindow?.rootViewController = vc
            })
            DispatchQueue.main.async {
                            self.view.addSubview(self.floatyBtn)

            }
        
        }
    }
    
    
}
extension UIButton {
    func setup(title: String, x: CGFloat, y: CGFloat, width: CGFloat, height: CGFloat, color: UIColor){
        frame = CGRect(x: x, y: y, width: width, height: height)
        backgroundColor = color
        setTitle(title , for: .normal)
    }
}
// MARK: - TableView Delegates
extension NotificationsVC : UITableViewDelegate, UITableViewDataSource {
    // MARK: - TableView Delegates
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ArrayLists.sharedInstance.newsFeedDataList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("selected index == \(indexPath.row)")
        let item = ArrayLists.sharedInstance.newsFeedDataList[indexPath.row]
        
        viewPdfFile(urlString: item.getnewslink(), urlType: item.getpublishname())
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  self.newsfeedTable.dequeueReusableCell(withIdentifier: "NotificationsCell") as! NotificationsCell
        let item = ArrayLists.sharedInstance.newsFeedDataList[indexPath.row]
        
        cell.notificationHeading_lbl.text = item.getTitleText()
        
        cell.ticker_lbl.text = " " + item.gettickerText() + " "
        
        cell.timestamp_lbl.text = FintechUtils.sharedInstance.unixToLocalTime(timestamp: item.gettimestamp())
        let tickerText = item.gettickerText()
        if tickerText == NewsfeedTicker.GENERAL_TICKER_TEXT {
           // cell.ticker_lbl.backgroundColor = NewsfeedTickerColors.GENERAL_TICKER_COLOR
        }else if tickerText == NewsfeedTicker.AAPL_TICKER_TEXT{
            //cell.ticker_lbl.backgroundColor = NewsfeedTickerColors.AAPl_Ticker_color
        }else if tickerText == NewsfeedTicker.FB_TICKER_TEXT{
            //cell.ticker_lbl.backgroundColor = NewsfeedTickerColors.FB_Ticker_color
        }else if tickerText == NewsfeedTicker.AMD_TICKER_TEXT{
            //cell.ticker_lbl.backgroundColor = NewsfeedTickerColors.AMD_Ticker_color
        }else if tickerText == NewsfeedTicker.MSFT_TICKER_TEXT{
            //cell.ticker_lbl.backgroundColor = NewsfeedTickerColors.MSFT_Ticker_color
        }
        
        if item.getIconUrl().isEmpty {
            
        } else {
            let url = URL(string: item.getIconUrl())
            cell.Image_img.load.request(with: url!)
        }
        
        cell.Image_img.clipsToBounds = true
        
        return cell
    }
}
// Mark: - search bar funaction
extension NotificationsVC: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        let searchText = searchText.lowercased()
        newsfeedTable.reloadData()
        
    }
    
}
