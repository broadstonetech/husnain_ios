//
//  PreMadePortfolioList + API.swift
//  Fintech
//
//  Created by broadstone on 04/03/2022.
//  Copyright © 2022 Broadstone Technologies. All rights reserved.
//

import Foundation
import UIKit
extension PreMadePortfolioListVC {
    func getPreMadePortfolios(){
        self.view.activityStartAnimating()
        let url = ApiUrl.GET_PRE_MADE_PORTFOLIOS_LIST
        let params = ["namespace": SPManager.sharedInstance.getCurrentUserNamespace()] as [String: AnyObject]
        
        APIManager.sharedInstance.postRequest(serviceName: url, sendData: params, success: { (response) in
            print(response)
            
            if APIManager.sharedInstance.isSuccess {
                DispatchQueue.main.async {
                    self.fetchPreMadePortfolioListResponse(response)
                    self.tableView.reloadData()
                }
            }else{
                
                DispatchQueue.main.async { [self] in
                    if response["message"] != nil {
                        self.showAlert(response["message"] as! String)
                    } else {
                        self.showAlert(ConstantStrings.NO_RESULT_FOUND_ALERT_TEXT)
                    }
                }
            }
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
        }, failure: { (data) in
            print(data)
            DispatchQueue.main.async { [self] in
                view.activityStopAnimating()
            }
            if APIManager.sharedInstance.error400 {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
            }
            if(APIManager.sharedInstance.error401)
            {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
            }else{
                self.showAlert(ConstantStrings.Error_msg_generic)
            }
        })
    }
    
    private func fetchPreMadePortfolioListResponse(_ res: [String: AnyObject]) {
        let success = res["success"] as! Bool
        let message = res["message"] as! String
        
        if success == false {
            portfoliosList = PreMadePortfolioListModel.init(success: success, message: message, data: [PreMadePortfolioListData]())
            return
        }
        
        let data = res["data"] as! [AnyObject]
        
        var portfolioList = [PreMadePortfolioListData]()
        for i in 0..<data.count {
            let item = data[i] as! [String: AnyObject]
            
            let portfolioId = item["portfolio_id"] as! Int
            let portfolioName = item["portfolio_name"] as! String
            let overAllZScore = item["overall_zScore"] as! Double
            let envZScore = item["env_zScore"] as! Double
            let socZScore = item["soc_zScore"] as! Double
            let govZScore = item["gov_zScore"] as! Double
            
            portfolioList.append(PreMadePortfolioListData.init(portfolioId: portfolioId, portfolioName: portfolioName, overAllZScore: overAllZScore, envZScore: envZScore, socZScore: socZScore, govZScore: govZScore))
        }
        
        portfoliosList = PreMadePortfolioListModel.init(success: success, message: message, data: portfolioList)
    }
    
    func getPortfolioDetail(portfolioId: Int) {
        self.view.activityStartAnimating()
        let url = ApiUrl.GET_PORTFOLIO_DETAILS
        //let params = getPortfolioDetailsParams(index: 103)
        let params = getPortfolioDetailsParams(index: portfolioId)
        print(params)
        
        APIManager.sharedInstance.postRequest(serviceName: url, sendData: params, success: { (response) in
            print(response)
            
            if APIManager.sharedInstance.isSuccess {
                DispatchQueue.main.async {
                    let model = self.fetchExternalPortfolioResponse(res: response)
                    if model == nil {
                        self.view.makeToast("No data found")
                        return
                    }
                    let storyboard = UIStoryboard(name: "Portfolio", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "PortfolioImpactViewController") as! PortfolioImpactViewController
                    vc.useCaseType = self.useCase
                    vc.portfolioImpactModel = model
                    vc.isViewingPreMadePortfolios = true
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }else{
                
                DispatchQueue.main.async { [self] in
                    if response["message"] != nil {
                        self.showAlert(response["message"] as! String)
                    } else {
                        self.showAlert(ConstantStrings.NO_RESULT_FOUND_ALERT_TEXT)
                    }
                }
            }
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
        }, failure: { (data) in
            print("Error: \(data)")
            DispatchQueue.main.async { [self] in
                view.activityStopAnimating()
            }
            if APIManager.sharedInstance.error400 {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
            }
            if(APIManager.sharedInstance.error401)
            {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
            }else{
                self.showAlert(ConstantStrings.Error_msg_generic)
            }
        })
    }
    
    private func getPortfolioDetailsParams(index: Int) -> [String: AnyObject] {
        let data = ["portfolio_id": index] as [String: AnyObject]
        
        return ["namespace": SPManager.sharedInstance.getCurrentUserNamespace(),
                "data": data] as [String: AnyObject]
    }
}
