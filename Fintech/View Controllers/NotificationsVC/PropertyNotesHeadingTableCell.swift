//
//  PropertyNotesHeadingTableCell.swift
//  TableViewExpandable
//
//  Created by Fahad Ali Awan on 24/07/2019.
//  Copyright © 2019 Org Technologies. All rights reserved.
//

import UIKit

class PropertyNotesHeadingTableCell: UITableViewCell {

    @IBOutlet weak var headingName: UILabel!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var selectionImage: UIImageView!
    
    @IBOutlet weak var Properties_view: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
}
