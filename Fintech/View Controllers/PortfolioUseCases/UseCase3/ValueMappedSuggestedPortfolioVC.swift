//
//  ValueMappedSuggestedPortfolioVC.swift
//  Fintech
//
//  Created by broadstone on 02/03/2022.
//  Copyright © 2022 Broadstone Technologies. All rights reserved.
//

import UIKit

class ValueMappedSuggestedPortfolioVC: PortfolioParent {
    
    @IBOutlet weak var portfolio1Label: UILabel!
    @IBOutlet weak var portfolio2Label: UILabel!
    @IBOutlet weak var portfolio3Label: UILabel!
    @IBOutlet weak var portfolio1DescLabel: UILabel!
    @IBOutlet weak var portfolio2DescLabel: UILabel!
    @IBOutlet weak var portfolio3DescLabel: UILabel!
    @IBOutlet weak var portfolio1RiskImage: UIImageView!
    @IBOutlet weak var portfolio2RiskImage: UIImageView!
    @IBOutlet weak var portfolio3RiskImage: UIImageView!
    
    var valueMappedSuggestedPortfolios = [ValueMappedPortfolioData]()
    var portfolioType = ""
    var positiveVals = [String]()
    var negativeVals = [String]()
    
    var apiCounter = 0

    override func viewDidLoad() {
        super.viewDidLoad()

       updateUI(isFromScoreImprovement: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        setNavBarTitile(title: "Back", topItem: getLocalizedString(key: "_portfolios"))
    }
    
    @IBAction func portfolio1BtnPressed(_ sender: UIButton) {
        goToGeneratedPortfolioVC(useCaseType: .UseCase3, model: valueMappedSuggestedPortfolios[0].portfolioData, isViewingPreMade: true)
    }
    
    @IBAction func portfolio2BtnPressed(_ sender: UIButton) {
        goToGeneratedPortfolioVC(useCaseType: .UseCase3, model: valueMappedSuggestedPortfolios[1].portfolioData, isViewingPreMade: true)
    }
    
    @IBAction func portfolio3BtnPressed(_ sender: UIButton) {
        goToGeneratedPortfolioVC(useCaseType: .UseCase3, model: valueMappedSuggestedPortfolios[2].portfolioData, isViewingPreMade: true)
    }
    
    @IBAction func scoreImprovementBtnPressed(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "ScoreImprovement", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ScoreImprovementValueMappedVC") as! ScoreImprovementValueMappedVC
        vc.delegate = self
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func educationBtnPressed(_ sender: UIButton) {
//        if let tabbedbar = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "tab_bar_start") as? MainTabBarController {
//            tabbedbar.modalPresentationStyle = .fullScreen
//            tabbedbar.selectedIndex = 3
//            self.navigationController?.pushViewController(tabbedbar, animated: true)
//
//       }
        let resoucesStoryboard = UIStoryboard(name: "Resources", bundle: nil)
        let vc = resoucesStoryboard.instantiateViewController(withIdentifier: "ResourcesVC") as! ResourcesVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    private func updateUI(isFromScoreImprovement: Bool) {
        if isFromScoreImprovement {
            portfolio1Label.text = valueMappedSuggestedPortfolios[0].template + " (\(getLocalizedRiskString(valueMappedSuggestedPortfolios[0].risk)))"
            portfolio2Label.text = valueMappedSuggestedPortfolios[1].template + " (\(getLocalizedRiskString(valueMappedSuggestedPortfolios[1].risk)))"
            portfolio3Label.text = valueMappedSuggestedPortfolios[2].template + " (\(getLocalizedRiskString(valueMappedSuggestedPortfolios[2].risk)))"
        }else {
            portfolio1Label.text = getLocalizedTemplateString(valueMappedSuggestedPortfolios[0].template) + " (\(getLocalizedRiskString(valueMappedSuggestedPortfolios[0].risk)))"
            portfolio2Label.text = getLocalizedTemplateString(valueMappedSuggestedPortfolios[1].template) + " (\(getLocalizedRiskString(valueMappedSuggestedPortfolios[1].risk)))"
            portfolio3Label.text = getLocalizedTemplateString(valueMappedSuggestedPortfolios[2].template) + " (\(getLocalizedRiskString(valueMappedSuggestedPortfolios[2].risk)))"
        }
    }
    
    private func getLocalizedTemplateString(_ template: String) -> String {
        switch template {
            case "catholic":
                return getLocalizedString(key: "template_catholic") + " "
            case "environment":
                return getLocalizedString(key: "template_env_imapct") + " "
            case "islamic":
                return getLocalizedString(key: "template_islamic") + " "
            case "methodist":
                return getLocalizedString(key: "template_methodist") + " "
            case "peace":
                return getLocalizedString(key: "template_peace") + " "
            case "vegan":
                return getLocalizedString(key: "template_vegan") + " "
            case "social":
                return getLocalizedString(key: "soc") + " "
            case "governance":
                return getLocalizedString(key: "gov") + " "
            default:
                return getLocalizedString(key: "Portfolio") + " "
            }
    }
    
    private func getLocalizedRiskString(_ rawScore: Int) -> String{
        if rawScore >= 0 && rawScore < 23 {
            return getLocalizedString(key: "_conservative")
        }
        else if rawScore >= 23 && rawScore < 33 {
            return getLocalizedString(key: "_balanced")
        }
        else if rawScore >= 33 && rawScore <= 47 {
            return getLocalizedString(key: "_aggressive")
        }else {
            return ""
        }
    }
    
    private func getRiskIcon(_ rawScore: Int) -> UIImage {
        if rawScore >= 0 && rawScore < 23 {
            return UIImage(named: "cheerful")!
        }
        else if rawScore >= 23 && rawScore < 33 {
            return UIImage(named: "angry")!
        }
        else {
            return UIImage(named: "care")!
        }
    }
    
}
extension ValueMappedSuggestedPortfolioVC: ScoreImprovementTemplateDelegate {
    func scoreImrpovementTemplateSelected(_ selectedTemplate: PortfolioValuesTemplate) {
        apiCounter = 0
        
        let risk1 = valueMappedSuggestedPortfolios[0].risk
        let risk2 = valueMappedSuggestedPortfolios[1].risk
        let risk3 = valueMappedSuggestedPortfolios[2].risk
        
        let param1 = getValuesCalculationParams(template: selectedTemplate.template, rawRisk: risk1, portfolioType: portfolioType, postiveVals: positiveVals, negativeVals: negativeVals)
        let param2 = getValuesCalculationParams(template: selectedTemplate.template, rawRisk: risk2, portfolioType: portfolioType, postiveVals: positiveVals, negativeVals: negativeVals)
        let param3 = getValuesCalculationParams(template: selectedTemplate.template, rawRisk: risk3, portfolioType: portfolioType, postiveVals: positiveVals, negativeVals: negativeVals)
        
        valueMappedSuggestedPortfolios[0].template = selectedTemplate.localizedName
        valueMappedSuggestedPortfolios[1].template = selectedTemplate.localizedName
        valueMappedSuggestedPortfolios[2].template = selectedTemplate.localizedName
        
        generatePortfolioAPI(url: ApiUrl.GET_VALUE_MAPPED_PORTFOLIO_IMPACT, params: param1, returnData: valueMappedSuggestedPortfolios[0])
        generatePortfolioAPI(url: ApiUrl.GET_VALUE_MAPPED_PORTFOLIO_IMPACT, params: param2, returnData: valueMappedSuggestedPortfolios[1])
        generatePortfolioAPI(url: ApiUrl.GET_VALUE_MAPPED_PORTFOLIO_IMPACT, params: param3, returnData: valueMappedSuggestedPortfolios[2])
    }
    
    func generatePortfolioAPI(url: String, params: [String: AnyObject], returnData: ValueMappedPortfolioData){
        self.view.activityStartAnimating()
        
        let payload = ["namespace": SPManager.sharedInstance.getCurrentUserNamespace(),
                       "data": params] as [String: AnyObject]
        
        APIManager.sharedInstance.postRequest(serviceName: url, sendData: payload, success: { (response) in
            print(response)
            
            if APIManager.sharedInstance.isSuccess {
                DispatchQueue.main.async {
                    let data = self.fetchExternalPortfolioResponse(res: response)!
                    self.valueMappedSuggestedPortfolios[returnData.portfolioId].portfolioData = data
                    self.apiCounter += 1
                    print("Api Called: \(self.apiCounter)")
                    if self.apiCounter == 3 {
                        self.apiCounter = 0
                        print("Api Called 3 times")
                        self.updateUI(isFromScoreImprovement: true)
                    }
                }
            }else{
                
                DispatchQueue.main.async { [self] in
                    if response["message"] != nil {
                        self.showAlert(response["message"] as! String)
                    } else {
                        self.showAlert(ConstantStrings.NO_RESULT_FOUND_ALERT_TEXT)
                    }
                }
            }
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
        }, failure: { (data) in
            print("Error: \(data)")
            DispatchQueue.main.async { [self] in
                view.activityStopAnimating()
            }
            if APIManager.sharedInstance.error400 {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
            }
            if(APIManager.sharedInstance.error401)
            {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
            }else{
                self.showAlert(ConstantStrings.Error_msg_generic)
            }
        })
    }
}

