//
//  PortfolioValuesTemplateDetailsVC.swift
//  Fintech
//
//  Created by broadstone on 10/08/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import UIKit

class PortfolioValuesTemplateDetailsVC: PortfolioParent {
    
    struct ValuesTemplateDetailsStruct {
        var index: Int!
        var includedValues: [String]!
        var excludedValues: [String]!
    }
    
    let catholicTemplateIncludedVals = [
        getLocalizedString(key: "following_vals_included"),
        getLocalizedString(key: "animal_welfare"),
        getLocalizedString(key: "family_values"),
        getLocalizedString(key: "gender_equality"),
        getLocalizedString(key: "_immigration"),
        getLocalizedString(key: "climate_change"),
        getLocalizedString(key: "_biodiversity"),
        getLocalizedString(key: "energy_efficient"),
        getLocalizedString(key: "customer_satisfaction"),
        getLocalizedString(key: "data_protection"),
        getLocalizedString(key: "gender_and_diversity"),
        getLocalizedString(key: "employee_engagement"),
        getLocalizedString(key: "community_relation"),
        getLocalizedString(key: "human_rights"),
        getLocalizedString(key: "labor_standards"),
        getLocalizedString(key: "board_composition"),
        getLocalizedString(key: "audit_committee")
    ]
    
    let catholicTemplateExcludedVals = [
        getLocalizedString(key: "following_vals_excluded"),
        getLocalizedString(key: "_abortion"),
        getLocalizedString(key: "adult_entertainment"),
        getLocalizedString(key: "_contraceptives"),
        getLocalizedString(key: "_firarms"),
        getLocalizedString(key: "_tobacco"),
        getLocalizedString(key: "controversial_weapons"),
        getLocalizedString(key: "_lgbtq"),
        getLocalizedString(key: "air_pollution"),
        getLocalizedString(key: "_defrostation"),
        getLocalizedString(key: "water_scarcity")
    ]
    
    let envTemplateIncludedVals = [
        getLocalizedString(key: "following_vals_included"),
        getLocalizedString(key: "animal_welfare"),
        getLocalizedString(key: "nuclear_power"),
        getLocalizedString(key: "family_values"),
        getLocalizedString(key: "gender_equality"),
        getLocalizedString(key: "_immigration"),
        getLocalizedString(key: "_vegan"),
        getLocalizedString(key: "climate_change"),
        getLocalizedString(key: "_biodiversity"),
        getLocalizedString(key: "energy_efficient"),
        getLocalizedString(key: "waste_management"),
        getLocalizedString(key: "customer_satisfaction"),
        getLocalizedString(key: "data_protection"),
        getLocalizedString(key: "gender_and_diversity"),
        getLocalizedString(key: "employee_engagement"),
        getLocalizedString(key: "community_relation"),
        getLocalizedString(key: "human_rights"),
        getLocalizedString(key: "labor_standards"),
        getLocalizedString(key: "board_composition"),
        getLocalizedString(key: "audit_committee")
    ]
    
    let envTemplateExcludedVals = [
        getLocalizedString(key: "following_vals_excluded"),
        getLocalizedString(key: "_coal"),
        getLocalizedString(key: "_firarms"),
        getLocalizedString(key: "fur_leather"),
        getLocalizedString(key: "_gmo"),
        getLocalizedString(key: "palm_oil"),
        getLocalizedString(key: "_pesticides"),
        getLocalizedString(key: "_tobacco"),
        getLocalizedString(key: "controversial_weapons"),
        getLocalizedString(key: "_lgbtq"),
        getLocalizedString(key: "air_pollution"),
        getLocalizedString(key: "_defrostation"),
        getLocalizedString(key: "water_scarcity"),
        getLocalizedString(key: "bribary_corruption")
    ]
    
    let islamicTemplateIncludedVals = [
        getLocalizedString(key: "following_vals_included"),
        getLocalizedString(key: "animal_welfare"),
        getLocalizedString(key: "family_values"),
        getLocalizedString(key: "gender_equality"),
        getLocalizedString(key: "_immigration"),
        getLocalizedString(key: "climate_change"),
        getLocalizedString(key: "_biodiversity"),
        getLocalizedString(key: "energy_efficient"),
        getLocalizedString(key: "customer_satisfaction"),
        getLocalizedString(key: "data_protection"),
        getLocalizedString(key: "gender_and_diversity"),
        getLocalizedString(key: "employee_engagement"),
        getLocalizedString(key: "community_relation"),
        getLocalizedString(key: "human_rights"),
        getLocalizedString(key: "labor_standards"),
        getLocalizedString(key: "board_composition"),
        getLocalizedString(key: "audit_committee")
    ]
    
    let islamicTemplateExcludedVals = [
        getLocalizedString(key: "following_vals_excluded"),
        getLocalizedString(key: "_abortion"),
        getLocalizedString(key: "adult_entertainment"),
        getLocalizedString(key: "_alcohol"),
        getLocalizedString(key: "_contraceptives"),
        getLocalizedString(key: "_firarms"),
        getLocalizedString(key: "_gambling"),
        getLocalizedString(key: "interest_bearing"),
        getLocalizedString(key: "_tobacco"),
        getLocalizedString(key: "controversial_weapons"),
        getLocalizedString(key: "_lgbtq"),
        getLocalizedString(key: "air_pollution"),
        getLocalizedString(key: "_defrostation"),
        getLocalizedString(key: "water_scarcity")
    ]
    
    
    let methodistTemplateIncludedVals = [
        getLocalizedString(key: "following_vals_included"),
        getLocalizedString(key: "animal_welfare"),
        getLocalizedString(key: "family_values"),
        getLocalizedString(key: "gender_equality"),
        getLocalizedString(key: "_immigration"),
        getLocalizedString(key: "climate_change"),
        getLocalizedString(key: "_biodiversity"),
        getLocalizedString(key: "energy_efficient"),
        getLocalizedString(key: "customer_satisfaction"),
        getLocalizedString(key: "data_protection"),
        getLocalizedString(key: "gender_and_diversity"),
        getLocalizedString(key: "employee_engagement"),
        getLocalizedString(key: "community_relation"),
        getLocalizedString(key: "human_rights"),
        getLocalizedString(key: "labor_standards"),
        getLocalizedString(key: "board_composition"),
        getLocalizedString(key: "audit_committee")
    ]
    
    let methodistTemplateExcludedVals = [
        getLocalizedString(key: "following_vals_excluded"),
        getLocalizedString(key: "_abortion"),
        getLocalizedString(key: "adult_entertainment"),
        getLocalizedString(key: "_contraceptives"),
        getLocalizedString(key: "_firarms"),
        getLocalizedString(key: "_tobacco"),
        getLocalizedString(key: "controversial_weapons"),
        getLocalizedString(key: "_lgbtq"),
        getLocalizedString(key: "air_pollution"),
        getLocalizedString(key: "_defrostation"),
        getLocalizedString(key: "water_scarcity")
    ]
    
    let peaceTemplateIncludedVals = [
        getLocalizedString(key: "following_vals_included"),
        getLocalizedString(key: "animal_welfare"),
        getLocalizedString(key: "family_values"),
        getLocalizedString(key: "gender_equality"),
        getLocalizedString(key: "_lgbtq"),
        getLocalizedString(key: "_immigration"),
        getLocalizedString(key: "_vegan"),
        getLocalizedString(key: "climate_change"),
        getLocalizedString(key: "_biodiversity"),
        getLocalizedString(key: "energy_efficient"),
        getLocalizedString(key: "customer_satisfaction"),
        getLocalizedString(key: "data_protection"),
        getLocalizedString(key: "gender_and_diversity"),
        getLocalizedString(key: "employee_engagement"),
        getLocalizedString(key: "community_relation"),
        getLocalizedString(key: "human_rights"),
        getLocalizedString(key: "labor_standards"),
        getLocalizedString(key: "board_composition"),
        getLocalizedString(key: "audit_committee")
    ]
    
    let peaceTemplateExcludedVals = [
        getLocalizedString(key: "following_vals_excluded"),
        getLocalizedString(key: "_coal"),
        getLocalizedString(key: "_firarms"),
        getLocalizedString(key: "fur_leather"),
        getLocalizedString(key: "_gmo"),
        getLocalizedString(key: "palm_oil"),
        getLocalizedString(key: "_pesticides"),
        getLocalizedString(key: "_tobacco"),
        getLocalizedString(key: "controversial_weapons"),
        getLocalizedString(key: "military_contract"),
        getLocalizedString(key: "air_pollution"),
        getLocalizedString(key: "_defrostation"),
        getLocalizedString(key: "waste_management"),
        getLocalizedString(key: "water_scarcity"),
        getLocalizedString(key: "bribary_corruption")
    ]
    
    let veganTemplateIncludedVals = [
        getLocalizedString(key: "following_vals_included"),
        getLocalizedString(key: "animal_welfare"),
        getLocalizedString(key: "family_values"),
        getLocalizedString(key: "gender_equality"),
        getLocalizedString(key: "_lgbtq"),
        getLocalizedString(key: "_immigration"),
        getLocalizedString(key: "_vegan"),
        getLocalizedString(key: "climate_change"),
        getLocalizedString(key: "_biodiversity"),
        getLocalizedString(key: "energy_efficient"),
        getLocalizedString(key: "customer_satisfaction"),
        getLocalizedString(key: "data_protection"),
        getLocalizedString(key: "gender_and_diversity"),
        getLocalizedString(key: "employee_engagement"),
        getLocalizedString(key: "community_relation"),
        getLocalizedString(key: "human_rights"),
        getLocalizedString(key: "labor_standards"),
        getLocalizedString(key: "board_composition"),
        getLocalizedString(key: "audit_committee")
    ]
    
    let veganTemplateExcludedVals = [
        getLocalizedString(key: "following_vals_excluded"),
        getLocalizedString(key: "fur_leather"),
        getLocalizedString(key: "_gmo"),
        getLocalizedString(key: "palm_oil"),
        getLocalizedString(key: "_pesticides"),
        getLocalizedString(key: "_tobacco"),
        getLocalizedString(key: "air_pollution"),
        getLocalizedString(key: "_defrostation"),
        getLocalizedString(key: "water_scarcity")
    ]
    
    let manualTemplateIncludedVals = [
        getLocalizedString(key: "emphasize_these_value"),
        getLocalizedString(key: "24a", comment: ""),
        getLocalizedString(key: "24b", comment: ""),
        getLocalizedString(key: "24c", comment: ""),
        getLocalizedString(key: "24d", comment: ""),
        getLocalizedString(key: "24e", comment: ""),
        getLocalizedString(key: "env"),
        getLocalizedString(key: "soc"),
        getLocalizedString(key: "gov")
    ]
    
    let manualTemplateExcludedVals = [
        getLocalizedString(key: "exclude_these_values"),
        getLocalizedString(key: "23a", comment: ""),
        getLocalizedString(key: "23b", comment: ""),
        getLocalizedString(key: "23c", comment: ""),
        getLocalizedString(key: "23d", comment: ""),
        getLocalizedString(key: "23e", comment: ""),
        getLocalizedString(key: "23f", comment: ""),
        getLocalizedString(key: "23g", comment: ""),
        getLocalizedString(key: "23h", comment: ""),
        getLocalizedString(key: "23i", comment: ""),
        getLocalizedString(key: "23j", comment: ""),
        getLocalizedString(key: "23k", comment: ""),
        getLocalizedString(key: "23l", comment: ""),
        getLocalizedString(key: "23m", comment: "")
    ]
    
    
    var tableViewData = [ValuesTemplateDetailsStruct]()
    
    var index = 0
    var templateName = ""
    var delegate: PortfolioTemplateDelegate?
    
    var selectedPositiveVals = [String]()
    var selectedNegativeVals  = [String]()
    
    var isDismissingByDragging = true
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var barItem: UIBarButtonItem!
    @IBOutlet weak var navItem: UINavigationItem!

    override func viewDidLoad() {
        super.viewDidLoad()

        //self.title = templateName
        //titleLabel.text = templateName
        
        barItem.title = getLocalizedString(key: "Done")
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.black]
        navBar.titleTextAttributes = textAttributes
        navItem.title = templateName
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableViewData = [
            ValuesTemplateDetailsStruct(index: 0, includedValues: catholicTemplateIncludedVals, excludedValues: catholicTemplateExcludedVals),
            ValuesTemplateDetailsStruct(index: 1, includedValues: envTemplateIncludedVals, excludedValues: envTemplateExcludedVals),
            ValuesTemplateDetailsStruct(index: 2, includedValues: envTemplateIncludedVals, excludedValues: envTemplateExcludedVals),
            ValuesTemplateDetailsStruct(index: 3, includedValues: islamicTemplateIncludedVals, excludedValues: islamicTemplateExcludedVals),
            ValuesTemplateDetailsStruct(index: 4, includedValues: methodistTemplateIncludedVals, excludedValues: methodistTemplateExcludedVals),
            ValuesTemplateDetailsStruct(index: 5, includedValues: peaceTemplateIncludedVals, excludedValues: peaceTemplateExcludedVals),
            ValuesTemplateDetailsStruct(index: 6, includedValues: envTemplateIncludedVals, excludedValues: envTemplateExcludedVals),
            ValuesTemplateDetailsStruct(index: 7, includedValues: veganTemplateIncludedVals, excludedValues: veganTemplateExcludedVals),
            ValuesTemplateDetailsStruct(index: 8, includedValues: manualTemplateIncludedVals, excludedValues: manualTemplateExcludedVals)
        ]
        
        if index == tableViewData.count - 1 {
            navItem.title = getLocalizedString(key: "choose_my_investment_vals")
        }
        
                             
    }
    
    func createNextButton(){
        
//        let rightButton = UIButton(frame: CGRect(x: 0, y: 0, width: 45, height: 16))
//        rightButton.setTitle(getLocalizedString(key: "_next"), for: .normal)
//        rightButton.addTarget(self, action: #selector(doneBtnPressed(_:)), for: .touchUpInside)
//        //rightButton.titleLabel?.font = UIFont(name: "HelveticaNeue", size: 15)
//
//        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: rightButton)
//        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: getLocalizedString(key: "_next"), style: .done, target: self, action: #selector(doneBtnPressed(_:)))
    }
    
    override func endAppearanceTransition() {
        if isBeingDismissed && isDismissingByDragging {
            if index == tableViewData.count - 1 {
                delegate?.portfolioValuesAdded(postivieVals: selectedPositiveVals, negativeVals: selectedNegativeVals)
            }
        }
    }
    
    
    
    
    @IBAction func doneBtnPressed(_ sender: UIBarButtonItem){
        isDismissingByDragging = false
        self.dismiss(animated: true, completion: nil)
        if index == tableViewData.count - 1 {
            delegate?.portfolioValuesAdded(postivieVals: selectedPositiveVals, negativeVals: selectedNegativeVals)
        }
        
        
    }

}
extension PortfolioValuesTemplateDetailsVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return tableViewData[index].includedValues.count
        }
        else if section == 1 {
            return tableViewData[index].excludedValues.count
        }
        else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: ValuesTemplateDetailCell
        if indexPath.row == 0 {
            cell = tableView.dequeueReusableCell(withIdentifier: "ValuesTemplateDetailCellHeading") as! ValuesTemplateDetailCell
            
        }else {
            cell = tableView.dequeueReusableCell(withIdentifier: "ValuesTemplateDetailCellTags") as! ValuesTemplateDetailCell
        }
        if indexPath.section == 0 {
            let text = tableViewData[index].includedValues[indexPath.row]
            cell.titleLabel.text = text
            if indexPath.row != 0 {
                if selectedPositiveVals.contains(text) {
                    cell.parentView.backgroundColor = .systemGreen
                    cell.titleLabel.textColor = .white
                }else {
                    cell.parentView.backgroundColor = .white
                    cell.titleLabel.textColor = .black
                }
            }
            
        }else {
            let text = tableViewData[index].excludedValues[indexPath.row]
            cell.titleLabel.text = text
            if indexPath.row != 0 {
                if selectedNegativeVals.contains(text) {
                    cell.parentView.backgroundColor = .systemGreen
                    cell.titleLabel.textColor = .white
                }else {
                    cell.parentView.backgroundColor = .white
                    cell.titleLabel.textColor = .black
                }
            }
            
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if index == tableViewData.count - 1 {
            if indexPath.row != 0 {
                if indexPath.section == 0 {
                    let text = tableViewData[index].includedValues[indexPath.row]
                    if selectedPositiveVals.contains(text) {
                        selectedPositiveVals.remove(text)
                    }else {
                        selectedPositiveVals.append(text)
                    }
                }else {
                    let text = tableViewData[index].excludedValues[indexPath.row]
                    if selectedNegativeVals.contains(text) {
                        selectedNegativeVals.remove(text)
                    }else {
                        selectedNegativeVals.append(text)
                    }
                }
                self.tableView.reloadData()
            }
        }
    }
}
