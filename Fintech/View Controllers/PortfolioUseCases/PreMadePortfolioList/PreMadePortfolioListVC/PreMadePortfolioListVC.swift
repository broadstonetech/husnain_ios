//
//  PreMadePortfolioListVC.swift
//  Fintech
//
//  Created by broadstone on 04/03/2022.
//  Copyright © 2022 Broadstone Technologies. All rights reserved.
//

import UIKit

class PreMadePortfolioListVC: PortfolioParent, PortfolioFilterVCDelegte {
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    var portfoliosList: PreMadePortfolioListModel!
    var useCase: UseCaseType!
    
    var filtersData = [PortfolioFilterCategories]()

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        
        filtersData = PortfolioFilterData.shared.getFiltersData()
        
        getPreMadePortfolios()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        setNavBarTitile(title: "Back", topItem: getLocalizedString(key: "_portfolios"))
    }
    
    @IBAction func filterBtnPressed(_ sender: UIButton) {
        let storyboard = UIStoryboard.init(name: "Portfolio", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "PortfolioFilterVC") as! PortfolioFilterVC
        vc.filters = filtersData
        vc.delegate = self
        self.present(vc, animated: true, completion: nil)
    }
    
    func portfolioFilterDelegate(data: [PortfolioFilterCategories]) {
        self.filtersData = data
    }
}
