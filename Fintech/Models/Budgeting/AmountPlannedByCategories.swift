//
//  UsersBudgetingPlanDataModel.swift
//  vesgo
//
//  Created by Apple on 21/01/2021.
//  Copyright © 2021 Apple. All rights reserved.
//

// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let usersBudgetingPlanDataModel = try? newJSONDecoder().decode(UsersBudgetingPlanDataModel.self, from: jsonData)

import Foundation

// MARK: - UsersBudgetingPlanDataModel
struct UsersBudgetingPlanDataModel: Codable {
    let success: Int?
    let message: userbudGetingPlanMessage?
}

// MARK: - Message
struct userbudGetingPlanMessage: Codable {
    let totalAmount: Int?
    let planDivision: [PlanDivisionn]?
    
    enum CodingKeys: String, CodingKey {
        case totalAmount = "total_amount"
        case planDivision = "plan_division"
    }
}

// MARK: - PlanDivision
struct PlanDivisionn: Codable {
    let categoryName: String?
    let amount: Double?
    let subCategory: [SubCategoryy]?
    
    enum CodingKeys: String, CodingKey {
        case categoryName = "category_name"
        case amount
        case subCategory = "sub_category"
    }
}

// MARK: - SubCategory
struct SubCategoryy: Codable {
    let subCategory: String?
    let amount: Double?
    
    enum CodingKeys: String, CodingKey {
        case subCategory = "sub_category"
        case amount
    }
}
