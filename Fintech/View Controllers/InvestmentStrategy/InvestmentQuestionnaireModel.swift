//
//  QuestionnaireModel.swift
//  Fintech
//
//  Created by broadstone on 31/03/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import Foundation




struct QuestionnaireCategoriesModel {
    var category: String
    var categoryId: Int
    var questionsData = [Questionnaire]()
    var isCompleted: Bool
    var questionIdPrefix: String
    
    init(category: String, categoryId: Int, questionsData: [Questionnaire], isCompleted: Bool, questionIdPrefix: String) {
        self.category = category
        self.categoryId = categoryId
        self.questionsData = questionsData
        self.isCompleted = isCompleted
        self.questionIdPrefix = questionIdPrefix
    }
    
}

struct Questionnaire {
    var question: String!
    var questionId: Int!
    
    var answers: [String]!
    var answersId: [Int]!
    
    var selectedAnswers = [String]()
    var selectedAnswersIds = [Int]()
    
    var isMultiSelection: Bool!
    
    var tagsPrefix: String!
    
    init(question: String, questionId: Int, answers: [String], answersId: [Int], isMultiSelection: Bool, tagsPrefix: String) {
        self.question = question
        self.questionId = questionId
        self.answers = answers
        self.answersId = answersId
        self.isMultiSelection = isMultiSelection
        self.tagsPrefix = tagsPrefix
    }
}
