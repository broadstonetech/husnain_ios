// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let userValuesModel = try? newJSONDecoder().decode(UserValuesModel.self, from: jsonData)

import Foundation

// MARK: - UserValuesModel
struct UserValuesModel: Codable {
    let success: Int
    let data: UserValuesDataClass
}

// MARK: - DataClass
struct UserValuesDataClass: Codable {
    let positiveValues: [String]
    let negativeValues: [String]
    let portfolioComposition: UserValuesPortfolioComposition

    enum CodingKeys: String, CodingKey {
        case positiveValues = "positive_values"
        case negativeValues = "negative_values"
        case portfolioComposition = "portfolio_composition"
    }
}

// MARK: - PortfolioComposition
struct UserValuesPortfolioComposition: Codable {
    let esg: [UserValuesEsg]
    let derivedValues: [UserValuesDerivedValue]

    enum CodingKeys: String, CodingKey {
        case esg = "ESG"
        case derivedValues = "derived_values"
    }
}

// MARK: - DerivedValue
struct UserValuesDerivedValue: Codable {
    let value: String
    let score: Double
    let scoreMean, scoreCalculation: String
    let scoreImprovement: UserValuesScoreImprovement

    enum CodingKeys: String, CodingKey {
        case value, score
        case scoreMean = "score_mean"
        case scoreCalculation = "score_calculation"
        case scoreImprovement = "score_improvement"
    }
}

// MARK: - ScoreImprovement
struct UserValuesScoreImprovement: Codable {
}

// MARK: - Esg
struct UserValuesEsg: Codable {
    let value: String
    let score: Double
    let scoreMean, scoreCalculation: String
    let scoreImprovement: ScoreImprovement
    let percentile: Double
    let grade: String

    enum CodingKeys: String, CodingKey {
        case value, score
        case scoreMean = "score_mean"
        case scoreCalculation = "score_calculation"
        case scoreImprovement = "score_improvement"
        case percentile, grade
    }
}
