//
//  PortfolioFilterData.swift
//  Fintech
//
//  Created by broadstone on 04/03/2022.
//  Copyright © 2022 Broadstone Technologies. All rights reserved.
//

import Foundation

protocol PortfolioFilterVCDelegte : AnyObject{
    func portfolioFilterDelegate(data: [PortfolioFilterCategories])
}

enum PortfolioCategories {
    case OverAllESG, EnvESG, SocESG, GovESG, SortByAlphabets
}

struct PortfolioFilterCategories {
    var name: String
    var options: [PortfolioFilterCategoryOptions]
    var isMultiSelection: Bool
    var selectedOptions: Any
    var category: PortfolioCategories
}

struct PortfolioFilterCategoryOptions {
    var name: String
    var key: Any
    var isSelected: Bool = false
}

class PortfolioFilterData {
    
    static let shared = PortfolioFilterData()
    
    
    func getEsgFilterOptions() -> [PortfolioFilterCategoryOptions] {
        let options: [PortfolioFilterCategoryOptions] = [
            PortfolioFilterCategoryOptions.init(name: getLocalizedString(key: "_excellent"), key: "excellent"),
            PortfolioFilterCategoryOptions.init(name: getLocalizedString(key: "_good"), key: "good"),
            PortfolioFilterCategoryOptions.init(name: getLocalizedString(key: "_average"), key: "average"),
            PortfolioFilterCategoryOptions.init(name: getLocalizedString(key: "below_average"), key: "below_average"),
            PortfolioFilterCategoryOptions.init(name: getLocalizedString(key: "needs_improvement"), key: "needs_improvement")
        ]
        
        return options
    }
    
    
    
    func getSortByNameOptions() -> [PortfolioFilterCategoryOptions] {
        let options: [PortfolioFilterCategoryOptions] = [
            PortfolioFilterCategoryOptions.init(name: getLocalizedString(key: "sort_by_name_a_z"), key: SortByAlphabets.AtoZ),
            PortfolioFilterCategoryOptions.init(name: getLocalizedString(key: "sort_by_name_z_a"), key: SortByAlphabets.ZtoA)
        ]
        return options
    }
    
    
    func getFiltersData() -> [PortfolioFilterCategories] {
        var filters = [PortfolioFilterCategories]()
        
        filters.append(PortfolioFilterCategories.init(name: getLocalizedString(key: "portfolio_impact"), options: getEsgFilterOptions(), isMultiSelection: false, selectedOptions: "", category: .OverAllESG))
        filters.append(PortfolioFilterCategories.init(name: getLocalizedString(key: "env_score"), options: getEsgFilterOptions(), isMultiSelection: false, selectedOptions: "", category: .EnvESG))
        filters.append(PortfolioFilterCategories.init(name: getLocalizedString(key: "social_score"), options: getEsgFilterOptions(), isMultiSelection: false, selectedOptions: "", category: .SocESG))
        filters.append(PortfolioFilterCategories.init(name: getLocalizedString(key: "governance_score"), options: getEsgFilterOptions(), isMultiSelection: false, selectedOptions: "", category: .GovESG))

        filters.append(PortfolioFilterCategories.init(name: getLocalizedString(key: "sort_by_alphabet"), options: getSortByNameOptions(), isMultiSelection: false, selectedOptions: SortByAlphabets.notSelected, category: .SortByAlphabets))
        
        return filters
    }
}
