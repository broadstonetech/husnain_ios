//
//  ArrayEquateable.swift
//  Fintech
//
//  Created by broadstone on 10/01/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import Foundation
extension Array where Element:Equatable {
    public mutating func remove(_ item:Element ) {
        var index = 0
        while index < self.count {
            if self[index] == item {
                self.remove(at: index)
            } else {
                index += 1
            }
        }
    }

    public func array( removing item:Element ) -> [Element] {
        var result = self
        result.remove( item )
        return result
    }
}
