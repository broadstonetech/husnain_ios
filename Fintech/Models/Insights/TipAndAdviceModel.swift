// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let tipAndAdviceModel = try? newJSONDecoder().decode(TipAndAdviceModel.self, from: jsonData)

import Foundation

// MARK: - TipAndAdviceModel
struct TipAndAdviceModel: Codable {
    let success: Bool
    let data: TipsAndAdviceData
}

// MARK: - DataClass
struct TipsAndAdviceData: Codable {
    let totalSpending: Double
    let totalDifference: Double
    let categoryData: [CategoryDatum]

    enum CodingKeys: String, CodingKey {
        case totalSpending = "total_spending"
        case totalDifference = "total_difference"
        case categoryData = "category_data"
    }
}

// MARK: - CategoryDatum
struct CategoryDatum: Codable {
    let categoryName: String
    let difference, totalSpent: Double

    enum CodingKeys: String, CodingKey {
        case categoryName = "category_name"
        case difference
        case totalSpent = "total_spent"
    }
}
