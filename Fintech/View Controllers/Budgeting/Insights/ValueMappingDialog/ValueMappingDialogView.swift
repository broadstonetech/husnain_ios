//
//  ValueMappingDialogView.swift
//  Fintech
//
//  Created by broadstone on 11/03/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import UIKit

class ValueMappingDialogView: UIView {
    
    var valueMappingTransactions = [ValueMappingDetailsModel]()
    
    @IBOutlet weak var parentView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var categoryName: UILabel!
    @IBOutlet weak var closeBtn: UIButton!
    @IBOutlet weak var valueMappingDesc: UILabel!
    @IBOutlet weak var merchantNameLabel: UILabel!
    @IBOutlet weak var amountSpentLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
   
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    private func commonInit(){
        Bundle.main.loadNibNamed("ValueMappingView", owner: self, options: nil)
        addSubview(parentView)
        parentView.frame = self.bounds
        parentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    
//    @objc func closeBtnTapped(){
//        parentView.removeFromSuperview()
//    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    
    
    
    func show(){
        UIApplication.shared.keyWindow?.addSubview(parentView)
    }


}
extension ValueMappingDialogView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return valueMappingTransactions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let identifier = "ValueMappingDialogCell"
        var cell: ValueMappingDialogCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? ValueMappingDialogCell
        if cell == nil {
            tableView.register(UINib(nibName: "ValueMappingCellNibView", bundle: nil), forCellReuseIdentifier: identifier)
            cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? ValueMappingDialogCell
        }
        
        cell.merchantName.text = valueMappingTransactions[indexPath.row].marchantName
        cell.amountSpent.text = "$" + FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: valueMappingTransactions[indexPath.row].amount)
        cell.date.text = FintechUtils.sharedInstance.getDateInUSLocale(timestamp: valueMappingTransactions[indexPath.row].data / 1000)
        return cell
    }
}
