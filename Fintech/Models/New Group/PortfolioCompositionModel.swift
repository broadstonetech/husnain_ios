// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let portfolioCompositionModel = try? newJSONDecoder().decode(PortfolioCompositionModel.self, from: jsonData)

import Foundation

// MARK: - PortfolioCompositionModel
struct PortfolioCompositionModel: Codable {
    let success: Int
    let message: String
    let data: [PortfolioCompositionData]
}

// MARK: - Message
struct PortfolioCompositionData: Codable {
    let industryType: String
    let industryShare: Double
    let companies: [PortfolioCompositionCompany]

    enum CodingKeys: String, CodingKey {
        case industryType = "industry_type"
        case industryShare = "industry_share"
        case companies
    }
}

// MARK: - Company
struct PortfolioCompositionCompany: Codable {
    let companyName, companySymbol: String
    let companyShare: Double

    enum CodingKeys: String, CodingKey {
        case companyName = "company_name"
        case companySymbol = "company_symbol"
        case companyShare = "company_share"
    }
}
