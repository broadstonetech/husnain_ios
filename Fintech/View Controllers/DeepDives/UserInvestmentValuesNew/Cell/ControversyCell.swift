//
//  ControversyCell.swift
//  Fintech
//
//  Created by broadstone on 18/03/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import UIKit

class ControversyCell: UITableViewCell {
    
    @IBOutlet weak var controversyLabel: UILabel!
    @IBOutlet weak var controversyScore: UILabel!
    @IBOutlet weak var ellipses: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
