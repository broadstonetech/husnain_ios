//
//  CharityTOCVC.swift
//  Fintech
//
//  Created by Apple on 18/12/2020.
//  Copyright © 2020 Broadstone Technologies. All rights reserved.
//

import UIKit
import Lottie
import Firebase
import FirebaseAnalytics
var Headingtext = [String]()

class CharityTOCVC: UIViewController {
    
    @IBOutlet weak var agree_btn: UIButton!
    
    @IBOutlet weak var main_heding_topbar_lbl: UILabel!
    @IBOutlet weak var decline_btn: UIButton!
    @IBOutlet weak var Vesgo_logo_view: AnimationView!
    
    
    
    
    override func viewDidLoad() {
        prepareData()
        super.viewDidLoad()

       
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        self.Vesgo_logo_view.contentMode = .scaleAspectFit
        self.Vesgo_logo_view.animationSpeed = 1.0
        self.Vesgo_logo_view.play()
         localization()
        setAnalytics()
    }
    func localization(){
           let Charity_with_Vesgo_lbl = getLocalizedString(key: "Charity_with_Vesgo_lbl", comment: "")
        main_heding_topbar_lbl.text = Charity_with_Vesgo_lbl
        agree_btn.setTitle(getLocalizedString(key: "agree_btn", comment: ""), for: .normal)
        decline_btn.setTitle(getLocalizedString(key: "Decline_btn", comment: ""), for: .normal)

    }
    
    
    func setAnalyticsAction(ScreenName: String, method: String){
           
           let screenClass = classForCoder.description()
           Analytics.logEvent(AnalyticsEventScreenView, parameters: [AnalyticsParameterScreenName: ScreenName, AnalyticsParameterScreenClass: screenClass, "Method" : method])
       }
       
       func setAnalytics(){
           
           let screenClass = classForCoder.description()
           Analytics.logEvent(AnalyticsEventScreenView, parameters: [AnalyticsParameterScreenClass: screenClass])
           
       }
    
    
    @IBAction func agree_pressed(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Charity", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "listOfCharitiesVC") as! listOfCharitiesVC
        // self.navigationController?.pushViewController(vc, animated: true)
        self.navigationController?.pushViewController(vc, animated: true)
        SPManager.sharedInstance.setIsUserViewingCharityForFirstTime(isViewingFirstTime: false)
        
        setAnalyticsAction(ScreenName: "Charity to CVC", method: "Agree")
        
    }
    
    @IBAction func decline_pressed(_ sender: Any) {
        if let tabbedbar = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "nav_bar_start") as? UINavigationController {
            tabbedbar.modalPresentationStyle = .fullScreen
            UIApplication.shared.keyWindow?.rootViewController = tabbedbar
        }
        setAnalyticsAction(ScreenName: "Charity to CVC", method: "Agree")
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    func prepareData() {
        
        Headingtext.append(getLocalizedString(key: "lorum_ispum_text", comment: ""))
    }
    
    
    
}




// MARK: - Tableview
extension CharityTOCVC: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Headingtext.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! TOCCell
        cell.heading_lbl.text = Headingtext[indexPath.row]
        
        return cell
    }
   
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
