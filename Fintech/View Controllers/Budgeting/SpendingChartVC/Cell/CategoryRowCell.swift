//
//  CategoryRowCell.swift
//  Fintech
//
//  Created by broadstone on 13/02/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import Foundation
import UIKit

class CategoryRowCell: UITableViewCell {
    
    @IBOutlet weak var imageBGView: UIView!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var categoryName: UILabel!
    @IBOutlet weak var amountSpent: UILabel!
    
    
    override class func awakeFromNib() {
        super.awakeFromNib()
    }
}
