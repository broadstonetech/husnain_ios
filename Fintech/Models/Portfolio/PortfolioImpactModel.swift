import Foundation

struct PortfolioImpact {
    let success: Bool
    let message: String
    let data: PortfolioCreationData
}

struct PortfolioCreationData {
    let overallESG: PortfolioCreationESG
    let assets: [PortfolioCreationAssets]
    let portfolioInfo: PortfolioInfo
    let controversyData: ControversyData
}

struct PortfolioCreationESG {
    let impact: String
    let overallBetterPeers: String
    let overallBadPeers: String
    let zScore: Double
    let risk: Double
    let environment: PortfolioCreationComposition
    let social: PortfolioCreationComposition
    let governance: PortfolioCreationComposition
    
}

struct PortfolioCreationAssets {
    let ticker: String
    let symbolName: String
    let industry: String
    let allocation: Double
    let portfolioComposition: PortfolioCreationESG
}


struct PortfolioCreationComposition {
    let impact: String
    let betterPeers: String
    let badPeers: String
    let zScore: Double
    let risk: Double
    let derivedValues : [PortfolioDerivedValues]
}

struct PortfolioDerivedValues {
    let value: String
    let score: Double
}

struct PortfolioInfo {
    var portfolioName: String = ""
    var portfolioId: Int = -1
    let portfolioType: String
}
