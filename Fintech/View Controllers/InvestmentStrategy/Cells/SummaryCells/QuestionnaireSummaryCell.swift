//
//  QuestionnaireSummaryCell.swift
//  Fintech
//
//  Created by broadstone on 20/10/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import UIKit

class QuestionnaireSummaryCell: UITableViewCell {
    
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var answerLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
