//
//  BudgetingDashboard.swift
//  vesgo
//
//  Created by Apple on 05/01/2021.
//  Copyright © 2021 Apple. All rights reserved.
//

import UIKit
import RingGraph
import Charts

var amountvalues = ["$86.00","$67.00","$40.00","$30.21","$12.00","$43.00","$32.00","$23.00","$40.00","$00.00"]
var date = ["01-01-2021","01-02-2021","01-03-2021","01-04-2021","01-05-2021","01-06-2021","01-07-2021","01-08-2021","01-09-2021","01-10-2021"]
class BudgetingDashboard: BudgetingParent {
    
    struct CardsCategories {
        let categoryName: String
        let amount: Double
        let bucketType: String
        let index: Int
    }
    
    //MARK:- Local Variables
    
    var heading = ["Mortgage/Rent", "Online services", "Education", "Healthcare", "Dining out", "Groceries", "Insurance", "Utilities", "Misc."]
    
    let lineChartDataPoints = ["Oct-20", "Nov-20", "Dec-20", "Jan-21"]
    let plannedAmount = [10000.0, 10400.0, 9000.0, 11000.0]
    let spentAmount = [10450.0, 9500.0, 8500.0, 12300.0]
    var singleGraphMeter = [RingMeter]()
    
    var budgetingDashboardModel: BudgetingDashboardModel!
    var transactionsDict = OrderedDictionary<String, Double>()
    var frequencyType = BudgetingConstants.FREQUENCY_TYPE_DAY
    
    var budgetingOldData: BudgetingOldPlanModel!
    
    var transactionsCards = [CardsCategories]()
    
    var isShowingPercentages = false
    
    var notificationTypeKey = FCM.NOTIFICATION_TYPE_DEFAULT
  
    //MARK:- IBOutlets
    
    @IBOutlet weak var planDurationLabel: UILabel!
    @IBOutlet weak var trackSpendingBtn: UIButton!
    @IBOutlet weak var cardsCollectionView: UICollectionView!
    @IBOutlet weak var AddCardView: UIView!
    @IBOutlet weak var transactionsView: UIView!
    @IBOutlet weak var spendingChartView: RingGraphView!

    
    
    @IBOutlet weak var dayBtn: UIButton!
    @IBOutlet weak var weekBtn: UIButton!
    @IBOutlet weak var monthBtn: UIButton!
    @IBOutlet weak var allBtn: UIButton!
    @IBOutlet weak var totalAmountSpent: UILabel!
    @IBOutlet weak var noDataFoundLabel: UILabel!
    
    @IBOutlet weak var barChartView: BarChartView!
    @IBOutlet weak var summaryViewBtn: UIButton!
    @IBOutlet weak var amountLabelsView: UIView!
    
    @IBOutlet weak var totalAmountLabel: UILabel!
    @IBOutlet weak var needsAmountLabel: UILabel!
    @IBOutlet weak var wantsAmountLabel: UILabel!
    @IBOutlet weak var havesAmountLabel: UILabel!
    
    @IBOutlet weak var parentView: UIView!
    @IBOutlet weak var bucketCardsStack: UIStackView!
    
    
    
    //MARK:- Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(bucketCardsTapped))
        bucketCardsStack.isUserInteractionEnabled = true
        bucketCardsStack.addGestureRecognizer(tapGesture)
        
        createPlusButton()
        
        if notificationTypeKey == FCM.NOTIFICATION_TYPE_SPENDING_INSIGHTS {
            insightsButtonPressed(self)
            return
        }
        
        getBudgetingOldPlanData() //get summary data
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        //setNavBarTitile(title: "Dashboard", topItem: "Dashboard")
        
        setNavBarTitle()
        
        removeAllPreviousVC()
           
        setDefaultFrequencyBtnsColor() //get details based on frequesncy

        //populateMovementsRingCharts(currentValue: 5000, maxValue: 12000)
        
    }
    
    //MARK:- Class Functions
    func setNavBarTitle(){
        if spendingChartView.isHidden {
            setNavBarTitile(title: "", topItem: getLocalizedString(key: "_summary"))
        }else {
            setNavBarTitile(title: "", topItem: getLocalizedString(key: "_spending"))
        }
    }
    
    func createPlusButton(){
        let summaryImage    = UIImage(named: "summary_chart")!
        let trackSpendingImage  = UIImage(named: "track_spending")!
        let editPlanImage  = UIImage(named: "edit_spending_plan")!
        
        let summaryBtn = UIButton(frame: CGRect(x: 0.0, y: 0.0, width: 30.0, height: 30.0))
        summaryBtn.setImage(summaryImage, for: .normal)
        summaryBtn.imageView?.contentMode = .scaleAspectFit
        //summaryBtn.frame = CGRect(x: 0.0, y: 0.0, width: 35.0, height: 35.0)
        summaryBtn.addTarget(self, action: #selector(summaryViewBtnPressed(_:)), for: .touchUpInside)
        summaryBtn.contentEdgeInsets.right = 5
        let barButtonItem = UIBarButtonItem(customView: summaryBtn)
        
        
        
        //let barButtonItem = UIBarButtonItem(image: summaryImage, style: .plain, target: self, action: #selector(summaryViewBtnPressed(_:)))

        let trackSpendingBtn = UIButton(type: .custom)
        trackSpendingBtn.setImage(trackSpendingImage, for: .normal)
        trackSpendingBtn.frame = CGRect(x: 0.0, y: 0.0, width: 30.0, height: 30.0)
        trackSpendingBtn.addTarget(self, action: #selector(manualTrackingBtnPressed(_:)), for: .touchUpInside)
        trackSpendingBtn.contentEdgeInsets.right = 5
        let barButtonItem1 = UIBarButtonItem(customView: trackSpendingBtn)
        
        let editPlanBtn = UIButton(type: .custom)
        editPlanBtn.setImage(editPlanImage, for: .normal)
        editPlanBtn.frame = CGRect(x: 0, y: 0.0, width: 30.0, height: 30.0)
        editPlanBtn.addTarget(self, action: #selector(editPlanBtnPressed(_:)), for: .touchUpInside)
        editPlanBtn.contentEdgeInsets.right = 5
        let barButtonItem2 = UIBarButtonItem(customView: editPlanBtn)
        
        
        let stackView = UIStackView(arrangedSubviews: [summaryBtn, trackSpendingBtn, editPlanBtn])
        stackView.alignment = .fill
        stackView.distribution = .fillEqually
        stackView.spacing = 8
        stackView.axis = .horizontal
        
        let barBtn = UIBarButtonItem(customView: stackView)

        //navigationItem.rightBarButtonItems = [barButtonItem, barButtonItem1, barButtonItem2]
        navigationItem.rightBarButtonItems = [barBtn]
    }
    
    @objc private func createBottomSheetForNavigation(){
        let optionMenu = UIAlertController(title: nil, message: getLocalizedString(key: "select_action"), preferredStyle: .actionSheet)
        
        let trackSpending = UIAlertAction(title: getLocalizedString(key: "track_spending"), style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            self.manualTrackingBtnPressed(self)
            
        })
        
        let editPlan = UIAlertAction(title: getLocalizedString(key: "edit_spending_plan"), style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            self.editPlanBtnPressed(self)
            
        })
        
        let cancel = UIAlertAction(title: getLocalizedString(key: "cancel_btn"), style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            
            self.dismiss(animated: true, completion: nil)
            
        })
        
        
        
        optionMenu.addAction(trackSpending)
        optionMenu.addAction(editPlan)
        optionMenu.addAction(cancel)
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    
    private func removeAllPreviousVC(){
        navigationController?.viewControllers.removeAll(where: { (vc) -> Bool in
            if vc.isKind(of: plusBudgetingVC.self) {
                return true
            } else {
                return false
            }
        })
    }
    
    
    private func populateMovementsRingCharts() {
        if budgetingDashboardModel == nil {
            return
        }
        
            if #available(iOS 13.0, *) {
                
                let needsImage = ImageProvider(image: FintechUtils.sharedInstance.getBucketIcon(bucketType: "needs").withTintColor(.white))
                let wantsImage = ImageProvider(image: FintechUtils.sharedInstance.getBucketIcon(bucketType: "wants").withTintColor(.white))
                let havesImage = ImageProvider(image: FintechUtils.sharedInstance.getBucketIcon(bucketType: "haves").withTintColor(.white))
                
                
                let tripleGraphMeters =
                    [RingMeter(title: "Needs", value: Int(budgetingDashboardModel.data!.planInfo!.needsBucketSpent!), maxValue: Int(budgetingDashboardModel.data!.planInfo!.needsBucketAmount!), colors: [UIColor(named: "NeedsBucketColor")!], symbolProvider: needsImage, backgroundColor : .systemGray5),
                     RingMeter(title: "Wants", value: Int(budgetingDashboardModel.data!.planInfo!.wantsBucketSpent!), maxValue: Int(budgetingDashboardModel.data!.planInfo!.wantsBucketAmount!), colors: [UIColor(named: "WantsBucketColor")!], symbolProvider: wantsImage, backgroundColor : .systemGray5),
                     RingMeter(title: "Haves", value: Int(budgetingDashboardModel.data!.planInfo!.havesBucketSpent!), maxValue: Int(budgetingDashboardModel.data!.planInfo!.havesBucketAmount!), colors: [UIColor(named: "HavesBucketColor")!], symbolProvider: havesImage, backgroundColor : .systemGray5)]

                if let graph = RingGraph(meters: tripleGraphMeters) {
                    let viewFrame = spendingChartView.frame
                    let graphFrame = CGRect(x: 0, y: 0, width: viewFrame.width, height: viewFrame.height)
                    let ringGraphView = RingGraphView(frame: graphFrame, graph: graph, preset: .MetersDescription)
//                    if let chartViewAdded = ringGraphView.viewWithTag(909090){
//                        chartViewAdded.removeFromSuperview()
//                    }
                    for view in self.spendingChartView.subviews {
                        view.removeFromSuperview()
                    }
                    ringGraphView.backgroundColor = .clear
                    //ringGraphView.tag = 909090
                    
                    spendingChartView.addSubview(ringGraphView)
                    spendingChartView.backgroundColor = .clear
                    
                    
                    
                    
                    
                    
                }
            } else {
                // Fallback on earlier versions
            }
    }
    
    //MARK:- IB Actions

    @IBAction func addCardButtonPressed(_ sender: Any) {
        getPlaidLinkToken()
    }
    
    @IBAction func manualTrackingBtnPressed(_ sender: Any) {
        let vc = UIStoryboard(name: "Budgeting", bundle: nil).instantiateViewController(withIdentifier: "ExpenseTracking") as! UIViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    @IBAction func insightsButtonPressed(_ sender: Any) {
        let vc = UIStoryboard(name: "Budgeting", bundle: nil).instantiateViewController(withIdentifier: "InsightsScene") 
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func summaryViewBtnPressed(_ sender: UISwitch) {
        if spendingChartView.isHidden {
            spendingChartView.isHidden = false
            amountLabelsView.isHidden = false
            planDurationLabel.isHidden = false
            barChartView.isHidden = true
            //summaryViewBtn.setTitle("Summary", for: .normal)
            
        }else {
            spendingChartView.isHidden = true
            amountLabelsView.isHidden = true
            planDurationLabel.isHidden = true
            barChartView.isHidden = false
            //summaryViewBtn.setTitle("Spending", for: .normal)
            
            
            if budgetingOldData == nil {
                getBudgetingOldPlanData()
            }else {
                drawLineChart()
            }
            
        }
        
        setNavBarTitle()
    }
    
    @objc func bucketCardsTapped() {
        if isShowingPercentages {
            isShowingPercentages = false
        }else {
            isShowingPercentages = true
        }
        UIView.transition(with: self.bucketCardsStack, duration: 1, options: [.transitionFlipFromBottom], animations: nil, completion: { _ in
            self.showAmountOrPercentageInBucket()
        })
        
        
    }
    
    private func showAmountOrPercentageInBucket(){
        if isShowingPercentages {
            
            var needsBucketTotalAmount = budgetingDashboardModel.data!.planInfo!.needsBucketAmount
            var wantsBucketTotalAmount = budgetingDashboardModel.data!.planInfo!.wantsBucketAmount
            var havesBucketTotalAmount = budgetingDashboardModel.data!.planInfo!.havesBucketAmount
            
            if needsBucketTotalAmount == 0.0 {
                needsBucketTotalAmount = 1
            }
            
            if wantsBucketTotalAmount == 0.0 {
                wantsBucketTotalAmount = 1
            }
            
            if havesBucketTotalAmount == 0.0 {
                havesBucketTotalAmount = 1
            }
            
            let needsPercent = (budgetingDashboardModel.data!.planInfo!.needsBucketSpent! / needsBucketTotalAmount!) * 100
            
            let wantsPercent = (budgetingDashboardModel.data!.planInfo!.wantsBucketSpent! / wantsBucketTotalAmount!) * 100
            
            let havesPercent = (budgetingDashboardModel.data!.planInfo!.havesBucketSpent! / havesBucketTotalAmount!) * 100
            
            needsAmountLabel.text = "\(FintechUtils.sharedInstance.doubleToPercentageOutput(doubleVal: needsPercent))%"
    
            
            wantsAmountLabel.text = "\(FintechUtils.sharedInstance.doubleToPercentageOutput(doubleVal: wantsPercent))%"
    
            
            havesAmountLabel.text = "\(FintechUtils.sharedInstance.doubleToPercentageOutput(doubleVal: havesPercent))%"
    
        }else {
            
            needsAmountLabel.text = "$\(FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: (budgetingDashboardModel.data!.planInfo!.needsBucketSpent)!))"
    
            
            wantsAmountLabel.text = "$\(FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: (budgetingDashboardModel.data!.planInfo!.wantsBucketSpent)!))"
    
            
            havesAmountLabel.text = "$\(FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: (budgetingDashboardModel.data!.planInfo!.havesBucketSpent)!))"
            
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    //MARK:- Budgeting Dashboard API
    func  getBudgetingDashboardDataAPI(){
        if budgetingDashboardModel == nil {
            parentView.isHidden = true
        }
        view.activityStartAnimating()
        let url = ApiUrl.GET_BUDGETING_DASHBOARD_DATA
        let namespace = SPManager.sharedInstance.getCurrentUserNamespace()

        postManagerUser(method: "POST", isThisURLEmbdedData: false, urlString: url, parameter: ["namespace": namespace, "frequency": frequencyType], success: {(value) in

            do {
                self.budgetingDashboardModel = try JSONDecoder().decode(BudgetingDashboardModel?.self, from: value)
                DispatchQueue.main.async { [self] in
                    fetchData()
                }
                

            } catch { }

            
            DispatchQueue.main.async { [self] in
                view.activityStopAnimating()
                parentView.isHidden = false
            }

        }, failure: {(err) in

            DispatchQueue.main.async { [self] in
                view.activityStopAnimating()
            }

            if APIManager.sharedInstance.error400 {

                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)

            }

            if(APIManager.sharedInstance.error401)

            {

                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)

            }else{
                self.showAlert(ConstantStrings.Error_msg_generic)
                

            }
        })

    }
    
    //Mark:- Fetch data
    private func fetchData(){
        transactionsDict.removeAll()
        
        
        totalAmountLabel.text = "$\(FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: (budgetingDashboardModel.data!.planInfo!.plannedAmountSpent)!)) " + getLocalizedString(key: "out_of") + "  $\(FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: (budgetingDashboardModel.data!.planInfo!.plannedAmount)!))"
        
//        needsAmountLabel.text = "$\(FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: (budgetingDashboardModel.data!.needsBucketSpent)!))"
//        of $\(FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: (budgetingDashboardModel.data!.needsBucketAmount)!))"
        
//        wantsAmountLabel.text = "$\(FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: (budgetingDashboardModel.data!.wantsBucketSpent)!))"
//        of $\(FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: (budgetingDashboardModel.data!.wantsBucketAmount)!))"
        
//        havesAmountLabel.text = "$\(FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: (budgetingDashboardModel.data!.havesBucketSpent)!))"
//        " of $\(FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: (budgetingDashboardModel.data!.havesBucketAmount)!))"
        
        showAmountOrPercentageInBucket()
        
        totalAmountSpent.text = "$" + FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: budgetingDashboardModel.data!.spendingHistory!.totalAmountSpent!)
        
        let startDateText = FintechUtils.sharedInstance.getDateInUSLocale(timestamp: Int64(((budgetingDashboardModel.data!.planInfo!.plannedStartDate)!/1000)))
        let endDateText = FintechUtils.sharedInstance.getDateInUSLocale(timestamp: Int64(((budgetingDashboardModel.data!.planInfo!.plannedEndDate)!/1000)))
        planDurationLabel.text = startDateText + " " + getLocalizedString(key: "_to") + " " + endDateText
        
        transactionsCards = [CardsCategories]()
        
        for i in 0..<budgetingDashboardModel.data!.data!.needsBucket!.count {
            if budgetingDashboardModel.data!.data!.needsBucket![i].amount != 0 {
                let categoryName = budgetingDashboardModel.data!.data!.needsBucket![i].categoryName
                let amount = budgetingDashboardModel.data!.data!.needsBucket![i].amount
                transactionsCards.append(CardsCategories(categoryName: categoryName!, amount: amount!, bucketType: "needs", index: i))
            }
        }
        
        for i in 0..<budgetingDashboardModel.data!.data!.wantsBucket!.count {
            if budgetingDashboardModel.data!.data!.wantsBucket![i].amount != 0 {
                let categoryName = budgetingDashboardModel.data!.data!.wantsBucket![i].categoryName
                let amount = budgetingDashboardModel.data!.data!.wantsBucket![i].amount
                transactionsCards.append(CardsCategories(categoryName: categoryName!, amount: amount!, bucketType: "wants", index: i))
            }
        }
        
        for i in 0..<budgetingDashboardModel.data!.data!.havesBucket!.count {
            if budgetingDashboardModel.data!.data!.havesBucket![i].amount != 0 {
                let categoryName = budgetingDashboardModel.data!.data!.havesBucket![i].categoryName
                let amount = budgetingDashboardModel.data!.data!.havesBucket![i].amount
                transactionsCards.append(CardsCategories(categoryName: categoryName!, amount: amount!, bucketType: "haves", index: i))
            }
        }
        for i in 0..<budgetingDashboardModel.data!.data!.miscBucket!.count {
            if budgetingDashboardModel.data!.data!.miscBucket![i].amount != 0 {
                let categoryName = budgetingDashboardModel.data!.data!.miscBucket![i].categoryName
                let amount = budgetingDashboardModel.data!.data!.miscBucket![i].amount
                transactionsCards.append(CardsCategories(categoryName: categoryName!, amount: amount!, bucketType: "misc", index: i))
            }
        }
        
        if transactionsCards.count == 0 {
            noDataFoundLabel.isHidden = false
            cardsCollectionView.isHidden = true
        }else{
            noDataFoundLabel.isHidden = true
            cardsCollectionView.isHidden = false
            cardsCollectionView.reloadData()
        }
        
        populateMovementsRingCharts()
    }
    
    
    //MARK:- Old Budgeting Plans API
    func  getBudgetingOldPlanData(){
        view.activityStartAnimating()
        let url = ApiUrl.GET_BUDGETING_OLD_DATA
        let namespace = SPManager.sharedInstance.getCurrentUserNamespace()

        postManagerUser(method: "POST", isThisURLEmbdedData: false, urlString: url, parameter: ["namespace": namespace], success: {(value) in

            do {
                self.budgetingOldData = try JSONDecoder().decode(BudgetingOldPlanModel?.self, from: value)
                DispatchQueue.main.async { [self] in
                    drawLineChart()
                }
                
            } catch {
                DispatchQueue.main.async { [self] in
                    let oldPlanData = [BudgetinOldPlanDatum]()
                    budgetingOldData = BudgetingOldPlanModel.init(success: false, data: oldPlanData)
                    drawLineChart()
                }
            }
            
            DispatchQueue.main.async { [self] in
                view.activityStopAnimating()
            }

        }, failure: {(err) in

            DispatchQueue.main.async { [self] in
                view.activityStopAnimating()
            }

            if APIManager.sharedInstance.error400 {

                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)

            }

            if(APIManager.sharedInstance.error401)

            {

                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)

            }else{
                self.showAlert(ConstantStrings.Error_msg_generic)
                

            }
        })

    }
    
    //Mark:- Frequency Btn Actions
    @IBAction func allBtnPressed(_ sender: Any) {
        frequencyType = BudgetingConstants.FREQUENCY_TYPE_ALL
        
        setDefaultFrequencyBtnsColor()
    }
    
    @IBAction func monthBtnPressed(_ sender: Any) {
        frequencyType = BudgetingConstants.FREQUENCY_TYPE_MONTH
        
        setDefaultFrequencyBtnsColor()
    }
    
    @IBAction func weekBtnPressed(_ sender: Any) {
        frequencyType = BudgetingConstants.FREQUENCY_TYPE_WEEK
        
        setDefaultFrequencyBtnsColor()
    }
    
    @IBAction func dayBtnPressed(_ sender: Any) {
        frequencyType = BudgetingConstants.FREQUENCY_TYPE_DAY
        
        setDefaultFrequencyBtnsColor()
    }
    
    @IBAction func editPlanBtnPressed(_ sender: Any) {
        showBudgetPlanTypeBottomSheet()
    }
    
    @IBAction func categoryChartViewBtnPressed(_ sender: Any) {
        let vc = UIStoryboard(name: "Budgeting", bundle: nil).instantiateViewController(withIdentifier: "CategorySpendingsChart") as! UIViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    private func setDefaultFrequencyBtnsColor(){
        dayBtn.setTitleColor(.systemGray, for: .normal)
        weekBtn.setTitleColor(.systemGray, for: .normal)
        monthBtn.setTitleColor(.systemGray, for: .normal)
        allBtn.setTitleColor(.systemGray, for: .normal)
        
        if frequencyType == BudgetingConstants.FREQUENCY_TYPE_DAY {
            dayBtn.setTitleColor(.black, for: .normal)
        }else if frequencyType == BudgetingConstants.FREQUENCY_TYPE_WEEK {
            weekBtn.setTitleColor(.black, for: .normal)
        }else if frequencyType == BudgetingConstants.FREQUENCY_TYPE_MONTH {
            monthBtn.setTitleColor(.black, for: .normal)
        }else if frequencyType == BudgetingConstants.FREQUENCY_TYPE_ALL {
            allBtn.setTitleColor(.black, for: .normal)
        }
        
        getBudgetingDashboardDataAPI() //get frequency and current plan details
    }
    
}
extension BudgetingDashboard:UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return transactionsCards.count
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! budgetingDashboardCell
        
        
        
        let element = transactionsCards[indexPath.row]
        
        cell.heading_lbl.text = element.categoryName
        cell.amount_lbl.text = "$" + FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: element.amount)
        
        cell.icon_img.image = FintechUtils.sharedInstance.getCaetgoryImageForBudgeting(value: element.categoryName.lowercased())
        cell.bucketIcon.image = FintechUtils.sharedInstance.getBucketIcon(bucketType: element.bucketType)
        
        if element.bucketType == "needs" {
            cell.mainview.backgroundColor = FintechUtils.sharedInstance.getColoredBackgroundForNeedsCategories(rowPosition: element.index)
        }else if element.bucketType == "wants" {
            cell.mainview.backgroundColor = FintechUtils.sharedInstance.getColoredBackgroundForWantsCategories(rowPosition: element.index)
        }else if element.bucketType == "haves" {
            cell.mainview.backgroundColor = FintechUtils.sharedInstance.getColoredBackgroundForHavesCategories(rowPosition: element.index)
        }else {
            cell.mainview.backgroundColor = .systemTeal
        }
        
        return cell
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        
        let vc = UIStoryboard(name: "Budgeting", bundle: nil).instantiateViewController(withIdentifier: "ExpenseDetailVC") as! UIViewController
        categorySelected = transactionsCards[indexPath.row].categoryName
        budgetingHistoryFrequencyType = self.frequencyType
        self.navigationController?.pushViewController(vc, animated: true)

        
        
        
        //print("aaaa")
    }
    
    
}
extension BudgetingDashboard: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = collectionView.frame.size.width
        let sizee = collectionView.frame.size

        //size.height
        return CGSize(width: size / 2.5 , height: sizee.height)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
}
extension BudgetingDashboard: ChartViewDelegate {
    private func drawLineChart(){
        
        var dataPoints = [String]()
        var values = [Double]()
        var values2 = [Double]()
        
        for i in 0..<budgetingOldData.data.count {
            dataPoints.append(FintechUtils.sharedInstance.getShortDate(fromSeconds: budgetingOldData.data[i].xAxis/1000))
            values.append(budgetingOldData.data[i].yAxisPlanned)
            values2.append(budgetingOldData.data[i].yAxisSpent)
        }
        
        let data = BarChartData()
        var lineChartEntry1 = [BarChartDataEntry]()
        var lineChartEntry2 = [BarChartDataEntry]()

        for i in 0..<values.count {
            lineChartEntry1.append(BarChartDataEntry(x: Double(i), y: Double(values[i]) ))
        }
        
        
        let line1 = BarChartDataSet(entries: lineChartEntry1, label: "Planned")
        line1.colors=[#colorLiteral(red: 0.337254902, green: 0.7725490196, blue: 0.5882352941, alpha: 1)]
        line1.drawValuesEnabled = false
        
        
        
        for i in 0..<values2.count {
            lineChartEntry2.append(BarChartDataEntry(x: Double(i), y: Double(values2[i]) ))
        }
        let line2 = BarChartDataSet(entries: lineChartEntry2, label: "Actual")
        line2.drawValuesEnabled = false
        line2.highlightEnabled = true
        line2.colors = [#colorLiteral(red: 0.9372549057, green: 0.3490196168, blue: 0.1921568662, alpha: 1)]
        
        let groupSpace = 0.15
        let barSpace = 0.05
        let barWidth = 0.4
        data.barWidth = barWidth
        
        
        data.addDataSet(line1)

        data.addDataSet(line2)
        data.setValueTextColor(NSUIColor.black)
        data.setValueFont(UIFont.init(name: "Verdana", size: 12.0)!)
        self.barChartView.data=data
        
        barChartView.leftAxis.axisMinimum = 0
        barChartView.xAxis.axisMinimum = Double(0)
        barChartView.xAxis.axisMaximum = Double(0) + data.groupWidth(groupSpace: groupSpace, barSpace: barSpace) * Double(2)  // group count : 2
        data.groupBars(fromX: Double(0), groupSpace: groupSpace, barSpace: barSpace)
        
        
        barChartView.xAxis.valueFormatter = IndexAxisValueFormatter(values:dataPoints)
        barChartView.xAxis.granularityEnabled = true
        barChartView.xAxis.centerAxisLabelsEnabled = true
        barChartView.xAxis.labelPosition = .bottom
        
        barChartView.xAxis.labelTextColor = UIColor.black
        barChartView.leftAxis.labelTextColor = UIColor.black

        barChartView.xAxis.granularity = 1
        barChartView.leftAxis.enabled = true
        barChartView.pinchZoomEnabled = true
        barChartView.scaleYEnabled = true
        barChartView.scaleXEnabled = true
        barChartView.extraBottomOffset = -15
        barChartView.legend.horizontalAlignment = Legend.HorizontalAlignment.right
        //barChartView.legend.verticalAlignment = Legend.VerticalAlignment.bottom
        barChartView.setVisibleXRangeMaximum(4)
        barChartView.setVisibleXRangeMinimum(4)
        
        
        /// lineChartView.setVisibleXRangeMaximum(11)
        
        
        //lineChartView.moveViewToX(90)
        barChartView.leftAxis.accessibilityScroll(UIAccessibilityScrollDirection(rawValue: Int(20))!)
        barChartView.doubleTapToZoomEnabled = false
        barChartView.backgroundColor = UIColor.clear
        barChartView.chartDescription?.text = ""
        barChartView.leftAxis.drawGridLinesEnabled = false
        barChartView.rightAxis.drawGridLinesEnabled = false
        barChartView.xAxis.drawGridLinesEnabled = false
        //lineChartView.description = "data is being processed"
        let yAxis = barChartView.rightAxis
        
        barChartView.leftAxis.granularity = 1.0
        
        yAxis.enabled = false
        yAxis.drawLabelsEnabled = false
        yAxis.drawAxisLineEnabled = false
        yAxis.drawGridLinesEnabled = false
        self.barChartView.xAxis.labelFont = UIFont.init(name: "Verdana", size: 12.0)!
        
        let legend = barChartView.legend
        legend.font = UIFont(name: "Verdana", size: 13.0)!
        barChartView.legend.textColor = UIColor.black
        //lineChartView.animate(xAxisDuration: 0.5, yAxisDuration: 1.0, easingOption: .easeInOutQuart)
        barChartView.drawGridBackgroundEnabled = false
        
        let valFormatter = NumberFormatter()
        valFormatter.numberStyle = .currency
        valFormatter.maximumFractionDigits = 2
        valFormatter.currencySymbol = "$"

        barChartView.leftAxis.valueFormatter = DefaultAxisValueFormatter(formatter: valFormatter)
        
        barChartView.delegate = self
        
        if dataPoints.isEmpty {
            barChartView.clear()
        }
        
    }
    
    public func chartValueSelected(_ chartView: ChartViewBase, entry: ChartDataEntry, highlight: Highlight){
        print("chartValueSelected : x = \(highlight.x)")
        let barChartEntry = entry.copy() as! BarChartDataEntry
    }
    
    public func chartValueNothingSelected(_ chartView: ChartViewBase){
        print("chartValueNothingSelected")
    }
}
private extension BudgetingDashboard {
    private func showBudgetPlanTypeBottomSheet(){
        let optionMenu = UIAlertController(title: nil, message: getLocalizedString(key: "budgeting_heading_dashboard"), preferredStyle: .actionSheet)
        
        let update = UIAlertAction(title: getLocalizedString(key: "_update"), style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            let vc = UIStoryboard(name: "Budgeting", bundle: nil).instantiateViewController(withIdentifier: "BudgetingPlan") as! plusBudgetingVC
            vc.budgetType = "update"
            self.navigationController?.pushViewController(vc, animated: true)
            
        })
        let createNew = UIAlertAction(title: getLocalizedString(key: "_new"), style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            let vc = UIStoryboard(name: "Budgeting", bundle: nil).instantiateViewController(withIdentifier: "BudgetingPlan") as! plusBudgetingVC
            vc.budgetType = "new"
            self.navigationController?.pushViewController(vc, animated: true)
            
        })
        
        let cancel = UIAlertAction(title: getLocalizedString(key: "cancel_btn"), style: .cancel, handler: {
            _ in
            self.dismiss(animated: true)
        })
        
        
        
        
        optionMenu.addAction(createNew)
        optionMenu.addAction(update)
        optionMenu.addAction(cancel)
        
        self.present(optionMenu, animated: true, completion: nil)
    }
}
