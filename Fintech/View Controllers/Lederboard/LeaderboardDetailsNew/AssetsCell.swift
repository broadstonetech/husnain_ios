//
//  AssetsCell.swift
//  Fintech
//
//  Created by broadstone on 18/03/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import UIKit

class AssetsCell: UITableViewCell {
    @IBOutlet weak var assetSymbol: UILabel!
    @IBOutlet weak var assetName: UILabel!
    @IBOutlet weak var assetShare: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
