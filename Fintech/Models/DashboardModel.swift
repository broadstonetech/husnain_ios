// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let dashboardModel = try? newJSONDecoder().decode(DashboardModel.self, from: jsonData)

import Foundation

// MARK: - DashboardModel
struct DashboardModel: Codable {
    let dashboardMessage: DashboardMessage
    
    enum CodingKeys: String, CodingKey {
        case dashboardMessage = "message"
    }
}

// MARK: - Message
struct DashboardMessage: Codable {
    let userInfo: UserInfo
    let risk: Double
    let positiveValues, negativeValues: [String]
    let portfolioComposition: DashboardPortfolioComposition

    enum CodingKeys: String, CodingKey {
        case userInfo = "user_info"
        case risk
        case positiveValues = "positive_values"
        case negativeValues = "negative_values"
        case portfolioComposition = "portfolio_composition"
    }
}

// MARK: - PortfolioComposition
struct DashboardPortfolioComposition: Codable {
    let esg, derivedValues: [DashboardEsg]

    enum CodingKeys: String, CodingKey {
        case esg = "ESG"
        case derivedValues = "derived_values"
    }
}

// MARK: - Esg
struct DashboardEsg: Codable {
    let value: String
    let score: Double
    let scoreMean, scoreCalculation: String
    let scoreImprovement: DashboardScoreImprovement

    enum CodingKeys: String, CodingKey {
        case value, score
        case scoreMean = "score_mean"
        case scoreCalculation = "score_calculation"
        case scoreImprovement = "score_improvement"
    }
}

// MARK: - ScoreImprovement
struct DashboardScoreImprovement: Codable {
}

// MARK: - UserInfo
struct UserInfo: Codable {
    let userInfoReturn, amount, userKeyFace, gender: String
    let ageGroup: String

    enum CodingKeys: String, CodingKey {
        case userInfoReturn = "return"
        case amount
        case userKeyFace = "user_key_face"
        case gender
        case ageGroup = "age_group"
    }
}
