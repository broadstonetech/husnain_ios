//
//  plusBudgetingVC.swift
//  vesgo
//
//  Created by Apple on 13/01/2021.
//  Copyright © 2021 Apple. All rights reserved.
//

import UIKit

class plusBudgetingVC: Parent {
    var headingValues = ["Mortgage/Rent", "Online services", "Education", "Healthcare", "Dining out", "Groceries", "Insurance", "Utilities", "Misc."]
    
    var texttfield = false
    let datePicker = UIDatePicker()
    var startDateInUNIX: Int64!
    var endDateInUNIX: Int64!
    
    var budgetType = "new"
    
    var amountTextField: UITextField!
    var budgetingCategories: BudgetingCategoriesClass!
    
    var isFirstTime = true
    
    @IBOutlet weak var tableview_plus: UITableView!
    @IBOutlet weak var end_date_textField: UITextField!
    @IBOutlet weak var start_date_textfield: UITextField!
    @IBOutlet weak var total_amount_textfield: UITextField!
    @IBOutlet weak var date_picker: UIDatePicker!
    @IBOutlet weak var picker_UIView: UIView!
    
    @IBOutlet weak var overallBucketView: UIView!
    @IBOutlet weak var needsBucketView: UIView!
    @IBOutlet weak var wantsBucketView: UIView!
    @IBOutlet weak var havesBucketView: UIView!
    @IBOutlet weak var needsPercentLabel: UILabel!
    @IBOutlet weak var wantsPercentLabel: UILabel!
    @IBOutlet weak var havesPercentLabel: UILabel!
    @IBOutlet weak var needsLabel: UILabel!
    @IBOutlet weak var wantsLabel: UILabel!
    @IBOutlet weak var havesLabel: UILabel!
    @IBOutlet weak var needsCardIcon: UIImageView!
    @IBOutlet weak var wantsCardIcon: UIImageView!
    @IBOutlet weak var havesCardIcon: UIImageView!
    
    @IBOutlet weak var amountAndDateView: UIView!
    //@IBOutlet weak var amountAndDateHeight: NSLayoutConstraint!
    @IBOutlet weak var submmitPlanView: UIView!
    @IBOutlet weak var blurView: UIVisualEffectView!
    @IBOutlet weak var amountDateViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var amountDateViewCenterConstraint: NSLayoutConstraint!
    //@IBOutlet weak var amountAndDateTopConstraint: NSLayoutConstraint!
    //@IBOutlet weak var amountAndDateVerticalConstraint: NSLayoutConstraint!
    
    var budgetingPlanData: BudgetingPlanData!
    
    var currentDateInUnix = Int64 ((Date.init().timeIntervalSince1970*1000).rounded())
    
    
    //private var datepicker: UIDatePicker?
    override func viewDidLoad() {
        super.viewDidLoad()
    
        total_amount_textfield.delegate = self
        total_amount_textfield.tag = 0
        
        start_date_textfield.delegate = self
        start_date_textfield.tag = 0
        end_date_textField.delegate = self
        end_date_textField.tag = 0
        
        changeAmountAndDateViews(isViewOpen: false)
        needsCardIcon.isHidden = true
        wantsCardIcon.isHidden = true
        havesCardIcon.isHidden = true
        
        
        initBudgetingPlanData()
        getBudgetingCategoriesAPI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        setNavBarTitile(title: "Spending plan", topItem: "Spending plan")
    }
    
    //
    //    @objc func myTargetFunction(textField: UITextField) {
    //
    //
    //    }
    //
    //    @objc func myTarget(textField: UITextField) {
    //
    //
    //    }
    
    @IBAction func startDateBtnPressed(_ sender: Any) {
        print("myTargetFunction")
        texttfield = false
        picker_UIView.isHidden = false
        
        
    }
    
    @IBAction func endDataBtnPressed(_ sender: Any) {
        print("myTargetFunction")
        texttfield = true
        picker_UIView.isHidden = false
        
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    @IBAction func cancelbuttonpicker_pressed(_ sender: Any) {
        picker_UIView.isHidden = true
        
    }
    @IBAction func doneButtonpicker_pressed(_ sender: Any) {
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateStyle = DateFormatter.Style.short
        
        let strDate = dateFormatter.string(from: date_picker.date)
        //start_date_textfield.text = strDate
        
        if texttfield == false {
            start_date_textfield.text = strDate
            //startDateInUNIX = datePicker.date.millisecondsSince1970
            startDateInUNIX = Int64 ((date_picker.date.timeIntervalSince1970*1000).rounded())
            currentDateInUnix = Int64((Date().timeIntervalSince1970*1000).rounded())
            
        }else{
            end_date_textField.text = strDate
            endDateInUNIX = Int64((date_picker.date.timeIntervalSince1970*1000).rounded())
            
        }
        
        // view.endEditing(true)
        picker_UIView.isHidden = true
        
        if total_amount_textfield.text!.isEmpty || Double(total_amount_textfield.text!) == 0.0{
            changeAmountAndDateViews(isViewOpen: false)
            
        } else{
            
            if startDateInUNIX == nil || startDateInUNIX == 0 ||
                        endDateInUNIX == nil || endDateInUNIX == 0 && isFirstTime {
                changeAmountAndDateViews(isViewOpen: false)
            }else {
                changeAmountAndDateViews(isViewOpen: true)
                
            }
            
            
        }
        
        
    }
    
    
    
    @IBAction func satrdateTextfield_pressed(_ sender: Any) {
        
    }
    
    @IBAction func enddate_textfield(_ sender: Any) {
        
    }
    
    
    @IBAction func submitbutton_pressed(_ sender: Any) {
        
//        let vc = UIStoryboard(name: "Budgeting", bundle: nil).instantiateViewController(withIdentifier: "BudgetingDashboard") as! UIViewController
//        self.navigationController?.pushViewController(vc, animated: true)
        
        
        if isInputsValidated() {
            //tableview_plus.reloadData()
            let amountPlanned = FintechUtils.sharedInstance.parseDouble(strValue: total_amount_textfield.text!)
            let sum = self.budgetingPlanData.needsBucket.bucketAmount + self.budgetingPlanData.wantsBucket.bucketAmount + self.budgetingPlanData.havesBucket.bucketAmount
            
            if sum == amountPlanned {
                //API CALL FOR SET PLAN
                submitBudgetingPlanAPI()
            }else{
                
                self.view.makeToast("Total spending should be equal to planned")

            }
        }
        
        
    }
    
    private func isInputsValidated() -> Bool {
        tableview_plus.reloadData()
        if (total_amount_textfield.text?.isEmpty)! ||  FintechUtils.sharedInstance.parseDouble(strValue: total_amount_textfield.text!) == 0.0{
            self.view.makeToast("Please enter total spending")
            return false
        }
        if (start_date_textfield.text?.isEmpty)!{
            self.view.makeToast("Please enter start date")
            return false
        }
        if (end_date_textField.text?.isEmpty)!{
            self.view.makeToast("Please enter end date")
            return false
        }
        if (startDateInUNIX > endDateInUNIX) {
            let text = "Please set start date before " + FintechUtils.sharedInstance.getDateInUSLocale(timestamp: endDateInUNIX/1000)
            self.view.makeToast(text)
            return false
        }
        
        return true
        
    }
    
    //MARK:- API for Set Budgeting Plan
    func submitBudgetingPlanAPI(){
        
        self.view.activityStartAnimating()
        var url = ApiUrl.SUBMIT_BUDGETING_PLAN
        
        
        let namespace = SPManager.sharedInstance.getCurrentUserNamespace()
        
        var needsBucketData = Array<AnyObject>()
        var wantsBucketData = Array<AnyObject>()
        var havesBucketData = Array<AnyObject>()
        for i in 0..<self.budgetingPlanData.needsBucket.bucketCategories.count {
            let category = [
            "category_name": self.budgetingPlanData.needsBucket.bucketCategories[i].label,
            "amount": self.budgetingPlanData.needsBucket.bucketCategories[i].amount] as [String:AnyObject]
            
            needsBucketData.append(category as AnyObject)
        }
        
        for i in 0..<self.budgetingPlanData.wantsBucket.bucketCategories.count {
            let category = [
            "category_name": self.budgetingPlanData.wantsBucket.bucketCategories[i].label,
            "amount": self.budgetingPlanData.wantsBucket.bucketCategories[i].amount] as [String:AnyObject]
            
            wantsBucketData.append(category as AnyObject)
        }
        
        for i in 0..<self.budgetingPlanData.havesBucket.bucketCategories.count {
            let category = [
            "category_name": self.budgetingPlanData.havesBucket.bucketCategories[i].label,
            "amount": self.budgetingPlanData.havesBucket.bucketCategories[i].amount] as [String:AnyObject]
            
            havesBucketData.append(category as AnyObject)
        }
        
        let amountDetailsParams = [
            "total_amount": self.budgetingPlanData.totalAmount,
            "needs_bucket_amount": self.budgetingPlanData.needsBucket.bucketAmount,
            "wants_bucket_amount": self.budgetingPlanData.wantsBucket.bucketAmount,
            "haves_bucket_amount": self.budgetingPlanData.havesBucket.bucketAmount,
            "needs_bucket_percent": self.budgetingPlanData.needsBucket.bucketPercentage,
            "wants_bucket_percent": self.budgetingPlanData.wantsBucket.bucketPercentage,
            "haves_bucket_percent": self.budgetingPlanData.havesBucket.bucketPercentage
        ] as [String: AnyObject]
        
        let planDivisionParams = [
            "needs_bucket": needsBucketData,
            "wants_bucket": wantsBucketData,
            "haves_bucket": havesBucketData
        ] as [String: AnyObject]
        
        
        
        let dataParams = [
            "start_date": startDateInUNIX,
            "end_date": endDateInUNIX,
            "type": budgetType,
            "amount_details": amountDetailsParams,
            "plan_division": planDivisionParams] as [String: AnyObject]
        
        let params = ["namespace": namespace, "data": dataParams] as [String: AnyObject]
        
        print(params)
        APIManager.sharedInstance.postRequest(serviceName: url, sendData: params, success: { (response) in
            print(response)
            
            if APIManager.sharedInstance.isSuccess {
                    
                    DispatchQueue.main.async { [self] in
                        
                        if startDateInUNIX > currentDateInUnix {
                            let okAction = UIAlertAction(title: getLocalizedString(key: "ok", comment: ""), style: .default) { (action) in
                                self.navigationController?.viewControllers.removeAll(where: {
                                    (vc) in
                                    vc.isKind(of: BudgetingDashboard.self)
                                })
                                self.PopVC()
                            }
                            let string = String(format: getLocalizedString(key: "you_create_a_post_date_plan"), FintechUtils.sharedInstance.getDateInUSLocale(timestamp: startDateInUNIX/1000))
                            self.showAlert(string, action: okAction)
                        }else {
                            let vc = UIStoryboard(name: "Budgeting", bundle: nil).instantiateViewController(withIdentifier: "BudgetingDashboard") as! UIViewController
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                        
                    }
                    
            } else {
                
            }
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
        }, failure: { (data) in
            DispatchQueue.main.async {
                print("IN FALIURE")
                self.view.activityStopAnimating()
            }
            if APIManager.sharedInstance.error400 {
                DispatchQueue.main.async {
                    self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
                }
            }
            if(APIManager.sharedInstance.error401)
            {
                DispatchQueue.main.async {
                    self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
                }
            }else{
                DispatchQueue.main.async {
                    self.showAlert(ConstantStrings.Error_msg_generic)
                }
            }
        })
    }
    
    
    // MARK:- Api for Get Categories
    func  getBudgetingCategoriesAPI(){
        self.view.activityStartAnimating()
        let url = ApiUrl.GET_BUDGETING_CATEGORIES
        let namespace = SPManager.sharedInstance.getCurrentUserNamespace()
        let params = ["namespace": namespace] as [String: AnyObject]
        APIManager.sharedInstance.postRequest(serviceName: url, sendData: params, success: {
            (response) in
            
            self.budgetingCategories = self.fetchCategoriesApiResponse(response: response)
            if self.budgetingCategories != nil {
                self.populateBudgetPlanData()
            }
            
            DispatchQueue.main.async { [self] in
                self.tableview_plus.reloadData()
            }
            
            DispatchQueue.main.async { [self] in
                view.activityStopAnimating()
            }
            
            
        }, failure: {
            (err) in
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
            if APIManager.sharedInstance.error400 {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
            }
            if(APIManager.sharedInstance.error401)
            {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
            }else{
                self.showAlert(ConstantStrings.Error_msg_generic)
            }
        })
    }
    
    private func fetchCategoriesApiResponse(response: [String: AnyObject]) -> BudgetingCategoriesClass{
        
        let success = response["success"] as! Bool
        let message = response["message"] as! String
        
        let data = response["data"] as! [String: AnyObject]
        let needsBucket = data["needs_bucket"] as! [String]
        let wantsBucket = data["wants_bucket"] as! [String]
        let havesBucket = data["haves_bucket"] as! [String]
        
        let categoriesData = BudgetingCategoriesData(needsBucketCategories: needsBucket, wantsBucketCategories: wantsBucket, havesBucketCategories: havesBucket)
        
        return BudgetingCategoriesClass(success: success, message: message, data: categoriesData)
        
    }
    
    private func populateBudgetPlanData() {
        var wantsBucketCategories = [BucketCategoryData]()
        var havesBucketCategories = [BucketCategoryData]()
        var needsBucketCategories = [BucketCategoryData]()
        for i in 0..<budgetingCategories.data.wantsBucketCategories.count {
            let label = budgetingCategories.data.wantsBucketCategories[i]
            wantsBucketCategories.append(BucketCategoryData(index: i, amount: 0.0, label: label))
        }
        
        for i in 0..<budgetingCategories.data.havesBucketCategories.count {
            let label = budgetingCategories.data.havesBucketCategories[i]
            havesBucketCategories.append(BucketCategoryData(index: i, amount: 0.0, label: label))
        }
        
        for i in 0..<budgetingCategories.data.needsBucketCategories.count {
            let label = budgetingCategories.data.needsBucketCategories[i]
            needsBucketCategories.append(BucketCategoryData(index: i, amount: 0.0, label: label))
        }
        
        let wantsBucket = BudgetingPlanBucketData(bucketAmount: 0.0, bucketPercentage: 0.0, bucketCategories: wantsBucketCategories)
        let havesBucket = BudgetingPlanBucketData(bucketAmount: 0.0, bucketPercentage: 0.0, bucketCategories: havesBucketCategories)
        let needsBucket = BudgetingPlanBucketData(bucketAmount: 0.0, bucketPercentage: 0.0, bucketCategories: needsBucketCategories)
        
        let budgetingPlanData = BudgetingPlanData(totalAmount: 0.0, startDate: 0, endDate: 0, needsBucket: needsBucket, wantsBucket: wantsBucket, havesBucket: havesBucket)
        
        self.budgetingPlanData = budgetingPlanData
    }
    
    
}
extension plusBudgetingVC: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if budgetingCategories == nil {
            return 0
        }
        return    1 + budgetingPlanData.needsBucket.bucketCategories.count
                + 1 + budgetingPlanData.wantsBucket.bucketCategories.count
                + 1 + budgetingPlanData.havesBucket.bucketCategories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row < 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "BucketHeadingCell") as! BucketHeadingCell
//            let headingLabel = "Needs (" + FintechUtils.sharedInstance.doubleToPercentageOutput(doubleVal: self.budgetingPlanData.needsBucket.bucketPercentage) + "%)"
            cell.bucketHeadingLabel.text = getLocalizedString(key: "_needs")
            cell.bucketAmountLabel.text = "$" + FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: self.budgetingPlanData.needsBucket.bucketAmount)
            cell.headingIcon.image = FintechUtils.sharedInstance.getBucketIcon(bucketType: "needs")
            cell.tooltipBtn.addToolTip(description: getLocalizedString(key: "needs_desc"), gesture: .singleTap, isEnabled: true)
            cell.parentView.backgroundColor = UIColor(named: "NeedsBucketColor")!
            
            return cell
        }
        else if indexPath.row < 1 + budgetingPlanData.needsBucket.bucketCategories.count {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! addDateBudgetingTableViewCell
            
            let index = indexPath.row - 1
            
            let headingLabel = budgetingPlanData.needsBucket.bucketCategories[index].label
            cell.amount_textfield.tag = 100 + index
            cell.amount_textfield.delegate = self
            cell.amount_textfield.text = FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: budgetingPlanData.needsBucket.bucketCategories[index].amount)
            cell.date_lbl.text = headingLabel
            cell.imageBgView.backgroundColor = FintechUtils.sharedInstance.getColoredBackgroundForNeedsCategories(rowPosition: index)
            cell.imgView.image = FintechUtils.sharedInstance.getCaetgoryImageForBudgeting(value: headingLabel.lowercased())
            cell.imgView.image?.withRenderingMode(.alwaysTemplate)
            cell.imgView.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)

            return cell
        }
        else if indexPath.row < 1 + budgetingPlanData.needsBucket.bucketCategories.count + 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "BucketHeadingCell") as! BucketHeadingCell
//            let headingLabel = "Wants (" + FintechUtils.sharedInstance.doubleToPercentageOutput(doubleVal: self.budgetingPlanData.wantsBucket.bucketPercentage) + "%)"
            cell.bucketHeadingLabel.text = getLocalizedString(key: "_wants")
            cell.bucketAmountLabel.text = "$" + FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: self.budgetingPlanData.wantsBucket.bucketAmount)
            cell.headingIcon.image = FintechUtils.sharedInstance.getBucketIcon(bucketType: "wants")
            cell.tooltipBtn.addToolTip(description: getLocalizedString(key: "wants_desc"), gesture: .singleTap, isEnabled: true)
            cell.parentView.backgroundColor = UIColor(named: "WantsBucketColor")!
            
            return cell
        }
        else if indexPath.row < 1 + budgetingPlanData.needsBucket.bucketCategories.count + 1 + budgetingPlanData.wantsBucket.bucketCategories.count {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! addDateBudgetingTableViewCell
            
            let index = indexPath.row - 1 - budgetingPlanData.needsBucket.bucketCategories.count - 1
            
            let headingLabel = budgetingPlanData.wantsBucket.bucketCategories[index].label
            cell.amount_textfield.tag = 200 + index
            cell.amount_textfield.delegate = self
            cell.amount_textfield.text = FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: budgetingPlanData.wantsBucket.bucketCategories[index].amount)
            cell.date_lbl.text = headingLabel
            cell.imageBgView.backgroundColor = FintechUtils.sharedInstance.getColoredBackgroundForWantsCategories(rowPosition: index)
            cell.imgView.image = FintechUtils.sharedInstance.getCaetgoryImageForBudgeting(value: headingLabel.lowercased())
            cell.imgView.image?.withRenderingMode(.alwaysTemplate)
            cell.imgView.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)

            return cell
        }
        else if indexPath.row < 1 + budgetingPlanData.needsBucket.bucketCategories.count + 1 + budgetingPlanData.wantsBucket.bucketCategories.count + 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "BucketHeadingCell") as! BucketHeadingCell
//            let headingLabel = "Haves (" + FintechUtils.sharedInstance.doubleToPercentageOutput(doubleVal: self.budgetingPlanData.havesBucket.bucketPercentage) + "%)"
            cell.bucketHeadingLabel.text = getLocalizedString(key: "_haves")
            cell.bucketAmountLabel.text = "$" + FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: self.budgetingPlanData.havesBucket.bucketAmount)
            cell.headingIcon.image = FintechUtils.sharedInstance.getBucketIcon(bucketType: "haves")
            cell.tooltipBtn.addToolTip(description: getLocalizedString(key: "haves_desc"), gesture: .singleTap, isEnabled: true)
            cell.parentView.backgroundColor = UIColor(named: "HavesBucketColor")!
            
            return cell
        }
        else if indexPath.row < 1 + budgetingPlanData.needsBucket.bucketCategories.count + 1 + budgetingPlanData.wantsBucket.bucketCategories.count + 1 +  budgetingPlanData.havesBucket.bucketCategories.count {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! addDateBudgetingTableViewCell
            let index = indexPath.row - 1 - budgetingPlanData.needsBucket.bucketCategories.count - 1 - budgetingPlanData.wantsBucket.bucketCategories.count - 1
            
            
            let headingLabel = budgetingPlanData.havesBucket.bucketCategories[index].label
            cell.amount_textfield.tag = 300 + index
            cell.amount_textfield.delegate = self
            cell.amount_textfield.text = FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: budgetingPlanData.havesBucket.bucketCategories[index].amount)
            cell.date_lbl.text = headingLabel
            cell.imageBgView.backgroundColor = FintechUtils.sharedInstance.getColoredBackgroundForHavesCategories(rowPosition: index)
            cell.imgView.image = FintechUtils.sharedInstance.getCaetgoryImageForBudgeting(value: headingLabel.lowercased())
            cell.imgView.image?.withRenderingMode(.alwaysTemplate)
            cell.imgView.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            

            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        print(indexPath.row)
        
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        let text = FintechUtils.sharedInstance.parseDouble(strValue: textField.text ?? "")
        if text != 0.0 {
            textField.text = String(text)
            //self.tableview_plus.reloadData()
        }
        
        if textField != total_amount_textfield {
            if FintechUtils.sharedInstance.parseDouble(strValue: textField.text!) == 0.0 {
                print("Textfield is empty")
                //textField.clearsOnBeginEditing = true
                //self.tableview_plus.reloadData()
            }
        }
        self.amountTextField = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        print(textField.text)
        
        if textField.tag == 0 {
            if textField.text!.isEmpty || Double(textField.text!) == 0.0{
                
                changeAmountAndDateViews(isViewOpen: false)
                self.budgetingPlanData.totalAmount = 0.0
                self.total_amount_textfield.text = FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: 0.0)
            } else{
                
                if startDateInUNIX == nil || startDateInUNIX == 0 ||
                            endDateInUNIX == nil || endDateInUNIX == 0 && isFirstTime {
                    changeAmountAndDateViews(isViewOpen: false)
                }else {
                    changeAmountAndDateViews(isViewOpen: true)
                    
                }
                self.budgetingPlanData.totalAmount = Double(textField.text!) ?? 0.0
                self.total_amount_textfield.text = FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: Double(textField.text!) ?? 0.0)
                
                
            }
            return
        }
        
        var amountEntered = 0.0
        self.amountTextField.text = textField.text!
        if textField.text!.isEmpty || Double(textField.text!) == 0.0 {
            amountEntered = BudgetingConstants.DEFAULT_AMOUNT_FOR_BUDGETING
        }else{
            amountEntered = FintechUtils.sharedInstance.parseDouble(strValue: textField.text!)
        }
        var index = 0
        let tag = textField.tag
        if tag >= 100 && tag < 200 {
            index = tag - 100
            
            let categoryData = BucketCategoryData(index: index, amount: amountEntered, label: self.budgetingPlanData.needsBucket.bucketCategories[index].label)
            
            self.budgetingPlanData.needsBucket.bucketCategories[index] = categoryData
            calculateTotalAmountForNeeds()
            
            DispatchQueue.main.async {
//                var indexPaths = [IndexPath]()
//                indexPaths.append(IndexPath.init(row: index, section: 0))
//                self.tableview_plus.reloadRows(at: indexPaths, with: .none)
                self.tableview_plus.reloadData()
            }
            
        }else if tag >= 200 && tag < 300 {
            index = tag - 200
            let categoryData = BucketCategoryData(index: index, amount: amountEntered, label: self.budgetingPlanData.wantsBucket.bucketCategories[index].label)
            
            self.budgetingPlanData.wantsBucket.bucketCategories[index] = categoryData
            calculateTotalAmountForWants()
            
            DispatchQueue.main.async {
                self.tableview_plus.reloadData()
            }
        }else {
            index = tag - 300
            let categoryData = BucketCategoryData(index: index, amount: amountEntered, label: self.budgetingPlanData.havesBucket.bucketCategories[index].label)
            
            self.budgetingPlanData.havesBucket.bucketCategories[index] = categoryData
            calculateTotalAmountForHaves()
            
            DispatchQueue.main.async {
                self.tableview_plus.reloadData()
            }
        }
        
        
        //tableview_plus.reloadData()
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        if textField.keyboardType == .decimalPad {
            let regex = try! NSRegularExpression(pattern: "^(^[0-9]{0,7})*([.,][0-9]{0,2})?$", options: .caseInsensitive)

            if let newText = (textField.text as NSString?)?.replacingCharacters(in: range, with: string) {
                return regex.firstMatch(in: newText, options: [], range: NSRange(location: 0, length: newText.count)) != nil

            } else {
                return false
            }
        }else {
            return false
        }


    }
    
    private func changeAmountAndDateViews(isViewOpen: Bool) {
        if isViewOpen {
            isFirstTime = false
//            tableview_plus.isHidden = false
//            overallBucketView.isHidden = false
//            submmitPlanView.isHidden = false
            blurView.isHidden = true
            amountDateViewTopConstraint.isActive = true
            amountDateViewCenterConstraint.isActive = false
            
        }else {
//            tableview_plus.isHidden = true
//            overallBucketView.isHidden = true
//            submmitPlanView.isHidden = true
            blurView.isHidden = false
            amountDateViewTopConstraint.isActive = false
            amountDateViewCenterConstraint.isActive = true
        }
    }
    
    
}
//MARK:- New Data Model for Budgeting Submit Plan
private extension plusBudgetingVC {
    func initBudgetingPlanData() {
        budgetingPlanData = BudgetingPlanData(totalAmount: 0.0, startDate: 0, endDate: 0, needsBucket: BudgetingPlanBucketData(), wantsBucket: BudgetingPlanBucketData(), havesBucket: BudgetingPlanBucketData())
        
        
    }
    
    func calculateTotalAmountForNeeds(){
        var totalAmount = 0.0
        for i in 0..<self.budgetingPlanData.needsBucket.bucketCategories.count {
            totalAmount += self.budgetingPlanData.needsBucket.bucketCategories[i].amount
        }
        
        self.budgetingPlanData.needsBucket.bucketAmount = totalAmount
        let percentage = (totalAmount / self.budgetingPlanData.totalAmount) * 100.0
        self.budgetingPlanData.needsBucket.bucketPercentage = percentage
        
        self.needsPercentLabel.text = FintechUtils.sharedInstance.doubleToPercentageOutput(doubleVal: percentage) + " out of 50%"
        
        if percentage > 0 {
            needsCardIcon.isHidden = false
            if percentage > 50 {
                needsCardIcon.image = UIImage(named: "AlertIcon")
            }else {
                needsCardIcon.image = UIImage(named: "GreenTickIcon")
            }
        }else {
            needsCardIcon.isHidden = true
        }
    }
    func calculateTotalAmountForWants(){
        var totalAmount = 0.0
        for i in 0..<self.budgetingPlanData.wantsBucket.bucketCategories.count {
            totalAmount += self.budgetingPlanData.wantsBucket.bucketCategories[i].amount
        }
        
        self.budgetingPlanData.wantsBucket.bucketAmount = totalAmount
        let percentage = (totalAmount / self.budgetingPlanData.totalAmount) * 100.0
        self.budgetingPlanData.wantsBucket.bucketPercentage = percentage
        
        self.wantsPercentLabel.text = FintechUtils.sharedInstance.doubleToPercentageOutput(doubleVal: percentage) + " out of 30%"
        
        if percentage > 0 {
            wantsCardIcon.isHidden = false
            if percentage > 30 {
                wantsCardIcon.image = UIImage(named: "AlertIcon")
            }else {
                wantsCardIcon.image = UIImage(named: "GreenTickIcon")
            }
        }else {
            wantsCardIcon.isHidden = true
        }
    }
    func calculateTotalAmountForHaves(){
        var totalAmount = 0.0
        for i in 0..<self.budgetingPlanData.havesBucket.bucketCategories.count {
            totalAmount += self.budgetingPlanData.havesBucket.bucketCategories[i].amount
        }
        
        self.budgetingPlanData.havesBucket.bucketAmount = totalAmount
        let percentage = (totalAmount / self.budgetingPlanData.totalAmount) * 100.0
        self.budgetingPlanData.havesBucket.bucketPercentage = percentage
        
        self.havesPercentLabel.text = FintechUtils.sharedInstance.doubleToPercentageOutput(doubleVal: percentage) + " out of 20%"
        
        if percentage > 0 {
            havesCardIcon.isHidden = false
            if percentage > 20 {
                havesCardIcon.image = UIImage(named: "AlertIcon")
            }else {
                havesCardIcon.image = UIImage(named: "GreenTickIcon")
            }
        }else {
            havesCardIcon.isHidden = true
        }
    }
}
