////
////  PortfolioVC.swift
////  Fintech
////
////  Created by MacBook-Mubashar on 23/07/2019.
////  Copyright © 2019 Broadstone Technologies. All rights reserved.
////
//
//import UIKit
//import Charts
//import SwiftyToolTip
//
//class PortfolioVC: UIViewController,  UITableViewDataSource, UITableViewDelegate {
//    @IBOutlet weak var info_btn: UIButton!
//    @IBOutlet weak var linechart: LineChart!//LineChartView!
//    @IBOutlet weak var companyInfo_btn : UIButton!
//    @IBOutlet weak var tableview_summary: UITableView!
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//
//        info_btn.addToolTip(description: "information about companies you own listed here. Tap to view more details.")
//
//        self.tableview_summary.delegate = self
//        self.tableview_summary.dataSource = self
//        self.tableview_summary.estimatedRowHeight = 300
//        self.tableview_summary.rowHeight = UITableView.automaticDimension
//
//        let dataEntries = generateRandomEntries()
//
//        linechart.dataEntries = dataEntries
//        linechart.isCurved = false
//
//        // Do any additional setup after loading the view.
//    }
//
//    private func generateRandomEntries() -> [PointEntry] {
//        var result: [PointEntry] = []
//        for i in 0..<100 {
//            let value = Int(arc4random() % 500)
//
//            let formatter = DateFormatter()
//            formatter.dateFormat = "d MMM"
//            var date = Date()
//            date.addTimeInterval(TimeInterval(24*60*60*i))
//
//            result.append(PointEntry(value: value, label: formatter.string(from: date)))
//        }
//        return result
//    }
//
//    @IBAction func infoBtn_pressed(_ sender: Any) {
//        info_btn.showToolTip()
//    }
//
//
//    //     MARK: - Chart data actions
//
//    @IBAction func loadDayData(_ sender: Any) {
//        print("day data pressed")
//        loadChart()
//
//        setAnalyticsAction(ScreenName: "PortfolioVC", method: "DayData")
//
//    }
//    @IBAction func loadWeekData(_ sender: Any) {
//        loadChart()
//        setAnalyticsAction(ScreenName: "PortfolioVC", method: "WeekData")
//    }
//    @IBAction func loadMonthData(_ sender: Any) {
//        loadChart()
//        setAnalyticsAction(ScreenName: "PortfolioVC", method: "MonthData")
//    }
//    @IBAction func loadYearData(_ sender: Any) {
//        loadChart()
//        setAnalyticsAction(ScreenName: "PortfolioVC", method: "YearData")
//    }
//    @IBAction func loadAllData(_ sender: Any) {
//        loadChart()
//        setAnalyticsAction(ScreenName: "PortfolioVC", method: "AllData")
//    }
//
//
//    func loadChart() {
//        let dataEntries = generateRandomEntries()
//
//        linechart.dataEntries = dataEntries
//        linechart.isCurved = false
//    }
//    // MARK: - TableView Delegates
//
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//
//        return 10
//
//    }
//
//    func numberOfSections(in tableView: UITableView) -> Int {
//
//        return 1
//    }
//
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//
//        return UITableView.automaticDimension
//    }
//
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        print("selected index == \(indexPath.row)")
//        let modelsCell =  self.tableview_summary.dequeueReusableCell(withIdentifier: "PortfolioSummaryCell") as! PortfolioSummaryCell
//
//        //        tableView.reloadData()
//    }
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell =  self.tableview_summary.dequeueReusableCell(withIdentifier: "PortfolioSummaryCell") as! PortfolioSummaryCell
//
//        cell.label1.text = "Finance"
//      //  cell.label2.text = "SCHW, CACC "
//        cell.label3.text = "1.133%"
//
//        return cell
//    }
//
//
//
//    /*
//     // MARK: - Navigation
//
//     // In a storyboard-based application, you will often want to do a little preparation before navigation
//     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//     // Get the new view controller using segue.destination.
//     // Pass the selected object to the new view controller.
//     }
//     */
//
//}
