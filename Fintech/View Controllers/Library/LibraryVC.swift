//
//  LibraryVC.swift
//  Fintech
//
//  Created by MacBook-Mubashar on 31/07/2019.
//  Copyright © 2019 Broadstone Technologies. All rights reserved.
//

import UIKit
import Foundation
import AVFoundation
import Floaty

class LibraryVC: Parent {
    //MARK: -IB Outlets
    //Mark : - views
    @IBOutlet weak var deepDives_btn: UIButton!
    @IBOutlet weak var keyDocs_btn: UIButton!
    @IBOutlet weak var media_btn: UIButton!
    @IBOutlet weak var deepDivesTableview: UITableView!
    @IBOutlet weak var meditableView: UITableView!
    @IBOutlet weak var porttfolio_btn: UIButton!
    @IBOutlet weak var keyDocsTableview: UITableView!
    @IBOutlet weak var topbarSeegmentControl: UISegmentedControl!
    @IBOutlet weak var segment1_view: UIView!
    @IBOutlet weak var segment2_view: UIView!
    @IBOutlet weak var segment3_view: UIView!
    
    @IBOutlet weak var floaty_btn: Floaty!
    
    // MArk : - Variable
    let MAIN_DATA : String = "main_data"
    let KEY_DATA : String = "key_data"
    let MEDIA_DATA : String = "media_data"
    var SELECTED_DATA_TYPE : String = ""
    private var dis = String()
    
    //  MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
       
        
        
        
        self.tabBarController?.tabBar.isHidden = true

       self.floatybutton()
//        if SPManager.sharedInstance.saveUserLoginStatus() {
//        }else{
//            let floatya = Floaty()
//            floatya.buttonColor = #colorLiteral(red: 0, green: 0.5628422499, blue: 0.3188166618, alpha: 1)
//            floatya.buttonImage = UIImage(named: "baseline_person_add_black")
//            self.view.addSubview(floatya)
//            //    button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
//            //self.view.addSubview(button)
//        }
//
        deepDives_btn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        // Do any additional setup after loading the view.
        self.initTableViews()
    }
    func floatybutton() {
        
        if SPManager.sharedInstance.saveUserLoginStatus() {
            floaty_btn.isHidden = true
        }else{
            
            //  let floatya = Floaty()
            // floatya.buttonColor = #colorLiteral(red: 0, green: 0.5628422499, blue: 0.3188166618, alpha: 1)
            floaty_btn.buttonColor = #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1)
            floaty_btn.itemButtonColor = #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1)
            floaty_btn.addItem("Model Portfolios",icon: UIImage(named: "services-portfolio")!, handler: { item in
                if let tabbedbar = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "nav_bar_sampleportfolio") as? UINavigationController {
                    
                    UIApplication.shared.keyWindow?.rootViewController = tabbedbar
                }
            })
            
            floaty_btn.addItem("Account Setup",icon: UIImage(named: "baseline_person_add_black")!, handler: { item in
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "Main_login_page") as UIViewController
                UIApplication.shared.keyWindow?.rootViewController = vc
            })
            DispatchQueue.main.async {
                self.view.addSubview(self.floaty_btn)
                
            }
            //    button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
            //self.view.addSubview(button)
            
        }
    }
    //MARK: - IB Acction
    @IBAction func portfolio_pressed(_ sender: Any) {
        self.tabBarController!.selectedIndex = 0
        
        setAnalyticsAction(ScreenName: "LibraryVC", method: "portfolioPressed")

    }
    @IBAction func mediaBtn_pressed(_ sender: Any) {
        segment1_view.isHidden = true
        segment2_view.isHidden = true
        segment3_view.isHidden = false
        
        deepDives_btn.titleLabel?.fontSize = 15
        keyDocs_btn.titleLabel?.fontSize = 15
        media_btn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        setAnalyticsAction(ScreenName: "LibraryVC", method: "mediaBtnPressed")
        
    }
    @IBAction func keyDocsBtn_pressed(_ sender: Any) {
        
        segment1_view.isHidden = true
        segment2_view.isHidden = false
        segment3_view.isHidden = true
        
        
        deepDives_btn.titleLabel?.fontSize = 15
        media_btn.titleLabel?.fontSize = 15
        keyDocs_btn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        keyDocs_btn.backgroundColor = UIColor.clear
        setAnalyticsAction(ScreenName: "LibraryVC", method: "keyDocsPressed")
        
    }
    @IBAction func Deepdivies_btn(_ sender: Any) {
        segment1_view.isHidden = false
        segment2_view.isHidden = true
        segment3_view.isHidden = true
        // deepDives_btn.backgroundColor = UIColor.blue
        media_btn.titleLabel?.fontSize = 15
        keyDocs_btn.titleLabel?.fontSize = 15
        deepDives_btn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        setAnalyticsAction(ScreenName: "LibraryVC", method: "deepDives")
    }
    
    //MARK: - Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        
        SELECTED_DATA_TYPE = MAIN_DATA
        if NetworkReachability.isConnectedToNetwork() {
            self.getLibData()
        } else {
            self.view.makeToast(ConstantStrings.NO_INTERNET_CONNECTION_FOUND)
        }
        
    }
    override func viewDidAppear(_ animated: Bool) {
        
        localization()
    }
    
    func localization(){
        setNavBarTitile(title:getLocalizedString(key: "Library", comment: ""), topItem: getLocalizedString(key: "Library", comment: ""))
        deepDives_btn.setTitle(getLocalizedString(key: "DEEP DIVES", comment: ""), for: .normal)
        keyDocs_btn.setTitle(getLocalizedString(key: "KEY DOCS", comment: ""), for: .normal)
        media_btn.setTitle(getLocalizedString(key: "MEDIA", comment: ""), for: .normal)
        
    }
    //  MARK: - Class Functions
    func initTableViews() {
        
        self.keyDocsTableview.delegate = self
        self.keyDocsTableview.dataSource = self
        self.keyDocsTableview.rowHeight = UITableView.automaticDimension
        self.keyDocsTableview.estimatedRowHeight = 200
        
        self.deepDivesTableview.delegate = self
        self.deepDivesTableview.dataSource = self
        self.deepDivesTableview.rowHeight = UITableView.automaticDimension
        self.deepDivesTableview.estimatedRowHeight = 200
        
        self.meditableView.delegate = self
        self.meditableView.dataSource = self
        self.meditableView.rowHeight = UITableView.automaticDimension
        self.meditableView.estimatedRowHeight = 200
        
    }
    // MARK: - Segment Control Actions
    @IBAction func segmentIndexChanged(_ sender: Any) {
        switch topbarSeegmentControl.selectedSegmentIndex
        {
        case 0:
            SELECTED_DATA_TYPE = MAIN_DATA
            deepDivesTableview.reloadData()
            segment1_view.isHidden = false
            segment2_view.isHidden = true
            segment3_view.isHidden = true
        case 1:
            SELECTED_DATA_TYPE = KEY_DATA
            keyDocsTableview.reloadData()
            segment1_view.isHidden = true
            segment2_view.isHidden = false
            segment3_view.isHidden = true
        case 2:
            SELECTED_DATA_TYPE = MEDIA_DATA
            meditableView.reloadData()
            segment1_view.isHidden = true
            segment2_view.isHidden = true
            segment3_view.isHidden = false
        default:
            print("default case")
        }
    }
    // MARK: - API calls
    func getLibData() {
        
        self.view.activityStartAnimating()
        let url = ApiUrl.GET_LIBRARY_DATA_URL
        let namespace = SPManager.sharedInstance.getCurrentUserNamespace()
        let data:Dictionary<String,AnyObject> = [:]
        
        let params:Dictionary<String,AnyObject> = ["namespace" : namespace as AnyObject, "data" : data as AnyObject]
        print(data)
        APIManager.sharedInstance.postRequest(serviceName: url, sendData: params, success: { (response) in
            print(response)
            
            if APIManager.sharedInstance.isSuccess {
                let res = response["message"] as AnyObject
                DispatchQueue.main.async {
                    
            
                    
                    self.fetchMetaData(res: res as! [String : AnyObject], table_view: self.deepDivesTableview)
                }
                
            }else{
                if response["message"] != nil {
                    self.showAlert(response["message"] as! String)
                } else {
                    self.showAlert(ConstantStrings.NO_RESULT_FOUND_ALERT_TEXT)
                }
            }
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
        }, failure: { (data) in
            print(data)
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
            if APIManager.sharedInstance.error400 {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
            }
            if(APIManager.sharedInstance.error401)
            {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
            }else{
                
            }
        })
        
    }
    // MARK: - fetchData
    func fetchMetaData(res : [String : AnyObject], table_view : UITableView) {
        ArrayLists.sharedInstance.LibraryMainDataList.removeAll()
        ArrayLists.sharedInstance.LibraryDocsDataList.removeAll()
        ArrayLists.sharedInstance.LibraryMediaDataList.removeAll()
        
        let mainData = res["main_data"] as! [[String : AnyObject]]
        let docsData = res["doc_data"] as! [[String : AnyObject]]
        let mediaData = res["media_data"] as! [[String : AnyObject]]
        
        DispatchQueue.main.async {
            
            ArrayLists.sharedInstance.LibraryMainDataList = self.parseJsonList(data: mainData)
            ArrayLists.sharedInstance.LibraryDocsDataList = self.parseJsonList(data: docsData)
            ArrayLists.sharedInstance.LibraryMediaDataList = self.parseJsonList(data: mediaData)
        }
        
        DispatchQueue.main.async {
            self.keyDocsTableview.reloadData()
        }
        DispatchQueue.main.async {
            self.deepDivesTableview.reloadData()
        }
        
        DispatchQueue.main.async {
            self.meditableView.reloadData()
        }
    }
    
    func parseJsonList(data : [[String : AnyObject]]) -> [LibraryDataModel] {
        
        var list : [LibraryDataModel] = [LibraryDataModel]()
        for jsonData in data {
            
            let model = LibraryDataModel()
            var SELECTED_DATA_TYPE : String = ""

            let mainHeading = jsonData["main_heading"] as! String
            let subHeading = jsonData["subheading"] as! String
            //parse model scaling json
            let description = jsonData["description"] as! String
            let iconUrl = jsonData["icon_url"] as! String
            let progressValue = jsonData["progress_value"] as! Double
            let dataType = jsonData["type"] as! String
            let timestamp = jsonData["eve_sec"] as! Int
            
            if jsonData["additional_info"] as? [String : AnyObject] != nil {
                let additionalInfo = jsonData["additional_info"] as! [String : AnyObject]
                //parse additional info
                //            if additionalInfo != nil {
                let infoType = additionalInfo["type"] as! String
                
                model.setInfoType(type: infoType)
                if infoType == "text" {
                    let infoValue = additionalInfo["value"] as! [String : AnyObject]
                    let infoHeading = infoValue["info_heading"] as! String
                    let infoSubHeading = infoValue["info_sub_heading"] as! String
                    let infoDescription = infoValue["info_description"] as! String
                    let infoDisclaimerText = infoValue["info_disclaimer_text"] as! String
                    let infoDisclaimerLink = infoValue["info_disclaimer_link"] as! String
                    let infoDisclaimerLinkType = infoValue["info_disclaimer_link_type"] as! String
                    
                    model.setInfoHeading(text: infoHeading)
                    model.setInfoSubHeading(text: infoSubHeading)
                    model.setInfoDescription(text: infoDescription)
                    model.setInfoDisclaimerText(text: infoDisclaimerText)
                    model.setInfoDisclaimerLink(text: infoDisclaimerLink)
                    model.setInfoDisclaimerLinkType(text: infoDisclaimerLinkType)
                } else {
                    let infoValue = additionalInfo["value"] as! String
                    model.setInfoValue(value: infoValue)
                }
            } else {
                model.setInfoType(type: "none")
            }
            
            model.setMainHeadingText(text: mainHeading)
            model.setSubHeadingText(text: subHeading)
            model.setDescriptionText(text: description)
            model.setIconImage(text: iconUrl)
            model.setProgressValue(text: progressValue)
            model.setDataType(text: dataType)
            
            list.append(model)
        }
        
        return list
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    @objc func viewDiscalimerTapped() {
        viewPdfFile(urlString: dis,urlType: "")
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    
}

// MARK: - Ext. TableView Delegates
extension LibraryVC : UITableViewDelegate, UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if SELECTED_DATA_TYPE == MAIN_DATA {
            
            return ArrayLists.sharedInstance.LibraryMainDataList.count
        } else if SELECTED_DATA_TYPE == KEY_DATA {
            return ArrayLists.sharedInstance.LibraryDocsDataList.count
        } else if SELECTED_DATA_TYPE == MEDIA_DATA {
            return ArrayLists.sharedInstance.LibraryMediaDataList.count
        }
        return 0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if tableView == meditableView {
            return UITableView.automaticDimension
        } else if tableView == keyDocsTableview {
            return UITableView.automaticDimension
        } else if tableView == deepDivesTableview {
            return UITableView.automaticDimension
        }
        return 0
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("selected index == \(indexPath.row)")
        
        var infoType : String = ""
        var infoValue : String = ""
        if tableView == deepDivesTableview {
            let item = ArrayLists.sharedInstance.LibraryMainDataList[indexPath.row]
            infoType = item.getInfoType()
            infoValue = item.getInfoValue()
            dis = item.getInfoDisclaimerLink()
            self.viewDiscalimerTapped()
        } else if tableView == keyDocsTableview {
            let item = ArrayLists.sharedInstance.LibraryDocsDataList[indexPath.row]
            infoType = item.getInfoType()
            infoValue = item.getInfoValue()
        } else {
            let item = ArrayLists.sharedInstance.LibraryMediaDataList[indexPath.row]
            infoType = item.getInfoType()
            infoValue = item.getInfoValue()
        }
        
        if infoType == "text" {
            
            
        }else if infoType == "video" {
            viewPdfFile(urlString: infoValue, urlType: infoType)
        } else if infoType != "none" {
            viewPdfFile(urlString: infoValue, urlType: infoType)
        }
        
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        
       
        
        if tableView == deepDivesTableview {
             let deepDiveCell =  self.deepDivesTableview.dequeueReusableCell(withIdentifier: "DeepDivesCell") as! DeepDivesCell
            let item = ArrayLists.sharedInstance.LibraryMainDataList[indexPath.row]
            deepDiveCell.heading_lbl.text = item.getMainHeadingText()
            deepDiveCell.description_lbl.text = item.getDescriptionText()
            deepDiveCell.Image_deepDives_img.layer.cornerRadius = 10
            deepDiveCell.Image_deepDives_img.clipsToBounds = true
            return deepDiveCell
        } else if tableView == keyDocsTableview {
            let keyDocsCell =  self.keyDocsTableview.dequeueReusableCell(withIdentifier: "KeyDocsCell") as! DeepDivesCell
            let item = ArrayLists.sharedInstance.LibraryDocsDataList[indexPath.row]
            keyDocsCell.heading_lbl.text = item.getMainHeadingText()
            keyDocsCell.description_lbl.text = item.getDescriptionText()
            return keyDocsCell
        } else {
             let mediaCell =  self.meditableView.dequeueReusableCell(withIdentifier: "MediaCell") as! DeepDivesCell
            let item = ArrayLists.sharedInstance.LibraryMediaDataList[indexPath.row]
            mediaCell.heading_lbl.text = item.getMainHeadingText()
            mediaCell.description_lbl.text = item.getDescriptionText()
            
            if item.getInfoType() == "video" {
                let url = URL(string: item.getInfoValue())
                if let thumbnailImage = getThumbnailImage(forUrl: url!) {
                    mediaCell.Image_Media_img.image = thumbnailImage
                } else {
                    //set a place holder image in imageview
                }
            } else {
                // if only for video -- hide image vieww
            }
            return mediaCell
        }
      //  return deepDiveCell
        
    }
    //Mark :- check to show/hide cell accessory
    func showHideCellAccessory(infoType : String, cell : UITableViewCell) {
        if infoType != "none" {
            cell.accessoryType = .disclosureIndicator
        } else {
            cell.accessoryType = .none
        }
    }
}
