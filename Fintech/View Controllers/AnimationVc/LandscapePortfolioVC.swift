//
//  LandscapePortfolioVC.swift
//  Fintech
//
//  Created by Apple on 28/08/2020.
//  Copyright © 2020 Broadstone Technologies. All rights reserved.
//

import UIKit
import SwiftyToolTip
import Charts
class LandscapePortfolioVC: Parent{

    var xAxis:[String] = []
    var yAxis:[Double] = []
    
    private var dis = String()
    var SELECTED_DATA_TYPE : String = ""
    let MAIN_DATA : String = "main_data"
     var bechYAxis:[Double] = []
    let unitsSold = [ 0.999999999999806,
                      2.0000000000004761,
                      2.9999999999998579,
                      1.0125076992217941e-14]
    
    var months = ["15/03/19", "16/03/19", "17/03/19", "18/03/19", "19/03/19"]
    @IBOutlet weak var linechartview: LineChartView!
    
    @IBOutlet weak var Day_lbl: UIButton!
    @IBOutlet weak var Week_lbl: UIButton!
    // MArk: - outlets view1- summary
    @IBOutlet weak var year_btn: UIButton!
    @IBOutlet weak var month_lbl: UIButton!
    @IBOutlet weak var All_btn: UIButton!
    
        // MArk: - class varibales
        var refreshControl: UIRefreshControl!
        var selectedChartInterval : String = "40"
        var dataEntriesChart: [PointEntry]?
        var dataEntriesBenchmarkChart : [PointEntry]?
        var benchMarkTitle = ""
        // MARK: - Life Cycle
        override func viewDidLoad() {
            super.viewDidLoad()
            // Do any additional setup after loading the view.
        }
        override func viewWillAppear(_ animated: Bool) {
            
        }
        override func viewDidAppear(_ animated: Bool) {
            localization()
            
            if NetworkReachability.isConnectedToNetwork() {
                getSummaryData()
            } else {
                self.view.makeToast(ConstantStrings.NO_INTERNET_CONNECTION_FOUND)
            }
        }
    

        func localization(){
          //  setNavBarTitile(title:getLocalizedString(key: "Portfolio", comment: ""), topItem: getLocalizedString(key: "Portfolio", comment: ""))
            navigationController?.navigationBar.barStyle = .blackOpaque
            Day_lbl.setTitle(getLocalizedString(key: "40", comment: ""), for: .normal)
            Week_lbl.setTitle(getLocalizedString(key: "50", comment: ""), for: .normal)
            month_lbl.setTitle(getLocalizedString(key: "70", comment: ""), for: .normal)
            year_btn.setTitle(getLocalizedString(key: "100", comment: ""), for: .normal)
            All_btn.setTitle(getLocalizedString(key: "All", comment: ""), for: .normal)
           // compalinDetails_lbl.text = getLocalizedString(key: "Companies You own", comment: "")
            
        }
        //     MARK: - Class Functions
        // MARK: - Line chart funcs
        //generate chart random enteries for test data
        func generateRandomEntries(arrayList : [PortfolioDataModel]) -> [PointEntry] {
            var result: [PointEntry] = []
            for i in 0..<arrayList.count {
                
                let yValue = arrayList[i].getDataYaxisValue()
                let xValue = arrayList[i].getDataXaxisValue() //Int(arc4random() % 500)
                
                result.append(PointEntry(systolic: Int(yValue), diastolic: Int(ArrayLists.sharedInstance.portfolioBenchmarkDayChartDataList[i].getDataYaxisValue()), label: xValue))
                
            }
            return result
        }
    
        func loadChart(interval : String) {
            let dataEntriesAppChart = generateRandomEntries(arrayList: ArrayLists.sharedInstance.portfolioDayChartDataList)
            let minValue = 9000
            let maxValue = 11000
            
        }
    
        func loadBenchmarkChart(interval : String) {
            let  dataEntriesChart = generateRandomEntries(arrayList: ArrayLists.sharedInstance.portfolioBenchmarkMonthChartDataList)
        }
    
        //     MARK: - IB Actions
        @IBAction func infoBtn_pressed(_ sender: Any) {
           // info_btn.showToolTip()
        }
        @IBAction func graphBtn_pressed(_ sender: Any) {
            guard let graphVC = self.storyboard?.instantiateViewController(withIdentifier: "graph_vc") as? GraphVC else {
                return
            }
            self.navigationController?.pushViewController(graphVC, animated: true)
        }
    
        //     MARK: - Segment Actions
    

        //     MARK: - Chart data actions
    
        @IBAction func loadDayData(_ sender: Any) {
            self.Day_lbl.backgroundColor = #colorLiteral(red: 0.7137254902, green: 0.7647058824, blue: 0.9176470588, alpha: 1)
            self.Week_lbl.backgroundColor = UIColor.clear
            self.month_lbl.backgroundColor = UIColor.clear
            self.year_btn.backgroundColor = UIColor.clear
            self.All_btn.backgroundColor = UIColor.clear
            selectedChartInterval = "40"
            print("day data pressed")
            getvalues(list: ArrayLists.sharedInstance.portfolioDayChartDataList, bechmarklist: ArrayLists.sharedInstance.portfolioBenchmarkDayChartDataList)
            
        }
        @IBAction func loadWeekData(_ sender: Any) {
            selectedChartInterval = "50"
            self.Week_lbl.backgroundColor = #colorLiteral(red: 0.7137254902, green: 0.7647058824, blue: 0.9176470588, alpha: 1)
            self.Day_lbl.backgroundColor = UIColor.clear
            self.month_lbl.backgroundColor = UIColor.clear
            self.year_btn.backgroundColor = UIColor.clear
            self.All_btn.backgroundColor = UIColor.clear
            getvalues(list: ArrayLists.sharedInstance.portfolioWeekChartDataList, bechmarklist: ArrayLists.sharedInstance.portfolioBenchmarkWeekChartDataList)
            //loadChart(interval: selectedChartInterval)
        }
        @IBAction func loadMonthData(_ sender: Any) {
            
            self.month_lbl.backgroundColor = #colorLiteral(red: 0.7137254902, green: 0.7647058824, blue: 0.9176470588, alpha: 1)
            self.Day_lbl.backgroundColor = UIColor.clear
            self.Week_lbl.backgroundColor = UIColor.clear
            self.year_btn.backgroundColor = UIColor.clear
            self.All_btn.backgroundColor = UIColor.clear
            selectedChartInterval = "70"
            //loadChart(interval: selectedChartInterval)
            getvalues(list: ArrayLists.sharedInstance.portfolioMonthChartDataList, bechmarklist: ArrayLists.sharedInstance.portfolioBenchmarkMonthChartDataList)
        }
        @IBAction func loadYearData(_ sender: Any) {
            self.year_btn.backgroundColor = #colorLiteral(red: 0.7137254902, green: 0.7647058824, blue: 0.9176470588, alpha: 1)
            self.Day_lbl.backgroundColor = UIColor.clear
            self.month_lbl.backgroundColor = UIColor.clear
            self.Week_lbl.backgroundColor = UIColor.clear
            self.All_btn.backgroundColor = UIColor.clear
            selectedChartInterval = "100"
            //loadChart(interval: selectedChartInterval)
            getvalues(list: ArrayLists.sharedInstance.portfolioYearChartDataList, bechmarklist: ArrayLists.sharedInstance.portfolioBenchmarkYearChartDataList)
        }
        @IBAction func loadAllData(_ sender: Any) {
            self.All_btn.backgroundColor = #colorLiteral(red: 0.7137254902, green: 0.7647058824, blue: 0.9176470588, alpha: 1)
            self.Day_lbl.backgroundColor = UIColor.clear
            self.month_lbl.backgroundColor = UIColor.clear
            self.year_btn.backgroundColor = UIColor.clear
            self.Week_lbl.backgroundColor = UIColor.clear
            selectedChartInterval = "all"
            // loadChart(interval: selectedChartInterval)
        }
    
    
        //MARK: -Future Predictions Api
        func getFuturePredctions() {
            self.view.activityStartAnimating()
            let url = ApiUrl.GET_PORTFOLIO_FUTURE_PREDICTIONS_URL
            let namespace = SPManager.sharedInstance.getCurrentUserNamespace()
            let data:Dictionary<String,AnyObject> = [:]
            
            //namespace:"test6-MecY"
            let params:Dictionary<String,AnyObject> = ["namespace" : namespace as AnyObject, "data" : data as AnyObject]
            print(data)
            APIManager.sharedInstance.postRequest(serviceName: url, sendData: params, success: { (response) in
                print(response)
                
                if APIManager.sharedInstance.isSuccess {
                    let res = response["message"] as AnyObject
                    DispatchQueue.main.async {
                        
                        self.fetchFuturePredictionData(jsonData: res as! [[String : AnyObject]])
                       // self.futurePredictionsTableView.reloadData()
                    }
                    
                }else{
                    if response["message"] != nil {
                        DispatchQueue.main.async {
                            self.showAlert(response["message"] as! String)
                        }
                    } else {
                        DispatchQueue.main.async {
                            self.showAlert(ConstantStrings.NO_RESULT_FOUND_ALERT_TEXT)
                        }
                    }
                }
                DispatchQueue.main.async {
                    self.view.activityStopAnimating()
                }
            }, failure: { (data) in
                print(data)
                DispatchQueue.main.async {
                    self.view.activityStopAnimating()
                }
                if APIManager.sharedInstance.error400 {
                    self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
                }
                if(APIManager.sharedInstance.error401)
                {
                    self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
                }else{
                    
                }
            })
        }
        func fetchFuturePredictionData(jsonData : [[String : AnyObject]]) {
            ArrayLists.sharedInstance.futurePredictionsList.removeAll()
            for data in jsonData {
                let plan = data["plan"] as! String
                let details = data["details"] as! [[String: AnyObject]]
                for planDetail in details {
                    
                    let item = FuturePredictionsModel()
                    let confidence = planDetail["confidence_level"] as! Double
                    let profit = planDetail["profit"] as! Double
                    item.setConfidenceLevel(Int(confidence))
                    item.setProfit(Int(profit))
                    item.setPlan(plan)
                    if plan == "0 years" {
                        
                    } else {
                        if plan == "5 years" {
                            ArrayLists.sharedInstance.futurePredictionsTextualList.append(item)
                        }
                        ArrayLists.sharedInstance.futurePredictionsList.append(item)
                    }
                }
                
            }
        }
    
        // MARK: - Navigation
        func getSummaryData() {
            self.view.activityStartAnimating()
            let namespace = SPManager.sharedInstance.getCurrentUserNamespace()
            var url = ApiUrl.GET_USER_PORTFOLIO_SUMMARY_DATA_URL
            let charset = CharacterSet(charactersIn: "gst-")
                   if namespace.rangeOfCharacter(from: charset) != nil {
                       print("guest namespace")
                    url = ApiUrl.GET_GUEST_PORTFOLIO_SUMMARY_DATA_URL
                   } else {
                       print("usernamespace")
                    url = ApiUrl.GET_USER_PORTFOLIO_SUMMARY_DATA_URL
                   }
            let data:Dictionary<String,AnyObject> = [:]
            //test6-MecY
            let params:Dictionary<String,AnyObject> = ["namespace" : "test6-MecY" as AnyObject, "data" : data as AnyObject]
            print(data)
            APIManager.sharedInstance.postRequest(serviceName: url, sendData: params, success: { (response) in
                print(response)
                
                if APIManager.sharedInstance.isSuccess {
                    let res = response["message"] as AnyObject
                    
                    DispatchQueue.main.async {
                      //  self.summary_view.isHidden = false
                                            //QuestionnaireParser.sharedInstance.fetchMetaData(json: res as! [String : AnyObject])
                        
                        self.fetchMetaData(jsonData: res as! [String : AnyObject])
                    }
                    
                }else{
                    if response["message"] != nil {
                        DispatchQueue.main.async {
                            self.showAlert(response["message"] as! String)
                        }
                    } else {
                        DispatchQueue.main.async {
                            self.showAlert(ConstantStrings.NO_RESULT_FOUND_ALERT_TEXT)
                        }
                    }
                }
                DispatchQueue.main.async {
                    self.view.activityStopAnimating()
                }
            }, failure: { (data) in
                print(data)
                DispatchQueue.main.async {
                    self.view.activityStopAnimating()
                }
                if APIManager.sharedInstance.error400 {
                    self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
                }
                if(APIManager.sharedInstance.error401)
                {
                    self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
                }else{
                    
                }
            })
        }
    
        // MARK: - fetchData
        func fetchMetaData(jsonData : [String : AnyObject]) {
            ArrayLists.sharedInstance.portfolioSummaryDataList.removeAll()
            ArrayLists.sharedInstance.portfolioHistoryDataList.removeAll()
            ArrayLists.sharedInstance.portfolioDetailsDataList.removeAll()
            
            let companiesData = jsonData["categories_data"] as! [[String : AnyObject]]
            let chartData = jsonData["chart_data"] as! [String : AnyObject]
            let benchmarkChartData = jsonData["benchmark_chart_data"] as! [String : AnyObject]
            let historyData = jsonData["history"] as! [[String : AnyObject]]
            let detailsData = jsonData["details"] as! [[String : AnyObject]]
            
            for jsonData in companiesData {
                let model = PortfolioDataModel()
                let companyName = jsonData["name"] as! String
                let companyValue = jsonData["value"] as! Double
                let childCompanies = jsonData["companies"] as! [String]
                print("companyValue== \(companyValue)")
                model.setCategoryValue(value: companyValue)
                model.setCategoryName(name: companyName)
                model.childCompaniesList = childCompanies
                
                ArrayLists.sharedInstance.portfolioSummaryDataList.append(model)
            }
            //Mark: - parse app chart data
            print(chartData)
            let dayData = chartData["day_data"] as! [String : AnyObject]
            let weekData = chartData["week_data"] as! [String : AnyObject]
            let monthData = chartData["month_data"] as! [String : AnyObject]
            let yearData = chartData["year_data"] as! [String : AnyObject]
            //Mark: - parse benchmark chart data
            
            let benchmarkTitle = benchmarkChartData["title"] as! String
            self.benchMarkTitle = benchmarkTitle
            let benchmarkDayData = benchmarkChartData["day_data"] as! [String : AnyObject]
            let benchmarkWeekData = benchmarkChartData["week_data"] as! [String : AnyObject]
            let benchmarkMonthData = benchmarkChartData["month_data"] as! [String : AnyObject]
            let benchmarkYearData = benchmarkChartData["year_data"] as! [String : AnyObject]
            
            //Mark: - add app chart data into lists
            print(dayData)
            print(weekData)
            print(monthData)
            print(yearData)
            ArrayLists.sharedInstance.portfolioDayChartDataList = parseChartJsonData(data: dayData)
            ArrayLists.sharedInstance.portfolioWeekChartDataList = parseChartJsonData(data: weekData)
            ArrayLists.sharedInstance.portfolioMonthChartDataList = parseChartJsonData(data: monthData)
            ArrayLists.sharedInstance.portfolioYearChartDataList = parseChartJsonData(data: yearData)
            
            //Mark: - get benchmark data
            ArrayLists.sharedInstance.portfolioBenchmarkDayChartDataList = parseChartJsonData(data: benchmarkDayData)
            ArrayLists.sharedInstance.portfolioBenchmarkWeekChartDataList = parseChartJsonData(data: benchmarkWeekData)
            ArrayLists.sharedInstance.portfolioBenchmarkMonthChartDataList = parseChartJsonData(data: benchmarkMonthData)
            ArrayLists.sharedInstance.portfolioBenchmarkYearChartDataList = parseChartJsonData(data: benchmarkYearData)
            ArrayLists.sharedInstance.portfolioHistoryDataList = parseHistoryJsonData(data: historyData)
            ArrayLists.sharedInstance.portfolioDetailsDataList = parseDetailsJsonData(data: detailsData)
            
               self.getvalues(list: ArrayLists.sharedInstance.portfolioDayChartDataList, bechmarklist: ArrayLists.sharedInstance.portfolioBenchmarkDayChartDataList)
            DispatchQueue.main.async {
               // table_view.reloadData()
                self.getvalues(list: ArrayLists.sharedInstance.portfolioDayChartDataList, bechmarklist: ArrayLists.sharedInstance.portfolioBenchmarkDayChartDataList)
                
                //            self.getvalues(list: ArrayLists.sharedInstance.portfolioWeekChartDataList, bechmarklist: ArrayLists.sharedInstance.portfolioBenchmarkDayChartDataList )
            }
                //            self.getvalues(list: ArrayLists.sharedInstance.portfolioWeekChartDataList, bechmarklist: ArrayLists.sharedInstance.portfolioBenchmarkDayChartDataList )
            
        }
    
        func parseChartJsonData(data : [String : AnyObject]) -> [PortfolioDataModel] {
            var list : [PortfolioDataModel] = [PortfolioDataModel]()
            let lastRefreshedInterval = data["last_refresh"] as! Int
            let chartData = data["data"] as! [[String : AnyObject]]
            for jsonData in chartData {
                let model = PortfolioDataModel()
                let xAxis = jsonData["x_axis"] as! String
                let yAxis = jsonData["y_axis"] as! Double
                print(xAxis)
                model.setDataInterval(interval:"year_data")
                model.setDataXaxisValue(value: xAxis)
                model.setDataYaxisValue(value: yAxis)
                model.setLastRefreshedTime(value: lastRefreshedInterval)
                list.append(model)
            }
            return list
        }
        //Mark: - parse portfolio history data
        func parseHistoryJsonData(data : [[String : AnyObject]]) -> [PortfolioDataModel] {
            var list : [PortfolioDataModel] = [PortfolioDataModel]()
            for jsonData in data {
                let model = PortfolioDataModel()
                let keyName = jsonData["name"] as! String //current_year
                let keyValue = jsonData["value"] as! String //2019
                let appName = jsonData["app_name"] as! String //fintech
                let appValue = jsonData["app_value"] as! Double //-7.5
                let competitorName = jsonData["competitor_name"] as! String //s&p
                let competitorValue = jsonData["competitor_value"] as! Double //5.0
             //   self.historyAppName_lbl.text = appName
               // self.historyCompetitorname_lbl.text = competitorName
                model.setKeyName(name: keyName)
                model.setKeyValue(value: keyValue)
                model.setAppName(name: appName)
                model.setAppValue(value: appValue)
                model.setCompetitorName(name: competitorName)
                model.setCompetitorValue(value: competitorValue)
                list.append(model)
            }
            return list
        }
        // Mark: - parse portfolio details data
        func parseDetailsJsonData(data : [[String : AnyObject]]) -> [NotificationDataModel] {
            
            var list : [NotificationDataModel] = [NotificationDataModel]()
            for jsonData in data {
                let model = NotificationDataModel()
                let mainHeading = jsonData["main_heading"] as! String
                let subHeading = jsonData["subheading"] as! String
                let description = jsonData["description"] as! String
                let iconUrl = jsonData["icon_url"] as! String
                //            let progressValue = jsonData["progress_value"] as! Double //umair is changing string-> float
                let dataType = jsonData["type"] as! String
                let timestamp = jsonData["eve_sec"] as! Double
                
                model.setMainHeadingText(text: mainHeading)
                model.setSubHeadingText(text: subHeading)
                model.setDescriptionText(text: description)
                model.setIconImage(text: iconUrl)
                model.setProgressValue(text: 2.3)
                model.setDataType(text: dataType)
                model.setUnixTimestamp(timestamp: timestamp)
                
                if jsonData["additional_info"] as? [String : AnyObject] != nil {
                    let additionalInfo = jsonData["additional_info"] as! [String : AnyObject]
                    //Mark: - parse additional info
                    let infoType = additionalInfo["type"] as! String
                    model.setInfoType(type: infoType)
                    if infoType == "text" {
                        let infoValue = additionalInfo["value"] as! [String : AnyObject]
                        let infoHeading = infoValue["info_heading"] as! String
                        let infoSubHeading = infoValue["info_sub_heading"] as! String
                        let infoDescription = infoValue["info_description"] as! String
                        let infoDisclaimerText = infoValue["info_disclaimer_text"] as! String
                        let infoDisclaimerLink = infoValue["info_disclaimer_link"] as! String
                        
                        
                        model.setInfoHeading(text: infoHeading)
                        model.setInfoSubHeading(text: infoSubHeading)
                        model.setInfoDescription(text: infoDescription)
                        model.setInfoDisclaimerText(text: infoDisclaimerText)
                        model.setInfoDisclaimerLink(text: infoDisclaimerLink)
                        //                model.setInfoDisclaimerLinkType(text: infoDisclaimerLinkType)
                    } else {
                        let infoValue = additionalInfo["value"] as! String
                        model.setInfoValue(value: infoValue)
                    }
                } else {
                    model.setInfoType(type: "none")
                }
                
                
                list.append(model)
            }
            print("details parsed list size== \(list.count)")
            return list
        }
    
        func setChart(dataPoints: [String], values: [Double], values2:[Double]) {
            
            
            let data = LineChartData()
            var lineChartEntry1 = [ChartDataEntry]()
            var lineChartEntry2 = [ChartDataEntry]()
            var lineChartEntry3 = [ChartDataEntry]()

            for i in 0..<values2.count {
                lineChartEntry1.append(ChartDataEntry(x: Double(i), y: Double(values2[i]) ))
            }
            let line1 = LineChartDataSet(entries: lineChartEntry1, label: "Fintech")
            
            line1.lineWidth=2.0
            line1.colors=[UIColor.orange]
            // line1.circleColors=[UIColor.brown]
            let gradient1 = getGradientFilling(color: #colorLiteral(red: 0.1960784346, green: 0.3411764801, blue: 0.1019607857, alpha: 1))
            line1.fill = Fill.fillWithLinearGradient(gradient1, angle: 90.0)
            line1.drawFilledEnabled = true
            line1.drawValuesEnabled = false
            line1.drawCirclesEnabled = false
            //line1.mode = .cubicBezier
            data.addDataSet(line1)
            
            
            for i in 0..<values.count {
                lineChartEntry2.append(ChartDataEntry(x: Double(i), y: Double(values[i]) ))
            }
            var line2 = LineChartDataSet(entries: lineChartEntry2, label: "S&P 500")
            line2.mode = .cubicBezier
            line2.drawValuesEnabled = false
            line2.drawCirclesEnabled = false
            
            line2.lineWidth=2.0
            line2.colors=[UIColor.yellow]
            let gradient = getGradientFilling(color: #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1))
           // line2.fill = Fill.fillWithLinearGradient(gradient, angle: 90.0)
            line2.drawFilledEnabled = true
            
            data.addDataSet(line2)
            
            
            var line3 = LineChartDataSet(entries: lineChartEntry3, label: "S&P 500")
            line3.mode = .cubicBezier
            line3.drawValuesEnabled = false
            line3.drawCirclesEnabled = false
            
            line3.lineWidth=2.0
            line3.colors=[UIColor.yellow]
            let gradient2 = getGradientFilling(color: #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1))
            line3.fill = Fill.fillWithLinearGradient(gradient, angle: 90.0)
            line3.drawFilledEnabled = true
            
            data.addDataSet(line3)
            
            
            
            data.setValueTextColor(NSUIColor.black)
            data.setValueFont(UIFont.init(name: "Verdana", size: 12.0)!)
            self.linechartview.data=data
            
            linechartview.xAxis.valueFormatter = IndexAxisValueFormatter(values:dataPoints)
            linechartview.xAxis.granularityEnabled = true
            linechartview.xAxis.labelPosition = .bottom
            
            linechartview.xAxis.labelTextColor = UIColor.white
            linechartview.leftAxis.labelTextColor = UIColor.white
            linechartview.xAxis.labelCount = 30
            linechartview.xAxis.granularity = 1
            linechartview.leftAxis.enabled = true
            linechartview.pinchZoomEnabled = false
            linechartview.scaleYEnabled = true
            linechartview.scaleXEnabled = true
            linechartview.highlighter = nil
            linechartview.setVisibleXRangeMaximum(10)
            linechartview.moveViewToX(90)
            
            linechartview.leftAxis.accessibilityScroll(UIAccessibilityScrollDirection(rawValue: Int(20))!)
            linechartview.doubleTapToZoomEnabled = true
            linechartview.backgroundColor = UIColor.clear
            linechartview.chartDescription?.text = ""
            linechartview.leftAxis.drawGridLinesEnabled = false
            linechartview.rightAxis.drawGridLinesEnabled = false
            linechartview.xAxis.drawGridLinesEnabled = false
            
            self.linechartview.xAxis.labelFont = UIFont.init(name: "Verdana", size: 12.0)!
            
            let legend = linechartview.legend
            legend.font = UIFont(name: "Verdana", size: 11.0)!
            linechartview.legend.textColor = UIColor.white
            linechartview.animate(xAxisDuration: 0.5, yAxisDuration: 1.0, easingOption: .easeInOutQuart)
            
            linechartview.drawGridBackgroundEnabled = false
            
            
        }
    }



    extension LandscapePortfolioVC{
        
        //MARK :- Creating gradient for filling space under the line chart
        private func getGradientFilling(color:CGColor) -> CGGradient {
            // MARK :-Setting fill gradient color
            let coloTop = UIColor(red: 141/255, green: 133/255, blue: 220/255, alpha: 1).cgColor
            let colorBottom = UIColor(red: 230/255, green: 155/255, blue: 210/255, alpha: 1).cgColor
            //MARK :- Colors of the gradient
            let gradientColors = [color, color] as CFArray
            //MARK :- Positioning of the gradient
            let colorLocations: [CGFloat] = [0.7, 0.0]
            //MARK :- Gradient Object
            return CGGradient.init(colorsSpace: CGColorSpaceCreateDeviceRGB(), colors: gradientColors, locations: colorLocations)!
        }
        func getvalues(list:[PortfolioDataModel],bechmarklist:[PortfolioDataModel]){
            xAxis = []
            yAxis = []
            bechYAxis = []
            
            for item in list{
                let yValue = item.getDataYaxisValue()
                let xValue = item.getDataXaxisValue()
                yAxis.append(yValue)
                xAxis.append(xValue)
            }
            for item in list{
                let yValue = item.getDataYaxisValue()
                
                bechYAxis.append(yValue)
                
                
                
                setChart(dataPoints: xAxis, values: yAxis, values2: bechYAxis)
                
            }
        }
        
        
        

}

