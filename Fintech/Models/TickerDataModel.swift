//
//  TickerDataModel.swift
//  Fintech
//
//  Created by Apple on 27/09/2020.
//  Copyright © 2020 Broadstone Technologies. All rights reserved.

// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let tickerModel = try? newJSONDecoder().decode(TickerModel.self, from: jsonData)

//import Foundation
//
// // MARK: - TickerModel
//struct TickerModel: Codable {
//    let message: [Message1]?
//}
//// MARK: - Message
//struct Message1: Codable {
//    let name: String?
//    let companies: [Company]?
//    let value: Double?
//    let categoryID: Int?
//
//    enum CodingKeys: String, CodingKey {
//        case name, companies, value
//        case categoryID = "category_id"
//    }
//}
//// MARK: - Company
//struct Company: Codable {
//    let ticker, tickerTitle: String?
//    let tickerShare: Int?
//    enum CodingKeys: String, CodingKey {
//        case ticker
//        case tickerTitle = "ticker_title"
//        case tickerShare = "ticker_share"
//    }
//}
















//struct TickerModel: Codable {
//    let message: [Message1]?
//}
//
//// MARK: - Message
//struct Message1: Codable {
//    let name: String?
//    let companies: [Company]?
//    let value, categoryID: Int?
//
//    enum CodingKeys: String, CodingKey {
//        case name, companies, value
//        case categoryID = "category_id"
//    }
//}
//
//// MARK: - Company
//struct Company: Codable {
//    let ticker, tickerTitle: String?
//    let tickerShare: Double?
//
//    enum CodingKeys: String, CodingKey {
//        case ticker
//        case tickerTitle = "ticker_title"
//        case tickerShare = "ticker_share"
//    }
//}





import Foundation

// MARK: - TickerModel
struct TickerModel: Codable {
    var message: [Message1]?
}

// MARK: - Message
struct Message1: Codable {
    let name: String?
    var companies: [Company]?
    let amount: Double?
    let value, categoryID: Int?
    
    enum CodingKeys: String, CodingKey {
        case name, companies, value
        case categoryID = "category_id"
        case amount

    }
}

// MARK: - Company
struct Company: Codable {
    let ticker, tickerTitle: String?
    let tickerShare: Double
    let tickerAmmount: Double
    let cikNumber: Int
    
    enum CodingKeys: String, CodingKey {
        case ticker
        case tickerTitle = "ticker_title"
        case tickerShare = "ticker_share"
        case tickerAmmount = "ticker_ammount"
        case cikNumber = "cik_number"

    }
}
