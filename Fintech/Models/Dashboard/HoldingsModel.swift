//
//  HoldingsModel.swift
//  vesgoAPI
//
//  Created by Billal on 28/01/2021.
//
import Foundation

// MARK: - Welcome
struct Welcome: Codable {
    let success: Int?
    let data: [WelcomeDatum]?
}

// MARK: - WelcomeDatum
struct WelcomeDatum: Codable {
    let ticker: String?
    let chartData: ChartData?

    enum CodingKeys: String, CodingKey {
        case ticker
        case chartData = "chart_data"
    }
}

// MARK: - ChartData
struct ChartData: Codable {
    let lastRefresh: Int?
    let data: [ChartDataDatum]?

    enum CodingKeys: String, CodingKey {
        case lastRefresh = "last_refresh"
        case data
    }
}

// MARK: - ChartDataDatum
struct ChartDataDatum: Codable {
    let xAxis: String?
    let yAxis: Double?

    enum CodingKeys: String, CodingKey {
        case xAxis = "x_axis"
        case yAxis = "y_axis"
    }
}
