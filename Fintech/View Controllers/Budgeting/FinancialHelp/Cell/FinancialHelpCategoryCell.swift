//
//  FinancialHelpCategoryCell.swift
//  Fintech
//
//  Created by broadstone on 16/02/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import UIKit

class FinancialHelpCategoryCell: UITableViewCell {
    @IBOutlet weak var spentLabel: UILabel!
    @IBOutlet weak var plannedLabel: UILabel!
    @IBOutlet weak var amountSpentLabel: UILabel!
    @IBOutlet weak var peerSpentLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var imgBgView: UIView!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var diffLabel: UILabel!
    @IBOutlet weak var diffImage: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
