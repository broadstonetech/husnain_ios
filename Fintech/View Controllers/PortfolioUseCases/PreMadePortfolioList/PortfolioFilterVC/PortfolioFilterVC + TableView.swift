//
//  PortfolioFilterVC + TableView.swift
//  Fintech
//
//  Created by broadstone on 04/03/2022.
//  Copyright © 2022 Broadstone Technologies. All rights reserved.
//

import Foundation
import UIKit

extension PortfolioFilterVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return filters.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1 + filters[section].options.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FilterHeaderCell") as! FilterHeaderCell
            cell.headingLabel.text = filters[indexPath.section].name
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FilterOptionsCell") as! FilterOptionsCell
            let data = filters[indexPath.section].options[indexPath.row - 1]
            cell.optionLabel.text = data.name
            
            if data.isSelected {
                cell.optionImage.isHidden = false
            }else {
                cell.optionImage.isHidden = true
            }
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            print("Not clickable")
        }else {
            removeSelectedOption(for: indexPath.section)
            filters[indexPath.section].options[indexPath.row - 1].isSelected = true
            filters[indexPath.section].selectedOptions = filters[indexPath.section].options[indexPath.row - 1].key
            tableView.reloadData()
        }
        
    }
    
    private func removeSelectedOption(for section: Int) {
        for i in 0..<filters[section].options.count {
            filters[section].options[i].isSelected = false
        }
    }
}
