//
//  SearchResultVC.swift
//  Fintech
//
//  Created by broadstone on 03/02/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import Foundation
import UIKit

class SearchResultVC: Parent ,UITableViewDelegate,UITableViewDataSource {
    
    var name: String = ""
    var incrementalSearchModel: IncrementalSearchModel!
    var searchIndex: Int = 0
    let dataArray1 = ["Environment","Social","Governance", "Vegan", "Animal rights","Child labor", "Water", "Climate change"]
    let dataArray2 = [5.5, 6.63, 10.50, 1, 1, 0, 1, 0]
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
        setNavBarTitile(title: "Search results", topItem: "Search results")
    }
    
    @IBAction func back(_ sender: Any) {
        self.PopVC()
    }
    
    //MARK: Populating the tableview
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1 + 1 + incrementalSearchModel.message[searchIndex].esgValues.count + 1 +
            incrementalSearchModel.message[searchIndex].derivedValues.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if indexPath.row < 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell2") as! Cell2
            cell.dataLbl.text = incrementalSearchModel.message[searchIndex].symbolName
            return cell
            
        }else if indexPath.row < 1 + 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SearchResultHeadingCell") as! SearchResultHeadingCell
            
            cell.headingLabel.text = "Investment impact"
            return cell
        }
        else if indexPath.row < 1 + 1 + incrementalSearchModel.message[searchIndex].esgValues.count {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell1") as! Cell1
            cell.data1Lbl.text = incrementalSearchModel.message[searchIndex].esgValues[indexPath.row - 2].value
            
            cell.data2Lbl.text =
                String(incrementalSearchModel.message[searchIndex].esgValues[indexPath.row - 2].percentile) +
                " percentile (" +
                String(incrementalSearchModel.message[searchIndex].esgValues[indexPath.row - 2].score) + ")"
            
            cell.imageCell.image = FintechUtils.sharedInstance.getInvestmentValuesIcons(investmentValue: incrementalSearchModel.message[searchIndex].esgValues[indexPath.row - 2].value)
            return cell
        }else if indexPath.row < 1 + 1 + incrementalSearchModel.message[searchIndex].esgValues.count + 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "SearchResultHeadingCell") as! SearchResultHeadingCell
            
            cell.headingLabel.text = "Investment impact"
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell1") as! Cell1
            let index = indexPath.row - 2 - incrementalSearchModel.message[searchIndex].esgValues.count - 1
            cell.data1Lbl.text = incrementalSearchModel.message[searchIndex].derivedValues[index].value
            
            if incrementalSearchModel.message[searchIndex].derivedValues[index].score > 0.0{
                cell.data2Lbl.text  = "Some impact"
            }else {
                cell.data2Lbl.text  = "No impact"
            }
            
            cell.imageCell.image = FintechUtils.sharedInstance.getInvestmentValuesIcons(investmentValue: incrementalSearchModel.message[searchIndex].derivedValues[index].value)
            return cell
        }
        
        return UITableViewCell()
    }
    
}
