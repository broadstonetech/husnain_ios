//
//  BucketHeadingCell.swift
//  Fintech
//
//  Created by broadstone on 03/07/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import UIKit

class BucketHeadingCell: UITableViewCell {
    
    @IBOutlet weak var bucketHeadingLabel: UILabel!
    @IBOutlet weak var bucketAmountLabel: UILabel!
    @IBOutlet weak var tooltipBtn: UIImageView!
    @IBOutlet weak var headingIcon: UIImageView!
    @IBOutlet weak var parentView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
