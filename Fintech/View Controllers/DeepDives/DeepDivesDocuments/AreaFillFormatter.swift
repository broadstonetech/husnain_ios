//
//  AreaFillFormatter.swift
//  Fintech
//
//  Created by broadstone on 01/11/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import Foundation
import Charts
class AreaFillFormatter: IFillFormatter {

    var fillLineDataSet: LineChartDataSet?
    
    init(fillLineDataSet: LineChartDataSet) {
        self.fillLineDataSet = fillLineDataSet
    }
    
    public func getFillLinePosition(dataSet: ILineChartDataSet, dataProvider: LineChartDataProvider) -> CGFloat {
        return 0.0
    }
    
    public func getFillLineDataSet() -> LineChartDataSet {
        return fillLineDataSet ?? LineChartDataSet()
    }
    
}
