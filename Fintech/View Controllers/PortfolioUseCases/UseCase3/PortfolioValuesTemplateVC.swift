//
//  PortfolioValuesTemplateVC.swift
//  Fintech
//
//  Created by broadstone on 10/08/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import UIKit
import Firebase

protocol PortfolioTemplateDelegate {
    func portfolioValuesAdded(postivieVals: [String], negativeVals: [String])
}

struct EsgCoreValuesStruct {
    var valueName: String
    var isImpactful: Bool
}



class PortfolioValuesTemplateVC: PortfolioParent {
    
    var templateData = [PortfolioValuesTemplate]()
    
    var availableTemplates = [
        getLocalizedString(key: "template_catholic"),
        getLocalizedString(key: "template_env_imapct"),
        getLocalizedString(key: "gov"),
        getLocalizedString(key: "template_islamic"),
        getLocalizedString(key: "template_methodist"),
        getLocalizedString(key: "template_peace"),
        getLocalizedString(key: "soc"),
        getLocalizedString(key: "template_vegan")
    ]
    
    var selectedTemplateIndex = -1
    var selectedTemplate = ""
    var includedValues = [String]()
    var excludedValues = [String]()
    var env = false
    var soc = false
    var gov = false
    var coreValues = [EsgCoreValuesStruct]()
    var isInterestBearing: Bool!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var swipeLabel: UILabel!
    @IBOutlet weak var doneBtn: UIButton!
    @IBOutlet weak var segmentControl: UISegmentedControl!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        templateData = getPortfolioTemplates()
        
        segmentControl.setTitle(getLocalizedString(key: "vesgo_suggested"), forSegmentAt: 0)
        segmentControl.setTitle(getLocalizedString(key: "choose_own"), forSegmentAt: 1)
        let titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        segmentControl.setTitleTextAttributes(titleTextAttributes, for: .selected)
        
        tableView.delegate = self
        tableView.dataSource = self
        
        createRightButton(title: getLocalizedString(key: "_next"), action: #selector(doneBtnPressed(_:)))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        setNavBarTitile(title: getLocalizedString(key: "investment_values"), topItem: getLocalizedString(key: "investment_values"))
        
        //headingLabel.text = getLocalizedString(key: "suggested_by_vesgo")
        swipeLabel.text = getLocalizedString(key: "swipe_left_to_view_details")
        
        segmentControl.selectedSegmentIndex = 0
        swipeLabel.isHidden = false
        tableView.isHidden = false
        tableView.reloadData()
    }
    
    @IBAction func doneBtnPressed(_ sender: UIButton){
        let selectedTemplate = mapValuesTemplate()
        if selectedTemplate == .NOT_SET {
            self.view.makeToast("Please select the template")
            return
        }else {
            pushPortfolioRiskVC(positiveVals: includedValues, negativeVals: excludedValues, template: self.selectedTemplate)
        }
    }
    
    @IBAction func segmentIndexChanged(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
            case 0:
                swipeLabel.isHidden = false
                tableView.isHidden = false
                break
            case 1:
                swipeLabel.isHidden = true
                tableView.isHidden = true
                detailButtonPressed(cellIndex: 8, isManualEntry: true)
                break
            default:
                break
        }
    }
}
extension PortfolioValuesTemplateVC: UITableViewDelegate, UITableViewDataSource, PortfolioTemplateDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return templateData.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 5
        }
        return 20
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int)
    {
        view.tintColor = #colorLiteral(red: 0.9411764706, green: 0.9411764706, blue: 0.9411764706, alpha: 1)
        view.backgroundColor = #colorLiteral(red: 0.9411764706, green: 0.9411764706, blue: 0.9411764706, alpha: 1)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ValuesTemplateCell") as! ValuesTemplateCell
        
        cell.templateLabel.text = templateData[indexPath.section].localizedName
        cell.templateImage.image = templateData[indexPath.section].templateImage
        cell.cellIndex = indexPath.section
//        cell.clickDelegate = self
        
        if selectedTemplateIndex == indexPath.section {
            cell.parentView.backgroundColor = .systemGreen
            cell.templateLabel.textColor = .white
//            cell.detailBtn.tintColor = .white
        }else {
            cell.parentView.backgroundColor = .white
            cell.templateLabel.textColor = .black
//            cell.detailBtn.tintColor = .black
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedTemplateIndex = indexPath.section
//        if indexPath.section == availableTemplates.count - 1 {
//            detailButtonPressed(cellIndex: indexPath.section, isManualEntry: false)
//        }
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
//        if indexPath.section == availableTemplates.count - 1 {
//            return UISwipeActionsConfiguration()
//        }
        let detailsAction = UIContextualAction(style: .normal, title:  getLocalizedString(key: "_details"), handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in

            self.detailButtonPressed(cellIndex: indexPath.section, isManualEntry: false)
            success(true)
            
            
        })
        //detailsAction.image = UIImage(sys)
        detailsAction.backgroundColor = #colorLiteral(red: 0.2935393751, green: 0.6106157899, blue: 0.8765475154, alpha: 1)

        return UISwipeActionsConfiguration(actions: [detailsAction])
    }
    
    func detailButtonPressed(cellIndex: Int, isManualEntry: Bool) {
        //Present template detail screen
        let vc = UIStoryboard(name: "Portfolio", bundle: nil).instantiateViewController(withIdentifier: "PortfolioValuesTemplateDetailsVC") as! PortfolioValuesTemplateDetailsVC
        vc.index = cellIndex
        vc.delegate = self
        
        if isManualEntry {
            vc.templateName = getLocalizedString(key: "choose_my_own")
            vc.selectedPositiveVals = includedValues
            vc.selectedNegativeVals = excludedValues
        }else {
            vc.templateName = templateData[cellIndex].localizedName
        }
        self.present(vc, animated: true, completion: nil)
    }
    
    func portfolioValuesAdded(postivieVals: [String], negativeVals: [String]) {
        if postivieVals.count == 0 && negativeVals.count == 0 {
            //selectedTemplateIndex = -1
            segmentControl.selectedSegmentIndex = 0
            tableView.isHidden = false
            swipeLabel.isHidden = false
            includedValues.removeAll()
            excludedValues.removeAll()
        }else {
            includedValues = postivieVals
            excludedValues = negativeVals
            
            pushPortfolioRiskVC(positiveVals: includedValues, negativeVals: excludedValues, template: selectedTemplate)
        }
        self.tableView.reloadData()
    }
    
    func mapValuesTemplate() -> ValuesTemplate {
        if selectedTemplateIndex == 0 {
            
            selectedTemplate = "catholic"
            return .Catholic
        }
        else if selectedTemplateIndex == 1 {
            
            selectedTemplate = "environment"
            return .Environment
        }
        else if selectedTemplateIndex == 2 {
            
            selectedTemplate = "islamic"
            return .Islamic
        }
        else if selectedTemplateIndex == 3 {
            
            selectedTemplate = "governance"
            return .Governance
        }
        else if selectedTemplateIndex == 4 {
            
            selectedTemplate = "methodist"
            return .Methodist
        }
        else if selectedTemplateIndex == 5 {
            
            selectedTemplate = "peace"
            return .Peace
        }
        else if selectedTemplateIndex == 6 {
            
            selectedTemplate = "social"
            return .Social
        }
        else if selectedTemplateIndex == 7 {
            
            selectedTemplate = "vegan"
            return .Vegan
        }
        else if selectedTemplateIndex == 8 {
            
            selectedTemplate = ""
            return .Manual
        }
        else {
            selectedTemplate = ""
            return .NOT_SET
        }
        
    }
}
