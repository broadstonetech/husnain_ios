// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let riskToleranceModel = try? newJSONDecoder().decode(RiskToleranceModel.self, from: jsonData)

import Foundation

// MARK: - RiskToleranceModel
struct RiskToleranceModel: Codable {
    let success: Int
    let message: RiskToleranceMessage
}

// MARK: - Message
struct RiskToleranceMessage: Codable {
    let risk: Double
}
