//
//  ResourcesVC + UserExpertiseLvl.swift
//  Fintech
//
//  Created by broadstone on 13/01/2022.
//  Copyright © 2022 Broadstone Technologies. All rights reserved.
//

import Foundation
import UIKit

extension ResourcesVC {
    
    func calculateUserPlan() {
        let db = Databasehandler()
        var educationData = db.readEducation()
        
        educationData.sort(by: {
            $0.sortedID < $1.sortedID
        })
        
        let userLevel = SPManager.sharedInstance.getUserEducationExpertiseLevel()
        
        if userLevel != EducationExpertiseValues.levelNotSet {
            vesgoSuggestedLesson.removeAll()
            othersLesson.removeAll()
            othersLesson.append(EducationModel.init(id: -1, heading: getLocalizedString(key: "learn_with_vesgo"), subheading: "", image: "other_lessons", type: "", sortedID: -1, bgColor: "YouMayAlsoLearn"))
            for item in educationData {
                if item.type == userLevel {
                    vesgoSuggestedLesson.append(item)
                }else {
                    othersLesson.append(item)
                }
            }
            
            vesgoSuggestedLesson.append(EducationModel.init(id: -1, heading: getLocalizedString(key: "You may also learn"), subheading: "", image: "other_lessons", type: "", sortedID: -1, bgColor: "YouMayAlsoLearn"))
            
            vesgoSuggestedEduCollectionView.reloadData()
            userEducationView.isHidden = false
            userExpertiseView.isHidden = true
        }else {
            userEducationView.isHidden = true
            userExpertiseView.isHidden = false
        }
    }
    
    @IBAction func beginnerBtnPressed(_ sender: UIButton) {
        SPManager.sharedInstance.setUserEducationExpertiseLevel(EducationExpertiseValues.levelBigginer)
        calculateUserPlan()
    }
    
    @IBAction func IntermediateBtnPressed(_ sender: UIButton) {
        SPManager.sharedInstance.setUserEducationExpertiseLevel(EducationExpertiseValues.levelIntermediate)
        calculateUserPlan()
    }
    
    @IBAction func expertBtnPressed(_ sender: UIButton) {
        SPManager.sharedInstance.setUserEducationExpertiseLevel(EducationExpertiseValues.levelExpert)
        calculateUserPlan()
    }
    
    @IBAction func quizBtnPressed(_ sender: UIButton) {
        self.tabBarController?.tabBar.isHidden = true
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "QuizExpertiselevel_VC") as! QuizExpertiselevel_VC
        navigationController?.pushViewController(vc, animated: true)
        
        setAnalyticsAction(ScreenName: "EducationalScreenVc", method: "Quiz")
    }
    
    
}
