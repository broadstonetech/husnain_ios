//
//  ScoreImprovementVC.swift
//  Fintech
//
//  Created by broadstone on 09/04/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import UIKit

class ScoreImprovementVC: Parent {
    
    struct TableViewStructure {
        var desc: String!
        var link: String!
    }
    
    var tableViewData = [TableViewStructure]()
    
    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()

        tableViewData.append(TableViewStructure(desc: getLocalizedString(key: "score_improvement_heading", comment: ""), link: ""))
        tableViewData.append(TableViewStructure(desc: getLocalizedString(key: "score_link1_desc", comment: ""), link: "https://www.footprintcalculator.org"))
        tableViewData.append(TableViewStructure(desc: getLocalizedString(key: "score_link2_desc", comment: ""), link: "https://www.green-e.org/certified-resources/carbon-offsets"))
        tableViewData.append(TableViewStructure(desc: getLocalizedString(key: "score_link3_desc", comment: ""), link: "https://www.citizen.org"))
        tableViewData.append(TableViewStructure(desc: getLocalizedString(key: "score_link4_desc", comment: ""), link: "https://www.ucsusa.org/"))
        tableViewData.append(TableViewStructure(desc: getLocalizedString(key: "score_link5_desc", comment: ""), link: "https://freedomtothrive.org"))
        tableViewData.append(TableViewStructure(desc: getLocalizedString(key: "score_link7_desc", comment: ""), link: "https://wren.co/"))
        tableViewData.append(TableViewStructure(desc: getLocalizedString(key: "score_link8_desc", comment: ""), link: "https://goldstandard.org/"))
        tableViewData.append(TableViewStructure(desc: getLocalizedString(key: "score_link6_desc", comment: ""), link: "https://en-roads.climateinteractive.org/scenario.html?v=2.7.29"))
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.reloadData()
    }
    
}
extension ScoreImprovementVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableViewData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ScoreImprovementVcHeadingCell") as! ScoreImprovementVcHeadingCell
            cell.headingLabel.text = tableViewData[indexPath.row].desc
            
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ScoreImprovementLinkCell") as! ScoreImprovementLinkCell
            cell.descLabel.text = tableViewData[indexPath.row].desc
            cell.linkLabel.text = tableViewData[indexPath.row].link
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row > 0 {
            FintechUtils.sharedInstance.openUrl(url: tableViewData[indexPath.row].link)
        }
    }
}
