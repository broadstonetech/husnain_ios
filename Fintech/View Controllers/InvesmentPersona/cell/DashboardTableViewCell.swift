//
//  DashboardTableViewCell.swift
//  Fintech
//
//  Created by broadstone on 28/01/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import UIKit
import Charts
class DashboardTableViewCell: UITableViewCell {
    
    @IBOutlet weak var chartView_line: LineChartView!
    @IBOutlet weak var heading_lbl: UILabel!
    
    @IBOutlet weak var line_view: UIView!
    var candidates: Array<String>?
    var ratings: Array<Float>?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
