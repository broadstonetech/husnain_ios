//
//  SearchResultNewVC.swift
//  Fintech
//
//  Created by broadstone on 14/03/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import UIKit

class SearchResultNewVC: Parent {

    struct TableViewData {
        var opened: Bool
        var title: String
        var desc: String
        var data: Any
        var cardColor: UIColor
    }
    
    var tableViewDataArray = [TableViewData]()
    
    var searchData: SearchData!
    var controversyModel: ControversyModel!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var dialogView: UIView!
    @IBOutlet weak var blurView: UIVisualEffectView!

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        // Do any additional setup after loading the view.
        
        if searchData.portfolioComposition.controversy == 0 {
            tableViewDataArray.append(TableViewData(opened: false, title: getLocalizedString(key: "_controversy"), desc: getLocalizedString(key: "no_significant_controversy"), data: false, cardColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)))
        }else {
            tableViewDataArray.append(TableViewData(opened: false, title: getLocalizedString(key: "_controversy"), desc: String(searchData.portfolioComposition.controversy), data: true, cardColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)))
        }
        tableViewDataArray.append(TableViewData(opened: false, title: getLocalizedString(key: "_impact", comment: ""), desc: getLocalizedString(key: "my_impact_desc", comment: ""), data: searchData.portfolioComposition, cardColor: #colorLiteral(red: 0.9976013303, green: 0.9241877198, blue: 0.9816413522, alpha: 1)))
        tableViewDataArray.append(TableViewData(opened: false, title: getLocalizedString(key: "env_score", comment: ""), desc: getLocalizedString(key: "env_desc"), data: searchData.envData, cardColor: #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1)))
        tableViewDataArray.append(TableViewData(opened: false, title: getLocalizedString(key: "social_score"), desc: getLocalizedString(key: "social_desc"), data: searchData.socialData, cardColor: #colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1)))
        tableViewDataArray.append(TableViewData(opened: false, title: getLocalizedString(key: "governance_score"), desc: getLocalizedString(key: "governance_desc"), data: searchData.governanceData, cardColor: #colorLiteral(red: 0.5058823824, green: 0.3372549117, blue: 0.06666667014, alpha: 1)))
        tableViewDataArray.append(TableViewData(opened: false, title: getLocalizedString(key: "esg_subfactors", comment: ""), desc: "", data: 1, cardColor: #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1)))
        
        
        tableView.reloadData()
        
        closeControversyDialog()
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(closeControversyDialog))
        blurView.isUserInteractionEnabled = true
        blurView.addGestureRecognizer(tapGesture)
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        setNavBarTitile(title: "Back", topItem: searchData.symbolName)
    }
    

}
extension SearchResultNewVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return tableViewDataArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableViewDataArray[section].opened {
//            if section == 0 {
//                let data = tableViewDataArray[section].data as! [String]
//                return 1 + data.count
//            }
            if section == 0 {
                return 1
            }
            else if section == 1 {
                return 1
            }
            else if section == 5 {
                return 1
            }
            else {
//                let data = tableViewDataArray[section].data as! EsgScore
//                return 1 + data.derivedValues.count
                return 1
            }
        }else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ControversyCell") as! ControversyCell
            
            cell.controversyLabel.text = tableViewDataArray[indexPath.section].title
            cell.controversyScore.text = tableViewDataArray[indexPath.section].desc
            
            let flag = (tableViewDataArray[indexPath.row].data as? Bool) ?? false
            if flag {
                cell.ellipses.isHidden = false
            }else {
                cell.ellipses.isHidden = true
            }
            
            return cell
        }
        else if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "UserInvestmentMainHeadingCell") as! InvestmentValuesNewMainHeadingCell
            
            let data = tableViewDataArray[indexPath.section].data as! OverallNewEsgModel
            cell.headingLabel.text = tableViewDataArray[indexPath.section].title

            cell.scoreLabel.text = FintechUtils.sharedInstance.getImpactForTickerSearch(data.zScore) + "\n" + getLocalizedString(key: "better_than") + " " + data.badPeers + " " + getLocalizedString(key: "_peers")
            //cell.parentView.backgroundColor = tableViewDataArray[indexPath.section].cardColor
            
            return cell
        }else if indexPath.section == 5 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "EsgSubFactorCell") as! ControversyCell
            
            cell.controversyLabel.text = tableViewDataArray[indexPath.section].title
            
            return cell
        }
        else {
            let data = tableViewDataArray[indexPath.section].data as! NewEsgScoreModel
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "UserInvestmentSubHeadingCell") as! InvestmentValuesNewSubHeadingCell
                
                
                cell.headingLabel.text = tableViewDataArray[indexPath.section].title
                //cell.descLabel.text = tableViewDataArray[indexPath.section].desc
                
                cell.scoreLabel.text = FintechUtils.sharedInstance.getImpactForTickerSearch(data.zScore) + "\n" + getLocalizedString(key: "better_than") + " " + data.badPeers + " " + getLocalizedString(key: "_peers")
                cell.headingLabel.textColor = tableViewDataArray[indexPath.section].cardColor
                return cell
                
            }else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "UserValuesCell2") as! UserValuesCell2
                
                let derivedValue = data.derivedValues[indexPath.row - 1]
                
                cell.headingLbl.text = derivedValue.valueName
                cell.iconImage.image = FintechUtils.sharedInstance.getInvestmentValuesIcons(investmentValue: derivedValue.valueName)
                //cell.parentView?.backgroundColor = tableViewDataArray[indexPath.section].cardColor
                
                if derivedValue.score > 0.0 {
                    cell.impactLbl.text = "Some impact"
                }else {
                    cell.impactLbl.text = "No impact"
                }
                
                return cell
            }
            
            
            
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            if tableViewDataArray[indexPath.section].opened {
                tableViewDataArray[indexPath.section].opened = false
            }else {
                tableViewDataArray[indexPath.section].opened = true
            }
            
        }
        if indexPath.section == 0 {
            if searchData.portfolioComposition.controversy == 0 {
                self.view.makeToast("No controversy")
            }else {
                if controversyModel == nil {
                    getControversyAPI()
                }else {
                    showControversyDialog(model: controversyModel)
                }
            }
        }
        else if indexPath.section == 5 {
            let storyboard = UIStoryboard(name: "Portfolio", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "EsgSubFactorVC") as! EsgSubFactorVC
            vc.envTagsData = parseSubFactorsData(esgType: PortfolioEsgType.ENV)
            vc.socTagsData = parseSubFactorsData(esgType: PortfolioEsgType.SOC)
            vc.govTagsData = parseSubFactorsData(esgType: PortfolioEsgType.GOV)
            vc.tagsLabelColor = .black
            vc.tagsBackgroundColor = .lightGray
            vc.companyName = searchData.symbolName
            self.navigationController?.pushViewController(vc, animated: true)
            //self.navigationController?.present(vc, animated: true, completion: nil)
        }
        
        let sections = IndexSet.init(integer: indexPath.section)
        tableView.reloadSections(sections, with: .none)
    }
    
    private func parseSubFactorsData(esgType: String) -> [SwiftTagData]{
        var tagsData = [SwiftTagData]()
        
        if esgType == PortfolioEsgType.ENV {
            for value in searchData.envData.derivedValues {
                tagsData.append(SwiftTagData.init(label: value.valueName, tagImage: FintechUtils.sharedInstance.getInvestmentValuesIcons(investmentValue: value.valueName), value: value.score))
            }
        }else if esgType == PortfolioEsgType.SOC {
            for value in searchData.socialData.derivedValues {
                tagsData.append(SwiftTagData.init(label: value.valueName, tagImage: FintechUtils.sharedInstance.getInvestmentValuesIcons(investmentValue: value.valueName), value: value.score))
            }
        }else {
            for value in searchData.governanceData.derivedValues {
                tagsData.append(SwiftTagData.init(label: value.valueName, tagImage: FintechUtils.sharedInstance.getInvestmentValuesIcons(investmentValue: value.valueName), value: value.score))
            }
        }
        
        
        
        return tagsData
    }
    
}
//MARK:- Controversy API Calling
extension SearchResultNewVC {
    func  getControversyAPI(){
        self.view.activityStartAnimating()
        let url = ApiUrl.GET_TICKET_CONTROVERSY
        let namespace = SPManager.sharedInstance.getCurrentUserNamespace()
        
        let params:Dictionary<String,AnyObject> =
            ["namespace" : namespace  as AnyObject,
             "ticker": searchData.symbol as AnyObject]
        print(params)
        
        APIManager.sharedInstance.postRequest(serviceName: url, sendData: params, success: { (response) in
            print(response)
            
            if APIManager.sharedInstance.isSuccess {
                DispatchQueue.main.async { [self] in
                    controversyModel = fetchControversyData(res: response)
                    showControversyDialog(model: controversyModel)
                }
            }else{
                
                DispatchQueue.main.async { [self] in
                    if response["message"] != nil {
                        self.showAlert(response["message"] as! String)
                    } else {
                        self.showAlert(ConstantStrings.NO_RESULT_FOUND_ALERT_TEXT)
                    }
                }
            }
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
        }, failure: { (data) in
            print(data)
            DispatchQueue.main.async { [self] in
                view.activityStopAnimating()
            }
            if APIManager.sharedInstance.error400 {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
            }
            if(APIManager.sharedInstance.error401)
            {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
            }else{
                self.showAlert(ConstantStrings.Error_msg_generic)
            }
        })
        
    }
    
    func fetchControversyData(res: [String: AnyObject]) -> ControversyModel {
        let success = res["success"] as! Bool
        let message = res["message"] as! String
        let data = res["data"] as! [String: AnyObject]
        
        let count = data["Controversy_count"] as! Int
        let relatedControversies = data["relatedControversy"] as! [String]
        let peerControversy = data["avgPeerControversy"] as! Double
        
        let controversyData = ControversyData.init(count: count, controversies: relatedControversies, peerControversy: peerControversy)
        
        return ControversyModel.init(success: success, message: message, data: controversyData)
    }
    
    func showControversyDialog(model: ControversyModel) {
        let dialog = ControversyDialogView.init(frame: dialogView.bounds)
        dialog.controversyModel = model
        dialog.setValues()
        
        dialogView.addSubview(dialog)
        dialogView.isHidden = false
        blurView.isHidden = false
    }
    
    @objc func closeControversyDialog() {
        dialogView.isHidden = true
        blurView.isHidden = true
        for view in self.dialogView.subviews {
            view.removeFromSuperview()
        }
    }
}
