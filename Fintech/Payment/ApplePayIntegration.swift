//
//  ApplePayIntegration.swift
//  Fintech
//
//  Created by broadstone on 08/01/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import Foundation
import PassKit
import Stripe

class ApplePayIntegration: PaymentsBaseClass {
    let applePayButton: PKPaymentButton = PKPaymentButton(paymentButtonType: .plain, paymentButtonStyle: .black)
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        applePayButton.isHidden = !StripeAPI.deviceSupportsApplePay()
//        applePayButton.addTarget(self, action: #selector(handleApplePayButtonTapped), for: .touchUpInside)
        if StripeAPI.deviceSupportsApplePay() {
        handleApplePayButtonTapped()
        } else {
            DispatchQueue.main.async {
            self.view.makeToast(ConstantStrings.APPLE_PAY_NOT_SUPPORTED, duration: 3.0, position: .center)
        }
        }
    }
    
//    @objc
    func handleApplePayButtonTapped() {
        let merchantIdentifier = PaymentAPIKeys.APPLE_MERCHANT_ID
        let paymentRequest = StripeAPI.paymentRequest(withMerchantIdentifier: merchantIdentifier, country: PaymentConstants.COUNTRY_CODE, currency: PaymentConstants.CURRENCY)
        
        // Configure the line items on the payment request
        paymentRequest.paymentSummaryItems = [
            // The final line should represent your company;
            // it'll be prepended with the word "Pay" (i.e. "Pay iHats, Inc $50")
            PKPaymentSummaryItem(label: "Vesgo", amount: 50.00),
        ]
        if let applePayContext = STPApplePayContext(paymentRequest: paymentRequest, delegate: self) {
            // Present Apple Pay payment sheet
            applePayContext.presentApplePay(on: self)
        } else {
            self.view.makeToast(ConstantStrings.APPLE_PAY_NOT_SUPPORTED, duration: 3.0, position: .center)
        }
    }
    
}
extension ApplePayIntegration: STPApplePayContextDelegate{
    func applePayContext(_ context: STPApplePayContext, didCreatePaymentMethod paymentMethod: STPPaymentMethod, paymentInformation: PKPayment, completion: @escaping STPIntentClientSecretCompletionBlock) {
        print(paymentInformation.token)
    }
    
    func applePayContext(_ context: STPApplePayContext, didCompleteWith status: STPPaymentStatus, error: Error?) {
        switch status {
        case .success:
            print("Success")
            break
        case .error:
            print("Error")
            break
        case .userCancellation:
            print("User Cancelled")
            break
        @unknown default:
            fatalError()
        }
    }
    
    
}
