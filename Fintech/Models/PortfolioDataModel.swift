//
//  PortfolioDataModel.swift
//  Fintech
//
//  Created by MacBook-Mubashar on 28/08/2019.
//  Copyright © 2019 Broadstone Technologies. All rights reserved.
//

import Foundation

class PortfolioDataModel: NSObject {
    
    //compaies data
    fileprivate var categoryName : String
    fileprivate var categoryValue : Double
    //chart data
    fileprivate var dataInterval : String
    fileprivate var dataXaxisValue : String
    fileprivate var dataYaxisValue : Double
    fileprivate var lastRefreshedTime : Int
    //details data
    
    fileprivate var keyName : String
    fileprivate var keyValue : String
    fileprivate var appName : String
    fileprivate var appValue : Double
    fileprivate var competitorName : String
    fileprivate var competitorValue : Double
    var childCompaniesList : [String] = [String]()
    
    override init() {
        
        categoryName = ""
        categoryValue = 0.00
        dataInterval = ""
        dataXaxisValue = ""
        dataYaxisValue = 0.00
        lastRefreshedTime = -1
        keyName = ""
        keyValue = ""
        appName = ""
        appValue = 0.00
        competitorName = ""
        competitorValue = 0.00
    }
    
    func getCategoryName() -> String {
        return categoryName
    }
    func setCategoryName(name: String) {
        self.categoryName = name
    }
    
    func getCategoryValue() -> Double {
        return categoryValue
    }
    func setCategoryValue(value: Double) {
        self.categoryValue = value
    }
    
    func getDataInterval() -> String {
        return dataInterval
    }
    func setDataInterval(interval: String) {
        self.dataInterval = interval
    }
    func getDataXaxisValue() -> String {
        return dataXaxisValue
    }
    func setDataXaxisValue(value: String) {
        self.dataXaxisValue = value
    }
    func getDataYaxisValue() -> Double{
        return dataYaxisValue
    }
    func setDataYaxisValue(value: Double) {
        self.dataYaxisValue = value
    }
    func getLastRefreshedTime() -> Int{
        return lastRefreshedTime
    }
    func setLastRefreshedTime(value: Int) {
        self.lastRefreshedTime = value
    }
    func getKeyName() -> String {
        return keyName
    }
    func setKeyName(name: String) {
        self.keyName = name
    }
    func getKeyValue() -> String {
        return keyValue
    }
    func setKeyValue(value: String) {
        self.keyValue = value
    }
    func getAppName() -> String {
        return appName
    }
    func setAppName(name: String) {
        self.appName = name
    }
    func getAppValue() -> Double {
        return appValue
    }
    func setAppValue(value: Double) {
        self.appValue = value
    }
    func getCompetitorName() -> String {
        return competitorName
    }
    func setCompetitorName(name: String) {
        self.competitorName = name
    }
    func getCompetitorValue() -> Double {
        return competitorValue
    }
    func setCompetitorValue(value: Double) {
        self.competitorValue = value
    }
}

