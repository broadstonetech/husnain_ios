//
//  SwiftTagCell.swift
//  Fintech
//
//  Created by broadstone on 30/08/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import UIKit

class SwiftTagCell: UICollectionViewCell {
    @IBOutlet weak var parentView: UIView!
    @IBOutlet weak var tagLabel: UILabel!
    @IBOutlet weak var tagImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
