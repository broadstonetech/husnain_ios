// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let financialHelpModel = try? newJSONDecoder().decode(FinancialHelpModel.self, from: jsonData)

import Foundation

// MARK: - FinancialHelpModel
struct FinancialHelpModel: Codable {
    let success: Int
    let data: [FinancialHelpDatum]
}

// MARK: - Datum
struct FinancialHelpDatum: Codable {
    let numOfDays: Int
    let totalAmountPlanned, totalAmountSpent, totalAmountSaved: Double
    let budget: [Budget]

    enum CodingKeys: String, CodingKey {
        case numOfDays = "num_of_days"
        case totalAmountPlanned = "total_amount_planned"
        case totalAmountSpent = "total_amount_spent"
        case totalAmountSaved = "total_amount_saved"
        case budget
    }
}

// MARK: - Budget
struct Budget: Codable {
    let categoryName: String
    let plannedAmount: Double
    let spentAmount: Double

    enum CodingKeys: String, CodingKey {
        case categoryName = "category_name"
        case plannedAmount = "planned_amount"
        case spentAmount = "spent_amount"
    }
}
