// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let recurringChargesNewModel = try? newJSONDecoder().decode(RecurringChargesNewModel.self, from: jsonData)

import Foundation

// MARK: - RecurringChargesNewModel
struct RecurringChargesNewModel: Codable {
    let success: Int
    let message: String
    let data: [RecurringChargesNewDatum]
}

// MARK: - Datum
struct RecurringChargesNewDatum: Codable {
    let merchantName: String
    let frequency: String
    let aggregatedAmount: Double
    let transacs: [Transac]

    enum CodingKeys: String, CodingKey {
        case merchantName = "merchant_name"
        case frequency
        case aggregatedAmount = "aggregated_amount"
        case transacs
    }
}

// MARK: - Transac
struct Transac: Codable {
    let amount: Double
    let date: Int
}
