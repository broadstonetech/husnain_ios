//
//  PortfolioHistoryCell.swift
//  Fintech
//
//  Created by MacBook-Mubashar on 31/07/2019.
//  Copyright © 2019 Broadstone Technologies. All rights reserved.
//

import Foundation
import UIKit


class PortfolioHistoryCell: UITableViewCell {
    
    
    @IBOutlet weak var stack_view: UIStackView!
    @IBOutlet weak var label_a: UILabel!
    @IBOutlet weak var label_b: UILabel!
    @IBOutlet weak var label_c: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
