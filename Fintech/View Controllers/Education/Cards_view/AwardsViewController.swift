//
//  AwardsViewController.swift
//  Fintech
//
//  Created by apple on 23/07/2020.
//  Copyright © 2020 Broadstone Technologies. All rights reserved.
//

import UIKit
import Lottie

class AwardsViewController: Parent {
    var mainQuestion = Int()
    var doneFlag = Bool()
    var innnerQuestion = String()
    var lastQuestion = Int()
    var user = ""
    var lessonName = String()
    
    @IBOutlet weak var heading_lbl: UILabel!
    
    @IBOutlet weak var share_certificate_btn: UIButton!
    @IBOutlet weak var continue_lbl: UIButton!
    @IBOutlet weak var descripation_lbl: UILabel!
    @IBOutlet weak var award: AnimationView!
    @IBOutlet weak var awrad_img: UIImageView!
    @IBOutlet weak var lesson_award_view: UIView!
    
    @IBOutlet weak var backgound_img: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        award.contentMode = .scaleAspectFit
        //2. Set animation loop mode
            award.loopMode = .loop
        // 3. Adjust animation speed
         award.animationSpeed = 1.0
        //award = AnimationView(name: "trophy")
        self.award.play()
        lesson_award_view.isHidden = true
        let db = Databasehandler.init(isForProgress: true)
        if !doneFlag {
        user = SPManager.sharedInstance.getCurrentUserEmail()
        if user == "" {
            user = "test"
        }
        let query = "INSERT INTO appData (user,mainQuestion,innerQuestion,lastQestion) VALUES ('\(user)','\(mainQuestion)','\(innnerQuestion)',\(lastQuestion))"
                  let result = db.executeQuery(query: query, isForProgress: true)
                  print(result)
        if result{
            print("save")
            lesson_award_view.isHidden = false

      ///  backgound_img.image = UIImage(named: "backgoundimg")

        }else{
            print("not save")
        //backgound_img.image = UIImage(named: "awards")
            
        }
        }
        
        
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        localization()
    }
    func localization(){
//        setNavBarTitile(title:getLocalizedString(key: "Education", comment: ""), topItem: getLocalizedString(key: "Education", comment: ""))
        continue_lbl.setTitle(getLocalizedString(key: "Continue", comment: ""), for: .normal)
          share_certificate_btn.setTitle(getLocalizedString(key: "Share certificate", comment: ""), for: .normal)
        heading_lbl.text = getLocalizedString(key: "Congratulations", comment: "")
        descripation_lbl.text = getLocalizedString(key: "wow", comment: "")
        
        //navigationController?.navigationBar.barStyle = .blackOpaque
    }
    
    
    
    @IBAction func continue_btn(_ sender: Any) {
        DispatchQueue.main.async {
            
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: EduSectionDetailsVC .self) {
                    self.navigationController!.popToViewController(controller, animated: true)
                    break
                }
            }
        }
    }
    
    @IBAction func share_btn(_ sender: Any) {
        
        let text = "I just finished learning about " + lessonName + ". You can learn about " + lessonName
            + " and many other financial literacy topics on Vesgo.\n" + ConstantStrings.APP_LINK
        shareTextualData(textData: text)
        
        
//        let image = UIImage(named: "Badge1")
//        
//        // set up activity view controller
//        let imageToShare = [ image! ]
//        let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
//        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
//        
//        // exclude some activity types from the list (optional)
//        activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook ]
//        
//        // present the view controller
//        self.present(activityViewController, animated: true, completion: nil)
        
        setAnalyticsAction(ScreenName: "AwardsVC", method: "shareAwards")
        
    }
    

    
    @IBAction func GoBack(){
        
        DispatchQueue.main.async {
         
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: EduSectionDetailsVC .self) {
                    self.navigationController!.popToViewController(controller, animated: true)
                    break
                }
            }
        }
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
