//
//  DetailViewController.swift
//  Fintech
//
//  Created by Apple on 10/07/2020.
//  Copyright © 2020 Broadstone Technologies. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAnalytics
class DetailViewController: UIViewController {
    
    //MARK: - Properties
    var color : UIColor? {
        didSet {
            containerView.backgroundColor = color
        }
    }
    
    var textTitle: String? {
        didSet {
            title = textTitle
        }
    }
    
    let containerView = UIView()
    
    // ContainerView Constraint
    var leftAnchorConstraint: NSLayoutConstraint!
    var rightAnchorConstraint: NSLayoutConstraint!
    var topAnchorConstraint: NSLayoutConstraint!
    var bottomAnchorConstraint: NSLayoutConstraint!
    
    
    //MARK: - Init
    override func loadView() {
        view = UIView()
        view.addSubview(containerView)
        configureContainerView()
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor(red:0.29, green:0.63, blue:0.49, alpha:1.0)

        // Do any additional setup after loading the view.
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        setAnalytics()
    }
    
    func setAnalytics(){
        
        let screenClass = classForCoder.description()
        Analytics.logEvent(AnalyticsEventScreenView, parameters: [AnalyticsParameterScreenClass: screenClass])
        
    }
    //MARK: - Configurations
    func configureContainerView() {
        containerView.translatesAutoresizingMaskIntoConstraints = false
        topAnchorConstraint = containerView.topAnchor.constraint(equalTo: view.topAnchor)
        bottomAnchorConstraint = containerView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        rightAnchorConstraint = containerView.rightAnchor.constraint(equalTo: view.rightAnchor)
        leftAnchorConstraint = containerView.leftAnchor.constraint(equalTo: view.leftAnchor)
        NSLayoutConstraint.activate([topAnchorConstraint,bottomAnchorConstraint,rightAnchorConstraint,leftAnchorConstraint])
    }
    
    func configurePositionOfContainer(left: CGFloat, right: CGFloat, top: CGFloat, bottom: CGFloat) {
        topAnchorConstraint.constant = top
        bottomAnchorConstraint.constant = bottom
        leftAnchorConstraint.constant = left
        rightAnchorConstraint.constant = right
        view.layoutIfNeeded()
    }
    //MARK: - Handlers
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
