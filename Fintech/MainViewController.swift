//
//  ViewController.swift
//  Fintech
//
//  Created by MacBook-Mubashar on 22/07/2019.
//  Copyright © 2019 Broadstone Technologies. All rights reserved.
//

import UIKit
import LocalAuthentication
import AVFoundation
import MarqueeLabel
import Lottie
class MainViewController: Parent {
    lazy var player = AVPlayer()
    @IBOutlet weak var back_main_view: UIView!
    @IBOutlet weak var gif_view: UIView!
    
    @IBOutlet weak var explore_vesgo_btn: UIButton!
    
    @IBOutlet weak var join_now_btn: UIButton!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
         playBgIntroVideo()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
      
        //loadTopViewGif()
        

       // animation_view.contentMode = .scaleAspectFit
        // 2. Set animation loop mode
             //animation_view.loopMode = .loop
        // 3. Adjust animation speed
       // animation_view.animationSpeed = 1.0
        //award = AnimationView(name: "trophy")
       // self.animation_view.play()
               //loadTopViewGif()
    
        if SPManager.sharedInstance.saveUserLoginStatus() {
            if let tabbedbar = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "nav_bar_start") as? UINavigationController {
                tabbedbar.modalPresentationStyle = .fullScreen
                UIApplication.shared.keyWindow?.rootViewController = tabbedbar
            }
        }else{
             localization()
        }
    }
    
    
    func localization(){
        // setNavBarTitile(title:getLocalizedString(key: "Education", comment: ""), topItem: getLocalizedString(key: "Education", comment: ""))
      
        
        join_now_btn.setTitle(getLocalizedString(key: "Join now", comment: ""), for: .normal)
        explore_vesgo_btn.setTitle(getLocalizedString(key: "Explore Vesgo", comment: ""), for: .normal)
        
        //navigationController?.navigationBar.barStyle = .blackOpaque
    }
    
    
    
    
    @IBAction func signIn_pressed(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "Main_login_page") as UIViewController
        // self.navigationController?.pushViewController(vc, animated: true)
        // self.navigationController?.pushViewController(vc, animated: true)
        
        UIApplication.shared.keyWindow?.rootViewController = vc

    }
    @IBAction func ExploreVesgo_pressed(_ sender: Any) {
        if let tabbedbar = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "nav_bar_start") as? UINavigationController {
            tabbedbar.modalPresentationStyle = .fullScreen
            UIApplication.shared.keyWindow?.rootViewController = tabbedbar
        }

    }
}
extension MainViewController{
    func playBgIntroVideo() {
        guard let videoPath = Bundle.main.path(forResource: "vesgo-first-run-intro-withoverlay", ofType:"mp4") else {
            print("error return")
            return
        }
        player = AVPlayer(url: NSURL(fileURLWithPath: videoPath) as URL)
        
        let playerLayer = AVPlayerLayer(player: player)
        playerLayer.frame = self.back_main_view.frame
        //playerLayer.frame = self.showView_invesmentStrategy_View.frame
        playerLayer.videoGravity = AVLayerVideoGravity(rawValue: AVLayerVideoGravity.resizeAspectFill.rawValue)
        self.back_main_view.layer.insertSublayer(playerLayer, at: 0)//(playerLayer)
        //self.showView_invesmentStrategy_View.layer.insertSublayer(playerLayer, at: 0)
        player.play()
        player.isMuted = true
        
        NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime, object: self.player.currentItem, queue: .main)
        { [weak self] _ in
            self?.player.seek(to: CMTime.zero)
            self?.player.play()
        }
        
    }
    func loopVideo(player : AVPlayer) {
        player.play()
        
    }
}
extension MainViewController {
    func loadTopViewGif() {
        
        let imageData = try? Data(contentsOf: Bundle.main.url(forResource: "mainheading", withExtension: "gif")!)
        
        let advTimeGif = UIImage.gifImageWithData(imageData!)
       
        let imageView2 = UIImageView(image: advTimeGif)
        
        self.gif_view.addSubview(imageView2)
        
        
        
        imageView2.translatesAutoresizingMaskIntoConstraints = false
        
        imageView2.leadingAnchor.constraint(equalTo: self.gif_view.leadingAnchor, constant: 0).isActive = true
        
        
        
        imageView2.bottomAnchor.constraint(equalTo: self.gif_view.bottomAnchor, constant: 0).isActive = true
        
        imageView2.topAnchor.constraint(equalTo: self.gif_view.topAnchor, constant: 0).isActive = true
        
        imageView2.trailingAnchor.constraint(equalTo: self.gif_view.trailingAnchor, constant: 0).isActive = true
        
        
        
        //    let animatedImage = UIImage.gif(name: "FILE_NAME")
        
        //    let imageView = UIImageView(...)
        
        //    imageView.animationImages = animatedImage?.images
        
        //    imageView.animationDuration = (animatedImage?.duration)! / 2
        
        //    imageView.startAnimating()
        
    }
}

