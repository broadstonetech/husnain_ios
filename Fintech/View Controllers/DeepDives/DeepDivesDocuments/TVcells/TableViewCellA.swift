//
//  TableViewCellA.swift
//  SecDemo
//
//  Created by SaifUllah Butt on 26/10/2021.
//

import UIKit

class TableViewCellA: UITableViewCell {
    @IBOutlet weak var companiesTitleOutlet: UILabel!
    @IBOutlet weak var companyOutletView: UIView!
    @IBOutlet weak var separator: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
