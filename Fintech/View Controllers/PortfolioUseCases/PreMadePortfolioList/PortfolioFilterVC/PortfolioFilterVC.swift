//
//  PortfolioFilterVC.swift
//  Fintech
//
//  Created by broadstone on 04/03/2022.
//  Copyright © 2022 Broadstone Technologies. All rights reserved.
//

import UIKit

class PortfolioFilterVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var filters = [PortfolioFilterCategories]()
    var delegate: PortfolioFilterVCDelegte?

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        
    }
    
    @IBAction func applyBtnPressed(_ sender: UIButton) {
        self.dismiss(animated: true)
        delegate?.portfolioFilterDelegate(data: filters)
    }
    
    @IBAction func cancelBtnPressed(_ sender: UIButton) {
        self.dismiss(animated: true)
    }
    
    
}
