//
//  ViewController.swift
//  Vesgo-PF
//
//  Created by Billal on 01/02/2021.
//

import UIKit
import Charts

class PortfolioCompositionVC: Parent {

    @IBOutlet weak var tableView: UITableView!
    
    let headings = ["Technology", "Consumer"]
    let percentage = ["65%", "35%"]
    let detail1Labels = ["Facebook Inc.", "Apple Inc.", "Alphabet Inc."]
    let detail1Shares = ["40%", "35%", "25%"]
    
    let detail2Labels = ["General Electric", "Nestle"]
    let detail2Shares = ["65%", "35%"]
    
    var portfolioCompositionModel: PortfolioCompositionModel!
    
    var chartValuesSum = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 200
        
        getUserPortfolioComposition()
        
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        setNavBarTitile(title: "Portfolio analysis", topItem: "Portfolio analysis")
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    //Mark:- Portfolio Composition API
    func  getUserPortfolioComposition(){
        view.activityStartAnimating()
        let url = ApiUrl.GET_PORTFOLIO_COMPOSITION
        let namespace = SPManager.sharedInstance.getCurrentUserNamespace()
        //let namespace = "ali"

        postManagerUser(method: "POST", isThisURLEmbdedData: false, urlString: url, parameter: ["namespace": namespace], success: {(value) in

            do {
                self.portfolioCompositionModel = try JSONDecoder().decode(PortfolioCompositionModel?.self, from: value)
                DispatchQueue.main.async { [self] in
                    portfolioCompositionModel?.data.sorted {
                        $0.industryShare > $1.industryShare
                    }
                    tableView.reloadData()
                }
                

            } catch {
                
            }

            
            DispatchQueue.main.async { [self] in
                view.activityStopAnimating()
            }

        }, failure: {(err) in

            DispatchQueue.main.async { [self] in
                view.activityStopAnimating()
            }

            if APIManager.sharedInstance.error400 {

                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)

            }

            if(APIManager.sharedInstance.error401)

            {

                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)

            }else{
                self.showAlert(ConstantStrings.Error_msg_generic)
                

            }
        })

    }
    
    
}
extension PortfolioCompositionVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if portfolioCompositionModel == nil {
            return 0
        }else {
            if section == 0 {
                return 1
            }else {
                return 1 + portfolioCompositionModel.data[section - 1].companies.count
            }
        }
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if portfolioCompositionModel == nil {
            return 0
        }else {
            return 1 + portfolioCompositionModel.data.count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PieChartCell") as! PieChartCell
            populatePieChart(pieChartView: cell.pieChartView)
            return cell
        }
        
        else {
            let parentIndex = indexPath.section - 1
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "DataCell1") as! DataCell1
                let parent = portfolioCompositionModel.data[parentIndex]
                cell.headingLbl?.text = parent.industryType + " (\(parent.companies.count))"
                cell.percentageLbl?.text = FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: parent.industryShare) + "%"
                cell.legendColor.backgroundColor = FintechUtils.sharedInstance.getColoredBackgroundPortfolioComposition(rowPosition: parentIndex)
                return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "DataCell2") as! DataCell2
       
                let index = indexPath.row - 1
                let child = portfolioCompositionModel.data[parentIndex].companies[index]
                cell.detailLbl1?.text = child.companyName
                cell.tickerPercentage?.text = FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: child.companyShare) + "%"

                return cell
            }
        }
        return UITableViewCell()
    }
    
    
}

extension PortfolioCompositionVC: ChartViewDelegate {
    
    func populatePieChart(pieChartView: PieChartView) {
        
        var colors: [UIColor] = []
      
      // 1. Set ChartDataEntry
      var dataEntries: [ChartDataEntry] = []
        for i in 0..<portfolioCompositionModel.data.count {
            let item = portfolioCompositionModel.data[i]
            let dataEntry = PieChartDataEntry(value: item.industryShare, label: item.industryType, data: item.industryType as AnyObject)
            dataEntries.append(dataEntry)
            colors.append(FintechUtils.sharedInstance.getColoredBackgroundPortfolioComposition(rowPosition: i))
            chartValuesSum += item.industryShare
      }
      // 2. Set ChartDataSet
        let pieChartDataSet = PieChartDataSet(entries: dataEntries, label: nil)
        pieChartDataSet.colors = colors
      // 3. Set ChartData
      let pieChartData = PieChartData(dataSet: pieChartDataSet)
      let format = NumberFormatter()
        format.numberStyle = .percent
        format.maximumFractionDigits = 1
        format.minimumFractionDigits = 1
        format.decimalSeparator = "."
        format.groupingSeparator = .none
        format.multiplier = 1.0
        
//        format.numberStyle = .percent
//        format.maximumFractionDigits = 1
//        format.locale = .current
      let formatter = DefaultValueFormatter(formatter: format)
      pieChartData.setValueFormatter(formatter)
      // 4. Assign it to the chart’s data
      pieChartView.data = pieChartData
        pieChartView.drawEntryLabelsEnabled = false
        pieChartView.legend.enabled = false
        pieChartDataSet.drawValuesEnabled = false
        
        pieChartView.highlightValue(Highlight.init(x: 0, dataSetIndex: 0, stackIndex: 0))
        if pieChartView.highlighted.count > 0 {
            let text = portfolioCompositionModel.data[0].industryType + "\n" + FintechUtils.sharedInstance.doubleToPercentageOutput(doubleVal: portfolioCompositionModel.data[0].industryShare) + "%"
            pieChartView.centerAttributedText = getAttributedCenterText(text: text)
            //pieChartView.centerText = spendingCategory[pieChartView.highlighted[0].dataSetIndex].categoryName
        }
        pieChartView.delegate = self
    }
    
    func getAttributedCenterText(text: String) -> NSAttributedString{
        let style = NSMutableParagraphStyle()
        style.alignment = NSTextAlignment.center
        let chartAttribute = [ NSAttributedString.Key.font: UIFont(name: "HelveticaNeue", size: 15.0)!,
                               .paragraphStyle: style]
        let chartAttrString = NSAttributedString(string: text, attributes: chartAttribute)
        
        return chartAttrString
    }
    
    func chartValueSelected(_ chartView: ChartViewBase, entry: ChartDataEntry, highlight: Highlight) {
        let chartView = chartView as! PieChartView
        chartView.highlightValue(highlight)
        if chartView.highlighted.count > 0 {
            let pieChartEntry = entry as! PieChartDataEntry
            let text = pieChartEntry.label! + "\n" + FintechUtils.sharedInstance.doubleToPercentageOutput(doubleVal: (pieChartEntry.value / chartValuesSum)*100) + "%"
            chartView.centerAttributedText = getAttributedCenterText(text: text)
        }
    }

}

