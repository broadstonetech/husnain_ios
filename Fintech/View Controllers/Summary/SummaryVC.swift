//
//  SummaryVC.swift
//  Fintech
//
//  Created by apple on 23/03/2020.
//  Copyright © 2020 Broadstone Technologies. All rights reserved.
//

import UIKit
var count:Int = 1
var pcount = ""

var SelectedQuestionNO:Int = -1
//let defaults = UserDefaults.standard
var summaryFlag = false
//var cn :[String]=[String]()
class SummaryVC: Parent {
    var personalList = [summary]()
    var investemenList = [summary]()
    var valuesList = [summary]()
    var riskList = [summary]()
    var categories = ["Demographics","Investment","Risk","Values"]
    //var categories = ["Personal details","Investment related","Values","Risk assessment"]
    var answersArray:[String] = []
    @IBOutlet weak var tableview: UITableView!
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    override func viewDidLoad() {
        self.navigationController?.isNavigationBarHidden = true
        
        
        for sum in summaryList{
            if sum.catogries == 1 {
                personalList.append(sum)
            }else
                if sum.catogries == 2 {
                    investemenList.append(sum)
                }else
                    if sum.catogries == 3 {
                        valuesList.append(sum)
                    }else
                        if sum.catogries == 4 {
                            riskList.append(sum)
            }
        }
        print(answersArray)
        tableview.reloadData()
        super.viewDidLoad()
        tableview.reloadData()
    }
    
    @IBAction func submit(){
//        if let tabbedbar = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "nav_bar_start") as? UINavigationController {
//            self.view.window?.rootViewController = tabbedbar
//        }
        //submitquestionapicall()
        openBottomActionSheet()
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    func submitQuestinnaire() {
        count = count+1
        self.view.activityStartAnimating()
        let url = ApiUrl.SUBMIT_QUESTIONNAIRE
        let namespace = SPManager.sharedInstance.getCurrentUserNamespace()
        let answersDict = HomeSurveyVC.conertToDictionary(selectedHost: ArrayLists.sharedInstance.ListOfSubmittedAnswers)
        
        let params:Dictionary<String,AnyObject> = ["namespace" : namespace as AnyObject, "data" : answersDict as AnyObject ]
        print(params)
        APIManager.sharedInstance.postRequest(serviceName: url, sendData: params, success: { (response) in
            print(response)
            if APIManager.sharedInstance.isSuccess {
                self.saveUserData(value: summaryList, key: SPManager.sharedInstance.getCurrentUserEmail())
                let res = response["message"] as AnyObject
                SPManager.sharedInstance.saveUserPortfolioStatus(isPortfolioComplete : true)
                DispatchQueue.main.async {
                    ArrayLists.sharedInstance.ListOfSubmittedAnswers.removeAll()
                    if let tabbedbar = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "nav_bar_start") as? UINavigationController {
                        self.view.window?.rootViewController = tabbedbar
                    }
                }
                //move to portfolio vc
                
            } else {
                if response["message"] != nil {
                    DispatchQueue.main.async {
                        self.showAlert(response["message"] as! String)
                    }
                } else {
                    DispatchQueue.main.async {
                        self.showAlert(ConstantStrings.REQUEST_SUPPORT_FAILURE_ALERT_TEXT)
                    }
                }
            }
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
            
            self.setAnalyticsAction(ScreenName: "SummaryVC", method: "QuestinnaireSubmited")
            
        }, failure: { (data) in
            print(data)
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
            if APIManager.sharedInstance.error400 {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
            }
            if(APIManager.sharedInstance.error401)
            {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
            }else{
                
            }
        })
        
    }
    
    
//    func submitquestionapicall(){
//        let url = ApiUrl.SUBMIT_QUESTIONNAIRE
//        let namespace = SPManager.sharedInstance.getCurrentUserNamespace()
//        //let namespace = "uk-DdZ0"
//        print(namespace)
//        postManagerUser(method: "POST", isThisURLEmbdedData: false, urlString: url, parameter: ["namespace": namespace,"data": ArrayLists.sharedInstance.SELECTED_ANSWERS_DICT], success: {(value) in
//            do
//            {
//                let jsonObject = try JSONDecoder().decode(questanswer?.self, from: value)
//                print(jsonObject)
//
//                DispatchQueue.main.async {
//
//                    ArrayLists.sharedInstance.SELECTED_ANSWERS_DICT.removeAll()
//                    if let tabbedbar = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "nav_bar_start") as? UINavigationController {
//                        self.view.window?.rootViewController = tabbedbar
//                    }
//                }
//
//            }
//            catch
//            {
//                print("aqib")
//            }
//
//        }, failure: {(err) in
//            DispatchQueue.main.async {
//                //self.btnEnable = true
//                // self.POPUp(message: "Something went wrong", time: 1.8)
//            }
//            if APIManager.sharedInstance.error400{
//                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
//            }
//            if(APIManager.sharedInstance.error401)
//            {
//                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
//            }else{
//            }
//        })
//    }
    
    
    
    
    func apiupdateFuncation(portfolioType: String){

            data = self.getUserData(key: SPManager.sharedInstance.getCurrentUserEmail()) ?? [summary]()

        var demographicIndex = 1, investmentIndex = 1, riskIndex = 1, valuesIndex = 1
        
            for sum in data{

                //let item = SubmitQDataModel()

                var questionno = ""

                var cate = ""

                let answer = sum.answertext

                //item.question_id =

                print(sum)

                if sum.catogries == 1 {

                    // let model = SubmitQDataModel()

                    //questionno = "qd\(sum.questionNo!)"
                    questionno = "qd\(demographicIndex)"
                    demographicIndex += 1
                    cate = "Demographics"

                    

                    print("qd\(sum.questionNo!)")

                    print(sum.answertext!)

                    print("demographics")

                    // model.answerid = [sum.answertext!]

                    

                } else

                    if sum.catogries == 2 {

                        //questionno = "qi\(sum.questionNo!)"
                        questionno = "qi\(investmentIndex)"
                        investmentIndex += 1

                        cate = "Investment"

                        print("qi\(sum.questionNo!)")

                        print(sum.answertext!)

                        print("investment")

                    }else

                        if sum.catogries == 3 {

                            //questionno = "qr\(sum.questionNo!)"
                            questionno = "qr\(riskIndex)"
                            riskIndex += 1
                            cate = "Risk"

                            

                            print("qr\(sum.questionNo!)")

                            print(sum.answertext!)

                            print("risk")

                        }else

                            if sum.catogries == 4 {

                                //questionno = "qv\(sum.questionNo!)"
                                questionno = "qv\(valuesIndex)"
                                valuesIndex += 1
                                

                                cate = "Values"

                                

                                print("qv\(sum.questionNo!)")

                                print(sum.answertext!)

                                print("values")

                                

                }

                var array = [String]()

                for i in  answer!.components(separatedBy: ",")

                {

                    let last = i.last

                    array.append("\(last ?? "a")")

                }

                //answer.components(separatedBy: ",")

                

                answelists.append(SubmitQDataModel(qid: questionno,aid: array,cid: cate))

                print(array)

                

            }

            for i in answelists {

                let questionNo = i.question_id

                let answersId = i.answer_id

                let categoryId = i.category_id

                var dic = [String : Any]()

                dic["question_id"] = questionNo as String

                dic["answer_id"] =  answersId as [String]

                dic["category_id"] = categoryId as String

                SELECTED_ANSWERS_DICT.append(dic)

                

            }

            print(SELECTED_ANSWERS_DICT)

            submitquestionapicall(typeOption: portfolioType)

           

            

            

            

        }

        class func conertToDictionary(selectedHost : [QuestionnaireModel]) -> [[String: Int]] {

            var list = [[String: Int]]()

            

            for e in selectedHost {

                var dic = [String : Int]()

                dic["question_id"] = e.getQuestionID()

                dic["category_id"] = e.getCategoryId()

                dic["answer_id"] = e.getAnswerId()

                

                list.append(dic)

                

            }

            return list

        }
    
    
    

}

extension SummaryVC : UITableViewDelegate,UITableViewDataSource{

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
                  return categories.count

    }


    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
 func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "sHeadCell") as! SummaryHeadCell
        var data =  [summary]()
        if  indexPath.row == 0{
           let color1 = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
            cell.setColor(color: color1)
            for v in cell.stackView.subviews{
                v.removeFromSuperview()
            }
            for item in personalList {
                    let view:SummaryView = UIView.fromNib()
                    view.Answer_lbl.text = self.getAnswer(answer: item.answertext ?? "")
                    view.Question_lbl.text =  getLocalizedString(key: item.questiontext ?? "", comment: "")
                    view.button.tag = item.questionNo ?? -1
                    view.button.addTarget(self, action: #selector(self.editPress(sender:)), for: .touchUpInside)
                    view.setColor(color: color1)
                    cell.stackView.addArrangedSubview(view)
                }
            cell.name_lbl.text = getLocalizedString(key: self.categories[indexPath.row], comment: "")
                return cell
         }
         else if  indexPath.row == 1{
            let color2 = #colorLiteral(red: 0.9411764741, green: 0.4980392158, blue: 0.3529411852, alpha: 1)
            cell.setColor(color: color2)

            for v in cell.stackView.subviews{
                v.removeFromSuperview()
            }
            for item in investemenList {
                    let view:SummaryView = UIView.fromNib()
                    view.Answer_lbl.text = self.getAnswer(answer: item.answertext ?? "")
                    view.Question_lbl.text =  getLocalizedString(key: item.questiontext ?? "", comment: "")
                    view.button.tag = item.questionNo ?? -1
                    view.button.addTarget(self, action: #selector(self.editPress(sender:)), for: .touchUpInside)
                    view.setColor(color: color2)
                    cell.stackView.addArrangedSubview(view)
                }
            cell.name_lbl.text = getLocalizedString(key: self.categories[indexPath.row], comment: "")
                return cell
         }
         else if  indexPath.row == 2{
            
              let color3 = #colorLiteral(red: 0.1764705926, green: 0.4980392158, blue: 0.7568627596, alpha: 1)
            cell.setColor(color: color3)

            for v in cell.stackView.subviews{
                v.removeFromSuperview()
            }
            for item in valuesList {
                    let view:SummaryView = UIView.fromNib()
                    view.Answer_lbl.text = self.getAnswer(answer: item.answertext ?? "")
                    view.Question_lbl.text =  getLocalizedString(key: item.questiontext ?? "", comment: "")
                    view.button.tag = item.questionNo ?? -1
                    view.button.addTarget(self, action: #selector(self.editPress(sender:)), for: .touchUpInside)
                    view.setColor(color: color3)
                    cell.stackView.addArrangedSubview(view)
                }
            cell.name_lbl.text = getLocalizedString(key: self.categories[indexPath.row], comment: "")
                return cell
         }
        else if  indexPath.row == 3{
             let color4 = #colorLiteral(red: 0.5843137503, green: 0.8235294223, blue: 0.4196078479, alpha: 1)
            cell.setColor(color: color4)

            for v in cell.stackView.subviews{
                v.removeFromSuperview()
            }
            for item in riskList {
                    let view:SummaryView = UIView.fromNib()
                    view.Answer_lbl.text = self.getAnswer(answer: item.answertext ?? "")
                    view.Question_lbl.text =  getLocalizedString(key: item.questiontext ?? "", comment: "")
                    view.button.tag = item.questionNo ?? -1
                    view.button.addTarget(self, action: #selector(self.editPress(sender:)), for: .touchUpInside)
                    view.setColor(color: color4)
                    cell.stackView.addArrangedSubview(view)
                }
            cell.name_lbl.text = getLocalizedString(key: self.categories[indexPath.row], comment: "")
                return cell
         }

    return UITableViewCell()
   
 }

    func getAnswer(answer : String)-> String{
        let array = answer.split(separator: ",")
                var finalAnswer = [String]()
                for ans in array{
                    let str = getLocalizedString(key: String(ans), comment: "")
                    finalAnswer.append(str)
                }
        return finalAnswer.joined(separator: ",")
    }
   @objc func editPress(sender: UIButton){
    SelectedQuestionNO =  summaryList.firstIndex(where: {$0.questionNo == sender.tag}) ?? -1
        if summaryFlag{
            summaryFlag = false
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeSurveyVC") as! HomeSurveyVC
            self.navigationController?.pushViewController(vc, animated: true)
            //pushToViewController(vc, animated: true)
            // UIApplication.shared.keyWindow?.rootViewController = vc

        }else{
          self.navigationController?.popViewController(animated: true)
        }
    }

}


extension UIView {
    class func fromNib<T: UIView>() -> T {
        return Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
}

extension SummaryVC{

    func submitquestionapicall(typeOption: String){
        view.activityStartAnimating()
        let url = ApiUrl.SUBMIT_QUESTIONNAIRE

        let namespace = SPManager.sharedInstance.getCurrentUserNamespace()

        //let namespace = "uk-DdZ0"

        print(namespace)

        postManagerUser(method: "POST", isThisURLEmbdedData: false, urlString: url, parameter: ["namespace": namespace,"type": typeOption,"data": SELECTED_ANSWERS_DICT], success: {(value) in

            do

            {

                let jsonObject = try JSONDecoder().decode(questanswer?.self, from: value)

                print(jsonObject)

                DispatchQueue.main.async {
                    ArrayLists.sharedInstance.SELECTED_ANSWERS_DICT.removeAll()
                    if let tabbedbar = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "nav_bar_start") as? UINavigationController {
                            self.view.window?.rootViewController = tabbedbar
                        }
                }

            }

            catch

            {

                print("aqib")

            }
            
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }

            

        }, failure: {(err) in

            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }

            if APIManager.sharedInstance.error400{

                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)

            }

            if(APIManager.sharedInstance.error401)

            {

                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)

            }else{
                self.showAlert(ConstantStrings.Error_msg_generic)

            }

        })

    }


 

}

struct questanswer: Codable{

    let message : String?


}


extension SummaryVC{
    func openBottomActionSheet() {
        // if NetworkHelper.sharedInstance.isDmoatOnline {
        let optionMenu = UIAlertController(title: nil, message: "Select portfolio type", preferredStyle: .actionSheet)
        
        let stocks = UIAlertAction(title: "Stocks only", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            self.apiupdateFuncation(portfolioType: "stocks")
            
        })
        let etf = UIAlertAction(title: "ETFs only", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            self.apiupdateFuncation(portfolioType: "etf")
            
        })
        
        let vesgo = UIAlertAction(title: "Let Vesgo decide", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            
            self.apiupdateFuncation(portfolioType: "both")
            
        })
        
        
        
        
        optionMenu.addAction(stocks)
        optionMenu.addAction(etf)
        optionMenu.addAction(vesgo)
        self.present(optionMenu, animated: true, completion: nil)
    }
}
