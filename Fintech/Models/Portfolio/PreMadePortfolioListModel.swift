//
//  PreMadePortfolioListModel.swift
//  Fintech
//
//  Created by broadstone on 04/03/2022.
//  Copyright © 2022 Broadstone Technologies. All rights reserved.
//

import Foundation

struct PreMadePortfolioListModel {
    let success: Bool
    let message: String
    let data: [PreMadePortfolioListData]
}

struct PreMadePortfolioListData {
    let portfolioId: Int
    let portfolioName: String
    let overAllZScore: Double
    let envZScore: Double
    let socZScore: Double
    let govZScore: Double
}
