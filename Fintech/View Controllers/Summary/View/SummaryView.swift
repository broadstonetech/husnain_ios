//
//  SummaryView.swift
//  Fintech
//
//  Created by apple on 17/05/2020.
//  Copyright © 2020 Broadstone Technologies. All rights reserved.
//

import UIKit

class SummaryView: UIView {
    @IBOutlet weak var Question_lbl: UILabel!
    @IBOutlet weak var Answer_lbl: UILabel!
    @IBOutlet weak var backView :UIView!
    @IBOutlet weak var button :UIButton!
    var section : Int!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    func setColor(color : UIColor){
        backView.borderColor =  color
    }

}
