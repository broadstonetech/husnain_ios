//
//  PortfolioDetailsCell.swift
//  Fintech
//
//  Created by MacBook-Mubashar on 29/08/2019.
//  Copyright © 2019 Broadstone Technologies. All rights reserved.
//

import UIKit

class PortfolioDetailsCell: UITableViewCell {

    @IBOutlet var subHeading_lbl: UILabel!
    @IBOutlet var mainHeading_lbl: UILabel!
    @IBOutlet var description_lbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
