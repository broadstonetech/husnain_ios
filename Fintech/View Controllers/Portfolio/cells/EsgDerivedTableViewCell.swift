//
//  EsgDerivedTableViewCell.swift
//  Fintech
//
//  Created by Apple on 27/01/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import UIKit

class EsgDerivedTableViewCell: UITableViewCell {
    
    @IBOutlet weak var details1_portfoliocomposition_lbl: UILabel!
    @IBOutlet weak var heading_lbl: UILabel!
    @IBOutlet weak var img_portfoliocomposition_img: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
