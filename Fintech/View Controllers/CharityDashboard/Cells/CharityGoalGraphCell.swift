//
//  CharityGoalGraphCell.swift
//  Fintech
//
//  Created by broadstone on 31/12/2020.
//  Copyright © 2020 Broadstone Technologies. All rights reserved.
//

import Foundation
import UIKit
import RingGraph
class CharityGoalGraphCell: UITableViewCell {
    @IBOutlet weak var goalProgressGraphRing:RingGraphView!
    
    @IBOutlet weak var goalNotSetView: UIView!
    @IBOutlet weak var goalAvailableView: UIView!
    @IBOutlet weak var targetDateView: UIView!
    @IBOutlet weak var setGoalBtn: UIButton!
    @IBOutlet weak var amountSpent: UILabel!
    @IBOutlet weak var totalAmount: UILabel!
    
    @IBOutlet weak var targetData: UILabel!
    override class func awakeFromNib() {
        super.awakeFromNib()
    }
    @IBOutlet weak var amountDonatedLblForGraph: UILabel!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    public func populateMovementsRingCharts(currentValue : Int, maxValue : Int) {
        
        for view in goalProgressGraphRing.subviews {
            view.removeFromSuperview()
        }
        
            if #available(iOS 13.0, *) {
                let ringColor = UIColor(named: "AppMainColor")!
                let singleGraphMeter = [RingMeter(title: "Completed", value: currentValue, maxValue: maxValue, colors: [ringColor],symbolProvider: ImageProvider(image: UIImage(named: "")), backgroundColor : #colorLiteral(red: 0.9411764706, green: 0.9411764706, blue: 0.9411764706, alpha: 1))]

                if let graph = RingGraph(meters: singleGraphMeter) {
                    let viewFrame = goalProgressGraphRing.frame
                    let graphFrame = CGRect(x: 0, y: 0, width: viewFrame.width, height: viewFrame.height)
                    let ringGraphView = RingGraphView(frame: graphFrame, graph: graph, preset: .None)
                    ringGraphView.backgroundColor = .clear
                    goalProgressGraphRing.addSubview(ringGraphView)
                    goalProgressGraphRing.backgroundColor = .clear
                }
            } else {
                // Fallback on earlier versions
            }
    }
}
