//
//  HoldingsDialogView.swift
//  Fintech
//
//  Created by broadstone on 11/02/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import UIKit

class HoldingsDialogView: UIView {
    
    @IBOutlet weak var parentView:UIView!
    @IBOutlet weak var closeBtn: UIButton!
    @IBOutlet weak var tickerName: UILabel!
    @IBOutlet weak var tickerSymbol: UILabel!
    @IBOutlet weak var amountInvestedLbl: UILabel!
    @IBOutlet weak var industryLbl: UILabel!
    @IBOutlet weak var returnLbl: UILabel!
    @IBOutlet weak var returnFreq: UILabel!
    @IBOutlet weak var portfolioWeightLbl: UILabel!
    
    @IBOutlet weak var amountInvestedTxt: UILabel!
    @IBOutlet weak var industryTxt: UILabel!
    @IBOutlet weak var returnTxt: UILabel!
    @IBOutlet weak var portfolioWeightTxt: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    private func commonInit(){
        Bundle.main.loadNibNamed("HoldingsDialogView", owner: self, options: nil)
        addSubview(parentView)
        parentView.frame = self.bounds
        parentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        
        localization()
    }
    
    func localization(){
        amountInvestedTxt.text = getLocalizedString(key: "amount_invested", comment: "")
        industryTxt.text = getLocalizedString(key: "industry_", comment: "")
        returnTxt.text = getLocalizedString(key: "return_txt", comment: "")
        returnFreq.text = getLocalizedString(key: "past_week", comment: "")
        portfolioWeightTxt.text = getLocalizedString(key: "portfolio_weight", comment: "")
        closeBtn.setTitle(getLocalizedString(key: "ok", comment: ""), for: .normal)
    }
    
    
//    @objc func closeBtnTapped(){
//        parentView.removeFromSuperview()
//    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    
    
    
    func show(){
        UIApplication.shared.keyWindow?.addSubview(parentView)
    }
}
