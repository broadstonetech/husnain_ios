//
//  ViewController.swift
//  SecDemo
//
//  Created by SaifUllah Butt on 26/10/2021.
//

import UIKit
import Foundation
import MapKit
struct CellData {
    var opened: Bool
    var companyData: companiesSubmissionDataModel
}

class SecDataVC: Parent, UITextViewDelegate {
    
    var companies = [CellData]()
    
    static var title = companiesSubmissionDataModel()
    
    private var tableViewData = [CellData]()
    
    @IBOutlet weak var card1: UIView!
    @IBOutlet weak var card2: UIView!
    
    @IBOutlet weak var addressView: UIView!
    
    @IBOutlet weak var companyName: UILabel!
    @IBOutlet weak var cik: UILabel!
    @IBOutlet weak var ticker: UILabel!
    @IBOutlet weak var exchange: UILabel!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var location: UILabel!
    
    @IBOutlet weak var factTv: UITableView!
    @IBOutlet weak var locationBtnOutlet: UIButton!
    
    let baseUrl = "https://data.sec.gov/submissions/CIK"
    let baseURLFacts = "https://data.sec.gov/api/xbrl/companyfacts/CIK"
    var CIK = ""
    var comName = ""
    
    var phoneNumber = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        factTv.delegate = self
        factTv.dataSource = self
        print("Cik: \(CIK)")
        callSubmissionApi(CIK: CIK)
        callFactsApi(CIK: CIK)

        card1.isHidden = true
        card2.isHidden = true
        
        let phNumGesture = UITapGestureRecognizer.init(target: self, action: #selector(openPhonePadDialer))
        location.isUserInteractionEnabled = true
        location.addGestureRecognizer(phNumGesture)
        
        let addressGesture = UITapGestureRecognizer.init(target: self, action: #selector(openMap))
        address.isUserInteractionEnabled = true
        address.addGestureRecognizer(addressGesture)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        setNavBarTitile(title: comName, topItem: comName)
    }
    
    @objc func openPhonePadDialer(){
        guard let url = URL(string: "telprompt://\(phoneNumber)"),
            UIApplication.shared.canOpenURL(url) else {
            print("Cannot open phone pad")
            return
        }
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
        
    }
    
    func openMapForPlace(lat:Double = 0, long:Double = 0, placeName:String = "") {
        let latitude: CLLocationDegrees = lat
        let longitude: CLLocationDegrees = long

        let regionDistance:CLLocationDistance = 100
        let coordinates = CLLocationCoordinate2DMake(latitude, longitude)
        let regionSpan = MKCoordinateRegion(center: coordinates, latitudinalMeters: regionDistance, longitudinalMeters: regionDistance)
        let options = [
            MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
            MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
        ]
        let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = placeName
        mapItem.openInMaps(launchOptions: options)
    }
    
    @objc func openMap(){
        coordinates(forAddress: address.text!) {
            (location) in
            guard let location = location else {
                print("Location not found")
                return
            }
            self.openMapForPlace(lat: location.latitude, long: location.longitude)
        }
        
        setAnalyticsAction(ScreenName: "SecDataVC", method: "OpenMap")
        
    }
    
    func coordinates(forAddress address: String, completion: @escaping (CLLocationCoordinate2D?) -> Void) {
        let geocoder = CLGeocoder()
        geocoder.geocodeAddressString(address) {
            (placemarks, error) in
            guard error == nil else {
                print("Geocoding error: \(error!)")
                completion(nil)
                return
            }
            completion(placemarks?.first?.location?.coordinate)
        }
    }
    

    
    func callSubmissionApi (CIK: String) {
        
        let url = baseUrl+CIK+".json"
        getRequest(url: url, CIK: CIK) { data in
            //  print("the required data is \(data)")
            
            let companyname = data["name"] as! String
            let cik = data["cik"] as! String
            let phone = (data["phone"] as? String) ?? "--"
            var ticker = (data["tickers"] as? [String]) ?? [String](arrayLiteral: "--")
            if ticker.isEmpty {
                ticker.append("--")
            }
            var exchange = (data["exchanges"] as? [String]) ?? [String](arrayLiteral: "--")
            if exchange.isEmpty {
                exchange.append("--")
            }
            let addresses = data["addresses"] as? [String: AnyObject]
            let mailingAdress = addresses?["business"] as? [String: AnyObject]
            var address = (mailingAdress?["street1"] as? String) ?? ""
            if address != "" {
               address = "\(address),"
            }
            var street2 = (mailingAdress?["street2"] as? String) ?? ""
            if street2 != "" {
                street2 = " \(street2),"
            }
            var city = (mailingAdress?["city"] as? String) ?? ""
            if city != "" {
                city = " \(city),"
            }
            
            var zip = (mailingAdress?["zipCode"] as? String) ?? ""
            if zip != "" {
                zip = " \(zip),"
            }
            
            var country = (mailingAdress?["stateOrCountryDescription"] as? String) ?? ""
            if country != "" {
                country = " \(country)."
            }
            
            let businessAddress = address + street2 + city + zip + country
            
            
            let totalData = SubmissionModel.init(companyName: companyname, cik: cik, ticker: ticker, exchange: exchange, address: businessAddress, number: phone)
            DispatchQueue.main.async {
                self.phoneNumber = totalData.number
                self.card1.isHidden = false
                self.companyName.text = totalData.companyName
                self.cik.text = totalData.cik
                self.ticker.text = (totalData.ticker[0]) ?? "--"
                self.exchange.text = (totalData.exchange[0]) ?? "--"
                self.address.text = totalData.address
                self.location.text = totalData.number.applyPatternOnNumbers(pattern: "(###) ###-####", replacementCharacter: "#")
                self.card1.isHidden = false
            }
        
        
        } failure: { error in
            print("the req error is \(error)")
        }
    }
    
    func callFactsApi(CIK: String) {
        self.view.activityStartAnimating()
        let url = baseURLFacts+CIK+".json"
        getRequest(url: url, CIK: CIK) { data in
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
            self.companies.removeAll()
            let facts = data["facts"] as! [String:AnyObject]
            let gaap = facts["us-gaap"] as! [String: AnyObject]
            for each in gaap{
                self.companies.append(CellData.init(opened: false, companyData:  self.fetchCompanyDetails(res: gaap[each.key] as! [String: AnyObject])))
            }
            DispatchQueue.main.async {
                self.factTv.reloadData()
                self.card2.isHidden = false
            }
        } failure: { data in
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
            print(data)
        }
    }
    
    func fetchCompanyDetails(res: [String: AnyObject]) -> companiesSubmissionDataModel {
        let companyTitle = (res["label"] as? String) ?? ""
        let units = res["units"] as! [String: AnyObject]
        if var usdUnits = units["USD"] as? [AnyObject]{
            var compainesUnits = [CompaniesUnit]()
            usdUnits = usdUnits.reversed()
            //  for i in 0..<usdUnits.count
            if(usdUnits.count > 3){
                for i in 0..<4 {
                    let item = usdUnits[i] as! [String: AnyObject]
                    let strValue = FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: (item["val"] as? Double) ?? 0.0)
                    let value = ("$\(strValue)")
                    let fy = item["fy"] as! Int
                    let fp = item["fp"] as! String
                    compainesUnits.append(CompaniesUnit.init(value: value, fiscalYear: fy, fiscalPeriod: fp))
                }
            }
            else{
                for i in 0..<usdUnits.count {
                let item = usdUnits[i] as! [String: AnyObject]
                    let strValue = FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: (item["val"] as? Double) ?? 0.0)
                let value = ("$\(strValue)")
                let fy = item["fy"] as! Int
                let fp = item["fp"] as! String
                compainesUnits.append(CompaniesUnit.init(value: value, fiscalYear: fy, fiscalPeriod: fp))
               }
            }
            let companiesModel = companiesSubmissionDataModel.init(title: companyTitle, units: compainesUnits)
          //  companiesModel.units = companiesModel.units.reversed()
            return companiesModel
        }
        else{
            var shares = (units["shares"] as? [AnyObject]) ?? []
            var compainesUnits = [CompaniesUnit]()
            shares = shares.reversed()
            //  for i in 0..<usdUnits.count
            if(shares.count > 3){
                for i in 0..<4 {
                    let item = shares[i] as! [String: AnyObject]
                    let strValue = FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: (item["val"] as? Double) ?? 0.0)
                    let value = ("\(strValue)")
                    let fy = item["fy"] as! Int
                    let fp = item["fp"] as! String
                    compainesUnits.append(CompaniesUnit.init(value: value, fiscalYear: fy, fiscalPeriod: fp))
                }
            }
            else{
                for i in 0..<shares.count {
                let item = shares[i] as! [String: AnyObject]
                    let strValue = FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: (item["val"] as? Double) ?? 0.0)
                let value = ("\(strValue)")
                let fy = item["fy"] as! Int
                let fp = item["fp"] as! String
                compainesUnits.append(CompaniesUnit.init(value: value, fiscalYear: fy, fiscalPeriod: fp))
               }
            }
            let companiesModel = companiesSubmissionDataModel.init(title: companyTitle, units: compainesUnits)
            return companiesModel
        }
    }
    
    
    func getRequest(url:String, CIK: String, success:@escaping ( _ data : [String : AnyObject])->Void, failure:@escaping ( _ data:[String : AnyObject])->Void) {
        //create the session object
        let session = URLSession.shared
        //now create the NSMutableRequest object using the url object
        var request = URLRequest(url: URL(string: url)!)
        request.httpMethod = "GET" //set http method as POST
        //        request.timeoutInterval = 20
        //HTTP Headers
        request.addValue("Mozilla/5.0", forHTTPHeaderField: "User-Agent")
        request.addValue("application/geo+json;version=1", forHTTPHeaderField: "Accept")
        //create dataTask using the session object to send data to the server
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            guard error == nil else {return}
            guard let data = data else {return}
            do {
                //create json object from data
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: AnyObject] {
                    success(json)
                    //handle json...
                    //hide activity indicator
                }
                //hide activity indicator
            } catch let error {
                //hide activity indicator
                print(error.localizedDescription)
                var errors = [String : AnyObject]()
                errors["Error"] = "Server Error" as AnyObject
                failure(errors)
            }
        })
        task.resume()
    }
}

extension SecDataVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return companies.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if companies[section].opened == true{
            return 1 + companies[section].companyData.units.count
        }else {
            return 1
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            let cell = factTv.dequeueReusableCell(withIdentifier: "NameCell", for: indexPath) as! TableViewCellA
            cell.textLabel?.numberOfLines = 0
        //    cell.textLabel?.text = companies[indexPath.section].companyData.title
            cell.companiesTitleOutlet.text = companies[indexPath.section].companyData.title
            
            return cell
        }
        else{
            let index = indexPath.row - 1
            let cell = factTv.dequeueReusableCell(withIdentifier: "EntriesCell", for: indexPath) as! EntriesCell
            // cell.textLabel?.text = String(companies[indexPath.section].companyData.units[index].value)
            cell.value.text = String(companies[indexPath.section].companyData.units[index].value)
            cell.year.text = String(companies[indexPath.section].companyData.units[index].fiscalYear) + "," + String(companies[indexPath.section].companyData.units[index].fiscalPeriod)
            
            
            return cell
        }
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            if companies[indexPath.section].opened == true{
                companies[indexPath.section].opened = false
                let sections = IndexSet.init(integer: indexPath.section)
                tableView.reloadSections(sections, with: .none)
            }else{
                companies[indexPath.section].opened = true
                let sections = IndexSet.init(integer: indexPath.section)
                tableView.reloadSections(sections, with: .none)
            }
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}


