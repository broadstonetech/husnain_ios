//
//  VerifyPasswordTokenVC.swift
//  Fintech
//
//  Created by MacBook-Mubashar on 26/08/2019.
//  Copyright © 2019 Broadstone Technologies. All rights reserved.
//

import UIKit
import SwiftyJSON
import Lottie
class VerifyPasswordTokenVC: Parent {
    @IBOutlet weak var animation_img_view: AnimationView!
    
    @IBOutlet var descriptiontext_lbl: UILabel!
    @IBOutlet var token_tf: UITextField!
    @IBOutlet var submit_btn: UIButton!
    
    @IBOutlet weak var heading_des: UILabel!
    var descriptionText : String = ""
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        animation_img_view.contentMode = .scaleAspectFit
        animation_img_view.animationSpeed = 0.5
        animation_img_view.play()
        token_tf.delegate = self
        //IB actions
        submit_btn.addTarget(self, action: #selector(callVerifyToken), for: UIControl.Event.touchUpInside)
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //descriptiontext_lbl.text = descriptionText
        localization()
        
    }
    
    
    func localization(){
        submit_btn.setTitle(getLocalizedString(key: "Verify", comment: ""), for: .normal)
        let heading = getLocalizedString(key: "VERIFY CODE", comment: "")
        let descripation = getLocalizedString(key: "VerifyviewDes", comment: "")
        
        descriptiontext_lbl.text = heading
        heading_des.text = descripation
        token_tf.placeholder = getLocalizedString(key: "enter_code", comment: "")
        
    }
    
    
    //MARK: - Controlling the Keyboard
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == token_tf {
            token_tf.resignFirstResponder()
        } else{
            textField.resignFirstResponder()
            
        }
        return true
    }
    
    // MARK: - IB Actions
    @objc func callVerifyToken() {
        if checkFormInputs() {
            DispatchQueue.main.async {
                
                if NetworkReachability.isConnectedToNetwork() {
                    DispatchQueue.main.async {
                    self.callVerifyTokenApi()
                    }
                    
                } else {
                    self.showAlert(ConstantStrings.NO_INTERNET_AVAILABLE)
                }
            }
        }
    }
    
    @IBAction func cancelPressed(_ sender: UIButton) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "Forgot") as!ForgotPasswordVC
        vc.modalPresentationStyle = .fullScreen
        UIApplication.shared.keyWindow?.rootViewController = vc
    }
    
    func checkFormInputs() -> Bool {
        guard let token = token_tf.text, !token.isEmpty else {
            showAlert(ConstantStrings.token_empty)
            return false
        }
        
        return true
    }
    
    // MARK: - API call--SignIn
    func callVerifyTokenApi() {
        self.view.activityStartAnimating()
        let url = ApiUrl.FORGOT_PASSWORD_VERIFY_TOKEN_URL
        let data:Dictionary<String,AnyObject> = ["username": SPManager.sharedInstance.getCurrentUserUsername() as AnyObject, "token" : token_tf.text as AnyObject]
        
        let namespace = SPManager.sharedInstance.getCurrentUserNamespace()
        let params:Dictionary<String,AnyObject> = ["data" : data as AnyObject, "namespace" : namespace as AnyObject]
        print(data)
        APIManager.sharedInstance.postRequest(serviceName: url, sendData: params, success: { (response) in
            print(response)
            
            if APIManager.sharedInstance.isSuccess {
                
                DispatchQueue.main.async {
                    let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)

                    let vc = storyBoard.instantiateViewController(withIdentifier: "ChangePasswordVC") as! ChangePasswordVC
                    vc.isFromForgotpassword = true
                    self.present(vc, animated:true, completion:nil)
                }
                
            }else{
                DispatchQueue.main.async {
                if response["message"] as AnyObject != nil {
                    self.showAlert(response["message"] as! String)
                }else {
                    self.showAlert(ConstantStrings.Error_msg_generic)
                }
                }
            }
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
        }, failure: { (data) in
            print(data)
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
            if APIManager.sharedInstance.error400 {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
            }
            if(APIManager.sharedInstance.error401)
            {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
            }else{
                
            }
        })
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        token_tf.resignFirstResponder()
        
    }
    
}
