//
//  CorrelationVC.swift
//  Fintech
//
//  Created by broadstone on 27/08/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import UIKit

class CorrelationVC: BudgetingParent {
    
    @IBOutlet weak var correlationView: CorelationDialogView!
    
    var corelationCategoryName: String!
    var corelationModel: SpendingCorelationModel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        correlationView.corelationModel = corelationModel
        correlationView.corelationModel.data.sort {
                $0.value > $1.value
        }
        correlationView.setCategoryName(name: corelationCategoryName)
        correlationView.setViewValues()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        setNavBarTitile(title: "Back", topItem: getLocalizedString(key: "spending_correlation"))
    }
    

}
