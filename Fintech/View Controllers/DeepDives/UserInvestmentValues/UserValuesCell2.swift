//
//  UserValuesCell2.swift
//  Vesgo-PF
//
//  Created by Billal on 02/02/2021.
//

import UIKit

class UserValuesCell2: UITableViewCell {

    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var headingLbl: UILabel!
    @IBOutlet weak var impactLbl: UILabel!
    @IBOutlet weak var parentView: UIView?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
