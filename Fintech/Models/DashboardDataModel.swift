//
//  DashboardDataModel.swift
//  Fintech
//
//  Created by Aqib on 15/09/2020.
//  Copyright © 2020 Broadstone Technologies. All rights reserved.
//

//import UIKit
import Foundation

class DashboardDataModel: NSObject {

    fileprivate var agegrouptext : String
    fileprivate var gendertext : String
    fileprivate var maritalstatus : String
    fileprivate var score : String
    fileprivate var percentile : Double
    fileprivate var value : String
    fileprivate var scoremeantext : String
    fileprivate var scorecalculation : String
    fileprivate var scoreimprovement : String
    fileprivate var risksocre : Int
    fileprivate var returns : String
    fileprivate var userkeyface : String
    fileprivate var risk : Double
    fileprivate var amount: String

     var positivevalues  = [String]()
     var negativevalues  = [String]()

    
       override init() {
        risk =  -0.0
        returns = ""
        userkeyface = ""
        agegrouptext = ""
        gendertext = ""
        maritalstatus = ""
       score =  ""
        risksocre = -1
        percentile = -1.0
        value = ""
        scoremeantext = ""
        scorecalculation = ""
        scoreimprovement = ""
        amount = ""
    }
    func setrisk(_ text: Double){
        self.risk = text
    }
    func  getrisk() ->Double {
        return self.risk
    }
    //
    func setscore(_ text: String){
           self.score = text
       }
    func  getscore() ->String {
           return self.score
       }
    //
    func setriskscore(_ text: Int){
     self.risksocre = text
       }
    func  getrisksocre() ->Int {
     return Int(self.risksocre)
       }
    //
    func setpercentile(_ text: Double){
        self.percentile = text
          }
       func  getpercentile() ->Double {
        return self.percentile
          }
    //
    func setmaritalstatus(_ text: String){
           self.maritalstatus = text
       }
    func  getmaritalstatus() ->String {
           return self.maritalstatus
       }
    //
    func setagegrouptext(_ text: String){
        self.agegrouptext = text
    }
    func getagegrouptext() ->String {
           return self.agegrouptext
       }
    //
    func setvalue(_ text: String){
        self.value = text
    }
    func getvalue() ->String {
        return self.value
    }
    //
    func setscoremeantext(_ text: String){
        self.scoremeantext = text
    }
    func getscoremeantext() ->String {
           return self.scoremeantext
       }
    //
    func setscorecalculation(_ text: String){
        self.scorecalculation = text
    }
    func getscorecalculation() ->String {
        return self.scorecalculation
    }
    //
    func setscoreimprovement(_ text: String){
        self.scoreimprovement = text
    }
    func getscoreimprovement() ->String {
        return self.scoreimprovement
    }
  //
    
    func setreturs(_ text: String){
        self.returns = text
    }
    func  getreturnss() ->String {
        return self.returns
    }
    //
    func setuerkeyfacevslues(_ text: String){
        self.userkeyface = text
    }
    func  getuserkeyface() ->String {
        return self.userkeyface
    }
    
    func setPortfolioAmount(amount: String){
        self.amount = amount
    }
    
    func getPortfolioAmount() -> String {
        return self.amount
    }
    
//    func setpositivevalues(_ text: String){
//          self.positivevalues = text
//      }
//      func getpositivevalues() ->String {
//          return self.positivevalues
//      }
//    //
//    func setnegativevalues(_ text: String){
//        self.negativevalues = text
//    }
//    func getnegativevalues() ->String {
//        return self.negativevalues
//    }
    
    
    
}
