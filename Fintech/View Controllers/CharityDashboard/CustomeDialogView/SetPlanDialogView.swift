//
//  SetPlanDialogView.swift
//  Fintech
//
//  Created by broadstone on 06/01/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import Foundation
import UIKit
class SetPlanDialogView: UIView, UITextFieldDelegate {
    static let sharedInstance = SetPlanDialogView()
    
    
    @IBOutlet weak var parentView:UIView!
    @IBOutlet weak var targetAmount:UITextField!
    @IBOutlet weak var targetDate:UITextField!
    @IBOutlet weak var setPlanBtn:UIButton!
    
    let datePicker = UIDatePicker()
    
    var targetDateInUNIX: Int64!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    private func commonInit(){
        Bundle.main.loadNibNamed("CustomDialogView", owner: self, options: nil)
        addSubview(parentView)
        parentView.frame = self.bounds
        parentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        targetAmount.delegate = self
        createDatePicker()
    }
    
    
//    @objc func closeBtnTapped(){
//        parentView.removeFromSuperview()
//    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        if targetAmount.keyboardType == .decimalPad {
            let regex = try! NSRegularExpression(pattern: "^(^[0-9]{0,7})*([.,][0-9]{0,2})?$", options: .caseInsensitive)

            if let newText = (targetAmount.text as NSString?)?.replacingCharacters(in: range, with: string) {
                return regex.firstMatch(in: newText, options: [], range: NSRange(location: 0, length: newText.count)) != nil

            } else {
                return false
            }
        }else {
            return false
        }
    }
    
    func createDatePicker(){
        let toolbar  = UIToolbar()
        toolbar.sizeToFit()
        
        let doneBtn = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(donePressed))
        
        let spacer = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        
        let cancelBtn = UIBarButtonItem(barButtonSystemItem: .cancel, target: nil, action: #selector(cancelPressed))
        
        toolbar.setItems([cancelBtn, spacer, doneBtn], animated: true)
        targetDate.inputAccessoryView = toolbar
        targetDate.inputView = datePicker
        datePicker.minimumDate = .init(timeIntervalSince1970: NSDate.init().timeIntervalSince1970)
        datePicker.datePickerMode = .date
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
        }
        
    }
    
    @objc func donePressed(){
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        formatter.timeStyle = .none
        
        targetDate.text = formatter.string(from: datePicker.date)
        targetDateInUNIX = Int64 ((datePicker.date.timeIntervalSince1970*1000).rounded())
        endEditing(true)
    }
    
    @objc func cancelPressed(){
        endEditing(true)
    }
    
    func show(){
        UIApplication.shared.keyWindow?.addSubview(parentView)
    }
}
