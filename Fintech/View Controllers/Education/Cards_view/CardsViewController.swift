//
//  CardsViewController.swift
//  Fintech
//
//  Created by M Zaryab on 10/06/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import UIKit
import Koloda

class CardsViewController: UIViewController {
    
    //MARK:- Iboutlets
//    @IBOutlet weak var totalCards: UILabel!
    @IBOutlet weak var currentCard: UILabel!
    @IBOutlet weak var kolodaView: KolodaView!
    @IBOutlet weak var headingTxt: UILabel!
    //MARK:- Properties
    var index = 0
    var viewModelData = [CardsDataModel]()
    var doneFlag = Bool()
    var mainQuestion = Int()
    var innnerQuestion = String()
    var lastQuestion = Int()
    var lessonTitle = ""
    var answerId = ""
    let db = Databasehandler()
    var lessonsCardData = [LessonAnswerModel]()
    
    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        kolodaView.layer.cornerRadius = 20
        kolodaView.delegate = self
        kolodaView.dataSource = self
        print("qid is \(answerId)")
        print("question text is \(lessonTitle)")
        loadCardData()
        self.headingTxt.text = lessonTitle
//        totalCards.text = (lessonsCardData.count as NSNumber).stringValue
        currentCard.text = "\((kolodaView.currentCardIndex+1 as NSNumber).stringValue) / \((lessonsCardData.count as NSNumber).stringValue)"
        
        
    }
    //MARK:- Methods...
    func loadCardData(){
        lessonsCardData = db.readLessonAnswer(id: answerId)
        DispatchQueue.main.async {
            for item in self.lessonsCardData{
                self.viewModelData.append(CardsDataModel(text: item.lesson))
            }
            self.kolodaView?.reloadData()
        }
        print(viewModelData)
    }
    //MARK:- IBActions
    @IBAction func showNextCard(_ sender: Any) {
        print("next card.........")
        kolodaView?.swipe(.right)
        currentCard.text = "\((kolodaView.currentCardIndex+1 as NSNumber).stringValue) /  \((lessonsCardData.count as NSNumber).stringValue)"
        if kolodaView.currentCardIndex == lessonsCardData.count{
            let vc = storyboard?.instantiateViewController(withIdentifier: "AwardsViewController") as! AwardsViewController
            vc.mainQuestion = self.mainQuestion
            vc.innnerQuestion = self.innnerQuestion
            vc.lastQuestion = self.lastQuestion
            vc.doneFlag = self.doneFlag
            vc.lessonName = self.lessonTitle
            navigationController?.pushViewController(vc, animated: true)
        }
        else{
            currentCard.text = "\((kolodaView.currentCardIndex+1 as NSNumber).stringValue) /  \((lessonsCardData.count as NSNumber).stringValue)"
        }
    }
    
    @IBAction func showPreviousCard(_ sender: Any) {
        print("previous card.......")
        kolodaView?.revertAction()
//        currentCard.text = "\((kolodaView.currentCardIndex+1 as NSNumber).stringValue) / "
        currentCard.text = "\((kolodaView.currentCardIndex+1 as NSNumber).stringValue) /  \((lessonsCardData.count as NSNumber).stringValue)"
    }
    @IBAction func backBtn(_ sender: Any) {
        doneFlag = false
        selectedIndex -= 1
        navigationController?.popViewController(animated: true)
    }
}

//MARK:- Extension
extension CardsViewController: KolodaViewDelegate{
    func kolodaDidRunOutOfCards(_ koloda: KolodaView) {
        kolodaView?.reloadData()
    }
    func koloda(_ koloda: KolodaView, didSwipeCardAt index: Int, in direction: SwipeResultDirection) {
        print("card swipe towards \(direction) direction....")
    }
}
extension CardsViewController: KolodaViewDataSource{
    func kolodaNumberOfCards(_ koloda: KolodaView) -> Int {
        print(viewModelData.count)
        return lessonsCardData.count
        
    }
    func koloda(_ koloda: KolodaView, viewForCardAt index: Int) -> UIView {
        let view = UIView()
        view.frame = koloda.frame
        view.backgroundColor = .white
        view.layer.cornerRadius = 18
        view.clipsToBounds = true
        let contentLbl = UILabel()
        view.addSubview(contentLbl)
        contentLbl.frame = CGRect(x: 15,
                                  y: 5,
                                  width: view.frame.size.width - 30,
                                  height: view.frame.size.height - 10)
        contentLbl.font =  UIFont(name: "helvetica-neue", size: 20)
        contentLbl.textAlignment = .center
        contentLbl.numberOfLines = 0
        contentLbl.textColor = .black
        contentLbl.text = viewModelData[index].text
        return view
    }
    func kolodaPanFinished(_ koloda: KolodaView, card: DraggableCardView) {
        currentCard.text = "\((kolodaView.currentCardIndex+1 as NSNumber).stringValue) /  \((lessonsCardData.count as NSNumber).stringValue)"
        if kolodaView.currentCardIndex == viewModelData.count{
            print("out of cards")
            let vc = storyboard?.instantiateViewController(withIdentifier: "AwardsViewController") as! AwardsViewController
            vc.mainQuestion = self.mainQuestion
            vc.innnerQuestion = self.innnerQuestion
            vc.lastQuestion = self.lastQuestion
            vc.doneFlag = self.doneFlag
            vc.lessonName = self.lessonTitle
            navigationController?.pushViewController(vc, animated: true)
        }
        else{
//            currentCard.text = " / \((kolodaView.currentCardIndex+1 as NSNumber).stringValue) / "
            currentCard.text = "\((kolodaView.currentCardIndex+1 as NSNumber).stringValue) /  \((lessonsCardData.count as NSNumber).stringValue)"
        }
    }
}
