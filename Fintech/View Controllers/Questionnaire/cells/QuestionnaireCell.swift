//
//  QuestionnaireCell.swift
//  Fintech
//
//  Created by MacBook-Mubashar on 22/07/2019.
//  Copyright © 2019 Broadstone Technologies. All rights reserved.
//

import UIKit
import Foundation

class QuestionnaireCell: UITableViewCell {

    
    @IBOutlet weak var selectedAnswer_img: UIImageView!
    @IBOutlet weak var heading1_lbl: UILabel!
    @IBOutlet weak var collapseAble_view: UIView!
    @IBOutlet weak var heading2_lbl: UILabel!
    
    @IBOutlet weak var headingbody_lbl: UILabel!
    @IBOutlet weak var body_img: UIImageView!
    @IBOutlet weak var callback_lbl: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
