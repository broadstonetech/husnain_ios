//
//  Resources + ChatWithAdvisor.swift
//  Fintech
//
//  Created by broadstone on 13/01/2022.
//  Copyright © 2022 Broadstone Technologies. All rights reserved.
//

import Foundation

protocol ChatWithAdvisorDelegate {
    func startChatBtnPressed(advisorModel: AdvisorsModel)
}


extension ResourcesParent: ChatWithAdvisorDelegate {
    
    func goToChatVC(advisorName: String, chatId: String, chatHistory: [RecentChatLastMessage]) {
        self.tabBarController?.tabBar.isHidden = true
        let vc = AdvisorChatVC.init(nibName: nil, bundle: nil)
        vc.advisorName = advisorName
        vc.chatId = chatId
        vc.history = chatHistory
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func goToAdvisorProfile(advisorModel: AdvisorsModel) {
        let objViewController = self.storyboard?.instantiateViewController(withIdentifier: "AdvisorProfileVC") as! AdvisorProfiileVC
        objViewController.advisorData = advisorModel
        objViewController.advisorChatDelegate = self
        self.present(objViewController, animated: true, completion: nil)
    }
    
    func startChatBtnPressed(advisorModel: AdvisorsModel) {
        startChatWithAdvisor(advisorModel: advisorModel)
    }
}
