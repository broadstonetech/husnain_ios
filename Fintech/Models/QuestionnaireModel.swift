//
//  QuestionnaireModel.swift
//  Fintech
//
//  Created by MacBook-Mubashar on 23/07/2019.
//  Copyright © 2019 Broadstone Technologies. All rights reserved.
//

import Foundation


class QuestionnaireModel: NSObject {
    
    fileprivate var questionID : Int
    fileprivate var questionType : Int
    fileprivate var questionText : String
    fileprivate var questionGuidedText : String
    fileprivate var questionAttributedText : String
    fileprivate var categoryId : Int
    fileprivate var answerId : Int
    fileprivate var questionBackgroundImage : String
    fileprivate var questionGuidedImage : String
    var answersList : [[String : AnyObject]] = [[String : AnyObject]]()
    
    
    override init() {
        
        questionID = -1
        questionType = -1
        questionText = ""
        questionGuidedText = ""
        questionAttributedText = ""
        answerId = -1
        categoryId = -1
        questionGuidedImage = ""
        questionBackgroundImage = ""
        
    }
    
    func getQuestionID() -> Int {
        return questionID
    }
    func setQuestionID(id : Int) {
        self.questionID = id
    }
    //
    func getQuestionType() -> Int {
        return questionType
    }
    func setQuestionType(type : Int) {
        self.questionType = type
    }
    
    func getQuestionText() -> String {
        return questionText
    }
    func setQuestionText(text : String) {
        self.questionText = text
    }
    
    func getQuestionGuidedText() -> String {
        return questionGuidedText
    }
    func setQuestionGuidedText(text : String) {
        self.questionGuidedText = text
    }
    
    func getQuestionAttributedText() -> String {
        return questionAttributedText
    }
    func setQuestionAttributedText(text : String) {
        self.questionAttributedText = text
    }
    
    func getAnswerId () -> Int {
        return answerId
    }
    func setAnswerId (id : Int) {
        self.answerId = id
    }
    
    func getCategoryId () -> Int {
        return categoryId
    }
    func setCategoryId (id : Int) {
        self.categoryId = id
    }
    
    func getQuestionBackgroundImage() -> String {
        return questionBackgroundImage
    }
    func setQuestionBackgroundImage(text : String) {
        self.questionBackgroundImage = text
    }
    
    func getQuestionGuidedImage() -> String {
        return questionGuidedImage
    }
    func setQuestionGuidedImage(text : String) {
        self.questionGuidedImage = text
    }
}
