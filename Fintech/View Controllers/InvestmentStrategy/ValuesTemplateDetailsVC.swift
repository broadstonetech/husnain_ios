//
//  ValuesTemplateDetailsVC.swift
//  Fintech
//
//  Created by broadstone on 01/06/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import UIKit

class ValuesTemplateDetailsVC: Parent {
    
    struct ValuesTemplateDetailsStruct {
        var index: Int!
        var includedValues: [String]!
        var excludedValues: [String]!
    }
    
    let catholicTemplateIncludedVals = [
        getLocalizedString(key: "following_vals_included"),
        getLocalizedString(key: "animal_welfare"),
        getLocalizedString(key: "family_values"),
        getLocalizedString(key: "gender_equality"),
        getLocalizedString(key: "_immigration"),
        getLocalizedString(key: "climate_change"),
        getLocalizedString(key: "_biodiversity"),
        getLocalizedString(key: "energy_efficient"),
        getLocalizedString(key: "customer_satisfaction"),
        getLocalizedString(key: "data_protection"),
        getLocalizedString(key: "gender_and_diversity"),
        getLocalizedString(key: "employee_engagement"),
        getLocalizedString(key: "community_relation"),
        getLocalizedString(key: "human_rights"),
        getLocalizedString(key: "labor_standards"),
        getLocalizedString(key: "board_composition"),
        getLocalizedString(key: "audit_committee")
    ]
    
    let catholicTemplateExcludedVals = [
        getLocalizedString(key: "following_vals_excluded"),
        getLocalizedString(key: "_abortion"),
        getLocalizedString(key: "adult_entertainment"),
        getLocalizedString(key: "_contraceptives"),
        getLocalizedString(key: "_firarms"),
        getLocalizedString(key: "_tobacco"),
        getLocalizedString(key: "controversial_weapons"),
        getLocalizedString(key: "_lgbtq"),
        getLocalizedString(key: "air_pollution"),
        getLocalizedString(key: "_defrostation"),
        getLocalizedString(key: "water_scarcity")
    ]
    
    let envTemplateIncludedVals = [
        getLocalizedString(key: "following_vals_included"),
        getLocalizedString(key: "animal_welfare"),
        getLocalizedString(key: "nuclear_power"),
        getLocalizedString(key: "family_values"),
        getLocalizedString(key: "gender_equality"),
        getLocalizedString(key: "_immigration"),
        getLocalizedString(key: "_vegan"),
        getLocalizedString(key: "climate_change"),
        getLocalizedString(key: "_biodiversity"),
        getLocalizedString(key: "energy_efficient"),
        getLocalizedString(key: "waste_management"),
        getLocalizedString(key: "customer_satisfaction"),
        getLocalizedString(key: "data_protection"),
        getLocalizedString(key: "gender_and_diversity"),
        getLocalizedString(key: "employee_engagement"),
        getLocalizedString(key: "community_relation"),
        getLocalizedString(key: "human_rights"),
        getLocalizedString(key: "labor_standards"),
        getLocalizedString(key: "board_composition"),
        getLocalizedString(key: "audit_committee")
    ]
    
    let envTemplateExcludedVals = [
        getLocalizedString(key: "following_vals_excluded"),
        getLocalizedString(key: "_coal"),
        getLocalizedString(key: "_firarms"),
        getLocalizedString(key: "fur_leather"),
        getLocalizedString(key: "_gmo"),
        getLocalizedString(key: "palm_oil"),
        getLocalizedString(key: "_pesticides"),
        getLocalizedString(key: "_tobacco"),
        getLocalizedString(key: "controversial_weapons"),
        getLocalizedString(key: "_lgbtq"),
        getLocalizedString(key: "air_pollution"),
        getLocalizedString(key: "_defrostation"),
        getLocalizedString(key: "water_scarcity"),
        getLocalizedString(key: "bribary_corruption")
    ]
    
    let islamicTemplateIncludedVals = [
        getLocalizedString(key: "following_vals_included"),
        getLocalizedString(key: "animal_welfare"),
        getLocalizedString(key: "family_values"),
        getLocalizedString(key: "gender_equality"),
        getLocalizedString(key: "_immigration"),
        getLocalizedString(key: "climate_change"),
        getLocalizedString(key: "_biodiversity"),
        getLocalizedString(key: "energy_efficient"),
        getLocalizedString(key: "customer_satisfaction"),
        getLocalizedString(key: "data_protection"),
        getLocalizedString(key: "gender_and_diversity"),
        getLocalizedString(key: "employee_engagement"),
        getLocalizedString(key: "community_relation"),
        getLocalizedString(key: "human_rights"),
        getLocalizedString(key: "labor_standards"),
        getLocalizedString(key: "board_composition"),
        getLocalizedString(key: "audit_committee")
    ]
    
    let islamicTemplateExcludedVals = [
        getLocalizedString(key: "following_vals_excluded"),
        getLocalizedString(key: "_abortion"),
        getLocalizedString(key: "adult_entertainment"),
        getLocalizedString(key: "_alcohol"),
        getLocalizedString(key: "_contraceptives"),
        getLocalizedString(key: "_firarms"),
        getLocalizedString(key: "_gambling"),
        getLocalizedString(key: "interest_bearing"),
        getLocalizedString(key: "_tobacco"),
        getLocalizedString(key: "controversial_weapons"),
        getLocalizedString(key: "_lgbtq"),
        getLocalizedString(key: "air_pollution"),
        getLocalizedString(key: "_defrostation"),
        getLocalizedString(key: "water_scarcity")
    ]
    
    
    let methodistTemplateIncludedVals = [
        getLocalizedString(key: "following_vals_included"),
        getLocalizedString(key: "animal_welfare"),
        getLocalizedString(key: "family_values"),
        getLocalizedString(key: "gender_equality"),
        getLocalizedString(key: "_immigration"),
        getLocalizedString(key: "climate_change"),
        getLocalizedString(key: "_biodiversity"),
        getLocalizedString(key: "energy_efficient"),
        getLocalizedString(key: "customer_satisfaction"),
        getLocalizedString(key: "data_protection"),
        getLocalizedString(key: "gender_and_diversity"),
        getLocalizedString(key: "employee_engagement"),
        getLocalizedString(key: "community_relation"),
        getLocalizedString(key: "human_rights"),
        getLocalizedString(key: "labor_standards"),
        getLocalizedString(key: "board_composition"),
        getLocalizedString(key: "audit_committee")
    ]
    
    let methodistTemplateExcludedVals = [
        getLocalizedString(key: "following_vals_excluded"),
        getLocalizedString(key: "_abortion"),
        getLocalizedString(key: "adult_entertainment"),
        getLocalizedString(key: "_contraceptives"),
        getLocalizedString(key: "_firarms"),
        getLocalizedString(key: "_tobacco"),
        getLocalizedString(key: "controversial_weapons"),
        getLocalizedString(key: "_lgbtq"),
        getLocalizedString(key: "air_pollution"),
        getLocalizedString(key: "_defrostation"),
        getLocalizedString(key: "water_scarcity")
    ]
    
    let peaceTemplateIncludedVals = [
        getLocalizedString(key: "following_vals_included"),
        getLocalizedString(key: "animal_welfare"),
        getLocalizedString(key: "family_values"),
        getLocalizedString(key: "gender_equality"),
        getLocalizedString(key: "_lgbtq"),
        getLocalizedString(key: "_immigration"),
        getLocalizedString(key: "_vegan"),
        getLocalizedString(key: "climate_change"),
        getLocalizedString(key: "_biodiversity"),
        getLocalizedString(key: "energy_efficient"),
        getLocalizedString(key: "customer_satisfaction"),
        getLocalizedString(key: "data_protection"),
        getLocalizedString(key: "gender_and_diversity"),
        getLocalizedString(key: "employee_engagement"),
        getLocalizedString(key: "community_relation"),
        getLocalizedString(key: "human_rights"),
        getLocalizedString(key: "labor_standards"),
        getLocalizedString(key: "board_composition"),
        getLocalizedString(key: "audit_committee")
    ]
    
    let peaceTemplateExcludedVals = [
        getLocalizedString(key: "following_vals_excluded"),
        getLocalizedString(key: "_coal"),
        getLocalizedString(key: "_firarms"),
        getLocalizedString(key: "fur_leather"),
        getLocalizedString(key: "_gmo"),
        getLocalizedString(key: "palm_oil"),
        getLocalizedString(key: "_pesticides"),
        getLocalizedString(key: "_tobacco"),
        getLocalizedString(key: "controversial_weapons"),
        getLocalizedString(key: "military_contract"),
        getLocalizedString(key: "air_pollution"),
        getLocalizedString(key: "_defrostation"),
        getLocalizedString(key: "waste_management"),
        getLocalizedString(key: "water_scarcity"),
        getLocalizedString(key: "bribary_corruption")
    ]
    
    let veganTemplateIncludedVals = [
        getLocalizedString(key: "following_vals_included"),
        getLocalizedString(key: "animal_welfare"),
        getLocalizedString(key: "family_values"),
        getLocalizedString(key: "gender_equality"),
        getLocalizedString(key: "_lgbtq"),
        getLocalizedString(key: "_immigration"),
        getLocalizedString(key: "_vegan"),
        getLocalizedString(key: "climate_change"),
        getLocalizedString(key: "_biodiversity"),
        getLocalizedString(key: "energy_efficient"),
        getLocalizedString(key: "customer_satisfaction"),
        getLocalizedString(key: "data_protection"),
        getLocalizedString(key: "gender_and_diversity"),
        getLocalizedString(key: "employee_engagement"),
        getLocalizedString(key: "community_relation"),
        getLocalizedString(key: "human_rights"),
        getLocalizedString(key: "labor_standards"),
        getLocalizedString(key: "board_composition"),
        getLocalizedString(key: "audit_committee")
    ]
    
    let veganTemplateExcludedVals = [
        getLocalizedString(key: "following_vals_excluded"),
        getLocalizedString(key: "fur_leather"),
        getLocalizedString(key: "_gmo"),
        getLocalizedString(key: "palm_oil"),
        getLocalizedString(key: "_pesticides"),
        getLocalizedString(key: "_tobacco"),
        getLocalizedString(key: "air_pollution"),
        getLocalizedString(key: "_defrostation"),
        getLocalizedString(key: "water_scarcity")
    ]
    
    
    var tableViewData = [ValuesTemplateDetailsStruct]()
    
    var index = 0
    var templateName = ""
    
    
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var titleLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        //self.title = templateName
        titleLabel.text = templateName
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableViewData = [
            ValuesTemplateDetailsStruct(index: 0, includedValues: catholicTemplateIncludedVals, excludedValues: catholicTemplateExcludedVals),
            ValuesTemplateDetailsStruct(index: 1, includedValues: envTemplateIncludedVals, excludedValues: envTemplateExcludedVals),
            ValuesTemplateDetailsStruct(index: 2, includedValues: islamicTemplateIncludedVals, excludedValues: islamicTemplateExcludedVals),
            ValuesTemplateDetailsStruct(index: 3, includedValues: methodistTemplateIncludedVals, excludedValues: methodistTemplateExcludedVals),
            ValuesTemplateDetailsStruct(index: 4, includedValues: peaceTemplateIncludedVals, excludedValues: peaceTemplateExcludedVals),
            ValuesTemplateDetailsStruct(index: 5, includedValues: veganTemplateIncludedVals, excludedValues: veganTemplateExcludedVals)
        ]
                             
    }
    

}
extension ValuesTemplateDetailsVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return tableViewData[index].includedValues.count
        }
        else if section == 1 {
            return tableViewData[index].excludedValues.count
        }
        else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: ValuesTemplateDetailCell
        print("Section: \(indexPath.section)")
        if indexPath.row == 0 {
            cell = tableView.dequeueReusableCell(withIdentifier: "ValuesTemplateDetailCellHeading") as! ValuesTemplateDetailCell
            
        }else {
            cell = tableView.dequeueReusableCell(withIdentifier: "ValuesTemplateDetailCellTags") as! ValuesTemplateDetailCell
        }
        if indexPath.section == 0 {
            cell.titleLabel.text = tableViewData[index].includedValues[indexPath.row]
        }else {
            cell.titleLabel.text = tableViewData[index].excludedValues[indexPath.row]
        }
        return cell
    }
}
