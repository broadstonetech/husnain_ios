//
//  QuizCell.swift
//  Fintech
//
//  Created by apple on 08/07/2020.
//  Copyright © 2020 Broadstone Technologies. All rights reserved.
//

import UIKit

class QuizCell: UITableViewCell {

    @IBOutlet weak var optionLbl: UILabel!
    @IBOutlet weak var backView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
