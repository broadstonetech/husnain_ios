//
//  CorelationDialogView.swift
//  Fintech
//
//  Created by broadstone on 17/07/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import UIKit

class CorelationDialogView: UIView {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var parentView: UIView!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var dataAvailableView: UIView!
    @IBOutlet weak var dataNotAvailableLabel: UILabel!
    @IBOutlet weak var categoryIcon: UIImageView!

    var corelationModel: SpendingCorelationModel!
    
    var positiveImpact = [SpendingCoreationData]()
    var negativeImpact = [SpendingCoreationData]()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    private func commonInit(){
        Bundle.main.loadNibNamed("CorelationDialogView", owner: self, options: nil)
        addSubview(parentView)
        parentView.frame = self.bounds
        parentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        tableView.delegate = self
        tableView.dataSource = self
        
    }
    
//    @objc func closeBtnTapped(){
//        parentView.removeFromSuperview()
//    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    
    
    
    func show(){
        UIApplication.shared.keyWindow?.addSubview(parentView)
    }
    
    func setCategoryName(name: String) {
        categoryLabel.text = name
        
        categoryIcon.image = FintechUtils.sharedInstance.getCaetgoryColoredImageForBudgeting(value: name.lowercased())
    }
    
    func setViewValues(){
        corelationModel.data.remove(at: 0)
        for item in corelationModel.data {
            if item.value >= 0.0 {
                positiveImpact.append(item)
            }else {
                negativeImpact.append(item)
            }
        }
        
        segmentControl.selectedSegmentIndex = 0
        if positiveImpact.count > 0 {
            dataAvailableView.isHidden = false
            dataNotAvailableLabel.isHidden = true
        }else {
            dataAvailableView.isHidden = true
            dataNotAvailableLabel.isHidden = false
        }
        
        
    }
    
    @IBAction func segementIndexChanged(_ sender: UISegmentedControl) {
        switch segmentControl.selectedSegmentIndex {
        case 0:
            if positiveImpact.count > 0 {
                dataAvailableView.isHidden = false
                dataNotAvailableLabel.isHidden = true
            }else {
                dataAvailableView.isHidden = true
                dataNotAvailableLabel.isHidden = false
            }
            break
        case 1:
            if negativeImpact.count > 0 {
                dataAvailableView.isHidden = false
                dataNotAvailableLabel.isHidden = true
            }else {
                dataAvailableView.isHidden = true
                dataNotAvailableLabel.isHidden = false
            }
            break
        default:
            break
        }
        
        tableView.reloadData()
    }

}

extension CorelationDialogView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if segmentControl.selectedSegmentIndex == 0 {
            return positiveImpact.count
        }else {
            return negativeImpact.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let identifier = "RecurringChargesNibCell"
        var cell: RecurringChargesNibCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? RecurringChargesNibCell
        if cell == nil {
            tableView.register(UINib(nibName: "RecurringChargesNibDialogCell", bundle: nil), forCellReuseIdentifier: identifier)
            cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? RecurringChargesNibCell
        }
        
        cell.date.text = corelationModel.data[indexPath.row].categoryName
        cell.amount.text = FintechUtils.sharedInstance.doubleToRoundedOutput(doubleVal: corelationModel.data[indexPath.row].value * 100.0) + "%"
        
        return cell
    }
}
