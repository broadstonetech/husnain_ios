//
//  ManualTrackingData.swift
//  Fintech
//
//  Created by broadstone on 08/07/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import Foundation
struct ManualTrackingData {
    var needsBucketData: [ManualTrackingBucketData]
    var wantsBucketData: [ManualTrackingBucketData]
    var havesBucketData: [ManualTrackingBucketData]
}

struct ManualTrackingBucketData {
    var index: Int
    var categoryName: String
    var amount: Double
    var spendingDate: Int64
    var marchantName: String
}
