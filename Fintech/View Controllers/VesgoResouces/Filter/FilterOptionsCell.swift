//
//  FilterOptionsCell.swift
//  Fintech
//
//  Created by broadstone on 19/01/2022.
//  Copyright © 2022 Broadstone Technologies. All rights reserved.
//

import UIKit

class FilterOptionsCell: UITableViewCell {
    
    @IBOutlet weak var optionLabel: UILabel!
    @IBOutlet weak var optionImage: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
