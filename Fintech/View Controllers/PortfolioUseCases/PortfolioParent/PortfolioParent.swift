//
//  PortfolioParent.swift
//  Fintech
//
//  Created by broadstone on 09/08/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import UIKit



class PortfolioParent: Parent {
    
    var portfolioImpactModel: PortfolioImpact!
    var portfolioListModel: PortfolioListModel!
    var valueMappedDelegate: ValueMappedApiDelegate?
    var portfolioSavedDalegate: SavePortfolioDelegate?
    var portfolioListApiDalegate: PortfolioListApiDalegate?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    
    
    func generatePortfolioAPI(url: String, params: [String: AnyObject], useCaseType: UseCaseType, for update: Bool){
        self.view.activityStartAnimating()
        print(params)
        
        APIManager.sharedInstance.postRequest(serviceName: url, sendData: params, success: { (response) in
            print(response)
            
            if APIManager.sharedInstance.isSuccess {
                DispatchQueue.main.async {
                    let _ = self.fetchExternalPortfolioResponse(res: response)
                    if update {
                        self.pushAssetAllocationVCForUpdate(useCaseType: useCaseType)
                    }else {
                        if (useCaseType == .UseCase1 || useCaseType == .UseCase3) && url != ApiUrl.GET_PORTFOLIO_DETAILS{
                            if self.portfolioImpactModel.data.portfolioInfo.portfolioName == "" {
                                
                                self.goToGeneratedPortfolioVC(useCaseType: useCaseType, model: nil)
                            }else {
                                self.portfolioSavedDalegate?.portfolioSaved(success: true)
                            }
                        }else {
                            self.goToGeneratedPortfolioVC(useCaseType: useCaseType, model: nil)
                        }
                    }
                    
                }
            }else{
                
                DispatchQueue.main.async { [self] in
                    if response["message"] != nil {
                        self.showAlert(response["message"] as! String)
                    } else {
                        self.showAlert(ConstantStrings.NO_RESULT_FOUND_ALERT_TEXT)
                    }
                }
            }
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
        }, failure: { (data) in
            print("Error: \(data)")
            DispatchQueue.main.async { [self] in
                view.activityStopAnimating()
            }
            if APIManager.sharedInstance.error400 {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
            }
            if(APIManager.sharedInstance.error401)
            {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
            }else{
                self.showAlert(ConstantStrings.Error_msg_generic)
            }
        })
    }
    
    
    func fetchExternalPortfolioResponse(res: [String: AnyObject]) -> PortfolioImpact? {
        let success = res["success"] as! Bool
        let message = res["message"] as! String
        
        if success == false {
            return nil
        }
        let allData = res["data"] as! [String: AnyObject]
        let data = allData["esg_calculations"] as! [String: AnyObject]
        
        let portfolioName = (allData["portfolio_name"] as? String) ?? ""
        let portfolioId = (allData["portfolio_id"] as? Int) ?? -1
        let portfolioType = allData["portfolio_type"] as? String
        
        let portfolioInfo = PortfolioInfo(portfolioName: portfolioName, portfolioId: portfolioId, portfolioType: portfolioType ?? "")
        
        let controversyData = fetchControversyData(res: data["controvercy_data"] as! [String: AnyObject])
        
        let overAllESG = data["overall_esg"] as! [String: AnyObject]
        
        let impact = overAllESG["impact"] as! String
        let betterPeers = (overAllESG["better_peers"] as? String) ?? ""
        let badPeers = (overAllESG["bad_peers"] as? String) ?? ""
        let riskScore = (overAllESG["risk_score"] as? Double) ?? 0.0
        let zScore = (overAllESG["z_score"] as? Double) ?? 0.0
        
        
        let portfolioEnvData = fetchEsgValues(portfolioComposition: data, key: "Environment")
        let portfolioSocialData = fetchEsgValues(portfolioComposition: data, key: "Social")
        let portfolioGovData = fetchEsgValues(portfolioComposition: data, key: "Governance")
        
        let assets = data["assets"] as! [AnyObject]

        var portfolioAssets = [PortfolioCreationAssets]()
        for i in 0..<assets.count {
            let asset = assets[i] as! [String: AnyObject]
            let ticker = asset["ticker"] as! String
            let company = asset["Symbol_name"] as! String
            let industry = asset["industry"] as! String
            let allocation = asset["allocation"] as! Double
            let tickerComposition = asset["portfolio_composition"] as! [String: AnyObject]
            
            
            let impact = tickerComposition["overall_impact"] as! String
            let betterPeers = (tickerComposition["overall_better_peers"] as? String) ?? ""
            let badPeers = (tickerComposition["overall_bad_peers"] as? String) ?? ""
            
            let env = fetchEsgValues(portfolioComposition: tickerComposition, key: "environment")
            let social = fetchEsgValues(portfolioComposition: tickerComposition, key: "social")
            let governance = fetchEsgValues(portfolioComposition: tickerComposition, key: "governance")
            
            let composition = PortfolioCreationESG.init(impact: impact, overallBetterPeers: betterPeers, overallBadPeers: badPeers, zScore: 0.0, risk: 0.0, environment: env, social: social, governance: governance)
            
            portfolioAssets.append(PortfolioCreationAssets.init(ticker: ticker, symbolName: company, industry: industry, allocation: allocation, portfolioComposition: composition))
        }
        
        let portfolioCompostion = PortfolioCreationESG.init(impact: impact, overallBetterPeers: betterPeers, overallBadPeers: badPeers, zScore: zScore, risk: riskScore, environment: portfolioEnvData, social: portfolioSocialData, governance: portfolioGovData)
        
        let portfolioData = PortfolioCreationData.init(overallESG: portfolioCompostion, assets: portfolioAssets, portfolioInfo: portfolioInfo, controversyData: controversyData)
        
        self.portfolioImpactModel = PortfolioImpact.init(success: success, message: message, data: portfolioData)
        
        return portfolioImpactModel
    }
    
    func fetchEsgValues(portfolioComposition: [String: AnyObject], key: String) -> PortfolioCreationComposition{
        let esg = portfolioComposition[key] as! [String: AnyObject]
        let impact = esg["impact"] as! String
        let betterPeers = (esg["better_peers"] as? String) ?? ""
        let badPeers = (esg["bad_peers"] as? String) ?? ""
        let riskScore = (esg["risk_score"] as? Double) ?? 0.0
        let zScore = (esg["z_score"] as? Double) ?? 0.0
        
        let derviedValues = esg["derived_values"] as? [AnyObject] ?? []
    
        var vals = [PortfolioDerivedValues]()
        
        for i in 0..<derviedValues.count {
            let item = derviedValues[i] as! [String: AnyObject]
            let value = item["name"] as! String
            let valScore = item["score"] as! Double
            
            vals.append(PortfolioDerivedValues.init(value: value, score: valScore))
        }
        
        let esgData = PortfolioCreationComposition.init(impact: impact, betterPeers: betterPeers, badPeers: badPeers, zScore: zScore, risk: riskScore, derivedValues: vals)
        
        return esgData
        
    }
    
    private func fetchControversyData(res: [String: AnyObject]) -> ControversyData {
        let count = res["Controversy_count"] as! Int
        let peerControversy = (res["avgPeerControversy"] as? Double) ?? 0.0
        let controversies = res["relatedControversy"] as! [AnyObject]
        
        var data = [String]()
        
        for i in 0..<controversies.count {
            data.append(controversies[i] as! String)
        }
        
        return ControversyData.init(count: count, controversies: data, peerControversy: peerControversy)
    }
    
    func getPortfolioList(useCaseType: UseCaseType, isNavigatingBack: Bool){
        self.view.activityStartAnimating()
        let url = ApiUrl.GET_PORTFOLIO_LIST
        let params = getPortfolioListParams(useCaseType: useCaseType)
        print(params)
        
        APIManager.sharedInstance.postRequest(serviceName: url, sendData: params, success: { (response) in
            //print(response)
            
            if APIManager.sharedInstance.isSuccess {
                DispatchQueue.main.async { [self] in
                    fetchPortfolioListResponse(res: response)
                    if isNavigatingBack {
                        portfolioListApiDalegate?.portfolioListApiCalled(model: portfolioListModel.data)
                    }else {
                        if portfolioListModel.data.count > 0 {
                            goToPortfolioListVC(useCaseType: useCaseType)
                        }else {
                            if useCaseType == .UseCase1 {
                                let vc = ExternalPortfolioVC()
                                self.navigationController?.pushViewController(vc, animated: true)
                            }
                            else if useCaseType == .UseCase2 {
                                pushAddSymbolVC()
                            }else if useCaseType == .UseCase3 {
                                pushPortfolioTemplateVC()
                            }
                        }
                    }
                }
            }else{
                
                DispatchQueue.main.async { [self] in
                    if response["message"] != nil {
                        self.showAlert(response["message"] as! String)
                    } else {
                        self.showAlert(ConstantStrings.NO_RESULT_FOUND_ALERT_TEXT)
                    }
                }
            }
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
        }, failure: { (data) in
            print(data)
            DispatchQueue.main.async { [self] in
                view.activityStopAnimating()
            }
            if APIManager.sharedInstance.error400 {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
            }
            if(APIManager.sharedInstance.error401)
            {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
            }else{
                self.showAlert(ConstantStrings.Error_msg_generic)
            }
            DispatchQueue.main.async { [self] in
                if useCaseType == .UseCase2 {
                    pushAddSymbolVC()
                }else if useCaseType == .UseCase3 {
                    //TODO: Start a NO case of UC3
                }
            }
        })
    }
    
    private func getPortfolioListParams(useCaseType: UseCaseType) -> [String: AnyObject]{
        if useCaseType == .UseCase1 {
            let data = ["portfolio_type": "broker"] as [String: AnyObject]
            return ["namespace": SPManager.sharedInstance.getCurrentUserNamespace(),
                    "data": data] as [String: AnyObject]
        }
        else if useCaseType == .UseCase2 {
            let data = ["portfolio_type": "ticker_based"] as [String: AnyObject]
            return ["namespace": SPManager.sharedInstance.getCurrentUserNamespace(),
                    "data": data] as [String: AnyObject]
        }else if useCaseType == .UseCase3 {
            let data = ["portfolio_type": "value_based"] as [String: AnyObject]
            return ["namespace": SPManager.sharedInstance.getCurrentUserNamespace(),
                    "data": data] as [String: AnyObject]
        }else {
            return ["namespace": SPManager.sharedInstance.getCurrentUserNamespace()] as [String: AnyObject]
        }
    }
    
    private func fetchPortfolioListResponse(res: [String: AnyObject]) {
        let success = res["success"] as! Bool
        let message = res["message"] as! String
        let data = res["data"] as! [AnyObject]
        
        var portfolios = [PortfolioListData]()
        for i in 0..<data.count {
            let portfolio = data[i] as! [String: AnyObject]
            let portfolioId = portfolio["portfolio_id"] as! Int
            let portfolioName = portfolio["portfolio_name"] as! String
            
            portfolios.append(PortfolioListData.init(portfolioId: portfolioId, portfolioName: portfolioName))
        }
        
        portfolioListModel = PortfolioListModel.init(success: success, message: message, data: portfolios)
        
    }
    
    
}

//MARK:- Value Mapped Tickers API
extension PortfolioParent {
    func callValueMappedTickerAPI(bucketType: String, isNavigatingToNextVC: Bool, valueName: String, risk: Int){
        self.view.activityStartAnimating()
        let url = ApiUrl.GET_VALUE_MAPPED_TICKERS
        
        APIManager.sharedInstance.postRequest(serviceName: url, sendData: getValueMappedTickerParams(valueName: valueName, bucketType: bucketType, risk: Int(risk)), success: { (response) in
            print(response)
            
            if APIManager.sharedInstance.isSuccess {
                DispatchQueue.main.async {
                    self.fetchValueMappedTickerData(res: response, isNavigatingToNextVC: isNavigatingToNextVC, bucketType: bucketType, valueName: valueName, risk: risk)
                }
            }else{
                
                DispatchQueue.main.async { [self] in
                    if response["message"] != nil {
                        self.showAlert(response["message"] as! String)
                    } else {
                        self.showAlert(ConstantStrings.NO_RESULT_FOUND_ALERT_TEXT)
                    }
                }
            }
            DispatchQueue.main.async {
                self.view.activityStopAnimating()
            }
        }, failure: { (data) in
            print(data)
            DispatchQueue.main.async { [self] in
                view.activityStopAnimating()
            }
            if APIManager.sharedInstance.error400 {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_400)
            }
            if(APIManager.sharedInstance.error401)
            {
                self.showAlert(ConstantStrings.GENERIC_ALERT_TEXT_401)
            }else{
                self.showAlert(ConstantStrings.Error_msg_generic)
            }
        })
    }
    
    func getValueMappedTickerParams(valueName: String, bucketType: String, risk: Int) -> [String: AnyObject]{
        let data = ["bucket_type": bucketType,
                    "value_name": valueName,
                    "risk": risk] as [String: AnyObject]
        
        return ["namespace": SPManager.sharedInstance.getCurrentUserNamespace(),
                "data": data] as [String: AnyObject]
    }
    
    func fetchValueMappedTickerData(res: [String: AnyObject], isNavigatingToNextVC: Bool, bucketType: String, valueName: String, risk: Int){
        let success = res["success"] as! Bool
        let message = res["message"] as! String
        let data = res["data"] as! [AnyObject]
        
        var assets = [PortfolioTickersData]()
        for j in 0..<data.count {
            let asset = data[j] as! [String: String]
            let companyName = asset["company_name"] ?? ""
            let ticker = asset["ticker"] ?? ""
            
            assets.append(PortfolioTickersData.init(symbol: ticker, CompanyName: companyName))
        }
        
        let valueMappedTickersModel = ValueMappedTickersModel.init(success: success, message: message, data: assets)
        
        if isNavigatingToNextVC {
            //pushValueMappedTickerVC(tickersData: valueMappedTickersModel, args: args)
            pushValueMappedTickerVC(tickersData: valueMappedTickersModel, risk: risk)
        }else {
//            if bucketType == "average" {
//                averageModel = valueMappedTickersModel
//            }else {
//                laggardModel = valueMappedTickersModel
//            }
            valueMappedDelegate?.apiCalled(valueName: valueName, bucketType: bucketType, model: valueMappedTickersModel)
        }
    }
    //MARK: Values Calculation Params
    func getValuesCalculationParams(template: String, rawRisk: Int, portfolioType: String, postiveVals: [String], negativeVals: [String]) -> [String: AnyObject] {
        let templateType = QuestionnaireCalculationHelper.sharedInstance.getSelectedTemplateFromString(template)
        
        switch templateType {
            case .Catholic:
            return QuestionnaireCalculationHelper.sharedInstance.getCatholicTemplateParams(isPortfolioUseCase: true, risk: rawRisk, portfolioType: portfolioType)
                    
            case .Environment:
                return QuestionnaireCalculationHelper.sharedInstance.getEnvTemplateParams(isPortfolioUseCase: true, risk: rawRisk, portfolioType: portfolioType)
                    
            case .Islamic:
                return QuestionnaireCalculationHelper.sharedInstance.getIslamicTemplateParams(isPortfolioUseCase: true, risk: rawRisk, portfolioType: portfolioType)
                    
            case .Methodist:
                return QuestionnaireCalculationHelper.sharedInstance.getMethodistTemplateParams(isPortfolioUseCase: true, risk: rawRisk, portfolioType: portfolioType)
                    
            case .Peace:
                return QuestionnaireCalculationHelper.sharedInstance.getPeaceTemplateParams(isPortfolioUseCase: true, risk: rawRisk, portfolioType: portfolioType)
                    
            case .Vegan:
                return QuestionnaireCalculationHelper.sharedInstance.getVeganTemplateParams(isPortfolioUseCase: true, risk: rawRisk, portfolioType: portfolioType)
            case .Social:
                return QuestionnaireCalculationHelper.sharedInstance.getSocTemplateParams(isPortfolioUseCase: true, risk: rawRisk, portfolioType: portfolioType)
            case .Governance:
                return QuestionnaireCalculationHelper.sharedInstance.getGovTemplateParams(isPortfolioUseCase: true, risk: rawRisk, portfolioType: portfolioType)
            case .NOT_SET:
                return getManualEnteredValuesParams(negativeVals: negativeVals, positiveVals: postiveVals, portfolioType: portfolioType, risk: rawRisk)
            default:
                return getManualEnteredValuesParams(negativeVals: negativeVals, positiveVals: postiveVals, portfolioType: portfolioType, risk: rawRisk)
    
        
        }
    }
    func getManualEnteredValuesParams(negativeVals: [String], positiveVals: [String], portfolioType: String, risk: Int) -> [String: AnyObject] {
        return [
                "positive_values": positiveVals,
                "negative_values": negativeVals,
                "esg_percentile": 0,
                "env_percentile": 0,
                "soc_percentile": 0,
                "gov_percentile": 0,
                "minorities": 0,
                "interest_bearing": QuestionnaireCalculationHelper.sharedInstance.getInterestBearingValue(negativeValues: negativeVals),
                "risk": QuestionnaireCalculationHelper.sharedInstance.getRiskDist(risk),
                "template": "",
                "portfolio_type": portfolioType]
                    as [String: AnyObject]
    }

}
