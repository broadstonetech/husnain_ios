//
//  personalValuesDetailsVC.swift
//  Fintech
//
//  Created by Apple on 10/09/2020.
//  Copyright © 2020 Broadstone Technologies. All rights reserved.
//

import UIKit

var imgname = ""
var detailsname = ""
let headingarry = ["What dose this score means?","Score calculation","What can I do to improve this score?"]
var descripation = ["This score indicates a company's commitment to value through investments and corporate initiatives.",
                    "The score for this particular value is derived by observing many factors including a company’s line of business, active initiatives, and commitment to certain values. This information is collected by the Vesgo team through company filings, news, press releases, and active shareholder petitions.",]
var descripationdetails = ["Environmental score evaluates companies on the conservation and protection of the natural environment.","Factors considered are: \n \u{2022} Carbon emissions and air quality \n \u{2022} Energy use and conservation \n \u{2022} Natural resources and land use \n \u{2022} Waste management and water quality \n \u{2022} Hazardous materials use"]
var socialdescripationdetails = ["Social score measures the company’s relationship with employees, suppliers, clients, and communities.","The following considerations are evaluated: \n \u{2022} Labor standards and employee relations \n \u{2022} Production quality and safety \n \u{2022} Local community impact \n \u{2022} Equal employment opportunities \n \u{2022} Health care, education, and housing services"
]
var grovancedescripation = ["This score indicates a company's commitment to value through investments and corporate initiatives.","Governance evaluates the standards for corporate governance, company leadership, risk controls, and shareholder rights including the following: \n \u{2022} Ethical business practices \n \u{2022} Board independence and diversity   \n \u{2022} Voting rights \n \u{2022} Executive pay vs. employee pay \n \u{2022}  Account and tax transparency"]




class personaValuesDetailsVC: UIViewController {

    @IBOutlet weak var img_icon: UIImageView!
    
    @IBOutlet weak var productname_lbl: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.isNavigationBarHidden = false
        
        self.tableView.rowHeight = 400
        self.tableView.estimatedRowHeight = 400
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.isNavigationBarHidden = true
        productname_lbl.text = detailsname
        img_icon.image = FintechUtils.sharedInstance.getInvestmentValuesIcons(investmentValue: detailsname)
    }
    

   
     @IBAction func Back_pressed(_ sender: Any) {
        navigationController?.popViewController(animated: true)

        
     }
     /*
     // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
}

extension personaValuesDetailsVC:UITableViewDelegate,UITableViewDataSource{
    // MARK: - TableView Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 2{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ScoreImprovementCell") as! ScoreImporovementCell
            cell.initLabels()
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! personadetailTableViewCell
            cell.heading_lbl.text = headingarry[indexPath.row]
            if detailsname == "Environment" {
                cell.descripation_lbl.text = descripationdetails[indexPath.row]
                return cell

            }else if detailsname == "Social"{
                cell.descripation_lbl.text = socialdescripationdetails[indexPath.row]
                return cell

            }else if detailsname == "Governance"{
                cell.descripation_lbl.text = grovancedescripation[indexPath.row]
                return cell

            }else {
                cell.descripation_lbl.text = descripation[indexPath.row]
                return cell
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 150
        }else if indexPath.row == 1{
            if detailsname == "Governance"{
                return 220
            }
            return 205
        }else {
            return 600
        }
        
    }
}
