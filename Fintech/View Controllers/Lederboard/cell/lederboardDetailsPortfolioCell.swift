//
//  lederboardDetailsPortfolioCell.swift
//  Fintech
//
//  Created by Aqib on 18/09/2020.
//  Copyright © 2020 Broadstone Technologies. All rights reserved.
//

import UIKit

class lederboardDetailsPortfolioCell: UITableViewCell {
    @IBOutlet weak var name_stockallocation_lbl: UILabel!
    @IBOutlet weak var values_stockallocation_lbl: UILabel!
    @IBOutlet weak var ticker_stockallocation_lbl: UILabel!
    @IBOutlet weak var tickerTitle_stockallocation_lbl: UILabel!
    
    @IBOutlet weak var tickerSahre_stockAllocation_lbl: UILabel!
    @IBOutlet weak var img_value: UIImageView!
    @IBOutlet weak var heading_lbl_value: UILabel!
    @IBOutlet weak var heading_lbl_impact: UILabel!
    @IBOutlet weak var details_img_values: UIImageView!
    @IBOutlet weak var img_heading_impact: UIImageView!
    
    @IBOutlet weak var presentage_impact_lbl: UILabel!
    @IBOutlet weak var main_heading_Invesmentimpact: UILabel!
    
    @IBOutlet weak var main_heading_values: UILabel!
    @IBOutlet weak var score_lbl: UILabel!
    
    @IBOutlet weak var img_drivevalues_impact: UIImageView!
    
    @IBOutlet weak var heading_derivevalues_impact_lbl: UILabel!
    
    @IBOutlet weak var persentage_derivevalues_impact_lbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
